<?php

    $classTripCart = _autoload("classTripCart");

    $OrderId = (int)$_REQUEST['OrderId'] ?: (int)$_REQUEST['orderNumber'];
    $Order = DB::selectArray("SELECT * FROM cartData WHERE Ord_Code = $OrderId");
    if(!$Order['Ord_Code']) {
        header("HTTP/1.1 404 Not Found");
        header("Status: 404 Not Found");
        include($_SERVER['DOCUMENT_ROOT']."/error.php");
        die;
    }

    if($_REQUEST[classTripCart::$DoID] == "PaymentSuccess" || $_REQUEST['tsdo'] == "PaymentSuccess") {
        header("Location: /cart/?".classTripCart::$DoID."=PaymentSuccess&OrderId=$OrderId");
        die;
    } elseif($_REQUEST[classTripCart::$DoID] == "PaymentFail" || $_REQUEST['tsdo'] == "PaymentFail") {
        header("Location: /cart/?".classTripCart::$DoID."=PaymentFail&OrderId=$OrderId");
        die;
    } else {
        $CartData = json_decode($Order['Ord_Data'], true);
        $Order = array_merge($Order, $CartData);
        $Request = "https://yoomoney.ru/eshop.xml";
        $Request .= "?shopId=71378";
        $Request .= "&scid=72593";
        $Request .= "&sum=".$Order['TotalSum'];
        $Request .= "&customerNumber=".$Order['User']['us_id'];
        $Request .= "&orderNumber=".$Order['Ord_Code'];
        $Request .= "&shopSuccessURL=".DOMAIN_URL."/merchant/yandex.php?".classTripCart::$DoID."=PaymentSuccess&OrderId=".$Order['Ord_Code'];
        $Request .= "&shopFailURL=".DOMAIN_URL."/merchant/yandex.php?".classTripCart::$DoID."=PaymentFail&OrderId=".$Order['Ord_Code'];
        $Request .= "&cps_email=".$Order['User']['mail'];
        $Request .= "&cps_phone=".$Order['User']['phone'];
        header("Location: $Request");
        die;
    }

<?php

    $classTripCart = _autoload("classTripCart");

    $OrderId = (int)$_REQUEST['OrderId'];
    $Order = DB::selectArray("SELECT * FROM cartData WHERE Ord_Code = $OrderId");

    if(!$Order['Ord_Code']) {
        header("HTTP/1.1 404 Not Found");
        header("Status: 404 Not Found");
        include($_SERVER['DOCUMENT_ROOT']."/error.php");
        die;
    }

    header("Location: /cart/?tcdo=OrderInfo&OrderId=$OrderId");

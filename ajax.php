<?php

    function Convert(&$Arr) {
        foreach($Arr as $k => $v) {
            if(is_array($v)) {
                return $Arr;
            } else {
                //            $Arr[$k] = iconv("UTF-8", "utf-8", $v);
                $Arr[$k] = String_RusCharsDeCode($v);
                $GLOBALS[$k] = $Arr[$k];
            }
        }
        return $Arr;
    }

    function String_RusCharsDeCode($string) {
        $russian_codes = array('%u0410','%u0411','%u0412','%u0413','%u0414','%u0415','%u0401','%u0416','%u0417','%u0418','%u0419','%u041A','%u041B','%u041C','%u041D','%u041E','%u041F','%u0420','%u0421','%u0422','%u0423','%u0424','%u0425','%u0426','%u0427','%u0428','%u0429','%u042A','%u042B','%u042C','%u042D','%u042E','%u042F','%u0430','%u0431','%u0432','%u0433','%u0434','%u0435','%u0451','%u0436','%u0437','%u0438','%u0439','%u043A','%u043B','%u043C','%u043D','%u043E','%u043F','%u0440','%u0441','%u0442','%u0443','%u0444','%u0445','%u0446','%u0447','%u0448','%u0449','%u044A','%u044B','%u044C','%u044D','%u044E','%u044F');
        $russian_chars = array("А",     "Б",     "В",     "Г",     "Д",     "Е",     "Ё",     "Ж",     "З",     "И",     "Й",     "К",     "Л",     "М",     "Н",     "О",     "П",     "Р",     "С",     "Т",     "У",     "Ф",     "Х",     "Ц",     "Ч",     "Ш",     "Щ",     "Ъ",     "Ы",     "Ь",     "Э",     "Ю",     "Я",     "а",     "б",     "в",     "г",     "д",     "е",     "ё",     "ж",     "з",     "и",     "й",     "к",     "л",     "м",     "н",     "о",     "п",     "р",     "с",     "т",     "у",     "ф",     "х",     "ц",     "ч",     "ш",     "щ",     "ъ",     "ы",     "ь",     "э",     "ю",     "я");
        return str_replace($russian_codes,$russian_chars,$string);
    }

    function String_RusCharsEnCode($string) {
        $russian_codes = array('%u0410','%u0411','%u0412','%u0413','%u0414','%u0415','%u0401','%u0416','%u0417','%u0418','%u0419','%u041A','%u041B','%u041C','%u041D','%u041E','%u041F','%u0420','%u0421','%u0422','%u0423','%u0424','%u0425','%u0426','%u0427','%u0428','%u0429','%u042A','%u042B','%u042C','%u042D','%u042E','%u042F','%u0430','%u0431','%u0432','%u0433','%u0434','%u0435','%u0451','%u0436','%u0437','%u0438','%u0439','%u043A','%u043B','%u043C','%u043D','%u043E','%u043F','%u0440','%u0441','%u0442','%u0443','%u0444','%u0445','%u0446','%u0447','%u0448','%u0449','%u044A','%u044B','%u044C','%u044D','%u044E','%u044F');
        $russian_chars = array("А",     "Б",     "В",     "Г",     "Д",     "Е",     "Ё",     "Ж",     "З",     "И",     "Й",     "К",     "Л",     "М",     "Н",     "О",     "П",     "Р",     "С",     "Т",     "У",     "Ф",     "Х",     "Ц",     "Ч",     "Ш",     "Щ",     "Ъ",     "Ы",     "Ь",     "Э",     "Ю",     "Я",     "а",     "б",     "в",     "г",     "д",     "е",     "ё",     "ж",     "з",     "и",     "й",     "к",     "л",     "м",     "н",     "о",     "п",     "р",     "с",     "т",     "у",     "ф",     "х",     "ц",     "ч",     "ш",     "щ",     "ъ",     "ы",     "ь",     "э",     "ю",     "я");
        return str_replace($russian_chars,$russian_codes,$string);
    }



    if(!IsRobots()) {
        session_start();

        include_once(COMMON_PATH."/JsHttpRequestLib/JsHttpRequest.php");

        $_REQUEST = Convert($_REQUEST);
        $_GET = Convert($_GET);
        $_POST = Convert($_POST);

        // Создаем главный объект библиотеки
        // Указываем кодировку страницы
        $JsHttpRequest = new JsHttpRequest("utf-8");

        // Формируем результат
        $_RESULT = NULL;

        $sec = $_REQUEST['s'] ? (int)$_REQUEST['s'] : null;
        if($sec)
            $PageObj = new classPage($sec);

        DB::getInstance();
        if($obj = _autoload($_REQUEST['c'], $_REQUEST['m'], $PageObj)) {
            $obj->isJsHttpRequest = true;
            if($_REQUEST['a'] && method_exists($obj, $_REQUEST['a'])) {
                $GetFunction = $_REQUEST['a'];
                eval("\$obj->$GetFunction();");
                $_RESULT = $obj->AjaxData;
            } elseif (method_exists($obj, 'JsHttpRequestAction')) {
                $_RESULT = $obj->JsHttpRequestAction();
            } else {
                $obj->Action(preg_replace("/\W+/", "", $_REQUEST['a']));
                $_RESULT = $obj->AjaxData;
            }
        } else {
            /*
             * Bad class or module
             */
            $_RESULT['debug'] = "class ".$_REQUEST['c']." doesn't exists";
        }
    } else
        die;

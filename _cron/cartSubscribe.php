<?php

    $Mail  = _autoload("classMail");

    $Subscribes = DB::selectArray("SELECT * FROM cartData WHERE Ord_Status IN (1) AND ((Ord_Change < NOW() - INTERVAL 1 DAY AND Ord_Change > NOW() - INTERVAL 2 DAY) OR (Ord_Change < NOW() - INTERVAL 3 DAY AND Ord_Change > NOW() - INTERVAL 4 DAY))", "Ord_Code");

    foreach($Subscribes as $Key => $Order) {
        $SendMail = false;
        $Data = json_decode($Order['Ord_Data'], true);
        if($Data['User']['mail']) {
            $SendMail = true;
            $External['Message']  = "Здравствуйте".($Data['User']['name'] ? " ".$Data['User']['name'] : "").", Вы оформили заказ #".$Order['Ord_Code']." на сайте ".DOMAIN_NAME." для оплаты перейдите по ссылке <a href=\"".DOMAIN_URL."/cart/?tcdo=Payment&OrderId=".$Order['Ord_Code']."\">оплатить</a>";
            $External['Message'] .= "<br><br>Статус вашего заказа и все подробности вы можете посмотреть на сайте в <a href=\"".DOMAIN_URL."/user/\">личном кабинете</a>";
            $External['mail'] = $Data['User']['mail'];
        }

        if($SendMail) {
            $Template = $Mail->GetTemplate("mailTemplate");
            $MessageHtml = $Mail->MailTemplate2Php($Template, $External);
            $Content = $Mail->Template2Php($MessageHtml);

            $Mail->SendMail($Data['User']['mail'], "Оформление заказа на ".DOMAIN_NAME, $Content);
        }
    }


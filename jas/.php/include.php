<?php

    define("CJAS_CATALOG", "/jas");
    define("CJAS_ROOT_PATH", CLIENT_PATH.CJAS_CATALOG);
    define("CJAS_FILES_URL", DOMAIN_URL.CJAS_CATALOG);

    function header403() { header ("HTTP/1.0 403 Forbidden"); die(); }
    function header404() { header ("HTTP/1.0 404 Not Found"); die(); }
    function header503() { header ("HTTP/1.0 503 Service Unavailable"); die(); }

    function JAS_BeginOb() {
        ob_start();
    }

    function JAS_FlushOb() {
        $Content = ob_get_contents();
        ob_end_clean();

        $Content = str_replace("\n", "", $Content);
        $Content = str_replace("\r", "", $Content);
        $Content = str_replace("\"", "\\\"", $Content);
        echo $Content;
    }

    function CJAS_RusU($str) {
        $Letters = array(
            "\\u2500", "\\u2502", "\\u250C", "\\u2510", "\\u2514", "\\u2518", "\\u251C", "\\u2524",
            "\\u252C", "\\u2534", "\\u253C", "\\u2580", "\\u2584", "\\u2588", "\\u258C", "\\u2590",
            "\\u2591", "\\u2592", "\\u2593", "\\u2320", "\\u25A0", "\\u2219", "\\u221A", "\\u2248",
            "\\u2264", "\\u2265", "\\u00A0", "\\u2321", "\\u00B0", "\\u00B2", "\\u00B7", "\\u00F7",
            "\\u2550", "\\u2551", "\\u2552", "\\u0451", "\\u2553", "\\u2554", "\\u2555", "\\u2556",
            "\\u2557", "\\u2558", "\\u2559", "\\u255A", "\\u255B", "\\u255C", "\\u255d", "\\u255E",
            "\\u255F", "\\u2560", "\\u2561", "\\u0401", "\\u2562", "\\u2563", "\\u2564", "\\u2565",
            "\\u2566", "\\u2567", "\\u2568", "\\u2569", "\\u256A", "\\u256B", "\\u256C", "\\u00A9",
            "\\u044E", "\\u0430", "\\u0431", "\\u0446", "\\u0434", "\\u0435", "\\u0444", "\\u0433",
            "\\u0445", "\\u0438", "\\u0439", "\\u043A", "\\u043B", "\\u043C", "\\u043d", "\\u043E",
            "\\u043F", "\\u044F", "\\u0440", "\\u0441", "\\u0442", "\\u0443", "\\u0436", "\\u0432",
            "\\u044C", "\\u044B", "\\u0437", "\\u0448", "\\u044d", "\\u0449", "\\u0447", "\\u044A",
            "\\u042E", "\\u0410", "\\u0411", "\\u0426", "\\u0414", "\\u0415", "\\u0424", "\\u0413",
            "\\u0425", "\\u0418", "\\u0419", "\\u041A", "\\u041B", "\\u041C", "\\u041d", "\\u041E",
            "\\u041F", "\\u042F", "\\u0420", "\\u0421", "\\u0422", "\\u0423", "\\u0416", "\\u0412",
            "\\u042C", "\\u042B", "\\u0417", "\\u0428", "\\u042d", "\\u0429", "\\u0427", "\\u042A"
        );

        for ($i=128; $i<=255; $i++) {
            $str = str_replace(chr($i), $Letters[$i-128], $str);
        }
        return $str;
    };

    function CJAS_Require(&$base, $element, &$array) {
        $element_path = $base."/".$element;
        if (!$array[$element_path]) {
            $array[$element_path] = $element_path;

            if(debug())
                $debug_path = $element_path.".debug";

            $path = file_exists($debug_path) ? $debug_path :
                ( file_exists($element_path.".requires") ? $element_path.".requires" :
                    ( $base."/".substr($element, 0, strpos($element, "."))."/index".substr($element, strpos($element, ".")).".requires" )
                );

            if(file_exists($path)) {
                $pathinfo = pathinfo(realpath($path));
                CJAS_RequireAll($pathinfo["dirname"], file($path), $array);
                unset($array[$element_path]);
                $array[$element_path] = $element_path;
            }
        }
    }

    function CJAS_RequireAll(&$base, $files, &$array) {
        if (is_array($files)) {
            reset($files);
            while (list(,$element) = each($files)) {
                if ($element = trim($element))
                    CJAS_Require($base, $element, $array);
            }
        }
    }

    $contents = "";

    $CJAS_CSSDIR = CJAS_ROOT_PATH."/.css";
    $CJAS_JSDIR  = CJAS_ROOT_PATH."/.js";
    $Uri = substr($_SERVER['REQUEST_URI'], strlen(CJAS_CATALOG) + 1);

    $Ex = explode("?", $Uri);
    $Uri = $Ex[0];

    if (strpos($Uri, "..") !== false || strpos($Uri, "/") !== false || strpos($Uri, "\\") !== false)
        header403();

    $Ext = explode(".", $Uri);
    $Ext = ".".array_pop($Ext);

    $Files = explode(";", $Uri);
    for ($i=0; $i<sizeof($Files); $i++) {
        if (substr($Files[$i], 0, 1) == "/" || substr($Files[$i], 0, 1) == ".")
            header404();

        if (substr($Files[$i], -strlen($Ext), strlen($Ext)) !== $Ext)
            header403();

        $Files[$i] = str_replace(",", "/", $Files[$i]);
    }

    if($Ext === ".js") {
        $type = "javascript";
        $base = realpath($CJAS_JSDIR);
    } elseif($Ext === ".css") {
        $type = "css";
        $base = realpath($CJAS_CSSDIR);
    } else
        header503();

    for($i = 0; $i < sizeof($Files); $i++) {
        if(file_exists($base."/".$Files[$i].".requires")) {
            continue;
        } elseif(file_exists($base."/".substr($Files[$i], 0, strpos($Files[$i], "."))."/index$Ext.requires")) {
            continue;
        } elseif(file_exists($base."/".substr($Files[$i], 0, strpos($Files[$i], "."))."/index$Ext")) {
            continue;
        } elseif (!file_exists($base."/".$Files[$i])) {
            if($type != "css")
                header404();
        }
    }

    $combine_files = array();
    $all_elements = array();
    CJAS_RequireAll($base, $Files, $all_elements);

    reset($all_elements);
    while (list(, $element) = each($all_elements)) {
        if (!file_exists($element))
            continue;

        ob_start();
        include($element);
        array_push($combine_files, ob_get_contents());
        ob_end_clean();
    }

    $CJAS_WRITE = !$CJAS_DEBUG;

    reset($combine_files);
    while (list(, $element) = each($combine_files)) {
        $file_array = explode("\n", $element);
        reset($file_array);
        while (list($Key, $Value) = each($file_array)) {
            if(!trim($Value)) {
                if ($CJAS_WRITE && !$CJAS_PACK)
                    unset($file_array[$Key]);
            }
            else
                $file_array[$Key] = ($CJAS_WRITE ? trim($Value) : $Value);
        }
        $contents .= implode("\n", $file_array)."\n";
    }

//    if ($type == "javascript")
//        $contents = CJAS_RusU($contents);

    if ($CJAS_WRITE) {
        if ($type == "javascript" && $CJAS_PACK) {
            include("class.JavaScriptPacker.php");
            $packer = new JavaScriptPacker($contents, $CJAS_PACK, true, false);
            $contents = $packer->pack();
        }

        $Filename = CJAS_ROOT_PATH."/".$Uri;
        $fp = fopen($Filename, "w");
        fputs($fp, $contents);
        fclose($fp);
        chmod($Filename, 0777);
    }

    header("Cache-Control: private, must-revalidate, max-age=0");
    header("Content-Type: text/$type; charset=utf-8");
    header("Content-Length: ".strlen($contents));

    echo $contents;

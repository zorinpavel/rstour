JooS.Ajax = {

    Send: function(Method, Url, Query, CallbackFunction, target, MarkerView, Cache) {
        var req = new JsHttpRequest();

        if(Method == "GET" && !!Cache)
            req.caching = true;

        req.onreadystatechange = function() {
            if (req.readyState == 4) {
                CallbackFunction.call(req);
            } else {
                var TargetElement = document.getElementById(target) || target;
                if(TargetElement) {
                    var inHTML = "";
                    switch (req.readyState) {
                        case 0:
                            inHTML = "не инициализирован";
                            break;
                        case 1:
                            inHTML =  "загрузка...";
                            break;
                        case 2:
                            inHTML = "загружено";
                            break;
                        case 3:
                            inHTML = "в процессе...";
                            break;
                        case 4:
                            inHTML = "готово";
                            break;
                        default:
                            inHTML = "неизвестное состояние";
                    }
                    MarkerView = MarkerView || "loader";
                    if(MarkerView != "none" && MarkerView != "opacity")
                        TargetElement.innerHTML = "<img src=\"/img/" + MarkerView + ".gif\" title=\"" + inHTML + "\" align=\"center\" />";
                }
            }
        };
        req.open(Method, Url, true);
        req.send(Query);
    },

    Get: function(Url, Query, CallbackFunction, target, MarkerView, Cache) {
        this.Send("GET", Url, Query, CallbackFunction, target, MarkerView, Cache);
    },

    Post: function(Url, Query, CallbackFunction, target, MarkerView) {
        this.Send("POST", Url, Query, CallbackFunction, target, MarkerView);
    }

};

JooS.JSLoader = JooS.Reflect(JooS.Element, {
    getElementProperties: function() {
        return {
            src: this.source,
            type: "text/javascript",
            charset: "utf-8"
        };
    },

    attachCallback: function(callback) {
        var Element = this.getElement();
        if (JooS.Browser.IE) {
            Element.onreadystatechange= function() {
                var rs = this.readyState;
                if ("loaded" === rs || "complete" === rs) {
                    callback();
                    this.onreadstatechange = null;
                }
            };
        }
        else {
            Element.onload = function() {
                Element.onload = null;
                callback();
            };
        }
    },

    __constructor: function(source, callback) {
        this.source = source;
        this.__constructor.__parent("script");

        JooS.BodyDOM.appendChild(this);
        if (callback)
            this.attachCallback(callback);
    },
    __destructor: function() {
        this.removeNode();
        this.__destructor.__parent();
    }
});


JooS.Extend(JooS.JSLoader, {
    Scripts: [],

    Start: function(source, id) {
        id = id || Math.random();
        this.Scripts[id] = new this(source);
        return id;
    },
    Cleanup: function(id) {
        if (this.Scripts[id]) {
            this.Scripts[id].removeNode();
            this.Scripts[id].__destroy();
            delete this.Scripts[id];
        }
    }
});


JooS.CSSLoader = JooS.Reflect(JooS.Element, {
    getElementProperties: function() {
        return {
            rel: "stylesheet",
            type: "text/css",
            href: this.href
        };
    },

    __constructor: function(href) {
        this.href = href;
        this.__constructor.__parent("link");
        JooS.HeadDOM.prependChild(this);
    }
});


JooS.JSEval = JooS.Reflect(JooS.Element, {
    useTextNode: function() {
        return JooS.Browser.Safari || JooS.Browser.Opera;
    },
    getElementProperties: function() {
        var Prop = {
            type: "text/javascript",
            language: "JavaScript"
        };
        if (!this.useTextNode())
            Prop.text = this.EvalCode;
        return Prop;
    },

    __constructor: function(EvalCode) {
        this.EvalCode = EvalCode;
        this.__constructor.__parent("script");
        if (this.useTextNode())
            this.appendChild(new JooS.TextElement(EvalCode));
        JooS.BodyDOM.appendChild(this);
        this.removeNode();
    }
});


JooS.CSSInline = JooS.Reflect(JooS.Element, {
    getElementProperties: function() {
        return {
            type: "text/css"
        };
    },
    __constructor: function(cssText) {
        this.__constructor.__parent("style");

        var Self = this;
        JooS.Startup(function() {
            if (Self.getElement().styleSheet)
                Self.getElement().styleSheet.cssText = cssText;
            else
                Self.appendChild(new JooS.TextElement(cssText));
            JooS.HeadDOM.appendChild(Self);
        });
    }
});

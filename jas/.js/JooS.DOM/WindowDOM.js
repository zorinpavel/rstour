JooS.WindowDOM = new JooS.Element(window);

JooS.Extend(JooS.WindowDOM, {

    getScrollSize: function() {
        var width = 0, height = 0;
        if ( typeof( window.innerWidth ) == 'number' ) {
            width = window.innerWidth;
            height = window.innerHeight;
        }
        else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
            width = document.documentElement.clientWidth;
            height = document.documentElement.clientHeight;
        }
        else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
            width = document.body.clientWidth;
            height = document.body.clientHeight;
        }
        else {
            width = screen.width;
            height = screen.height;
        }
        return {
            width: parseInt(width),
            height: parseInt(height)
        }
    },

    getScrollPos: function() {
        var scrollx, scrolly;
        if (JooS.Browser.IE || JooS.Browser.Opera >= 9.5) {
            scrollx = document.documentElement.scrollLeft + document.body.scrollLeft;
            scrolly = document.documentElement.scrollTop + document.body.scrollTop;
        }
        else if (JooS.Browser.Opera) {
            scrollx = document.body.scrollLeft;
            scrolly = document.body.scrollTop;
        }
        else {
            scrollx = window.scrollX;
            scrolly = window.scrollY;
        }
        return {
            left: scrollx,
            top: scrolly
        };
    },

    ReloadWindow: function(Time, Href) {
        var Location = Href;
        var wSearch = ( window.location.search.match(/\&reload/) ? window.location.search.replace(/\&reload/, "&reload1") : (window.location.search + "&reload") );
        if(Href) {
            if(Href.replace(/\#\w+/, "") == (window.location.pathname + window.location.search))
                var Location = Location.replace(/\#\w+/, "") + "&reload" + Href.replace(/.+\#?/, "#");
            if(Href.replace(/\#\w+/, "") == "")
                var Location = Location.replace(/\#\w+/, "") + window.location.pathname + wSearch + Href;
        }
        if(Time) {
            setTimeout(function() {
                if(Location)
                    window.location.assign(Location);
                else
                    window.location.reload();
            }, Time * 1000);
        } else {
            if(Location)
                window.location.assign(Location);
            else
                window.location.reload();
        }
    }

});

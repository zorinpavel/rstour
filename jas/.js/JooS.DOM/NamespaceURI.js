JooS.NamespaceURI = (function() {
  var NamespaceURIs = { }, defaultName = "@";

  return {
    register: function(prefix, URI) {
      NamespaceURIs[prefix || defaultName] = URI;
    }, 
    get: function(prefix) {
      return NamespaceURIs[prefix || defaultName] || false;
    }
  };
})();

JooS.NamespaceURI.register("", "http://www.w3.org/1999/xhtml");

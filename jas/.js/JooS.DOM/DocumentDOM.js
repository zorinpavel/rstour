JooS.DocumentDOM = new JooS.Element(document);

JooS.Extend(JooS.DocumentDOM, {

    getScroll: function() {
        var Scroll = {
            scrollX: 0,
            scrollY: 0
        };
        if(document.compatMode == "CSS1Compat" && !JooS.Browser.Safari) {
            Scroll.scrollY = document.documentElement.scrollTop;
            Scroll.scrollX = document.documentElement.scrollLeft;
        } else {
            Scroll.scrollY = document.body.scrollTop;
            Scroll.scrollX = document.body.scrollLeft;
        }
        if(JooS.Browser.iPad) {
            Scroll.scrollX = window.pageXOffset;
            Scroll.scrollY = window.pageYOffset;
        }
        return Scroll;
    },

    getDocumentSize: function() {
        var Height, Width;
        Height = Math.max(document.compatMode != 'CSS1Compat' ? document.body.scrollHeight : document.documentElement.scrollHeight, this.getViewportHeight());
        Width = Math.max(document.compatMode != 'CSS1Compat' ? document.body.scrollWidth : document.documentElement.scrollWidth, this.getViewportWidth());
        return {
            height: Height,
            width: Width
        };
    },

    getViewportHeight: function() {
        return ((document.compatMode || JooS.Browser.IE) && !JooS.Browser.Opera) ? (document.compatMode == 'CSS1Compat') ? document.documentElement.clientHeight : document.body.clientHeight : (document.parentWindow || document.defaultView).innerHeight;
    },

    getViewportWidth: function() {
        return ((document.compatMode || JooS.Browser.IE) && !JooS.Browser.Opera) ? (document.compatMode == 'CSS1Compat') ? document.documentElement.clientWidth : document.body.clientWidth : (document.parentWindow || document.defaultView).innerWidth;
    }

});

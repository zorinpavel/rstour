JooS.XPathSelector = (function() {
    var cacheXPath = {}, cssToXPath = function(rule) {
        var regElement = /^([#.]?)([a-z0-9\\*_-]*)((\|)([a-z0-9\\*_-]*))?/i;
        var regAttr1 = /^\[([^\]]*)\]/i;
        var regAttr2 = /^\[\s*([^~=\s]+)\s*(~?=)\s*"([^"]+)"\s*\]/i;
        var regPseudo = /^:([a-z_-])+/i;
        var regCombinator = /^(\s*[>+\s])?/i;
        var regComma = /^\s*,/i;

        var index = 1;
        var parts = [ "descendant::", "*"];
        var lastRule = null;

        while (rule.length && rule != lastRule) {
            lastRule = rule;

            rule = rule.replace(/^\s*|\s*$/g,"");
            if (!rule.length)
                break;

            var m = regElement.exec(rule);
            if (m) {
                if (!m[1]) {
                    if (m[5])
                        parts[index] = m[5];
                    else
                        parts[index] = m[2];
                }
                else if (m[1] == '#')
                    parts.push("[@id='" + m[2] + "']");
                else if (m[1] == '.')
                    parts.push("[contains(concat(' ',normalize-space(@class),' '), ' " + m[2] + " ')]");

                rule = rule.substr(m[0].length);
            }

            m = regAttr2.exec(rule);
            if (m) {
                if (m[2] == "~=")
                    parts.push("[contains(@" + m[1] + ", '" + m[3] + "')]");
                else
                    parts.push("[@" + m[1] + "='" + m[3] + "']");

                rule = rule.substr(m[0].length);
            }
            else {
                m = regAttr1.exec(rule);
                if (m) {
                    parts.push("[@" + m[1] + "]");
                    rule = rule.substr(m[0].length);
                }
            }

            m = regPseudo.exec(rule);
            while (m) {
                rule = rule.substr(m[0].length);
                m = regPseudo.exec(rule);
            }

            m = regCombinator.exec(rule);
            if (m && m[0].length) {
                if (m[0].indexOf(">") != -1)
                    parts.push("/");
                else if (m[0].indexOf("+") != -1)
                    parts.push("/following-sibling::");
                else
                    parts.push("/descendant::");

                index = parts.length;
                parts.push("*");
                rule = rule.substr(m[0].length);
            }

            m = regComma.exec(rule);
            if (m) {
                parts.push(" | ", "descendant::", "*");
                index = parts.length-1;
                rule = rule.substr(m[0].length);
            }
        };

        return parts.join("");
    };

    return function(rule, elem) {
        var xpath = cacheXPath[rule] || (cacheXPath[rule] = cssToXPath(rule));

        var result = document.evaluate(xpath, elem, null, XPathResult.ORDERED_NODE_ITERATOR_TYPE, null);
        var nodes = [];

        for (var item = result.iterateNext(); item; item = result.iterateNext())
            nodes.push(item);

        return nodes;
    };
})();

JooS.jQuerySelector = function() {
    var styleFloat = /*@cc_on!@*/false ? "styleFloat" : "cssFloat";
    var chars =
        (JooS.Browser.Safari && JooS.Browser.Safari < 2) ||
        (JooS.Browser.Opera && JooS.Browser.Opera < 8)
            ? "(?:[\\w*_-]|\\\\.)" : "(?:[\\w\u0128-\uFFFF*_-]|\\\\.)";

    var quickChild = new RegExp("^>\\s*(" + chars + "+)");
    var quickID = new RegExp("^(" + chars + "+)(#)(" + chars + "+)");
    var quickClass = new RegExp("^([#.]?)(" + chars + "*)");

    return {
        props: {
            "for": "htmlFor",
            "class": "className",
            "float": styleFloat,
            cssFloat: styleFloat,
            styleFloat: styleFloat,
            innerHTML: "innerHTML",
            className: "className",
            value: "value",
            disabled: "disabled",
            checked: "checked",
            readonly: "readOnly",
            selected: "selected",
            maxlength: "maxLength",
            selectedIndex: "selectedIndex",
            defaultValue: "defaultValue",
            tagName: "tagName",
            nodeName: "nodeName"
        },
        trim: function( text ) {
            return (text || "").replace( /^\s+|\s+$/g, "" );
        },
        merge: function( first, second ) {
            if ( JooS.Browser.IE ) {
                for ( var i = 0; second[ i ]; i++ )
                    if ( second[ i ].nodeType != 8 )
                        first.push( second[ i ] );

            }
            else {
                for ( var i = 0; second[ i ]; i++ )
                    first.push( second[ i ] );
            }
            return first;
        },
        isXMLDoc: function( elem ) {
            return elem.documentElement && !elem.body ||
                elem.tagName && elem.ownerDocument && !elem.ownerDocument.body;
        },
        nodeName: function( elem, name ) {
            return elem.nodeName && elem.nodeName.toUpperCase() == name.toUpperCase();
        },
        parse: [
            /^(\[) *@?([\w-]+) *([!*$^~=]*) *('?"?)(.*?)\4 *\]/,
            new RegExp("^([:.#]*)(" + chars + "+)")
        ],

        find: function( t, context, namespaceURIs ) {
            namespaceURIs = namespaceURIs || { };

            if ( typeof t != "string" )
                return [ t ];

            if ( context && context.nodeType != 1 && context.nodeType != 9)
                return [ ];

            context = context || document;

            var ret = [context], done = [], last, nodeName;

            while ( t && last != t ) {
                var r = [];
                last = t;

                t = this.trim(t);

                var foundToken = false;
                var re = quickChild;
                var m = re.exec(t);

                if ( m ) {
                    nodeName = m[1].toUpperCase();

                    for ( var i = 0; ret[i]; i++ )
                        for ( var c = ret[i].firstChild; c; c = c.nextSibling )
                            if ( c.nodeType == 1 && (nodeName == "*" || c.nodeName.toUpperCase() == nodeName) )
                                r.push( c );

                    ret = r;
                    t = t.replace( re, "" );
                    if ( t.indexOf(" ") == 0 ) continue;
                    foundToken = true;
                }

                if ( t && !foundToken ) {
                    if ( !t.indexOf(",") ) {
                        if ( context == ret[0] ) ret.shift();

                        done = this.merge( done, ret );

                        r = ret = [context];

                        t = " " + t.substr(1,t.length);

                    } else {
                        var re2 = quickID;
                        var m = re2.exec(t);

                        if ( m ) {
                            m = [ 0, m[2], m[3], m[1] ];
                        } else {
                            re2 = quickClass;
                            m = re2.exec(t);
                        }

                        m[2] = m[2].replace(/\\/g, "");

                        var elem = ret[ret.length-1];

                        if ( m[1] == "#" && elem && elem.getElementById && !this.isXMLDoc(elem) ) {
                            var oid = elem.getElementById(m[2]);

                            if ( (JooS.Browser.IE || JooS.Browser.Opera) && oid && typeof oid.id == "string" && oid.id != m[2] )
                                oid = jQuery('[@id="'+m[2]+'"]', elem)[0];

                            ret = r = oid && (!m[3] || this.nodeName(oid, m[3])) ? [oid] : [];
                        } else {
                            for ( var i = 0; ret[i]; i++ ) {
                                var tag = m[1] == "#" && m[3] ? m[3] : m[1] != "" || m[0] == "" ? "*" : m[2];

                                if ( tag == "*" && ret[i].nodeName.toLowerCase() == "object" )
                                    tag = "param";

                                r = this.merge(
                                    r,
                                    namespaceURIs[tag] ? ret[i].getElementsByTagNameNS( namespaceURIs[tag], tag ) : ret[i].getElementsByTagName( tag )
                                );
                            }

                            if ( m[1] == "." )
                                r = this.classFilter( r, m[2] );

                            if ( m[1] == "#" ) {
                                var tmp = [];

                                for ( var i = 0; r[i]; i++ )
                                    if ( r[i].getAttribute("id") == m[2] ) {
                                        tmp = [ r[i] ];
                                        break;
                                    }

                                r = tmp;
                            }

                            ret = r;
                        }

                        t = t.replace( re2, "" );
                    }

                }

                if ( t ) {
                    var val = this.filter(t,r);
                    ret = r = val.r;
                    t = this.trim(val.t);
                }
            }

            if ( t )
                ret = [];

            if ( ret && context == ret[0] )
                ret.shift();

            done = this.merge( done, ret );

            return done;
        },

        classFilter: function(r,m,not){
            m = " " + m + " ";
            var tmp = [];
            for ( var i = 0; r[i]; i++ ) {
                var pass = (" " + r[i].className + " ").indexOf( m ) >= 0;
                if ( !not && pass || not && !pass )
                    tmp.push( r[i] );
            }
            return tmp;
        },

        filter: function(t,r,not) {
            var last;

            while ( t && t != last ) {
                last = t;

                var p = this.parse, m;

                for ( var i = 0; p[i]; i++ ) {
                    m = p[i].exec( t );

                    if ( m ) {
                        t = t.substring( m[0].length );

                        m[2] = m[2].replace(/\\/g, "");
                        break;
                    }
                }

                if ( !m )
                    break;

                if ( m[1] == "." )
                    r = this.classFilter(r, m[2], not);

                else if ( m[1] == "[" ) {
                    var tmp = [], type = m[3];

                    for ( var i = 0, rl = r.length; i < rl; i++ ) {
                        var a = r[i], an = this.props[m[2]];
                        var z = (an ? a[an] : a.getAttribute(m[2])) || '';

                        if ( (type == "" && !!z ||
                            type == "=" && z == m[5] ||
                            type == "!=" && z != m[5] ||
                            type == "^=" && z && !z.indexOf(m[5]) ||
                            type == "$=" && z.substr(z.length - m[5].length) == m[5] ||
                            (type == "*=" || type == "~=") && z.indexOf(m[5]) >= 0) ^ not )
                            tmp.push( a );
                    }

                    r = tmp;

                }
            }
            return { r: r, t: t };
        }
    };
}();

JooS.cssQuery = function(rule, elem, namespaceURIs) {
    elem = elem || document;
    if (elem.querySelectorAll)
        return elem.querySelectorAll(rule);
    else if (document.evaluate)
        return this.XPathSelector(rule, elem);
    else
        return this.jQuerySelector.find(rule, elem, namespaceURIs);
};

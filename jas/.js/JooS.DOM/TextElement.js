JooS.TextElement = JooS.Reflect(JooS.Class, {
  getElement: function(nodeValue) {
    if (!this.__htmlInit) {
      if (!this.htmlElement)
        this.htmlElement = document.createTextNode(nodeValue || this.nodeValue);
      this.__htmlInit = true;
    }
    return this.htmlElement;
  }, 
  getNodeValue: function() {
    return this.getElement().nodeValue;
  }, 
  setNodeValue: function(nodeValue) {
    this.getElement().nodeValue = nodeValue;
  }, 
  removeNode: JooS.Element.prototype.removeNode, 

  __constructor: function(nodeValue) {
    if (!nodeValue || typeof nodeValue == "string")
      this.nodeValue = nodeValue || "";
    else
      this.htmlElement = nodeValue;
  }
});

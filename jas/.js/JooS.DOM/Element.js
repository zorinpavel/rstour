JooS.Element = JooS.Reflect(JooS.Class, {
    prefix: "",

    getElementProperties: function() {
        return false;
    },
    getElementStyles: function() {
        return false;
    },
    getElement: function(TagName) {
        if (!this.htmlElement) {
            TagName = (this.prefix ? this.prefix + ":" : "") + (TagName || this.TagName || "div");
            this.htmlElement = JooS.Browser.IE ? document.createElement(TagName) : document.createElementNS(JooS.NamespaceURI.get(this.prefix), TagName);

            if (window.$)
                this.$ = $(this.htmlElement);
        }

        JooS.Extend(this.htmlElement, this.getElementProperties());
        JooS.Extend(this.htmlElement.style, this.getElementStyles());
        this.getElement = this.__getElement;

        return this.htmlElement;
    },
    __getElement: function() {
        return this.htmlElement;
    },

    create__eventEntry: function() {
        var joosObj = this;
        this.__eventEntry = function(event) {
            var evt = new JooS.DOMEvent(event);
            evt.launch(joosObj);
        }
    },

    attachEvent: function(Name, Func) {
        if (!this.__eventEntry)
            this.create__eventEntry();

        if (Name = JooS.DOMEvent.getEventName(Name)) {
            if (!this.eventActions[Name])
                if (window.attachEvent)
                    this.getElement().attachEvent("on" + Name, this.__eventEntry);
                else
                    this.getElement().addEventListener(Name, this.__eventEntry, false);

            this.eventActions[Name] = this.eventActions[Name] ? this.eventActions[Name] + 1 : 1;
            this[JooS.DOMEvent.getEventEntryPrefix(Name) + this.eventActions[Name]] = Func;
        }
    },

    detachEvent: function(Name) {
        if (Name = JooS.DOMEvent.getEventName(Name)) {
            if (this.eventActions[Name]) {
                if (window.attachEvent)
                    this.getElement().detachEvent("on" + Name, this.__eventEntry);
                else
                    this.getElement().removeEventListener(Name, this.__eventEntry, false);
                this.eventActions[Name] = null;
            }
        }
    },

    appendChild: function(Element) {
        this.getElement().appendChild(Element.getElement());
        return this;
    },

    prependChild: function(Element) {
        if(this.getElement().firstChild) {
            this.getElement().insertBefore(Element.getElement(), this.getElement().firstChild);
            return this;
        } else
            return this.appendChild(Element);
    },

    insertBefore: function(Element, beforeElement) {
        this.getElement().insertBefore(Element.getElement(), beforeElement.getElement());
    },

    removeChild: function(Element) {
        this.getElement().removeChild(Element.getElement());
        return this;
    },

    removeNode: function() {
        if(this.htmlElement) {
            var parentNode = this.htmlElement.parentNode;
            if(parentNode)
                parentNode.removeChild(this.htmlElement);
        }
    },

    clearBody: function() {
        if(this.htmlElement) {
            var node = this.htmlElement;
            while(node.hasChildNodes()) {
                node.removeChild(node.firstChild);
            }
        }
    },

    cssQuery: function(rule, namespaceURIs) {
        return JooS.cssQuery(rule, this.getElement(), namespaceURIs);
    },

    cssQueryStrict: function(rule, namespaceURIs) {
        var tmpClassName = "strict_rule" + parseInt(Math.random()*1000), Ret;
        this.addClassName(tmpClassName);
        Ret = JooS.cssQuery("." + tmpClassName + " > " + rule, this.getElement().parentNode, namespaceURIs);
        this.removeClassName(tmpClassName);
        return Ret;
    },

    cssQuerySelf: function(rule, namespaceURIs) {
        var tmpClassName = "strict_rule" + parseInt(Math.random()*1000), Ret;
        this.addClassName(tmpClassName);
        Ret = JooS.cssQuery("." + tmpClassName + " " + rule, this.getElement(), namespaceURIs);
        this.removeClassName(tmpClassName);
        return Ret;
    },

    hasAttribute: function(name) {
        return this.getElement().hasAttribute(name);
    },
    getAttribute: function(name) {
        return this.getElement().getAttribute(name);
    },
    setAttribute: function(name, value) {
        return this.getElement().setAttribute(name, value);
    },
    removeAttribute: function(name) {
        return this.getElement().removeAttribute(name);
    },
    getAttributes: function() {
        var attrs = this.getElement().attributes, i, attr, ret = {}, value, name;
        for (i=0; i<attrs.length; i++) {
            attr = attrs.item(i);
            if (JooS.Browser.IE && !attr.value)
                continue;
            ret[attr.name.toLowerCase()] = attr.value;
        }
        return ret;
    },

    addClassName: function (className, elem) {
        elem = elem || this.getElement();

        var currentClass = elem.className;
        if (!new RegExp(("(^|\\s)" + className + "(\\s|$)"), "i").test(currentClass)) {
            elem.className = currentClass + (currentClass ? " " : "") + className;
        }
        return this;
    },
    removeClassName: function (className, elem) {
        elem = elem || this.getElement();

        var classToRemove = new RegExp(("(^|\\s)" + className + "(\\s|$)"), "i");
        elem.className = (elem.className).replace(classToRemove, function (match) {
            var retVal = "";
            if (new RegExp("^\\s+.*\\s+$").test(match)) {
                retVal = match.replace(/(\s+).+/, "$1");
            }
            return retVal;
        }).replace(/^\s+|\s+$/g, "");
        return this;
    },
    replaceClassName: function (className, newClass, elem) {
        elem = elem || this.getElement();

        var classToRemove = new RegExp(("(^|\\s)" + className + "(\\s|$)"), "i");
        elem.className = (elem.className || "").replace(classToRemove, function (match, p1, p2) {
            var retVal = p1 + newClass + p2;
            if (new RegExp("^\\s+.*\\s+$").test(match)) {
                retVal = match.replace(/(\s+).+/, "$1");
            }
            return retVal;
        }).replace(/^\s+|\s+$/g, "");
        return this;
    },
    hasClassName: function (className) {
        return new RegExp(("(^|\\s)" + className + "(\\s|$)"), "i").test(this.getElement().className);
    },

    setStyle: function(name, value) {
        this.getElement().style[name] = value;
    },

    getStyle: function(prop, elem) {
        var currentStyle = [];
        elem = elem || this.getElement();

        if (document.defaultView && document.defaultView.getComputedStyle)
            currentStyle = document.defaultView.getComputedStyle(elem, null);
        else if (elem.currentStyle)
            currentStyle = elem.currentStyle;
        return currentStyle[prop] || "";
    },

    number_format: function(number, decimals, dec_point, thousands_sep) {
        number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
        var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function(n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                        .toFixed(prec);
            };
        // Fix for IE parseFloat(0.55).toFixed(0) = 0;
        s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
        }
        if ((s[1] || '')
                .length < prec) {
            s[1] = s[1] || '';
            s[1] += new Array(prec - s[1].length + 1)
                .join('0');
        }
        return s.join(dec);
    },

    __constructor: function(Tag, Properties, Styles) {
        this.eventActions = [];
        if (Properties)
            this.getElementProperties = JooS.Closure.Value(Properties);
        if (Styles)
            this.getElementStyles = JooS.Closure.Value(Styles);

        if (Tag && typeof Tag != "string") {
            this.htmlElement = Tag;
            this.getElement();
        }
        else {
            if (Tag == "input" && JooS.Browser.IE) {
                var type = (Properties && Properties.type) || "text";
                Tag = "<input type='" + type + "' />";
            }
            this.htmlElement = null;
            this.TagName = Tag;
        }
    },
    __destructor: function() {
        for (var i in this.eventActions)
            this.detachEvent(i);
        this.htmlElement = null;
    }
});

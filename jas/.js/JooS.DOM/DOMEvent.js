JooS.DOMEvent = JooS.Reflect(null, {
  launch: function(joosObj) {
    var pfx = this.getEventEntryPrefix(), returnValue = false, i, cnt = joosObj.eventActions[this.type];

    for (i=1; cnt && i<=cnt; i++)
      returnValue = returnValue || joosObj[pfx + i](this);

    if (!returnValue)
      this.preventDefault();
  }, 

  screenX: function() {
    return this.event.screenX;
  }, 
  screenY: function() {
    return this.event.screenY;
  }, 
  clientX: function() {
    return this.event.clientX;
  }, 
  clientY: function() {
    return this.event.clientY;
  }, 
  button: function() {
    return this.event.button;
  }, 
  wheelData: function() {
    var Delta = 0;
    if (this.event.wheelDelta)
      Delta = this.event.wheelDelta/120;
    else if (this.event.detail)
      Delta = -this.event.detail/3;

    return Delta;
  }, 
  ctrlKey: function() {
    return this.event.ctrlKey;
  }, 
  altKey: function() {
    return this.event.altKey;
  }, 
  shiftKey: function() {
    return this.event.shiftKey;
  }, 
  keyCode: function() {
    return this.event.keyCode ? this.event.keyCode : (this.event.which ? this.event.which : this.event.charCode);
  }, 
  getEventEntryPrefix: function(Name) {
    return "__eventEntry_" + (Name || this.type) + "_";
  }, 
  __constructor: function(event) {
    this.event = event || window.event;
    this.type = this.event.type;
  }, 
  __destroy: function() {
    this.event = null;
  }
});

if (window.attachEvent) {
  JooS.Virtual(JooS.DOMEvent, {
    preventDefault: function() {
      this.event.cancelBubble = true;
      this.event.returnValue = false;
    }, 
    offsetX: function() {
      return this.event.offsetX;
    }, 
    offsetY: function() {
      return this.event.offsetY;
    }
  });
}
else {
  JooS.Virtual(JooS.DOMEvent, {
    preventDefault: function() {
      this.event.preventDefault();
    }, 
    offsetX: function() {
      return this.event.layerX;
    }, 
    offsetY: function() {
      return this.event.layerY;
    }
  });
}

JooS.Extend(JooS.DOMEvent, {
  getEventName: function(Name) {
    switch (Name) {
      case "mousewheel":
        if (!JooS.Browser.Opera && !JooS.Browser.Safari && window.addEventListener)
          Name = "DOMMouseScroll";
        break;
      case "contextmenu":
        if (JooS.Browser.Opera)
          Name = false;
        break;
    };
    return Name;
  }, 
  getEventEntryPrefix: JooS.DOMEvent.prototype.getEventEntryPrefix
});

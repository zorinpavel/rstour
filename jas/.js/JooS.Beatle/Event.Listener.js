 /**
  *
  * extras.listener = [
  *   { event: имяСобытия, dispatcher: имяДиспатчера }, 
  *   ...
  *   { event: имяСобытия, dispatcher: имяДиспатчера }
  * ]
  *
  **/

  var EventListener = JooS.Reflect(JooS.Class.Plugin, {
    Event_listenerConstruct: function(events) {
      if (!this.Event_listenerNum)
        this.Event_listenerNum = [ ];

      if (events && events.constructor == Array) {
        for (var i=0; i<events.length; i++) {
          var id = events[i].dispatcher || "$", name = events[i].event, eventName = "Listener_" + name, Num, l_Num;

          if (id && this[eventName]) {
            Num = pageDispatchersHash[id];
            if (!pageListeners[Num])
              pageListeners[Num] = { };
            if (!pageListeners[Num][name])
              pageListeners[Num][name] = [ ];

            l_Num = pageListeners[Num][name].push(
              JooS.Closure.Function(this[eventName], this)
            ) - 1;

            this.Event_listenerNum.push({
              dispatcher: Num, 
              name: name, 
              listener: l_Num
            });
          }
        }
      }
    }, 

    Event_listenerConstructDefaultSelf: function(eventsArray) {
      this.Event_listenerConstruct(
        getEventsArray(
          eventsArray,
          this.Event_dispatcherConstructDefault()
        )
      );
    }, 
    Event_listenerConstructDefaultParent: function(eventsArray, className) {
      var parentObject = this.Component_getParentObject(className || false);
      if (parentObject) {
        this.Event_listenerConstruct(
          getEventsArray(
            eventsArray,
            parentObject.Event_dispatcherConstructDefault()
          )
        );
      }
    }, 

    Event_listenerDestruct: function(destructName) {
      var Num, name, l_Num;

      if (!this.Event_listenerNum) return;

       for (var i=0; i<this.Event_listenerNum.length; i++) {
        Num = this.Event_listenerNum[i].dispatcher;
        if (pageListeners[Num]) {
          name = this.Event_listenerNum[i].name;
          if (pageListeners[Num][name] && (!destructName || destructName == name)) {
            l_Num = this.Event_listenerNum[i].listener;
            if (typeof(pageListeners[Num][name][l_Num]) != "undefined")
              pageListeners[Num][name][l_Num] = false;
          }
        }
      }
      if (!destructName)
        this.Event_listenerNum = [ ];
    }
  });

/**
 *
 * extras.throws = [
 *   { event: имяСобытия, dispatcher: имяДиспатчера },
 *   ...
 *   { event: имфСобытия, dispatcher: имяДиспатчера }
 * ]
 *
 **/

var getEventsArray = function(simpleArray, id) {
    var events = [], i;
    for (i=0; i<simpleArray.length; i++)
        events.push({
            event: simpleArray[i],
            dispatcher: id
        });
    return events;
};

Beatle.Class = JooS.Reflect(JooS.Element, {
    Event_throwsConstruct: function(events) {
        if (!this.Event_throwsDispatchers)
            this.Event_throwsDispatchers = { };

        var i;
        if (events && events.constructor == Array) {
            for (i=0; i<events.length; i++) {
                if (events[i].dispatcherClass)
                    this.Event_throwsConstructDefaultParent([ events[i].event ], events[i].dispatcherClass);
                else
                    this.Event_throwsDispatchers[events[i].event] = events[i].dispatcher;
            }
        }
    },

    Event_throwsConstructDefaultSelf: function(eventsArray) {
        this.Event_throwsConstruct(
            getEventsArray(
                eventsArray,
                this.Event_dispatcherConstructDefault()
            )
        );
    },
    Event_throwsConstructDefaultParent: function(eventsArray, className) {
        var parentObject = this.Component_getParentObject(className || false);
        if (parentObject) {
            this.Event_throwsConstruct(
                getEventsArray(
                    eventsArray,
                    parentObject.Event_dispatcherConstructDefault()
                )
            );
        }
    },

    /**
     *
     * @param name String|Object { string name, string dispatcher }
     *
     **/
    throwEvent: function(nameObj) {
        var type = typeof(nameObj), name, dispatcherName, dispatcher, event;

        if (type.toLowerCase() == "object" && nameObj.constructor == Array) {
            name = nameObj.name;
            if (typeof(nameObj.dispatcher) == "object" && dispatcher.dispatchEvent)
                dispatcher = nameObj.dispatcher;
            else
                dispatcherName = nameObj.dispatcher;
        }
        else
            name = nameObj;

        if (event = pageEvents[name]) {
            if (!dispatcher) {
                var id = dispatcherName || this.Event_throwsDispatchers[name] || "$", Num = pageDispatchersHash[id];
                if (typeof(Num) != "undefined")
                    dispatcher = pageDispatchers[Num];
            }
            else {
                Num = dispatcher.Event_dispatcherDefaultNum;
                if (typeof(Num) != "undefined")
                    dispatcher = null;
            }

            if (dispatcher) {
                var i, args = [ ];
                for (i=1; i<arguments.length; i++)
                    args.push(arguments[i]);

                try {
                    dispatcher.dispatchEvent(
                        Num,
                        name,
                        event.getData.apply(event, args)
                    );
                }
                catch (notification) {
                    return notification;
                };
            }

        }
    },

    Component_setClassName: function(className) {
        this.Component_className = className;
    },
    Component_getClassName: function() {
        return this.Component_className;
    },
    Component_getId: function(DOMElement) {
        var Element = DOMElement || this.getElement();
        return Element.getAttribute(Beatle.componentAttribute);
    },
    Component_clearBody: function(leaveChildNodes) {
        Beatle.unloadComponents(this, true);
        if (!leaveChildNodes)
            this.getElement().innerHTML = "";
    },
    Component_Remove: function(leaveChildNodes, leaveDOMElement) {
        this.Component_clearBody(leaveChildNodes);

        Beatle.unregisterObject(
            this.Component_getId(),
            !leaveDOMElement
        );
    },
    Component_getParentObject: function(className) {
        var parentNode = this.getElement().parentNode, ret;
        while (parentNode) {
            ret = Beatle.getObjectByElement(parentNode);

            if (ret && (!className || ret.Component_getClassName() == className) )
                return ret;
            parentNode = parentNode.parentNode;
        }
        return null;
    }
});

JooS.Virtual(Beatle.Class, {
    __constructor: function(DOMElement, extras) {
        /*      this.extraData = (extras && extras.data) || {};*/
        this.extraData = (extras) || {};
        this.__constructor.__parent(DOMElement);

        this.Event_dispatcherConstruct(extras && extras.dispatcher);
        this.Event_listenerConstruct(extras && extras.listener);
        this.Event_throwsConstruct(extras && extras.throws);
    },
    __destructor: function() {
        this.Event_listenerDestruct();
        this.Event_dispatcherDestruct();

        this.__destructor.__parent();
    }
});

JooS.Plugin(Beatle.Class, EventDispatcher);
JooS.Plugin(Beatle.Class, EventListener);

Beatle.DocumentDOM = new Beatle.Class(document, {
    dispatcher: "$"
});

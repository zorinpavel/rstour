  JooS.Startup(function() {
    Beatle.loadComponents(JooS.DocumentDOM);
  });
  
  JooS.WindowDOM.attachEvent("unload", function() {
    Beatle.unregisterObjects();
  });

  return Beatle;
})();

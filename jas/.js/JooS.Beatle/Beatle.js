
  var rootName = "";
  var jsFolder = "<? $folder = dirname(dirname($_SERVER['PHP_SELF'])); echo $folder; ?>";
  var jsVer = "<? echo '/'.$JAS_FileVersion.';'; ?>";
  var cssVer = "<? echo str_replace(".js", ".css", '/'.$JAS_FileVersion.';'); ?>";

  var pageObjects = [], foundObjects = {};
  var componentCssClass = "js", componentCssRule = "." + componentCssClass;
  var pageEvents = {};
  
  var Beatle = {
    componentAttribute: "component", 

    registerEvent: function(name, sourceGetData) {
      var newEvent = JooS.Reflect(Event, sourceGetData? {
        getData: sourceGetData
      } : false);
      pageEvents[name] = new newEvent();
    }, 

    registerDispatcher: function(name) {
      this.DocumentDOM.Event_dispatcherConstruct(name);
    }, 

    registerClass: function(className) {
    }, 
    
    registerObject: function(Obj) {
      return pageObjects.push(Obj)-1;
    }, 

    createObject: function(className, HTMLElement) {
      if (this[className]) {
        var extraData = null, poId, newObject;

        if (HTMLElement.onclick) {
          extraData = HTMLElement.onclick();
          HTMLElement.onclick = null;
          HTMLElement.removeAttribute("onclick");
        }

        newObject = new Beatle[className](HTMLElement, extraData);
        newObject.Component_setClassName(className);

        poId = this.registerObject(newObject);

        HTMLElement.setAttribute(this.componentAttribute, poId);
      }
      else {
        try {
          console.log('componentClass not found: ' + className);
        }
        catch (e) {};
      }
    }, 

    getObjectById: function(id) {
      return pageObjects[id];
    }, 
    
    getObjectByElement: function(DOMElement) {
      if(DOMElement.getAttribute) {
        var id = DOMElement.getAttribute(this.componentAttribute);
        return this.getObjectById(id);
      }
    }, 
    
    unregisterObject: function(i, removeNode) {
      if (pageObjects[i]) {
        var htmlElement = pageObjects[i].htmlElement;

        pageObjects[i].__destroy();
        pageObjects[i] = null;

        if (removeNode && htmlElement && htmlElement.parentNode)
          htmlElement.parentNode.removeChild(htmlElement);
      }
    }, 
    
    unregisterObjects: function() {
      for (var i=0; i<pageObjects.length; i++)
        this.unregisterObject(i);
    }, 

    loadComponents: function(DOMElement) {
      var Behavior, i, j, className, requireClasses;
      if (DOMElement instanceof JooS.Element)
        Behavior = DOMElement.cssQuery(componentCssRule);
      else {
        Behavior = [];

        var rootElement = new JooS.Element(DOMElement), tmpBehavior = rootElement.cssQuery(componentCssRule);
        if (rootElement.hasClassName(componentCssClass)) {
          Behavior.push(DOMElement);
          for (i=0; i<tmpBehavior.length; i++)
            Behavior.push(tmpBehavior[i]);
        }
        else
          Behavior = tmpBehavior;
      }

      var foundObjectsHash = [];
      for (i=0; i<Behavior.length; i++) {
        className = Behavior[i].className.split(" ")[1];

        if (Beatle[className])
          Beatle.createObject(className, Behavior[i]);
        else {
          if (requireClasses = Behavior[i].getAttribute("requires")) {
            requireClasses = requireClasses.split(",");
            for (j=0; j<requireClasses.length; j++)
              if (!Beatle[requireClasses[j]] && !foundObjects[requireClasses[j]])
                foundObjects[requireClasses[j]] = [];
          }

          if (!foundObjects[className])
            foundObjects[className] = [];

          foundObjectsHash.push({
            className: className, 
            index: foundObjects[className].push(Behavior[i]) - 1
          });
        }
      }

      var jsStr = "", cssStr = "", jsFiles = [], jsLoading = 0, onload_Event = false;/* (!JooS.Browser.Firefox && !JooS.Browser.IE ? false : true); */

      var loadJsFiles = function() {
        var i, Obj;
        for (i=jsLoading; i<jsFiles.length; i++) {
          jsLoading++;

/*
          if (onload_Event)
            new JooS.JSLoader(jsFiles[i], loadJsFiles);
          else
*/
            new JooS.JSLoader(jsFiles[i]);

          return;
        }

        for (i=0; Obj = foundObjectsHash[i]; i++)
          Beatle.createObject(Obj.className, foundObjects[Obj.className][Obj.index]);
        foundObjects = { };
      };

      var appendClass = function(className) {
        if (className) {
          jsStr += (jsStr ? ";" : "") + "Beatle," + className + ".js";
          cssStr += (cssStr ? ";" : "") + "Beatle," + className + ".css";
        }

        if (!className || jsStr.length > 150) {
          new JooS.CSSLoader(rootName + jsFolder + cssVer + cssStr);
          jsFiles.push(rootName + jsFolder + jsVer + jsStr + ";Beatle,onload.js?");
          jsStr = "";
          cssStr = "";
        }
      };

      for (var i in foundObjects)
        appendClass(i.replace(/_/g, ","));
      if (jsStr)
        appendClass();
      if (!onload_Event)
        this.DOMEvent_onLoadScript = loadJsFiles;

      loadJsFiles();
    }, 

    unloadComponents: function(DOMElement, childNodesOnly) {
      var Element, Behavior, i, rootElement;

      Element = (DOMElement instanceof JooS.Element) ? DOMElement : new JooS.Element(DOMElement);
      Behavior = Element.cssQuery(componentCssRule);

      for (i=0; i<Behavior.length; i++)
        this.unregisterObject(Behavior[i].getAttribute(this.componentAttribute));

      if (!childNodesOnly) {
        if (rootElement = this.getObjectByElement(Element.getElement())) {
          this.unregisterObject(rootElement.getAttribute(this.componentAttribute));
        }
      }
    }
  };

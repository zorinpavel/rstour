  var pageDispatchersHash = { };

  var EventDispatcher = JooS.Reflect(JooS.Class.Plugin, {
    Event_dispatcherDefaultId: false, 
    Event_dispatcherDefaultNum: false, 

    Event_dispatcherConstruct: function(id) {
      if (!this.Event_dispatcherNum)
        this.Event_dispatcherNum = [ ];

      if (id && !pageDispatchersHash[id]) {

        var Num = pageDispatchers.push(this) - 1;
        pageDispatchersHash[id] = Num;

        this.Event_dispatcherNum.push(Num);
      }

      return Num;
    }, 
    Event_dispatcherConstructDefault: function() {
      if (!this.Event_dispatcherDefaultId) {
        this.Event_dispatcherDefaultId = Math.random();
        this.Event_dispatcherDefaultNum = this.Event_dispatcherConstruct(this.Event_dispatcherDefaultId);
      }
      return this.Event_dispatcherDefaultId;
    }, 
    Event_dispatcherDestruct: function() {
      var i, id, Num;

      for (i=0; i<this.Event_dispatcherNum.length; i++)  {
        Num = this.Event_dispatcherNum[i];
        id = pageDispatchers[Num];

        delete pageDispatchersHash[id];
        delete pageDispatchers[Num];
        delete pageListeners[Num];
      }
      this.Event_dispatcherNum = [ ];
    }
  });

  JooS.Virtual(EventDispatcher, {
    dispatchEvent: function(Num, name, data) {
      var l, i;
      if (l = pageListeners[Num])
        if (l = l[name])
           for (i=0; i<l.length; i++)
            if (l[i])
              l[i](data);
    }
  });

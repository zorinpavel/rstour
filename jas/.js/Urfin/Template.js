JooS.Template = JooS.Reflect(JooS.Class, {
  evaluate: function(object) {
    var template = this.template;
    if (object)
      for (var i in object)
        template = template.replace(new RegExp("(\\#\\{" + i + "\\})", "g"), object[i]);
    return template;
  }
});

JooS.Extend(JooS.Template, {
  templates: [], 
  get: function(id) {
    return this.templates[id] ? this.templates[id] : "";
  }, 
  set: function(id, str) {
    this.templates[id] = str;
  }
});

JooS.Virtual(JooS.Template, {
  __constructor: function(id) {
    if (!this.constructor.get(id)) {
      var elem = JooS.cssQuery("#" + id)[0];
      this.constructor.set(id, elem ? elem.innerHTML : "");
      elem = null;
    }
    this.template = this.constructor.get(id);
  }
});

JooS.Extend(JooS.DocumentDOM, {
    DragAction_Start: function(dragObj, event) {
        this.dragObj = dragObj;
        this.dragStartX = event.screenX();
        this.dragStartY = event.screenY();

        this.attachEvent("mousemove", this.DragAction_Move);
        this.attachEvent("mouseup", this.DragAction_Stop);
    },
    DragAction_Move: function(event) {
        var dX = event.screenX() - this.dragStartX, dY = event.screenY() - this.dragStartY;
        if (dX || dY)
            this.dragObj.DragEvent_onMove(dX, dY);
    },
    DragAction_Stop: function() {
        this.detachEvent("mouseup");
        this.detachEvent("mousemove");

        this.dragObj.DragEvent_onStop();
        this.dragObj = null;
    },
    isDragging: function() {
        return !!this.dragObj;
    }
});

JooS.Drag = JooS.Reflect(JooS.Class.Plugin, {
    DragAction_Start: function(event) {
        if (!this.DragEvent_onStart(event))
            JooS.DocumentDOM.DragAction_Start(this, event);
        else
            return true;
    },

    DragEvent_onStart: function(event) {
    },
    DragEvent_onMove: function(dX, dY) {
    },
    DragEvent_onStop: function() {
    },

    __initialize: function() {
        this.attachEvent("mousedown", this.DragAction_Start);
    }
});

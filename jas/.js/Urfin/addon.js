JooS.Extend(JooS.Element.prototype, {
    getElementById: function(Id) {
        var Element = document.getElementById(Id);
        return Element || "";
    },

    inObject: function(event) {
        var Obj = document.elementFromPoint(event.event.clientX, event.event.clientY);
        if(Obj == this.getElement())
            return true;
        else
            return false;
    },

    setHeight: function(height) {
        this.getElement().style.height = height ? parseInt(height) + "px" : "auto";
    },

    setWidth: function(width) {
        this.getElement().style.width = width ? parseInt(width) + "px" : "auto";
    },

    in_array: function(Str, Array) {
        for(var i in Array) {
            if(Str == Array[i])
                return true;
        }
        return false;
    },

});


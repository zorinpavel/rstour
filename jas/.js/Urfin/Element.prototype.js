JooS.Extend(JooS.Element.prototype, {
    getAttributes: function() {
        var attrs = this.getElement().attributes, i, attr, ret = {};
        for (i=0; i<attrs.length; i++) {
            attr = attrs.item(i);
            ret[attr.name] = attr.value;
        }
        return ret;
    },

    getAttribute: function(name) {
        return this.getElement().getAttribute(name);
    },

    getStyle: function(prop, elem) {
        var currentStyle = [];
        elem = elem || this.getElement();

        if (document.defaultView && document.defaultView.getComputedStyle)
            currentStyle = document.defaultView.getComputedStyle(elem, null);
        else if (elem.currentStyle)
            currentStyle = elem.currentStyle;
        return currentStyle[prop] || "";
    },

    addClassName: function (className, elem) {
        elem = elem || this.getElement();

        var currentClass = elem.className;
        if (!new RegExp(("(^|\\s)" + className + "(\\s|$)"), "i").test(currentClass)) {
            elem.className = currentClass + (currentClass ? " " : "") + className;
        }
        return this;
    },

    removeClassName: function (className, elem) {
        elem = elem || this.getElement();

        var classToRemove = new RegExp(("(^|\\s)" + className + "(\\s|$)"), "i");
        elem.className = (elem.className).replace(classToRemove, function (match) {
            var retVal = "";
            if (new RegExp("^\\s+.*\\s+$").test(match)) {
                retVal = match.replace(/(\s+).+/, "$1");
            }
            return retVal;
        }).replace(/^\s+|\s+$/g, "");
        return this;
    },

    replaceClassName: function (className, newClass, elem) {
        elem = elem || this.getElement();

        var classToRemove = new RegExp(("(^|\\s)" + className + "(\\s|$)"), "i");
        elem.className = (elem.className || "").replace(classToRemove, function (match, p1, p2) {
            var retVal = p1 + newClass + p2;
            if (new RegExp("^\\s+.*\\s+$").test(match)) {
                retVal = match.replace(/(\s+).+/, "$1");
            }
            return retVal;
        }).replace(/^\s+|\s+$/g, "");
        return this;
    },

    hasClassName: function (className) {
        return new RegExp(("(^|\\s)" + className + "(\\s|$)"), "i").test(this.getElement().className);
    },

    setPos: function(left, top) {
        this.htmlElement.style.left = parseInt(left) + "px";
        this.htmlElement.style.top = parseInt(top) + "px";
    },

    getPos: function() {
        return {
            left: this.htmlElement.offsetLeft,
            top: this.htmlElement.offsetTop
        };
    },

    getScrollPos: function() {
        return {
            left: this.htmlElement.scrollLeft,
            top: this.htmlElement.scrollTop
        };
    },

    getPagePos: function(elem) {
        var element = elem || this.getElement();
        if (element.getBoundingClientRect) {
            return this.getOffsetRect(element)
        } else {
            return this.getOffsetSum(element)
        }
    },

    getOffsetSum: function(elem) {
        var str = "";
        var valueT = 0, valueL = 0, pos;
        var element = elem || this.getElement();

        do {
            valueL += parseInt(element.offsetLeft) || 0;
            valueT += parseInt(element.offsetTop) || 0;
            element = element.offsetParent;

            if (element && !JooS.Browser.isOpera) {
                pos = this.getStyle("position", element);
                if (pos == "relative" || pos == "absolute") {
                    valueL += parseInt(this.getStyle("borderLeftWidth", element)) || 0;
                    valueT += parseInt(this.getStyle("borderTopWidth", element)) || 0;
                }
            }
        } while (element);

        return { left: valueL, top: valueT };
    },

    getOffsetRect: function(elem) {
        var element = elem || this.getElement();
        var box = element.getBoundingClientRect()

        var body = document.body
        var docElem = document.documentElement

        var scrollTop = window.pageYOffset || docElem.scrollTop || body.scrollTop
        var scrollLeft = window.pageXOffset || docElem.scrollLeft || body.scrollLeft

        var clientTop = docElem.clientTop || body.clientTop || 0
        var clientLeft = docElem.clientLeft || body.clientLeft || 0

        var top  = box.top +  scrollTop - clientTop
        var left = box.left + scrollLeft - clientLeft

        return { left: Math.round(left), top: Math.round(top) };
    },

    setSize: function(width, height) {
        this.getElement().style.width = width ? parseInt(width) + "px" : "";
        this.getElement().style.height = height ? parseInt(height) + "px" : "";
    },

    getSize: function() {
        var element = this.getElement();
        if (element.style.display != 'none')
            return {
                width: element.offsetWidth,
                height: element.offsetHeight
            };

        var els = element.style;
        var originalVisibility = els.visibility;
        var originalPosition = els.position;
        els.visibility = 'hidden';
        els.position = 'absolute';
        els.display = '';
        var originalWidth = element.clientWidth;
        var originalHeight = element.clientHeight;
        els.display = 'none';
        els.position = originalPosition;
        els.visibility = originalVisibility;
        return {
            width: parseInt(originalWidth),
            height: parseInt(originalHeight)
        };
    },

    setDisplay: function(display) {
        this.getElement().style.display = display;
        return this;
    },

    setVisibility: function(visibility) {
        this.getElement().style.visibility = visibility;
        return this;
    },

    setOpacity: (JooS.Browser.isIE ? function(opacity) {
        this.getElement().runtimeStyle.filter = "alpha(opacity="+opacity+")";
        return this;
    } :
        function(opacity) {
            this.getElement().style.opacity = opacity / 100;
            return this;
        }
    ),

    isArray: function(obj) {
        return (/Array/).test(Object.prototype.toString.call(obj));
    },

});

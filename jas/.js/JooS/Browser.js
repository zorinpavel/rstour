JooS.Browser = new function() {
    var n, v, t, ua = navigator.userAgent;

    if (window.opera)
        n = "Opera", v = window.opera.version();
    else if (/*@cc_on!@*/false) {
//  else if (ua.match(/\sMSIE\s/)) {
        n = "IE";
        v = parseInt(ua.match(/MSIE\s(\d+\.\d+)/)[1]);
    } else if (t = ua.match(/iPad/)) {
        n = "iPad";
        v = 1;
    } else if (t = ua.match(/\sChrome\/(\d+)/)) {
        n = "Chrome";
        t = parseFloat(t[1]);
        v = 1;
    } else if (t = ua.match(/AppleWebKit\/([\d.]+)/)) {
        n = "Safari";
        t = parseFloat(t[1]);
        if (t < 522.11) v = 2; else if (t < 525.13) v = 3; else if (t < 526.9) v = 3.1; else v = 4;
    } else if (t = ua.match(/\sGecko\/(\d{6})/)) {
        n = "Firefox";
        v = parseFloat(ua.match(/\sFirefox\/(\d+\.\d+)/)[1]);
    } else
        n = "Mobile", v = 1;

    this.name = n;
    this[n] = this.version = v;

    this.pngExt = (this.IE && this.IE < 5.5 ? "gif" : "png");
    this.pngClass = (this.IE && this.IE < 7 ? "png_scale" : "");

};

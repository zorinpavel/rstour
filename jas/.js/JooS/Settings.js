JooS.Settings = JooS.Reflect(JooS.Class, {
  getSettingsId: function() {
    return "JooS";
  }, 
  __constructor: function(Settings) {
    this.__common[this.getSettingsId()] = this;
    JooS.Extend(this, Settings);
  }
});

JooS.Closure = {
    Value: function(value) {
        return function() {
            return value;
        };
    },
    Function: function(Action, Context) {
        return function() {
            return Action.apply(Context || window, arguments);
        }
    }
};

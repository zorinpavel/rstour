JooS.Startup = (function() {
  var pageStartupActions = [], pageStarted = false;

  function onLoad() {
    if (!pageStarted) {
      pageStarted = true;
      for (var i=0; i<pageStartupActions.length; i++)
        pageStartupActions[i]();
    }
    window.onload = null;
  };

  var Startup = function(Action) {
    if (pageStarted)
      Action();
    else {
      if (!pageStartupActions.length) {
        if (this.Browser.Firefox || this.Browser.Opera >= 9)
          document.addEventListener("DOMContentLoaded", onLoad, false);
        else if (this.Browser.Safari) {
          var __timer = setInterval(function() {
            if (/loaded|complete/.test(document.readyState))
              clearInterval(__timer);
          }, 100);
        }

        window.onload = onLoad;
      }
      pageStartupActions.push(Action);
    }
  };

  Startup.onLoad_IEDefer = function(script) {
    if (script.readyState == "complete")
      onLoad();
  };

  return Startup;
}());


JooS.Startup(function() {
  JooS.BodyDOM = new JooS.Element(JooS.cssQuery("body")[0]);
  JooS.HeadDOM = new JooS.Element(JooS.cssQuery("head")[0]);
});

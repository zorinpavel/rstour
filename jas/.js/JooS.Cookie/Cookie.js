JooS.Cookie = {
  CookieSplit : ".", 

  Read: function(Name) {
	var nameEQ = Name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ')
		  c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0)
		  return c.substring(nameEQ.length,c.length);
	}
	return null;
  }, 

  Set: function(Name, Value, Days, Path) {
	if (Days) {
		var Expires = new Date();
		Expires.setTime(Expires.getTime()+(Days*24*60*60*1000));
	}
    /* � �� ������, �� � Path � IE ���� �� �������� */
	if(JooS.Browser.isIE)
	  Path = "";
    document.cookie = Name + "=" + escape(Value) + ( Expires ? "; expires=" + Expires.toGMTString() : "" ) + ( Path ? "; path=" + Path : "" );
  }, 

  Delete: function(Name) {
    if(this.Read(Name))
      this.Set(Name, "", -1);
  }, 

  SetArr: function(Name, Value, Days, Path) {
    var CookieStr = this.Read(Name);
    var CookieArr = new Array();
    if(CookieStr) {
      CookieArr = CookieStr.split(this.CookieSplit);
      for(i in CookieArr) {
        if(CookieArr[i] == Value) {
          CookieArr.splice(i, 1);
        }
      }
    }
    CookieArr.push(Value);
    CookieStr = "";
    CookieStr = CookieArr.join(this.CookieSplit);
    this.Set(Name, CookieStr, Days, Path);
  }, 

  DeleteArr: function(Name, Value, Days, Path) {
    var CookieStr = this.Read(Name);
    var CookieArr = new Array();
    if(CookieStr) {
      CookieArr = CookieStr.split(this.CookieSplit);
      for(i in CookieArr) {
        if(CookieArr[i] == Value) {
          CookieArr.splice(i, 1);
        }
      }
    }
    CookieStr = "";
    CookieStr = CookieArr.join(this.CookieSplit);
    this.Set(Name, CookieStr, Days, Path);
  }

};

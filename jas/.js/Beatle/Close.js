Beatle.Close = JooS.Reflect(Beatle.Class, {

    DOMEvent_onClick: function() {
        if(this.Removed) {
            this.Parent.removeClassName(this.extraData.OutEffect || "fadeOut");
            this.Parent.addClassName(this.extraData.InEffect || "fadeIn");
            this.Parent.htmlElement.style.animationDelay = "0s";
            this.Removed = false;
            this.removeClassName("removed");
        } else {
            this.Parent.removeClassName(this.extraData.InEffect || "fadeIn");
            this.Parent.addClassName(this.extraData.OutEffect || "fadeOut");
            this.Parent.htmlElement.style.animationDelay = "0s";
            this.Removed = true;
            this.addClassName("removed");
            var Timeout = setTimeout(JooS.Closure.Function(function () {
                if (this.Remove) {
                    this.Parent.setDisplay("none");
                    this.Parent.htmlElement.innerHTML = "";
                    this.Component_Remove();
                }
            }, this), 500);
        }
    },

    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.Remove = !this.extraData.liveHTML;
        this.Parent = new JooS.Element(document.getElementById(this.extraData.id) || this.htmlElement.parentNode);
        this.attachEvent("click", this.DOMEvent_onClick);

    }

});

Beatle.Bookmarks_Content = JooS.Reflect(Beatle.Class, {

    ClassName: "selected",

    Listener_Event_Bookmark_onActive: function(Id) {
        if(this.Id == Id) {
            this.addClassName(this.ClassName);
            for(var i in this.Inputs)
                this.Inputs[i].disabled = false;

        } else {
            this.removeClassName(this.ClassName);
            for(var i in this.Inputs)
                this.Inputs[i].disabled = true;
        }
    },

    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.Id = this.getAttribute("id") || this.extraData.Id;
        this.Inputs = this.cssQuery("input");

        this.Event_listenerConstructDefaultParent([
            "Event_Bookmark_onActive"
        ], "Bookmarks");
    }

});


Beatle.Bookmarks_Tab = JooS.Reflect(Beatle.Class, {

    ClassName: "selected",

    DomEvent_onClick: function() {
        if(this.isActive && !this.extraData.passUrl)
            return true;
        else
            this.throwEvent("Event_Bookmark_onActive", this.Id);
    },

    Listener_Event_Bookmark_onActive: function(Id) {
        if(this.Id == Id) {
            this.addClassName(this.ClassName);
            this.isActive = true;
            this.getElement().blur();
        } else {
            this.removeClassName(this.ClassName);
            this.isActive = false;
        }
    },

    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.Id = this.getAttribute("name") || this.extraData.Id;
        this.isActive = this.hasClassName(this.ClassName);

        this.attachEvent("click", this.DomEvent_onClick);

        this.Event_throwsConstructDefaultParent([
            "Event_Bookmark_onActive"
        ], "Bookmarks");

        this.Event_listenerConstructDefaultParent([
            "Event_Bookmark_onActive"
        ], "Bookmarks");

    }

});

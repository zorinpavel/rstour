Beatle.Calendar = JooS.Reflect(Beatle.Class, {

    DOMEvent_onChange: function() {
        var Name = this.Element.name;
        Name = Name.replace("\[", "\\[");
        Name = Name.replace("\]", "\\]");
        var Replacement = new RegExp(Name + "=([\\w|\\.|\\-]*)", "gi");
        var Href = window.location.search.replace(Replacement, "");

        if(this.extraData.Remove) {
            for(i in this.extraData.Remove) {
                var Remove = new RegExp(this.extraData.Remove[i] + "=(\\w+)", "gi");
                Href = Href.replace(Remove, "");
            }
        }
        Href = Href + "&" + Name + "=" + this.Element.value;
        Href = Href.replace(/^&/gi, "?");
        Href = Href.replace(/&{2,}/gi, "&");
        Href = Href.replace(/&$/gi, "");
        Href = Href.replace(/\?&/gi, "?");

        window.location.href = window.location.pathname + Href;
    },

    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.Element = this.htmlElement;

        var minDate = new Date();
        var maxDate = new Date(2050, 12, 31);
        var yearRange = [1940, 2050];

        switch(this.extraData.direction) {
            case 'both':
                minDate = new Date(1940, 1, 1);
                maxDate = new Date(2050, 12, 31);
                yearRange = [1940, 2050];
                break;
            case 'past':
                minDate = new Date(1940, 1, 1);
                maxDate = new Date();
                yearRange = [1940, new Date().getFullYear()];
                break;
            case 'future':
            default:
                minDate = new Date();
                maxDate = new Date(2050, 12, 31);
                yearRange = [new Date().getFullYear(), 2050];
                break;
        }

        var Picker = new Pikaday({
                field: this.htmlElement,
                firstDay: 1,
                minDate: minDate,
                maxDate: maxDate,
                yearRange: yearRange
            });

        if(this.extraData.change)
            this.attachEvent("change", this.DOMEvent_onChange);

        var Icon = new JooS.Element(this.htmlElement.nextSibling);
        Icon.Element = this.Element;
        Icon.extraData = this.extraData;
        Icon.attachEvent("click", Picker._onInputClick);

    }

});

Beatle.ViewportCheck = JooS.Reflect(Beatle.Class, {

    ClassName: "visible",

    Listener_Event_Document_Scroll: function() {
        var Scroll = JooS.DocumentDOM.getScroll();
        var ViewportHeight = JooS.DocumentDOM.getViewportHeight();
        var ElementScroll = this.getPagePos();
        if(Scroll.scrollY >= ElementScroll.top - ViewportHeight && Scroll.scrollY <= ElementScroll.top) {
            this.addClassName(this.ClassName);
//            this.Component_Remove(true, true);
        } else if(!this.extraData.once)
            this.removeClassName(this.ClassName);
    },

    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.ClassName = this.extraData.className || this.ClassName;

        this.Event_listenerConstructDefaultParent([
            "Event_Document_Scroll"
        ], "Body");

        this.Listener_Event_Document_Scroll();

    }

});

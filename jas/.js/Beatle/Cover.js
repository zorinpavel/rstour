Beatle.Cover = JooS.Reflect(Beatle.Class, {

    DOMEvent_onClick: function() {
        if(this.Aside)
            this.throwEvent("Event_Aside_Expand");
        if(this.Search)
            this.throwEvent("Event_Search_Expand");
    },

    Listener_Event_Aside_Expand: function() {
        if(this.hasClassName("expanded")) {
            this.removeClassName("expanded");
            this.Aside = false;
        } else {
            this.addClassName("expanded");
            this.Aside = true;
        }
    },

    Listener_Event_Search_Expand: function() {
        if(this.hasClassName("expanded")) {
            this.removeClassName("expanded");
            this.Search = false;
        } else {
            this.addClassName("expanded");
            this.Search = true;
        }
    },

    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.attachEvent("click", this.DOMEvent_onClick);

        this.Event_throwsConstructDefaultParent([
            "Event_Aside_Expand", "Event_Search_Expand"
        ], "Body");

        this.Event_listenerConstructDefaultParent([
            "Event_Aside_Expand", "Event_Search_Expand"
        ], "Body");

    }

});

Beatle.classHttp = JooS.Reflect(Beatle.Class, {

    defaultWidth: 300,
    countAppend: 1,
    theme: false,

    createElem: function() {
        var Div = document.createElement("div");
        Div.className= "js classHttp_Window" + ((this.Center || this.Theme == "fixed") ? " floatDivFixed" : " floatDiv");
        Div.id = "win_" + this.Target;

        var TextDiv = document.createElement("div");
        TextDiv.id = this.Target;


        switch(this.Theme) {
            case "window":
            case "fixed":
            default:
                Div.style.width = (this.extraData.width || this.defaultWidth + "px");
                Div.style.height = this.extraData.height;
                if(this.extraData.height)
                    TextDiv.style.height = this.extraData.height;
                TextDiv.className = "floatDivContent clear" + (this.extraData.className ? " " + this.extraData.className : "");
                this.createBack();
                this.createClose(Div);
//                this.createPointer(Div);
                Div.appendChild(TextDiv);
                break;
            case "none":
                Div.appendChild(TextDiv);
                break;
        }

        document.body.appendChild(Div);
        Beatle.loadComponents(Div);

        /* сюда идет response */
        return TextDiv;
    },

    createPointer: function(Div) {
        if(!this.extraData.center) {
            var Pointer = document.createElement("div");
            Pointer.className = "floatPointer";
            Pointer.id = "floatPointer";
            Div.appendChild(Pointer);
        }
    },

    createClose: function(Div) {
        var Close = document.createElement("div");
        Close.className = "js classHttp_Close floatClose icon close";
        Close.id = "floatClose";
        Div.appendChild(Close);
        Beatle.loadComponents(Close);
    },

    createBack: function() {
        var Back = document.createElement("div");
        Back.className = "js classHttp_Back floatBack" + (this.Center ? "" : " opacity");
        Back.id = "floatBack";
        document.body.appendChild(Back);
        Beatle.loadComponents(Back);
    },

    DomEvent_onClick: function() {
        var Data = {}, Names = [], Values = [], ArgvPairs = [];

        this.throwEvent("Event_classHttp_SetSelected");
        this.addClassName("selected");

        if(this.extraData.confirm) {
            if(!confirm(this.extraData.confirm))
                return;
        }

        if(this.extraData.params)
            Data = this.extraData.params;

        /* специально для селектов */
        if(this.extraData.onChange) {
            Data[this.extraData.name] = this.htmlElement.value;
        }

        this.Href = this.getAttribute("href");
        if(this.Href) {
            this.Href = this.Href.replace(/\/.*\/.*\?|\/.*\/|.*\?/, "");
            if(this.Href != "") {
                ArgvPairs = this.Href.split("&");
                for(var i in ArgvPairs) {
                    Values = ArgvPairs[i].split("=");
                    Data[Values[0]] = Values[1];
                }
            }
        }

        if(this.extraData.c)
            Data.c = this.extraData.c;
        if(this.extraData.m)
            Data.m = this.extraData.m;
        if(this.extraData.s)
            Data.s = this.extraData.s;
        if(this.extraData.a)
            Data.a = this.extraData.a;

        var Element = this.getElementById(this.Target);
        if(!Element && this.extraData.loader != "none") {
            Element = this.createElem();
            if(Element.parentNode.id.indexOf("win_") > -1) {
                var ClassName, Object = this;
                while(ClassName != "Body") {
                    Object = Object.Component_getParentObject();
                    ClassName = Object.Component_getClassName();
                    if(ClassName == "Floating") {
                        this.Position = {
                            left: this.Position.left,
                            top: this.getPagePos().top - JooS.DocumentDOM.getScroll().scrollY
                        };
                        Element.parentNode.className = Element.parentNode.className.replace("floatDiv", "floatDivFixed");
                        break;
                    }
                }
                this.throwEvent("Event_classHttp_SetPosition", this.Center, this.Size, this.Position, this.Orientation);
            }
        }
        Element = new JooS.Element(Element);

        if(Element && this.extraData._replace) {
            var ReplaceElement = document.createElement("div");
            ReplaceElement.id = "replace_" + this.Target;
            ReplaceElement.style.display = "none";
            Element.htmlElement.parentNode.insertBefore(ReplaceElement, Element.htmlElement);
        }

        if(Element && this.extraData._append) {
            this.extraData.loader = "none";
            var AppendElement = document.createElement("div");
            AppendElement.id = "append_" + this.Target;
            Element.htmlElement.parentNode.insertBefore(AppendElement, Element.htmlElement.nextSibling);
        }

        if(Element && this.extraData.iframe) {
            var Iframe = document.createElement("iframe");
            Iframe.style.width = "100%";
            Iframe.style.height = "100%";
            Iframe.src = this.getAttribute("href");
            Element.htmlElement.appendChild(Iframe);
            return;
        }

        var Loader = this.extraData.loader ? this.extraData.loader : 'loader';
        if(Loader == "opacity")
            Element.setOpacity(50);

        var Self = this;
        Beatle.unloadComponents(Element.htmlElement);
        JooS.Ajax.Get("/ajax.php", Data, function() {
            /* теряется lastChild; новый div с id = target */
            if(Self.extraData._replace) {
                ReplaceElement.innerHTML = this.responseText;
                ReplaceElement.className = Element.htmlElement.className;
                Beatle.loadComponents(ReplaceElement);
                Element.htmlElement.parentNode.removeChild(Element.htmlElement);
                ReplaceElement.id = Self.Target;
                ReplaceElement.style.display = "block";
            }

            if(Self.extraData._append) {
                AppendElement.innerHTML = this.responseText;
                AppendElement.className = Self.extraData._append.className ? Self.extraData._append.className : Element.htmlElement.className;
                Beatle.loadComponents(AppendElement);
                AppendElement.id = Self.Target + "_" + Self.countAppend++;
            }

            if(Self.extraData.remove) {
                Element.htmlElement.parentNode.removeChild(Element.htmlElement);
            } else if(Loader != "none") {
                Element.setOpacity(0);
                Element.htmlElement.innerHTML = this.responseText;
                Beatle.loadComponents(Element.htmlElement);
                Element.setOpacity(100);
            }

            if(this.responseJS) {
                /* Self.extraData.reload = "none" - не перегружать */
                if(this.responseJS.Location && !Self.extraData.reload) {
                    if(this.responseJS.Location === true)
                        JooS.WindowDOM.ReloadWindow(0);
                    else
                        JooS.WindowDOM.ReloadWindow(0, this.responseJS.Location);
                    Element.htmlElement.innerHTML = "переадресация...";
                }
                if(this.responseJS.Errors) {
                    Element.htmlElement.innerHTML = this.responseJS.Errors[0]['Error'];
                }
            }

            if(Self.Center) {
                JooS.Startup(function() {
                    Self.throwEvent("Event_classHttp_SetPosition", Self.Center, Self.Size, Self.Position);
                });
            }

        }, this.Target, Loader, this.Cache);

    },


    Listener_Event_classHttp_SetSelected: function() {
        this.removeClassName("selected");
    },


    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        /* для селектов */
        if(this.extraData.onChange) {
            this.attachEvent("change", this.DomEvent_onClick);
        } else
            this.attachEvent("click", this.DomEvent_onClick);

        this.Size = this.getSize();
        this.Position = this.getPagePos();
        this.Theme = this.extraData.theme;
        this.Target = this.getAttribute("target") ? this.getAttribute("target") : this.extraData.target ? this.extraData.target : "new";
        this.Center = this.extraData.center || false;
        this.Orientation = this.extraData.orientation || false;
        this.Cache = this.extraData.cache || true;

        if(this.getAttribute("load") || this.extraData.load) {
            if(!JooS.Browser.isChrome) {
                var Self = this;
                JooS.Startup(function() {
                        Self.DomEvent_onClick();
                    }
                );
            } else {
                this.DomEvent_onClick();
            }
        }

        this.Event_throwsConstructDefaultParent([
            "Event_classHttp_SetPosition", "Event_classHttp_SetSelected"
        ], "Body");

        this.Event_listenerConstructDefaultParent([
            "Event_classHttp_SetSelected"
        ], "Body");

    }

});

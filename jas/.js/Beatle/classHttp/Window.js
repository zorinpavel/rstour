Beatle.classHttp_Window = JooS.Reflect(Beatle.Class, {

    Listener_Event_classHttp_WindowClose: function() {
        this.Component_Remove();
    },

    Listener_Event_classHttp_SetPosition: function(Data) {
        this.Size = this.getSize();
        var Left = 0, Top = 0, PointerLeft = 20;
        if(Data.Center) {
            Left = parseInt( (document.body.clientWidth - this.Size.width) / 2);
            var WindowHeight = window.innerHeight || (document.documentElement && document.documentElement.clientHeight) || document.body.clientHeight;
            var ShiftTop = 0; //window.pageYOffset || (document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop;
            Top = parseInt( (WindowHeight - this.Size.height) / 2) + ShiftTop;

            if(WindowHeight < this.Size.height) {
                this.replaceClassName("floatDivFixed", "floatDiv");
                Top = JooS.WindowDOM.getScrollPos().top + 14;
            } else {
                this.replaceClassName("floatDiv", "floatDivFixed");
            }
            this.setPos(Left, Top);
        } else {
            switch(Data.Orientation) {
                case 'left':
                    Left = Data.Position.left + Data.Size.width - this.Size.width;
                    PointerLeft = this.Size.width - Data.Size.width / 2;
                    break;
                default:
                    if(this.Size.width + Data.Position.left + 5 > document.body.clientWidth) {
                        Left = document.body.clientWidth - this.Size.width - 10;
                        PointerLeft = Data.Position.left - Left + (Data.Size.width / 2);
                    } else {
                        Left = Data.Position.left;
                        PointerLeft = Data.Size.width / 2;
                    }
                    break;
            }
            this.setPos(Left, Data.Position.top + Data.Size.height);
            if(this.Pointer) {
                this.Pointer.setPos(PointerLeft, -22);
            }
        }

    },

    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        var Pointer = this.cssQueryStrict(".floatPointer")[0];
        if(Pointer)
            this.Pointer = new JooS.Element(Pointer);

        this.Event_listenerConstructDefaultParent([
            "Event_classHttp_WindowClose", "Event_classHttp_SetPosition"
        ], "Body");

    }
});

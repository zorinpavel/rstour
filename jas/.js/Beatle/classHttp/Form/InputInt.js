Beatle.classHttp_Form_InputInt = JooS.Reflect(Beatle.classHttp_Form_Input, {
  defaultLength: 1, 

  Do: function() {
    var Str = /\D/ig;
      if(this.getInput_Value().search(Str) != -1) {
        this.throwEvent("Event_classHttp_FormInput_onError", {
          Name: this.inputName, 
          ErrorText: "Поле может содержать только цыфры"
        });
      } else {
        this.throwEvent("Event_classHttp_FormInput_onOk", { Name : this.inputName });
      }
  }

});

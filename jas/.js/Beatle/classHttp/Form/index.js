Beatle.classHttp_Form = JooS.Reflect(Beatle.classHttp, {

    DomEvent_onClick: function() {
        return true;
    },

    DomEvent_onSubmit: function() {
        if(!this.extraData.submit)
            this.throwEvent("Event_HttpForm_Submit");
        else
            this.getElement().submit();
    },

    Listener_Event_HttpForm_Submit: function() {
        var Data = {}, Form;

        if(!this.extraData.submit) {
            if(!this.Sending) {
                this.Sending = true;
                Form = this.getElement();

                if(this.extraData.params) {
                    for(var j in this.extraData.params)
                        Data[j] = this.extraData.params[j];
                }

                for(var i=0; i < Form.elements.length; i++) {
                    if(Form.elements[i].name && Form.elements[i].type != 'button' && Form.elements[i].type != 'submit') {
                        if((Form.elements[i].type == 'radio' || Form.elements[i].type == 'checkbox')) {
                            if(Form.elements[i].checked == true)
                                Data[Form.elements[i].name] = Form.elements[i].value;
                        } else {
                            Data[Form.elements[i].name] = Form.elements[i].value;
                        }
                    }
                }

                if(this.extraData.c)
                    Data.c = this.extraData.c;
                if(this.extraData.m)
                    Data.m = this.extraData.m;
                if(this.extraData.s)
                    Data.s = this.extraData.s;
                if(this.extraData.a)
                    Data.a = this.extraData.a;

                var Element = this.getElementById(this.Target);
                if(!Element && this.extraData.loader != "none") {
                    Element = this.createElem();
                    if(Element.parentNode.id.indexOf("win_") > -1) {
                        this.createBack();
                        this.throwEvent("Event_classHttp_SetPosition", this.Center, this.Size, this.Position);
                    }
                }
                Element = new JooS.Element(Element);

                if(Element && this.extraData._replace) {
                    var ReplaceElement = document.createElement("div");
                    ReplaceElement.id = "replace_" + this.Target;
                    ReplaceElement.style.display = "none";
                    Element.htmlElement.parentNode.insertBefore(ReplaceElement, Element.htmlElement);
                }

                if(Element && this.extraData._append) {
                    var AppendElement = document.createElement("div");
                    AppendElement.id = "append_" + this.Target;
                    AppendElement.className = Element.htmlElement.className;
                    Element.htmlElement.parentNode.insertBefore(AppendElement, Element.htmlElement);
                    Element.htmlElement = AppendElement;
                    this.Target = AppendElement.id + this.countAppend;
                    this.countAppend = this.countAppend + 1;
                }

                var Loader = this.extraData.loader ? this.extraData.loader : 'loader';
                if(Loader == "opacity")
                    Element.setOpacity(50);

                var Self = this;
                Beatle.unloadComponents(Element.htmlElement);
                JooS.Ajax.Post("/ajax.php", Data, function() {
                    /* теряется lastChild; новый div с id = target */
                    if(Self.extraData._replace) {
                        ReplaceElement.innerHTML = this.responseText;
                        ReplaceElement.className = Element.htmlElement.className;
                        Beatle.loadComponents(ReplaceElement);
                        Element.htmlElemnt.parentNode.removeChild(Element.htmlElement);
                        ReplaceElement.id = Self.Target;
                        ReplaceElement.style.display = "block";
                    }

                    if(Self.extraData._append)
                        LoaderElement.innerHTML = "";

                    if(Self.extraData.remove) {
                        Element.htmlElement.parentNode.removeChild(Element.htmlElement);
                    } else if(Loader != "none") {
                        Element.setOpacity(0);
                        Element.htmlElement.innerHTML = this.responseText;
                        Beatle.loadComponents(Element.htmlElement);
                        Element.setOpacity(100);
                    }

                    if(this.responseJS) {
                        /* Self.extraData.reload = "none" - не перегружать */
                        if(this.responseJS.Location && !Self.extraData.reload) {
                            if(this.responseJS.Location === true)
                                JooS.WindowDOM.ReloadWindow(0);
                            else
                                JooS.WindowDOM.ReloadWindow(0, this.responseJS.Location);
                            Element.htmlElement.innerHTML = "переадресация...";
                        }
                        if(this.responseJS.Errors) {
                            Element.htmlElement.innerHTML = this.responseJS.Errors[0]['Error'];
                        }
                    }

                    if(Self.Center) {
                        JooS.Startup(function() {
                            Self.throwEvent("Event_classHttp_SetPosition", Self.Center, Self.Size, Self.Position);
                        });
                    }

                    Self.Sending = false;

                }, this.Target, Loader);

            }
        } else {
            this.getElement().submit();
        }
    },


    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.attachEvent("submit", this.DomEvent_onSubmit);

        this.Event_listenerConstructDefaultSelf([
            "Event_HttpForm_Submit"
        ]);

        this.Event_throwsConstructDefaultSelf([
            "Event_HttpForm_Submit"
        ]);

        this.Event_throwsConstructDefaultParent([
            "Event_classHttp_SetPosition"
        ], "Body");

    }
});

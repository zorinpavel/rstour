Beatle.classHttp_Form_Input = JooS.Reflect(Beatle.Class, {

    badClass: "error",
    defaultLength: 3,

    Listener_Event_classHttp_FormInput_focusField: function(name) {
        if(this.inputName == name)
            this.getElement().focus();
    },

    getInput_Value: function() {
        return this.getElement().value;
    },

    setInput_Value: function(value) {
        this.getElement().value = value;
    },


    Listener_Event_classHttp_FormInput_onError: function(Data) {
        if(Data.Name == this.inputName) {
            this.addClassName(this.badClass);
            this.htmlElement.setCustomValidity(Data.ErrorText);
        }
    },


    Listener_Event_classHttp_FormInput_onCancelError: function(Data) {
        if(Data.Name == this.inputName) {
            this.removeClassName(this.badClass);
            this.htmlElement.setCustomValidity('');
        }
    },


    DOMEvent_onChange: function() {
        this.throwEvent("Event_classHttp_FormInput_onCancelError", { Name: this.inputName });

        var Name = this.inputName;
        var Value = this.getInput_Value();

        clearTimeout(this.Timeout);
        this.Timeout = null;

        if(!Value && !this.Empty) {
            this.throwEvent("Event_classHttp_FormInput_onError", {
                Name: Name,
                ErrorText: ""
            });
        } else {
            if(Value.length > 0 && Value.length < this.Length) {
                this.throwEvent("Event_classHttp_FormInput_onError", {
                    Name: Name,
                    ErrorText: ""
                });
            } else {
                if(this.maxLength > 0 && Value.length > this.maxLength) {
                    this.throwEvent("Event_classHttp_FormInput_onError", {
                        Name: Name,
                        ErrorText: ""
                    });
                } else {
                    this.Timeout = setTimeout(JooS.Closure.Function(function() {
                        this.Do();
                    }, this), 500);
                }
            }
        }
    },


    Do: function() {
        if(this.extraData.c) {
            var Self = this;

            var Data = {};
            if(this.extraData.c)
                Data.c = this.extraData.c;
            if(this.extraData.m)
                Data.m = this.extraData.m;
            if(this.extraData.s)
                Data.s = this.extraData.s;
            if(this.extraData.a)
                Data.a = this.extraData.a;
            Data.v = this.getInput_Value();

            JooS.Ajax.Get("/ajax.php", Data, function() {
                Self.throwEvent("Event_classHttp_FormInput_onAjax", { Name: Self.inputName });
                if(this.responseJS) {
                    if(this.responseJS.Error) {
                        Self.throwEvent("Event_classHttp_FormInput_onError", {
                            Name: Self.inputName,
                            ErrorText: this.responseJS.Error
                        });
                    } else {
                        Self.throwEvent("Event_classHttp_FormInput_onOk", { Name: Self.inputName });
                    }
                }
            });
        } else {
            return;
        }
    },


    Listener_Event_classHttp_FormInput_GetValue: function(Name) {
        if(Name == this.inputName)
            throw { Value : this.getInput_Value() };
    },


    DOMEvent_onBlur: function() {
        if(!this.Submited) {
            this.throwEvent("Event_HttpForm_Submit");
        }
        this.Submited = false;
    },


    Listener_Event_HttpForm_Submit: function() {
        this.Submited = true;
        this.getElement().blur();
    },


    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.inputName = this.getElement().name;

        this.Length = this.extraData.length ? this.extraData.length : this.defaultLength;
        this.maxLength = this.extraData.maxlength || 0;
        this.Empty = this.extraData.empty || false;

        this.attachEvent("keyup", this.DOMEvent_onChange);

        if(this.extraData.onBlur)
            this.attachEvent("blur", this.DOMEvent_onBlur);
        if(this.extraData.onChange)
            this.attachEvent("change", this.DOMEvent_onBlur);

        this.Event_throwsConstructDefaultParent([
            "Event_classHttp_FormInput_onChange", "Event_classHttp_FormInput_onCancelError", "Event_classHttp_FormInput_onError", "Event_classHttp_FormInput_onAjax", "Event_classHttp_FormInput_onOk", "Event_classHttp_FormInput_GetValue", "Event_HttpForm_Submit"
        ], "classHttp_Form");

        this.Event_listenerConstructDefaultParent([
            "Event_classHttp_FormInput_setValues", "Event_classHttp_FormInput_focusField", "Event_classHttp_FormInput_onError", "Event_classHttp_FormInput_onCancelError", "Event_classHttp_FormInput_GetValue", "Event_HttpForm_Submit"
        ], "classHttp_Form");

    }

});

Beatle.classHttp_Form_Checkbox_Group = JooS.Reflect(Beatle.Class, {

    Listener_Event_HttpForm_Checkbox_UnCheckRadio: function(group) {
        if(group != this.extraData.group)
            this.throwEvent("Event_HttpForm_Checkbox_UnCheckGroup");
    },

    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.Event_throwsConstructDefaultSelf([
            "Event_HttpForm_Checkbox_UnCheckGroup"
        ]);

        this.Event_listenerConstructDefaultParent([
            "Event_HttpForm_Checkbox_UnCheckRadio"
        ], "classHttp_Form_Checkbox_Section");

    }

});

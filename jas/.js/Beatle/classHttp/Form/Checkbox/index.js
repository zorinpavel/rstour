Beatle.classHttp_Form_Checkbox = JooS.Reflect(Beatle.Class, {

    DomEvent_onClick: function() {
        if(this.Parent.hasClassName("checked")) {
            this.SetUnChecked();
        } else {
            this.throwEvent("Event_HttpForm_Checkbox_CheckGroup");
            this.SetChecked();
        }

        if(this.extraData.Submiter) {
            if(!this.Submited)
                this.throwEvent("Event_HttpForm_Submit");
            this.Submited = false;
        } else
            return true;
    },

    SetChecked: function() {
        this.Parent.addClassName("checked");
        if(this.extraData.value)
            this.Input.value = this.extraData.value;
        if(this.Input)
            this.Input.checked = true;
    },

    SetUnChecked: function() {
        this.Parent.removeClassName("checked");
        if(this.extraData.value)
            this.Input.value = "";
        if(this.Input)
            this.Input.checked = false;
    },

    Listener_Event_HttpForm_Checkbox_UnCheckGroup: function() {
        this.SetUnChecked();
    },

    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.Parent = new JooS.Element(this.getElement().parentNode);
        this.Input = this.Parent.cssQueryStrict("input[name='" + this.extraData.name + "']")[0];

        this.attachEvent("click", this.DomEvent_onClick);

        this.Event_throwsConstructDefaultParent([
            "Event_HttpForm_Submit"
        ], "classHttp_Form");

        this.Event_throwsConstructDefaultParent([
            "Event_HttpForm_Checkbox_CheckGroup"
        ], "classHttp_Form_Checkbox_Group");

        this.Event_throwsConstructDefaultParent([
            "Event_HttpForm_Checkbox_UnCheckRadio"
        ], "classHttp_Form_Checkbox_Section");

        this.Event_listenerConstructDefaultParent([
            "Event_HttpForm_Checkbox_UnCheckRadio"
        ], "classHttp_Form_Checkbox_Section");

        this.Event_listenerConstructDefaultParent([
            "Event_HttpForm_Checkbox_UnCheckGroup", "Event_HttpForm_Checkbox_CheckGroup"
        ]);

    }

});

Beatle.classHttp_Form_Radio = JooS.Reflect(Beatle.classHttp_Form_Checkbox, {

    DomEvent_onClick: function() {
        if(this.Parent.hasClassName("checked")) {
            this.SetUnChecked();
        } else {
            this.throwEvent("Event_HttpForm_Checkbox_UnCheckRadio", this.extraData.group);
            this.SetChecked();
        }

        if(this.extraData.Submiter) {
            if(!this.Submited)
                this.throwEvent("Event_HttpForm_Submit");
            this.Submited = false;
        } else
            return true;
    },

    Listener_Event_HttpForm_Checkbox_UnCheckRadio: function() {
        this.SetUnChecked();
    },

    Listener_Event_HttpForm_Checkbox_CheckGroup: function() {
        this.throwEvent("Event_HttpForm_Checkbox_UnCheckRadio", this.extraData.group);
        this.SetChecked();
    }


});

Beatle.classHttp_Form_Textarea = JooS.Reflect(Beatle.classHttp_Form_Input, {

    defaultLength: 30,
    HtmlMode: false,

    Do: function() {
        return;
    },

    DOMEvent_onChange: function() {
        if(this.HtmlMode) {
            setTimeout(JooS.Closure.Function(function() {
                this.throwEvent("Event_classHttp_FormTextEditor_insWysiwyg", this.htmlElement.value);
            }, this), 500);
        }
    },

    Listener_Event_classHttp_FormTextEditor_setHtml: function() {
        if(this.HtmlMode) {
            this.setDisplay("none");
            this.HtmlMode = false;
        } else {
            this.setDisplay("block");
            this.HtmlMode = true;
        }
    },

    Listener_Event_classHttp_FormTextEditor_insHtml: function(Value) {
        var nValue = Value.replace(/<br>/gi, "\n");
        this.htmlElement.value = nValue;
    },

    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.attachEvent("blur", this.DOMEvent_onChange);

        this.Event_throwsConstructDefaultParent([
            "Event_classHttp_FormTextEditor_registerField", "Event_classHttp_FormTextEditor_insWysiwyg"
        ], "classHttp_Form_TextEditor");

        this.Event_listenerConstructDefaultParent([
            "Event_classHttp_FormTextEditor_setHtml", "Event_classHttp_FormTextEditor_insHtml"
        ], "classHttp_Form_TextEditor");

        this.throwEvent("Event_classHttp_FormTextEditor_registerField", this);
        this.throwEvent("Event_classHttp_FormTextEditor_insWysiwyg", this.htmlElement.value);
    }

});

Beatle.classHttp_Form_ErrorIcon = JooS.Reflect(Beatle.Class, {

  errorSrc: "/img/form-error.png", 
  okSrc: "/img/form-ok.png", 
  defaultSrc: "/img/sp.gif", 
  loaderSrc: "/img/loader.gif", 

  TemplateTop: "" + 
            " <div class=\"helper-tleft\"><div class=\"helper-tright\"><div class=\"helper-tcenter\"></div></div></div>" +
            " <div class=\"helper-body\">", 
  TemplateBottom: "" +
            "</div>" +
            " <div class=\"helper-bleft\"><div class=\"helper-bright\"><div class=\"helper-bcenter\"></div></div></div>" +
            " <div class=\"helper-pointer\"></div>", 

  Listener_Event_classHttp_FormInput_onCancelError: function(Data) {
    if(Data.Name == this.extraData.Name) {
      this.htmlElement.src = this.defaultSrc;
    }
  }, 
  
  Listener_Event_classHttp_FormInput_onError: function(Data) {
    if(Data.Name == this.extraData.Name) {
      this.htmlElement.src = this.errorSrc;
      this.BoxCreate(Data.ErrorText);
      this.attachEvent("mouseover", this.DOMEvent_onMouseOver);
    }
  }, 

  Listener_Event_classHttp_FormInput_onOk: function(Data) {
    if(Data.Name == this.extraData.Name) {
      this.htmlElement.src = this.okSrc;
      this.detachEvent("mouseover");
    }
  }, 

  Listener_Event_classHttp_FormInput_onAjax: function(Data) {
    if(Data.Name == this.extraData.Name) {
      this.htmlElement.src = this.loaderSrc;
      this.detachEvent("mouseover");
    }
  }, 

  BoxCreate: function(Body) {
    if(!this.Helper) {
      this.Helper = new JooS.Element(document.createElement("div"));
      this.Helper.htmlElement.className = "helper";
      document.body.appendChild(this.Helper.htmlElement);
      var Html = this.TemplateTop + Body + this.TemplateBottom;
      this.Helper.htmlElement.innerHTML = Html;
    } else {
      var Html = this.TemplateTop + Body + this.TemplateBottom;
      this.Helper.htmlElement.innerHTML = Html;
    }
/*
    this.BoxShow();
    clearTimeout(this.hideTimeout);
    this.hideTimeout = setTimeout(JooS.Closure.Function(function() {
      this.BoxHide();
    }, this), 1500);
*/
  }, 

  DOMEvent_onMouseOver: function() {
    if(this.Helper)
      this.BoxShow();
    clearTimeout(this.hideTimeout);
  }, 
  
  DOMEvent_onMouseOut: function() {
    clearTimeout(this.hideTimeout);
    this.hideTimeout = setTimeout(JooS.Closure.Function(function() {
      this.BoxHide();
    }, this), 500);
  }, 

  BoxHide: function() {
    if(this.Helper)
      this.Helper.setDisplay("none");
  }, 
  
  BoxShow: function() {
    var screen = this.getPagePos();
    var StartX = screen.left - (JooS.Browser.isIE ? 40 : 37);
    var StartY = screen.top + 30;
    this.Helper.setPos(StartX, StartY);
    this.Helper.setDisplay("block");
  }, 
  
  __constructor: function(Element, extraData) {
    this.__constructor.__parent(Element, extraData);

    this.attachEvent("mouseover", this.DOMEvent_onMouseOver);
    this.attachEvent("mouseout", this.DOMEvent_onMouseOut);

    this.Event_listenerConstructDefaultParent([
      "Event_classHttp_FormInput_onCancelError", "Event_classHttp_FormInput_onError", "Event_classHttp_FormInput_onOk", "Event_classHttp_FormInput_onAjax"
    ], "classHttp_Form");

  }

});

Beatle.classHttp_Form_Submiter = JooS.Reflect(Beatle.Class, {

    DomEvent_onClick: function() {
        this.throwEvent("Event_HttpForm_Submit");
    },

    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        if(this.extraData.onChange)
            this.attachEvent("change", this.DomEvent_onClick);
        else
            this.attachEvent("click", this.DomEvent_onClick);

        this.Event_throwsConstructDefaultParent([
            "Event_HttpForm_Submit"
        ], "classHttp_Form");

    }
});

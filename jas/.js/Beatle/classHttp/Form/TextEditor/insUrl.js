Beatle.classHttp_Form_TextEditor_insUrl = JooS.Reflect(Beatle.Class, {
  
  HtmlMode: true, 

  Listener_Event_classHttp_FormTextEditor_setHtml: function() {
    if(this.HtmlMode) {
      this.setOpacity(30);
      this.HtmlMode = false;
    } else {
      this.setOpacity(100);
      this.HtmlMode = true;
    }
  }, 

  DOMEvent_onClick: function() {
    if(href = prompt('������� ������', 'http://'))
      this.throwEvent("Event_classHttp_FormTextEditor_insUrl", href);
  }, 

  __constructor: function(Element, extraData) {
    this.__constructor.__parent(Element, extraData);

    this.attachEvent("click", this.DOMEvent_onClick);

    this.Event_throwsConstructDefaultParent([
      "Event_classHttp_FormTextEditor_insUrl"
    ], "classHttp_Form_TextEditor");

    this.Event_listenerConstructDefaultParent([
      "Event_classHttp_FormTextEditor_setHtml"
    ], "classHttp_Form_TextEditor");

  }
});

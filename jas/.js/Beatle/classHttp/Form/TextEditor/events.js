Beatle.registerEvent("Event_classHttp_FormTextEditor_registerField");
Beatle.registerEvent("Event_classHttp_FormTextEditor_insTag");
Beatle.registerEvent("Event_classHttp_FormTextEditor_insUrl");
Beatle.registerEvent("Event_classHttp_FormTextEditor_insText");
Beatle.registerEvent("Event_classHttp_FormTextEditor_insCore", function(s1, s2, s3) {
  return {
    s1: s1, 
    s2: s2, 
    s3: s3
  };
});

Beatle.registerEvent("Event_classHttp_FormTextEditor_setHtml");
Beatle.registerEvent("Event_classHttp_FormTextEditor_insWysiwyg", function(Value) {
    return Value;
});
Beatle.registerEvent("Event_classHttp_FormTextEditor_insHtml", function(Value) {
    return Value;
});

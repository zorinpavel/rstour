Beatle.classHttp_Form_TextEditor_insCut = JooS.Reflect(Beatle.Class, {

  HtmlMode: true, 

  Listener_Event_classHttp_FormTextEditor_setHtml: function() {
    if(this.HtmlMode) {
      this.setOpacity(30);
      this.HtmlMode = false;
    } else {
      this.setOpacity(100);
      this.HtmlMode = true;
    }
  }, 

  DOMEvent_onClick: function() {
    this.throwEvent("Event_classHttp_FormTextEditor_insText", "\n<cut><!--//--></cut>\n");
  }, 
  __constructor: function(Element, extraData) {
    this.__constructor.__parent(Element, extraData);

    this.attachEvent("click", this.DOMEvent_onClick);

    this.Event_throwsConstructDefaultParent([
      "Event_classHttp_FormTextEditor_insText"
    ], "classHttp_Form_TextEditor");

    this.Event_listenerConstructDefaultParent([
      "Event_classHttp_FormTextEditor_setHtml"
    ], "classHttp_Form_TextEditor");

  }
});

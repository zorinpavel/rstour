Beatle.classHttp_Form_TextEditor_Wysiwyg = JooS.Reflect(Beatle.Class, {

  TextMode: false, 

  Listener_Event_classHttp_FormTextEditor_setHtml: function() {
    if(this.TextMode) {
      this.setDisplay("none");
      this.TextMode = false;
    } else {
      this.setDisplay("block");
      this.TextMode = true;
    }
  }, 

  Listener_Event_classHttp_FormTextEditor_insWysiwyg: function(Value) {
    var nValue = Value.replace(/\n/gi, "<br />");
    this.FrameDoc.body.innerHTML = nValue;
  }, 

  DOMEvent_onChange: function() {
    if(this.Parent.TextMode) {
      setTimeout(JooS.Closure.Function(function() {
        this.Parent.throwEvent("Event_classHttp_FormTextEditor_insHtml", this.htmlElement.body.innerHTML);
      }, this), 500);
    }
  }, 

  __constructor: function(Element, extraData) {
    this.__constructor.__parent(Element, extraData);

    this.Event_throwsConstructDefaultParent([
      "Event_classHttp_FormTextEditor_insHtml"
    ], "classHttp_Form_TextEditor");

    this.Event_listenerConstructDefaultParent([
      "Event_classHttp_FormTextEditor_setHtml", "Event_classHttp_FormTextEditor_insWysiwyg"
    ], "classHttp_Form_TextEditor");

    this.FrameDoc = JooS.Browser.IE ? this.htmlElement.contentWindow.document : this.htmlElement.contentDocument;
    this.FrameDoc.designMode = "on";
    var Style = this.FrameDoc.createElement("link");
    Style.rel = "stylesheet";
    Style.href = "/jas/<? echo str_replace(".js", ".css", $JAS_FileVersion); ?>;wysiwyg.css";
    this.FrameDoc.head.appendChild(Style);

    this.FrameClass = new JooS.Element(this.FrameDoc);
    this.FrameClass.Parent = this;
    this.FrameClass.attachEvent("keyup", this.DOMEvent_onChange);
    this.FrameClass.attachEvent("blur", this.DOMEvent_onChange);

  }

});

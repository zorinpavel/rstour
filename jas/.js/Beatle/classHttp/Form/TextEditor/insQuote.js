Beatle.cForms_TextEditor_insQuote = JooS.Reflect(Beatle.Class, {

  DOMEvent_onClick: function() {
    var Data = {
    };

    var Self = this;
    JooS.Ajax.Get("/ajax.php", Data, function() {
     if(this.responseJS) {
       var insText = "<quote>" + this.responseJS.login + " �����-�:\r\n" + this.responseJS.Text + "</quote>";
     } else {
       var insText = "<quote></quote>";
     }
     Self.throwEvent("Event_cFormsTextEditor_insText", insText);
    });
  }, 

  __constructor: function(Element, extraData) {
    this.__constructor.__parent(Element, extraData);

    this.Event_throwsConstructDefaultParent([
      "Event_cFormsTextEditor_insText"
    ], "cForms_TextEditor");

    this.attachEvent("click", this.DOMEvent_onClick);
  }
});

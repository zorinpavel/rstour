Beatle.classHttp_Form_TextEditor_insHtml = JooS.Reflect(Beatle.Class, {
  
  DOMEvent_onClick: function() {
    this.throwEvent("Event_classHttp_FormTextEditor_setHtml");
  }, 

  __constructor: function(Element, extraData) {
    this.__constructor.__parent(Element, extraData);

    this.attachEvent("click", this.DOMEvent_onClick);

    this.Event_throwsConstructDefaultParent([
      "Event_classHttp_FormTextEditor_setHtml"
    ], "classHttp_Form_TextEditor");

  }
});

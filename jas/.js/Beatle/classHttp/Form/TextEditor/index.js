Beatle.classHttp_Form_TextEditor = JooS.Reflect(Beatle.Class, {

  Listener_Event_classHttp_FormTextEditor_registerField: function(insField) {
    this.insField = insField;

    this.brkL = "<";
    this.brkR = ">";
    this.insBeg = 1;
    this.defa = '';
    this.selted = '';
  }, 

  Listener_Event_classHttp_FormTextEditor_insTag: function(tag) {
    this.insTag(tag);
  }, 

  Listener_Event_classHttp_FormTextEditor_insUrl: function(url) {
    this.insUrl(url);
  }, 

  Listener_Event_classHttp_FormTextEditor_insText: function(text) {
    this.insPicCore(text);
  }, 

  Listener_Event_classHttp_FormTextEditor_insCore: function(Data) {
    this.insPicCore(Data.s1, Data.s2, Data.s3);
  }, 

  getInsText: function() {
    this.insField.htmlElement.focus();

    var s = this.insField.htmlElement.value;
    if (this.insField.htmlElement.selectionEnd==null) {
      this.ch = 0;
      if (document.selection && document.selection.createRange) {
        this.tR=document.selection.createRange();
        this.ch='character';
        this.tR1=document.body.createTextRange();
      }
      if (!this.ch || this.tR.parentElement && this.tR.parentElement()!=this.insField.htmlElement) {
        this.insPosL=this.insPosR=s.length;
      }
      else {
        this.insPosL=this.tR.text.length;
        this.insPosR=-1;

        if (this.insField.htmlElement.type=='textarea') {
          this.tR1.moveToElementText(this.insField.htmlElement);

          var dNR = 0;
          if (window.navigator.appName == "Microsoft Internet Explorer") {
            var tR1=this.tR1.duplicate();
            tR1.setEndPoint('StartToEnd', this.tR);
            var tR2=this.tR1.duplicate();
            tR2.setEndPoint('StartToStart', this.tR);

            if (tR1.text == tR2.text) {
              if (tR1.text == "") {
                this.insPosR = s.length;
              }
              else {
                var tR3 = tR1.duplicate();
                do {
                  tR3.moveStart('character',-1);
                  if (tR3.text.toString().charCodeAt(0) == 13)
                    dNR += 2;
                  else
                    break;
                } while (1);
              }
            }
          }

          this.tR.setEndPoint('StartToStart',this.tR1);
          if (this.insPosR == -1)
            this.insPosR=this.tR.text.length + dNR;
        }
        else {
          this.tR.moveStart('textedit',-1);
          this.insPosR=this.tR.text.length;
        }
        this.insPosL=this.insPosR - this.insPosL;
      }
    }
    else {
      this.insPosL=this.insField.htmlElement.selectionStart;
      this.insPosR=this.insField.htmlElement.selectionEnd;
      if (this.insBeg && self.opera && !this.insPosL && !this.insPosR) {
        this.insPosL=this.insPosR=s.length;
        this.insBeg=0;
      }
    }

    return s.substring(this.insPosL,this.insPosR);
  }, 

  insPicCore: function(s1, s2, s3) {
    var isPic=s2==null;
    var s=this.insField.htmlElement.value;
    var scrl=this.insField.htmlElement.scrollTop;

    this.insPosL = 0;
    this.insPosR = 0;

    var insText=this.getInsText();

    if ((isInSel=this.selted==insText)&&s3==3) {
      isInSel=insText.length;insText='';
    }
    if (document.all)
      this.insField.htmlElement.defaultValue=s;
    else
      this.defa=s;

    if (isPic && !(s3==2&&insText!='')) {
      s2=s1;s1=''; 
    } //for addressing&picture code
    this.insField.htmlElement.value=s.substring(0,this.insPosL)+s1+insText+s2+s.substring(this.insPosR,s.length);
    if(isInSel&&s3==3)
      this.insPosR-=isInSel;
    var insCursor=this.insPosR+s1.length+(isPic||this.insPosL!=this.insPosR?s2.length:0);
    var insCursorL=insCursor;
    if(s3==1) {
      insCursorL=this.insPosL+s1.length;insCursor=s1.length+this.insPosR;
    }
    var a1=s.substr(0,s3!=3?this.insPosR:this.insPosR+isInSel).match(/\r\n/g);

    if (document.body.createTextRange) {
      var TS = this;

      setTimeout(function() {
        var t=TS.insField.htmlElement.createTextRange();
        t.collapse();
        t.moveEnd(TS.ch,(insCursor-(a1?a1.length:0)));
        t.moveStart(TS.ch,(insCursorL-((a1=s3!=3?s.substr(0,s3==1?TS.insPosL:TS.insPosR).match(/\r\n/g):a1)?a1.length:0)));
        t.select();
      }, 1);
    }
    else {
      if (document.all)
        this.insField.htmlElement.focus();
      if (this.insField.htmlElement.selectionEnd!=null) {
        this.insField.htmlElement.selectionStart=insCursorL;this.insField.htmlElement.selectionEnd=insCursor+(document.all?1:0);

        var insField = this.insField.htmlElement;
        setTimeout(function() {
          insField.focus();
          if (document.all)
            insField.selectionEnd--;
        }, 50);
        if (document.all) {
          var tR=document.selection.createRange();
          if (insCursorL==insCursor)
            tR.collapse();
          tR.select();
        }
        else if (scrl>0)
          this.insField.htmlElement.scrollTop=scrl;
      }
    }
  }, 

  insPic: function(s1, s2, s3) {
    if (!document.all && this.selted=='' && s3==3) {
      s1=s2;
      s2=this.brkL+'/'+s2+this.brkR;
      s3='';
    }
    if (s3==3) {
      s1+=s2;
      s2='';
    }
    s1=this.brkL+s1+(s2==this.brkR?'=':this.brkR); //'[b]' or '[b='
    if (s3==2) {
      s1+=s2;
      s2='';
    }
    this.insPicCore(s1, s2, s3);
  }, 

  insTag: function(s, c) {   //'b','[/b]' | 'c[/b]
    this.insPic(s, (c ? c : '') + this.brkL + '/' + s + this.brkR + (c ? ', ' : ''), c ? 2 : null);
  }, 

  insTagSel: function(s) {   //'b', '[/b]', 1
    this.insPic(s, this.brkL+'/' + s + this.brkR, 1);
  }, 

  insTagArg: function(s) {  //'b', ']'
    this.insPic(s, this.brkR);
  }, 

  insBack: function() {
    with (this.insField.htmlElement) {
      var s=document.all ? value : this.defa;
      value=document.all ? defaultValue : this.defa;
      if (document.all)
        defaultValue=s;
      else
        this.defa=s;
    }
  }, 

  insUrl: function(href) {
    if (this.getInsText())
      this.insPicCore('<a href="' + href + '">', '</a>', 2);
    else
      this.insPicCore(href);
  }, 

  insCapt: function(s){  //'b]selection[/',b,3 /*����� ������� ��� ������*/
    this.insPic(s + this.brkR + (this.selted=(document.getSelection?(self.str?str:(document.all?(document.getSelection()
      ?document.getSelection():document.selection.createRange().text):getSelection()))
      :(document.selection?document.selection.createRange().text:'')))
      +this.brkL+'/',s,3);
  }, 

  Listener_Event_File_Action: function(Data) {
/*    var Url = Data.url.replace(/\/imagepreview\/(.*?)\//i, "/imagepreview/view/"); */
    var Url = "/imagepreview/view" + Data.url;
    this.insPicCore("\n\n<img src=\"" + Url + "\" />", "", 2);
  }, 

  __constructor: function(Element, extraData) {
    this.__constructor.__parent(Element, extraData);

    this.Event_listenerConstructDefaultSelf([
      "Event_classHttp_FormTextEditor_registerField", "Event_classHttp_FormTextEditor_insTag",
      "Event_classHttp_FormTextEditor_insUrl", "Event_classHttp_FormTextEditor_insText",
      "Event_classHttp_FormTextEditor_insCore"
    ]);

    this.Event_listenerConstructDefaultParent([
      "Event_File_Action"
    ], "Body");

  }

});

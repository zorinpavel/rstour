Beatle.cForms_TextEditor_insColor = JooS.Reflect(Beatle.Class, {

  DefaultColors: {
    "red" : "FF0000", 
    "green" : "00FF00", 
    "blue" : "0000FF"
  }, 

  DOMEvent_onClick: function() {
    if(!this.Picker)
      this.CreatePicker();
    this.OpenPicker();
  }, 

  CreatePicker: function() {
    if(this.Colors) {
      this.Picker = new JooS.Element(document.createElement("div"));
      this.Picker.getElement().className = "ColorPicker";
      var Self = this;
      for(i in this.Colors) {
        var A = document.createElement("a");
        A.className = "color";
        A.href = "#";
        A.onclick = function() {
          this.InsertColor();
          return false;
        };
        A.color = this.Colors[i];
        A.InsertColor = function() {
          Self.throwEvent("Event_cFormsTextEditor_insTag", this.color);
          Self.ClosePicker();
        };
        var span = document.createElement("span");
        span.className = this.Colors[i];
        A.appendChild(span);
        this.Picker.getElement().appendChild(A);
      }
      this.getElement().parentNode.appendChild(this.Picker.getElement());
    }
  }, 

  setPosition: function(Obj) {
    var Pos = this.getPos();
    var Size = this.getSize();
    var Left = Pos.left;
    Obj.setPos(Left, Pos.top + Size.height + 5);
  }, 

  OpenPicker: function() {
    this.setPosition(this.Picker);
    this.Picker.setDisplay("block");
  }, 

  ClosePicker: function() {
    this.Picker.setDisplay("none");
  }, 

  InsertColor: function(Color) {
    this.throwEvent("Event_cFormsTextEditor_insTag", Color);
    this.ClosePicker();
  }, 

  __constructor: function(Element, extraData) {
    this.__constructor.__parent(Element, extraData);
    this.extraData = extraData;

    this.Colors = this.getAttribute("colors").split(",");

    this.Event_throwsConstructDefaultParent([
      "Event_cFormsTextEditor_insTag"
    ], "cForms_TextEditor");

    this.attachEvent("click", this.DOMEvent_onClick);
  }
});

Beatle.cForms_TextEditor_insUser = JooS.Reflect(Beatle.Class, {

  DOMEvent_onClick: function() {
    debugger;
    if(!this.Picker)
      this.CreatePicker();
    this.OpenPicker();
  }, 

  CreatePicker: function() {
    this.Picker = new JooS.Element(document.createElement("div"));
    this.Picker.getElement().className = "UserPicker";
    var Self = this;
    for(i=0; i <= 6; i++) {
      var A = document.createElement("a");
      A.className = "user-lev";
      A.href = "#";
      A.onclick = function() {
        this.InsertUser();
        return false;
      };
      A.user = i;
      A.InsertUser = function() {
        Self.throwEvent("Event_cFormsTextEditor_insCore", "<user level='" + this.user + "'>", "</user>", 2);
        Self.ClosePicker();
      };
      var span = document.createElement("span");
      span.className = "lev-" + i;
      var lev = document.createTextNode(i);
      span.appendChild(lev);
      A.appendChild(span);
      this.Picker.getElement().appendChild(A);
    }
    this.getElement().parentNode.appendChild(this.Picker.getElement());
  }, 

  setPosition: function(Obj) {
    var Pos = this.getPos();
    var Size = this.getSize();
    var Left = Pos.left;
    Obj.setPos(Left, Pos.top + Size.height + 5);
  }, 

  OpenPicker: function() {
    this.setPosition(this.Picker);
    this.Picker.setDisplay("block");
  }, 

  ClosePicker: function() {
    this.Picker.setDisplay("none");
  }, 

  InsertColor: function(Color) {
    this.throwEvent("Event_cFormsTextEditor_insTag", Color);
    this.ClosePicker();
  }, 

  __constructor: function(Element, extraData) {
    this.__constructor.__parent(Element, extraData);
    this.extraData = extraData;

    this.Event_throwsConstructDefaultParent([
      "Event_cFormsTextEditor_insCore"
    ], "cForms_TextEditor");

    this.attachEvent("click", this.DOMEvent_onClick);
  }
});

Beatle.registerEvent("Event_classHttp_FormInput_onChange");
Beatle.registerEvent("Event_classHttp_FormInput_onError", function(Data) {
  return Data;
});
Beatle.registerEvent("Event_classHttp_FormInput_onCancelError", function(Data) {
  return Data;
});
Beatle.registerEvent("Event_classHttp_FormInput_onOk", function(Data) {
  return Data;
});
Beatle.registerEvent("Event_classHttp_FormInput_onAjax", function(Data) {
  return Data;
});
Beatle.registerEvent("Event_classHttp_FormInput_GetValue", function(Data) {
  return Data;
});
Beatle.registerEvent("Event_HttpForm_Submit");

Beatle.registerEvent("Event_HttpForm_Checkbox_UnCheckRadio", function(Data) {
    return Data;
});
Beatle.registerEvent("Event_HttpForm_Checkbox_CheckGroup");
Beatle.registerEvent("Event_HttpForm_Checkbox_UnCheckGroup");

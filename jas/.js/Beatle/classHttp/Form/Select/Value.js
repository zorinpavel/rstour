Beatle.classHttp_Form_Select_Value = JooS.Reflect(Beatle.Class, {

    Listener_Event_Select_Choose: function(Data) {
        this.getElement().value = Data.Key;
    },

    Listener_Event_Select_Value_Clear: function() {
        this.getElement().value = "";
    },

    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.Event_listenerConstructDefaultParent([
            "Event_Select_Choose", "Event_Select_Value_Clear"
        ], "classHttp_Form_Select");

    }
});

Beatle.classHttp_Form_Select_Addons_UpdateCart = JooS.Reflect(Beatle.Class, {

    Event_CallBack: function(Data) {
        this.throwEvent("Event_Cart_Price", this.extraData.Name, 0, Data.Key);
    },

    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.Event_throwsConstructDefaultParent([
            "Event_Cart_Price"
        ], "Cart");

    }

});

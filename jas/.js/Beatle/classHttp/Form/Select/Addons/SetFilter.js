Beatle.classHttp_Form_Select_Addons_SetFilter = JooS.Reflect(Beatle.Class, {

    Event_CallBack: function(Data) {
        var HrefSearch = window.location.search;
        var Href = window.location.pathname.replace(/[^/\\&\?]+\.\w{3,4}(?=([\?&].*$|$))/gi, "");

        if(Data.Location)
            Href = Data.Location;

        if(this.isArray(this.extraData.Name)) {
            for(var i in this.extraData.Name) {
                var Name = this.extraData.Name[i];
                Name = Name.replace("\[", "\\[");
                Name = Name.replace("\]", "\\]");

                var Replacement = new RegExp(Name + "=([\\w|\\.|\\-]*)", "gi");
                HrefSearch = HrefSearch.replace(Replacement, "");
            }
        } else {
            var Name = this.extraData.Name;
            Name = Name.replace("\[", "\\[");
            Name = Name.replace("\]", "\\]");

            var Replacement = new RegExp(Name + "=([\\w|\\.|\\-]*)", "gi");
            HrefSearch = HrefSearch.replace(Replacement, "");
        }

        Href = Href + HrefSearch;

        if(this.extraData.Remove) {
            for(i in this.extraData.Remove) {
                var Remove = new RegExp(this.extraData.Remove[i] + "=(\\w+|%|-|\\+)+", "gi");
                Href = Href.replace(Remove, "");
            }
        }

        if(this.extraData.Add) {
            for(i in this.extraData.Add) {
                var Add = new RegExp(i + "=([\\w|\\.|\\-]*)", "gi");
                Href = Href.replace(Add, "");
                Href = Href + "&" + i + "=" + this.extraData.Add[i];
            }
        }

        if(Data.Key && Data.KeyName) {
            // if(this.isArray(this.extraData.Name)) {
            //     for(var i in this.extraData.Name) {
            //         Href = Href + "&" + this.extraData.Name[i] + "=" + Data.Key;
            //     }
            // } else
            //     Href = Href + "&" + this.extraData.Name + "=" + Data.Key;

            Href = Href + "&" + Data.KeyName + "=" + Data.Key;
        } else if(Data.extraData.Name) {
            Href = Href + "&" + Data.extraData.Name + "=" + Data.Key;
        }

        Href = Href.replace(/^&/gi, "?");
        Href = Href.replace(/&{2,}/gi, "&");
        Href = Href.replace(/&$/gi, "");
        Href = Href.replace(/\?$/gi, "");
        Href = Href.replace(/\?&/gi, "?");
        // window.location.href = window.location.pathname + Href;
        window.location.href = Href;
    },

    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);
    }

});

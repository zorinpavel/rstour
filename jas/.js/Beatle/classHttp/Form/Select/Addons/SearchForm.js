Beatle.classHttp_Form_Select_Addons_SearchForm = JooS.Reflect(Beatle.Class, {

    Event_CallBack: function(Data) {
        this.Element = new JooS.Element(this.getElementById("SearchForm"));
        var Self = this;
        rData = {
            c: "classHTML",
            m: "SearchForm"
        };
        for(var i in this.extraData) {
            rData[i] = this.extraData[i];
        }
        rData[this.extraData.Name] = Data.Key;

        this.Element.setOpacity(50);
        Beatle.unloadComponents(this.Element);

        JooS.Ajax.Get("/ajax.php", rData, function() {
            Self.Element.getElement().innerHTML = this.responseText;
            Beatle.loadComponents(Self.Element);
            Self.Element.setOpacity(100);
        }, this.Element, "opacity");
    },

    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);
    }

});

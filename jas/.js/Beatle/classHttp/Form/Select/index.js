Beatle.classHttp_Form_Select = JooS.Reflect(Beatle.Class, {

    Listener_Event_Select_Choose: function(Data) {
        if(this.extraData.CallBack) {
            if(!this.Div) {
                this.Div = document.createElement("div");
                this.Div.className = "js classHttp_Form_Select_Addons_" + this.extraData.CallBack;
                this.Div.onclick = (function(extraData) {
                    return function() {
                        return extraData;
                    }
                })(this.extraData);
                document.body.appendChild(this.Div);
                Beatle.loadComponents(this.Div);
            }
            //eval("var CallBack = new Beatle.classHttp_Form_Select_Addons_" + this.extraData.CallBack + "(" + Div + ");");
            //CallBack.extraData = this.extraData;

            var CallBack = Beatle.getObjectByElement(this.Div);
            this.callbackTimeout = setTimeout(JooS.Closure.Function(function() {
                CallBack.Event_CallBack(Data)
            }, this), 50);
        }
    },


    Listener_Event_Select_Rebuild: function(Data) {
        if(Data.Name == this.extraData.Name) {
            this.throwEvent("Event_Select_Choose", "", this.extraData.Default);
            this.throwEvent("Event_Select_Options_Remove");

            var Ul = this.cssQueryStrict("DIV UL.select-values")[0];
            for(var i in Data.Data) {
                var Li = document.createElement("li");
                Li.className = "js classHttp_Form_Select_Option";
                Li.onclick = (function(Key, Value) {
                    return function() {
                        return {
                            Key: Key,
                            Value: Value
                        }
                    }
                })(i, Data.Data[i]);
                Li.appendChild(document.createTextNode(Data.Data[i]));
                Ul.appendChild(Li);
            }
            Beatle.loadComponents(Ul);
        }
    },


    Listener_Event_Select_RebuildAll: function(Data) {
        this.throwEvent("Event_Select_Options_Remove");

        var Ul = this.cssQuerySelf("UL.select-values")[0];
        for(var i in Data) {
            var Li = document.createElement("li");
            Li.className = "js classHttp_Form_Select_Option";
            Li.setAttribute("data-level", Data[i].Level);
            Li.onclick = (function(Key, Value, KeyName) {
                return function() {
                    return {
                        Key: Key,
                        Value: Value,
                        KeyName: KeyName
                    }
                }
            })(Data[i].Value, Data[i].Name, Data[i].Key);
            Li.appendChild(document.createTextNode(Data[i].Name));
            Ul.appendChild(Li);
        }
        Beatle.loadComponents(Ul);
    },


    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.Event_throwsConstructDefaultSelf([
            "Event_Select_Options_Remove", "Event_Select_Choose"
        ]);

        this.Event_listenerConstructDefaultSelf([
            "Event_Select_Choose", "Event_Select_RebuildAll"
        ]);

        this.Event_listenerConstructDefaultParent([
            "Event_Select_Rebuild"
        ], "Body");

    }

});

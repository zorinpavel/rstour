Beatle.registerEvent("Event_Select_Expose", function(Key) {
  return Key;
});
Beatle.registerEvent("Event_Select_Conceal");
Beatle.registerEvent("Event_Select_CallBack", function(Name) {
  return Name;
});
Beatle.registerEvent("Event_Select_Choose", function(Key, Value, KeyName) {
  return {
    Key: Key,
    Value: Value,
    KeyName: KeyName,
  };
});
Beatle.registerEvent("Event_Select_Grow", function(Key) {
  return Key;
});
Beatle.registerEvent("Event_Select_Rebuild", function(Name, Data) {
  return {
      Name: Name,
      Data: Data
  };
});
Beatle.registerEvent("Event_Select_RebuildAll", function(Data) {
  return Data
});
Beatle.registerEvent("Event_Select_Options_Remove");
Beatle.registerEvent("Event_Select_Value_Clear");

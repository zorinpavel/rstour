Beatle.classHttp_Form_Select_Cancel = JooS.Reflect(Beatle.Class, {

    DomEvent_onClick: function() {
        this.throwEvent("Event_Select_Choose", 0, "&nbsp;");
    },

    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.attachEvent("click", this.DomEvent_onClick);

        this.Event_throwsConstructDefaultParent([
            "Event_Select_Choose"
        ], "classHttp_Form_Select");

    }
});

Beatle.classHttp_Form_Select_Option = JooS.Reflect(Beatle.Class, {

    DomEvent_onClick: function() {
        if(!this.extraData.Child) {
            this.throwEvent("Event_Select_Choose", this.extraData.Key, this.extraData.Value, this.extraData.KeyName);
            this.throwEvent("Event_Select_Conceal");
        } else {
            this.throwEvent("Event_Select_Grow", this.extraData);
            this.addClassName("Down");
        }
    },


    Listener_Event_Select_Grow: function(Data) {
        if(this.extraData.Parent == Data.Child || this.extraData.Child == Data.Child)
            this.addClassName("Opened");
        else
            this.removeClassName("Opened");
        if(this.extraData.Child != Data.Child && this.extraData.Child != Data.Parent)
            this.removeClassName("Down");
    },


    Listener_Event_Select_Options_Remove: function() {
        this.Component_Remove();
    },


    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.attachEvent("click", this.DomEvent_onClick);

        this.Event_throwsConstructDefaultParent([
            "Event_Select_Conceal", "Event_Select_Choose"
        ], "classHttp_Form_Select");

        this.Event_throwsConstructDefaultParent([
            "Event_Select_Grow"
        ], "classHttp_Form_Select");

        this.Event_listenerConstructDefaultParent([
            "Event_Select_Grow", "Event_Select_Options_Remove"
        ], "classHttp_Form_Select");

    }
});

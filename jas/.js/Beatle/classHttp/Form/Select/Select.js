Beatle.classHttp_Form_Select_Select = (function() {

    JooS.DocumentDOM.concealTimeout = false;

    var attachDocumentClick = (function() {
        return function(select) {
            JooS.DocumentDOM.oldSelect = JooS.DocumentDOM.currentSelect;
            JooS.DocumentDOM.currentSelect = select;
            JooS.DocumentDOM.concealTimeout = setTimeout(function() {
                JooS.DocumentDOM.concealTimeout = false;
                if (!JooS.DocumentDOM.oldSelect)
                    JooS.DocumentDOM.oldSelect = JooS.DocumentDOM.currentSelect;
            }, 50);
        }
    })();

    JooS.DocumentDOM.attachEvent("click", function() {
        if(this.oldSelect && this.currentSelect) {
            if(this.oldSelect != this.currentSelect) {
                this.oldSelect.throwEvent("Event_Select_Conceal");
                this.oldSelect = this.currentSelect;
            } else if(!this.concealTimeout) {
                this.currentSelect.throwEvent("Event_Select_Conceal");
            }
        }
        return true;
    });

    return JooS.Reflect(Beatle.Class, {

        Listener_Event_Select_Expose: function(Position) {
            // if(this.getSize().width + Position.left + 5 > document.body.clientWidth) {
            //     Left = document.body.clientWidth - this.getSize().width - 20;
            // } else {
            //     Left = Position.left - 2;
            // }
            // this.setPos(Left, Position.top + Position.height - 1);
            this.setDisplay("block");
            attachDocumentClick(this);
        },

        Listener_Event_Select_Grow: function() {
            attachDocumentClick(this);
        },

        Listener_Event_Select_Conceal: function() {
            this.setDisplay("none");
        },

        __constructor: function(Element, extraData) {
            this.__constructor.__parent(Element, extraData);

            this.Event_throwsConstructDefaultParent([
                "Event_Select_Conceal"
            ], "classHttp_Form_Select");

            this.Event_listenerConstructDefaultParent([
                "Event_Select_Expose", "Event_Select_Conceal", "Event_Select_Grow"
            ], "classHttp_Form_Select");

        }
    });

})();

Beatle.classHttp_Form_Select_Current = JooS.Reflect(Beatle.Class, {

    DomEvent_onClick: function() {
        this.throwEvent("Event_Select_Expose", this.Position);
    },

    Listener_Event_Select_Choose: function(Data) {
        this.Label.innerHTML = Data.Value;
    },

    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.Label = this.cssQueryStrict("div")[0];
        this.attachEvent("click", this.DomEvent_onClick);

        this.Position = this.getPos();

        var Size = this.getSize();
        this.Position.width = Size.width;
        this.Position.height = Size.height;

        this.Event_throwsConstructDefaultParent([
            "Event_Select_Expose"
        ], "classHttp_Form_Select");

        this.Event_listenerConstructDefaultParent([
            "Event_Select_Choose"
        ], "classHttp_Form_Select");

    }
});

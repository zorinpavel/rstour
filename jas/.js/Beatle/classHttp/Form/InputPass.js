Beatle.classHttp_Form_InputPass = JooS.Reflect(Beatle.classHttp_Form_Input, {

    Do: function() {
        if(this.extraData.field) {
            var Data = this.throwEvent("Event_classHttp_FormInput_GetValue", this.extraData.field);
            if(this.getInput_Value() != Data.Value) {
                this.throwEvent("Event_classHttp_FormInput_onError", {
                    Name: this.inputName,
                    ErrorText: "Пароли не совпадают"
                });
            } else {
                this.throwEvent("Event_classHttp_FormInput_onOk", { Name : this.inputName });
            }
        }
    }

});

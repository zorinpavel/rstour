Beatle.registerEvent("Event_Range_GetThumbSize");
Beatle.registerEvent("Event_Range_GetRangeSize");
Beatle.registerEvent("Event_Range_SetHighlightLeft", function(dX) {
    return dX;
});
Beatle.registerEvent("Event_Range_SetHighlightWidth", function(dX) {
    return dX;
});
Beatle.registerEvent("Event_Range_SetMax", function(MinMax, Left) {
    return {
        MinMax: MinMax,
        Left: Left
    };
});
Beatle.registerEvent("Event_Range_DragStop");
Beatle.registerEvent("Event_Range_GetValues");
Beatle.registerEvent("Event_Range_SetValuesMax", function(Value) {
    return Value;
});
Beatle.registerEvent("Event_Range_SetValuesMin", function(Value) {
    return Value;
});

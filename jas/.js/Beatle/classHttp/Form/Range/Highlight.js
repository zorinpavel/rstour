Beatle.classHttp_Form_Range_Highlight = JooS.Reflect(Beatle.Class, {

    Listener_Event_Range_SetHighlightWidth: function(dX) {
        this.setStyle("width", this.Width + dX + 'px');
    },

    Listener_Event_Range_SetHighlightLeft: function(dX) {
        this.setStyle("left", this.Left + dX + 'px');
        this.setStyle("width", this.Width - dX + 'px');
    },

    Listener_Event_Range_DragStop: function() {
        this.Width = parseInt(this.getStyle("width"));
        this.Left = parseInt(this.getStyle("left"));
    },

    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.Event_throwsConstructDefaultParent([
            "Event_Range_GetThumbSize", "Event_Range_GetRangeSize"
        ], "classHttp_Form_Range");

        this.Event_listenerConstructDefaultParent([
            "Event_Range_SetHighlightLeft", "Event_Range_SetHighlightWidth", "Event_Range_SetHighlightLeft", "Event_Range_DragStop"
        ], "classHttp_Form_Range");

        var Self = this;
        JooS.Startup(function() {
                Self.Timeout = setTimeout(JooS.Closure.Function(function() {
                    Self.ThumbSize = Self.throwEvent("Event_Range_GetThumbSize");
                    Self.RangeSize = Self.throwEvent("Event_Range_GetRangeSize");
                    Self.setStyle("left", Self.ThumbSize.width / 2 + 'px');
                    Self.Left = Self.ThumbSize.width / 2;
                    Self.setStyle("width", Self.RangeSize.width - Self.ThumbSize.width + 'px');
                    Self.Width = Self.RangeSize.width - Self.ThumbSize.width;
                }, Self), 100);
            }
        );
    }

});

Beatle.classHttp_Form_Range_Thumb = JooS.Reflect(Beatle.Class, {

    DragEvent_onMove: function(dX, dY) {
        if(this.Left + dX <= this.MaxLeft && this.Left + dX >= this.MinLeft) {
            this.setStyle("left", this.Left + dX + 'px');
            if(this.extraData.max) {
                this.throwEvent("Event_Range_SetHighlightWidth", dX);
                this.htmlElement.innerHTML = this.Values.Max + dX * this.Step;
                this.throwEvent("Event_Range_SetValuesMax", this.Values.Max + dX * this.Step);
            } else if(this.extraData.min) {
                this.throwEvent("Event_Range_SetHighlightLeft", dX);
                this.htmlElement.innerHTML = this.Values.Min + dX * this.Step;
                this.throwEvent("Event_Range_SetValuesMin", this.Values.Min + dX * this.Step);
            }
            this.throwEvent("Event_Range_SetMax", this.extraData.max, this.Left + dX);
        }
    },

    DragEvent_onStop: function() {
        this.Left = parseInt(this.getStyle("left"));
        this.Values = this.throwEvent("Event_Range_GetValues");
        this.throwEvent("Event_Range_DragStop");
        this.throwEvent("Event_HttpForm_Submit");
    },

    Listener_Event_Range_GetThumbSize: function() {
        throw this.Size;
    },

    Listener_Event_Range_SetMax: function(Data) {
        if(Data.MinMax && this.extraData.min)
            this.MaxLeft = Data.Left - this.Size.width;
        else if(!Data.MinMax && this.extraData.max)
            this.MinLeft = Data.Left + this.Size.width;
    },

    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.Size = this.getSize();

        this.Event_throwsConstructDefaultParent([
            "Event_HttpForm_Submit"
        ], "classHttp_Form");

        this.Event_listenerConstructDefaultParent([
            "Event_Range_GetThumbSize", "Event_Range_SetMax"
        ], "classHttp_Form_Range");

        this.Event_throwsConstructDefaultParent([
            "Event_Range_GetRangeSize", "Event_Range_SetHighlightWidth", "Event_Range_SetHighlightLeft",
            "Event_Range_SetMax", "Event_Range_DragStop", "Event_Range_GetValues",
            "Event_Range_SetValuesMax", "Event_Range_SetValuesMin"
        ], "classHttp_Form_Range");


        var Self = this;
        JooS.Startup(function () {
                Self.Timeout = setTimeout(JooS.Closure.Function(function () {
                    Self.Values = Self.throwEvent("Event_Range_GetValues");
                    Self.RangeSize = Self.throwEvent("Event_Range_GetRangeSize");
                    if(Self.extraData.max) {
                        Self.htmlElement.innerHTML = Self.Values.Max;
                        Self.setStyle("left", Self.RangeSize.width - Self.Size.width + 'px');
                        Self.MaxLeft = Self.Left = Self.RangeSize.width - Self.Size.width;
                        Self.MinLeft = Self.Size.width;
                    } else if(Self.extraData.min) {
                        Self.htmlElement.innerHTML = Self.Values.Min;
                        Self.MaxLeft = Self.RangeSize.width - Self.Size.width * 2;
                        Self.MinLeft = 0;
                        Self.Left = 0;
                    }

                    Self.Step = (Self.Values.Max - Self.Values.Min) / Self.RangeSize.width;
                    this.Step = Math.ceil(parseInt(this.Step.toFixed()) / 100) * 100;
                }, Self), 100);
            }
        );

        this.attachEvent("mousedown", this.DragAction_Start);

    }

});
JooS.Plugin(Beatle.classHttp_Form_Range_Thumb, JooS.Drag);

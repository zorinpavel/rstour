Beatle.classHttp_Form_Range = JooS.Reflect(Beatle.Class, {

    Listener_Event_Range_GetRangeSize: function() {
        throw this.getSize();
    },

    Listener_Event_Range_GetValues: function() {
        throw {
            Min: parseInt(this.RangeMin.value),
            Max: parseInt(this.RangeMax.value)
        };
    },

    Listener_Event_Range_SetValuesMax: function(Value) {
        this.RangeMax.value = Value;
    },

    Listener_Event_Range_SetValuesMin: function(Value) {
        this.RangeMin.value = Value;
    },

    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.Event_throwsConstructDefaultSelf([
            "Event_Range_GetThumbSize"
        ]);

        this.Event_listenerConstructDefaultSelf([
            "Event_Range_GetRangeSize", "Event_Range_GetValues", "Event_Range_SetValuesMax", "Event_Range_SetValuesMin"
        ]);

        //var Self = this;
        //JooS.Startup(function() {
        //        Self.Timeout = setTimeout(JooS.Closure.Function(function() {
        //            Self.throwEvent("Event_Range_GetThumbSize");
        //        }, Self), 100);
        //    }
        //);

        this.RangeMin = document.getElementById("range-min");
        this.RangeMax = document.getElementById("range-max");

    }

});

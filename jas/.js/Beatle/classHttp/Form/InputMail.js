Beatle.classHttp_Form_InputMail = JooS.Reflect(Beatle.classHttp_Form_Input, {

    Do: function() {
        var mailStr = /[0-9a-z_]+@[0-9a-z_^.-]+\.[a-z]{2,3}/i;
        if(!mailStr.test(this.getInput_Value()) && !this.Empty) {
            this.throwEvent("Event_classHttp_FormInput_onError", {
                Name: this.inputName,
                ErrorText: "Не верно введен почтовый яшик"
            });
        } else {
            if(this.extraData.c) {
                var Self = this;

                var Data = {};
                if(this.extraData.c)
                    Data.c = this.extraData.c;
                if(this.extraData.m)
                    Data.m = this.extraData.m;
                if(this.extraData.s)
                    Data.s = this.extraData.s;
                if(this.extraData.a)
                    Data.a = this.extraData.a;
                Data.v = this.getInput_Value();

                JooS.Ajax.Get("/ajax.php", Data, function() {
                    Self.throwEvent("Event_classHttp_FormInput_onAjax", { Name: Self.inputName });
                    if(this.responseJS) {
                        if(this.responseJS.Error) {
                            Self.throwEvent("Event_classHttp_FormInput_onError", {
                                Name: Self.inputName,
                                ErrorText: this.responseJS.Error
                            });
                        } else {
                            Self.throwEvent("Event_classHttp_FormInput_onOk", { Name: Self.inputName });
                        }
                    }
                });
            } else {
                this.throwEvent("Event_classHttp_FormInput_onOk", { Name : this.inputName });
            }
        }
    }

});

Beatle.classHttp_Image_View = JooS.Reflect(Beatle.Class, {

    DomEvent_onMouseOver: function(event) {
    },

    DomEvent_onClick: function(event) {
        if(this.Next(event))
            this.throwEvent("Event_classHttp_Image_Next", this.Key);
        else
            this.throwEvent("Event_classHttp_Image_Prev", this.Key);
    },


    Next: function(event) {
        this.Position = this.getPagePos();
        this.Size = this.getSize();

        var dx, dy;
        if(JooS.Browser.isIE) {
            dx = event.event.offsetX;
            dy = event.event.offsetY;
            if( dx >= 0 && dx <= (this.Size.width/2) && dy >= 0 || dy <= this.Size.height ) {
                return true;
            }
        } else {
            dx = event.event.clientX;
            dy = event.event.clientY;
            if( dx >= this.Position.left && dx <= this.Position.left + (this.Size.width/2) ) {
                return false;
            }
        }
        return true;
    },


    DomEvent_onLoad: function() {
        this.throwEvent("Event_classHttp_SetPosition", true);
        this.setOpacity(100);
    },


    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.attachEvent("load", this.DomEvent_onLoad);
//        this.attachEvent("click", this.DomEvent_onClick);
//        this.attachEvent("mouseover", this.DomEvent_onMouseOver);

//      this.Target = this.getAttribute("target") ? this.getAttribute("target") : this.extraData.target ? this.extraData.target : "_new";
        this.Key = this.extraData.Key;

        this.Event_throwsConstructDefaultParent([
            "Event_classHttp_Image_Next", "Event_classHttp_Image_Prev", "Event_classHttp_SetPosition"
        ], "Body");

    }

});

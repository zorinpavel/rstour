Beatle.classHttp_Image_Preview = JooS.Reflect(Beatle.classHttp, {

    DomEvent_onClick: function() {
        this.throwEvent("Event_classHttp_Image_Open", this.Key);
    },


    ImageOpen: function() {
        var Element = this.getElementById(this.Target);
        if(!Element) {
            this.throwEvent("Event_classHttp_WindowClose");
            Element = this.createElem();
            if(Element.parentNode.id.indexOf("win_") > -1) {
                this.throwEvent("Event_classHttp_SetPosition", true);
            }
        }

        Beatle.unloadComponents(Element);
        Element.innerHTML = "";

        Element.appendChild(this.Loader);
        var JooSElement = new JooS.Element(Element);

        var oImage = new Image();
        oImage.src = this.extraData.src;
        oImage.className = "js classHttp_Image_View";
        oImage.onclick = (function(key) {
            return function() {
                return {
                    Key: key
                }
            }
        })(this.Key);

        this.Interval = setInterval(JooS.Closure.Function(function() {
            if(this.isLoaded(oImage)) {
                Element.removeChild(this.Loader);

                var Href = this.getAttribute("href");
                if(Href) {
                    var A = document.createElement("A");
                    A.href = Href;
                    A.target = "_blank";
                    A.appendChild(oImage);
                    Element.appendChild(A);
                } else
                    Element.appendChild(oImage);

                Beatle.loadComponents(Element);

                setTimeout(JooS.Closure.Function(function() {
                    oImage.style.opacity = 1;
                    if(Element.parentNode.id.indexOf("win_") > -1) {
                        this.throwEvent("Event_classHttp_SetPosition", true);
                    }
                }, this), 100);
            }
        }, this), 100);

        if(this.Navigation) {
            var NavigationRight = document.createElement("A");
            NavigationRight.className = "js classHttp_Image_Navigation navigation-right";
            NavigationRight.href = "#";
            NavigationRight.onclick = (function (key) {
                return function () {
                    return {
                        navigation: 'right',
                        Key: key
                    }
                }
            })(this.Key);

            var NavigationLeft = document.createElement("A");
            NavigationLeft.href = "#";
            NavigationLeft.className = "js classHttp_Image_Navigation navigation-left";
            NavigationLeft.onclick = (function (key) {
                return function () {
                    return {
                        navigation: 'left',
                        Key: key
                    }
                }
            })(this.Key);
            Element.appendChild(NavigationRight);
            Element.appendChild(NavigationLeft);
        }

    },


    isLoaded: function(oImage) {
        if(oImage.complete) {
            clearInterval(this.Interval);
            return true;
        }
        return false;
    },


    Listener_Event_classHttp_Image_Open: function(Key) {
        if(Key == this.Key) {
            this.ImageOpen();
            this.addClassName("selected");
        } else
            this.removeClassName("selected");
    },


    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.Event_throwsConstructDefaultParent([
            "Event_classHttp_Image_Register", "Event_classHttp_Image_Open"
        ], "classHttp_Image");

        this.Event_throwsConstructDefaultParent([
            "Event_classHttp_WindowClose"
        ], "Body");

        this.Event_listenerConstructDefaultParent([
            "Event_classHttp_Image_Open"
        ], "classHttp_Image");

        this.throwEvent("Event_classHttp_Image_Register", this);

        this.Key = this.extraData.Key;
        this.Navigation = this.extraData.navigation;
        this.extraData.src = this.extraData.src || this.getAttribute("href");

        var Loader = this.extraData.loader || 'loader';
        this.Loader = new Image();
        this.Loader.src = "/img/" + Loader + ".gif";
        this.Loader.id = Loader;

    }

});

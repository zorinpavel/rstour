Beatle.classHttp_Image_Navigation = JooS.Reflect(Beatle.Class, {

    DomEvent_onClick: function(event) {
        if(this.extraData.navigation == "right")
            this.throwEvent("Event_classHttp_Image_Next", this.Key);
        else
            this.throwEvent("Event_classHttp_Image_Prev", this.Key);
    },

    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.attachEvent("click", this.DomEvent_onClick);

        this.Key = this.extraData.Key;

        this.Event_throwsConstructDefaultParent([
            "Event_classHttp_Image_Next", "Event_classHttp_Image_Prev", "Event_classHttp_SetPosition"
        ], "Body");

    }

});

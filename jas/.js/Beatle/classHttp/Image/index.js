Beatle.classHttp_Image = JooS.Reflect(Beatle.Class, {

    Listener_Event_classHttp_Image_Register: function(Object) {
        this.Images.push(Object);
    },


    Listener_Event_classHttp_Image_Next: function(Key) {
        for(var i = 0; i < this.Images.length; i++) {
            if(this.Images[i].Key == Key) {
                if(this.Images[i + 1])
                    this.throwEvent("Event_classHttp_Image_Open", this.Images[i + 1].Key);
                else
                    this.throwEvent("Event_classHttp_Image_Open", this.Images[0].Key);
            }
        }
    },


    Listener_Event_classHttp_Image_Prev: function(Key) {
        for(var i = 0; i < this.Images.length; i++) {
            if(this.Images[i].Key == Key) {
                if(this.Images[i - 1])
                    this.throwEvent("Event_classHttp_Image_Open", this.Images[i - 1].Key);
                else
                    this.throwEvent("Event_classHttp_Image_Open", this.Images[this.Images.length - 1].Key);
            }
        }
    },


    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.Images = [];

        this.Event_listenerConstructDefaultSelf([
            "Event_classHttp_Image_Register"
        ]);

        this.Event_listenerConstructDefaultParent([
            "Event_classHttp_Image_Next", "Event_classHttp_Image_Prev"
        ], "Body");

        this.Event_throwsConstructDefaultSelf([
            "Event_classHttp_Image_Open"
        ]);

    }

});

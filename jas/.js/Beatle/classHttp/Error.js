Beatle.classHttp_Error = JooS.Reflect(Beatle.Class, {

    defaultWidth: "300px", 

    createClose: function(Div) {
      var Close = document.createElement("button");
      Close.className = "js classHttp_Close floatClose";
      Close.id = "floatClose";
      Close.appendChild(document.createTextNode("OK"))
      Div.appendChild(Close);
      Beatle.loadComponents(Close);
    }, 

    createBack: function() {
      var Back = document.createElement("div");
      Back.className = "js classHttp_Back floatBack";
      Back.id = "floatBack";
      document.body.appendChild(Back);
      Beatle.loadComponents(Back);
    }, 

    DomEvent_onLoad: function() {
      var Div = document.createElement("div");
      Div.className= "js classHttp_Window floatDivFixed " + this.extraData.classname;
      Div.id = "win_Error";
      Div.style.width = this.Width;
      Div.style.visibility = "hidden";

      this.createBack();
      this.createClose(Div);

      var TextDiv = document.createElement("div");
      TextDiv.className = "floatDivContent clear";

      if(this.extraData.title) {
        var Title = document.createElement("div");
        Title.className = "floatTitle";
        Title.appendChild(document.createTextNode(this.extraData.title));
        TextDiv.appendChild(Title);
      }

      var Inner = document.createElement("div");
      Inner.innerHTML = this.getElement().innerHTML;

      TextDiv.appendChild(Inner);
      Div.appendChild(TextDiv);

      document.body.appendChild(Div);

      Beatle.loadComponents(Div);

      this.Timeout = setTimeout(JooS.Closure.Function(function() {
        this.throwEvent("Event_classHttp_SetPosition", true, this.Size, this.Position);
      }, this), 300);

    }, 

    __constructor: function(Element, extraData) {
      this.__constructor.__parent(Element, extraData);

      this.Size = this.getSize();
      this.Position = this.getPagePos();
      this.Width = this.extraData.width || this.defaultWidth;
      
      var Self = this;
      JooS.Startup(function() {
          Self.DomEvent_onLoad();
        }
      );

      this.Event_listenerConstructDefaultParent([
        "Event_classHttp_WindowClose", "Event_classHttp_SetPosition"
      ], "Body");

      this.Event_throwsConstructDefaultParent([
        "Event_classHttp_SetPosition"
      ], "Body");

    }

});

Beatle.classHttp_Back = JooS.Reflect(Beatle.Class, {

  DOMEvent_onClick: function() {
    this.throwEvent("Event_classHttp_WindowClose");
  }, 

  Listener_Event_classHttp_WindowClose: function() {
    this.Component_Remove();
  }, 

  __constructor: function(Element, extraData) {
    this.__constructor.__parent(Element, extraData);

    this.attachEvent("click", this.DOMEvent_onClick);

    this.Event_throwsConstructDefaultParent([
     "Event_classHttp_WindowClose"
    ], "Body");

    this.Event_listenerConstructDefaultParent([
      "Event_classHttp_WindowClose"
    ], "Body");

  }

});

Beatle.AddTraveler = JooS.Reflect(Beatle.Class, {

    DomEvent_OnClick: function() {
        if(!this.Template)
            this.Template = this.throwEvent("Event_Template_Find", this.extraData.templateName);

        this.extraData.templateData.i++;
        var Template = this.throwEvent("Event_Template_Temp2Html", this.Template.templateBody, this.extraData.templateData);
        var newElement = document.createElement("div");
        newElement.innerHTML = Template.Html;
        this.htmlElement.parentNode.insertBefore(newElement, this.htmlElement);
        Beatle.loadComponents(newElement);
    },

    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.attachEvent("click", this.DomEvent_OnClick);

        this.Event_throwsConstructDefaultParent([
            "Event_Template_Find", "Event_Template_Temp2Html"
        ], "Body");

        var Self = this;
        JooS.Startup(function() {
                Self.Timeout = setTimeout(JooS.Closure.Function(function() {
                    Self.Template = Self.throwEvent("Event_Template_Find", Self.extraData.templateName);
                }, Self), 500);
            }
        );
    }

});

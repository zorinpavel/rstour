Beatle.Comments_More = JooS.Reflect(Beatle.Class, {

    Loading: false,

    DomEvent_onClick: function() {
        if(!this.Loading) {
            this.Loading = true;
            this.Part++;
            this.setOpacity(50);

            var Data = {
                c: 'classComment',
                m: 'Comments',
                part: this.Part,
                ccn: this.extraData.ccn,
                ci: this.extraData.ci,
                debug: 1
            };

            var Self = this;
            JooS.Ajax.Get("/ajax.php", Data, function() {
                if(!this.responseJS.Error) {
                    var UL = document.createElement("ul");
                    UL.className = "comments-list children";
                    UL.innerHTML = this.responseText;

                    var Comments = Self.Component_getParentObject("Comments");
                    var NextUL = Comments.cssQueryStrict("UL")[0];
                    Comments.getElement().insertBefore(UL, NextUL);

                    Beatle.loadComponents(UL);

                    if(this.responseJS.QuantityLeft > 0) {
                        Self.getElement().innerHTML = "�������� ��� ... "; // + this.responseJS.QuantityLeft;
                        Self.setOpacity(100);
                    } else
                        Self.Component_Remove();
                }
                Self.Loading = false;
            }, this.getElement(), "loader_comments");
        }
    },

    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.Part = this.extraData.part || 1;
        this.attachEvent("click", this.DomEvent_onClick);

        this.Event_throwsConstructDefaultParent([
        ], "Comments");

    }
});

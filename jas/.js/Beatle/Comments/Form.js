Beatle.Comments_Form = JooS.Reflect(Beatle.Class, {

    DomEvent_onClick: function() {
        return true;
    },

    Listener_Event_HttpForm_Submit: function() {
        var Data = {}, Form;

        if(!this.extraData.submit) {
            if(!this.Sending) {
                this.Sending = true;
                Form = this.getElement();

                if(this.extraData.params) {
                    for(var j in this.extraData.params)
                        Data[j] = this.extraData.params[j];
                }

                for(var i=0; i < Form.elements.length; i++) {
                    if(Form.elements[i].name && Form.elements[i].type != 'button' && Form.elements[i].type != 'submit') {
                        if((Form.elements[i].type == 'radio' || Form.elements[i].type == 'checkbox')) {
                            if(Form.elements[i].checked == true)
                                Data[Form.elements[i].name] = Form.elements[i].value;
                        } else {
                            Data[Form.elements[i].name] = Form.elements[i].value;
                        }
                    }
                }

                if(this.extraData.c)
                    Data.c = this.extraData.c;
                if(this.extraData.m)
                    Data.m = this.extraData.m;
                if(this.extraData.s)
                    Data.s = this.extraData.s;
                if(this.extraData.a)
                    Data.a = this.extraData.a;

                var Element = this.getElementById(this.Target);
                Element = new JooS.Element(Element);
                this.Edit = Element.getAttribute("edit");
                this.NewBranch = (this.Target == 'reply-default');

                var Loader = 'opacity';
                this.setOpacity(50);

                var Self = this;
                JooS.Ajax.Post("/ajax.php", Data, function() {

                    if(this.responseJS) {

                        var ErrorHtmlElement = Element.cssQueryStrict(".error-box")[0];
                        if(!ErrorHtmlElement) {
                            Self.ErrorBox = new JooS.Element(document.createElement("ul"));
                            Self.ErrorBox.htmlElement.className = "error-box";
                            Element.htmlElement.insertBefore(Self.ErrorBox.htmlElement, Element.cssQueryStrict("div")[0]);
                        } else if(!Self.ErrorBox) {
                            Self.ErrorBox = new JooS.Element(ErrorHtmlElement);
                        }

                        if(this.responseJS.Error > 0) {
                            for(var i in this.responseJS.Errors) {
                                var ErrorMessage = document.createElement("li");
                                ErrorMessage.innerHTML = this.responseJS.Errors[i]["Error"];
                                Self.ErrorBox.htmlElement.appendChild(ErrorMessage);
                            }
                            Self.ErrorBox.setDisplay("block");
                        } else {
                            Self.ErrorBox.setDisplay("none");

                            var Textarea = Element.cssQuery("textarea")[0];
                            Textarea.value = "";

                            if(Self.Edit) {
                                Self.Component_getParentObject().cssQuery(".post-message")[0].innerHTML = this.responseJS.UC_Text;
                            } else {
                                var UL = document.createElement("ul");

                                if(Self.NewBranch) {
                                    UL.className = "comments-list";
                                    Element.htmlElement.parentNode.insertBefore(UL, Element.htmlElement);
                                } else {
                                    UL.className = "comments-list children";
                                    Element.htmlElement.parentNode.appendChild(UL);
                                }

                                UL.innerHTML = this.responseText;
                                Beatle.loadComponents(UL);
                                window.location.hash = "comment" + this.responseJS.CommentId;
                            }

                            Self.throwEvent("Event_CommentElement_DestroyForms", Self.Target);
                        }
                    }

                    Self.setOpacity(100);
                    Self.Sending = false;

                }, this.Target, Loader);

            }
        } else {
            this.getElement().submit();
        }
    },


    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.attachEvent("submit", this.Listener_Event_HttpForm_Submit);

        this.Target = this.getAttribute("target") ? this.getAttribute("target") : this.extraData.target ? this.extraData.target : "_new";

        this.Event_throwsConstructDefaultParent([
            "Event_CommentElement_DestroyForms"
        ], "Comments_Element");

    }

});

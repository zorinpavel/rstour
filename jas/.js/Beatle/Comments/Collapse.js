Beatle.Comments_Collapse = JooS.Reflect(Beatle.Class, {

    DomEvent_onClick: function() {
        this.throwEvent("Event_CommentElement_Collapse");
    },

    Listener_Event_CommentElement_Collapse: function() {
        if(this.Collapsed) {
            this.setDisplay("none");
            this.Collapsed = false;
        } else {
            this.setDisplay("block");
            this.Collapsed = true;
        }
    },

    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.Collapsed = this.extraData.collapsed;
        this.attachEvent("click", this.DomEvent_onClick);

        this.Event_throwsConstructDefaultParent([
            "Event_CommentElement_Collapse"
        ], "Comments_Element");

        this.Event_listenerConstructDefaultParent([
            "Event_CommentElement_Collapse"
        ], "Comments_Element");

    }
});

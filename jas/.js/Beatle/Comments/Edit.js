Beatle.Comments_Edit = JooS.Reflect(Beatle.Comments_Answer, {

    Edit: true,

    getValue: function() {
        var Textarea = new JooS.Element(this.Form.cssQuery("textarea")[0]);
        Textarea.getElement().value = this.Parent.cssQuery(".post-message")[0].innerHTML.replace(/<br>/g, "");

        this.Form.setAttribute("edit", "true");

    },

    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

    }
});

Beatle.Comments_Element = JooS.Reflect(Beatle.Class, {

    Collapsed: false,

    Listener_Event_CommentElement_Collapse: function() {
        if(!this.Collapsed) {
            this.addClassName("collapsed");
            this.Collapsed = true;
        } else {
            this.removeClassName("collapsed");
            this.Collapsed = false;
        }
    },

    Listener_Event_CommentElement_Hide: function() {
        this.addClassName("post-hidden");
        this.removeClassName("post-complain");
        if(!this.Collapsed)
            this.throwEvent("Event_CommentElement_Collapse")
    },

    Listener_Event_CommentElement_Show: function() {
        this.removeClassName("post-hidden");
    },

    Listener_Event_CommentElement_Complain: function() {
        this.addClassName("post-complain");
        if(!this.Collapsed)
            this.throwEvent("Event_CommentElement_Collapse")
    },

    Listener_Event_CommentElement_Delete: function() {
        this.Component_Remove();
    },

    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.Collapsed = this.hasClassName("collapsed");

        this.Event_listenerConstructDefaultSelf([
            "Event_CommentElement_Hide", "Event_CommentElement_Show", "Event_CommentElement_Collapse",
            "Event_CommentElement_Complain", "Event_CommentElement_Delete"
        ]);

        this.Event_throwsConstructDefaultSelf([
            "Event_CommentElement_Hide", "Event_CommentElement_Show", "Event_CommentElement_Collapse",
            "Event_CommentElement_Complain", "Event_CommentElement_Delete"
        ]);

    }
});

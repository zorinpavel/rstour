Beatle.Comments_Action = JooS.Reflect(Beatle.Class, {

    DomEvent_onClick: function() {
        var Data = {
            c: 'classComments',
            cdo: this.extraData.action,
            comment: this.extraData.comment
        };

        if(this.extraData.confirm) {
            if(!confirm(this.extraData.confirm))
                return;
        }

        var Self = this;
        JooS.Ajax.Post("/ajax.php", Data, function() {
            if(!this.responseJS.Error) {
                switch(Self.extraData.action) {
                    case 'hide':
                        Self.throwEvent("Event_CommentElement_Hide");
                        break;
                    case 'show':
                        Self.throwEvent("Event_CommentElement_Show");
                        break;
                    case 'complain':
                        Self.throwEvent("Event_CommentElement_Complain");
                        if(this.responseJS.Complain > 5)
                            Self.throwEvent("Event_CommentElement_Hide");
                        break;
                    case 'delete':
                        Self.throwEvent("Event_CommentElement_Delete");
                        break;
                    default:
                        break;
                }
            }
        }, this);
    },

    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.attachEvent("click", this.DomEvent_onClick);

        this.Event_throwsConstructDefaultParent([
            "Event_CommentElement_Hide", "Event_CommentElement_Show", "Event_CommentElement_Collapse",
            "Event_CommentElement_Complain", "Event_CommentElement_Delete"
        ], "Comments_Element");

    }
});

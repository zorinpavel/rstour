Beatle.Comments_Answer = JooS.Reflect(Beatle.Class, {

    DomEvent_onClick: function() {
        this.throwEvent("Event_CommentElement_HideForms", this.Id);

        if(!this.Form) {
            var Template = this.throwEvent("Event_Template_Find", "comments-reply");

            this.Form = new JooS.Element("div", {
                className: "comments-reply",
                id: this.Id
            });
            this.Form.getElement().innerHTML = Template.templateBody;

            var Inputs = this.Form.cssQuery("input");
            for(var i in Inputs) {
                if(Inputs[i].name == "parent")
                    Inputs[i].value = this.extraData.parent;
                if(Inputs[i].name == "comment")
                    Inputs[i].value = this.extraData.comment;
                if(Inputs[i].name == "UC_Rate")
                    Inputs[i].value = this.extraData.UC_Rate;
            }

            this.Form.cssQuery("form")[0].target = this.Id;

            var ErrorBox = this.Form.cssQuery(".error-box")[0];
            if(ErrorBox)
                ErrorBox.style.display = "none";

            var UL = this.Parent.cssQueryStrict("ul")[0];
            if(UL) {
                UL.parentNode.insertBefore(this.Form.htmlElement, UL);
            } else {
                this.Parent.getElement().appendChild(this.Form.htmlElement)
            }

            this.getValue();
            this.Visible = true;

            Beatle.loadComponents(this.Form.htmlElement);

        } else if(this.Visible) {
            this.Form.setDisplay("none");
            this.Visible = false;
        } else {
            this.Form.setDisplay("block");
            this.Visible = true;
        }
    },

    Listener_Event_CommentElement_Collapse: function() {
        if(this.Form) {
            this.Form.setDisplay("none");
            this.Visible = false;
        }
    },

    Listener_Event_CommentElement_HideForms: function(Id) {
        if(this.Form && Id != this.Id) {
            this.Form.setDisplay("none");
            this.Visible = false;
        }
    },

    Listener_Event_CommentElement_DestroyForms: function(Id) {
        if(this.Form && Id == this.Id) {
            this.Parent.getElement().removeChild(this.Form.htmlElement);
            this.Form.__destroy();
            this.Form = null;
        }
    },

    getValue: function() {
        return;
    },

    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.Id = this.Edit ? "e" + this.extraData.comment : this.extraData.parent;
        this.Parent = this.Component_getParentObject();

        this.attachEvent("click", this.DomEvent_onClick);

        this.Event_throwsConstructDefaultParent([
            "Event_Template_Find", "Event_Template_Temp2Html"
        ], "Body");

        this.Event_throwsConstructDefaultParent([
            "Event_CommentElement_HideForms"
        ], "Comments_Element");

        this.Event_listenerConstructDefaultParent([
            "Event_CommentElement_HideForms", "Event_CommentElement_DestroyForms", "Event_CommentElement_Collapse"
        ], "Comments_Element");

    }
});

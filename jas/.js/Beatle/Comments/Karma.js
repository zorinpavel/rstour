Beatle.Comments_Karma = JooS.Reflect(Beatle.Class, {

    DomEvent_onClick: function() {
        debugger;
        //if(this.hasClassName("plus")) {
        //} else {
        //}
        this.Parent.Span.htmlElement.innerHTML = "+1";
        this.Parent.Span.replaceClassName("green", "red");
    },


    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.Span = new JooS.Element(this.cssQueryStrict("span")[0]);

        var Elements = this.cssQueryStrict("A");
        this.Plus = new JooS.Element(Elements[0]);
        this.Plus.Parent = this;
        this.Minus = new JooS.Element(Elements[1]);
        this.Minus.Parent = this;

        this.Plus.attachEvent("click", this.DomEvent_onClick);
        this.Minus.attachEvent("click", this.DomEvent_onClick);

        this.Event_throwsConstructDefaultParent([
            "Event_CommentElement_Collapse"
        ], "Comments_Element");

    }
});

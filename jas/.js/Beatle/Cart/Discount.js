Beatle.Cart_Discount = JooS.Reflect(Beatle.Class, {

    Listener_Event_Cart_RowPrice: function() {
        this.throwEvent("Event_Cart_Price", "discount", 0, parseInt(this.Amount));
    },


    Listener_Event_Cart_Discount: function(Sum) {
        this.Inputs[1].innerHTML = this.number_format(Sum, 0, "", " ");
    },


    Listener_Event_Cart_Pincode: function(Data) {
        if(this.Amount < Data.P_Discount) {
            this.Inputs[0].innerHTML = Data.P_Discount;
            this.Amount = Data.P_Discount;
            this.Name = Data.P_Name;
            this.Listener_Event_Cart_RowPrice();
        }
    },

    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.Amount = parseInt(this.cssQuery("input[role='Amount']")[0].value);
        this.Inputs = this.cssQuery("span");
        this.Name = this.cssQuery("H3");

        this.Event_throwsConstructDefaultParent([
            "Event_Cart_Price"
        ], "Cart");

        this.Event_listenerConstructDefaultParent([
            "Event_Cart_Discount", "Event_Cart_Pincode"
        ], "Cart");

        var Self = this;
        JooS.Startup(function() {
                Self.Timeout = setTimeout(JooS.Closure.Function(function() {
                    Self.Listener_Event_Cart_RowPrice();
                }, Self), 100);
            }
        );

    }

});

Beatle.Cart_Element_Minus = JooS.Reflect(Beatle.Class, {

    DomEvent_OnClick: function() {
        this.throwEvent("Event_Cart_RowReduce");
    },

    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.attachEvent("click", this.DomEvent_OnClick);

        this.Event_throwsConstructDefaultParent([
            "Event_Cart_RowReduce"
        ], "Cart_Element");

    }

});

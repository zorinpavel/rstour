Beatle.Cart_Element = JooS.Reflect(Beatle.Class, {

    Listener_Event_Cart_RowPrice: function() {

        if(this.MaxValue && this.Inputs.Value.value > this.MaxValue && this.MinValue && this.Inputs.Value.value < this.MinValue) {
            this.Inputs.Value.value = this.MaxValue;
            this.Inputs.Value.style.backgroundColor = "#FF0000";
            var Timeout = setTimeout(JooS.Closure.Function(function() {
                this.Inputs.Value.style.backgroundColor = "#FFFFFF";
            }, this), 100);
        } else {

            var Items = {};
            Items[this.Inputs.Code.value.replace(/[A-Za-z]+/gi, "")] = this.Inputs.Value.value;

            var Data = {
                c: "classTripCart",
                m: "tripCart",
                tcdo: "Update"
            };
            if(this.extraData.extras)
                Data.extras = Items;
            else
                Data.tcode = Items;

            JooS.Ajax.Post("/ajax.php", Data, function() {});

            this.throwEvent("Event_Cart_Price", this.Inputs.Code.value, this.Inputs.Price.value, this.Inputs.Value.value);
        }
    },

    Listener_Event_Cart_DeleteRow: function() {
//        this.throwEvent("Event_Cart_Price", this.Inputs.Code.value, this.Inputs.Price.value, 0);
        this.Inputs.Value.value = 0;
        this.Listener_Event_Cart_RowPrice();

        this.setOpacity(0);
        this.Timeout = setTimeout(JooS.Closure.Function(function() {
            this.Component_Remove();
        }, this), 500);
    },

    Listener_Event_Cart_RowIncrease: function() {
        this.Inputs.Value.value = parseInt(this.Inputs.Value.value) + 1;
        this.Listener_Event_Cart_RowPrice();
    },

    Listener_Event_Cart_RowReduce: function() {
        if(this.Inputs.Value.value > this.MinValue)
            this.Inputs.Value.value = parseInt(this.Inputs.Value.value) - 1;
        this.Listener_Event_Cart_RowPrice();
    },

    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.Inputs = {};
        this.Inputs.Code = this.cssQuery("input[role='Code']")[0];
        this.Inputs.Price = this.cssQuery("input[role='Price']")[0];
        this.Inputs.Value = this.cssQuery("input[role='Value']")[0];

        this.MaxValue = this.extraData.MaxValue || 100;
        this.MinValue = this.extraData.MinValue > -1 ? this.extraData.MinValue : 1;

        this.Timeout = setTimeout(JooS.Closure.Function(function() {
            this.setOpacity(100);
        }, this), 100);

        this.Event_listenerConstructDefaultSelf([
            "Event_Cart_DeleteRow", "Event_Cart_RowPrice", "Event_Cart_RowIncrease", "Event_Cart_RowReduce"
        ]);

        this.Event_throwsConstructDefaultParent([
            "Event_Cart_Price"
        ], "Cart");

        var Self = this;
        JooS.Startup(function() {
                Self.Timeout = setTimeout(JooS.Closure.Function(function() {
//                    if(!Self.extraData.order)
                        Self.throwEvent("Event_Cart_Price", Self.Inputs.Code.value, Self.Inputs.Price.value, Self.Inputs.Value.value);
//                    Self.Listener_Event_Cart_RowPrice();
                }, Self), 100);
            }
        );

    }

});

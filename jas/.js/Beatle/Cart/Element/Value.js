Beatle.Cart_Element_Value = JooS.Reflect(Beatle.Class, {

    DomEvent_OnBlur: function() {
        this.throwEvent("Event_Cart_RowPrice");
    },

    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.attachEvent("blur", this.DomEvent_OnBlur);

        this.Event_throwsConstructDefaultParent([
            "Event_Cart_RowPrice"
        ], "Cart_Element");

    }

});

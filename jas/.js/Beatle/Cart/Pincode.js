Beatle.Cart_Pincode = JooS.Reflect(Beatle.Class, {

    DOMEvent_onChange: function() {
        var PinValue = this.htmlElement.value;

        PinValue = PinValue.replace(/\D+/gi, "");
        this.htmlElement.value = PinValue.replace(/([0-9]{3})(\s+)?([0-9]{0,4})?(\s+)?([0-9]{0,4})?/gi, "$1 $3 $5");

        if(PinValue.length >= 11) {

            if(!this.Self.Template)
                this.Self.Template = this.Self.throwEvent("Event_Template_Find", "TemplateDiscount");

            var ValueElement = document.getElementById("Value");
            var Data = {
                c: "classTripCart",
                a: "CheckPin",
                Pincode: PinValue
            };

            var Self = this;
            JooS.Ajax.Get("/ajax.php", Data, function() {
                if(this.responseJS) {
                    if(this.responseJS.Error) {
                        Self.addClassName("error");
                        ValueElement.innerHTML = "";
                    } else {
                        Self.removeClassName("error");

                        ValueElement.innerHTML = this.responseJS.P_Discount + "%";

                        var Template = Self.Self.throwEvent("Event_Template_Temp2Html", Self.Self.Template.templateBody, this.responseJS);
                        var newElement = document.getElementById("Discount") ? document.getElementById("Discount").parentNode : document.createElement("div");
                        newElement.innerHTML = Template.Html;
                        Self.Self.htmlElement.parentNode.insertBefore(newElement, Self.Self.htmlElement.nextElementSibling);
                        Beatle.loadComponents(newElement);
                        Self.Self.throwEvent("Event_Cart_Pincode", this.responseJS);
                    }
                }
            }, ValueElement);
        }
    },

    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.PinElement = new JooS.Element(this.cssQuery("input[role='Pincode']")[0]);
        this.PinElement.Self = this;
        this.PinElement.attachEvent("keyup", this.DOMEvent_onChange);

        this.Event_throwsConstructDefaultParent([
            "Event_Cart_Pincode"
        ], "Cart");

        this.Event_listenerConstructDefaultParent([
        ], "Cart");

        this.Event_throwsConstructDefaultParent([
            "Event_Template_Find", "Event_Template_Temp2Html"
        ], "Body");

        var Self = this;
        JooS.Startup(function() {
                Self.Timeout = setTimeout(JooS.Closure.Function(function() {
                    Self.Template = Self.throwEvent("Event_Template_Find", "TemplateDiscount");
                }, Self), 500);
            }
        );

    }

});

Beatle.Cart_Price = JooS.Reflect(Beatle.Class, {

    Calculate: function() {
        var Price = 0;
        for(var i in this.Prices) {
            if(parseInt(i)) {
                Price += this.Prices[i].Price * this.Prices[i].Value;
            }
        }
        if(this.Prices.discount) {
            this.Prices.discount.Price = Math.round(Price / 100 * this.Prices.discount.Value);
            Price -= this.Prices.discount.Price;
            this.throwEvent("Event_Cart_Discount", this.Prices.discount.Price);
        }
        for(var i in this.Prices) {
            if(i.search(/ex\d+/) >= 0) { // ex3
                Price += this.Prices[i].Price * this.Prices[i].Value;
            }
        }

        this.getElement().innerHTML = this.number_format(Price, 0, "", " ");

    },

    Listener_Event_Cart_Price: function(Data) {
        this.Prices[Data.Code] = {};
        this.Prices[Data.Code].Value = Data.Value;
        this.Prices[Data.Code].Price = Data.Price;
        this.Calculate();
    },

    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.Price = this.extraData.value || 0;
        this.Prices = {};

        this.Event_listenerConstructDefaultParent([
            "Event_Cart_Price"
        ], "Cart");

        this.Event_throwsConstructDefaultParent([
            "Event_Cart_Discount"
        ], "Cart");

    }

});

Beatle.registerEvent("Event_Cart_Price", function(Code, Price, Value) {
  return {
    Code: Code, 
    Price: Price, 
    Value: Value 
  };
});
Beatle.registerEvent("Event_Cart_DeleteRow");
Beatle.registerEvent("Event_Cart_RowPrice");
Beatle.registerEvent("Event_Cart_RowIncrease");
Beatle.registerEvent("Event_Cart_RowReduce");
Beatle.registerEvent("Event_Cart_Discount", function(Value) {
  return Value;
});
Beatle.registerEvent("Event_Cart_Pincode", function(Value) {
  return Value;
});

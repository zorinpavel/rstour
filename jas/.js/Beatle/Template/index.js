Beatle.Template = JooS.Reflect(Beatle.Class, {

    Listener_Event_Template_Find: function(name) {
        if (name == this.name) {
            throw {
                templateBody: this.templateBody
            }
        }
    },

    Listener_Event_Template_Temp2Html: function(TemplateData) {
        Html = TemplateData.Template.replace(/'/g, "\\'");
        Html = Html.replace(/{=(\w+)}/g, "\' + TemplateData.Data.$1 + \'");
        Html = Html.replace(/%7B=(\w+)%7D/g, "\' + TemplateData.Data.$1 + \'");
        Html = Html.replace(/\n/g, "");
        Html = Html.replace(/\r/g, "");
        Html = "\'" + Html + "\'";
        eval('Html = ' + Html);
        throw {
            templateBody: TemplateData.Template,
            Html: Html
        };
    },

    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.name = this.extraData.name || this.htmlElement.id;

        var className = this.extraData.className || "t";
        if(className) {
            var c = this.cssQuerySelf("." + className), i, e;
            for (i = 0; i < c.length; i++) {
                e = new JooS.Element(c[i]);
                e.removeClassName(className);
                e.getElement().className = "js " + e.getElement().className;
            }
            e = null;
        }
        this.templateBody = this.getElement().innerHTML;

//        if(!this.extraData.stay)
//            this.getElement().innerHTML = "";

        this.Event_listenerConstructDefaultParent([
            "Event_Template_Find", "Event_Template_Temp2Html"
        ], "Body");
    }

});

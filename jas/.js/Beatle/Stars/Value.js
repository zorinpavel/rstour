Beatle.Stars_Value = JooS.Reflect(Beatle.Class, {

    Listener_Event_Stars_Click: function(Mark) {
        this.htmlElement.value = Mark;
    },

    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.Event_listenerConstructDefaultParent([
            "Event_Stars_Click"
        ], "Stars");

    }
});

Beatle.Stars_Star = JooS.Reflect(Beatle.Class, {

    DomEvent_onMouseOver: function() {
        this.throwEvent("Event_Stars_MouseOver", this.extraData.mark);
    },

    DomEvent_onMouseOut: function() {
        this.throwEvent("Event_Stars_MouseOut");
    },

    Listener_Event_Stars_MouseOver: function(Mark) {
        if(Mark >= this.extraData.mark)
            this.addClassName("hover");
        else
            this.removeClassName("hover");
    },

    Listener_Event_Stars_Click: function(Mark) {
        if(Mark >= this.extraData.mark)
            this.addClassName("active");
        else
            this.removeClassName("active");
    },

    Listener_Event_Stars_MouseOut: function() {
        this.removeClassName("hover");
    },

    DomEvent_onClick: function() {
        this.throwEvent("Event_Stars_Click", this.extraData.mark);
    },

    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.attachEvent("mouseover", this.DomEvent_onMouseOver);
        this.attachEvent("mouseout", this.DomEvent_onMouseOut);
        this.attachEvent("click", this.DomEvent_onClick);

        this.Event_throwsConstructDefaultParent([
            "Event_Stars_MouseOver", "Event_Stars_MouseOut", "Event_Stars_Click"
        ], "Stars");

        this.Event_listenerConstructDefaultParent([
            "Event_Stars_MouseOver", "Event_Stars_MouseOut", "Event_Stars_Click"
        ], "Stars");

    }
});

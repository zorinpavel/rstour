Beatle.Slider_Preview = JooS.Reflect(Beatle.Class, {

    DOMEvent_onClick: function() {
        var Key = this.Parent.throwEvent("Event_Slider_GetSelected");
        this.Parent.throwEvent("Event_SliderNavigationClick", this.Key - Key);
    },

    Listener_Event_Slider_SetSelected: function(Key) {
        for(var i in this.PreviewFrames)
            this.PreviewFrames[i].removeClassName("selected");

        if(this.PreviewFrames[Key].Key == Key) {
            this.PreviewFrames[Key].addClassName("selected");

            this.Viewport.marginLeft = (Key - this.VisibleFrames + Math.floor(this.VisibleFrames / 2 )) * this.PreviewWidth * -1;
            this.Viewport.marginLeft = this.Viewport.marginLeft >= 0 ? 0 : this.Viewport.marginLeft <= (this.FramesCount * this.PreviewWidth - this.Width) * -1 ? (this.FramesCount * this.PreviewWidth - this.Width + 5) * -1 : this.Viewport.marginLeft;
            this.Viewport.htmlElement.style.marginLeft = this.Viewport.marginLeft + "px";

        }
    },

    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.PreviewFrames = [];

        var Slider = this.cssQuery(".preview-frame");
        for (var i = 0; i < Slider.length; i++) {
            this.PreviewFrames[i] = new JooS.Element(Slider[i]);
            this.PreviewFrames[i].Key = i;
            this.PreviewFrames[i].Parent = this;
            this.PreviewFrames[i].attachEvent("click", this.DOMEvent_onClick);
        }

        this.PreviewWidth = this.PreviewFrames[0].getSize().width;
        this.Width = this.getSize().width;
        this.VisibleFrames = Math.floor(this.Width / this.PreviewWidth);
        this.FramesCount = i;

        this.Viewport = new JooS.Element(this.cssQueryStrict(".preview-viewport")[0]);
        this.Viewport.marginLeft = 0;

        this.Event_listenerConstructDefaultParent([
            "Event_Slider_SetSelected"
        ], "Slider");

        this.Event_throwsConstructDefaultParent([
            "Event_SliderNavigationClick", "Event_Slider_GetSelected"
        ], "Slider");

    }

});

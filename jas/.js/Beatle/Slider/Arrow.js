Beatle.Slider_Arrow = JooS.Reflect(Beatle.Class, {

    DOMEvent_onClick: function() {
        this.throwEvent("Event_SliderNavigationClick", this.extraData.direction);
    },

    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.attachEvent("click", this.DOMEvent_onClick);

        this.Event_throwsConstructDefaultParent([
            "Event_SliderNavigationClick"
        ], "Slider");

    }

});

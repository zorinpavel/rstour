Beatle.Slider_Selector = JooS.Reflect(Beatle.Class, {

    DOMEvent_onClick: function() {
        var Key = this.Parent.throwEvent("Event_Slider_GetSelected");
        this.Parent.throwEvent("Event_SliderNavigationClick", this.Key - Key);
    },

    Listener_Event_Slider_SetSelected: function(Key) {
        for(var i in this.Selectors)
            this.Selectors[i].removeClassName("selected");

        if(this.Selectors[Key].Key == Key)
            this.Selectors[Key].addClassName("selected");
    },

    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.Selectors = [];

        var Selectors = this.cssQuery("a");
        for (var i = 0; i < Selectors.length; i++) {
            this.Selectors[i] = new JooS.Element(Selectors[i]);
            this.Selectors[i].Key = i;
            this.Selectors[i].Parent = this;
            this.Selectors[i].attachEvent("click", this.DOMEvent_onClick);
        }

        this.Event_listenerConstructDefaultParent([
            "Event_Slider_SetSelected"
        ], "Slider");

        this.Event_throwsConstructDefaultParent([
            "Event_SliderNavigationClick", "Event_Slider_GetSelected"
        ], "Slider");

    }

});

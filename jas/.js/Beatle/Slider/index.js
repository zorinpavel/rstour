Beatle.Slider = JooS.Reflect(Beatle.Class, {

    AnimationOutLeft: "fadeOut",
    AnimationOutRight: "fadeOut",
    AnimationInLeft: "fadeIn",
    AnimationInRight: "fadeIn",

    Listener_Event_SliderNavigationClick: function(Direction, Stay) {
        if(Direction > 0) {
            this.ViewFrames[this.Key].htmlElement.style.animationName = this.AnimationOutLeft;
            this.ViewFrames[this.Key].htmlElement.style.webkitAnimationName = this.AnimationOutLeft;
        } else {
            this.ViewFrames[this.Key].htmlElement.style.animationName = this.AnimationOutRight;
            this.ViewFrames[this.Key].htmlElement.style.webkitAnimationName = this.AnimationOutRight;
        }
        this.ViewFrames[this.Key].removeClassName("selected");

        this.Key = (this.Key + Direction) < 0 ? this.ViewFrames.length - 1 : (this.Key + Direction) > this.ViewFrames.length - 1 ? 0 : this.Key + Direction;

        if(Direction > 0) {
            this.ViewFrames[this.Key].htmlElement.style.animationName = this.AnimationInLeft;
            this.ViewFrames[this.Key].htmlElement.style.webkitAnimationName = this.AnimationInLeft;
        } else {
            this.ViewFrames[this.Key].htmlElement.style.animationName = this.AnimationInRight;
            this.ViewFrames[this.Key].htmlElement.style.webkitAnimationName = this.AnimationInRight;
        }
        this.ViewFrames[this.Key].addClassName("selected");

        this.throwEvent("Event_Slider_SetSelected", this.Key);

        if(!Stay)
            clearInterval(this.Timeout);
    },


    AutoScroll: function() {
        this.Timeout = setInterval(JooS.Closure.Function(function() {
            this.Listener_Event_SliderNavigationClick(1, true);
        }, this), this.extraData.autoscroll);
    },


    Listener_Event_Slider_GetSelected: function() {
        throw(this.Key);
    },


    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.Key = 0;
        this.ViewFrames = [];

        var ViewFrames = this.cssQuery(".action-frame");
        for (var i = 0; i < ViewFrames.length; i++)
            this.ViewFrames[i] = new JooS.Element(ViewFrames[i]);

        if(this.extraData.autoscroll) {
            var Self = this;
            JooS.Startup(function() {
                    Self.AutoScroll();
                }
            );
        }

        this.Event_listenerConstructDefaultSelf([
            "Event_SliderNavigationClick", "Event_Slider_GetSelected"
        ]);

        this.Event_throwsConstructDefaultSelf([
            "Event_Slider_SetSelected"
        ]);

    }

});

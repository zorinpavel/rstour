Beatle.ToolTip = JooS.Reflect(Beatle.Class, {

    ArrowLeft: 10,

    DomEvent_onMouseOver: function() {
        if(this.ToolTip) {
            //this.setPosition();
            this.ToolTip.setDisplay("block");
        } else {
            if(this.Title) {
                this.ToolTip = this.createElement();
                this.setPosition();
            }
        }
    },

    DomEvent_onMouseOut: function() {
        if(this.ToolTip)
            this.ToolTip.setDisplay("none");
    },

    setPosition: function() {
        var Pos = this.getPos();
        var Size = this.getSize();
        var ToolTipSize = this.ToolTip.getSize();
        var Left = 0;
        if(ToolTipSize.width + Pos.left + 5 > document.body.clientWidth) {
            Left = document.body.clientWidth - ToolTipSize.width - 5;
            this.ArrowLeft = Pos.left - Left;
        } else {
            Left = Pos.left - (this.ArrowLeft / 2);
        }

        if(this.Position != "top") {
            this.ToolTip.setPos(Left, Pos.top - Size.height);
        } else {
            this.ToolTip.setPos(Left, Pos.top - ToolTipSize.height - 5);
            this.Pointer.setPos(Size.width / 2, ToolTipSize.height - 5);
        }
    },

    createElement: function() {
        var Element = document.createElement("div");
        Element.className = "tooltip-layer" + " " + this.Position;

        var Rel = document.createElement("div");
        Rel.className = "tooltip-relative";
        var Content = document.createElement("div");
        Content.className = "tooltip-content";
        var Title = document.createTextNode(this.Title);
        this.Pointer = new JooS.Element(document.createElement("i"));
        this.Pointer.htmlElement.className = "tooltip-pointer";

        Content.appendChild(Title);
        Rel.appendChild(Content);
        Rel.appendChild(this.Pointer.htmlElement);
        Element.appendChild(Rel);

        document.body.appendChild(Element);

        return new JooS.Element(Element);
    },

    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.attachEvent("mouseover", this.DomEvent_onMouseOver);
        this.attachEvent("mouseout", this.DomEvent_onMouseOut);

        this.Title = this.extraData.title || this.getAttribute("title");
        this.getElement().title = "";
        this.Position = this.extraData.position || "top";

    }

});

Beatle.SearchByValue = JooS.Reflect(Beatle.classHttp_Form_Input, {

    ResultBlock: false,
    InputCodes: false,

    DOMEvent_onChange: function() {
        var Name = this.inputName;
        var Value = this.getInput_TagValue();

        clearTimeout(this.Timeout);
        this.Timeout = null;

        if(Value.length > 0 && Value.length >= this.Length) {
            if(this.maxLength > 0 && Value.length <= this.maxLength) {
                this.Timeout = setTimeout(JooS.Closure.Function(function() {
                    this.Do();
                }, this), 500);
            }
        } else if(this.ResultBlock)
            this.ResultBlock.setDisplay("none");
    },


    Do: function() {
        if(this.extraData.c) {
            var Self = this;

            var Data = {};
            if(this.extraData.c)
                Data.c = this.extraData.c;
            if(this.extraData.m)
                Data.m = this.extraData.m;
            if(this.extraData.s)
                Data.s = this.extraData.s;
            if(this.extraData.a)
                Data.a = this.extraData.a;
            Data.value = this.getInput_TagValue();

            JooS.Ajax.Get("/ajax.php", Data, function() {
                if(this.responseJS) {
                    Self.CreateResults();
                    if(this.responseJS.Error > 0) {
                        // Self.ResultBlock.htmlElement.innerHTML = this.responseJS.Errors[0]['Error'];
                        // Self.ResultBlock.addClassName("Error");
                        Self.ClearResults();
                    } else {
                        // Self.ResultBlock.removeClassName("Error");
                        Self.ShowResults(this.responseJS.Results);
                    }
                }
            });
        } else {
            return;
        }
    },


    getInput_TagValue: function() {
        var Values = this.getInput_Value().split(",");
        var Value = (Values[Values.length-1] != " ") ? Values[Values.length-1] : "";
        return Value;
    },


    CreateResults: function() {
        if(!this.ResultBlock) {
            var Div = document.createElement("div");
            Div.className = "select-all";
            this.ResultBlock = new JooS.Element(Div);
            this.htmlElement.parentNode.appendChild(this.ResultBlock.htmlElement);
        } else
            this.ResultBlock.setDisplay("block");
    },


    ClearResults: function() {
        this.ResultBlock.setDisplay("none");
        this.ResultBlock.clearBody();

        if(this.InputCodes) {
            for(var i in this.InputCodes)
                this.InputCodes[i].value = "";
        }
    },


    ShowResults: function(Results) {
        this.CreateResults();

        var Self = this;
        var InputValue = this.getInput_Value();
        var TagValue = this.getInput_TagValue();

        this.ResultBlock.clearBody();
        var ul = document.createElement("ul");
        ul.className = "select-values";
        for(var i in Results) {

            var li = document.createElement("li");
            li.value = Results[i].Value;
            li.setAttribute("data-level", Results[i].Level);
            li.innerHTML = Results[i].Name;

            var Li = new JooS.Element(li);
            Li.attachEvent("click", function() {
                var nValue = InputValue.substr(0, InputValue.length - TagValue.length);
                nValue = (nValue.length > 0) ?  nValue + " " + this.htmlElement.innerHTML : nValue + this.htmlElement.innerHTML;

                Self.setInput_Value(nValue);
                if(Self.InputCodes) {
                    for(var i in Self.InputCodes)
                        Self.InputCodes[i].value = this.htmlElement.value;
                }
                Self.ResultBlock.setDisplay("none");
                Self.htmlElement.focus();
            });
            ul.appendChild(li);
        }
        this.ResultBlock.htmlElement.appendChild(ul);
    },


    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.htmlElement.parentNode.style.position = "relative";
        this.InputCodes = JooS.cssQuery("input[type=hidden]", this.htmlElement.parentNode);

    }
});

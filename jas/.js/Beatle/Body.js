Beatle.registerEvent("Event_Document_Scroll");

Beatle.Body = JooS.Reflect(Beatle.Class, {

    Window_Scroll: function() {
        this.throwEvent("Event_Document_Scroll");
    },

    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.Event_throwsConstructDefaultSelf([
            "Event_Document_Scroll"
        ]);

    }

});

JooS.WindowDOM.attachEvent("scroll", function() {
    var Body = Beatle.getObjectByElement(document.body);
    if(Body)
        Body.Window_Scroll();
    return true;
});

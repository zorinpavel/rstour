Beatle.Stretch_Body = JooS.Reflect(Beatle.Class, {
    
    className: "selected",

    Listener_Event_Stretch_Click: function(Data) {
        if(Data.Id == this.Id) {
            if(this.hasClassName(this.className)) {
//                this.removeClassName(this.className);
//                this.Parent.removeClassName(this.className);
//                this.htmlElement.style.height = 0;
            } else {
                this.addClassName(this.className);
                this.Parent.addClassName(this.className);
//                this.htmlElement.style.height = this.Height;
            }
        } else {
            this.removeClassName(this.className);
            this.Parent.removeClassName(this.className);
//            this.htmlElement.style.height = 0;
        }
    },


    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.Id = this.getAttribute("id") || this.extraData.Id;
        this.Parent = new JooS.Element(this.htmlElement.parentNode);
        this.Height = this.extraData.height || "100px";

        this.Event_listenerConstructDefaultParent([
            "Event_Stretch_Click"
        ], "Stretch");

    }
});

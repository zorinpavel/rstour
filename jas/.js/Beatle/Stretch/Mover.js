Beatle.Stretch_Mover = JooS.Reflect(Beatle.Class, {

    className: "selected",

    DomEvent_onClick: function() {
        if(this.Id)
            this.throwEvent("Event_Stretch_Click", this.Id);
    },

    Listener_Event_Stretch_Click: function(Data) {
        if(Data.Id == this.Id) {
            if(this.hasClassName(this.className)) {
                this.removeClassName(this.className);
            } else {
                this.addClassName(this.className);
                this.htmlElement.style.height = this.Height;
            }
        } else {
            this.removeClassName(this.className);
        }
    },

    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.Id = this.getAttribute("name") || this.extraData.Id;
        this.attachEvent("click", this.DomEvent_onClick);

        this.Event_throwsConstructDefaultParent([
            "Event_Stretch_Click"
        ], "Stretch");

        this.Event_listenerConstructDefaultParent([
            "Event_Stretch_Click"
        ], "Stretch");

    }
});

Beatle.Scroll = JooS.Reflect(Beatle.Class, {

    IndentTop: 100,
    Part: 2,
    Count: 1,

    Listener_Event_Document_Scroll: function() {
        var Scroll = JooS.DocumentDOM.getScroll();
        if(Scroll.scrollY >= this.IndentTop)
            this.loadMore();
    },

    loadMore: function() {
        if(!this.Loading) {
            this.Loading = true;

            var Loader = this.extraData.loader || 'loader';
            var Data = {};

            if(this.extraData.c)
                Data.c = this.extraData.c;
            if(this.extraData.m)
                Data.m = this.extraData.m;
            if(this.extraData.s)
                Data.s = this.extraData.s;
            if(this.extraData.a)
                Data.a = this.extraData.a;

            if(this.extraData.params) {
                for(var i in this.extraData.params)
                    Data[i] = this.extraData.params[i];
            }

            Data.part = this.Part;

            if(this.Quantity && this.Limit)
                this.Count = this.Quantity - this.Limit * this.Part;

            var Div = document.createElement("div");
            Div.className = "scroll-loader";
            this.getElement().parentNode.insertBefore(Div, this.getElement());

            var Self = this;
            JooS.Ajax.Get("/ajax.php", Data, function() {
                Div.innerHTML = this.responseText;
                Beatle.loadComponents(Div);

                Self.Loading = false;
                Self.Part = Self.Part + 1;
                Self.IndentTop = Self.IndentTop * 2;

                if(Self.Count > 0) {
                    if(Self.CountObj)
                        Self.CountObj.innerHTML = Self.Count;
                } else {
                    Self.Component_Remove();
                }
            }, Div, Loader);
        }
    },

    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.IndentTop = (this.extraData.indent || this.IndentTop);

        this.Part = (this.extraData.part || this.Part);
        this.Quantity = this.extraData.quantity;
        this.Limit = this.extraData.limit;

        this.CountObj = this.cssQuery("span.count-obj")[0];

        this.attachEvent("click", this.loadMore);

        this.Event_listenerConstructDefaultParent([
            "Event_Document_Scroll"
        ], "Body");

    }

});

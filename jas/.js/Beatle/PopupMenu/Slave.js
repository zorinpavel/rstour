Beatle.PopupMenu_Slave = JooS.Reflect(Beatle.Class, {
    hideTimeout: null,

    Listener_Event_PM_onMouseOver: function(Id) {
        if(Id == this.Id) {
            this.throwEvent("Event_PM_setUnVisible", this.Id);
            this.throwEvent("Event_PM_setVisible", this.Id);
        }
    },


    Listener_Event_PM_onMouseOut: function(Id) {
        if(Id == this.Id) {
            clearTimeout(this.hideTimeout);
            this.hideTimeout = setTimeout(JooS.Closure.Function(function() {
                this.throwEvent("Event_PM_setUnVisible", this.Id, true); /* true if stay */
            }, this), 500);
        }
    },


    DomEvent_onMouseOver: function() {
        clearTimeout(this.hideTimeout);
    },


    DomEvent_onMouseOut: function() {
        this.Listener_Event_PM_onMouseOut(this.Id);
    },


    Listener_Event_PM_setVisible: function(Id) {
        if(Id == this.Id) {
            this.addClassName("Over");
        }
    },


    Listener_Event_PM_setUnVisible: function(Data) {
        clearTimeout(this.hideTimeout);
        if(Data.Stay && this.Active) {
            this.addClassName("Over");
        } else {
            this.removeClassName("Over");
        }
    },


    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.Id = this.extraData.id;
        this.Active = this.hasClassName("Over");

        this.attachEvent("mouseover", this.DomEvent_onMouseOver);
        this.attachEvent("mouseout", this.DomEvent_onMouseOut);

        this.Event_throwsConstructDefaultParent([
            "Event_PM_setVisible", "Event_PM_setUnVisible"
        ], "PopupMenu");

        this.Event_listenerConstructDefaultParent([
            "Event_PM_onMouseOver", "Event_PM_onMouseOut", "Event_PM_setVisible", "Event_PM_setUnVisible"
        ], "PopupMenu");
    }

});

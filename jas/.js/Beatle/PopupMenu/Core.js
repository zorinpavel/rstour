Beatle.PopupMenu_Core = JooS.Reflect(Beatle.Class, {

    OverClassName: "Over",

    DomEvent_onMouseOver: function() {
        this.throwEvent("Event_PM_onMouseOver", this.Id);
    },


    DomEvent_onMouseOut: function() {
        this.throwEvent("Event_PM_onMouseOut", this.Id);
    },


    Listener_Event_PM_setVisible: function(Id) {
        if(Id == this.Id)
            this.addClassName(this.OverClassName);
    },


    Listener_Event_PM_setUnVisible: function(Data) {
        if(Data.Stay && this.Active)
            this.addClassName(this.OverClassName);
        else
            this.removeClassName(this.OverClassName);
    },


    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.Id = this.extraData.id;
        this.Active = this.hasClassName(this.OverClassName);

        if(this.extraData.action == "click")
            this.attachEvent("click", this.DomEvent_onMouseOver);
        else
            this.attachEvent("mouseover", this.DomEvent_onMouseOver);

        this.attachEvent("mouseout", this.DomEvent_onMouseOut);

        this.Event_throwsConstructDefaultParent([
            "Event_PM_onMouseOver", "Event_PM_onMouseOut"
        ], "PopupMenu");

        this.Event_listenerConstructDefaultParent([
            "Event_PM_setVisible", "Event_PM_setUnVisible"
        ], "PopupMenu");
    }

});

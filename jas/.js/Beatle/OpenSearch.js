Beatle.registerEvent("Event_Search_Expand");

Beatle.OpenSearch = JooS.Reflect(Beatle.Class, {

    DOMEvent_onClick: function() {
        this.throwEvent("Event_Search_Expand");
    },

    Listener_Event_Search_Expand: function() {
        if(this.Parent) {
            if (this.Parent.hasClassName("expanded"))
                this.Parent.removeClassName("expanded");
            else
                this.Parent.addClassName("expanded");
        }
    },

    __constructor: function(Element, extraData) {
        this.__constructor.__parent(Element, extraData);

        this.Parent = new JooS.Element(document.getElementById(this.extraData.id) || this.htmlElement.parentNode);
        this.attachEvent("click", this.DOMEvent_onClick);

        this.Event_throwsConstructDefaultParent([
            "Event_Search_Expand"
        ], "Body");

        this.Event_listenerConstructDefaultParent([
            "Event_Search_Expand"
        ], "Body");

    }

});

<?php

    include_once(ADMIN_PATH."/setup.php");

    AdminHeaders();
    MainNavi("security");

?>

    <div class="EditData">

        <fieldset>
            <legend class="Header">sitemap.xml</legend>

            <?
                $FileName = "sitemap.xml";

                if($_REQUEST['Update']) {
                    $FileData = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n  <urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n";

                    $TreeData = _autoload("classMenu");
                    $TreeData->MenuType = 6;
                    $TreeData->GetChildren(0);
                    $Tree = $TreeData->GetTree(0);

                    $FileData .= addTree($Tree[0], $FileData);

                    $FileData .= "  </urlset>";

                    $fp = fopen(CLIENT_PATH."/$FileName", "w");
                    fputs($fp, $FileData);
                    fclose($fp);
                }

            ?>
            <form method="POST" enctype="multipart/form-data">
                <input type="hidden" name="Update" value="1">
                <input type="submit" value="Сгенерировать новый файл" class="blue">
            </form>
            <br>
            <?
                if(file_exists(CLIENT_PATH."/$FileName")) {
                    $Time = filectime(CLIENT_PATH."/$FileName");
                    $Lines = file(CLIENT_PATH."/$FileName");
                    echo "<b>".date("d.m.Y H:i:s", $Time)."</b> Стрк в файле ".count($Lines)."<br>";
                    echo "<a href=\"/$FileName\" target=\"_blank\">Скачать</a>";
                } else
                    echo "<i>Нет файла $FileName</i>";
            ?>

        </fieldset>

    </div>

<?php

    AdminFooter();

    function addTree($Tree, &$FileData, $Priority = 1) {
        foreach($Tree['Elements'] as $Key => $Section) {
            $FileData .= "    <url>\n";
            $FileData .= "      <loc>".DOMAIN_URL.$Section['Link']."</loc>\n";
            $FileData .= "      <changefreq>weekly</changefreq>\n";
            $FileData .= "      <priority>$Priority</priority>\n";
            $FileData .= "    </url>\n";
            if(count($Section['Elements']))
                addTree($Section, $FileData, $Priority - 0.1);
        }
    }
<?php

    include_once(ADMIN_PATH."/setup.php");

    AdminHeaders();
    MainNavi("security");

?>

    <div class="EditData">

        <fieldset>
            <legend class="Header">robots.txt</legend>

            <i style="color:red">Будьте крайне внимательны при редактировании, изменения могут сильно влиять на индексирование сайта</i><br><br>

            <?

                $FileName = CLIENT_PATH."/robots.txt";
                $FileData = file($FileName);

                if($_REQUEST['Update'])
                    $newData = explode("\r\n", $_REQUEST['upData']);

                foreach($FileData as $Key => $Str) {
                    $Str = str_replace("\r\n", "", $Str);
                    $FileData[$Key] = $Str;

                    if(strstr($Str, "##{")) {
                        $flag = true;
                        continue;
                    }
                    if(strstr($Str, "##}")) {
                        $flag = false;
                        continue;
                    }
                    if($flag) {
                        $Data[$Key] = $Str;
                        if(!$KeyFlag)
                            $KeyFlag = $Key;
                    }
                }

                if($_REQUEST['Update']) {
                    array_splice($FileData, $KeyFlag, count($Data));
                    array_splice($FileData, $KeyFlag, 0, $newData);

                    $FileData = implode("\r\n", $FileData);

                    $fp = fopen($FileName, "w");
                    fputs($fp, $FileData);
                    fclose($fp);
                }

                $Data = implode("\r\n", ($newData ? $newData : $Data));

            ?>
            <form method="POST">
                <input type="hidden" name="Update" value="1">
                <?
                    $Textarea = new Textarea("upData", $Data, 20, 100, "width:50%");
                    $Textarea->dPrint();
                ?>
                <div id="SaveIt"><input type="submit" name="Update" value="Сохранить" class="green"></div>
            </form>
            <?


            ?>

        </fieldset>

    </div>

<?php


    AdminFooter();
<?php

    $BlankColor  = "#FFFFFF";
    $TodayColor  = "#E5FFE5";
    $FutureColor = "#FFFFCC";
    $ArchColor   = "#EEEEEE";
    $AlertColor  = "#FEE6E6";

    $color = $Item['T_OnRequest'] ? $TodayColor : ($Item['T_Visible'] ? $BlankColor : $ArchColor);
?>

<tr style="background-color: <? echo $color; ?>;<? echo ($Item['T_Visible']? "opcity: 0.5;" : ""); ?>" id="row<? echo $Item['T_Code']; ?>">
    <td class="border" width="50%">
        <?
//            $T5RO = new T5RO("T_Order[".$Item['T_Code']."]", $Item['T_Order']);
//            $T5RO->dPrint();
            
            echo "[".$Item['T_Order']."] &nbsp; ";
            echo "<a href=\"edit.php?T_Code=".$Item['T_Code']."\" style=\"font-size:120%\">".$Item['T_Name']." (".$Item['T_Barcode'].")</a>";
            echo "<table class=\"border\">";
            foreach($Item['Dates'] as $Key => $Date) {
                $BackgroundColor = $BlankColor;
                if(date("Y-m-d", strtotime($Date['T_Date'])) < date("Y-m-d"))
                    $BackgroundColor = $ArchColor;
                elseif(date("Y-m-d", strtotime($Date['T_Date'])) <= date("Y-m-d", mktime(0, 0, 0, date("m"), date("d")+2, date("Y"))))
                    $BackgroundColor = $TodayColor;
                elseif($Date['T_Limit'] && $Date['T_Booked'] >= $Date['T_Limit'])
                    $BackgroundColor = $AlertColor;
                elseif($Date['T_Limit'] && $Date['T_Booked'] >= ($Date['T_Limit'] / 2))
                    $BackgroundColor = $FutureColor;

                echo "<tr style=\"background-color:$BackgroundColor;\" id=\"date".$Date['TD_Code']."\">";
                echo "<td class=\"border\" width=\"100px;\">".$Date['T_DateStr']."</td>";
                echo "<td class=\"border\" width=\"100px;\">".$Date['T_Places']." / ".$Date['T_Limit']." / ".$Date['T_Booked']." / ".$Date['T_Paid']."</td>";
                echo "<td class=\"border\"><a href=\"print.php?T_Code=".$Item['T_Code']."&T_Date=".$Date['T_Date']."\" target=\"_blank\"><img src=\"".ADMIN_URL."/images/print.png\"></a></td>";
                echo "<td class=\"border\"><a href=\"comment.php?T_Code=".$Item['T_Code']."&T_Date=".$Date['T_Date']."\" target=\"_blank\"><img src=\"".ADMIN_URL."/images/ms-word.png\"></a></td>";
                echo "<td class=\"border\"><a href=\"".ADMIN_URL."/cart/search.php?T_Name=".$Item['T_Barcode']."&T_Date=".$Date['T_Date']."\" target=\"_blank\"><img src=\"".ADMIN_URL."/images/magnifier.png\"></a></td>";
                if(Permissions::getPermissions(167)) {
                    echo "<td class=\"border\"><a href=\"#\" target=\"_blank\" onclick=\"if(confirm('Вы действительно хотите удалить дату выезда со всеми данными и бронями?')) Ajax.Get('date_delete.php', 'T_Date=".$Date['T_Date']."&T_Code=".$Item['T_Code']."', false, 'date".$Date['TD_Code']."', 'loader', false); return false;\"><img src=\"".ADMIN_URL."/images/delete.png\"></a></td>";
                }
                echo "</tr>";
            }
            echo "</table>";
        ?>
    </td>
    <td class="border" width="50%">
        <? echo $Item['T_Comment']; ?>
    </td>
</tr>


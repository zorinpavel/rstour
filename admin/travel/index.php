<?php

    include_once(ADMIN_PATH."/setup.php");
    include_once("setup.php");

    AdminHeaders();
    MainNavi("travel");

    $classTrip = _autoload("classTrip");
    $RequestData = array(
        "AllDates" => true,
    );
    
    if($_REQUEST['sValue'])
        $RequestData['sWord'] = $_REQUEST['sValue'];
    if($_REQUEST['sBarcode'])
        $RequestData['sBarcode'] = $_REQUEST['sBarcode'];
    if($_REQUEST['oDate']) {
        $RequestData['oDate'] = $_REQUEST['oDate'];
        $RequestData['AllDates'] = false;
    }
    if($_REQUEST['sExternal'])
        $RequestData['sExternal'] = $_REQUEST['sExternal'];
    else
        $RequestData['sExternal'] = "NULL";


    $Items = $classTrip->GetItems($RequestData, true);

    foreach($Items as $T_Code => $Item) {
        $Item = $classTrip->GetItemVars($Item, true, false);

        $dateSort = true;
        foreach($Item['Dates'] as $Key => $Date) {
            if(date("Y-m-d") > date("Y-m-d", strtotime($Date['T_Date']))) {
//                unset($Item['Dates'][$Key]);
            } elseif($dateSort) {
                $interval = date_diff(date_create($Date['T_Date']), date_create($dateIndex));
                $Item['_dateindex'] = $Item['_dateindex'] - $interval->format("%a%");
                $dateSort = false;
            }
        }
        $Items[$T_Code] = $Item;
    }

    $OrderCond = array("T_Visible", "DESC", "_relevant", "DESC", "T_Order", "DESC", "_dateindex", "DESC", "T_Code", "DESC");
    $Items = SortArray($Items, $OrderCond);

?>
    <div class="EditData">
        <fieldset>
            <legend class="Header">Добавить / редактировать туры (выбрано: <? echo count($Items); ?>)</legend>

            <div style="height:3em;">
                <div style="float:left">
                    <form method="GET">
                        <input type="text" name="sBarcode" value="<? echo $_REQUEST['sBarcode']; ?>" placeholder="Внутренний номер">
                        <input type="text" name="sValue" value="<? echo $_REQUEST['sValue']; ?>" placeholder="Название тура или дата">
                        &nbsp;&nbsp;
                        <? $InputDate = new InputDatePicker("oDate", $_REQUEST['oDate']); $InputDate->dPrint(); ?>
                        <button type="submit" class="green">Искать</button>
                        <button type="reset" class="gray" onclick="window.location.href = './';return false;">Сбросить</button>
                    </form>
                </div>
                <div style="float:right">
                    <?
                        if(Permissions::$Permissions[162]) {
                            $InputButton = new InputButton("New", "Добавить тур", "", "window.location = 'edit.php';", "blue");
                            $InputButton->dPrint();
                        }
                    ?>
                </div>
            </div>

            <div style="margin-bottom:2em;">
                <a href="<? echo GetLink("sExternal", "NULL"); ?>" role="button" class="<? echo (($_REQUEST['sExternal'] == "NULL" || !$_REQUEST['sExternal']) ? "transparent green" : ""); ?>">Только свои</a>
                <a href="<? echo GetLink("sExternal", "R"); ?>" role="button" class="<? echo (($_REQUEST['sExternal'] == "R") ? "transparent green" : ""); ?>">Русь</a>
                <a href="<? echo GetLink("sExternal", "MP"); ?>" role="button" class="<? echo (($_REQUEST['sExternal'] == "MP") ? "transparent green" : ""); ?>">Магпут</a>
            </div>

            <form method="POST" name="Main" enctype="multipart/form-data">
                <table width="100%" border="0" cellspacing="0" cellpadding="3" align="center" class="border">
                <?
                    foreach($Items as $Key => $Item) {
                        include("templates/row.php");
                    }
                ?>
                </table>
            </form>

        </fieldset>
    </div>
<?

    AdminFooter();
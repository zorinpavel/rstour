<?php

    include_once("setup.php");

    require_once(COMMON_PATH."/JsHttpRequestLib/JsHttpRequest.php");
    $JsHttpRequest = new JsHttpRequest("utf-8");
    DB::getInstance();

    $fileName = mysql_real_escape_string($_REQUEST['file']);

    $file = DB::selectValue("SELECT T_Image FROM tripImages WHERE T_Image = '$fileName'");
    if(!$file)
        $file = DB::selectValue("SELECT T_Image FROM tripItems WHERE T_Image = '$fileName'");
    if(!$file)
        $file = DB::selectValue("SELECT T_Scheme FROM tripItems WHERE T_Scheme = '$fileName'");
    if(!$file)
        $file = DB::selectValue("SELECT T_Place FROM tripPlaces WHERE T_Place = '$fileName'");

    if($file) {
        if(file_exists(CLIENT_PATH.$file))
            unlink(CLIENT_PATH.$file);
        $r = DB::Query("DELETE FROM tripImages WHERE T_Image = '$file'");
        $r = DB::Query("UPDATE tripItems SET T_Image = NULL WHERE T_Image = '$file'");
        $r = DB::Query("UPDATE tripItems SET T_Scheme = NULL WHERE T_Scheme = '$file'");
        $r = DB::Query("DELETE FROM tripPlaces WHERE T_Place = '$file'");

        $ACF = _autoload("classActualFile");
        if(is_object($ACF))
            $ACF->Expire($file);

        echo "Удален…";
    } else {
        echo "Не найдено";
    }

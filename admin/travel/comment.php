<?php

    include_once(ADMIN_PATH."/setup.php");
    include_once("setup.php");
    
    
    AdminHeaders();
    ?>
        <style type="text/css">
            TABLE {
                width: 100%;
                border-collapse: collapse;
                margin: 2em auto;
            }
            TABLE TR.table-header TD {
                font-weight: bold;
                text-align: center;
            }
            TABLE TD {
                font-size: 9pt;
                padding: 0.2em 0.3em;
                border: 1px solid #D3D4D4;
                vertical-align: top;
            }
            TEXTAREA {
                width: 100%;
                border: 0 none;
            }
        </style>
    <?

    $classTrip = _autoload("classTrip");
    $classTripOrders = _autoload("classTripOrders");
    $classCompany = _autoload("classCompany");
    
    $T_Code = (int)$_REQUEST['T_Code'];
    $T_Date = dateDecode($_REQUEST['T_Date']);
    $Item = $classTrip->GetItems(array("T_Code" => $T_Code));
    
    $Orders = DB::selectArray("SELECT * FROM cartData WHERE T_Code = $T_Code AND T_Date = '$T_Date' AND Ord_Status NOT IN (1, 6, 7)", "Ord_Code");
    foreach($Orders as $Ord_Code => $Order)
        $Orders[$Ord_Code] = $classTripOrders->GetOrderVars($Order);
    
    
    if($_REQUEST['Update']) {
        foreach($Orders as $Ord_Code => $Order) {
            $Update = false;
            
            foreach($Order['Travelers'] as $Key => $Traveler) {
                $Request = $_REQUEST['comment'][$Ord_Code][$Key];
                if($Request != $Order['Travelers'][$Key]['comment']) {
                    $Orders[$Ord_Code]['Travelers'][$Key]['comment'] = $Order['Travelers'][$Key]['comment'] = $Request;
                    $Update = true;
                }
            }
            
            if($Update) {
                unset($Order['Ord_Data']);
                $cartData = json_encode($Order);
                DB::Query("UPDATE cartData SET Ord_Data = '".mysql_real_escape_string($cartData)."', events = CONCAT(events, '".classUserEvents::Events("COMMENT")."') WHERE Ord_Code = $Ord_Code");
            }
        }
    }
    
    
    echo "<h2>".$Item[$T_Code]['T_Name']."</h2>";
    echo "Дата: <b>".dateEncode($T_Date)."</b>";

    echo "<form method=\"POST\">";
    echo "<table width=\"100%\">";
    echo "<tr class=\"table-header\">";
    echo "<td>#</td>";
    echo "<td>ФИО</td>";
    echo "<td>Дата рождения</td>";
    echo "<td>Паспорт</td>";
    echo "<td>Телефон</td>";
    echo "<td>Место</td>";
    echo "<td>Допуслуги</td>";
    echo "<td>Бронь</td>";
    echo "<td style=\"width:30%\">Комментарий</td>";
    echo "</tr>";

    $Number = 1;
    foreach($Orders as $Ord_Code => $Order) {
        foreach($Order['Travelers'] as $Key => $Traveler) {
            echo "<tr><td>".$Number++."</td>";
            echo "<td>".$Traveler['lastname']." ".$Traveler['name']." ".$Traveler['middlename']."</td>";
            echo "<td>".$Traveler['birthdate']."</td>";
            echo "<td>".$Traveler['passport']['number']."</td>";
            echo "<td>".$Order['TravelerPhone']."</td>";
            echo "<td>".$Traveler['place']."</td>";
            echo "<td>";
            if($Traveler['Extras']) {
                foreach($Traveler['Extras'] as $eKey => $Value) {
                    echo $Order['ExtrasData'][$eKey]['dI_Name']."<br>";
                }
            } else
                echo "-";
            echo "</td>";
            echo "<td>".$Order['Ord_Code']."</td>";
            echo "<td><textarea name=\"comment[$Ord_Code][$Key]\">".$Traveler['comment']."</textarea></td>";
            echo "</tr>";
        }
    }
    echo "</table>";
    
    ?>
            <div id="SaveIt">
                <input type="hidden" name="T_Code" value="<? echo $T_Code; ?>">
                <input type="hidden" name="T_Date" value="<? echo $T_Date; ?>">
                <input type="hidden" name="Update" value="Update">
                <input type="submit" value="Сохранить" class="green">
                <a href="#!" role="button" class="blue" onclick="window.print(); return false;">Распечатать</a>
            </div>
        </form>
    <?

    
    AdminFooter();
    
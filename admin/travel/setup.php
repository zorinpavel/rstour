<?php


    class sefRegions extends T30RO {
        function sefRegions($Code) {
            $R = sefRouter::Get("Regions", $Code);
            $Router = $R['Url'];
            if($_REQUEST['Update']) {
                $Header = DB::selectValue("SELECT dR_Name FROM dataRegions WHERE dR_Code = $Code");
                $Router = sefRouter::GetRouteStatic($Header);
                $Router = "/tours/$Router";
                sefRouter::Register("Regions", $Code, $Router);
            }
            $this->T30RO("", $Router);
        }
    }


    $Regions = array(
        "Header"        => "Регионы",
        "TableName"     => "dataRegions",
        "Id"            => "dR_Code",
        "Order"         => "dR_Name, dR_Code",
        "Headers"       => array("Название"),
        "Fields"        => array("dR_Name"),
        "Types"         => array("T30"),
        "DBTypes"       => array("CHAR"),
        "Uploads"       => array(),
        "ButtonHeaders" => array("Адрес", "", ""),
        "Buttons"       => array("sefRegions", "GoCities", "GoRegionEdit"),
        "Filters"       => array(),
        "FiltersNames"  => array(),
        "FiltersTypes"   => array(),
        "FiltersCompare" => array(),
        "EditOnly" => 0
    );


    class InputRegionSelect extends InputDBSelect {
        function InputRegionSelect($Name, $Value, $onChange="") {
            $this->InputDBSelect($Name, $Value, "dataRegions", "dR_Code", "dR_Name", "", "dR_Name, dR_Code", "", $onChange);
        }
    }


    class GoCities extends InputButton {
        function GoCities($Code) {
            $this->InputButton("Go$Code", "Города", "", "window.location = './lists.php?list=cities&dR_Code=$Code';");
        }
    }


    class GoRegionEdit extends InputButton {
        function GoRegionEdit($Code) {
            $this->InputButton("Go$Code", "Редактировать", "", "window.location = './region.php?dR_Code=$Code';", "green");
        }
    }


    class GoCityEdit extends InputButton {
        function GoCityEdit($Code) {
            $this->InputButton("Go$Code", "Редактировать", "", "window.location = './city.php?dC_Code=$Code';", "green");
        }
    }


    class GoTypeEdit extends InputButton {
        function GoTypeEdit($Code) {
            $this->InputButton("Go$Code", "Редактировать", "", "window.location = './type.php?dT_Code=$Code';", "green");
        }
    }


    class sefCities extends T30RO {
        function sefCities($Code) {
            $R = sefRouter::Get("Cities", $Code);
            $Router = $R['Url'];
            if($_REQUEST['Update']) {
                $Header = DB::selectValue("SELECT dC_Name FROM dataCities WHERE dC_Code = $Code");
                $Router = sefRouter::GetRouteStatic($Header);
                $Router = "/tours/$Router";
                sefRouter::Register("Cities", $Code, $Router);
            }
            $this->T30RO("", $Router);
        }
    }


    class sefTypes extends T30RO {
        function sefTypes($Code) {
            $R = sefRouter::Get("Types", $Code);
            $Router = $R['Url'];
            if($_REQUEST['Update']) {
                $Header = DB::selectValue("SELECT dT_Name FROM dataTypes WHERE dT_Code = $Code");
                $Router = sefRouter::GetRouteStatic($Header);
                $Router = "/tours/$Router";
                sefRouter::Register("Types", $Code, $Router);
            }
            $this->T30RO("", $Router);
        }
    }


    class sefTrip {
        public $Router;

        function sefTrip() {
            $R = sefRouter::Get("classTrip", $_REQUEST['T_Code']);
            $this->Router = $R['Url'];
        }

        function dPrint() {
            ?>
                <input type="text" name="Ite_Sef" id="Ite_Sef" value="<? echo $this->Router; ?>" size="53" onkeyup="makeSef(this.value, 100);return false;" placeholder="" autocomplete="off" style="margin-bottom:3px;">.html
                <br>
                <button class="blue" onclick="makeSef(false);return false;">Из заголовка</button>
                <script type="text/javascript">
                    function makeSef(Value, keyTimeout = 0) {
                        Value = Value || document.getElementById("T_Name").value;

                        clearTimeout(keyTimeout);
                        keyTimeout = setTimeout(function() {
                            Ajax.Get("/ajax.php", {
                                c: 'sefRouter',
                                a: 'GetRoute',
                                Route: Value,
                            }, fillValue, false, false, false);
                        }, keyTimeout);
                    }

                    function fillValue() {
                        var Ite_Sef = document.getElementById("Ite_Sef");
                        if(this.responseJS.Error) {
                            Ite_Sef.style.backgroundColor = "rgba(255, 0, 0, 0.5)";
                        } else {
                            Ite_Sef.style.backgroundColor = false;
                            Ite_Sef.value = this.responseJS.Route;
                        }
                    }
                </script>
            <?
        }
    }


    $Cities = array(
        "Header"        => "Города",
        "TableName"     => "dataCities",
        "Id"            => "dC_Code",
        "Order"         => "dC_Name, dC_Code",
        "Headers"       => array("Название"),
        "Fields"        => array("dC_Name"),
        "Types"         => array("T30"),
        "DBTypes"       => array("CHAR"),
        "Uploads"       => array(),
        "ButtonHeaders" => array("Адрес", ""),
        "Buttons"       => array("sefCities", "GoCityEdit"),
        "Filters"       => array("dR_Code", "list"),
        "FiltersNames"  => array("Регион", false),
        "FiltersTypes"   => array("InputRegionSelect", "InputHidden"),
        "FiltersCompare" => array("N", false),
        "EditOnly" => 0
    );


    $Types = array(
        "TableName" => "dataTypes",
        "Id"            => "dT_Code",
        "Order"         => "dT_Code",
        "Header"        => "Типы туров",
        "Headers"       => array("#", "Название"),
        "Fields"        => array("dT_Code", "dT_Name"),
        "Types"         => array("InputLabel", "T30"),
        "Uploads"       => array(),
        "ButtonHeaders" => array("Адрес", ""),
        "Buttons"       => array("sefTypes", "GoTypeEdit"),
        "Filters"       => array(),
        "FiltersNames"  => array(),
        "FiltersTypes"   => array(),
        "FiltersCompare" => array(),
        "EditOnly" => 0
    );


    $Type = array(
        "Header"        => "Редактировать тип тура",
        "TableName"     => "dataTypes",
        "Id"            => "dT_Code",
        "IdName"        => "dT_Name",
        "Headers"       => array("Название", "Текст вверху", "H1 (signature)", "title для списка туров", "desc для списка туров", "SEO текст"),
        "Fields"        => array("dT_Name", "dT_topText", "dT_seoHeader", "dT_seoTitle", "dT_seoDesc", "dT_seoText"),
        "Types"         => array("T50", "InputHtml", "T50", "T50", "T50", "TA100"),
        "DBTypes"       => array("CHAR", "CHAR/NULL", "CHAR/NULL", "CHAR/NULL", "CHAR/NULL", "CHAR/NULL"),
        "Buttons"       => array(),
        "ButtonHeaders" => array(),
        "ViewOnly"      => 0,
        "Params"        => 0,
    );


    $Transports = array(
        "TableName" => "dataTransports",
        "Id"            => "dT_Code",
        "Order"         => "dT_Code",
        "Fields"        => array("dT_Name"),
        "Types"         => array("T30"),
        "Uploads"       => array(),
        "Header"        => "Типы транспорта",
        "Headers"       => array("Название"),
        "ButtonHeaders" => array(),
        "Buttons"       => array(),
        "Filters"       => array(),
        "FiltersNames"  => array(),
        "FiltersTypes"   => array(),
        "FiltersCompare" => array(),
        "EditOnly" => 0
    );


    $EditTrip = array(
        "Header"        => "Редактировать тур",
        "TableName"     => "tripItems",
        "Id"            => "T_Code",
        "IdName"        => "T_Name",
        "Headers"       => array("Активный", "Адрес", "Title страницы", "meta desc страницы", "Преоритет показа", "Внутренний номер", "Название", "Кол-во дней", "Цена", "Коэффициент", "Скидка для детей", "Описание", "Комментарий"),
        "Fields"        => array("T_Visible", "", "T_Title", "T_MetaDesc", "T_Order", "T_Barcode", "T_Name", "T_Days", "T_Price", "T_Discount", "T_KidsDiscount", "T_Desc", "T_Comment"),
        "Types"         => array("tVisible", "sefTrip", "T50", "T50", "T5", "T20", "tName", "T5", "T10", "T10", "T10", "InputHtml", "TA50"),
        "DBTypes"       => array("INT", "IGNORE", "CHAR/NULL", "CHAR/NULL", "INT", "CHAR", "CHAR", "INT", "INT", "FLOAT", "INT/NULL", "CHAR", "CHAR/NULL"),
        "ButtonHeaders" => array(),
        "Buttons"       => array(),
        "ViewOnly"      => 0,
        "Params"        => 1,
    );


    class T50max extends T50 {
        function T50max($Name, $Value) {
            $this->Maxlength = 50;
            $this->T50($Name, $Value);
        }
    }


    class tName extends InputText {
        function tName($Name, $Value) {
            $Bg = "";
            $this->Maxlength = 50;
            if(!Permissions::getPermissions(170)) {
                $this->Readonly = true;
                $Bg = $Value ? "background:#EEEEEE" : "";
            }
            $this->InputText($Name, $Value, 53, $Bg, "", true);
        }
    }


    class tVisible extends InputCheckbox {
        function tVisible($Name, $Value) {
            if(!Permissions::getPermissions(170)) {
                $this->Disabled = true;
            }
            $this->InputCheckbox($Name, $Value);
        }
    }


    class tripFilesMultiSelect {
        function tripFilesMultiSelect($Name, $Value, $ValueKey) {
            $this->Name = $Name;
            $this->Value = $Value;
            $this->ValueKey = $ValueKey;
        }

        function dPrint($Cnt = 0) {
            if($this->Value) {
                foreach($this->Value as $k => $Value) {
                    dPrint("<div id=\"".$this->Name."_v_$k\">");
                    dPrint("<input type=\"file\" name=\"".$this->Name."[$k]\">");
                    dPrint("<a href=\"".$Value[$this->ValueKey]."\" target=\"_blank\">".$Value[$this->ValueKey]."</a> &nbsp; <small>[<a href=\"#\" onClick=\"if(confirm('Вы действительно хотите удалить файл?')) { openUrl('file_del.php?file=".$Value[$this->ValueKey]."', '".$this->Name."_v_$k'); return false; }\" class=\"SectionLineEdit\">удалить</a>]</small>");
                    dPrint("</div>");
                }
            } else {
                if(!$Cnt)
                    dPrint("<div id=\"".$this->Name."_".$Cnt."\">");
                dPrint("<input type=\"file\" name=\"".$this->Name."[$Cnt]\">");
                if(!$Cnt)
                    dPrint("</div>");
            }
        }
    }


    class InputExtras extends InputMultiSelect {
        function InputExtras($Name, $Value, $Options, $Style="", $onChange="", $CheckBoxes="", $Prices=array()) {
            $this->Prices = $Prices;
            parent::InputMultiSelect($Name, $Value, $Options, $Style, $onChange, $CheckBoxes);
        }

        function dPrint() {
            if(!is_array($this->Value))
                $this->Value = array();
            foreach($this->Options as $Key => $Value) {
                $Checked = (in_array($Key, $this->Value)? " checked" : "");
                dPrint("<input type=\"checkbox\" name=\"".$this->Name."[$Key]\" value=\"$Key\" id=\"$Key\"$Checked class=\"radio\">&nbsp;<label for=\"$Key\" style=\"display:inline-block;width:80%;margin:0.3em 0;\">$Value</label>");
                dPrint("<input type=\"text\" name=\"TI_Price[$Key]\" value=\"".$this->Prices[$Key]."\" style=\"width:40px;\">");
                dPrint("<br>");
            }
        }
    }


    class tripDatesMultiSelect {
        function tripDatesMultiSelect($Name, $Value) {
            $this->Name = $Name;
            $this->Value = $Value;
        }

        function dPrint() {
            global $Item;

            ?>
                <script type="text/javascript">
                    function OnRequest(Checkbox) {
                        var dateAdd = document.getElementById("dateAdd");
                        var datesTable = document.getElementById("datesTable");
                        if(Checkbox.checked && confirm('Даты по запросу удалят все данные и брони по существующим датам.\nПродолжить?')) {
                            dateAdd.style.display = "none";
                            datesTable.style.display = "none";
                        } else {
                            Checkbox.checked = false;
                            dateAdd.style.display = "";
                            datesTable.style.display = "";
                        }
                    }
                </script>
            <?

            echo "<label><input type=\"checkbox\" name=\"DateOnRequest\" value=\"1\" class=\"radio\" onchange=\"OnRequest(this);\"".($Item['T_OnRequest'] ? " checked" : "").">По запросу</label>";

            if(!$Item['T_OnRequest']) {
                echo "<div style=\"margin:3px 0;\" id=\"dateAdd\">";
                $InputDate = new InputDateRO("dT_Date[0]", false);
                $InputDate->Id = "dT_Date[0]";
                $InputDate->dPrint();
                $InputT10P = new T5("dT_Places[0]", false);
                $InputT10P->dPrint();
                dPrint("</div>");
                dPrint("<table class=\"border\" width=\"50%\" id=\"datesTable\">");
                dPrint("<tr><th class=\"border\">Дата</th><th class=\"border\" style=\"width:20%\">Мест</th><th class=\"border\" style=\"width:20%\">Лимит</th><th class=\"border\" style=\"width:20%\">Забронировано</th><th class=\"border\" style=\"width:20%\">Оплачено</th><th class=\"border\" style=\"width:20%\">Координатор</th><th class=\"border\" style=\"width:20%\">Время сбора</th><th class=\"border\" style=\"width:20%\">Отправление</th><th class=\"border\">&nbsp;</th></tr>");
                foreach($this->Value as $dKey => $Date) {
                    $CanEdit = true;
                    if(date("Y-m-d") >= date("Y-m-d", strtotime($Date['T_Date'])))
                        $CanEdit = false;
                    dPrint("<tr style=\"background-color:".($CanEdit ? "#F0FFF0" : "#EFF3F8")."\" id=\"date".$Date['TD_Code']."\"><td class=\"border\">");
                    $InputDate = new InputDateRO("dT_Date[$dKey]", $Date['T_Date']);
                    $InputDate->dPrint();
                    dPrint("</td><td class=\"border\" align=\"center\">");
                    if($CanEdit)
                        $InputT5 = new T5("dT_Places[$dKey]", $Date['T_Places']); else
                        $InputT5 = new T5RO("dT_Places[$dKey]", $Date['T_Places'], false);
                    $InputT5->dPrint();
                    dPrint("</td><td class=\"border\" align=\"center\">");
                    if($CanEdit)
                        $InputT5Lim = new T5("dT_Limit[$dKey]", $Date['T_Limit']); else
                        $InputT5Lim = new T5RO("dT_Limit[$dKey]", $Date['T_Limit'], false);
                    $InputT5Lim->dPrint();
                    dPrint("</td><td class=\"border\" align=\"center\">");
                    $InputT5L = new T5RO("dT_Booked[$dKey]", $Date['T_Booked'], false);
                    $InputT5L->dPrint();
                    dPrint("</td><td class=\"border\" align=\"center\">");
                    $InputT5L = new T5RO("dT_Paid[$dKey]", $Date['T_Paid'], false);
                    $InputT5L->dPrint();
                    dPrint("</td>");
                    dPrint("</td><td class=\"border\" align=\"center\">");
                    $InputGuide = new InputGuideSelect("dT_Guide[$dKey]", $Date['T_Guide'], false);
                    $InputGuide->dPrint();
                    dPrint("</td>");
                    dPrint("</td><td class=\"border\" align=\"center\">");
                    $InputCTime = new T10("dT_CollectionTime[$dKey]", date("H:i", strtotime($Date['T_CollectionTime'])));
                    $InputCTime->dPrint();
                    dPrint("</td>");
                    dPrint("</td><td class=\"border\" align=\"center\">");
                    $InputDTime = new T10("dT_DepartureTime[$dKey]", date("H:i", strtotime($Date['T_DepartureTime'])));
                    $InputDTime->dPrint();
                    dPrint("</td>");
                    if(Permissions::getPermissions(167) && date("Y-m-d", strtotime($Date['T_Date'])) < date("Y-m-d")) {
                        echo "<td class=\"border\"><a href=\"#\" target=\"_blank\" onclick=\"if(confirm('Вы действительно хотите удалить дату выезда со всеми данными и бронями?')) Ajax.Get('date_delete.php', 'T_Date=".$Date['T_Date']."&T_Code=".$Date['T_Code']."', false, 'date".$Date['TD_Code']."', 'loader', false); return false;\"><img src=\"".ADMIN_URL."/images/delete.png\"></a></td>";
                    }
                    dPrint("</tr>");
                    dPrint("<tr style=\"background-color:".($CanEdit ? "#F0FFF0" : "#EFF3F8")."\" id=\"date".$Date['TD_Code']."\"><td class=\"border\"><b>Место стречи</b></td>");
                    dPrint("<td class=\"border\" colspan=\"7\">");
                    $InputPlace = new InputText("dT_Place[$dKey]", $Date['T_Place'], 50, "width:99%");
                    $InputPlace->dPrint();
                    dPrint("</td>");
                    dprint("</tr>");
                }
                dPrint("</table>");
            }
        }
    }


    $Included = array(
        "Header"        => "Дополнительные услуги",
        "TableName"     => "dataIncluded",
        "Id"            => "dI_Code",
        "Order"         => "dI_Code",
        "Headers"       => array("Название", "Цена", "Категория", "Описание"),
        "Fields"        => array("dI_Name", "dI_Price", "dI_Category", "dI_Desc"),
        "Types"         => array("T30", "T10", "InputExtraCategoriesSelect", "TA50"),
        "DBTypes"       => array("CHAR", "CHAR", "INT", "CHAR"),
        "Uploads"       => array(),
        "ButtonHeaders" => array(),
        "Buttons"       => array(),
        "Filters"       => array("dI_Category"),
        "FiltersNames"  => array("Категория"),
        "FiltersTypes"   => array("InputExtraCategoriesSelect"),
        "FiltersCompare" => array("N"),
        "EditOnly" => 0
    );


    $Places = array(
        "Header"        => "Достопримечательности",
        "TableName"     => "dataPlaces",
        "Id"            => "P_Code",
        "Order"         => "P_Code",
        "Headers"       => array("Название", "Описание", "Координаты"),
        "Fields"        => array("P_Name", "P_Desc", "P_Coords"),
        "Types"         => array("T30", "TA50", "T20"),
        "Uploads"       => array(),
        "ButtonHeaders" => array(),
        "Buttons"       => array(),
        "Filters"       => array(),
        "FiltersNames"  => array(),
        "FiltersTypes"   => array(),
        "FiltersCompare" => array(),
        "EditOnly" => 0
    );


    class GoHotelEdit extends InputButton {
        function GoHotelEdit($Code) {
            $this->InputButton("Go$Code", "Редактировать", "", "window.location = './hotels.php?dH_Code=$Code';", "green");
        }
    }


    $Hotels = array(
        "Header"        => "Отели",
        "TableName"     => "dataHotels",
        "Id"            => "dH_Code",
        "Order"         => "dH_Name, dH_Code",
        "Headers"       => array("Название"),
        "Fields"        => array("dH_Name"),
        "Types"         => array("T30"),
        "Uploads"       => array(),
        "ButtonHeaders" => array(""),
        "Buttons"       => array("GoHotelEdit"),
        "Filters"       => array(),
        "FiltersNames"  => array(),
        "FiltersTypes"   => array(),
        "FiltersCompare" => array(),
        "EditOnly" => 0
    );


    class hotelFilesMultiSelect {
        function hotelFilesMultiSelect($Name, $Value) {
            $this->Name = $Name;
            $this->Value = $Value;
        }

        function dPrint($Cnt = 0) {
            if($this->Value) {
                foreach($this->Value as $k => $Value) {
                    dPrint("<div id=\"".$this->Name."_v_$k\">");
                    dPrint("<input type=\"file\" name=\"".$this->Name."[$k]\">");
                    dPrint("<a href=\"".$Value['dHI_Image']."\" target=\"_blank\">".$Value['dHI_Image']."</a> &nbsp; <small>[<a href=\"#\" onClick=\"if(confirm('Вы действительно хотите удалить файл?')) { openUrl('hotel_file_del.php?file=".$Value['dHI_Image']."', '".$this->Name."_v_$k'); return false; }\" class=\"SectionLineEdit\">удалить</a>]</small>");
                    dPrint("</div>");
                }
            } else {
                if(!$Cnt)
                    dPrint("<div id=\"".$this->Name."_".$Cnt."\">");
                dPrint("<input type=\"file\" name=\"".$this->Name."[$Cnt]\">");
                if(!$Cnt)
                    dPrint("</div>");
            }
        }
    }


    $Hotel = array(
        "Header"        => "Редактировать отель",
        "TableName"     => "dataHotels",
        "Id"            => "dH_Code",
        "IdName"        => "dH_Name",
        "Headers"       => array("Название", "Город", "Описание"),
        "Fields"        => array("dH_Name", "dC_Code", "dH_Desc"),
        "Types"         => array("T50", "InputCitySelect", "InputHtml"),
        "DBTypes"       => array("CHAR", "INT/NULL", "CHAR"),
        "Buttons"       => array(),
        "ButtonHeaders" => array(),
        "ViewOnly"      => 0,
        "Params"        => 1,
    );


    class cityFilesMultiSelect {
        function cityFilesMultiSelect($Name, $Value) {
            $this->Name = $Name;
            $this->Value = $Value;
        }

        function dPrint($Cnt = 0) {
            if($this->Value) {
                foreach($this->Value as $k => $Value) {
                    dPrint("<div id=\"".$this->Name."_v_$k\">");
                    dPrint("<input type=\"file\" name=\"".$this->Name."[$k]\">");
                    dPrint("<a href=\"".$Value['dCI_Image']."\" target=\"_blank\">".$Value['dCI_Image']."</a> &nbsp; <small>[<a href=\"#\" onClick=\"if(confirm('Вы действительно хотите удалить файл?')) { openUrl('city_file_del.php?file=".$Value['dCI_Image']."', '".$this->Name."_v_$k'); return false; }\" class=\"SectionLineEdit\">удалить</a>]</small>");
                    dPrint("</div>");
                }
            } else {
                if(!$Cnt)
                    dPrint("<div id=\"".$this->Name."_".$Cnt."\">");
                dPrint("<input type=\"file\" name=\"".$this->Name."[$Cnt]\">");
                if(!$Cnt)
                    dPrint("</div>");
            }
        }
    }


    class InputCitySelect extends InputDBSelect {
        function InputCitySelect($Name, $Value, $onChange="") {
            $this->Empty = true;
            $this->InputDBSelect($Name, $Value, "dataCities", "dC_Code", "dC_Name", "", "dC_Name ASC", "", $onChange);
        }
    }


    $City = array(
        "Header"        => "Редактировать город",
        "TableName"     => "dataCities",
        "Id"            => "dC_Code",
        "IdName"        => "dC_Name",
        "Headers"       => array("meta_title", "meta_desc", "Название", "Описание", "title для списка туров", "desc для списка туров", "H1 (signature)", "SEO текст"),
        "Fields"        => array("dC_metaTitle", "dC_metaDesc", "dC_Name", "dC_Desc", "dC_seoTitle", "dC_seoDesc", "dC_seoHeader", "dC_seoText"),
        "Types"         => array("T50", "T50", "T50", "InputHtml", "T50", "T50", "T50", "TA100"),
        "DBTypes"       => array("CHAR", "CHAR", "CHAR", "CHAR", "CHAR", "CHAR", "CHAR", "CHAR"),
        "Buttons"       => array(),
        "ButtonHeaders" => array(),
        "ViewOnly"      => 0,
        "Params"        => 1,
    );


    $Region = array(
        "Header"        => "Редактировать регион / область",
        "TableName"     => "dataRegions",
        "Id"            => "dR_Code",
        "IdName"        => "dR_Name",
        "Headers"       => array("Название", "title для списка туров", "desc для списка туров", "H1 (signature)", "SEO текст"),
        "Fields"        => array("dR_Name", "dR_seoTitle", "dR_seoDesc", "dR_seoHeader", "dR_seoText"),
        "Types"         => array("T50", "T50", "T50", "T50", "TA100"),
        "DBTypes"       => array("CHAR", "CHAR", "CHAR", "CHAR", "CHAR"),
        "Buttons"       => array(),
        "ButtonHeaders" => array(),
        "ViewOnly"      => 0,
    );

    $Guides = array(
        "Header"        => "Гиды",
        "TableName"     => "dataGuides",
        "Id"            => "dG_Code",
        "Order"         => "dG_Name, dG_Code",
        "Headers"       => array("Имя Фамилия", "Телефон"),
        "Fields"        => array("dG_Name", "dG_Phone"),
        "Types"         => array("T30", "T20"),
        "Uploads"       => array(),
        "ButtonHeaders" => array(),
        "Buttons"       => array(),
        "Filters"       => array(),
        "FiltersNames"  => array(),
        "FiltersTypes"   => array(),
        "FiltersCompare" => array(),
        "EditOnly" => 0
    );


    class InputGuideSelect extends InputDBSelect {
        function InputGuideSelect($Name, $Value, $onChange="") {
            $this->Empty = true;
            $this->InputDBSelect($Name, $Value, "dataGuides", "dG_Code", "dG_Name", "", "dG_Name, dG_Code", "", $onChange);
        }
    }


    class InputExtraCategoriesSelect extends InputSelect {
        function InputExtraCategoriesSelect($Name, $Value, $onChange = "") {
            $this->Empty = true;
            $Options = array(
                "1" => "Питание",
                "2" => "Размещение",
                "3" => "Экскурсии",
            );
            $this->InputSelect($Name, $Value, $Options, "", $onChange);
        }
    }


    $Pincodes = array(
        "Header"        => "Пин-коды",
        "TableName"     => "pinCodes",
        "Id"            => "P_Code",
        "Order"         => "P_Code",
        "Headers"       => array("Код", "Имя", "Скидка <small>(%)</small>"),
        "Fields"        => array("P_Barcode", "P_Name", "P_Discount"),
        "Types"         => array("T20RO", "T20", "T10RO"),
        "Uploads"       => array(),
        "ButtonHeaders" => array(),
        "Buttons"       => array(),
        "Filters"       => array(),
        "FiltersNames"  => array(),
        "FiltersTypes"   => array(),
        "FiltersCompare" => array(),
        "EditOnly" => 0
    );



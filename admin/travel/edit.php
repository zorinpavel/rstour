<?php

    include_once(ADMIN_PATH."/setup.php");
    include_once("setup.php");

    AdminHeaders();
    MainNavi("travel");
    
    $Last = DB::selectArray("SHOW TABLE STATUS LIKE 'tripItems'");
    $T_Code = (int)$_REQUEST['T_Code'];

    if(!$_REQUEST['T_Code']) {
        ?><script type="text/javascript">window.location = 'edit.php?T_Code=<? echo $Last['Auto_increment']; ?>';</script><?
        die;
    }

    $classTrip = _autoload("classTrip");
    $classTrip->ItemId = $_REQUEST['T_Code'];
    $Items = $classTrip->GetItems(false, true);

    if($_REQUEST['Update']) {

        if($Items[$T_Code])
            $Item = $classTrip->GetItemVars($Items[$T_Code], false);

        DB::Query("DELETE FROM tripTypes WHERE T_Code = $T_Code");
        DB::Query("DELETE FROM tripRegions WHERE T_Code = $T_Code");
        DB::Query("DELETE FROM tripCities WHERE T_Code = $T_Code");
        DB::Query("DELETE FROM tripDays WHERE T_Code = $T_Code");
        DB::Query("DELETE FROM tripIncluded WHERE T_Code = $T_Code");
        DB::Query("DELETE FROM tripExtras WHERE T_Code = $T_Code");
        DB::Query("DELETE FROM tripHotels WHERE T_Code = $T_Code");

        if($_REQUEST['dT_Code']) {
            foreach($_REQUEST['dT_Code'] as $dT_Code) {
                if($dT_Code)
                    DB::Query("REPLACE INTO tripTypes SET T_Code = $T_Code, dT_Code = $dT_Code");
            }
        }
        if($_REQUEST['dR_Code']) {
            foreach($_REQUEST['dR_Code'] as $dR_Code) {
                if($dR_Code)
                    DB::Query("REPLACE INTO tripRegions SET T_Code = $T_Code, dR_Code = $dR_Code");
            }
        }
        if($_REQUEST['dC_Code']) {
            foreach($_REQUEST['dC_Code'] as $dC_Code) {
                if($dC_Code)
                    DB::Query("REPLACE INTO tripCities SET T_Code = $T_Code, dC_Code = $dC_Code");
            }
        }
        if($_REQUEST['dT_Days']) {
            foreach($_REQUEST['dT_Days'] as $D_Code => $dT_Day) {
                if($dT_Day)
                    DB::Query("REPLACE INTO tripDays SET T_Code = $T_Code, D_Code = $D_Code, D_Desc = '".mysql_real_escape_string(nl2br($dT_Day))."'");
            }
        }
        if($_REQUEST['DateOnRequest']) {
            DB::Query("UPDATE tripItems SET T_OnRequest = 1 WHERE T_Code = $T_Code");
            DB::Query("DELETE FROM tripDates WHERE T_Code = $T_Code");
//            DB::Query("DELETE FROM cartData WHERE T_Code = $T_Code");
            if($_REQUEST['DateOnRequest'] != $Item['T_OnRequest'])
                $EventsArray['Dates'] = "По запросу";
        } else {
            DB::Query("UPDATE tripItems SET T_OnRequest = NULL WHERE T_Code = $T_Code");
            if($_REQUEST['dT_Date']) {
                foreach($_REQUEST['dT_Date'] as $TD_Code => $Date) {
                    if($Date) {
                        $Date = dateDecode($Date);
                        DB::Query("INSERT INTO tripDates (T_Code, T_Date, T_Places, T_Limit, T_Guide, T_place, T_CollectionTime, T_DepartureTime)
                                    VALUES
                                      ($T_Code, '$Date', ".($_REQUEST['dT_Places'][$TD_Code] ?: 0).", ".($_REQUEST['dT_Limit'][$TD_Code] ?: 0).", ".($_REQUEST['dT_Guide'][$TD_Code] ?: "NULL").", ".($_REQUEST['dT_Place'][$TD_Code] ? "'".$_REQUEST['dT_Place'][$TD_Code]."'" : "NULL").", ".($_REQUEST['dT_CollectionTime'][$TD_Code] ? "'".$_REQUEST['dT_CollectionTime'][$TD_Code]."'" : "NULL").", ".($_REQUEST['dT_DepartureTime'][$TD_Code] ? "'".$_REQUEST['dT_DepartureTime'][$TD_Code]."'" : "NULL").")
                                    ON DUPLICATE KEY UPDATE
                                        T_Places = ".($_REQUEST['dT_Places'][$TD_Code] ?: 0).", T_Limit = ".($_REQUEST['dT_Limit'][$TD_Code] ?: 0).", T_Guide = ".($_REQUEST['dT_Guide'][$TD_Code] ?: "NULL").", T_Place = ".($_REQUEST['dT_Place'][$TD_Code] ? "'".$_REQUEST['dT_Place'][$TD_Code]."'" : "NULL").", T_CollectionTime = ".($_REQUEST['dT_CollectionTime'][$TD_Code] ? "'".$_REQUEST['dT_CollectionTime'][$TD_Code]."'" : "NULL").", T_DepartureTime = ".($_REQUEST['dT_DepartureTime'][$TD_Code] ? "'".$_REQUEST['dT_DepartureTime'][$TD_Code]."'" : "NULL"));

                        if(!in_array($TD_Code, array_keys($Item['Dates']))) {
                            $EventsArray['Dates'][$Date] = "NEW";
                        } else {
                            foreach($Item['Dates'] as $iTD_Code => $iDate) {
                                if($iDate['T_Places'] != $_REQUEST['dT_Places'][$iTD_Code] || $iDate['T_Limit'] != $_REQUEST['dT_Limit'][$iTD_Code] || $iDate['T_Paid'] != $_REQUEST['dT_Paid'][$iTD_Code]) {
                                    $EventsArray['Dates'][$Date]  = "EDIT";
                                    $EventsArray['Dates'][$Date] .= "\n                 ".$iDate['T_Places']." => ".$_REQUEST['dT_Places'][$iTD_Code];
                                    $EventsArray['Dates'][$Date] .= "\n                 ".$iDate['T_Limit']." => ".$_REQUEST['dT_Limit'][$iTD_Code];
                                    $EventsArray['Dates'][$Date] .= "\n                 ".$iDate['T_Paid']." => ".$_REQUEST['dT_Paid'][$iTD_Code];
                                    $EventsArray['Dates'][$Date] .= "\n                 ".$iDate['T_Guide']." => ".$_REQUEST['dT_Guide'][$iTD_Code];
                                }
                            }
                        }
                    }
                }
            }
        }
        if($_REQUEST['dI_Code']) {
            foreach($_REQUEST['dI_Code'] as $dI_Code) {
                if($dI_Code)
                    DB::Query("REPLACE INTO tripIncluded SET T_Code = $T_Code, dI_Code = $dI_Code");
            }
        }
        if($_REQUEST['dE_Code']) {
            foreach($_REQUEST['dE_Code'] as $dE_Code) {
                if($dE_Code)
                    DB::Query("INSERT INTO tripExtras (T_Code, dI_Code, TI_Price) VALUES ($T_Code, $dE_Code, ".($_REQUEST['TI_Price'][$dE_Code] ?: "NULL").") ON DUPLICATE KEY UPDATE TI_Price = ".($_REQUEST['TI_Price'][$dE_Code] ?: "NULL"));
            }
        }
        if($_REQUEST['dH_Code']) {
             foreach($_REQUEST['dH_Code'] as $dH_Code) {
                 if($dH_Code)
                    DB::Query("REPLACE INTO tripHotels SET T_Code = $T_Code, dH_Code = $dH_Code");
             }
        }

        if($_FILES['dImages']) {
            foreach($_FILES['dImages']['name'] as $fKey => $fName) {
                if($_FILES['dImages']['name'][$fKey] && !$_FILES['dImages']['error'][$fKey]) {
                    preg_match("/\.(\w+)$/", $_FILES['dImages']['name'][$fKey], $Extension);
                    $Filename = "$T_Code.$fKey.trip.".Cyr2Lat(str_replace($Extension[1], "", $_FILES['dImages']['name'][$fKey])).".".$Extension[1];
                    
                    move_uploaded_file($_FILES['dImages']['tmp_name'][$fKey], CLIENT_PATH."/data/$Filename");
                    DB::Query("INSERT INTO tripImages SET T_Code = ".$T_Code.", T_Image = '/data/$Filename'");
                }
            }
        }
        if($_FILES['T_Image']['name']) {
            preg_match("/\.(\w+)$/", $_FILES['T_Image']['name'], $Extension);
            $Filename = "$T_Code.image.".Cyr2Lat(str_replace($Extension[1], "", $_FILES['T_Image']['name'])).".".$Extension[1];
            
            move_uploaded_file($_FILES['T_Image']['tmp_name'], CLIENT_PATH."/data/$Filename");
            DB::Query("UPDATE tripItems SET T_Image = '/data/$Filename' WHERE T_Code = $T_Code");
            if($Item['T_Image']) {
                if(file_exists(CLIENT_PATH.$Item['T_Image']))
                    unlink(CLIENT_PATH.$Item['T_Image']);
                $ACF = _autoload("classActualFile");
                if(is_object($ACF))
                    $ACF->Expire($Item['T_Image']);
            }
        }
        if($_FILES['T_Scheme']['name']) {
            preg_match("/\.(\w+)$/", $_FILES['T_Scheme']['name'], $Extension);
            $Filename = "$T_Code.scheme.".Cyr2Lat(str_replace($Extension[1], "", $_FILES['T_Scheme']['name'])).".".$Extension[1];

            move_uploaded_file($_FILES['T_Scheme']['tmp_name'], CLIENT_PATH."/data/$Filename");
            DB::Query("UPDATE tripItems SET T_Scheme = '/data/$Filename' WHERE T_Code = $T_Code");
            if($Item['T_Scheme']) {
                if(file_exists(CLIENT_PATH.$Item['T_Scheme']))
                    unlink(CLIENT_PATH.$Item['T_Scheme']);
                $ACF = _autoload("classActualFile");
                if(is_object($ACF))
                    $ACF->Expire($Item['T_Scheme']);
            }
        }
        if($_FILES['dPlaces']) {
            foreach($_FILES['dPlaces']['name'] as $fKey => $fName) {
                if($_FILES['dPlaces']['name'][$fKey] && !$_FILES['dPlaces']['error'][$fKey]) {
                    preg_match("/\.(\w+)$/", $_FILES['dPlaces']['name'][$fKey], $Extension);
                    $Filename = "$T_Code.$fKey.place.".Cyr2Lat(str_replace($Extension[1], "", $_FILES['dPlaces']['name'][$fKey])).".".$Extension[1];

                    move_uploaded_file($_FILES['dPlaces']['tmp_name'][$fKey], CLIENT_PATH."/data/$Filename");
                    DB::Query("INSERT INTO tripPlaces SET T_Code = ".$T_Code.", T_Place = '/data/$Filename'");
                }
            }
        }

        if($Item['T_Price'] != $_REQUEST['T_Price'])
            $EventsArray['Price'] = "\n            ".$Item['T_Price']." => ".$_REQUEST['T_Price'];

            if(count($EventsArray))
                DB::Query("UPDATE tripItems SET events = CONCAT(events, '".classUserEvents::Events("EDIT", $EventsArray)."') WHERE T_Code = $T_Code");

        if($_REQUEST['Ite_Sef']) {
            sefRouter::Register("classTrip", $T_Code, $_REQUEST['Ite_Sef']);
        }

        @unlink(CACHE_PATH."/$T_Code.tourvars");
        @unlink(CACHE_PATH."/$T_Code.tourvars.part");

    }


    $Table = "EditTrip";
    include(EDIT_ONE_PATH);


    // не давайть менять активность если нет прав (вернуть обратно)
    if($_REQUEST['Update'] && !Permissions::getPermissions(170)) {
        DB::Query("UPDATE tripItems SET T_Visible = ".$Item['T_Visible']." WHERE T_Code = $T_Code");
    }

    if($T_Code) {
        $classTrip->ItemId = $T_Code;
        $Items = $classTrip->GetItems(false, true);

        if($Items[$T_Code]) {
            $Item = $classTrip->GetItemVars($Items[$T_Code], false);
        } else {
            $Item['Types'] = array();
            $Item['Regions'] = array();
            $Item['Cities'] = array();
            $Item['Dates'] = array();
        }

?>

        <table width="100%" class="border">
            <tr><th colspan="2" class="border">Дополнительные параметры:</th></tr>
            <tr>
                <td class="border">Стиль тура</td>
                <td class="border">
                    <?
                        $Types = DB::selectArray("SELECT * FROM dataTypes", "dT_Code");
                        foreach($Types as $tKey => $Type) {
                            $Options[$tKey] = $Type['dT_Name'];
                            $tValues = array_keys($Item['Types']);
                        }
                        $InputTypes = new InputMultiSelect("dT_Code", $tValues, $Options, "", "", true);
                        $InputTypes->dPrint();
                    ?>
                </td>
            </tr>
            <tr>
                <td class="border">Регион и город</td>
                <td class="border">
                    <?
                        foreach($Item['Regions'] as $rKey => $Region) {
                            $InputRegionsSelect = new InputRegionSelect("dR_Code[]", $rKey);
                            $InputRegionsSelect->Empty = true;
                            $InputRegionsSelect->dPrint();
                            foreach($Item['Cities'] as $cKey => $City) {
                                if($City['dR_Code'] == $rKey) {
                                    $InputCitiesSelect = new InputDBSelect("dC_Code[]", $cKey, "dataCities", "dC_Code", "dC_Name", "dR_Code = $rKey", "", "display:block;margin:3px 20px;");
                                    $InputCitiesSelect->Empty = true;
                                    $InputCitiesSelect->dPrint();
                                }
                            }
                            for($c = 1;$c <= 3;$c++) {
                                $InputCitiesSelect = new InputDBSelect("dC_Code[]", "", "dataCities", "dC_Code", "dC_Name", "dR_Code = $rKey", "", "display:block;margin:3px 20px;");
                                $InputCitiesSelect->Empty = true;
                                $InputCitiesSelect->dPrint();
                            }
                        }
                        $InputRegionsSelect = new InputRegionSelect("dR_Code[]", "");
                        $InputRegionsSelect->Empty = true;
                        $InputRegionsSelect->dPrint();
                    ?>
                </td>
            </tr>
            <tr>
                <td class="border">Отели</td>
                <td class="border">
                    <?
                        $Cond = "dC_Code IS NULL";
                        if($Item['Cities'])
                            $Cond .= " OR dC_Code IN (".join(", ", array_keys($Item['Cities'])).")";

                        if(count($Item['Hotels'])) {
                            $i = 0;
                            foreach ($Item['Hotels'] as $dH_Code => $Hotel) {
                                $i++;
                                $InputHotels = new InputDBSelect("dH_Code[$i]", $dH_Code, "dataHotels", "dH_Code", "dH_Name", $Cond, "dH_Name");
                                $InputHotels->Empty = true;
                                $InputHotels->dPrint();
                                echo "<br>";
                            }
                        }
                        for($j = $i + 1; $j <= $i + 3; $j++) {
                            $InputHotels = new InputDBSelect("dH_Code[$j]", fasle, "dataHotels", "dH_Code", "dH_Name", $Cond, "dH_Name");
                            $InputHotels->Empty = true;
                            $InputHotels->dPrint();
                            echo "<br>";
                        }
                    ?>
                </td>
            </tr>
            <tr>
                <td class="border">Программа тура</td>
                <td class="border">
                    <?
                        for($i = 1; $i <= $Item['T_Days'];$i++) {
                            $InputDaysText = new Textarea("dT_Days[$i]", preg_replace("/\<br \/\>/i", "", $Item['Days'][$i]['D_Desc']), 10, 20, "width:700px;height:200px");
                            echo "День $i<br>";
                            $InputDaysText->dPrint();
                            echo "<br>";
                        }
                    ?>
                </td>
            </tr>
            <tr>
                <td class="border">В стоимость включено</td>
                <td class="border">
                    <?
                        $Included = DB::selectArray("SELECT * FROM dataIncluded WHERE dI_Price <= 0", "dI_Code");
                        foreach($Included as $iKey => $Extra) {
                            $iOptions[$iKey] = $Extra['dI_Name'];
                            if($Item['Included'])
                                $iValues = array_keys($Item['Included']);
                        }
                        $InputIncluded = new InputMultiSelect("dI_Code", $iValues, $iOptions, "", "", true);
                        $InputIncluded->dPrint();
                    ?>
                </td>
            </tr>
            <tr>
                <td class="border">Дополнительно можно преобрести</td>
                <td class="border">
                    <div>
                        <?
                            if($Item['Extras']) {
                                foreach($Item['Extras'] as $eKey => $eData) {
                                    echo "<label style=\"padding:0.3em 0.5em;border-radius:4px;margin:0.5em;display:inline-block;background-color:rgb(239, 239, 239);\">";
                                    echo "<input type=\"checkbox\" name=\"dE_Code[$eKey]\" value=\"$eKey\" id=\"$eKey\" checked class=\"radio\">";
                                    echo " ".$eData['dI_Name']." (".$eData['dE_Price'].")</label>";
                                }
                            }
                        ?>
                    </div>
                    <i>Выберите категорию:</i> &nbsp;
                    <?
                        $ExtraCategory = new InputExtraCategoriesSelect("dI_Category", 0, "Ajax.Get('ajax_extras.php', 'dI_Category=' + this.value + '&T_Code=$T_Code', false, 'ajax_extras', 'loader', false);");
                        $ExtraCategory->dPrint();
                    ?>
                    <div id="ajax_extras" style="margin: 1em 0;width:50%;"></div>
                </td>
            </tr>
            <tr>
                <td class="border">Фотографии</td>
                <td class="border">
                    <?
                        $InputFile = new InputFile("T_Image", $Item['T_Image'], "Главное фото");
                        $InputFile->dPrint();
                        $InputFile = new InputFile("T_Scheme", $Item['T_Scheme'], "Схема маршрута");
                        $InputFile->dPrint();
//                        $InputFile = new InputFile("T_Place", $Item['T_Place'], "Место встречи");
//                        $InputFile->dPrint();
                    ?>
                    <br>Место встречи<br>
                    <a href="#" onClick="addFile('dPlaces');return false;" title="Добавить"><img src="/admin/images/str_add.gif"></a>
                    <div id="Files_Ite_ColdPlaces">
                        <?
                            $tripFiles = new tripFilesMultiSelect("dPlaces", $Item['T_Places'], "T_Place");
                            $tripFiles->dPrint();
                        ?>
                    </div>
                    <br>Остальные файлы<br>
                    <a href="#" onClick="addFile('dImages');return false;" title="Добавить"><img src="/admin/images/str_add.gif"></a>
                    <div id="Files_Ite_ColdImages">
                        <?
                            $tripFiles = new tripFilesMultiSelect("dImages", $Item['Images'], "T_Image");
                            $tripFiles->dPrint();
                        ?>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="border">Даты выездов</td>
                <td class="border">
                    <div id="Files_Ite_Col_Images">
                        <?
                            $tripDates = new tripDatesMultiSelect("dDates", $Item['Dates']);
                            $tripDates->dPrint();
                        ?>
                    </div>
                </td>
            </tr>
        </table>

<?
    }

?>
        <div id="SaveIt">
            <input type="hidden" name="Update" value="1">
            <input type="submit" value="Сохранить" class="green">
            &nbsp; &nbsp; &nbsp; &nbsp;
            <input type="button" value="Удалить" class="red" onclick="if(confirm('Удалить тур и все данные')) window.location.href = 'delete.php?T_Code=<? echo $T_Code; ?>';">
        </div>
    </form>

    </fieldset>
</div>

<? if(Permissions::getPermissions(167)) { ?>
    <div style="width:60%;margin:2em 10%;">
        <fieldset><legend class="Header">Events:</legend>
            <pre><? echo substr($Item['events'], -1500); ?></pre>
        </fieldset>
    </div>
<? } ?>

<?


    AdminFooter();

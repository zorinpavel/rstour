<?php

    include_once(ADMIN_PATH."/setup.php");
    include_once("setup.php");

    AdminHeaders();
    MainNavi("travel");

    ?>
    <div class="EditData">
        <fieldset>
            <legend class="Header">Выберите список</legend>
            <a href="./lists.php?list=regions">Регионы</a> &middot;
            <a href="./lists.php?list=cities">Города</a> &middot;
            <a href="./lists.php?list=types">Типы туров</a> &middot;
            <a href="./lists.php?list=transports">Транспорт</a>
        </fieldset>
    </div>
    <?

    switch($_REQUEST['list']) {
        case 'regions':
        default:
            $Table = "Regions";
            break;
        case 'cities':
            $Table = "Cities";
            $dR_Code = (int)$_REQUEST['dR_Code'] ? (int)$_REQUEST['dR_Code'] : false;
            if(!$dR_Code) {
                $dR_Code = DB::selectValue("SELECT dR_Code FROM dataRegions ORDER BY dR_Code LIMIT 1");
                if($dR_Code)
                    echo "<script language=\"JavaScript\">window.location = './lists.php?list=cities&dR_Code=$dR_Code';</script>";
            }
            break;
        case 'types':
            $Table = "Types";
            break;
        case 'transports':
            $Table = "Transports";
            break;
    }

    include(EDIT_DATA_PATH);

    AdminFooter();
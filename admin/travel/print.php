<?php

    include_once(ADMIN_PATH."/setup.php");
    include_once("setup.php");

    $BlankColor  = "#FFFFFF";
    $TodayColor  = "#E5FFE5";
    $FutureColor = "#FFFFCC";
    $ArchColor   = "#EEEEEE";
    $AlertColor  = "#FEE6E6";

    $classPDF = _autoload("classPDF");
    $classTrip = _autoload("classTrip");
    $classTripOrders = _autoload("classTripOrders");
    $classCompany = _autoload("classCompany");

    $mpdf = new mPDF("utf-8", "A4-L", "", "", "15", "15", "38", "18", "9", "9", "l");
    $mpdf->allow_charset_conversion = true;
    $mpdf->autoLangToFont = true;
    $mpdf->CSSselectMedia = "pdf";
//    $mpdf->SetProtection($Protections);

    ob_start();
    $classPDF->Ins2Php("template_header");
    $PageHeader = ob_get_clean();
    $mpdf->SetHTMLHeader($PageHeader);

    ob_start();
    $classPDF->Ins2Php("template_footer");
    $PageFooter = ob_get_clean();
    $mpdf->SetHTMLFooter($PageFooter);


    ob_start();
    echo "
        <!DOCTYPE html>
        <html lang=\"".PAGE_LANGUAGE."\">
        <head>
         <META charset=\"UTF-8\" />
         <link rel=\"author\" href=\"".DOMAIN_NAME."\" />
         <link rel=\"shortcut icon\" href=\"/favicon.ico\" />
         <link rel=\"stylesheet\" href=\"/jas/pdf.css\" media=\"pdf\" />
         <title>PDF ".DOMAIN_NAME."</title>
        </head>
        <body>
        <div class=\"pdf-body\">";

    $T_Code = (int)$_REQUEST['T_Code'];
    $T_Date = dateDecode($_REQUEST['T_Date']);

    $Orders = DB::selectArray("SELECT * FROM cartData WHERE T_Code = $T_Code AND T_Date = '$T_Date' AND Ord_Status NOT IN (1, 6, 7)", "Ord_Code");

    $Item = $classTrip->GetItems(array("T_Code" => $T_Code));
    echo "<br><h3>".$Item[$T_Code]['T_Name']."</h3>";
    echo "Дата: <b>".dateEncode($T_Date)."</b><br>";

    echo "<br><table width=\"100%\">";
    echo "<tr class=\"table-header\">";
    echo "<td>#</td>";
    echo "<td>ФИО</td>";
    echo "<td>Дата рождения</td>";
    echo "<td>Паспорт</td>";
    echo "<td>Телефон</td>";
    echo "<td>Место</td>";
//    echo "<td>Размещение</td>";
    echo "<td>Допуслуги</td>";
    echo "<td>Номер брони</td>";
    echo "<td>Агентство</td>";
    echo "</tr>";

    $Number = 1;
    foreach($Orders as $Ord_Code => $Order) {
        $Order = array_merge($Order, $classTripOrders->GetOrderVars($Order));
        foreach($Order['Travelers'] as $Key => $Traveler) {
            $Color = in_array($Order['Ord_Status'], array(2,3,5)) ? $BlankColor : (in_array($Order['Ord_Status'], array(4)) ? $FutureColor : $AlertColor);
            echo "<tr style=\"background-color:$Color;\"><td>".$Number++."</td>";
            echo "<td>".$Traveler['lastname']." ".$Traveler['name']." ".$Traveler['middlename']."</td>";
            echo "<td>".$Traveler['birthdate']."</td>";
            echo "<td>".$Traveler['passport']['number']."</td>";
            echo "<td>".$Traveler['phone']."</td>";
            echo "<td>".$Traveler['place']."</td>";
//            echo "<td>".$Traveler['address']."</td>";
            echo "<td>";
            if($Traveler['Extras']) {
                foreach($Traveler['Extras'] as $eKey => $Value) {
                    echo $Order['ExtrasData'][$eKey]['dI_Name']."<br>";
                }
            } else
                echo "-";
            echo "</td>";
            echo "<td>".$Order['Ord_Number']."</td>";
            echo "<td>".($Order['C_Code'] ? $Order['User']['Company']['C_Name'] : $Order['User']['lastname']." ".$Order['User']['name'])."</td>";
            echo "</tr>";
        }
    }
    echo "</table>";

    echo "</div></body></html>";

    $PageContent = ob_get_clean();

//    echo $PageContent;
//    die;

    $mpdf->WriteHTML($PageContent);
    $mpdf->Output();

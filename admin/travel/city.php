<?php

    include_once(ADMIN_PATH."/setup.php");
    include_once("setup.php");

    AdminHeaders();
    MainNavi("travel");


?>
    <div class="EditData">
        <fieldset>
            <legend class="Header">Выберите список</legend>
            <a href="./lists.php?list=regions">Регионы</a> &middot;
            <a href="./lists.php?list=cities">Города</a> &middot;
            <a href="./lists.php?list=types">Типы туров</a> &middot;
            <a href="./lists.php?list=transports">Транспорт</a>
        </fieldset>
    </div>
<?


    $dC_Code = (int)$_REQUEST['dC_Code'];
    if($dC_Code) {
        $Table = "City";
        include(EDIT_ONE_PATH);

        if($_FILES['dC_Images']) {
            foreach($_FILES['dC_Images']['name'] as $fKey => $fName) {
                if($_FILES['dC_Images']['name'][$fKey] && !$_FILES['dC_Images']['error'][$fKey]) {
                    $Filename = $dC_Code.".city.".Cyr2Lat($_FILES['dC_Images']['name'][$fKey]);
                    move_uploaded_file($_FILES['dC_Images']['tmp_name'][$fKey], CLIENT_PATH."/data/$Filename");
                    DB::Query("INSERT INTO dataCitiesImages SET dC_Code = ".$dC_Code.", dCI_Image = '/data/$Filename'");
                }
            }
        }

        $Images = DB::selectArray("SELECT dCI_Code, dCI_Image FROM dataCitiesImages WHERE dC_Code = $dC_Code", "dCI_Code");

        ?>
                <table class="border" width="100%">
                    <tr>
                        <td class="border">Фотографии</td>
                        <td class="border">
                            <a href="#" onClick="addFile('dC_Images');return false;" title="Добавить"><img src="/admin/images/str_add.gif"></a>
                            <div id="Files_Ite_ColdC_Images">
                                <?
                                    $tripFiles = new cityFilesMultiSelect("dC_Images", $Images);
                                    $tripFiles->dPrint();
                                ?>
                            </div>
                        </td>
                    </tr>
                </table>
                <div id="SaveIt">
                    <input type="hidden" name="Update" value="Update">
                    <input type="submit" value="Сохранить" class="green">
                </div>
            </form>

            </fieldset>
        </div>
        <?
    }


    AdminFooter();
<?php

    include_once("setup.php");

    require_once(COMMON_PATH."/JsHttpRequestLib/JsHttpRequest.php");
    $JsHttpRequest = new JsHttpRequest("utf-8");
    DB::getInstance();


    $dI_Category    = (int)$_REQUEST['dI_Category'];
    $T_Code         = (int)$_REQUEST['T_Code'];
    
    $Item['Extras'] = DB::selectArray("SELECT * FROM tripExtras WHERE T_Code = $T_Code", "dI_Code");
    $Extras = DB::selectArray("SELECT di.*, te.TI_Price FROM dataIncluded AS di LEFT JOIN tripExtras AS te ON di.dI_Code = te.dI_Code AND T_Code = $T_Code WHERE di.dI_Price > 0 AND di.dI_Category = $dI_Category", "dI_Code");
    
    if(count($Extras)) {
        foreach($Extras as $eKey => $Extra) {
            $eOptions[$eKey] = $Extra['dI_Name']." (".$Extra['dI_Price'].")";
            $ePrices[$eKey] = $Extra['TI_Price'];
            if($Item['Extras'])
                $eValues = array_keys($Item['Extras']);
        }
    
        $InputExtras = new InputExtras("dE_Code", $eValues, $eOptions, "", "", true, $ePrices);
        $InputExtras->dPrint();
    } else
        echo "<div class=\"error\">В этой категории нет дополнительных услуг</div>";
<?php

    include_once(ADMIN_PATH."/setup.php");
    include_once("setup.php");

    AdminHeaders();
    MainNavi("travel");


    $dH_Code = (int)$_REQUEST['dH_Code'];
    if($dH_Code) {
        $Table = "Hotel";
        include(EDIT_ONE_PATH);

        if($_FILES['dH_Images']) {
            foreach($_FILES['dH_Images']['name'] as $fKey => $fName) {
                if($_FILES['dH_Images']['name'][$fKey] && !$_FILES['dH_Images']['error'][$fKey]) {
                    $Filename = $dH_Code.".hotel.".Cyr2Lat($_FILES['dH_Images']['name'][$fKey]);
                    move_uploaded_file($_FILES['dH_Images']['tmp_name'][$fKey], CLIENT_PATH."/data/$Filename");
                    DB::Query("INSERT INTO dataHotelsImages SET dH_Code = ".$dH_Code.", dHI_Image = '/data/$Filename'");
                }
            }
        }

        $Images = DB::selectArray("SELECT dHI_Code, dHI_Image FROM dataHotelsImages WHERE dH_Code = $dH_Code", "dHI_Code");

        ?>
                <table class="border" width="100%">
                    <tr>
                        <td class="border">Фотографии</td>
                        <td class="border">
                            <a href="#" onClick="addFile('dH_Images');return false;" title="Добавить"><img src="/admin/images/str_add.gif"></a>
                            <div id="Files_Ite_ColdH_Images">
                                <?
                                    $tripFiles = new hotelFilesMultiSelect("dH_Images", $Images);
                                    $tripFiles->dPrint();
                                ?>
                            </div>
                        </td>
                    </tr>
                </table>
                <div id="SaveIt">
                    <input type="hidden" name="Update" value="Update">
                    <input type="submit" value="Сохранить" class="green">
                </div>
            </form>

            </fieldset>
        </div>
        <?
    } else {
        $Table = "Hotels";
        include(EDIT_DATA_PATH);
    }

    AdminFooter();
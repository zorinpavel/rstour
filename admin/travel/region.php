<?php

    include_once(ADMIN_PATH."/setup.php");
    include_once("setup.php");

    AdminHeaders();
    MainNavi("travel");


?>
    <div class="EditData">
        <fieldset>
            <legend class="Header">Выберите список</legend>
            <a href="./lists.php?list=regions">Регионы</a> &middot;
            <a href="./lists.php?list=cities">Города</a> &middot;
            <a href="./lists.php?list=types">Типы туров</a> &middot;
            <a href="./lists.php?list=transports">Транспорт</a>
        </fieldset>
    </div>
<?


    $dR_Code = (int)$_REQUEST['dR_Code'];
    if($dR_Code) {
        $Table = "Region";
        include(EDIT_ONE_PATH);
    }


    AdminFooter();
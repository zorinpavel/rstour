<?php

    $T_Code = (int)$_REQUEST['T_Code'];

    $classTrip = _autoload("classTrip");
    $classTrip->ItemId = $T_Code;
    $Items = $classTrip->GetItems(false, true);

    if($Items[$T_Code]) {
        $Item = $classTrip->GetItemVars($Items[$T_Code], false);

        DB::Query("DELETE FROM tripTypes WHERE T_Code = $T_Code");
        DB::Query("DELETE FROM tripRegions WHERE T_Code = $T_Code");
        DB::Query("DELETE FROM tripCities WHERE T_Code = $T_Code");
        DB::Query("DELETE FROM tripDays WHERE T_Code = $T_Code");
        DB::Query("DELETE FROM tripIncluded WHERE T_Code = $T_Code");
        DB::Query("DELETE FROM tripExtras WHERE T_Code = $T_Code");
        DB::Query("DELETE FROM tripDates WHERE T_Code = $T_Code");
        DB::Query("DELETE FROM tripHotels WHERE T_Code = $T_Code");

        $ACF = _autoload("classActualFile");

        foreach($Item['Images'] as $Key => $Image) {
            if(file_exists(CLIENT_PATH.$Image['T_Image']))
                unlink(CLIENT_PATH.$Image['T_Image']);
            $ACF->Expire($Image['T_Image']);
        }
        DB::Query("DELETE FROM tripImages WHERE T_Code = $T_Code");
    
        foreach($Item['T_Places'] as $pKey => $pImage) {
            if(file_exists(CLIENT_PATH.$pImage['T_Place']))
                @unlink(CLIENT_PATH.$pImage['T_Place']);
            $ACF->Expire($pImage['T_Place']);
        }
        DB::Query("DELETE FROM tripPlaces WHERE T_Code = $T_Code");
    
        if(file_exists(CLIENT_PATH.$Item['T_Image']))
            @unlink(CLIENT_PATH.$Item['T_Image']);
        $ACF->Expire($Item['T_Image']);
    
        if(file_exists(CLIENT_PATH.$pImage['T_Scheme']))
            @unlink(CLIENT_PATH.$pImage['T_Scheme']);
        $ACF->Expire($pImage['T_Scheme']);

        DB::Query("DELETE FROM tripItems WHERE T_Code = $T_Code");

        header("Location: /admin/travel/");
    }


<?php

    $Modules = array(
        "classHTML",
        "classMenu",
        "classScroll",
        "classMail",
        "classContentData",
        "classContentList",
        "classContent",
        "classContentFilter",
//        "classContentEdit",
        "classContentCart",
    );

    $Sections = array(
        "index" => array (
            "Path" => "/",
            "Sec_Code" => 1,
            "Help" => "Главная",
            "Hidden" => true,
        ),
        "users" => array (
            "Path" => "/users",
            "Sec_Code" => 2,
            "Help" => "Управление пользователями панели",
        ),
        "manage" => array (
            "Path" => "/manage",
            "Sec_Code" => 3,
            "Help" => "Структура",
        ),
        "content" => array (
            "Path" => "/content",
            "Sec_Code" => 4,
            "Help" => "Управление содержимым",
        ),
        "filemanager" => array (
            "Path" => "/filemanager",
            "Sec_Code" => 10,
            "Help" => "Управление файлами",
        ),
    );

    $SubSections = array(
        "index" => array (
            "index" => array(
                "Help" => "Juliet v. ".J_VER,
            ),
            "clear_cache" => array(
                "Name" => "Сбросить кэш",
                "Path" => "clear_cache.php",
                "Help" => "Удалить все временные файлы",
                "Sec_Code" => 11,
            ),
            "tags" => array(
                "Name" => "Теги",
                "Path" => "tags.php",
                "Help" => "Управление тегами",
                "Sec_Code" => 12,
            ),
        ),
        "users" => array (
            "users" => array (
                "Name" => "Список пользователей",
                "Path" => "users.php",
                "Help" => "Добавить / редактировать пользователя",
            ),
            "index" => array (
                "Name" => "Поск пользователя",
                "Path" => "index.php",
                "Help" => "Поиск пользователя",
            ),
            "groups" => array (
                "Name" => "Группы пользователей",
                "Path" => "groups.php",
                "Help" => "Группы пользователей",
                "Sec_Code" => 21,
            ),
            "permissions" => array (
                "Name" => "Доступ",
                "Path" => "permissions.php",
                "Help" => "Добавить / редактировать доступ",
                "Sec_Code" => 22,
            ),
        ),
        "manage" => array (
            "index" => array (
                "Name" => "Разделы сайта",
                "Path" => "index.php",
                "Help" => "Разделы сайта",
            ),
            "blocks" => array (
                "Name" => "Блоки",
                "Path" => "blocks.php",
                "Help" => "Блоки",
                "Sec_Code" => 31,
            ),
            "structure" => array (
                "Name" => "Структуры контента",
                "Path" => "structure.php",
                "Help" => "Структуры контента",
                "Sec_Code" => 32,
            ),
            "lists" => array (
                "Name" => "Списки",
                "Path" => "lists.php",
                "Help" => "Управление списками",
                "Sec_Code" => 33,
            ),
            "variables" => array (
                "Name" => "Переменные",
                "Path" => "variables.php",
                "Help" => "Управление переменными",
                "Sec_Code" => 34,
            ),
            "templates" => array (
                "Name" => "Шаблоны",
                "Help" => "Редактировать шаблоны",
                "Sec_Code" => 35,
                "Hidden" => true
            ),
        ),
        "content" => array (
            "index" => array (
                "Name" => "Управление контентом",
                "Path" => "index.php",
                "Help" => "Управление контентом",
            ),
            "structure" => array (
                "Name" => "Структуры контента",
                "Path" => "../manage/structure.php",
                "Help" => "Структуры контента",
            ),
            "lists" => array (
                "Name" => "Списки",
                "Path" => "../manage/lists.php",
                "Help" => "Управление списками",
            ),
        ),
    );

    array_push($Modules, "classUsers");
    include(MODULE_USERS_PATH."/export.php");

    array_push($Modules, "classSettings");
    include(MODULE_SETTINGS_PATH."/export.php");

    $Sections["siteusers"] = array (
        "Path" => "/siteusers",
        "Help" => "Управление пользователями сайта",
        "Sec_Code" => 5,
    );
    $SubSections["siteusers"] = array (
        "users" => array (
            "Name" => "Список пользователей",
            "Path" => "users.php",
            "Help" => "Добавить / редактировать пользователя",
        ),
        "index" => array (
            "Name" => "Поиск пользователя",
            "Path" => "index.php",
            "Help" => "Поиск пользователя",
        ),
        "groups" => array (
            "Name" => "Группы пользователей",
            "Path" => "groups.php",
            "Help" => "Группы пользователей",
            "Sec_Code" => 51,
        ),
        "permissions" => array (
            "Name" => "Права доступа",
            "Path" => "permissions.php",
            "Help" => "Права доступа",
            "Sec_Code" => 52,
        ),
        "fields" => array (
            "Name" => "Поля анкеты",
            "Path" => "fields.php",
            "Help" => "Поля анкеты пользователя",
            "Sec_Code" => 53,
        ),
        "companies" => array (
            "Name" => "Турагентства",
            "Path" => "companies.php",
            "Help" => "Заругистрированные турагентства",
            "Sec_Code" => 54,
        ),
        "logs" => array (
            "Name" => "Просматривать логи",
            "Sec_Code" => 55,
            "Hidden" => true,
        ),
        "edit" => array (
            "Name" => "Редактировать пользователя",
            "Sec_Code" => 56,
            "Hidden" => true,
        ),
    );

    $Sections['cart'] = array (
        "Path" => "/cart",
        "Sec_Code" => 9,
        "Help" => "Управление корзиной",
    );
    $SubSections['cart'] = array (
        "search" => array (
            "Name" => "Поиск заказов",
            "Path" => "search.php",
            "Help" => "Поиск закзов",
        ),
        "index" => array (
            "Name" => "Список заказов",
            "Path" => "index.php?Ord_Status=1",
            "Help" => "Список заказов",
        ),
        "params" => array (
            "Name" => "Параметры для корзины",
            "Path" => "params.php",
            "Help" => "Параметры для корзины",
            "Sec_Code" => 91,
        ),
        "delivery" => array (
            "Name" => "Способы доставки",
            "Path" => "delivery.php",
            "Help" => "Редактирование и настройка способов доставки",
            "Sec_Code" => 91,
        ),
        "payment" => array (
            "Name" => "Платежные системы",
            "Path" => "payment.php",
            "Help" => "Редактирование и настройка платежных систем",
            "Sec_Code" => 91,
        ),
        "view" => array (
            "Name" => "Просмотривать подробности заказов",
            "Help" => "Просмотр подробностей заказов",
            "Path" => "view.php",
            "Sec_Code" => 95,
            "Hidden" => true,
        ),
        "edit" => array (
            "Name" => "Редактировать заказы",
            "Path" => "edit.php",
            "Help" => "Редактирование и настройка заказов",
            "Sec_Code" => 92,
            "Hidden" => true,
        ),
        "delete" => array (
            "Name" => "Удалять заказы",
            "Path" => "delete.php",
            "Help" => "Удаление заказов",
            "Sec_Code" => 93,
            "Hidden" => true,
        ),
        "logs" => array (
            "Name" => "Просматривать логи",
            "Sec_Code" => 94,
            "Hidden" => true,
        ),
    );

    $Sections['imagepreview'] = array (
        "Path" => "/imagepreview",
        "Help" => "Обработка изображений",
        "Sec_Code" => 6,
    );

    array_push($Modules, "classSubscribe");
    include(MODULE_SUBSCRIBE_PATH."/export.php");

    $Sections['subscribe'] = array (
        "Path" => "/subscribe",
        "Sec_Code" => 7,
        "Help" => "Рассылка",
    );
    $SubSections["subscribe"] = array (
        "index" => array (
            "Name" => "Рассылки",
            "Path" => "index.php",
            "Help" => "Управление рассылками",
        ),
        "templates" => array (
            "Name" => "Шаблоны",
            "Path" => "templates.php",
            "Help" => "Добавить / редактировать шаблоны",
            "Sec_Code" => 71,
        ),
        "users" => array (
            "Name" => "Адреса",
            "Path" => "users.php",
            "Help" => "Управление адресами",
        ),
        "groups" => array (
            "Name" => "Группы пользователй",
            "Path" => "../siteusers/groups.php",
            "Help" => "Управление группами пользователей",
        ),
    );

    $Sections['security'] = array (
        "Path" => "/security",
        "Help" => "Защита",
        "Sec_Code" => 8,
    );
    $SubSections["security"] = array (
        "index" => array (
            "Name" => ".htaccess",
            "Path" => "index.php",
            "Help" => "Файл .htaccess",
            "Sec_Code" => 81,
        ),
        "robots" => array (
            "Name" => "robots.txt",
            "Path" => "robots.php",
            "Help" => "Файл robots.txt",
            "Sec_Code" => 82,
        ),
        "sitemap" => array (
            "Name" => "sitemap.xml",
            "Path" => "sitemap.php",
            "Help" => "Файл sitemap.xml",
            "Sec_Code" => 83,
        ),
        "permissions" => array (
            "Name" => "Права доступа",
            "Path" => "../siteusers/permissions.php",
            "Help" => "Права доступа",
        ),
    );

    array_push($Modules, "classForm");
    include(MODULE_FORM_PATH."/export.php");

    $Sections['form'] = array (
        "Path" => "/form",
        "Help" => "Управление 'Формами'",
        "Sec_Code" => 14,
    );
    $SubSections["form"] = array (
        "index" => array (
            "Name" => "Список форм",
            "Path" => "index.php",
            "Help" => "Список форм",
        ),
        "columns" => array (
            "Name" => "Поля формы",
            "Path" => "columns.php",
            "Help" => "Добавить / редактировать поля формы",
            "Sec_Code" => 141,
        ),
        "lists" => array (
            "Name" => "Списки",
            "Path" => "../manage/lists.php",
            "Help" => "Управление списками",
        ),
        "content" => array (
            "Name" => "Данные формы",
            "Path" => "content.php",
            "Help" => "Редактировать данные формы",
            "Sec_Code" => 142,
        ),
        "mails" => array (
            "Name" => "Адреса",
            "Path" => "mails.php",
            "Help" => "Редактировать адреса",
            "Sec_Code" => 143,
        ),
    );

    array_push($Modules, "classFaq");
    include(MODULE_FAQ_PATH."/export.php");

    $Sections['faq'] = array (
        "Path" => "/faq",
        "Help" => "Вопрос / Ответ",
        "Sec_Code" => 17,
    );
    $SubSections['faq'] = array (
        "index" => array (
            "Name" => "Данные",
            "Path" => "index.php",
            "Help" => "редактировать данные",
        ),
        "formslist" => array (
            "Name" => "Список форм",
            "Path" => "../form/index.php",
            "Help" => "Список форм",
        ),
        "delrecord" => array (
            "Name" => "Удалить содержимое",
            "Path" => "delrecord.php",
            "Sec_Code" => 172,
            "Hidden" => true,
        ),
    );

    array_push($Modules, "classBanner");
    include(MODULE_BANNER_PATH."/export.php");

    $Sections['banners'] = array (
        "Path" => "/banners",
        "Sec_Code" => 13,
        "Help" => "Баннерная реклама",
    );
    $SubSections["banners"] = array (
        "index" => array (
            "Name" => "Список аккаунтов",
            "Path" => "index.php",
            "Help" => "Добавить / редактировать аккаунты",
            "Sec_Code" => 131,
        ),
        "places" => array (
            "Name" => "Список мест",
            "Path" => "places.php",
            "Help" => "Добавить / редактировать баннерные места",
            "Sec_Code" => 132,
        ),
        "templates" => array (
            "Name" => "Шаблоны",
            "Path" => "templates.php",
            "Help" => "Добавить / редактировать шаблоны баннеров",
            "Sec_Code" => 133,
        ),
    );

    $Sections['travel'] = array (
        "Path" => "/travel",
        "Sec_Code" => 16,
        "Help" => "Туры",
    );

    $SubSections["travel"] = array (
        "index" => array (
            "Name" => "Туры",
            "Path" => "index.php",
            "Help" => "Список туров",
            "Sec_Code" => 161,
        ),
        "edit" => array (
            "Name" => "Редактировать тур",
            "Path" => "edit.php",
            "Help" => "Редактировать тур",
            "Sec_Code" => 162,
            "Hidden" => true,
        ),
        "active" => array (
            "Name" => "Активировать тур",
            "Sec_Code" => 170,
            "Hidden" => true,
        ),
        "lists" => array (
            "Name" => "Списки",
            "Path" => "lists.php",
            "Help" => "Редактировать списки",
            "Sec_Code" => 163,
        ),
        "included" => array (
            "Name" => "Дополнительные услуги",
            "Path" => "included.php",
            "Help" => "Редактировать список дополнительных услуг",
            "Sec_Code" => 164,
        ),
        "places" => array (
            "Name" => "Места",
            "Path" => "places.php",
            "Help" => "Редактировать список достопримечательностей",
            "Sec_Code" => 165,
        ),
        "hotels" => array (
            "Name" => "Отели",
            "Path" => "hotels.php",
            "Help" => "Редактировать список доступных отелей",
            "Sec_Code" => 166,
        ),
        "guides" => array (
            "Name" => "Гиды",
            "Path" => "guides.php",
            "Help" => "Редактировать список гидов",
            "Sec_Code" => 168,
        ),
        "logs" => array (
            "Name" => "Просматривать логи",
            "Sec_Code" => 167,
            "Hidden" => true,
        ),
        "pincodes" => array (
            "Name" => "Пин-коды",
            "Path" => "pincodes.php",
            "Help" => "Редактировать список доступных кодов",
            "Sec_Code" => 169,
        ),
    );

    array_push($Modules, "classTrip");
    array_push($Modules, "classTripCart");
    array_push($Modules, "classTripOrders");
    array_push($Modules, "classTripDispatch");
    array_push($Modules, "classCity");
    array_push($Modules, "classCompany");
    include(MODULE_TRIP_PATH."/export.php");

    array_push($Modules, "classPDF");

    $Sections['comments'] = array (
        "Path" => "/comments",
        "Sec_Code" => 18,
        "Help" => "Комментарии",
    );

    array_push($Modules, "classComments");
    include(MODULE_COMMENTS_PATH."/export.php");


    /* ******* */


    include(EDIT_PATH."/export.php");


    function AdminHeaders($body = true) {
        ?>
        <!DOCTYPE html>
        <html lang="ru">
        <head>
            <META charset="UTF-8" />
            <title>ADMIN MAIN</title>
            <link rel="stylesheet" href="<?=ADMIN_URL?>/css/admin.css" type="text/css">
            <link rel="stylesheet" href="<?=ADMIN_URL?>/css/dialog.css" type="text/css">
            <link rel="stylesheet" href="<?=ADMIN_URL?>/css/datepicker.css" type="text/css">
            <script type="text/javascript" src="<?=ADMIN_URL?>/js/cookies_lib.js"></script>
            <script type="text/javascript" src="<?=ADMIN_URL?>/js/browser.js"></script>
            <script type="text/javascript" src="<?=ADMIN_URL?>/js/JsHttpRequest.js"></script>
            <script type="text/javascript" src="<?=ADMIN_URL?>/js/http.js"></script>
            <script type="text/javascript" src="<?=ADMIN_URL?>/js/wopen.js"></script>
            <script type="text/javascript" src="<?=ADMIN_URL?>/js/datepicker.js"></script>
        </head>
        <body bgcolor="#FFFFFF">
    <?
    }



    function MainNavi($Section) {
        global $Sections, $SubSections, $CurrentSection;
        ?>
        <div class="mainSections">
            <div style="float:right;text-align:left" id="Auth">
                <img src="/admin/images/user.png" class="png" width=20 height=20 align=absmiddle hspace=5 title="Текущий пользователь"><?=$_SERVER['PHP_AUTH_USER']?><br>
                <img src="/admin/images/version.png" class="png" width=20 height=20 align=absmiddle hspace=5 title="Текущая версия"><?=J_VER?><br>
            </div>
            <?

                $Permissions = Permissions::getPermissions();

                foreach($Sections as $Key => $Properties) {
                    if($Permissions[$Properties['Sec_Code']]['Sec_Code'] || !$Properties['Sec_Code'] || getEditAuth()) {
                        ?><a href="<? echo ADMIN_URL.$Properties['Path']; ?>/" class="mainButton<? echo ($CurrentSection == $Key ? " selected" : ""); ?>"><img src="/admin/images/<? echo $Key; ?>.png" class="mainSection png" title="<? echo $Properties['Help']; ?>"></a><?
                        echo "\n";
                    }
                }

            ?>
        </div>
        <?

        SubSections();

        $curSubSection = str_replace(dirname($_SERVER['PHP_SELF'])."/", "", $_SERVER['PHP_SELF']);
        $curSubSection = str_replace(".php", "", $curSubSection);

        $Header = is_array($SubSections[$Section]) ? ($SubSections[$Section][$curSubSection]['Help'] ? $SubSections[$Section][$curSubSection]['Help'] : $Sections[$Section]['Help']) : $Sections[$Section]['Help'];
        echo "<H3 class=\"Header\">".$Header;
        echo "<a href=\"http://help.inweb.nnov.ru/".$Section."/".$curSubSection."\" onClick=\"wopen(this.href);return false;\"><img src=\"/admin/images/help.png\" class=\"png\" width=\"15\" height=\"15\" align=\"absmiddle\" hspace=\"5\" title=\"Справка\"></a>";
        echo "</H3>";
    }


    function SubSections() {
        global $Sections, $SubSections, $CurrentSection;
        $CurrentSection = $CurrentSection ? $CurrentSection : "index";

        $Permissions = Permissions::getPermissions();

        ?><div class="subSections">
            <?
                if($SubSections[$CurrentSection]) {
                    foreach($SubSections[$CurrentSection] as $Key => $Properties) {
                        $RootPath = false;
                        if($Key == "index") {
                            $Properties['Sec_Code'] = $Sections[$CurrentSection]['Sec_Code'];
                        } else if(!$Properties['Sec_Code'] && !preg_match("/\/(\w+)\//", $Properties['Path'], $RootPath)) {
                            $Properties['Sec_Code'] = $Sections[$CurrentSection]['Sec_Code'];
                        } elseif(!$Properties['Sec_Code']) {
                            if($SubSections[$RootPath[1]]) {
                                foreach($SubSections[$RootPath[1]] as $ssKey => $ssSection) {
                                    if($ssKey == $Key && $ssSection['Sec_Code']) {
                                        $Properties['Sec_Code'] = $ssSection['Sec_Code'];
                                        break;
                                    }
                                }
                            } else
                                $Properties['Sec_Code'] = $Sections[$RootPath[1]]['Sec_Code'];
                        }

                        if(!$Properties['Hidden'] && ($Permissions[$Properties['Sec_Code']] || getEditAuth())) {
                            ?><a href="<?= ADMIN_URL."/".$CurrentSection."/".$Properties['Path'] ?>"
                                 title="<?= $Properties['Help'] ?>"><?= $Properties['Name'] ?></a><?
                        }

                    }
                }
            ?>
        </div><?
    }


    function AdminFooter() {
        echo "</body></html>";
    }

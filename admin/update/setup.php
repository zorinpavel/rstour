<?php

class Update {

    protected static $BaseUrl = "download.inweb.nnov.ru";
    protected static $ConfigUrl = "/config";

    public static function checkVersion($Version = 0) {
        return version_compare($Version, J_VER);
    }


    public static function getConfig($asJSON = false, $LocalPath = false) {
        return self::XML2Array(self::getFile(self::$ConfigUrl."/config".($LocalPath ? "" : ".".J_VER).".xml", $LocalPath), $asJSON);
    }


    public static function getFile($FilePath, $LocalPath = false) {
        $FilePath = ($LocalPath ? CLIENT_PATH : self::$BaseUrl).$FilePath;

        if($LocalPath)
            return file_get_contents($FilePath);

        $curl_timeout = 10;
        $stream_context = @stream_context_create(
            array(
                'http' => array(
                    'timeout' => $curl_timeout,
                    'method' => "POST",
                    'content' => array(
                        'lkey' => "asd"
                    ),
                    'header' => "User-Agent:Juliet/1.0\r\n"
                )
            )
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $FilePath);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, $curl_timeout);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
        $opts = stream_context_get_options($stream_context);
        if (isset($opts['http']['method']) && strtolower($opts['http']['method']) == 'post') {
//            curl_setopt($ch, CURLOPT_POST, true);
            if(isset($opts['http']['content'])) {
                if(!is_array($opts['http']['content']))
                    parse_str($opts['http']['content'], $opts['http']['content']);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $opts['http']['content']);
            }
        }
        $content = curl_exec($ch);

        $data = pathinfo($FilePath);
        $data['http_code'] = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if(!curl_errno($ch) && $data['http_code'] == 200)
            $data['content'] = $content;
        else {
            self::registerError($data['http_code']." / ".(curl_errno($ch) ? curl_errno($ch).": ".curl_error($ch) : ""));
            return false;
        }
        curl_close($ch);

        return $data['content'];
    }


    public static function registerError($ErrorStr) {
//        echo $ErrorStr;
    }

    public static function XML2Array($xml, $asJSON = false) {

        self::normalizeSimpleXML(simplexml_load_string($xml), $object);
        if($asJSON)
            return @json_encode($object);
        else
            return @json_decode(@json_encode($object), 1);

    }

    private static function normalizeSimpleXML($obj, &$result) {
        $data = $obj;
        if(is_object($data)) {
            $data = get_object_vars($data);
        }
        if(is_array($data)) {
            foreach ($data as $key => $value) {
                $res = null;
                self::normalizeSimpleXML($value, $res);
                if($key == '@attributes' && $key) {
                    $result = $res;
                } elseif($key == 'file' && $key && !is_array($res)) {
                    $result[$key] = array($res);
                } elseif($key == 'query' && $key && !is_array($res)) {
                    $result[$key] = array($res);
                } elseif($key == 'module' && $key && !isset($res[0])) {
                    $result[$key] = array($res);
                } else {
                    $result[$key] = $res;
                }
            }
        } else {
            $result = $data;
        }
    }

}
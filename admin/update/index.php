<?php
    
    include_once(ADMIN_PATH."/setup.php");
    include_once("setup.php");
    
    AdminHeaders();
    MainNavi("update");

?>
    <style type="text/css">
        #SaveIt {
            font-style: italic;
        }
    </style>

    <div class="EditData">
        <fieldset>
            <legend class="Header">Обновление системы</legend>
            <button class="blue" onclick="Send('GET', 'ajax.php', { do: 'check' }, document.getElementById('checkforupdates'), 'checkforupdates');">Проверить наличие обновлений</button>
            <div id="checkforupdates"></div>
        </fieldset>
    </div>

    <script type="text/javascript">
        var Config = <? echo str_replace("\"", "'",Update::getConfig(true)); ?>;
        var Sections = ["common", "admin", "database"];

        function Update(s, m, f, Try = false) {
            var SaveIt = document.getElementById("SaveIt");
            if(Config[Sections[s]]) {
                SaveIt.innerHTML = Config[Sections[s]].module[m].name + "... " + (Config[Sections[s]].module[m].file ? "(" + Config[Sections[s]].module[m].file[f] + ")" : "");

                Ajax.Get("ajax.php", { do: Sections[s], s: Sections[s], m: m, f: f }, function () {
                    if(this.responseJS.try) {
                        SaveIt.style.color = "brown";
                        SaveIt.innerHTML = this.responseJS.try + "<br>" + SaveIt.innerHTML;
                        if(!Try)
                            Update(s, m, f, true);
                        SaveIt.style.color = "brown";
                    } else if (this.responseJS.errors) {
                        SaveIt.style.color = "red";
                        for (var e in this.responseJS.errors)
                            SaveIt.innerHTML += "<br>" + this.responseJS.errors[e];
                        if(document.getElementById(Sections[s] + m))
                            document.getElementById(Sections[s] + m).style.backgroundColor = "#FEE6E6";
                    } else {
                        if(this.responseJS.attentions) {
                            SaveIt.style.color = "red";
                            for(var e in this.responseJS.attentions)
                                SaveIt.innerHTML += "<br>" + this.responseJS.attentions[e];
                            if(document.getElementById(Sections[s] + m))
                                document.getElementById(Sections[s] + m).style.backgroundColor = "#FCF8E5";
                        } else {
                            SaveIt.style.color = "inherit";
                            if(document.getElementById(Sections[s] + m))
                                document.getElementById(Sections[s] + m).style.backgroundColor = "#E5FFE5";
                        }

                        if(Config[Sections[s]].module[m].file && f < Config[Sections[s]].module[m].file.length - 1) {
                            Update(s, m, f + 1);
                        } else if(Config[Sections[s]].module[m].query && f < Config[Sections[s]].module[m].query.length - 1) {
                            Update(s, m, f + 1);
                        } else if(m < Config[Sections[s]].module.length - 1) {
                            Update(s, m + 1, 0);
                        } else if(s < Sections.length - 1) {
                            Update(s + 1, 0, 0);
                        } else {
                            SaveIt.innerHTML = "Все файлы загружены успешно";
                            ChangeVersion();
                        }
                    }
                });

            } else if (s < Sections.length - 1) {
                Update(s + 1, 0, 0);
            } else {
                SaveIt.innerHTML = "Все файлы загружены успешно";
                ChangeVersion();
            }
        }

        function ChangeVersion() {
            var SaveIt = document.getElementById("SaveIt");
            SaveIt.innerHTML = "Подготовка системы к новому запуску " + Config['nextversion'];

            Ajax.Get("ajax.php", { do: 'changeversion' }, function() {
                window.location = "/admin/";
            });
        }
    </script>
<?
    
    
    
    AdminFooter();
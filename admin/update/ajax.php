<?php
    
    error_reporting(E_ALL & ~(E_NOTICE | E_STRICT | E_DEPRECATED));

    include_once("setup.php");

    require_once(COMMON_PATH."/JsHttpRequestLib/JsHttpRequest.php");
    $JsHttpRequest = new JsHttpRequest("utf-8");

    $Action = $_REQUEST['do'];

    $Config = Update::getConfig();
    $Forbidden = Update::XML2Array(Update::getFile("/admin/update/config.xml", true));

    switch($Action) {
        case "check":
            if($Config && Update::checkVersion($Config['nextversion'])) {
                echo "<h3>Есть доступные обновления</h3>";
                if(is_array($Config['common']['module']) || is_array($Config['admin']['module']) || is_array($Config['database'])) {
                    ?><div style="color:red;margin-bottom:1em;">
                        <i>ВНИМАНИЕ! Есть локализация под проект, некоторые файлы не будут загружены.<br>
                            В некоторых случаях обновление может привести к потере работоспособности!<br>
                            Настоятельно рекомендуем сделать резервное копирование!
                        </i>
                    </div><?
                }
                echo "<table class=\"border\" width=\"70%\">";
                echo "<tr><th class=\"border\" colspan=\"3\">Обновить до версии ".$Config['nextversion']."</th></tr>";
                if(is_array($Config['common']['module'])) {
                    echo "<tr><th class=\"border\" colspan=\"3\">Модули</th></tr>";
                    foreach($Config['common']['module'] as $Key => $Object) {
                        echo "<tr id=\"common$Key\"><td class=\"border\" colspan=\"2\">".$Object['name']."</td><td class=\"border\">".$Object['description'].($Object['attantion'] ? "<br><small style=\"color:red;\">".$Object['attantion']."</small>" : "")."</td></tr>";
                    }
                }
                if(is_array($Config['admin']['module'])) {
                    echo "<tr><th class=\"border\" colspan=\"3\">Интерфейс</th></tr>";
                    foreach($Config['admin']['module'] as $Key => $Object) {
                        echo "<tr id=\"admin$Key\"><td class=\"border\" colspan=\"2\">".$Object['name']."</td><td class=\"border\">".$Object['description']."</td></tr>";
                    }
                }
//                if(is_array($Config['database'])) {
//                    echo "<tr><th class=\"border\" colspan=\"3\">База данных</th></tr>";
//                    foreach($Config['database']['module'] as $Key => $Object) {
//                        echo "<tr id=\"database$Key\"><td class=\"border\" colspan=\"2\">".$Object['name']."</td><td class=\"border\">".$Object['description']."</td></tr>";
//                    }
//                }
                echo "</table>";
                echo "<div id=\"SaveIt\"><button class=\"green\" onclick=\"Update(0, 0, 0);\">Начать обновление</button></div>";
            } else
                echo "<h3>У вас установлена последняя версия продукта</h3>";
            break;
        case "update":
        case "common":
        case "admin":
            $_RESULT = NULL;
            $BasePath = "/download/".$Config['nextversion'];
            $FilePath = $Config[$_REQUEST['s']]['module'][$_REQUEST['m']]['path'].$Config[$_REQUEST['s']]['module'][$_REQUEST['m']]['file'][$_REQUEST['f']];
            $PathInfo = pathinfo($FilePath);

            foreach($Forbidden[$_REQUEST['s']]['module'] as $Key => $Object) {
                if($Object['path'] == $Config[$_REQUEST['s']]['module'][$_REQUEST['m']]['path']) {
                    $_RESULT['attentions'][] = $Object['path'];
                    foreach($Object['file'] as $fKey => $File) {
                        if($Object['path'].$File == $FilePath) {
                            $_RESULT['attentions'][] = $FilePath;
                            Update::registerError($FilePath);
                            die;
                        }
                    }
                }
            }

            if(!is_dir(CLIENT_PATH.$PathInfo['dirname']."/")) {
                mkdir(CLIENT_PATH.$PathInfo['dirname'], 0755);
                $_RESULT['try'] = "Создание директории ".$PathInfo['dirname']."/";
            } else
                $FileContent = Update::getFile($BasePath.$FilePath, false);

            if(!$FileContent) {
                $_RESULT['errors'][] = "Ошибка получения файлов";
                $_RESULT['errors'][] = $BasePath.$FilePath;
            } else {
                $fp = fopen(CLIENT_PATH.$FilePath, "w");
                if(!$fp) {
                    $_RESULT['errors'][] = "Ошибка записи файлов";
                    $_RESULT['errors'][] = ADMIN_URL.$FilePath;
                    die;
                }
                fputs($fp, $FileContent);
                fclose($fp);
            }
            break;
        case "database":
            $Query = $Config[$_REQUEST['s']]['module'][$_REQUEST['m']]['query'][$_REQUEST['f']];
            preg_match("/(ALTER TABLE) (\w+)/i", $Query, $TableName);
            $TableExists = DB::selectValue("SHOW TABLES LIKE '".$TableName[2]."'");
            if($TableExists)
                DB::Query($Query);
            if(mysql_errno(DB::getInstance()))
                $_RESULT['errors'][] = mysql_error(DB::getInstance());
            break;
        case "delete":
            break;
        case "changeversion":
            $_RESULT['nextversion'] = $Config['nextversion'];
            $Setup = file_get_contents(CLIENT_PATH."/setup/setup.php");
            $Setup = preg_replace("/\"J_VER\", \"(.*)\"/i", "\"J_VER\", \"".$Config['nextversion']."\"", $Setup);
            $fp = fopen(CLIENT_PATH."/setup/setup.php", "w");
            fputs($fp, $Setup);
            fclose($fp);
            break;
        default:
            echo "NO Action";
            break;
    }
<?php


    class GoDelComment extends InputButton {
        function GoDelComment($Code) {
            $this->InputButton("Go$Code", "Удалить", "", "if(confirm('Вы действительно хотите удалить комментарий?')) { openUrl('del_comment.php?UC_Code=$Code', 'delFolder', this, true, 0, 200, 'Удалить содержимое'); }", "red");
        }
    }


    class GoAnswer extends InputButton {
        function GoAnswer($Code) {
            $Parent_Code = $Code;
            include_once("answer.php");
            $this->childrenComment = $childrenComment;
            if(!$this->childrenComment)
                $this->InputButton("Go$Code", "Написать ответ", "", "openUrl('answer.php?parent=$Code', 'GoAnswer_$Code');", "blue");
        }

        function dPrint() {
            if(!$this->childrenComment)
                parent::dPrint();
        }
    }


    $CommentsList = array(
        "TableName" => "Comments",
        "Id"            => "UC_Code",
        "Order"         => "UC_Time DESC",
        "Header"        => "Комментарии",
        "Headers"       => array("Время", "login", "email", "Комментарий"),
        "Fields"        => array("UC_Time", "login", "email", "UC_Text"),
        "Types"         => array("InputTime", "T15RO", "T15RO", "TA30"),
        "ButtonHeaders" => array("&nbsp;", "&nbsp;"),
        "Buttons"       => array("GoAnswer", "GoDelComment"),
        "Filters"       => array(),
        "FiltersNames"  => array(),
        "FiltersTypes"   => array(),
        "FiltersCompare" => array(),
        "EditOnly"      => 1,
        "Where"         => "UC_Parent = 0",
    );


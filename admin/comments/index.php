<?php

    include_once(ADMIN_PATH."/setup.php");
    include_once("setup.php");

    AdminHeaders();
    MainNavi("comments");


?>
    <script type="text/javascript">
        function sendIt(Button, url) {
            var Data = {}, formObj = Button.form;
            for(var i=0; i < formObj.elements.length; i++) {
                if(formObj.elements[i].type == "checkbox" || formObj.elements[i].type == "radio") {
                    if(formObj.elements[i].name && formObj.elements[i].value > 0 && formObj.elements[i].checked) {
                        Data[formObj.elements[i].name] = formObj.elements[i].value;
                    }
                } else {
                    if(formObj.elements[i].name) {
                        Data[formObj.elements[i].name] = formObj.elements[i].value;
                    }
                }
            }

            var target = formObj.elements['target'].value;
            var Obj = document.getElementById(target);
            Send("POST", url, Data, Obj, target);
            return false;
        }
    </script>
<?


    $Table = "CommentsList";
    include(EDIT_DATA_PATH);
    
    
    AdminFooter();

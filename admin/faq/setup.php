<?php


    class GoDelRecord extends InputButton {
        function GoDelRecord($Code) {
            if(Permissions::$Permissions[172]) {
                $this->InputButton("Go$Code", "Удалить", "", "if(confirm('Вы действительно хотите удалить запись?')) { openUrl('delrecord.php?Con_Code=$Code', 'delRecord', this, true, 0, 200, 'Удалить содержимое'); }", "red");
            }
        }
    }


    class GoContent extends InputButton {
        function GoContent($Code) {
            $this->InputButton("Go$Code", "Содержимое", "", "window.location = 'content.php?Lst_Code=$Code';", "green");
        }
    }


    class InputFaqSelect extends InputDBSelect {
        function InputFaqSelect($Name, $Value, $onChange = "") {
            $this->InputDBSelect($Name, $Value, "formList", "Lst_Code", "Lst_Name", "", "Lst_Order, Lst_Code", "", $onChange);
        }
    }


    $faqContent = array(
        "Header"        => "Содержимое",
        "TableName"     => "faqContent",
        "Id"            => "Con_Code",
        "Order"         => "Con_Order DESC, Con_Code DESC",
        "Headers"       => array("Дата", "Вопрос", "Ответ", "Дата", "[X]"),
        "Fields"        => array("Con_Date", "Con_Data", "Ans_Data", "Ans_Date", "Con_State"),
        "Types"         => array("InputTime", "faqContentElem", "TA50", "InputDateTime", "InputCheckbox"),
        "DBTypes"         => array("CHAR", "IGNORE", "CHAR", "CHAR/NULL", "INT"),
        "Buttons"       => array("GoDelRecord"),
        "ButtonHeaders" => array(""),
        "Filters"       => array("Lst_Code"),
        "FiltersNames"  => array("Форма"),
        "FiltersTypes"   => array("InputFaqSelect"),
        "FiltersCompare" => array("N"),
        "EditOnly" => 1
    );


    class faqContentElem  {

        function faqContentElem($Name, $Value) {
            $this->Name = $Name;
            $this->Value = $Value;
        }

        function dPrint() {
            $Lst_Code = $_REQUEST['Lst_Code'] ? $_REQUEST['Lst_Code'] : 1;
            $Value = json_decode($this->Value, true);

            $fieldsLabel = DB::selectArray("SELECT Frm_Field, Frm_Id, Frm_Type FROM formFields WHERE Lst_Code='$Lst_Code' ORDER BY Frm_Order", "Frm_Code");

            echo "<table cellpadding=0 cellspacing=1 border=0 bgcolor=\"#FEFEFE\" width=100%>";
            $i = 0;
            $Fields = array();
            foreach($fieldsLabel as $flk => $flv) {
                $Fields[$i]["Type"] = $fieldsLabel[$flk]["Frm_Type"];
                $Fields[$i]["Label"] = $fieldsLabel[$flk]["Frm_Field"];
                $Fields[$i]["Id"] = $fieldsLabel[$flk]["Frm_Id"];

                if(is_array($Value[$Fields[$i]["Id"]])) {
                    $Fields[$i]["Value"] = "";
                    $c = 1;
                    while(list($k, $v) = each($Value[$Fields[$i]["Id"]])) {
                        $Fields[$i]["Value"] .= $v;
                        if($c != count($Value[$flk])) {
                            $Fields[$i]["Value"] .= ", ";
                        }
                        $c++;
                    }
                } else {
                    $Fields[$i]["Value"] = $Value[$Fields[$i]["Id"]];
                    unset($Value[$Fields[$i]["Id"]]);
                }
                $i++;
            }
            foreach($Fields as $k => $v) {
                if($Fields[$k]["Type"] == 7) {
                    echo "<tr><td valign=top class=border colspan=2>&nbsp;&nbsp;&nbsp;<i>".$Fields[$k]["Label"]."</i></td></tr>";
                } elseif($Fields[$k]["Type"] == 6) {
                    echo "<tr><td valign=top class=border width=35%>".$Fields[$k]["Label"]."</td><td valign=top class=border style=\"background:#FEFEFE\"><a href=\"/data/mail/".$Fields[$k]["Value"]."\" target=\"_blank\">".$Fields[$k]["Value"]."</a></td></tr>";
                } elseif($Fields[$k]["Type"] == 2) {
                    echo "<tr><td valign=top class=border width=35%>".$Fields[$k]["Label"]."</td><td valign=top class=border style=\"background:#FEFEFE\">".$Fields[$k]["Value"]."</td></tr>";
                } else {
                    echo "<tr><td valign=top class=border width=35%>".$Fields[$k]["Label"]."</td><td valign=top class=border style=\"background:#FEFEFE\">".$Fields[$k]["Value"]."</td></tr>";
                }
            }
            if(is_array($Value)) {
                echo "<tr><td colspan=2>Дополнтельные поля:</td></tr>";
                foreach($Value as $k => $v) {
                    echo "<tr><td>$k:</td><td>$v</td></tr>";
                }
            }
            echo "</table>";
        }
    }


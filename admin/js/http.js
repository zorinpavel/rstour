
function openUrl(url, target, parentObj, center, Width, Height, Title, ClassName, Styles) {
    var Obj = document.getElementById(target) || createObj(target);
    var Data = {};
    if(parentObj) {
        hideSelect();
        Obj = openFloatDiv(Obj, target, parentObj, center, Width, Height, Title, ClassName, Styles);
    }

    Ajax.Send("GET", url, Data, false, target);
    return false;

}

function sendForm(formObj, pObj) {
    var url = formObj.action, Data = {};
    for(var i=0; i < formObj.elements.length; i++) {
        if(formObj.elements[i].type == "file") {
        } else {
            if(formObj.elements[i].type == "checkbox" || formObj.elements[i].type == "radio") {
                if(formObj.elements[i].name && formObj.elements[i].value > 0 && formObj.elements[i].checked) {
                    Data[formObj.elements[i].name] = formObj.elements[i].value;
                }
            } else {
                if(formObj.elements[i].name) {
                    Data[formObj.elements[i].name] = formObj.elements[i].value;
                }
            }
        }
    }

    var target = formObj.elements['target'].value;
    var Obj = document.getElementById(target) || createObj(target);
    if(pObj) {
        hideSelect();
        Obj = openFloatDiv(Obj, target, pObj);
    }

    Ajax.Post(url, Data, false, target);

    return false;
}


function launchJavascript(responseText) {
    var ScriptFragment = '<script.*?>((\n|\r|.)*?)<\/script>';
    var match    = new RegExp(ScriptFragment, 'img');
    var scripts  = responseText.match(match);

    if(scripts) {
        var js = '';
        for(var s = 0; s < scripts.length; s++) {
            var match = new RegExp(ScriptFragment, 'im');
            js += scripts[s].match(match)[1];
        }
        eval(js);

        return true;
    }

    return false;
}


function openFloatDiv(Obj, target, parentObj, center, Width, Height, Title, ClassName, Styles) {
    /*
     замена Obj как объект с id=target на
     Obj как TextDiv внутри div id='win_' + target
     */
    Styles = Styles || {};
    Styles.Width = (Width ? parseInt(Width) : 500) + "px";
    Styles.Height = (Height ? parseInt(Height) : 300) + "px";

    var elXY = new Coords(0,0);

    if(!center) {
        elXY = getPos(parentObj);
        Styles.top = elXY.y + 20 + 'px';
        if((elXY.x + 10 + Obj.clientWidth) > document.body.scrollWidth) {
            Styles.left = document.body.scrollWidth - Obj.clientWidth - 10 + 'px';
        } else {
            Styles.left = elXY.x + 10 + 'px';
        }
    }

    var styleString = function() {
        var str = "display:block;";
        for(var prop in Styles) {
            str += prop + ":" + Styles[prop] + ";";
        }

        return str;
    }

    Obj.style = styleString();

    if(document.getElementById('win_' + target))
        return Obj;

    if(!document.getElementById("Back")) {
        var Back = document.createElement("div");
        Back.className = "background";
        Back.style.display = 'block';
        Back.id = "Back";
        document.body.appendChild(Back);
    } else {
        document.getElementById("Back").style.display = "block";
    }

    var TextDiv = document.createElement("div");
    Obj.id = 'win_' + target;
    TextDiv.id = target;
    TextDiv.className = "content";

    var Header = document.createElement("div");
    Header.className = "header";

    var A = closeImg(Obj, document.body, "Закрыть");
    Header.appendChild(A);

    var Title = Title || "&nbsp;";
    var TitleD = document.createElement("h2");
    TitleD.innerHTML = Title;
    Header.appendChild(TitleD);

    Obj.appendChild(Header);

    Obj.appendChild(TextDiv);

    return TextDiv; // сюда идет responseText
}


function closeImg(Parent, sParent, title) {  /* Parent - то что удаляем; sParent - откуда удаляем */
    var A = document.createElement("a");
    A.innerHTML = "&nbsp;";
    A.id = "closeButton";
    A.title = title;
    A.onclick = function() {
        Destroy(Parent, sParent);
        return false;
    };
    return A;
}


function createObj(target) {
    var Div = document.createElement("div");
    Div.className= 'floatDiv';
    Div.id = target;
    document.body.appendChild(Div);
    return Div;
}


function Destroy(dObj, fromObj) {
    showSelect();
    document.getElementById("Back").style.display = "none";
    if(dObj) {
        if(dObj.parentNode) {
            if(dObj.parentNode == fromObj)
                fromObj.removeChild(dObj);
            else
                document.removeChild(dObj);
            dObj = null;
        } else {
            dObj = null;
        }
    }
}


function getPos(elem) {
    var retVal = new Coords(0,0);
    while(elem.offsetParent != null && elem.tagName != 'BODY') {
        retVal.x += elem.offsetLeft;
        retVal.y += elem.offsetTop;
        elem = elem.offsetParent
    }
    if (browser && browser.isFF) {
        retVal.x -= 0;
        retVal.y -= 0;
    }
    return retVal;
}


function Coords(x,y) {
    this.x=parseInt(x);
    this.y=parseInt(y);
}


function hideSelect() {
    var selArray=(document.getElementsByTagName)?
        document.getElementsByTagName('SELECT'):
        ((document.all)?document.all.tags('SELECT'):null);
    if (selArray) {
        for (i=0;i<selArray.length;i++) {
            selArray[i].style.visibility='hidden';
        }
    }
}


function showSelect() {
    var selArray=(document.getElementsByTagName)?document.getElementsByTagName('SELECT'):((document.all)?document.all.tags('SELECT'):null);
    if (selArray) for (i=0;i<selArray.length;i++) selArray[i].style.visibility='visible';
}


var Ajax = {

    Send: function(Method, Url, Query, CallbackFunction, target, MarkerView, Cache) {
        var req = new JsHttpRequest();
        var TargetElement = document.getElementById(target) || target;
        var MarkerView = MarkerView || "loader";

        if(Method == "GET" && !!Cache)
            req.caching = true;

        req.onreadystatechange = function() {
            if (req.readyState == 4) {
                if(TargetElement)
                    TargetElement.innerHTML = req.responseText;
                launchJavascript(req.responseText);

                if(CallbackFunction)
                    CallbackFunction.call(req);
            } else {
                if(TargetElement) {
                    var inHTML = "";
                    switch (req.readyState) {
                        case 0:
                            inHTML = "не инициализирован";
                            break;
                        case 1:
                            inHTML =  "загрузка...";
                            break;
                        case 2:
                            inHTML = "загружено";
                            break;
                        case 3:
                            inHTML = "в процессе...";
                            break;
                        case 4:
                            inHTML = "готово";
                            break;
                        default:
                            inHTML = "неизвестное состояние";
                    }
                    if(MarkerView != "none" && MarkerView != "opacity")
                        TargetElement.innerHTML = "<img src=\"/admin/images/" + MarkerView + ".gif\" title=\"" + inHTML + "\" align=\"center\" />";
                }
            }
        };
        req.open(Method, Url, true);
        req.send(Query);
    },

    Get: function(Url, Query, CallbackFunction, target, MarkerView, Cache) {
        this.Send("GET", Url, Query, CallbackFunction, target, MarkerView, Cache);
    },

    Post: function(Url, Query, CallbackFunction, target, MarkerView) {
        this.Send("POST", Url, Query, CallbackFunction, target, MarkerView);
    }

};


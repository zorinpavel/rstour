function wopen(href, width, height) {
    width = width ? width : 900;
    height = height ? height : 700;
    window.open(href, "help", "width=" + width + ", height=" + height + ", top=100, left=100, resizable=yes, menubar=no, toolbar=no, status=yes, scrollbars=yes");
    return false;
}

var Cnt = 1;
function addFile(Col_Code, fCnt) {
    Cnt++;
    Cnt = (fCnt > Cnt) ? fCnt : Cnt;
    document.getElementById("Files_Ite_Col" + Col_Code).innerHTML += "<div id=\"Ite_Col" + Col_Code + "_" + Cnt + "\"></div>";
    openUrl('file_add.php?Cnt=' + Cnt + '&Col_Code=' + Col_Code, 'Ite_Col' + Col_Code + '_' + Cnt);
}

function addField(Col_Code) {
    Cnt++;
    document.getElementById("Field_Ite_Col" + Col_Code).innerHTML += "<div id=\"Ite_Col" + Col_Code + "_" + Cnt + "\"></div>";
    openUrl('field_add.php?Cnt=' + Cnt + '&Col_Code=' + Col_Code, 'Ite_Col' + Col_Code + '_' + Cnt);
}

function addTField(Col_Code) {
    Cnt++;
    document.getElementById("Field_Ite_Col" + Col_Code).innerHTML += "<div id=\"Ite_Col" + Col_Code + "_" + Cnt + "\"></div>";
    openUrl('tfield_add.php?Cnt=' + Cnt + '&Col_Code=' + Col_Code, 'Ite_Col' + Col_Code + '_' + Cnt);
}

function SelectAll(Checker) {
    for(var i=0; i < Checker.form.elements.length; i++) {
        var Element = Checker.form.elements[i];
        if(Element.name && Element.name.indexOf('SelectAll') >= 0) {
            if(Checker.checked)
                Element.checked = true;
            else
                Element.checked = false;
        }
    }
}

function T50Count(Input) {
    document.getElementById("count_" + Input.name).innerHTML = Input.value.length;
}
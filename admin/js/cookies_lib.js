function del_in_array(value, arr) {
    var len = arr.length;
    for(var i=0; i < len; i++) {
        if(arr[i] == value)
            arr = arr.splice(i, 1);
    }
    return arr;
}

function CreateCookie(name, value, expiredays) {
    var todayDate = new Date();
    todayDate.setDate(todayDate.getDate() + expiredays );
    document.cookie = name + ' = ' + value + '; path=/; expires=' + todayDate.toGMTString() + ';'
    return ReadCookie(name);
}

function ReadCookie(cookiename) {
    var numOfCookies = document.cookie.length;
    var nameOfCookie = cookiename + "=";
    var cookieLen = nameOfCookie.length;
    var x = 0;
    while (x <= numOfCookies) {
        var y = (x + cookieLen);
        if (document.cookie.substring(x,y) == nameOfCookie)
            return (extractCookieValue(y));
        x = document.cookie.indexOf(" ", x) + 1;
        if (x == 0)
            break;
    }
    return null;
}

function extractCookieValue(val) {
    if ((endOfCookie = document.cookie.indexOf(";", val)) == -1 ) {
        endOfCookie = document.cookie.length;
    }
    return unescape(document.cookie.substring(val, endOfCookie));
}

function deleteCookie(name, val) {
    var todayDate = new Date();
    todayDate.setDate(todayDate.getDate() - 1);
    document.cookie = name + "=" + val + "; expires=" + todayDate.toGMTString() + ";";
}

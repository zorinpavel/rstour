var Cookie = ReadCookie(CookieName) || CreateCookie(CookieName, "", 20);
Cookie = Cookie.split("|");

function ShowSec(sec, plus) {
    if (sec <= 0) return;
    var o, id;
    id = 'Sec_' + sec;
    if(o = document.getElementById(id)) {
        del_in_array(sec, Cookie);
        if (o.style.display == 'none') {
            Cookie[Cookie.length] = sec;
            o.style.display = '';
            plus.innerHTML = "<img src=\"/admin/images/folder_m.gif\" class=\"icon\">";
        } else {
            o.style.display = 'none';
            plus.innerHTML = "<img src=\"/admin/images/folder_p.gif\" class=\"icon\">";
        }
    }
    CreateCookie(CookieName, Cookie.join("|"), 20);
}

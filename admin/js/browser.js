function Browser() {
    var s, i, ua;

    ua = navigator.userAgent;
    this.isIE    = false;
    this.isNS    = false;
    this.isFF    = false;
    this.isOpera = false;
    this.version = null;

    this.minIE = 5;
    this.minNS = 7;
    this.minFF = 1;
    this.minOpera = 8;

    this.goodIE = 5.5;
    this.goodNS = 7;
    this.goodFF = 1;
    this.goodOpera = 7;

    this.ProcessMap = function() {
        if (this.isIE && this.version < this.minIE)
            return false;
        if (this.isNS && this.version < this.minNS)
            return false;
        if (this.isFF && this.version < this.minFF)
            return false;
        if (this.isOpera && this.version < this.minOpera)
            return false;

        if (!this.isIE && !this.isNS && !this.isFF && !this.isOpera)
            return false;

        return true;
    };

    if (window.opera) {
        this.isOpera = true;

        if ((i = ua.indexOf(s = "Opera/")) >= 0)
            this.version = parseFloat(ua.substr(i + s.length));
        else if ((i = ua.indexOf(s = "Opera")) >= 0)
            this.version = parseFloat(ua.substr(i + s.length));

        return;
    }
    else {
        s = "MSIE";
        if ((i = ua.indexOf(s)) >= 0) {
            this.isIE = true;
            this.version = parseFloat(ua.substr(i + s.length));
            return;
        }

        s = "Netscape6/";
        if ((i = ua.indexOf(s)) >= 0) {
            this.isNS = true;
            this.version = parseFloat(ua.substr(i + s.length));
            return;
        }

        s = "Netscape/";
        if ((i = ua.indexOf(s)) >= 0) {
            this.isNS = true;
            this.version = parseFloat(ua.substr(i + s.length));
            return;
        }

        s = "Firefox/";
        if ((i = ua.indexOf(s)) >= 0) {
            this.isFF = true;
            this.version = parseFloat(ua.substr(i + s.length));
            return;
        }

        if (navigator.product == "Gecko") {
            this.isNS = true;
            this.version = (parseInt(navigator.productSub) > 20020800 ? 7 : 5);
            return;
        }

        this.isNS = true;
        this.version = 7;
        return;
    }
};

Browser.prototype.oldVersionDisclaimer = function() {
    if (this.isIE)
        document.write("Please, update your Internet Explorer up to " + this.minIE + ".x or higher");
    else if (this.isNS)
        document.write("Please, update your NetScape Navigator up to " + this.minNS + ".x or higher");
    else if (this.isFF)
        document.write("Please, update your Firefox up to " + this.minFF + ".x or higher");
    else if (this.isOpera)
        document.write("Please, update your Opera up to " + this.minOpera + ".x or higher");
};

Browser.prototype.goodVersionDisclaimer = function() {
    var Msg = "";
    if (this.isIE && this.version < this.goodIE)
        Msg = "Работает в режиме совместимости. Пожалуйста, обновите версию вашего Internet Explorer до " + this.goodIE + " или выше";
    return Msg;
};

var browser = new Browser();

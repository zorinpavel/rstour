<?

    include_once(ADMIN_PATH."/setup.php");
    include_once("setup.php");


    AdminHeaders();
    MainNavi("users");

    if($_REQUEST['Update'] && $_REQUEST['per']) {
        DB::Query("TRUNCATE TABLE AdmPermissions");
        foreach($_REQUEST['per'] as $Sec_Code => $Groups) {
            foreach($Groups as $Gro_Code => $Value)
                if($Value)
                    DB::Query("REPLACE INTO AdmPermissions SET Sec_Code = $Sec_Code, Gro_Code = $Gro_Code");
        }
    }

    $Groups = DB::selectArray("SELECT * FROM AdmGroups", "Gro_Code");

?>

    <div class="EditData">
    <fieldset>
    <legend class="Header">Права доступа</legend>

    <form enctype="multipart/form-data" name="Main" method="post">

        <table class="border">
            <tr>
                <th class="border">Раздел</th>
                <?
                    foreach($Groups as $Gro_Code => $Group) {
                        echo "<th class=\"border\">".$Group['Gro_Name']."</th>";
                    }
                ?>
            </tr>
            <?
                //       unset($Sections['index']);
                foreach($Sections as $Key => $Section) {
                    if($Section['Sec_Code']) {
                        echo "<tr><td class=\"border\">".$Section['Help']."</td>";
                        $Permissions = DB::selectArray("SELECT * FROM AdmPermissions WHERE Sec_Code = ".$Section['Sec_Code'], "Gro_Code");
                        foreach($Groups as $Gro_Code => $Group) {
                            $Selected = "";
                            if($Permissions[$Gro_Code])
                                $Selected = " checked";
                            echo "<td class=\"border\" align=\"center\"><input type=\"checkbox\" class=\"radio\" value=\"1\" name=\"per[".$Section['Sec_Code']."][$Gro_Code]\"$Selected></td>";
                        }
                        echo "</tr>";
                        if($SubSections[$Key]) {
                            foreach($SubSections[$Key] as $sKey => $sSection) {
                                if($sSection['Sec_Code']) {
                                    echo "<tr><td class=\"border\" style=\"padding-left:20px;\"><i>".$sSection['Name']."</i></td>";
                                    $Permissions = DB::selectArray("SELECT * FROM AdmPermissions WHERE Sec_Code = ".$sSection['Sec_Code'], "Gro_Code");
                                    foreach($Groups as $Gro_Code => $Group) {
                                        $Selected = "";
                                        if($Permissions[$Gro_Code])
                                            $Selected = " checked";
                                        echo "<td class=\"border\" align=\"center\"><input type=\"checkbox\" class=\"radio\" value=\"1\" name=\"per[".$sSection['Sec_Code']."][$Gro_Code]\"$Selected></td>";
                                    }
                                    echo "</tr>";
                                }
                            }
                        }
                    }
                }
            ?>
        </table>

        <input type="hidden" name="Update" value="1">
        <div id="SaveIt"><input type="submit" name="Update" value="Сохранить" class="green"></div>
    </form>

<?


    AdminFooter();

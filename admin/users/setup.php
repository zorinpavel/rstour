<?php


class GoUsrEdit extends InputButton {

    function GoUsrEdit($Code) {
        $this->InputButton("Go$Code", "Редактировать", "", "window.location = './edit.php?us_id=$Code';", "green");
    }
}


class GoUsrEditGroup extends InputButton {

    function GoUsrEditGroup($Code) {
        $this->Code = $Code;
        $this->InputButton("Go$Code", "Сменить группу", "", "window.location = './edit.php?us_id=$Code';", "blue");
    }


    function dPrint() {
        $r = db_query("SELECT g.Gro_Name FROM AdmUserGroups AS aug, AdmGroups AS g WHERE aug.Gro_Code = g.Gro_Code AND aug.us_id = ".$this->Code);
        while($Groups = mysql_fetch_assoc($r)) {
            $i++;
            echo "<b>".$Groups['Gro_Name']."</b>";
            if($i < mysql_num_rows($r))
                echo ", ";
        }
//        echo " &nbsp; ";
//        parent::dPrint();
    }
}


class AdmGroupDBSelect extends InputDBSelect {

    function AdmGroupDBSelect($Name, $Value) {
        $this->InputDBSelect($Name, $Value, "AdmGroups", "Gro_Code", "Gro_Name", "", "Gro_Order", "", $onChange);
        $this->Empty = 1;
    }
}


class InputHiddenUser extends InputHidden {

    function dPrint() {
        $result = db_query("SELECT login FROM AdmUsers WHERE us_id='".$this->Value."'");
        list($login) = mysql_fetch_row($result);
        echo "<b>$login</b><br>";
        parent::dPrint();
    }


    function InputHiddenUser($Name, $Value) {
        $this->InputHidden($Name, $_REQUEST['us_id']);
    }
}


$AdmGroups = array(
    "TableName"      => "AdmGroups",
    "Id"             => "Gro_Code",
    "Order"          => "Gro_Order",
    "Fields"         => array("Gro_Name", "Gro_Order"),
    "Types"          => array("T50", "T5"),
    "Uploads"        => array(),
    "Header"         => "Группы пользователей",
    "Headers"        => array("Название", "Порядок<br>вывода"),
    "Buttons"        => array(),
    "ButtonHeaders"  => array(),
    "Filters"        => array(),
    "FiltersNames"   => array(),
    "FiltersTypes"   => array(),
    "FiltersCompare" => array(),
    "EditOnly"       => 0,
);


$AdmUsers = array(
    "TableName"      => "AdmUsers",
    "Id"             => "us_id",
    "Order"          => "login",
    "Fields"         => array("login", "email"),
    "Types"          => array("T30", "T30"),
    "Uploads"        => array(),
    "Header"         => "Юзеры",
    "Headers"        => array("Логин", "email"),
    "Buttons"        => array("GoUsrEditGroup", "GoUsrEdit"),
    "ButtonHeaders"  => array("&nbsp;", "&nbsp;"),
    "Filters"        => array(),
    "FiltersNames"   => array(),
    "FiltersTypes"   => array(),
    "FiltersCompare" => array(),
    "EditOnly"       => 0,
);


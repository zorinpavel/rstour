<?php

    class classMailProperties extends classProperties {

        public function Template2Php(&$Template, $External = array(), $Eval = false) {
            parent::Template2Php($Template, $External, $Eval);

            $Template = preg_replace("/(?<=(?:href)\=\"|(?:action)\=\"|(?:src)\=\")(?=[^http:|\.|\?|#|mailto:])(.*?)(?=\")/", DOMAIN_URL."$1", $Template);
            $Template = preg_replace("/(?<=(?:url)\(\')(?=[^http:|\.|\?|#])(.*?)(?=\')/", DOMAIN_URL."$1", $Template);

            ob_start();
            eval("?>$Template<?");
            $Content = ob_get_contents();
            ob_end_clean();

            return $Content;
        }


        public function MailTemplate2Php($Template, $External = array(), $Eval = true) {
            $this->ParseWhile($Template);
            $this->ParseIf($Template);

            $this->FindAllTags($Template);

            foreach($this->Blocks as $Key => $Tag) {
                $BlockType  = $Tag["type"];
                $BlockName  = $Tag["name"];
                $BlockClass = $Tag["class"];

                $EvalStr = "";

                if($BlockType == INS_FUNCTION) {
                    $EvalStr = "<? \$this->$BlockName(); ?>";
                } elseif($BlockType == INS_VARIABLE) {
                    $EvalStr = eval("return \$External['$BlockName'];");
                } elseif ($BlockType == INS_DATA) {
                    if($Tag["external"])
                        $EvalStr = eval("return \$External['$BlockName'];");
                    else
                        $EvalStr = eval(" return \$this->Data['$BlockName'];");
                } elseif($BlockType == INS_BLOCK) {

                    if(file_exists(CACHE_PATH."/mail.template.$BlockName")) {
                        $EvalStr = eval("return file_get_contents(CACHE_PATH.\"/mail.template.$BlockName\");");
                    } else {
                        $EvalStr = "\$Obj = _autoload(\"$BlockClass\", \"$BlockName\", \$this); \$Obj->Action();";

                        ob_start();
                        eval($EvalStr);
                        $Content = ob_get_contents();
                        ob_end_clean();

                        $Filename = CACHE_PATH."/mail.template.$BlockName";
                        $fp = fopen($Filename, "w");
                        fputs($fp, $Content);
                        fclose($fp);
                        chmod($Filename, 0755);

                        $EvalStr = eval("return \$Content;");
                    }

                }

                $Template = str_replace($Tag["src"], $EvalStr, $Template);

            }

            $Template = preg_replace("/(?<=(?:href)\=\"|(?:action)\=\"|(?:src)\=\")(?=[^http:|\.|\?|#|mailto:])(.*?)(?=\")/", DOMAIN_URL."$1", $Template);
            $Template = preg_replace("/(?<=(?:url)\(\')(?=[^http:|\.|\?|#])(.*?)(?=\')/", DOMAIN_URL."$1", $Template);

            return $Template;
        }


        public function GetTmp($Tmp_Code=0) {
            $r = db_query("SELECT Tmp_Content FROM subscribeTemplates WHERE Tmp_Code = $Tmp_Code");
            list($Template) = mysql_fetch_row($r);
            return $Template;
        }


        public function GetParams($Mes_Code=0) {
            $Params = array();
            $r = db_query("SELECT MPa_Data, MPa_Value FROM subscribeMessageParams WHERE Mes_Code = $Mes_Code");
            while($Mes = mysql_fetch_assoc($r)) {
                $Params[$Mes['MPa_Data']] = $Mes['MPa_Value'];
            }
            return $Params;
        }


        function classMailProperties($sec = "", $Parent = "") {
            parent::classProperties($sec, $Parent);
        }

    }

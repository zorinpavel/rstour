<?php

    class classMail extends classMailProperties {

        public $SmtpConn = false;
        public $Errors = array();

        public $Smtp = MAIL_HOST;
        public $fromMail = MAIL_FROM;
        public $fromMailName = MAIL_FROM_NAME;
        public $adminMail = MAIL_ADMIN;
        public $adminMailName = MAIL_ADMIN_NAME;
        public $Pass = MAIL_PASS;
        public $Port = MAIL_PORT;
        public $Headers = "";
        public $Debug = false;

        public $ServerAuth = true;

        public $Properties = array(
            "public" => array(
                "Names" => array(
                    "mailTemplate",
                ),
                "Types" => array(
                    "InputTemplates",
                ),
                "Labels" => array(
                    "Шаблон письма по умолчанию",
                )
            ),
            "private" => array(
                "Names" => array(
                ),
                "Types" => array(
                ),
                "Labels" => array(
                )
            )
        );

        public $DefaultTemplates = array(
            "mailTemplate" => array(0 => "<ins type=\"data\" name=\"Message\">"),
        );

        public function GetClassName() {
            return __CLASS__;
        }

        function OpenConnection() {
            $this->SmtpConn = fsockopen($this->Smtp, $this->Port, $errno, $errstr, 10);

            if(!$this->SmtpConn) {
                array_push($this->Errors, "Соединение с сервером ".$this->Smtp." не прошло!");
                if(debug()) {
                    ?><pre><? print_r($this->Errors); ?></pre><?
                }
                fclose($this->SmtpConn);
                unset($this->SmtpConn);
                return;
            }

            $data = $this->getData($this->SmtpConn);
            fputs($this->SmtpConn, "EHLO ".DOMAIN_NAME."\r\n");
            $code = substr($this->getData($this->SmtpConn), 0, 3);
            if($code != 250) {
                array_push($this->Errors, "$code Ошибка приветсвия EHLO");
                if(debug()) {
                    ?><pre><? print_r($this->Errors); ?></pre><?
                }
                fclose($this->SmtpConn);
                unset($this->SmtpConn);
                return;
            }

            if($this->ServerAuth) {
                fputs($this->SmtpConn, "AUTH LOGIN\r\n");
                $code = substr($this->getData($this->SmtpConn), 0, 3);
                if($code != 334) {
                    array_push($this->Errors, "$code Сервер не разрешил начать авторизацию");
                    if(debug()) {
                        ?><pre><? print_r($this->Errors); ?></pre><?
                    }
                    fclose($this->SmtpConn);
                    unset($this->SmtpConn);
                    return;
                }

                fputs($this->SmtpConn, base64_encode($this->fromMail)."\r\n");
                $code = substr($this->getData($this->SmtpConn), 0, 3);
                if($code != 334) {
                    array_push($this->Errors, "$code Ошибка доступа к такому пользователю");
                    if(debug()) {
                        ?><pre><? print_r($this->Errors); ?></pre><?
                    }
                    fclose($this->SmtpConn);
                    unset($this->SmtpConn);
                    return;
                }


                fputs($this->SmtpConn,base64_encode($this->Pass)."\r\n");
                $code = substr($this->getData($this->SmtpConn), 0, 3);
                if($code != 235) {
                    array_push($this->Errors, "$code Неверный пароль");
                    if(debug()) {
                        ?><pre><? print_r($this->Errors); ?></pre><?
                    }
                    fclose($this->SmtpConn);
                    unset($this->SmtpConn);
                    return;
                }
            }
        }

        function CloseConnection() {
            if($this->SmtpConn) {
                fputs($this->SmtpConn, "RSET\r\n");
                fputs($this->SmtpConn, "QUIT\r\n");
                fclose($this->SmtpConn);
                unset($this->SmtpConn);
            }
        }

        function SendMailHold($mail, $Subject, $Message, $from = "") {
            $this->Hold = true;
            $this->SendMail($mail, $Subject, $Message, $from);
        }

        function SendMail($mail, $Subject, $Message, $from = "") {
            
            $mails = preg_split("/[\s,;]+/", $mail);
            if(count($mails) > 1) {
                foreach($mails as $mail)
                    $this->SendMail($mail, $Subject, $Message, $from);
    
                return;
            }
            
            if(preg_match("/^(\w|\.|-)+?@(\w|-)+?\.\w{2,4}($|\.\w{2,4})$/i", trim($mail))) {

                if(!$this->SmtpConn)
                    $this->OpenConnection();

                if($from != "")
                    $this->fromMail = $from;

                $Subject = $Subject ? $Subject : DOMAIN_NAME;

                $Headers  = "";
                $Headers  = $this->Headers;
                $Headers .= "From: =?utf-8?Q?".str_replace("+", "_", str_replace("%", "=", urlencode($this->fromMailName)))."?= <".$this->fromMail.">\r\n";
                $Headers .= "Reply-To: =?utf-8?Q?".str_replace("+", "_", str_replace("%", "=", urlencode($this->adminMailName)))."?= <".$this->adminMail.">\r\n";
                $Headers .= "To: =?utf-8?Q?".str_replace("+", "_", str_replace("%", "=", urlencode($mail)))."?= <$mail>\r\n";
                $Headers .= "Subject: =?utf-8?Q?".str_replace("+", "_", str_replace("%", "=", urlencode($Subject)))."?=\r\n";
                $Headers .= "Message-ID: <".$this->Uid.">\r\n";
                $Headers .= "Content-Transfer-Encoding: 8bit\r\n";
                $Headers .= "Content-Type: text/html; charset=utf-8\r\n";

                $size_msg = strlen($Headers."\r\n".$Message);

                if($this->SmtpConn) {
                    fputs($this->SmtpConn,"MAIL FROM: <".$this->fromMail."> SIZE=".$size_msg."\r\n");
                    $code = substr($this->getData($this->SmtpConn), 0, 3);
                    if($code != 250) {
                        array_push($this->Errors, "$code Сервер отказал в команде MAIL FROM");
                        if(debug()) {
                            ?><pre><? print_r($this->Errors); ?></pre><?
                        }
                        fclose($this->SmtpConn);
                        return;
                    }

                    fputs($this->SmtpConn,"RCPT TO: <".$mail.">\r\n");
                    $code = substr($this->getData($this->SmtpConn), 0, 3);
                    if($code != 250 AND $code != 251) {
                        array_push($this->Errors, "$code Сервер не принял команду RCPT TO $mail");
                        if(debug()) {
                            ?><pre><? print_r($this->Errors); ?></pre><?
                        }
                        fclose($this->SmtpConn);
                        return;
                    }

                    fputs($this->SmtpConn,"DATA\r\n");
                    $code = substr($this->getData($this->SmtpConn), 0, 3);
                    if($code != 354) {
                        array_push($this->Errors, "$code Сервер не принял DATA");
                        if(debug()) {
                            ?><pre><? print_r($this->Errors); ?></pre><?
                        }
                        fclose($this->SmtpConn);
                        return;
                    }

                    fputs($this->SmtpConn, $Headers."\r\n".$Message."\r\n.\r\n");
                    $code = substr($this->getData($this->SmtpConn), 0, 3);
                    if($code != 250) {
                        array_push($this->Errors, "$code Ошибка отправки письма");
                        if(debug()) {
                            ?><pre><? print_r($this->Errors); ?></pre><?
                        }
                        fclose($this->SmtpConn);
                        return;
                    }
                }

                if(!$this->Hold && $this->SmtpConn)
                    $this->CloseConnection();

            }

        }

        function getData($SmtpConn) {
            $data = "";
            while($str = fgets($SmtpConn, 515)) {
                $data .= $str;
                if(substr($str,3,1) == " ")
                    break;
            }
            if($this->Debug)
                echo "<pre>$data</pre>";
            return $data;
        }

        function classMail($sec = "", $Parent = "") {
            parent::classMailProperties($sec, $Parent);

            $this->Uid = md5(uniqid(time()));

            $this->Headers  = "Date: ".date("r")."\r\n";
            $this->Headers .= "X-Mailer: PHP/".phpversion()."\r\n";
            $this->Headers .= "X-Priority: 3 (Normal)\r\n";
            //            $this->Headers .= "Content-Type: multipart/mixed; boundary=\"".$this->Uid."\"\r\n";
            //            $this->Headers .= тело файла
            //            $this->Headers .= "Content-Type: text/html; charset=utf-8\r\n";

            $this->Data['AdminMail'] = $this->adminMail;
            $this->Data['AdminMailName'] = ($this->adminMailName != "") ? $this->adminMailName : $this->adminMail;
            $this->Data['DomainName'] = DOMAIN_NAME;

        }
    
        public static function getCss($url) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_REFERER, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch);
            curl_close($ch);
            return $result;
        }
    
    }

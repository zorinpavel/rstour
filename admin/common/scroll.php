<?php

    class InputScroll extends InputModules {
        function InputScroll($Name, $Value) {
            parent::InputModules($Name, $Value, "classScroll");
        }
    }

    class classScroll extends classProperties {
        var $Properties = array(
            "public" => array(
                "Names" => array(
                ),
                "Types" => array(
                ),
                "Labels" => array(
                )
            ),
            "private" => array(
                "Names" => array(
                    "PartLinks",
                    "Limit",
                    "Groups",
                    "Empty",
                ),
                "Types" => array(
                    "InputTemplates",
                    "T50",
                    "T50",
                    "InputYesNo",
                ),
                "Labels" => array(
                    "Шаблон",
                    "Ограничение вывода по умолчанию",
                    "Количество групп",
                    "Выводить если 1",
                )
            )
        );

        var $DefaultTemplates = array(
            "PartLinks" => array(
                0 => "
<table border=\"1\">
<form method=\"get\" name=\"PartLinks\">
 <tr>
  <td nowrap>
   <whileins data=\"Parts\">
    <ifins exists=\"link\"><a href=\"<ins type=\"data\" name=\"link\">\"><ins type=\"data\" name=\"item\"></a></ifins>
    <ifins notexists=\"link\"><ins type=\"data\" name=\"item\"></ifins>
   </whileins>
  </td>
  <td nowrap>
Показывать по

<select name=\"limit\" class=\"FormField\" onChange=\"document.cookie='limit='+document.PartLinks.limit.options[selectedIndex].value+'; path=/; expires=Thu, 23-Jul-2020 20:10:00 GMT';history.go(0);return false;\">
 <option value=10<ifins type=\"equal\" data=\"Limit\" value=\"10\"> selected</ifins>>10</option>
 <option value=20<ifins type=\"equal\" data=\"Limit\" value=\"20\"> selected</ifins>>20</option>
 <option value=50<ifins type=\"equal\" data=\"Limit\" value=\"50\"> selected</ifins>>50</option>
</select>
  </td>
 </tr>
</form>
</table>"
            )
        );

        function GetClassName() {
            return __CLASS__;
        }

        function Action() {
            global $PHP_SELF, $QUERY_STRING, $REQUEST_URI;

            $Quantity = ($this->Data["Quantity"] ? $this->Data["Quantity"] : 0);

            $Part = $this->Data["Part"];
            $First = $this->Data["First"];

            $REQUEST_URI = preg_replace("/(".$this->PartID."=)(\d)*/", "", $REQUEST_URI);
            $REQUEST_URI = preg_replace("/^&/", "", $REQUEST_URI);
            $REQUEST_URI = preg_replace("/&$/", "", $REQUEST_URI);
            $REQUEST_URI = preg_replace("/[&]{2}/", "&", $REQUEST_URI);
            //        $REQUEST_URI = preg_replace("/[?&]/", "", $REQUEST_URI);
            $Link = $REQUEST_URI;
            $Link = preg_replace("/index.php/", "", $Link);
            if(strpos($Link, "?") === false) {
                $Link .= "?";
            } else {
                $Link .= "&";
            }
            //        $Link .= ($REQUEST_URI ? "?$REQUEST_URI&" : "?");
            /*
                    $Link = $_SERVER['PHP_SELF'];
                    $Link = preg_replace("/index.php/", "", $Link);
                    $QUERY_STRING = preg_replace("/($PartID=)(\d)*.../", "", $QUERY_STRING);
                    $QUERY_STRING = preg_replace("/^&/", "", $QUERY_STRING);
                    $QUERY_STRING = preg_replace("/&$/", "", $QUERY_STRING);
                    $QUERY_STRING = preg_replace("/[&]{2}/", "&", $QUERY_STRING);
                    $Link .= ($QUERY_STRING ? "?$QUERY_STRING&" : "?");
            */

            $Parts = array();

            if($Part > $this->Groups) {
                $this->Groups = $this->Groups > 0 ? $this->Groups : 1;
                $Previous = floor(($Part-1)/$this->Groups)*$this->Groups;
                array_push(
                    $Parts,
                    array(
                        "link" => $Link.$this->PartID."=$Previous",
                        "item" => "<<",
                        "previous" => $Previous,
                        "part" => "<<",
                    )
                );
            }
            $Num = $Previous;
            $i = 1;
            while ($Num*$this->Limit<$Quantity && $i<=$this->Groups) {
                $Num++;
                $First = (($Num-1)*$this->Limit) + 1;
                $Last = $Num * $this->Limit;
                $Last = ($Last>$Quantity ? $Quantity : $Last);
                if ($Num == $Part) {
                    array_push(
                        $Parts,
                        array(
                            "link" => "",
                            "item" => "$First..$Last",
                            "part" => $Num,
                        )
                    );
                }
                else {
                    array_push(
                        $Parts,
                        array(
                            "link" => $Link.$this->PartID."=$Num",
                            "item" => "$First..$Last",
                            "part" => $Num,
                        )
                    );
                }
                $i++;
            }

            if ($Num*$this->Limit < $Quantity) {
                $Next = $Num+1;
                array_push(
                    $Parts,
                    array(
                        "link" => $Link.$this->PartID."=$Next",
                        "item" => ">>",
                        "next" => $Next,
                        "part" => ">>",
                    )
                );
            }

            if (count($Parts) > 1 || $this->GetProperty("Empty")) {
                $this->Data["Parts"] = $Parts;

                $this->Ins2Php("PartLinks");
            }
        }

        function classScroll($sec = "", $Parent = "") {
            parent::classProperties($sec, $Parent);

            $Limit = $_COOKIE['limit'][$this->SectionID];
            $this->Limit = $this->Data["Limit"] = ($Limit ?: $this->GetProperty("Limit"));

            $this->PartID = "part";

            $Part = $_REQUEST[$this->PartID];
            $Part = (int)$Part;
            $Part = ($Part > 0 ? $Part : 1);

            $this->Data["Part"] = $Part;
            $First = ($Part-1) * $this->Limit;
            $this->Data["First"] = $First;

            $this->Groups = $this->GetProperty("Groups");

        }
    }

<?php


    class classPage extends classSecurity {

        static $Version;
        static $Css;
        static $Js;
        static $Skin;
        static $Browser;
        static $isMobile = false;

        public $PageVariables = array();
        public $PageType;
        
        var $Properties = array(
            "public" => array(
                "Names" => array(
                ),
                "Types" => array(
                ),
                "Labels" => array(
                )
            ),
            "private" => array(
                "Names" => array(
                    "page_css_file" => "page_css_file",
                    "page_meta_desc" => "page_meta_desc",
                    "page_meta_keywords" => "page_meta_keywords",
                    "page_title" => "page_title",
                    "page_name" => "page_name",
                    "page_signature" => "page_signature",
                    "parent" => "page_parent",
                    "localdir" => "page_localdir",
                    "page_type",
                    "template" => "page_template",
                    "section" => "page_redirect_section",
                    "url" => "page_redirect_url",
                    "page_visible",
                    "page_access"
                ),
                "Types" => array(
                    "page_css_file" => "T50",
                    "page_meta_keywords" => "T50",
                    "page_meta_desc" => "T50Count",
                    "page_title" => "T50Count",
                    "page_name" => "T50",
                    "page_signature" => "T50",
                    "parent" => "InputEditSectionsTree",
                    "localdir" => "T50",
                    "InputPageTypes",
                    "template" => "InputTemplates",
                    "section" => "InputSectionsTree",
                    "url" => "T50",
                    "InputVisible",
                    "InputAccess"
                ),
                "Labels" => array(
                    "page_css_file" => "CSS файл",
                    "page_meta_desc" => "META description",
                    "page_meta_keywords" => "META keywords",
                    "page_title" => "TITLE страницы",
                    "page_name" => "Название раздела",
                    "page_signature" => "Подпись к разделу",
                    "parent" => "Родительский раздел",
                    "localdir" => "Локальная директория",
                    "Тип страницы",
                    "template" => "Шаблон страницы",
                    "section" => "Раздел",
                    "url" => "Ссылка",
                    "Видимый",
                    "Доступ"
                )
            )
        );

        var $DefaultTemplates = array(
            "page_template" => array(
                0 => "
<!DOCTYPE html>
<html lang=\"<? echo PAGE_LANGUAGE; ?>\">
<head>
 <meta charset=\"UTF-8\" />
 <meta content=\"width=device-width, initial-scale=1.0\" name=\"viewport\">
 <meta name=\"description\" content=\"<ins type=\"buffered\" name=\"page_meta_desc\">\" />
 <meta name=\"keywords\" content=\"<ins type=\"buffered\" name=\"page_meta_keywords\">\" />
 <link rel=\"author\" href=\"http://inweb.nnov.ru\" />
 <link rel=\"shortcut icon\" href=\"/favicon.ico\" />
 <link rel=\"stylesheet\" href=\"/jas/<? echo classPage::\$Css; ?>;index.css<ins type=\"variable\" name=\"page_css_file\">\">
 <title><ins type=\"buffered\" name=\"page_title\"></title>
</head>
<body class=\"js Body\">

<div id=\"page-wrapper\">

  <header></header>

  <div class=\"page-body\">
    <? echo DOMAIN_URL; ?><br>
    Текущее время: <ins type=\"data\" name=\"current_time\"><br><br>
  </div>

  <div id=\"page-pusher\"></div>
</div>

<footer></footer>

<script type=\"text/javascript\" src=\"/jas/<? echo classPage::\$Js; ?>;common.js\"></script>
</body>
</html>"
            )
        );

        function GetClassName() {
            return __CLASS__;
        }

        
        function Action() {
            ob_start();
            register_shutdown_function(array($this, 'endBuffering'));
//            register_shutdown_function(array($this, 'showAdminPanel'));

            if($this->PageType == INS_PAGE_NORMAL) {

                $this->Data["current_time"] = date("d")." ".self::$RusMonths[date("m")]." ".date("Y").", ".date("H:i:s");
                
                $this->Data['page_localdir'] = $this->Properties["private"]["Values"]["localdir"];

                $this->Data['page_title'] = $this->Properties["private"]["Values"]["page_title".$this->LngPrefix];
                $this->Data['page_name'] = $this->Properties["private"]["Values"]["page_name".$this->LngPrefix];
                $this->Data['page_signature'] = $this->Properties["private"]["Values"]["page_signature".$this->LngPrefix];

                $this->Data['page_title'] = $this->Data['page_title'] ?: $this->Data['page_name'];
                
                $this->setBuffered("page_title", $this->Data['page_title']);
                $this->setBuffered("page_name", $this->Data['page_name']);
                $this->setBuffered("page_signature", $this->Data['page_signature']);
                $this->setBuffered("page_meta_desc", $this->Properties["private"]["Values"]["page_meta_desc".$this->LngPrefix]);
                $this->setBuffered("page_meta_keywords", $this->Properties["private"]["Values"]["page_meta_keywords".$this->LngPrefix]);

                if($this->Users) {
                    $realDo = $_REQUEST[classUsers::$DoID];
                    switch($realDo) {
                        case "exit":
                            $this->Users->PageExit();
                            break;
                        case "enter":
                            $this->Users->PageEnter($_REQUEST['login'], $_REQUEST['email'], $_REQUEST['phone'], $_REQUEST['passwd']);
                            $this->User = $this->Users->Data;
                            break;
                    }
                    // закрытые страницы для определенных груп пользователей
                    $this->PageAuth();
                }
                $this->Ins2Php("page_template");
            } elseif ($this->PageType == INS_PAGE_SECTION) {
                $Obj = new classPage($this->GetProperty("page_redirect_section"), $this->Parent);
                $Obj->Action();
            } elseif ($this->PageType == INS_PAGE_REDIRECT) {
                header("HTTP/1.1 301 Moved Permanently");
                header("Location: ".$this->GetProperty("page_redirect_url"));
                die();
            }

//            $PageContent = ob_get_clean();
//            echo $PageContent;

        }


        public function PageAuth() {
            /*
              <ifins exists="access"></ifins>
            */
            $Access = $this->GetProperty("page_access");
            $Allow = false;

            if(!$Access) {
                $Groups = DB::selectArray("SELECT p.Gro_Code, g.Gro_Name
                                               FROM Permissions AS p
                                              LEFT JOIN Groups AS g
                                               ON p.Gro_Code = g.Gro_Code
                                              WHERE Sec_Code = ".(int)$this->SectionID, "Gro_Code");

                if(count($Groups) && $this->User['Group']) {
                    if(in_array($this->User['Group'], array_keys($Groups)))
                        $Allow = true;
                }
            } else
                $Allow = true;

            if(!$Allow) {
                $this->Data["Error"] = "Доступ запрещен!";
                header("HTTP/1.1 403 Forbidden");
                $P = new classPage("error");
                $P->Action();
                die;
            } else {
                $this->Data['access'] = 1;
            }
        }


        public function GetVariables() {
            $Obj = new storageVariables();
            $Vars = $Obj->GetProperty("var");

            if(is_array($Vars)) {
                $this->Properties["private"]["Names"]["Variables"] = "Separator";
                $this->Properties["private"]["Types"]["Variables"] = "";
                $this->Properties["private"]["Labels"]["Variables"] = "Переменные";
                
                foreach($Vars as $Key => $Var) {
                    if($Var['id']) {
                        $this->Properties["private"]["Names"][$Var["id"]] = $Var["id"];
                        $this->Properties["private"]["Types"][$Var["id"]] = storeVariables::$Options[$Var["type"]];
                        $this->Properties["private"]["Labels"][$Var["id"]] = "<i>".$Var["desc"]."<br>(".$Var["id"].")</i>";
                        $this->Properties["private"]["Values"][$Var["id"]] = $Var["value"];

                        $this->PageVariables[$Var["id"]] = $Var["value"];
                        $this->Data[$Var["id"]] = $Var["value"];
                    }
                }
            }
        }
        
        
        function classPage($sec = "", $Parent = "") {
            global $TemplateSets;

            if(!(int)$sec) {
                unset($this->Properties["private"]["Names"]["parent"]);
                unset($this->Properties["private"]["Names"]["localdir"]);
            }

            $this->GetVariables();

            if(DEFAULT_LANGUAGE) {
                foreach($TemplateSets as $k => $v) {
                    if($k && $k != DEFAULT_LANGUAGE) {
                        $this->Properties["private"]["Names"][] = "Separator";
                        $this->Properties["private"]["Types"][] = "";
                        $this->Properties["private"]["Labels"][] = $v;

                        $this->Properties["private"]["Names"]["page_meta_desc_".$k] = "page_meta_desc_".$k;
                        $this->Properties["private"]["Types"]["page_meta_desc_".$k] = "T50";
                        $this->Properties["private"]["Labels"]["page_meta_desc_".$k] = "META description";

                        $this->Properties["private"]["Names"]["page_meta_keywords_".$k] = "page_meta_keywords_".$k;
                        $this->Properties["private"]["Types"]["page_meta_keywords_".$k] = "T50";
                        $this->Properties["private"]["Labels"]["page_meta_keywords_".$k] = "META keywords";

                        $this->Properties["private"]["Names"]["page_title_".$k] = "page_title_".$k;
                        $this->Properties["private"]["Types"]["page_title_".$k] = "T50";
                        $this->Properties["private"]["Labels"]["page_title_".$k] = "TITLE страницы";

                        $this->Properties["private"]["Names"]["page_name_".$k] = "page_name_".$k;
                        $this->Properties["private"]["Types"]["page_name_".$k] = "T50";
                        $this->Properties["private"]["Labels"]["page_name_".$k] = "Название раздела";

                        $this->Properties["private"]["Names"]["page_signature_".$k] = "page_signature_".$k;
                        $this->Properties["private"]["Types"]["page_signature_".$k] = "T50";
                        $this->Properties["private"]["Labels"]["page_signature_".$k] = "Подпись к разделу";

                        $this->Properties["private"]["Names"]["page_visible_".$k] = "page_visible_".$k;
                        $this->Properties["private"]["Types"]["page_visible_".$k] = "InputYesNo";
                        $this->Properties["private"]["Labels"]["page_visible_".$k] = "Видимый";
                    }
                }
            }

            parent::classSecurity($sec, $Parent);

            $this->PageType = (int)$this->GetProperty("page_type");
            if ($this->PageType != INS_PAGE_NORMAL)
                unset($this->Properties["private"]["Names"]["template"]);
            if ($this->PageType != INS_PAGE_SECTION)
                unset($this->Properties["private"]["Names"]["section"]);
            if ($this->PageType != INS_PAGE_REDIRECT)
                unset($this->Properties["private"]["Names"]["url"]);

            if(!self::$Css || !self::$Js)
                self::Browser();

            foreach($this->PageVariables as $Key => $Value) {
                $this->Data[$Key] = $this->GetProperty($Key);
            }
            
        }


        public function Browser() {
            /*
              Определяет версию браузера
            */
            $Ver = CSS_VER;
            $sVer = CSS_SVER;

            if(debug()) {
                $Ver = "debug.".$Ver;
                $sVer++;
            }
            self::$Version = $Ver.".".$sVer;

            $ua = $_SERVER["HTTP_USER_AGENT"];

            if (preg_match("/Opera/", $ua)) {
                $i = strpos($ua, "Opera");
                $br = "Opera";
                $css = "opera";
            }
            elseif ($i = strpos($ua, "iPad")) {
                $br = "iPad";
                $ver = 1;
                $css = "ipad";
                self::$isMobile = true;
            }
            elseif ($i = strpos($ua, "iPhone")) {
                $br = "iPhone";
                $ver = 1;
                $css = "iphone";
                self::$isMobile = true;
            }
            elseif ($i = strpos($ua, "Android")) {
                $br = "Android";
                $ver = 1;
                $css = "android";
                self::$isMobile = true;
            }
            elseif ($i = strpos($ua, "Chrome")) {
                $br = "Chrome";
                $css = "chrome";
            }
            elseif ($i = strpos($ua, "Safari")) {
                $br = "Safari";
                $css = "safari";
            }
            elseif ($i = strpos($ua, "MSIE")) {
                $br = "MSIE";
                $css = "ie";
            }
            elseif ($i = strpos($ua, "Firefox")) {
                $br = "Firefox";
                $css = "ff";
            }
            elseif(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od|ad)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $ua) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($ua, 0, 4))) {
                self::$isMobile = true;
                $br = "Unknown";
                $css = "unknown";
            }
            elseif(IsRobots()) {
                $br = "Robot";
                $css = "robot";
            }
            else {
                $br = "Unknown";
                $css = "unknown";
            }


            self::$Browser = array(
                "type" => $br,
                "css" => ";browser,$css.css",
                "isMobile" => self::$isMobile,
            );

            self::$Css = "ver,".self::$Version.".css";
            self::$Js  = "ver,".self::$Version.".js";

        }

    }

    class InputPageTypes extends InputSelect {
        function InputPageTypes($Name, $Value) {
            $Options = array(
                INS_PAGE_NORMAL => "Обычный",
                INS_PAGE_SECTION => "Переадресация на раздел",
                INS_PAGE_REDIRECT => "Переадресация на ссылку"
            );
            parent::InputSelect($Name, $Value, $Options);
        }
    }

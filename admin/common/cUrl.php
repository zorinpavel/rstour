<?php


    class cUrl {

        public static $LogFile = "cUrl";
        public static $LogMode = false;


        public static function GetInput($isJSON = true) {
            $getContents = file_get_contents("php://input") ?: $_REQUEST;
            $Input = ($isJSON && self::isJson($getContents)) ? json_decode($getContents, true) : $getContents;

            if(self::$LogMode)
                Log::AddLog($Input, self::$LogFile);

            return $Input;
        }


        public static function Request($method, $path, $parameters = array(), $headers = false, $ResponseType = "JSON", $auth = false, $options = false) {
            $method = strtoupper($method);

            if($method == "GET" && count($parameters))
                $path .= "?".http_build_query($parameters, "", "&");

            $curlHandler = curl_init();
            curl_setopt($curlHandler, CURLOPT_URL, $path);

            if($headers)
                curl_setopt($curlHandler, CURLOPT_HTTPHEADER, $headers);

            if($auth) {
                curl_setopt($curlHandler, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                curl_setopt($curlHandler, CURLOPT_USERPWD, $auth);
            }

            if(is_array($options)) {
                foreach($options as $optionKey => $optionValue)
                    curl_setopt($curlHandler, $optionKey, $optionValue);
            }

            curl_setopt($curlHandler, CURLOPT_HEADER, true);
            curl_setopt($curlHandler, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curlHandler, CURLOPT_FAILONERROR, false);
            curl_setopt($curlHandler, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curlHandler, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curlHandler, CURLOPT_TIMEOUT, 10);
            curl_setopt($curlHandler, CURLOPT_CONNECTTIMEOUT, 10);

            if($method == "POST") {
                curl_setopt($curlHandler, CURLOPT_POST, true);
                curl_setopt($curlHandler, CURLOPT_POSTFIELDS, $parameters);
            }

            $responseBody = curl_exec($curlHandler);
            $statusCode   = curl_getinfo($curlHandler, CURLINFO_HTTP_CODE);
            $info         = curl_getinfo($curlHandler);
            $errno        = curl_errno($curlHandler);
            $error        = curl_error($curlHandler);

            curl_close($curlHandler);

            $rawHeaders = explode("\n", substr($responseBody, 0, $info['header_size']) );
            $responseHeaders = array();
            foreach($rawHeaders as $rawHeader) {
                if(preg_match('/^(.*?)\\:\\s+(.*?)$/m', $rawHeader, $headerPart))
                    $responseHeaders[$headerPart[1]] = $headerPart[2];
            }

            $responseBody = substr($responseBody, $info['header_size']);
            $responseArray = array();

            if(strtoupper($ResponseType) == "JSON")
                $responseArray = self::ResponseJson($statusCode, $responseBody);

            if(strtoupper($ResponseType) == "XML")
                $responseArray = self::ResponseXml($statusCode, $responseBody);

            $Response = array(
                "status_code" => $statusCode,
                "response_body" => $responseBody,
                "response_params" => $responseArray,
                "response_headers" => $responseHeaders,
                "error_code" => $errno,
                "error" => $error,
                "request_params" => $parameters,
                "info" => $info,
            );

            return $Response;
        }


        static function ResponseJson($statusCode, $responseBody) {
            if(!empty($responseBody)) {

                $responseError = array();
                if($statusCode >= 400)
                    preg_match("/{(.*)}/i", $responseBody, $responseError);

                $response = json_decode($responseBody, true);
                if(!self::isJson($responseBody)) {
                    self::Log(json_decode($responseError[0], true));

                    return json_decode($responseError[0], true);
                }

                return $response;
            }

            return false;
        }


        static function ResponseXml($statusCode, $responseBody) {
            if(!empty($responseBody)) {

                $responseError = array();
                if($statusCode >= 400)
                    preg_match("/{(.*)}/i", $responseBody, $responseError);

                $xml = simplexml_load_string($responseBody, "SimpleXMLElement", LIBXML_NOCDATA);
                $response = json_decode(json_encode($xml), true);

                if(!$response) {
                    self::Log($responseError[0], true);

                    return $responseError[0];
                }

                return $response;
            }

            return false;
        }


        static function Log($Data = array()) {
            if(!self::$LogMode)
                return;

            Log::AddLog($Data, self::$LogFile);
        }


        public static function isJson($string) {
            json_decode((string)$string);
            return (json_last_error() == JSON_ERROR_NONE);
        }


    }
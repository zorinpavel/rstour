<?php

/*
      Usage: $Input = new InputClass($Param1, ...);
             $Input->dPrint();

      Label($Value[, $Style])
        ├── Lnk($Url, $Value[, $Style])
        ├── Input($Name, $Value [, $Style])
        │     ├── InputHidden($Name, $Value)
        │     ├── InputText($Name, $Value[, $Size, $Style, $Maxlength])
        │     │      ├── InputPassword($Name, [$Value, $Size, $Style])
        │     │      └── InputFile($Name[, $Size, $Style])
        │     ├── InputSubmit($Name, $Value[, $Style, $OnClick])
        │     │      └── InputButton($Name, $Value[, $Style, $OnClick])
        │     ├── InputCheckbox($Name, $Value[, $Style])
        │     │      └── InputRadio($Name, $Value, $RadioValue[, $Style])
        │     ├── InputSelect($Name, $Value, $Options[, $Style, $onChange])
        │     │      └── InputDBSelect($Name, $Value, $TableName, $TableCode, $TableExpr[, $TableOrder, $Style, $onChange])
        │     ├── Textarea($Name, $Value, $Rows, $Cols[, $Style])
*/

    class Label {
        var $Value;
        var $Style = "";

        function dPrint() {
            $SpanOpen  = ($this->Style ? "<span style=\"".$this->Style."\">" : "");
            $SpanClose = ($this->Style ? "</span>" : "");
            dPrint($SpanOpen.$this->Value.$SpanClose);
        }

        function Label($Value, $Style = "") {
            $this->Value = $Value;
            $this->Style = $Style;
        }
    }

    class Lnk extends Label {
        var $Url;

        function dPrint() {
            $Style = ($this->Style ? " style=\"".$this->Style."\"" : "");
            dPrint("<a href=\"".$this->Url."\"$Style>".$this->Value."</a>");
        }

        function Lnk($Url, $Value, $Style="") {
            $this->Label($Value, $Style);
            $this->Url = $Url;
        }
    }

    class Input extends Label {
        var $Name = "";
        var $Id;

        function dPrint() {
        }

        function Input($Name, $Value, $Style="") {
            $this->Label($Value, $Style);
            $this->Name = $Name;
            $this->Id = $this->Id ?: $this->Name;
        }
    }

    class InputHidden extends Input {
        function dPrint() {
            dPrint("<input type=\"hidden\" name=\"".$this->Name."\" id=\"".$this->Id."\" value=\"".$this->Value."\">");
        }

        function InputHidden($Name, $Value) {
            $this->Input($Name, $Value);
        }
    }
    
    class InputText extends Input {
        public $Size = "";
        public $Maxlength = "";
        public $Type = "";
        public $Readonly = false;
        public $CanEdit = true;
        public $Empty = true;
        public $OnClick = false;
        public $OnChange = false;
        public $OnKeyup = false;
        
        function dPrint() {
            $Style = ($this->Style ? " style=\"".$this->Style."\"" : "");
            $Maxlength = ($this->Maxlength ? " maxlength=\"".$this->Maxlength."\"" : "");
            //    $this->Value = htmlspecialchars($this->Value);
            if(($this->Value || !$this->CanEdit) && $this->Readonly)
                $Readonly = " readonly";
            $Onclick = ($this->OnClick ? " onclick=\"".$this->OnClick."\"" : "");
            $Onchange = ($this->OnChange ? " onchange=\"".$this->OnChange."\"" : "");
            $Onkeyup = ($this->OnKeyup ? " onkeyup=\"".$this->OnKeyup."\"" : "");
            dPrint("<input type=\"".$this->Type."\" name=\"".$this->Name."\" id=\"".$this->Id."\" size=\"".$this->Size."\" value=\"".$this->Value."\"$Style$Maxlength$Readonly$Onclick$Onchange$Onkeyup>");
        }
        
        function InputText($Name, $Value, $Size=10, $Style = "", $Maxlength = "", $Onclick = "", $Onchange = "", $Onkeyup = "") {
            $this->Input($Name, $Value, $Style);
            $this->Size = $Size;
            $this->Maxlength = $Maxlength;
            $this->Onclick = $Onclick;
            $this->Onchange = $Onchange;
            $this->OnKeyup = $Onkeyup;
            $this->Type = "text";
        }
    }

    class InputPassword extends InputText {
        function InputPassword($Name, $Value="", $Size=15, $Style="") {
            $this->InputText($Name, $Value, $Size, $Style);
            $this->Type = "password";
        }
    }

//    class InputFile extends InputText {
//        var $FilenameClass = "MainLink";
//        var $DataDir;
//
//        function dPrint() {
//            $Style = ($this->Style ? " style=\"".$this->Style."\"" : "");
//            $FilenameClass = ($this->FilenameClass ? " style=\"".$this->FilenameClass."\"" : $Style);
//            $Value = ($this->Value ? "<br><a href=\"".DOMAIN_URL.$this->Value."\" target=\"_blank\"$FilenameClass>".$this->Value."</a><input type=\"hidden\" name=\"".$this->Name."\" value=\"".$this->Value."\">" : "");
//            dPrint("<input type=\"".$this->Type."\" name=\"".$this->Name."\" size=\"".$this->Size."\"> $Value");
//        }
//
//        function InputFile($Name, $Value, $DataDir="data") {
//            $this->InputText($Name, $Value);
//            $this->Type = "file";
//            $this->DataDir = $DataDir;
//        }
//    }

    class InputFile extends InputText {
        function InputFile($Name, $Value, $Label = "") {
            $this->InputText($Name, $Value);
            $this->Label = $Label;
        }

        function dPrint() {
            if($this->Value) {
                if($this->Label)
                    dPrint($this->Label."<br>");
                dPrint("<div id=\"".$this->Name."\">");
                dPrint("<input type=\"file\" name=\"".$this->Name."\">");
                dPrint("<a href=\"".$this->Value."\" target=\"_blank\">".$this->Value."</a> &nbsp; <small>[<a href=\"#\" onClick=\"if(confirm('Вы действительно хотите удалить файл?')) { openUrl('file_del.php?file=".$this->Value."', '".$this->Name."'); return false; }\" class=\"SectionLineEdit\">удалить</a>]</small>");
                dPrint("</div>");
            } else {
                if($this->Label)
                    dPrint($this->Label."<br>");
                dPrint("<div id=\"".$this->Name."\">");
                dPrint("<input type=\"file\" name=\"".$this->Name."\">");
                dPrint("</div>");
            }
        }
    }


    class InputSubmit extends Input {
        var $OnClick = false;
        var $Type = "";
        var $Disabled = false;

        function dPrint() {
            $Style = ($this->Style ? " style=\"".$this->Style."\"" : "");
            $Class = ($this->ClassName ? " class=\"".$this->ClassName."\"" : "");
            $OnClick = ($this->OnClick ? " onclick=\"".$this->OnClick."\"" : "");
            $Disabled = ($this->Disabled ? " disabled" : "");
            dPrint("<input type=\"".$this->Type."\" name=\"".$this->Name."\" value=\"".$this->Value."\"$Style$OnClick$Class$Disabled>");
        }

        function InputSubmit($Name, $Value, $Style="", $OnClick="", $Class="") {
            $this->Input($Name, $Value);
            $this->OnClick = $OnClick;
            $this->ClassName = $Class;
            $this->Type = "submit";
        }
    }

    class InputButton extends InputSubmit {
        function InputButton($Name, $Value, $Style="", $OnClick="", $Class="") {
            $this->InputSubmit($Name, $Value, $Style, $OnClick, $Class);
            $this->Type = "button";
        }
    }

    class InputCheckbox extends Input {
        var $Type = "";

        function isChecked() {
            return ($this->Value ? " checked" : "");
        }

        function isDisabled() {
            return ($this->Disabled ? " disabled" : "");
        }

        function dPrint() {
            $Style = ($this->Style ? " style=\"".$this->Style."\"" : "");
            dPrint("<input type=\"".$this->Type."\" name=\"".$this->Name."\" id=\"".$this->Name."\" value=\"".($this->Value ?: 1)."\" class=\"radio\"".$Style.$this->isChecked().$this->isDisabled().">");
            if($this->Label)
                dPrint("<label for=\"".$this->Name."\">".$this->Label."</label>");
        }

        function InputCheckbox($Name, $Value, $Style = "", $Label = "") {
            $this->Input($Name, $Value, $Style);
            $this->Type = "checkbox";
            $this->Label = $Label;
        }
    }

    class InputRadio extends InputCheckbox {
        var $SelectedValue = "";

        function isChecked() {
            return ($this->Value == $this->SelectedValue ? " checked" : "");
        }

        function dPrint() {
            $Style = ($this->Style ? " style=\"".$this->Style."\"" : "");
            $Checked = $this->isChecked();
            dPrint("<input type=\"".$this->Type."\" name=\"".$this->Name."\" value=\"".$this->Value."\" id=\"".$this->Value."\" class=\"radio\"$Style$Checked>");
            if($this->Label)
                dPrint("<label for=\"".$this->Name."\">".$this->Label."</label>");
        }

        function InputRadio($Name, $Value, $SelectedValue, $Label = "", $Style="") {
            $this->InputCheckBox($Name, $Value, $Style);
            $this->Type = "radio";
            $this->SelectedValue = $SelectedValue;
            $this->Label = $Label;
        }
    }

    class InputSelect extends Input {
        var $Options = Array();
        var $Empty = 0;
        var $onChange = "";

        function dPrint() {
            $onChange = ($this->onChange ? " onChange=\"".$this->onChange."\"" : "");
            $Style = ($this->Style ? " style=\"".$this->Style."\"" : "");
            echo "<select name=\"".$this->Name."\" id=\"".$this->Name."\"$Style$onChange single>";
            if ($this->Empty) {
                echo "<option value=\"\">..</option>";
            }
            $cssClass = " class=\"".$this->cssClass."\"";
            foreach($this->Options as $Key => $Value) {
                $Checked = ($Key == $this->Value ? " selected" : "");

                echo "<option value=\"$Key\"$Checked$cssClass>$Value</option>";
            }
            echo "</select>";
        }

        function InputSelect($Name, $Value, $Options, $Style="", $onChange="") {
            $this->Input($Name, $Value, $Style);
            $this->Options = $Options;
            $this->onChange = $onChange;
            $this->Style = $Style;
        }
    }

    class InputDBSelect extends InputSelect {
        function InputDBSelect($Name, $SelectValue, $TableName, $TableCode, $TableExpr, $TableFilter="", $TableOrder="", $Style="", $onChange="", $cssClass="") {
            if(!$TableOrder)
                $TableOrder = $TableExpr;
            
            if($TableFilter)
                $TableFilter = "WHERE $TableFilter";
            
            $this->Options = DB::selectArray("SELECT $TableCode, $TableExpr FROM $TableName $TableFilter", $TableCode);

            if($TableOrder) {
                $fOrder = array();
                $Orders = explode(",", $TableOrder);
                foreach($Orders as $Order) {
                    $ArrOrder = explode(" ", trim($Order));
                    $ArrOrder[1] = $ArrOrder[1] ?: "ASC";
                    array_push($fOrder, $ArrOrder[0]);
                    array_push($fOrder, $ArrOrder[1]);
                }
                $this->Options = sortArray($this->Options, $fOrder);
            }

            foreach($this->Options as $Key => $Value)
                $this->Options[$Key] = $Value[$TableExpr];

            $this->InputSelect($Name, $SelectValue, $this->Options, $Style, $onChange, $cssClass);
        }
    }

    class Textarea extends Input {
        var $Rows = "";
        var $Cols = "";

        function dPrint() {
            $Name = $this->Name;
            $Rows = $this->Rows;
            $Cols = $this->Cols;
            $Value = $this->Value;
            $Style = ($this->Style ? " style=\"".$this->Style."\"" : "");
            dPrint("<textarea name=\"$Name\" rows=\"$Rows\" cols=\"$Cols\" wrap=\"soft\"$Style>$Value</textarea>");
        }

        function Textarea($Name, $Value, $Rows, $Cols, $Style="") {
            $this->Input($Name, $Value, $Style);
            $this->Rows = $Rows;
            $this->Cols = $Cols;
        }
    }

    class contentListsDBSelect extends InputDBSelect {
        function contentListsDBSelect($Name, $Value, $onChange="") {
            $this->InputDBSelect($Name, $Value, "contentLists", "Lst_Code", "Lst_Name", "", "Lst_Order DESC, Lst_Code", "", $onChange);
        }
    }

    class InputApplet extends Label {
        function InputApplet($Name, $Value, $CopyDir, $Arr) {
            $this->Name = $Name;
            $this->Value = $Value;
            $this->CopyDir = $CopyDir;
            $this->Arr = $Arr;
        }

        function dPrint() {
            echo ($this->Value ? "<a href=\"".DOMAIN_URL.$this->Value."\" target=\"_blank\">".$this->Value."</a>" : "");
            echo "<input type=\"hidden\" name=\"".$this->Name."\" id=\"id_".$this->Name."\" value=\"".$this->Value."\"><br>";
            ?>
            <script language="javascript">
                function createUploaderAttribute() {
                    var uploader = document.jumpLoaderApplet.getUploader();
                    var attrSet = uploader.getAttributeSet();
                    var attr = attrSet.createStringAttribute("Param[<?php echo $this->Name; ?>]", "<?php echo $this->Value; ?>");
                    attr = attrSet.createStringAttribute("Param[CopyDir]", "<?php echo $this->CopyDir; ?>");
                    attr = attrSet.createStringAttribute("<?php echo $this->Arr['Id']; ?>", "<?php echo (int)$_REQUEST[$this->Arr['Id']]; ?>");
                    <?php
                      foreach($this->Arr as $Key => $Value) {
                        echo "\n           attr = attrSet.createStringAttribute(\"Param[".$Key."]\", \"".$Value."\");";
                      }
                    ?>
                    attr.setSendToServer(true);
                }
                function uploaderFileStatusChanged( uploader, file ) {
                    if(file.getStatus() == 2) {
                        document.getElementById("id_<?php echo $this->Name; ?>").value = file.getResponseContent();
                        //window.location.href = window.location.href;
                        document.forms["Main"].submit();
                    }
                    traceEvent( "content=" + file.getResponseContent() );
                }
                function traceEvent( message ) {
                    //document.getElementById("txtEvents").value += message + "\r\n";
                }
            </script>
            <applet name="jumpLoaderApplet"
                    code="jmaster.jumploader.app.JumpLoaderApplet.class"
                    archive="<?php echo ADMIN_URL; ?>/common/input/applet/jumploader_z.jar,<?php echo ADMIN_URL; ?>/common/input/applet/xfiledialog.jar"
                    width="130"
                    height="45"
                    mayscript>
                <param name="ac_messagesZipUrl" value="<?php echo ADMIN_URL; ?>/common/input/applet/messages_ru.zip"/>
                <param name="uc_uploadUrl" value="<?php echo ADMIN_URL; ?>/common/input/applet/load.php"/>
                <param name="vc_useNativeFileDialog" value="true"/>
                <param name="vc_uploadListViewName" value="_compact"/>
                <param value="framed" name="ac_mode">
                <param value="true" name="ac_fireUploaderFileStatusChanged">
            </applet>
            <script>createUploaderAttribute();</script>
            <?php /* <textarea id="txtEvents" wrap="off" rows="10" style="width: 100%; font: 10px monospace;" name="txtEvents"></textarea> */ ?>
        <?php
        }
    }


    class InputHtml {

        function InputHtml($Name, $Value, $Width = 600, $Height = 200, $Options = array()) {
            $this->Name = $Name;
            $this->Value = $Value;
            $this->Width = $Width;
            $this->Height = $Height;
            $this->Options = "{fullPanel : true}";
            if(count($Options)) {
                $this->Options = "{";
                foreach($Options as $Key => $Value) {
                    $this->Options .= "$Key : $Value, ";
                }
                $this->Options = substr($this->Options, 0, -2);
                $this->Options .= "}";
            }
        }

        function dPrint() {
            ?>
            <script src="<? echo EDITOR_URL ?>/nicEdit.js" type="text/javascript"></script>
            <script type="text/javascript">
                bkLib.onDomLoaded(function() {
                    new nicEditor(<? echo $this->Options; ?>).panelInstance('<? echo $this->Name; ?>');
                });
            </script>
            <textarea style="width:<? echo $this->Width; ?>px;height:<? echo $this->Height; ?>px;" id="<? echo $this->Name; ?>" name="<? echo $this->Name; ?>"><? echo $this->Value; ?></textarea>
            <?
        }

    }

<?php


    class InputArrayPV_Type extends InputSelect {
        function InputArrayPV_Type($Name, $Value) {
            $this->Options = array (
                1 => "checkbox",
                2 => "text",
                3 => "select",
            );
            parent::InputSelect($Name, $Value, $this->Options);
        }
    }

    class InputArrayPV extends Input {
        function InputArray($Name, $Value, $Types){
            parent::Input($Name, $Value, $Class);
        }

        function dPrint(){

            ?>
            <script>
                /**
                 *   Добавляет значения полей в хэш значений
                 */
                function addToArray_<?=$this->Name;?>(form){
                    key=form['<?=$this->Name;?>_new_key'].value;
                    value=form['<?=$this->Name;?>_new_value'].value;
                    field=form['<?=$this->Name;?>_hidden'];
                    field.name='<?=$this->Name;?>['+key+']';
                    field.value=value;
                    form.submit();
                }
                /**
                 *   Удаляет поле из хэша значений
                 */
                function deleteFromArray_<?=$this->Name;?>(form, key){
                    field=form['<?=$this->Name;?>['+key+']'];
                    field.name='<?=$this->Name;?>_null';
                    field=form['<?=$this->Name;?>['+key+'][Value]'];
                    field.name='<?=$this->Name;?>_null';
                    field=form['<?=$this->Name;?>['+key+'][Type]'];
                    field.name='<?=$this->Name;?>_null';
                    field=form['<?=$this->Name;?>['+key+'][Label]'];
                    field.name='<?=$this->Name;?>_null';
                    field=form['<?=$this->Name;?>['+key+'][Signature]'];
                    field.name='<?=$this->Name;?>_null';
                    form.submit();
                }
            </script>
            <table>
                <tr><td colspan="10"><hr></td></tr>
                <tr><td><b>Ключ</b></td><td><b>Значение по умолчанию</b></td><td><b>Тип</b></td><td><b>Описание</b></td><td>&nbsp;</td></tr>
                <tr><td colspan="10"><hr></td></tr>
                <?
                    if (is_array($this->Value)){
                        foreach ($this->Value as $key=>$value) {
                            ?><tr>
                            <td><input type="hidden" name="<?=$this->Name;?>[<?=$key;?>]" value="<?=$key;?>"><?=$key;?></td>
                            <td><input type="text" name="<?=$this->Name;?>[<?=$key;?>][Value]" value="<?=$value['Value'];?>"></td>
                            <td>
                                <?
                                    $Type = new InputArrayPV_Type($this->Name."[".$key."][Type]", $this->Value[$key]['Type']);
                                    $Type->dPrint();
                                ?>
                            </td>
                            <td>
                                <?
                                    $Label = new T30($this->Name."[".$key."][Label]", $this->Value[$key]['Label']);
                                    $Label->dPrint();
                                ?>
                            </td>
                            <td><input type="button" onclick="deleteFromArray_<?=$this->Name;?>(this.form, '<?=$key;?>')" value="Удалить" class="red"></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="4">
                                    <?
                                        $Label = new T50($this->Name."[".$key."][Signature]", $this->Value[$key]['Signature']);
                                        $Label->dPrint();
                                    ?>
                                </td>
                            </tr>
                            <?
                        }
                    }
                ?>
                <tr>
                    <td>
                        <input type="hidden" name="<?=$this->Name;?>_hidden" value="0">
                        <input type="text" name="<?=$this->Name;?>_new_key">
                    </td>
                    <td><input type="text" name="<?=$this->Name;?>_new_value"></td>
                    <td><input type="button" name="create" onclick="addToArray_<?=$this->Name;?>(this.form);" value="Добавить" class="blue"></td>
                </tr>
            </table>
            <?
        }
    }

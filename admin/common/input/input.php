<?php
    
    class InputLabel extends Label {
        function InputLabel($Name, $Value = "") {
            $this->Label($Value, "");
        }
    }

    class InputLnk extends Lnk {
        function InputLnk($Name, $Url) {
            $Value = substr($Url, 0, 50)."...";
            $this->Lnk($Url, $Value, "");
        }
    }

    class T5 extends InputText {
        function T5($Name, $Value) {
            $this->InputText($Name, $Value, 5, "");
        }
    }

    class T10 extends InputText {
        function T5($Name, $Value) {
            $this->InputText($Name, $Value, 10, "");
        }
    }

    class T15 extends InputText {
        function T15($Name, $Value) {
            $this->InputText($Name, $Value, 18, "");
        }
    }

    class T20 extends InputText {
        function T20($Name, $Value) {
            $this->InputText($Name, $Value, 23, "");
        }
    }

    class T30 extends InputText {
        function T30($Name, $Value) {
            $this->InputText($Name, $Value, 33, "");
        }
    }

    class T50 extends InputText {
        function T50($Name, $Value) {
            $this->InputText($Name, $Value, 53, "");
        }
    }

    class P50 extends InputPassword {
        function P50($Name, $Value) {
            $this->InputPassword($Name, $Value, 53, "");
        }
    }

    class TA10 extends Textarea {
        function TA10($Name, $Value) {
            $this->Textarea($Name, $Value, 3, 33);
        }
    }

    class TA30 extends Textarea {
        function TA30($Name, $Value) {
            $this->Textarea($Name, $Value, 5, 43);
        }
    }

    class TA50 extends Textarea {
        function TA50($Name, $Value) {
            $this->Textarea($Name, $Value, 10, 53);
        }
    }

    class TA100 extends Textarea {
        function TA100($Name, $Value) {
            $this->Textarea($Name, $Value, 10, 53, "width:95%");
        }
    }

    class T5RO extends InputText {
        function T5RO($Name, $Value) {
            $this->Readonly = true;
            $Bg = $Value ? "background:#EEEEEE" : "";
            $this->InputText($Name, $Value, 5, $Bg, "", true);
        }
    }

    class T10RO extends InputText {
        function T10RO($Name, $Value) {
            $this->Readonly = true;
            $Bg = $Value ? "background:#EEEEEE" : "";
            $this->InputText($Name, $Value, 10, $Bg, "", true);
        }
    }

    class T15RO extends InputText {
        function T15RO($Name, $Value) {
            $this->Readonly = true;
            $Bg = $Value ? "background:#EEEEEE" : "";
            $this->InputText($Name, $Value, 18, $Bg, "", true);
        }
    }

    class T20RO extends InputText {
        function T20RO($Name, $Value) {
            $this->Readonly = true;
            $Bg = $Value ? "background:#EEEEEE" : "";
            $this->InputText($Name, $Value, 23, $Bg, "", true);
        }
    }

    class T30RO extends InputText {
        function T30RO($Name, $Value) {
            $this->Readonly = true;
            $Bg = $Value ? "background:#EEEEEE" : "";
            $this->InputText($Name, $Value, 33, $Bg, "", true);
        }
    }

    class T50Count extends InputText {
        function dPrint() {
            parent::dPrint();
            echo "<span id=\"count_".$this->Name."\"></span>";
        }
        
        function T50Count($Name, $Value) {
            $this->InputText($Name, $Value, 53, "", 255, "", "", "T50Count(this)");
        }
    }

    class InputTime extends T20RO {
        public $Now;

        function InputTime($Name, $Value) {

            $Now = $this->Now ? date("Y-m-d H:i:s", time()) : strtotime($Value) ? $Value : "";

            $this->InputHidden = new InputHidden($Name, $Now);
            $this->InputHidden->dPrint();

            $this->Readonly = true;
            $Date = strtotime($Value) ? date("d.m.Y H:i:s", strtotime($Value)) : "";
            $this->T20RO($Name."_date", $Date);
        }
    }

    class InputDate extends InputText {
        function InputDate($Name, $Value) {
            if(preg_match("/-/", $Value))
                $Value = strtotime($Value);
            if($Value)
                $Value = date("Y-m-d", $Value);

            $this->InputText($Name, $Value, 15);
        }
    }

    class InputDateTime extends InputText {
        function InputDateTime($Name, $Value) {
            if(preg_match("/-/", $Value))
                $Value = strtotime($Value);
            if($Value)
                $Value = date("Y-m-d H:i:s", $Value);

            $this->InputText($Name, $Value, 20);
        }
    }

    class InputDateRO extends InputText {
        function InputDateRO($Name, $Value) {
            if($Value) {
                $this->InputHidden = new InputHidden($Name, $Value);
                $this->InputHidden->dPrint();
            }
            $this->Readonly = true;
            $Bg = (strtotime($Value) > 0) ? "background:#EEEEEE" : "";
            $Date = (strtotime($Value) > 0) ? date("d.m.Y", strtotime($Value)) : "";
            $this->InputText(($Value ? "date-" : "").$Name, $Date, 15, $Bg, "", true);
            if(!$Value)
                dPrint("<a href=\"#\" onClick=\"return showDatePicker('".$this->Id."');\" title=\"Выбрать дату\"><img src=\"".ADMIN_URL."/images/calendar_add.png\" style=\"vertical-align:middle;\"></a>");
        }
    }

    class InputDatePicker extends InputText {
        function InputDatePicker($Name, $Value) {
            $this->InputText($Name, $Value);
        }
        
        function dPrint() {
            $InputHidden = new InputHidden($this->Name, $this->Value);
            $InputHidden->Id = $this->Name;
            $InputHidden->dPrint();
    
            $Date = (strtotime($this->Value) > 0) ? date("d.m.Y", strtotime($this->Value)) : "";
            $this->Id = "date-".$this->Name;
            $this->InputText("date-".$this->Name, $Date, 10);
    
            $this->OnChange = "return DateChange(this);";
            if($this->CanEdit)
                dPrint("<a href=\"#\" onClick=\"return showDatePicker('".$this->Id."');\" title=\"Выбрать дату\"><img src=\"".ADMIN_URL."/images/calendar_add.png\" style=\"vertical-align:middle;\"></a>");
            parent::dPrint();
        }
    }

    class InputTemplates extends InputSelect {
        var $mod;
        var $sec;

        var $External = array();
        var $ExternalValues = array();

        var $Blocks   = array();

        function Analize() {
            $this->External = array();
            $this->Blocks   = array();
            $this->Files    = array();

            eval("\$Obj = new ".$this->mod."('".$this->sec."');");
            $Template = $Obj->GetTemplate($this->Name);
            $Obj->FindAllTags($Template);

            reset($Obj->Blocks);
            while (list($Key, $Block) = each($Obj->Blocks)) {
                if ($Block["external"]) {
                    array_push($this->External, $Block);
                }
                elseif ($Block["type"] == "block") {
                    array_push($this->Blocks, $Block);
                }
            }
        }

        function dPrintExternal() {
            if(!is_array($this->External))
                return;
            if(!sizeof($this->External))
                return;

            echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\">";
            reset($this->External);
            while (list($Key, $Block) = each($this->External)) {
                echo "
 <tr>
  <td>".$Block["external"]."</td>
  <td width=\"100%\">";
                $ExternalName = $this->Name."[External][".$Block["name"]."]";
                $ExternalValue = $this->ExternalValues[$Block["name"]];
                if ($Block["type"] == INS_VARIABLE) {
                    $Obj = new InputText($ExternalName, $ExternalValue, 50);
                    $Obj->dPrint();
                }
                if ($Block["type"] == INS_INCLUDE) {
                    $Obj = new InputText($ExternalName, $ExternalValue, 30);
                    $Obj->dPrint();
                    $ExternalFileName = $this->Name."[External][".$Block["name"]."_file]";
                    $ExternalFileValue = $this->ExternalValues[$Block['name']."_file"];
                    $Obj = new InputText($ExternalFileName, $ExternalFileValue, 30);
                    $Obj->dPrint();
                }
                elseif ($Block["type"] == INS_BLOCK) {
                    $Obj = new InputModules($ExternalName, $ExternalValue, $Block["class"]);
                    $Obj->dPrint();
                }

                echo "
  </td>
 </tr>";
            }
            echo "</table>";
        }

        function dPrintSubBlocks() {
            echo "<table border=\"0\" cellspacing=\"2\" cellpadding=\"2\">";
            foreach($this->Blocks as $Key => $Block) {
                $mod = $Block["class"];
                $sec = $Block["name"];
                echo "
 <tr>
  <td nowrap>$mod.$sec</td>
  <td width=\"100%\"><input type=\"button\" value=\"Свойства...\" onClick=\"window.location='sec_private.php?mod=$mod&sec=$sec'\" class=\"blue\"></td>
 </tr>";
            }
            echo "</table>";
        }

        function dPrintFiles() {
            if(!is_array($this->Files))
                return;
            if(!sizeof($this->Files))
                return;

            echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\">";
            foreach($this->Files as $Key => $File) {
                echo "
 <tr>
  <td>".$File["external"]."</td>
  <td width=\"100%\">";
                $ExternalName = $this->Name."[External][".$File["name"]."]";
                $ExternalValue = $this->ExternalValues[$File["name"]];
                if ($Block["type"] == INS_INCLUDE) {
                    $Obj = new InputText($ExternalName, $ExternalValue, 50);
                    $Obj->dPrint();
                }
                echo "
  </td>
 </tr>";
            }
            echo "</table>";
        }

        function dPrint() {
            global $PHP_SELF, $sec;
            parent::dPrint();

            $Names = explode("[", $this->Name);
            $this->Name = $Names[0];

            echo "
<input type=\"button\" type=\"button\" class=\"gray\" value=\"Редактировать\" onClick=\"window.open('templates.php?mod=".$this->mod."&TemplatesName=".$this->Name."&id=".$this->Value."&back=$PHP_SELF&sec=$sec');\">
<input type=\"button\" type=\"button\" class=\"gray\" value=\"Создать\" onClick=\"window.open('templates.php?mod=".$this->mod."&TemplatesName=".$this->Name."&id=0&parent_id=".$this->Value."&back=$PHP_SELF&sec=$sec');\">\n";

            $this->Analize();
            $this->dPrintExternal();
            $this->dPrintSubBlocks();
            //      $this->dPrintFiles();
        }

        function InputTemplates($Name, $TValue) {
            $this->mod = $_REQUEST['mod'];
            $this->sec = $_REQUEST['sec'];

            eval("\$Obj = new ".$this->mod."('".$this->sec."');");

            $Templates = array();
            $T1 = $Obj->DefaultTemplates;
            if (is_array($T1[$Name])) {
                while (list($Key, $Value) = each($T1[$Name])) {
                    $Templates[$Key] = "Default #$Key";
                }
            }

            $FileName = SETTINGS_PATH."/".$this->mod."_templates_".$Name.".php";
            if (file_exists($FileName)) {
                include($FileName);
                if (is_array($Data)) {
                    while (list($Key, $Value) = each($Data)) {
                        $Templates[$Key] = $Value;
                    }
                }
            }

            if (is_array($TValue["External"])) {
                $this->ExternalValues = $TValue["External"];
            }
            $this->InputSelect($Name."[Value]", $TValue["Value"], $Templates);
        }
    }

    class InputYesNo extends Input {
        function dPrint() {
            $R1 = new InputRadio($this->Name, 1, $this->Value);
            $R1->dPrint();
            echo "&nbsp;Да&nbsp;";
            $R2 = new InputRadio($this->Name, 0, $this->Value);
            $R2->dPrint();
            echo "&nbsp;Нет";
        }

        function InputYesNo($Name, $Value) {
            parent::Input($Name, $Value);
        }
    }

    class InputVisible extends InputYesNo {
        function InputVisible($Name, $Value) {
            parent::InputYesNo($Name, $Value);
        }
    }

    class InputAccess extends InputYesNo {
        function dPrint() {
            parent::dPrint();

            global $sec;
            echo " <input type=\"button\" value=\"Группы\" class=\"gray\" onClick=\"openUrl('edit_access.php?sec=$sec', 'edit_access', this, true, 500, '', 'Доступ в раздел');\">";
        }

        function InputAccess($Name, $Value) {
            parent::InputYesNo($Name, $Value);
        }
    }

    class InputSectionsTree extends InputSelect {
        var $Tree = array();
        var $Tree2 = array();

        function GetThread($Code, $Name, $Level) {
            $PartIndent = "- ";
            $Indent = str_repeat($PartIndent, $Level);

            $this->Tree2[$Code] = $Indent."$Name";
            if (is_array($this->Tree[$Code])) {
                while (list($Key, $Name) = each($this->Tree[$Code])) {
                    $this->GetThread($Key, $Name, $Level+1);
                }
            }
        }

        function GetTree() {
            $result = db_query("SELECT Sec_Code, Sec_Parent, Sec_Name, Sec_LocalDir FROM Sections ORDER BY Sec_Order DESC, Sec_Code");
            $this->Tree = array();
            while (list($Sec_Code, $Sec_Parent, $Sec_Name, $Sec_LocalDir) = mysql_fetch_row($result)) {
                $this->Tree[$Sec_Parent][$Sec_Code] = "$Sec_Name ($Sec_LocalDir)";
            }
            $this->GetThread(0, "Главная страница", 0);
        }

        function InputSectionsTree($Name, $Value, $onClick="", $Style="") {
            $this->GetTree();
            $this->InputSelect($Name, $Value, $this->Tree2, $Style, $onClick);
        }
    }

    class InputEditSectionsTree extends InputSectionsTree {
        function GetThread($Code, $Name, $Level) {
            global $sec;
            if ($Code != $sec) {
                parent::GetThread($Code, $Name, $Level);
            }
        }

        function InputEditSectionsTree($Name, $Value, $onClick="") {
            parent::InputSectionsTree($Name, $Value, $onClick, "max-width:500px");
        }
    }

    class InputMultiSelect extends Input {
        var $Options = Array();
        var $Empty = 0;
        var $onChange = "";
        var $CheckBoxes = false;
        var $Multiple = false;
        var $Type = "checkbox";

        function dPrint() {
            if(!is_array($this->Value)) $this->Value = array();
            $onChange = ($this->onChange ? " onChange=\"".$this->onChange."\"" : "");
            $Style = ($this->Style ? " style=\"".$this->Style."\"" : "");
            $Multiple = ($this->Multiple ? " multiple" : "");
            if(!$this->CheckBoxes) {
                foreach($this->Value as $Key => $Value) {
                    dPrint("<select name=\"".$this->Name."[$Key]\"$Style$onChange>");
                    foreach($this->Options as $oKey => $oValue) {
                        $Checked = ($oValue == $Value) ? "selected" : "";
                        dPrint("<option value=\"$Key\" $Checked>$oValue</option>");
                    }
                    dPrint("</select><br>");
                }

                $rows = (count($this->Options) > 5) ? 7 : count($this->Options);
                dPrint("<select name=\"".$this->Name."[]\"$Style$onChange $Multiple size=\"$rows\">");
                if ($this->Empty) {
                    dPrint("<option value=\"\">&nbsp;</option>");
                }
                foreach($this->Options as $Key => $Value) {
                    $Checked = (in_array($Key, $this->Value) ? " selected" : "");
                    dPrint("<option value=\"$Key\"$Checked>$Value</option>");
                }
                dPrint("</select>");
            } else {
                foreach($this->Options as $Key => $Value) {
                    $Checked = (in_array($Key, $this->Value)? " checked" : "");
                    dPrint("<input type=\"".$this->Type."\" name=\"".$this->Name."".($this->Type != "radio" ? "[]" : "")."\" value=\"$Key\" id=\"$Key\"$Checked class=\"radio\">&nbsp;<label for=\"$Key\">$Value</label><br>");
                }

            };
        }

        function InputMultiSelect($Name, $Value, $Options, $Style="", $onChange="", $CheckBoxes="") {
            $this->Input($Name, $Value, $Style);
            $this->Options = $Options;
            $this->onChange = $onChange;
            $this->CheckBoxes = $CheckBoxes;
        }
    }


    class InputDBMultiSelect extends InputMultiSelect {
        function InputDBMultiSelect($Name, $SelectValue, $TableName, $TableCode, $TableExpr, $TableFilter="", $TableOrder="", $Style="", $onChange="", $CheckBoxes="") {
            if (!$TableOrder) {
                $TableOrder = $TableExpr;
            }
            if ($TableFilter) {
                $TableFilter = "WHERE $TableFilter";
            }
            $Options = DB::selectArray("SELECT $TableCode, $TableExpr FROM $TableName $TableFilter ORDER BY $TableOrder", $TableCode);
            foreach($Options as $Key => $Value)
                $Options[$Key] = $Value[$TableExpr];

            $this->InputMultiSelect($Name, $SelectValue, $Options, $Style, $onChange, $CheckBoxes);
        }
    }

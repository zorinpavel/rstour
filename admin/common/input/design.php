<?

function dPrint($str) {
  echo "$str\n";
}

function dPrintAlert($Alert) {
  dPrint("<script language=\"JavaScript\">alert('$Alert');</script>");
}

function dPrintWindowLocation($Location) {
  dPrint("<script language=\"JavaScript\">window.location=\"$Location\"</script>");
}

function dPrintInputSubmit($Name, $Value) {
  dPrint("<input type=\"submit\" name=\"$Name\" value=\"$Value\">");
}

function dPrintInputHidden($Name, $Value) {
  dPrint("<input type=\"hidden\" name=\"$Name\" value=\"$Value\">");
}

function dPrintInputText($Name, $Value, $Size=10, $Class="") {
  $Class = ($Class ? " class=\"$Class\"" : "");
  dPrint("<input type=\"text\" name=\"$Name\" value=\"$Value\" size=\"$Size\"$Class>");
}

function dPrintInputPassword($Name, $Value, $Size, $Class="") {
  $Class = ($Class ? " class=\"$Class\"" : "");
  dPrint("<input type=\"password\" name=\"$Name\" value=\"$Value\" size=\"$Size\"$Class>");
}

function dPrintInputFile($Name, $Size) {
  dPrint("<input type=\"file\" name=\"$Name\" size=\"$Size\">");
}

function dPrintInputButton($Name, $Value, $OnClick="") {
  if ($OnClick) {
    $OnClickClause = " onClick=\"$OnClick\"";
  }
  dPrint("<input type=\"button\" name=\"$Name\" value=\"$Value\"$OnClickClause>");
}

function dPrintInputCheckBox($Name, $Value, $Disabled="") {
  if ($Disabled) {
    $DisabledClause = " disabled";
  }
  $Checked = ($Value==0 ? "" : " checked");
  dPrint("<input type=\"checkbox\" class=\"checkbox\" name=\"$Name\"$Checked$DisabledClause>");
}

function dPrintInputRadio($Name, $RadioValue, $Value, $Label) {
  $Checked = ($Value != $RadioValue ? "" : " checked");
  dPrint("<input type=\"radio\" name=\"$Name\" value=\"$Value\" class=\"radio\"$Checked><span class=\"MainText\">$Label</span>");
}

function dPrintInputSelect($Name, $Value, $TableName, $TableCode, $TableExpr, $TableOrder="") {
  if (!$TableOrder) {
    $TableOrder = $TableExpr;
  }
  $result = db_query("SELECT $TableCode, $TableExpr FROM $TableName ORDER BY $TableOrder");
  dPrint("<select name=\"$Name\">");
  if ($result) {
    while (list($Code, $Name) = mysql_fetch_row($result)) {
      $Selected = ($Code==$Value ? " selected" : "");
      dPrint("<option value=\"$Code\"$Selected>$Name</option>");
    }
  }
  dPrint("</select>");
}

function dPrintTextArea($Name, $Value, $Rows, $Cols) {
  dPrint("<textarea name=\"$Name\" rows=\"$Rows\" cols=\"$Cols\" wrap=\"virtual\">$Value</textarea>");
}

?>
<?php

    class classImagePreview {

        public static function Make($Path, $TargetDest, $PreviewType) {

            $TypeInfo = ImagePreview_Type::Get($PreviewType);

            $w = $TypeInfo['width'];
            $h = $TypeInfo['height'];

            $im = imagecreatetruecolor($w, $h);

            if(!is_file(CLIENT_PATH.$Path)) {
                $error_message = "no file"; // нет указанного файла
            } else {
                $imsize=getimagesize(CLIENT_PATH.$Path);
                $imsx=$imsize[0];
                $imsy=$imsize[1];
                $imstype=$imsize[2];
                if(!$imstype) $error_message = "no graphics";
                elseif($imstype>3) $error_message = "not support";
            }

            // для jpeg и gif - запускаем обработку
            if($imstype==1) {
                $ims = imagecreatefromgif(CLIENT_PATH.$Path);// echo 'gif';
                $format = "gif";
            } elseif($imstype==2) {
                $ims = imagecreatefromjpeg(CLIENT_PATH.$Path);// echo 'jpg';
                ImageInterlace($im, true);
                $format = "jpg";
            } elseif($imstype==3) {
                $ims = imagecreatefrompng(CLIENT_PATH.$Path);// echo 'png';
                //        $imsize=getimagesize(CLIENT_PATH.$Path); $imsx=$imsize[0]; $imsy=$imsize[1];
                $format = "png";
            } else {
                $ims = imagecreate(1,1);// echo 'none';
                $imsx=1; $imsy=1;
                $format = "gif";
            }

            if( !isset($TypeInfo['width']) && !isset($TypeInfo['height']) ) { return false; };
            if( isset($TypeInfo['width']) && !isset($TypeInfo['height']) ) $h = round($imsy*$w/$imsx);
            if( !isset($TypeInfo['width']) && isset($TypeInfo['height']) ) $w = round($imsx*$h/$imsy);
            if( isset($TypeInfo['fill'])) {
                $hex_color = $TypeInfo['fill'];
                $color = self::html2rgb($hex_color);
            } else {
                $color = array(255,255,255);
            }

            /* оставить истенный размер */
            if($h >= $imsy && $w >= $imsx && !$TypeInfo['watermark']) {
                $w = $imsx;
                $h = $imsy;

                $preview['format'] = $format;
                $preview['dest'] = CLIENT_PATH.$Path;
                return $preview;
            }

            $white_border_color = imagecolorallocate($im, $color[0], $color[1], $color[2]);

            imagefill($im, 1, 1, $white_border_color);

            if($TypeInfo['crop'])
                $koef = max($w/$imsx,$h/$imsy);
            else
                $koef = min($w/$imsx,$h/$imsy);

            if($imstype==3){
                imagealphablending($im, false);
                imagesavealpha($im, true);
                $transparent = imagecolorallocatealpha($im, 255, 255, 255, 127);
                imagefilledrectangle($im, 0, 0, $w, $h, $transparent);
            }

            imagecopyresampled($im, $ims, $w/2-$koef*$imsx/2, $h/2-$koef*$imsy/2, 0, 0, $koef*$imsx+1, $koef*$imsy+1, $imsx, $imsy);
            imagedestroy($ims);

            if(function_exists('imageconvolution')) {
                $intSharpness = self::findSharp($imsx, $w);
                $arrMatrix = array(
                    array(-1, -2, -1),
                    array(-2, $intSharpness + 12, -2),
                    array(-1, -2, -1)
                );
                imageconvolution($im, $arrMatrix, $intSharpness, 0);
            }

            if($TypeInfo['watermark']) {
                // добавляем водный знак
                $imw = imagecreatefrompng(CLIENT_PATH.$TypeInfo['watermark']);
                $imwsize = getimagesize(CLIENT_PATH.$TypeInfo['watermark']); $imwx = $imwsize[0]; $imwy = $imwsize[1];

                /* внизу слева */
//                $paddingx = 5;
//                $paddingy = $h - $imwy - 5;
//                imagecopyresized($im, $imw, $paddingx, $paddingy, 0, 0, $imwx, $imwy, $imwx, $imwy);

                /* по центру */
                $padding = ($w - $imwx)/2;
                imagecopyresized($im, $imw, $padding, $h/2 - $imwy/2, 0, 0, $imwx, $imwy, $imwx, $imwy);

                imagedestroy($imw);
            }

            // вывод системного сообщения (если есть)
            if(strlen($error_message)) {
                imagestring($im, 1, 3, 2, "error:", $white_border_color);
                imagestring($im, 1, 3, 10, $error_message, $white_border_color);
                echo "error: $error_message";
                return false;
            } else {

                $Dirs = explode("/", $Path);
                $Dirs = array_slice($Dirs, 1, -1);

                $DirPath = IPV_PREVIEWS_PATH."/$PreviewType";
                foreach($Dirs as $key => $Dir) {
                    $DirPath .= "/$Dir";
                    if(is_dir($DirPath)) {
                        continue;
                    } else {
                        mkdir($DirPath, 0775);
                        chmod($DirPath, 0775);
                    }
                }

                // запись окончательного изображения
                if($imstype == 2) {
                    imageinterlace($im, true);
                    imagejpeg($im, $TargetDest, $TypeInfo['quality']);
                } elseif($imstype == 1) {
                    imagegif($im, $TargetDest);
                } elseif($imstype == 3) {
                    imagepng($im, $TargetDest);
                }

                imagedestroy($im);

                $preview = array(
                    "format" => $format,
                    "dest" => $TargetDest,
                );

                _autoload("classActualFile");
                classActualFile::Reg($Path, "/".$PreviewType.$Path, $TypeInfo['comment']);

                return $preview;
            }
        }

        public static function html2rgb($color) {
            if ($color[0] == '#')
                $color = substr($color, 1);
            if (strlen($color) == 6)
                list($r, $g, $b) = array($color[0].$color[1],
                    $color[2].$color[3],
                    $color[4].$color[5]);
            elseif (strlen($color) == 3)
                list($r, $g, $b) = array($color[0].$color[0], $color[1].$color[1], $color[2].$color[2]);
            else
                return false;
            $r = hexdec($r);
            $g = hexdec($g);
            $b = hexdec($b);
            return array($r, $g, $b);
        }

        /* sharpen images function */
        public static function findSharp($intOrig, $intFinal) {
            $intFinal = $intFinal * (750.0 / $intOrig);
            $intA     = 52;
            $intB     = -0.27810650887573124;
            $intC     = .00047337278106508946;
            $intRes   = $intA + $intB * $intFinal + $intC * $intFinal * $intFinal;
            return max(round($intRes), 0);
        }


    }

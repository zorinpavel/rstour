<?

class ImagePreview_Details {

        function Get($TypeInfo,$SceneInfo,$Source,$width=null,$height=null,$orient=null,$dontusesrc=false) {
        
                $SourceFile=$Source;
                $Extra=null;
                $AF_Src=null;

                if(isset($TypeInfo['ExtraClass'])) {
                        $ManagerClass=$TypeInfo['ExtraClass'];
                        __autoload($ManagerClass);
                        if(class_exists($ManagerClass)) {
                                $Manager =& new $ManagerClass();
                                list($SourceFile,$Extra,$AF_Src)=$Manager->Get($Source);
                                if(!$SourceFile) return array();
                        }
                }

                if(!$AF_Src) $AF_Src=$SourceFile;

                // по умолчанию ничего не знаем...
                $previewwidth = null;
                $previewheight = null;
                $previewformat = IPV_DEFAULT_FORMAT;
                $previewrotate = 0;

                $orientRotation=0;

                // если необходимо, берем данные о размере картинки из самого файла
                if(!$width && !$height && !$dontusesrc) {
                        if(false===(list($width, $height, $type)=@getimagesize(IPV_IMAGES_PATH.'/'.$SourceFile))) {
                                echo("error: ".IPV_IMAGES_PATH.'/'.$SourceFile);
                                echo "no image!";
                                die();
                        }
                }

                // определение формата превью исходя из имени записываемого файла
                $possibleFormats = array('gif','jpeg','png');
                preg_match('/\S+\.(\S+)$/',$Source,$m);
                $formatFromFilename = $m[1];
                if(strtolower($formatFromFilename) == 'jpg') $formatFromFilename = 'jpeg';
                if(in_array(strtolower($m[1]),$possibleFormats)) $previewformat = strtolower($m[1]);
                else $previewformat = 'jpeg';

                // читаем exif для проверки поворота JPEG-изображения
                if(!$orient && !$dontusesrc) {
                        $exifData = @exif_read_data(IPV_IMAGES_PATH.'/'.$SourceFile);
                        if(sizeof($exifData) && isset($exifData['Orientation'])) $orient = $exifData['Orientation'];
                        else $orient = 1;
                        if($orient == 6) $orientRotation = 90;
                        elseif($orient == 8) $orientRotation = 270;
                        else $orientRotation = 0;
                }

                // поворот исходя из сцены и ориентации
                $previewrotate = $orientRotation + $SceneInfo['rotate'];
                $previewrotate = ($previewrotate % 360);
                

                // берем размеры изображения, которое будет наложено на превьюху
                if($width && $height) {

                        if(!(($previewrotate - 90) % 180)) {
                                $temp = $width;
                                $width = $height;
                                $height = $temp;
                        }

                        //сделано для того, чтобы можно было указать только высоту или только ширину по какой сжимать
                        $sootn = $width/$height;
                        if((!isset($TypeInfo['height']))&&(isset($TypeInfo['width']))) $TypeInfo['height'] = $TypeInfo['width']/$sootn;
                        if((!isset($TypeInfo['width']))&&(isset($TypeInfo['height']))) $TypeInfo['width'] = $TypeInfo['height']*$sootn;

                        // определяем коэффициент уменьшения превью исходя из размеров изображения
                        $resizeCoef = ($TypeInfo['fill'] == "crop")?(max($TypeInfo['width']/$width,$TypeInfo['height']/$height)):(min($TypeInfo['width']/$width,$TypeInfo['height']/$height));
                        if(!$TypeInfo['enlarge'] && $resizeCoef>1) $resizeCoef=1;
                        $previewimagewidth = round($width * $resizeCoef);
                        $previewimageheight = round($height * $resizeCoef);                     

                }

                // попытка определить размеры превью только исходя из типа превью
                if(isset($TypeInfo['fill']) && $TypeInfo['fill']!='') {
                        $previewwidth = $TypeInfo['width'];
                        $previewheight = $TypeInfo['height'];
                }

                // в противном случае нужно знать размеры картинки
                elseif($resizeCoef && $width && $height) {
                        $previewwidth = $width*$resizeCoef;
                        $previewheight = $height*$resizeCoef;
                }
/*
                // если знаем размеры картинки, то вычисляем размеры превьюхи
                elseif($width && $height) {

                        // определяем коэффициент уменьшения превью исходя из размеров изображения
                        $resizeCoef = ($TypeInfo['fill'] == "crop")?(max($TypeInfo['width']/$width,$TypeInfo['height']/$height)):(min($TypeInfo['width']/$width,$TypeInfo['height']/$height));
                        if(!$TypeInfo['enlarge'] && $resizeCoef>1) $resizeCoef=1;
                        $previewwidth = round($width * $resizeCoef);
                        $previewheight = round($height * $resizeCoef);

                }
*/
                
                // возвращаем параметры
                return array(
                        'format' => $previewformat,
                        'width' => $previewwidth,
                        'height' => $previewheight,
                        'rotate' => $previewrotate,
                        'fillColor' => $TypeInfo['fill'],
                        'previewimagewidth' => $previewimagewidth,
                        'previewimageheight' => $previewimageheight,
                        'Extra' => $Extra,
                        'SourceFile' => $SourceFile,
                        'AF_Src' => $AF_Src,
                );

        }

}

?>
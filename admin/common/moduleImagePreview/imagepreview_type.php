<?php

class ImagePreview_Type {
    public static function Get ($type = 'default') {
        if(!$type) $type = 'default';
        static $typesInfo = array();
        if(isset($typesInfo[$type])) return $typesInfo[$type];

        if($type!='default') {
            $settingsPath = IPV_PREVIEWS_PATH."/$type/_settings.php";
            if(file_exists($settingsPath) && @include($settingsPath)) {
                $typesInfo[$type] = $_settings;
                return $typesInfo[$type];
            } else {
                return false;//чтоб не было хуйни
            }
        }

        $settingsPath = IPV_PREVIEWS_PATH."/_settings.php";
        if(file_exists($settingsPath) && @include($settingsPath)) {
            $typesInfo[$type]=$_settings;
        } else {
            $typesInfo[$type]=false;
        }
        return $typesInfo[$type];
    }
}

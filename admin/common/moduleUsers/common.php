<?php

  class InputUsers extends InputModules {
    function InputUsers($Name, $Value) {
      parent::InputModules($Name, $Value, "classUsers");
    }
  }

  class InputGroup extends InputDBSelect {
    function InputGroup($Name, $Value) {
      $this->Empty = 1;
      $this->InputDBSelect($Name, $Value, "Groups", "Gro_Code", "Gro_Name", "", "Gro_Order", "", $onChange);
    }
  }

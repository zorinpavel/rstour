<?php

  self::$EnterErrors = array(
    1  => "You incorrectly entered the password, check the keyboard and try again.", 
    2  => "User not found.", 
    3  => "You must enter a password.", 
    4  => "You must enter a login or e-mail.", 
    5  => "Your registration is not active, you need to confirm the existence of the mail box.", 
    6  => "You need to log in to change the data.", 
    7  => "All required fields must be filled.", 
    8  => "Passwords mismatch.", 
    9  => "You must enter old password.",
    10 => "Invalid old password.", 
    11 => "This username is already taken, choose another.", 
    12 => "This e-mail is already taken, choose another.", 
    13 => "Invalid file format.", 
    14 => "File istoo big.", 
    15 => "Invalid image code.", 
    16 => "The password must be at least 6 characters.", 
    17 => "Can not register this username.", 
  );

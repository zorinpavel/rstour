<?php

    class classUsers extends classProperties {

        public static $User;
        public static $PageUser;

        public static $DoID = "udo";
        public static $UserId = "us_id";

        public static $EnterErrors = array();
        public static $Messages = array();

        public static $MySectionPath = "/user/";

        public $ParentSection = 0;
        public $AjaxData;

        var $Properties = array(
            "public" => array(
                "Names" => array(
                    "mailReg",
                    "UseCaptcha",
                    "mailTemplate",
                    "mailRegTemplate",
                    "mailPassTemplate",
                    "LoginMail",
                    "UseLocalDir",
                    "MySection",
                ),
                "Types" => array(
                    "InputYesNo",
                    "InputYesNo",
                    "InputTemplates",
                    "InputTemplates",
                    "InputTemplates",
                    "InputYesNo",
                    "InputYesNo",
                    "InputSectionsTree",
                ),
                "Labels" => array(
                    "Подтверждать регистрацию?",
                    "Использовать капчу?",
                    "Шаблон письма по умолчанию",
                    "Шаблон письма регистрация",
                    "Шаблон письма забыли пароль",
                    "Логин = email",
                    "Использовать localdir?",
                    "Линый кабинет",
                )
            ),
            "private" => array(
                "Names" => array(
                    "Group",
                    "template_enter",
                    "template_reg",
                    "template_confirm",
                    "template_forgot",
                    "template_change",
                    "template_edit",
                    "Separator",
                    "template_info_short",
                    "template_info",
                ),
                "Types" => array(
                    "InputGroup",
                    "InputTemplates",
                    "InputTemplates",
                    "InputTemplates",
                    "InputTemplates",
                    "InputTemplates",
                    "InputTemplates",
                    "Separator",
                    "InputTemplates",
                    "InputTemplates",
                ),
                "Labels" => array(
                    "Группа",
                    "Вход",
                    "Регистрация",
                    "Подтверждение",
                    "Забыли пароль",
                    "Сменить пароль",
                    "Редактировать данные",
                    "Личный кабинет",
                    "Короткая информация",
                    "Полный профиль",
                )
            )
        );


        var $DefaultTemplates = array(
            "template_enter" => array(0 => "
    <whileins data=\"Errors\">
      <div class=\"error\"><ins type=\"data\" name=\"Error\"></div>
    </whileins>
    <form method=\"POST\">
      <input type=\"hidden\" name=\"<ins type=\"data\" name=\"DoID\">\" value=\"enter\">
      Вход
      <input type=\"text\" name=\"login\" value=\"\" placeholder=\"Логин или e-mail\">
      <input type=\"password\" name=\"pass\" value=\"\">
      <input type=\"submit\" value=\"Войти\">
      <div>
        <a href=\"<ins type=\"data\" name=\"MySectionPath\">?<ins type=\"data\" name=\"DoID\">=reg\">регистрация</a>
        <a href=\"<ins type=\"data\" name=\"MySectionPath\">?<ins type=\"data\" name=\"DoID\">=forgot\">забыли пароль?</a>
      </div>
    </form>
          "),
            "template_reg" => array(0 => "
    <whileins data=\"Errors\">
      <div class=\"error\"><ins type=\"data\" name=\"Error\"></div>
    </whileins>
    <a href=\"<ins type=\"data\" name=\"MySectionPath\">?<ins type=\"data\" name=\"DoID\">=enter\">Уже зарегистрированы на сайте?</a>
    <ifins notexists=\"RegOk\">
      <form method=\"POST\" enctype=\"multipart/form-data\">
        <input type=\"hidden\" name=\"<ins type=\"data\" name=\"DoID\">\" value=\"reg\">
        Логин: <input type=\"text\" name=\"login\" value=\"\"><br>
        E-mail: <input type=\"text\" name=\"mail\" value=\"\"><br>
        Пароль: <input type=\"password\" name=\"pass\" value=\"\"><br>
        Пароль еще раз: <input type=\"password\" name=\"pass2\" value=\"\"><br>
        <input type=\"submit\" value=\"Регистрация\">
      </form>
    </ifins>
    <ifins exists=\"RegOk\">
      Регистрация прошла успешно, спасибо!
    </ifins>
          "),
            "template_confirm" => array(0 => ""),
            "template_forgot" => array(0 => "
    <whileins data=\"Errors\">
      <div class=\"error\"><ins type=\"data\" name=\"Error\"></div>
    </whileins>
    <form method=\"POST\">
      <input type=\"hidden\" name=\"<ins type=\"data\" name=\"DoID\">\" value=\"forgot\">
      <input type=\"hidden\" name=\"Update\" value=\"1\">
      <input type=\"text\" name=\"mail\" class=\"js ClearValue\" value=\"Логин или e-mail\" onclick=\"return {};\">
      <input type=\"submit\" value=\"Ок\">
    </form>
          "),
            "template_change" => array(0 => ""),
            "template_edit" => array(0 => ""),
            "template_info_short" => array(0 => ""),
            "template_info" => array(0 => ""),
            "mailTemplate" => array(0 => ""),
            "mailRegTemplate" => array(0 => ""),
            "mailPassTemplate" => array(0 => ""),
        );


        function GetClassName() {
            return __CLASS__;
        }


        function Action($Do = "") {

            if(!self::$User)
                $this->GetInfo();
            else
                $this->Data = array_merge($this->Data, self::$User);

            // $realDo = $_REQUEST[$this->GetProperty("Do")] ? $_REQUEST[$this->GetProperty("Do")] : $Do;
            $realDo = ( in_array($Do, array("short", "info", "enter")) ) ? $Do : $_REQUEST[self::$DoID];

            if((int)$_REQUEST['error'])
                array_push($this->Data['Errors'], array("Error" => self::$EnterErrors[(int)$_REQUEST['error']]));
            if(count($this->Data['Errors']))
                $this->Data['Error'] = 1;

            switch($realDo) {
                case "short":
                    $this->ShortInfo();
                    break;
                case "info":
                    $this->Info();
                    break;
                case "reg":
                    if($_REQUEST['login'] || $_REQUEST['mail'])
                        $this->CheckValidReg();
                    $this->Ins2Php("template_reg");
                    break;
                case "fastreg":
                    if($_REQUEST['mail'])
                        $this->CheckValidFastReg(false);
                    $this->Ins2Php("template_reg");
                    break;
                case "forgot":
                    $this->ForgotPass();
                    break;
                case "change":
                    $this->ChangePass();
                    break;
                case "edit":
                    $this->Edit();
                    break;
                case "delphoto":
                    $this->DeletePhoto();
                    break;
                case "enter" :
                    if(!$this->Data['EnterOk'] && !self::$User['us_id']) {
                        $this->Ins2Php("template_enter");
                    } else {
                        $this->Info();
                    }
                    break;
                case "confirm" :
                    $Sum = str_replace("\W+", "", $_REQUEST['key']);
                    $s = DB::Query("SELECT us_id, login, mail, passwd FROM Users WHERE md5(login) = '".mysql_real_escape_string($Sum)."'");
                    list($us_id, $login, $this->Data['mail'], $passwd) = mysql_fetch_row($s);
                    if($us_id) {
                        DB::Query("UPDATE Users SET status = 1 WHERE us_id = $us_id");
                        // поменять статус броней с "Аккаунт не поддтвержден" на "На бронированиии"
                        DB::Query("UPDATE cartData SET Ord_Status = 4 WHERE us_id = $us_id AND Ord_Status = 8");
                        array_push($this->Data['Messages'], array("Message" => self::$Messages['ConfirmOk']));
                        $this->Data['Message'] = 1;
                    } else {
                        array_push($this->Data['Errors'], array("Error" => self::$EnterErrors[2]));
                    }
                    $this->Ins2Php("template_confirm");
                    $this->Ins2Php("template_enter");
                    break;
                default:
                    $this->Info();
                    break;
            }

        }


        public function ShortInfo() {
            //          if(self::$User['us_id']) {
            $this->Ins2Php("template_info_short");
            //          } else {
            //            $this->Ins2Php("template_enter");
            //          }
        }


        public function Info() {

            if(!self::$PageUser['us_id']) {
                $this->GetPageUserInfo();
            }

            $this->Data['owner'] = (self::$PageUser['us_id'] == self::$User['us_id']);

            if(!self::$PageUser['us_id']) {
                if(!self::$User['us_id']) {
                    $this->Ins2Php("template_enter");
                } else {
                    $this->Data = array_merge($this->Data, $this->GetTotalInfo(self::$User));
                    $this->Ins2Php("template_info");
                }
            } else {
                $this->Data = array_merge($this->Data, $this->GetTotalInfo(self::$PageUser));
                $this->Ins2Php("template_info");
            }

        }


        public function GetPageUserInfo() {
//            preg_match("/^(user\/)?([a-zA-Z0-9-_\.]+)(?<!\/)/", $_REQUEST['_path'], $Localdir);
//            if((int)$Localdir[2]) {
//                list($us_id, $Userdir) = mysql_fetch_row(db_query("SELECT us_id, us_id FROM Users WHERE us_id = ".(int)$Localdir[2]));
//            } elseif($Localdir[2]) {
//                list($us_id, $Userdir) = mysql_fetch_row(db_query("SELECT us_id, localdir FROM Users WHERE localdir = '".strtolower(mysql_real_escape_string($Localdir[2]))."'"));
//            }
//            if($us_id)
//                self::$PageUser = $this->GetUserInfo($us_id);

            /* если пользователя страницы не найдено считаем что это он сам */
            if(!self::$PageUser)
                self::$PageUser = self::$User;
        }


        public function GetInfo() {
            if($this->SessId && $this->SessId != "") {
                list($us_id) = mysql_fetch_row(db_query("SELECT u.us_id FROM Users AS u WHERE u.session = '".$this->SessId."'"));
                if($us_id)
                    $this->Data = array_merge($this->Data, $this->GetUserInfo($us_id));
            }

            $this->RegisterUserAction();
            self::$User = $this->Data;

            /* берем пользователя текущей страницы */
            $this->GetPageUserInfo();
        }


        function Edit() {
            if(!self::$User['us_id']) {
                array_push($this->Data['Errors'], array("Error" => self::$EnterErrors[6]));
                $this->Ins2Php("template_enter");
                return;
            }

            $this->Data = $this->GetTotalInfo(self::$User);

            if($_REQUEST['Update'] && $this->Data['us_id']) {
                $login   = $this->Data['login'] = mysql_real_escape_string($_REQUEST['login']);
                $mail    = $this->Data['mail']  = mysql_real_escape_string($_REQUEST['mail']);
                $oldpass = mysql_real_escape_string($_REQUEST['oldpass']);
                $passwd  = mysql_real_escape_string($_REQUEST['pass']);
                $passwd2 = mysql_real_escape_string($_REQUEST['pass2']);
                $name    = $this->Data['name']  = mysql_real_escape_string($_REQUEST['name']);
                $phone   = $this->Data['phone'] = preg_replace( "/\W+/i", "", trim( strip_tags($_REQUEST['phone']) ) );
                $descr   = $this->Data['descr'] = mysql_real_escape_string(substr(strip_tags($_REQUEST['descr']), 0, 255));
                $bth_day    = $this->Data['bth_day']   = (int)$_REQUEST['bth_day'];
                $bth_month  = $this->Data['bth_month'] = (int)$_REQUEST['bth_month'];
                $bth_year   = $this->Data['bth_year']  = (int)$_REQUEST['bth_year'];

                if(!$login || !$mail)
                    array_push($this->Data['Errors'], array("Error" => self::$EnterErrors[7]));
                if($passwd != $passwd2) {
                    array_push($this->Data['Errors'], array("Error" => self::$EnterErrors[8]));
                    if(!$oldpass)
                        array_push($this->Data['Errors'], array("Error" => self::$EnterErrors[9]));
                }

                $r = db_query("SELECT us_id, login, mail, passwd, photo FROM Users WHERE us_id = ".$this->Data['us_id']);
                list($dbus_id, $dblogin, $dbmail, $dbpass, $dbphoto) = mysql_fetch_row($r);
                if($passwd != "" && $dbpass != md5($oldpass)) {
                    array_push($this->Data['Errors'], array("Error" => self::$EnterErrors[10]));
                } elseif(!$passwd) {
                    $newpass = $dbpass;
                } else {
                    $newpass = md5($passwd);
                }

                $r = db_query("SELECT us_id FROM Users WHERE LOWER(login) = LOWER('$login')");
                list($us_id) = mysql_fetch_row($r);
                if($us_id && $us_id != $this->Data['us_id']) {
                    array_push($this->Data['Errors'], array("Error" => self::$EnterErrors[11]));
                }

                $mr = db_query("SELECT us_id FROM Users WHERE LOWER(mail) = LOWER('$mail')");
                list($mus_id) = mysql_fetch_row($mr);
                if($mus_id && $mus_id != $this->Data['us_id']) {
                    array_push($this->Data['Errors'], array("Error" => self::$EnterErrors[12]));
                }

                if($_FILES['photo']['size'] > 0) {
                    $Types = array("image/gif", "image/jpeg", "image/jpg");
                    if(!in_array($_FILES['photo']['type'], $Types)) {
                        array_push($this->Data['Errors'], array("Error" => self::$EnterErrors[13]));
                    }
                    if($_FILES['photo']['size'] > 1000000) {
                        array_push($this->Data['Errors'], array("Error" => self::$EnterErrors[14]));
                    }
                    if($dbphoto != "" && file_exists(CLIENT_PATH.$dbphoto)) {
                        unlink(CLIENT_PATH.$dbphoto);
                        $ACF = _autoload("classActualFile");
                        $ACF->Expire($dbphoto);
                    }
                    preg_match("/\..{3}/", $_FILES['photo']['name'], $ext);
                    $photo = "/data/users/$dbus_id.".time().strtolower($ext[0]);
                    move_uploaded_file($_FILES['photo']['tmp_name'], CLIENT_PATH.$photo);
                } else {
                    $photo = $dbphoto;
                }
                if(!count($this->Data['Errors'])) {
                    db_query("UPDATE Users SET login = '$login', mail = '$mail', passwd = '".$newpass."', name = '$name', phone = '".$phone."', descr = '$descr', photo = '$photo' WHERE us_id = ".$this->Data['us_id']);
                    $this->UpdateData();
                    $this->Data['EditOk'] = 1;

                    if($this->isJsHttpRequest)
                        $this->AjaxData['Location'] = $this->Data['page'];
                    else
                        header("Location: ".$this->Data['page']);
                }
            }
            if(count($this->Data['Errors']))
                $this->Data['Error'] = 1;

            $this->Ins2Php("template_edit");

        }


        public function PageExit() {
            $this->ForgetUser();
            if($_REQUEST['back'])
                $Url = mysql_real_escape_string($_REQUEST['back']);
            else
                $Url = "/";
            header("Location: $Url");
            die();
        }


        public function PageEnter($Back = false, $mail = "", $login = "", $pass = "") {
            $BackUrl = $Back ?: mysql_real_escape_string($_REQUEST['back']);

            $mail = $mail ?: $_REQUEST['mail'];
            $login = $login ?: $_REQUEST['login'];
            $pass = $pass ?: $_REQUEST['pass'];

            if($mail || $login) {
                $UserError = self::CheckValidEnter($login, $mail ,$pass);
                if($UserError) {
                    if(self::GetProperty("MySection")) {
                        $BackUrl = $BackUrl ? "&back=$BackUrl" : "";

                        if($this->isJsHttpRequest)
                            $this->AjaxData['Location'] = self::GetPath(self::GetProperty("MySection"))."?".$this->Data['DoID']."=enter&error=$UserError$BackUrl";
                        else
                            header("Location: ".self::GetPath(self::GetProperty("MySection"))."?".$this->Data['DoID']."=enter&error=$UserError$BackUrl");
                        die;
                    }
                } elseif($BackUrl) {
                    if($this->isJsHttpRequest)
                        $this->AjaxData['Location'] = $BackUrl;
                    else
                        header("Location: $BackUrl");
                    die;
                }
            }
        }


        function ForgotPass() {
            $login = mysql_real_escape_string($_REQUEST['mail']);
            $email = mysql_real_escape_string($_REQUEST['mail']);

            $Cond = array();
            if(strlen($login))
                $Cond[] = "login = '$login'";
            if(strlen($email))
                $Cond[] = "mail = '$email'";
            $Conds = implode(" OR ", $Cond);

            if($_REQUEST['Update']) {
                if(strlen($Conds)) {
                    $r = db_query("SELECT us_id, login, mail, name, passwd FROM Users WHERE $Conds");
                    list($us_id, $dlogin, $dmail, $dname, $passwd) = mysql_fetch_row($r);
                    if($dlogin && $dmail) {

                        $log = md5($dlogin);
                        $Subject = self::$Messages['PassSubject'];
                        $url = DOMAIN_URL.self::$MySectionPath."?udo=change&hp=$passwd&hin=$log";
                        $Message = "$url";

                        $Mail = _autoload("classMail");
                        $Mail->Data = array_merge($Mail->Data,
                            array(
                                "Subject" => $Subject,
                                "Message" => $Message,
                                "mail" => $dmail,
                                "name" => $dname,
                                "dlogin" => $dlogin,
                                "url" => $url,
                            )
                        );

                        $Template = $this->GetTemplate("mailPassTemplate");
                        if(!$Template || $Template == "")
                            $Template = $Mail->GetTemplate("mailTemplate");
                        $MessageHtml = $Mail->MailTemplate2Php($Template);

                        $Mail->SendMail($dmail, $Subject, $MessageHtml);

                        array_push($this->Data['Messages'], array("Message" => "На указанную почту, Вам выслана ссылка для смены пароля."));
                    } else {
                        array_push($this->Data['Errors'], array("Error" => self::$EnterErrors[2]));
                    }
                } else {
                    array_push($this->Data['Errors'], array("Error" => self::$EnterErrors[4]));
                }
            }

            if(count($this->Data['Errors']))
                $this->Data['Error'] = 1;

            if(count($this->Data['Messages']))
                $this->Data['Message'] = 1;

            $this->Ins2Php("template_forgot");
        }


        function ChangePass() {
            $rpass = mysql_real_escape_string($_REQUEST['hp']);
            $rlogin = mysql_real_escape_string($_REQUEST['hin']);

            $User = DB::selectArray("SELECT us_id, login, mail FROM Users WHERE passwd = '$rpass' AND md5(login) = '$rlogin'");
            $this->Data = array_merge($this->Data, $User);

            if($User['us_id']) {
                if($_REQUEST['Update']) {
                    $passwd  = mysql_real_escape_string($_REQUEST['pass']);
                    $passwd2 = mysql_real_escape_string($_REQUEST['pass2']);
                    if($passwd != $passwd2)
                        array_push($this->Data['Errors'], array("Error" => self::$EnterErrors[8]));

                    if($passwd && count($this->Data['Errros']) <= 0) {
                        db_query("UPDATE Users SET passwd = '".md5($passwd)."' WHERE us_id = ".$User['us_id']);
                        array_push($this->Data['Messages'], array("Message" => "Ваш пароль изменен"));
                        $this->SetUser($User['us_id']);

                        if($this->isJsHttpRequest)
                            $this->AjaxData['Location'] = self::$MySectionPath;
                        else
                            header("Location: ".self::$MySectionPath);

                    } else {
                        array_push($this->Data['Errors'], array("Error" => self::$EnterErrors[3]));
                    }
                }
            } else {
                array_push($this->Data['Errors'], array("Error" => self::$EnterErrors[2]));
            }

            if(count($this->Data['Errors']))
                $this->Data['Error'] = 1;

            $this->Ins2Php("template_change");
        }


        function CheckValidFastReg($Redirect = true) {
            $email   = $this->Data['mail']  = mysql_real_escape_string($_REQUEST['mail']);
            $login   = $this->Data['login'] = $_REQUEST['mail'] ? mysql_real_escape_string(preg_replace("/@([\w|\.]+)/i", "", $_REQUEST['mail'])) : mysql_real_escape_string(preg_replace("/\D+/i", "", $_REQUEST['phone']));
            $name    = $this->Data['name']  = mysql_real_escape_string($_REQUEST['name']);
            $lastname = $this->Data['lastname'] = mysql_real_escape_string($_REQUEST['lastname']);
            $middlename = $this->Data['middlename'] = mysql_real_escape_string($_REQUEST['middlename']);
            $phone   = $this->Data['phone'] = preg_replace( "/\W+\+/i", "", trim( strip_tags($_REQUEST['phone']) ) );

            $passwd  = mysql_real_escape_string($_REQUEST['pass']);
            $passwd2 = mysql_real_escape_string($_REQUEST['pass2']);
            if(!$passwd)
                $passwd = $passwd2 = bin2hex(openssl_random_pseudo_bytes(4));
            
            if($this->GetProperty("UseCaptcha")) {
                if(!$_REQUEST['verify_code'] || strtoupper($_REQUEST['verify_code']) != strtoupper($_SESSION['verify_code'])) {
                    array_push($this->Data["Errors"], array("Error" => self::$EnterErrors[15]));
                }
                
//                // Google captcha
//                $link = "https://www.google.com/recaptcha/api/siteverify";
//                $ch = curl_init();
//                curl_setopt($ch, CURLOPT_URL, $link);
//                curl_setopt($ch, CURLOPT_HEADER, false);
//                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//                curl_setopt($ch, CURLOPT_TIMEOUT, 1);
//                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
//                curl_setopt($ch, CURLOPT_POST, 1);
//                curl_setopt($ch, CURLOPT_POSTFIELDS, "secret=6LfctR0UAAAAAFP2gwXssB3u0PyCohsdbMLfN2_U&response=".$_REQUEST['g-recaptcha-response']);
//                $string = curl_exec($ch);
//                $data['curl_errno'] = curl_errno($ch);
//                if(!curl_errno($ch) && !strpos($string, "503")) {
//                    $data = json_decode($string, true);
//                }
//                curl_close($ch);
//
//                if(!$data['success'])
//                    array_push($this->Data["Errors"], array("Error" => self::$EnterErrors[15]));
            }

            if(!$name || !$phone)
                array_push($this->Data['Errors'], array("Error" => self::$EnterErrors[7]));
    
            if(!$passwd || strlen($passwd) < 6)
                array_push($this->Data['Errors'], array("Error" => self::$EnterErrors[16]));
            if(!$passwd || $passwd != $passwd2)
                array_push($this->Data['Errors'], array("Error" => self::$EnterErrors[8]));

            list($email_name, $email_domain) = explode("@", $email);
            if(in_array($email_domain, ['thefmail.com']))
                array_push($this->Data['Errors'], array("Error" => self::$EnterErrors[4]));

            $us_id = DB::selectValue("SELECT us_id FROM Users WHERE LOWER(login) = LOWER('$login')".($email ? " OR LOWER(mail) = LOWER('$email')" : ""));
            if($us_id)
                array_push($this->Data['Errors'], array("Error" => self::$EnterErrors[18]));
    
            $this->Data['Error'] = count($this->Data['Errors']);
            
            if(!$this->Data['Error']) {
                $Group = $this->GetProperty("Group");
                $status = $this->GetProperty("mailReg") ? 0 : 1;
    
                DB::Query("INSERT INTO Users (login, mail, passwd, status, name, lastname, middlename, phone) VALUES ('$login', '$email', '".md5($passwd)."', $status, '$name', '$lastname', '$middlename', '$phone')");
                $us_id = mysql_insert_id();
    
                if($Group > 0)
                    DB::Query("INSERT INTO UserGroups SET us_id = ".$us_id.", Gro_Code = $Group");
    
                if($this->GetProperty("mailReg") && $email) {
                    $confirmLink = DOMAIN_URL.self::$MySectionPath."?udo=confirm&key=".md5($login);
                    $Message = "<br><br>Прежде чем Вы сможете авторизоваться необходимо подтвердить что Вы являетесь владельцем этого почтового ящика.<br>
                                        Для активации аккаунта перейдите по ссылке <a href=\"$confirmLink\">$confirmLink</a>";
    
                    $Subject = self::$Messages['RegSubject'];
                    $Mail = _autoload("classMail");
                    $External = array(
                        "Subject"  => $Subject,
                        "Message"  => $Message,
                        "mail"     => $email,
                        "name"     => ($name ?: $login),
                        "login"    => $login,
                        "password" => $passwd,
                    );
    
                    $Template = $this->GetTemplate("mailRegTemplate");
                    $MessageHtml = $Mail->MailTemplate2Php($Template, $External);
    
                    $Mail->SendMail($email, $Subject, $MessageHtml);
                }

                if(count($Mail->Errors)) {
                    $this->Data['Error'] = 1;
                    foreach($Mail->Errors as $Error)
                        array_push($this->Data['Errors'], array("Error" => $Error));
                }

                if(!$this->Data['Error']) {
                    array_push($this->Data['Messages'], array("Message" => self::$Messages['FastRegOk']));
                    $this->Data['Message'] = count($this->Data['Messages']);
                    $this->SetUser($us_id);
                }
                
                if($Redirect) {
                    if($this->isJsHttpRequest) {
                        if(!$this->AjaxData['Location'])
                            $this->AjaxData['Location'] = self::$MySectionPath;
                    } else {
                        header("Location: ".self::$MySectionPath);
                    }
                    die;
                }
                
            }

        }
        
        
        function CheckValidReg($Redirect = true) {
            $this->Data['Errors'] = array();

            if($this->GetProperty("LoginMail"))
                $login = $this->Data['login'] = mysql_real_escape_string(preg_replace("/@([\w|\.]+)/i", "", $_REQUEST['mail']));
            else
                $login = $this->Data['login'] = mysql_real_escape_string($_REQUEST['login']);

            $email   = $this->Data['mail'] = mysql_real_escape_string($_REQUEST['mail']);

            $passwd  = mysql_real_escape_string($_REQUEST['pass']);
            $passwd2 = mysql_real_escape_string($_REQUEST['pass2']);
            if(!$passwd)
                $passwd = $passwd2 = bin2hex(openssl_random_pseudo_bytes(4));

            $name    = $this->Data['name'] = mysql_real_escape_string($_REQUEST['name']);
            $lastname = $this->Data['lastname'] = mysql_real_escape_string($_REQUEST['lastname']);
            $middlename = $this->Data['middlename'] = mysql_real_escape_string($_REQUEST['middlename']);
            $phone   = $this->Data['phone'] = preg_replace( "/\W+\+/i", "", trim( strip_tags($_REQUEST['phone']) ) );
            $descr   = mysql_real_escape_string(substr(strip_tags($_REQUEST['descr']), 0, 255));
            $this->Data['descr'] = str_replace("<br>", "\n", $descr);
            $bth_day    = $this->Data['bth_day'] = (int)$_REQUEST['bth_day'] ?: "NULL";
            $bth_month  = $this->Data['bth_month'] = (int)$_REQUEST['bth_month'] ?: "NULL";
            $bth_year   = $this->Data['bth_year'] = (int)$_REQUEST['bth_year'] ?: "NULL";
            $localdir = $this->Data['localdir'] = $this->GetProperty("UseLocalDir") ? "'".strtolower(Cyr2Lat($login))."'" : "NULL";
    
            if($this->GetProperty("UseCaptcha")) {
//                if(!$_REQUEST['verify_code'] || strtoupper($_REQUEST['verify_code']) != strtoupper($_SESSION['verify_code'])) {
//                    array_push($this->Data["Errors"], array("Error" => self::$EnterErrors[15]));
//                }
        
                // Google captcha
                $link = "https://www.google.com/recaptcha/api/siteverify";
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $link);
                curl_setopt($ch, CURLOPT_HEADER, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_TIMEOUT, 1);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, "secret=6LfctR0UAAAAAFP2gwXssB3u0PyCohsdbMLfN2_U&response=".$_REQUEST['g-recaptcha-response']);
                $string = curl_exec($ch);
                $data['curl_errno'] = curl_errno($ch);
                if(!curl_errno($ch) && !strpos($string, "503")) {
                    $data = json_decode($string, true);
                }
                curl_close($ch);
        
                if(!$data['success'])
                    array_push($this->Data["Errors"], array("Error" => self::$EnterErrors[15]));
            }

            if(!$login || !$email)
                array_push($this->Data['Errors'], array("Error" => self::$EnterErrors[4]));
            if(!$passwd || strlen($passwd) < 6)
                array_push($this->Data['Errors'], array("Error" => self::$EnterErrors[16]));
            if(!$passwd || $passwd != $passwd2)
                array_push($this->Data['Errors'], array("Error" => self::$EnterErrors[8]));

            list($email_name, $email_domain) = explode("@", $email);
            if(in_array($email_domain, ['thefmail.com']))
                array_push($this->Data['Errors'], array("Error" => self::$EnterErrors[4]));

            if(!$name || !$lastname || !$middlename || !$phone)
                array_push($this->Data['Errors'], array("Error" => self::$EnterErrors[7]));

            if($this->GetProperty("UseLocalDir")) {
                list($Sec_Code) = mysql_fetch_row(db_query("SELECT Sec_Code FROM Sections WHERE Sec_LocalDir = $localdir"));
                if($Sec_Code || in_array($localdir, array("admin", "setup", "data", "img", "jas", "css", "imagepreview", "js", "_cron", "ie6", "user", "styles", "e-mail", "fonts")))
                    array_push($this->Data['Errors'], array("Error" => self::$EnterErrors[17]));
            }

            $r = db_query("SELECT us_id FROM Users WHERE LOWER(login) = LOWER('$login')");
            list($us_id) = mysql_fetch_row($r);
            if($us_id) {
//                $this->PageEnter(false, $email, $login, $passwd);
                if($this->GetProperty("LoginMail"))
                    array_push($this->Data['Errors'], array("Error" => self::$EnterErrors[12]));
                else
                    array_push($this->Data['Errors'], array("Error" => self::$EnterErrors[11]));
            } else {
                $r = db_query("SELECT us_id FROM Users WHERE LOWER(mail) = LOWER('$email')");
                list($us_id) = mysql_fetch_row($r);
                if($us_id) {
//                    $this->PageEnter(false, $email, $login, $passwd);

                    array_push($this->Data['Errors'], array("Error" => self::$EnterErrors[12]));
                } else {
                    if($_FILES['photo']['size'] > 0) {
                        $Types = array("image/gif", "image/jpeg", "image/png");
                        if(!in_array($_FILES['photo']['type'], $Types)) {
                            array_push($this->Data['Errors'], array("Error" => self::$EnterErrors[13]));
                        }
                        if($_FILES['photo']['size'] > 1000000) {
                            array_push($this->Data['Errors'], array("Error" => self::$EnterErrors[14]));
                        }
                    }

                    $Group = $this->GetProperty("Group");

                    if($Group == 2) { // менеджеры компании
                        $classCompany = _autoload("classCompany");
                        $classCompany->CheckValidReg(true);
                        $classCompany->Data['Errors'] = array_merge($classCompany->Data['Errors'], $this->Data['Errors']);
                        $this->Data = array_merge($this->Data, $classCompany->Data);
                    }

                    if(count($this->Data['Errors']) <= 0) {
                        $status = $this->GetProperty("mailReg") ? 0 : 1;

                        $r = db_query("INSERT INTO Users (login, localdir, mail, passwd, status, name, lastname, middlename, phone, bth_day, bth_month, bth_year, descr) VALUES ('$login', $localdir, '$email', '".md5($passwd)."', $status, '$name', '$lastname', '$middlename', '$phone', $bth_day, $bth_month, $bth_year, '$descr')");
                        $us_id = mysql_insert_id();

                        if($Group == 2) { // менеджеры компании
                            $classCompany->Update($us_id);
                            if($classCompany->Data['Error']) {
                                $this->Data['Errors'] = array_merge($this->Data['Errors'], $classCompany->Data['Errors']);
                                array_push($this->Data['Errors'], array("Error" => "Во время регистрации компании произошла ощибка обратитесь в службу поддержки"));
                                $Redirect = false;
                            }
                        }
                        
                        if($Group > 0)
                            db_query("INSERT INTO UserGroups SET us_id = ".$us_id.", Gro_Code = $Group");

                        if($_FILES['photo']['size'] > 0) {
                            preg_match("/\..{3}/", $_FILES['photo']['name'], $ext);
                            $photo = "/data/users/".$us_id.".".time().strtolower($ext[0]);
                            move_uploaded_file($_FILES['photo']['tmp_name'], CLIENT_PATH.$photo);
                            db_query("UPDATE Users SET photo = '$photo' WHERE us_id = ".$us_id);
                        }

                        $Fields = DB::selectArray("SELECT * FROM UserFields ORDER BY Uf_Code", "Uf_Code");
                        foreach($Fields as $Key => $Field) {
                            if($_REQUEST[$Field['Uf_Name']] != "") {
                                $Data[$Field['Uf_Name']] = mysql_real_escape_string(strip_tags($_REQUEST[$Field['Uf_Name']]));
                                $Update = 1;
                            }
                        }
                        if($Update) {
                            $dbData = serialize($Data);
                            db_query("REPLACE INTO UserData SET Data = '$dbData', us_id = ".$us_id);
                        }

                        if($this->GetProperty("mailReg")) {
                            $Subject = self::$Messages['RegSubject'];
                            $confirmLink = DOMAIN_URL.self::$MySectionPath."?udo=confirm&key=".md5($login);
                            $Message = "<br><br>Прежде чем Вы сможете авторизоваться необходимо подтвердить что Вы являетесь владельцем этого почтового ящика.<br>
                                        Для активации аккаунта перейдите по ссылке <a href=\"$confirmLink\">$confirmLink</a>";

                            $Mail = _autoload("classMail");
                            $External = array(
                                "Subject" => $Subject,
                                "Message" => $Message,
                                "mail" => $email,
                                "name" => ($name ? "$lastname $name $middlename" : $login),
                                "login" => $login,
                                "password" => $passwd,
                            );

                            $Template = $this->GetTemplate("mailRegTemplate");
                            $MessageHtml = $Mail->MailTemplate2Php($Template, $External);

                            $Mail->SendMail($email, $Subject, $MessageHtml);

                            if(count($Mail->Errors)) {
                                $this->Data['Error'] = 1;
                                foreach($Mail->Errors as $Error)
                                    array_push($this->Data['Errors'], array("Error" => $Error));
                            }
                        }

                        array_push($this->Data['Messages'], array("Message" => self::$Messages['RegOk']));
                        $this->Data['Message'] = count($this->Data['Messages']);
                        $this->Data['Success'] = true;

                        if(!$this->GetProperty("mailReg")) {
                            $this->PageEnter(false, $email, $login, $passwd);

                            if($Redirect) {
                                if($this->isJsHttpRequest) {
                                    if(!$this->AjaxData['Location'])
                                        $this->AjaxData['Location'] = self::$MySectionPath;
                                } else {
                                    header("Location: ".self::$MySectionPath);
                                }
                                die;
                            }
                        }
                    }

                }
            }
            if(count($this->Data['Errors']))
                $this->Data['Error'] = 1;
        }


        public function CheckValidEnter($login, $email, $passwd) {

            if($login || $email) {
                $Cond[] = "LOWER(mail) = LOWER('".mysql_real_escape_string($email)."')";
                if($login)
                    $Cond[] = "LOWER(login) = LOWER('".mysql_real_escape_string($login)."')";

                $Conditions = join(" OR ", $Cond);

                if($passwd) {
                    $query = "SELECT us_id, login, mail, passwd, status FROM Users WHERE ($Conditions)";
                    $result = db_query($query);

                    list($us_id, $login, $email, $db_passwd, $status) = mysql_fetch_row($result);

                    if($us_id) {
                        if (md5($passwd) == $db_passwd) {
                            if($status > 0) {
                                $this->SetUser($us_id);
                                $this->Data['EnterOk'] = 1;
                                return false;
                            } else {
                                array_push($this->Data['Errors'], array("Error" => self::$EnterErrors[5]));
                                return "5";
                            }
                        } else {
                            $this->ForgetUser();
                            array_push($this->Data['Errors'], array("Error" => self::$EnterErrors[1]));
                            return "1";
                        }
                    } else {
                        $this->ForgetUser();
                        array_push($this->Data['Errors'], array("Error" => self::$EnterErrors[2]));
                        return "2";
                    }
                } else {
                    $this->ForgetUser();
                    array_push($this->Data['Errors'], array("Error" => self::$EnterErrors[3]));
                    return "3";
                }
            } else {
                $this->ForgetUser();
                array_push($this->Data['Errors'], array("Error" => self::$EnterErrors[4]));
                return "4";
            }
        }


        function RegisterUserAction() {
            $Section = 0;
            $curtime = time();

            if($this->Data['us_id']) {

                $Data = array();
                if($curtime - $this->Data['lasttime'] < 3600) {
                    $this->Data['prevlasttime'] = $this->Data['prevlasttime'] + ($curtime - $this->Data['lasttime']);
                    if(!$this->Data['country'])
                        $Data = classGeoIP::GetData();
                } else {
                    $this->Data['prevlasttime'] = $this->Data['lasttime'];
                    $Data = classGeoIP::GetData();
                }

                if(!$this->Data['lasttime']) {
                    $this->Data['prevlasttime'] = $curtime;
                    $Data = classGeoIP::GetData();
                }
                $this->Data['lasttime'] = $curtime;

                $query = "REPLACE INTO UserTimeLog SET us_id = ".$this->Data['us_id'].", Sec_Code = ".$Section.", lasttime = $curtime, prevlasttime = ".$this->Data['prevlasttime'];

                if($Data['ip'])
                    $query .= ", ip = '".$Data['ip']."'";

                if($Data['country'])
                    $query .= ", country = '".$Data['country']."'";

                if($Data['city'])
                    $query .= ", city = '".$Data['city']."'";

                db_query("$query");

                $this->Data = array_merge($this->Data, $Data);

            } else {
                if($curtime - $_SESSION['lasttime'] < 3600) {
                    $_SESSION['prevlasttime'] = $_SESSION['prevlasttime'] + ($curtime - $_SESSION['lasttime']);
                    $_SESSION['lasttime'] = $curtime;
                } elseif($_SESSION['lasttime'] > 0) {
                    $_SESSION['prevlasttime'] = $_SESSION['lasttime'];
                    $_SESSION['lasttime'] = $curtime;
                } else {
                    $_SESSION['prevlasttime'] = $curtime - 36000;
                    $_SESSION['lasttime'] = $curtime;
                }
                $_SESSION = array_merge($_SESSION, classGeoIP::GetData());
                $this->Data = array_merge($this->Data, $_SESSION);
            }

        }


        function ForgetUser() {
            DB::Query("UPDATE Users SET session = '' WHERE session = '".$this->SessId."'");
            unset($_SESSION['Cart']['User']);
        }


        function SetUser($us_id = 0) {
            if($us_id) {
                DB::Query("UPDATE Users SET session = '".$this->SessId."' WHERE us_id = $us_id");
                $this->GetInfo();
                classSecurity::$Permissions = false;
                classSecurity::GetCan();
            }
        }


        public static function GetUserInfo($us_id = 0) {
            $User = array();
            if($us_id) {
                $User = DB::selectArray("SELECT u.*, DATE_FORMAT(u.regtime, '%d.%m.%Y %H:%i') AS regdate, tl.lasttime, tl.prevlasttime, tl.ip, tl.country, tl.city
                                           FROM Users AS u
                                           LEFT JOIN UserTimeLog AS tl
                                            ON tl.us_id = u.us_id
                                          WHERE u.us_id = $us_id");

                if($User['us_id']) {
                    $User['avatar'] = ($User['photo'] != "") ? "/imagepreview/avatar".$User['photo'] : "/img/noavatar.png";
                    $User['page']   = ($User['localdir'] != "") ? "/".$User['localdir']."/" : self::$MySectionPath.$User['us_id']."/";
                    $User['Groups'] = DB::selectArray("SELECT ug.Gro_Code, g.Gro_Name
                                                         FROM UserGroups AS ug
                                                        LEFT JOIN Groups AS g
                                                         ON ug.Gro_Code = g.Gro_Code
                                                        WHERE ug.us_id = ".$User['us_id'], "Gro_Code");

                    foreach($User['Groups'] as $User['Group'] => $Group) { $User['GroupName'] = $Group['Gro_Name']; };

                    $raz = time() - $User['lasttime'];
                    if($raz > 259200)
                        $User['ltt'] = "более 3-х дней назад";
                    elseif($raz < 259200)
                        $User['ltt'] = "3 дня назад";
                    if($raz < 172800)
                        $User['ltt'] = "2 дня назад";
                    if($raz < 86400)
                        $User['ltt'] = "вчера";
                    if($raz < 10800)
                        $User['ltt'] = "3 часа назад";
                    if($raz < 3600)
                        $User['ltt'] = "1 час назад";
                    if($raz < 1800)
                        $User['ltt'] = "30 минут назад";
                    if($raz < 600)
                        $User['ltt'] = "менее 10 минут назад";
                    if($raz < 300)
                        $User['ltt'] = "менее 5 минут назад";
                    if($raz < 180)
                        $User['ltt'] = "сейчас на сайте";

                    $User['Company'] = classCompany::GetUserCompany($User['us_id']);

                    $Fields = DB::selectArray("SELECT * FROM UserFields WHERE Uf_Primary IS NOT NULL ORDER BY Uf_Code", "Uf_Code");
                    if(count($Fields))
                        $User = array_merge($User, self::GetUserFields($User, $Fields));
                }
            }

            return $User;
        }


        public function GetTotalInfo($User = array()) {

            $User = array_merge($User, $this->GetUserFields($User));

            $Forum = _autoload("classForum");

            if(class_exists("classForum")) {
                $Themes = array();
                $Topics = DB::selectArray("SELECT ft.*, UNIX_TIMESTAMP(ft.Top_Time) AS Top_UnixTime, fth.Theme_Subject, fth.Sec_Code, UNIX_TIMESTAMP(fth.Theme_Time) AS Theme_UnixTime
                                           FROM forumTopics AS ft
                                          LEFT JOIN forumThemes AS fth
                                           ON ft.Theme_Code = fth.Theme_Code
                                            AND fth.Theme_Status = 1
                                          WHERE ft.us_id = ".$User['us_id']."
                                           AND ft.Top_Status = 1
                                          ORDER BY ft.Top_Time DESC
                                          LIMIT 20", "Top_Code");

                foreach($Topics as $Top_Code => $Topic) {
                    if(!is_array($Themes[$Topic['Theme_Code']]['Topics'])) {
                        $Themes[$Topic['Theme_Code']]['Forum_Path'] = $Forum->GetPath($Topic['Sec_Code']);
                        $Themes[$Topic['Theme_Code']]['Forum_Name'] = $Forum->ForumName;
                        $Themes[$Topic['Theme_Code']]['Theme_Subject'] = $Topic['Theme_Subject'];
                        $Themes[$Topic['Theme_Code']]['Theme_Path'] = $Themes[$Topic['Theme_Code']]['Forum_Path']."?".classForum::$ThemeID."=".$Topic['Theme_Code'];
                    }
                    $Topic['Top_Path'] = $Themes[$Topic['Theme_Code']]['Forum_Path']."?".classForum::$ThemeID."=".$Topic['Theme_Code']."#".$Top_Code;
                    $Themes[$Topic['Theme_Code']]['Topics'][$Top_Code] = $Forum->getTopicData($Topic);
                }

                $User['Themes'] = $Themes;
            }


            $classComment = _autoload("classComment");

            if(class_exists("classComment")) {
                $UserComments = DB::selectArray("SELECT *, UNIX_TIMESTAMP(UC_Time) AS UnixTime
                                                 FROM Comments
                                                WHERE us_id = ".$User['us_id']."
                                                 AND UC_Status = 1
                                                ORDER BY UC_Time DESC
                                                LIMIT 20", "UC_Code");

                foreach($UserComments as $UC_Code => $Comment) {
                    $Comment['Date'] = $classComment->mkToday($Comment['UnixTime']);

                    $Comments[$Comment['ClassItem']]['Topics'][$UC_Code] = $Comment;
                }

                $User['Comments'] = $Comments;
            }

            return $User;
        }


        public static function GetUserFields($User, $Fields = array()) {
            $dbData = DB::selectArray("SELECT * FROM UserData WHERE us_id = ".$User['us_id']);
            $Data = json_decode($dbData['Data'], true);

            if(!count($Fields))
                $Fields = DB::selectArray("SELECT * FROM UserFields ORDER BY Uf_Code", "Uf_Code");

            foreach($Fields as $Key => $Field) {
                if($Data[$Field['Uf_Name']] != "") {
                    $Fields[$Key]['Uf_Value'] = $Data[$Field['Uf_Name']];
                    $User[$Field['Uf_Name']] = $Data[$Field['Uf_Name']];
                }
            }

            $User['Fields'] = $Fields;

            return $User;
        }


        public function classUsers($sec = "", $Parent = "") {

            if(PAGE_LANGUAGE)
                include_once("lang/".PAGE_LANGUAGE.".php");

            parent::classProperties($sec, $Parent);
            $this->SessId = preg_replace("/\W+$/", "", session_id());

            $this->Data['Errors'] = array();
            $this->Data['Messages'] = array();
            $this->Data['DoID'] = self::$DoID;
            $this->Data['MySectionPath'] = self::$MySectionPath;

        }


        /* Ajax Tools */

        public function UpdateData() {
            if(!$this->Data['us_id'])
                $this->GetInfo();

            if($this->Data['us_id']) {
                $Fields = DB::selectArray("SELECT * FROM UserFields ORDER BY Uf_Code", "Uf_Code");
                $this->Data['Fields'] = $Fields;

                $dbData = DB::selectArray("SELECT * FROM UserData WHERE us_id = ".$this->Data['us_id']);
                $Data = json_decode($dbData['Data'], true);

                $Fields = DB::selectArray("SELECT * FROM UserFields ORDER BY Uf_Code", "Uf_Code");
                foreach($Fields as $Key => $Field) {
                    if($_REQUEST[$Field['Uf_Name']] != "") {
                        $Data[$Field['Uf_Name']] = mysql_real_escape_string(strip_tags($_REQUEST[$Field['Uf_Name']]));
                        $Update = 1;
                    }
                }
                if($Update) {
                    $dbData = json_encode($Data);
                    db_query("REPLACE INTO UserData SET Data = '$dbData', us_id = ".$this->Data['us_id']);
                }
                if($_REQUEST['name'])
                    db_query("UPDATE Users SET name = '".mysql_real_escape_string(strip_tags($_REQUEST['name']))."' WHERE us_id = ".$this->Data['us_id']);
                if($_REQUEST['phone'])
                    db_query("UPDATE Users SET phone = '".mysql_real_escape_string(strip_tags($_REQUEST['phone']))."' WHERE us_id = ".$this->Data['us_id']);
            }

        }


        public function CheckLogin() {
            if(!self::$User)
                $this->GetInfo();

            $login = mysql_real_escape_string($_REQUEST['v']);
            if($login != self::$User['login']) {

                $r = db_query("SELECT us_id, localdir FROM Users WHERE login = '$login'");
                list($dblogin, $localdir) = mysql_fetch_row($r);
                if($dblogin) {
                    $this->AjaxData['Error'] = self::$EnterErrors[11];
                } else {
                    list($Sec_Code) = mysql_fetch_row(db_query("SELECT Sec_Code FROM Sections WHERE Sec_LocalDir = '$login'"));
                    if($Sec_Code || in_array($localdir, array("admin", "setup", "data", "img", "jas", "css", "imagepreview", "js", "_cron", "ie6", "user", "styles", "e-mail")))
                        $this->AjaxData['Error'] = self::$EnterErrors[17];
                    else
                        $this->AjaxData['Ok'] = 1;
                }
            }
        }


        public function DeletePhoto() {
            if(!self::$User)
                $this->GetInfo();
            $r = db_query("SELECT photo FROM Users WHERE us_id = ".$this->Data['us_id']);
            list($photo) = mysql_fetch_row($r);
            unlink(CLIENT_PATH.$photo);
            db_query("UPDATE Users SET photo = '' WHERE us_id = ".$this->Data['us_id']);
            $ACF = _autoload("classActualFile");
            $ACF->Expire($photo);
        }


        public function SearchByValue($Str = "") {
            $Str = mysql_real_escape_string(strtolower($Str ? $Str : trim($_REQUEST['value'])));

            $this->AjaxData['Results'] = DB::selectArray("SELECT u.login AS Name, u.us_id AS Value FROM Users AS u WHERE LOWER(u.mail) LIKE LOWER('$Str%') LIMIT 0, 10", "us_id");

            if(count($this->AjaxData['Results']))
                $this->AjaxData['Ok'] = 1;
            else
                $this->AjaxData['Error'] = "Пользователей не найдено";
        }


    }


<?php

    class classBanner extends classProperties {

        public $ClickId = "%Code%";
        public $_cnt = 0;

        var $Properties = array(
            "public" => array(
                "Names" => array(
                ),
                "Types" => array(
                ),
                "Labels" => array(
                )
            ),
            "private" => array(
                "Names" => array(
                    "Stat",
                    "PlaceId",
                    "Cnt",
                    "Shuffle",
                    "Stopper",
                ),
                "Types" => array(
                    "InputYesNo",
                    "bannerPlacesDBSelect",
                    "T5",
                    "InputYesNo",
                    "BannerBlocksInputSelect",
                ),
                "Labels" => array(
                    "Статистика",
                    "Рекламное место",
                    "Кол-во баннеров",
                    "Случайный порядок?",
                    "Заглушка",
                )
            )
        );

        var $DefaultTemplates = array();

        function GetClassName() {
            return __CLASS__;
        }

        function Action() {
            if(!$this->GetProperty("Stat")) {

                $PlaceId = $this->PlaceId;

                $Banners = DBMem::selectArray("SELECT bh.* FROM bannerHints AS bh LEFT JOIN bannerAccaunts AS ba ON ba.Acc_Code = bh.Acc_Code AND ba.Acc_Active = 1 WHERE bh.Pla_Code = $PlaceId AND bh.Hnt_Active = 1", "Hnt_Code", "banner.place.$PlaceId");
                $Banners = sortArray($Banners, array("Hnt_Order", "DESC", "Hnt_Code", "DESC"));

                /* берем данные всех найденых баннеров - не хорошо */
                foreach($Banners as $Code => $Banner) {
                    $Cache_Id = $Banner['Hnt_Code'].".banner";
                    $CacheFile = CACHE_PATH."/".$Cache_Id;

                    if(file_exists($CacheFile) && filesize($CacheFile)) {
                        $BannerVars = $this->LoadData($CacheFile);
                    } else {
                        $BannerVars['Params'] = $this->GetParams($Banner['Hnt_Code']);
                        $BannerVars['Code'] = $Banner['Hnt_Code'];
                        $this->SaveData($CacheFile, $BannerVars);
                    }
                    $Banner = array_merge($Banner, $BannerVars);

                    $Banners[$Code] = $Banner;

                    if($Banner['Hnt_GeoTarget']) {
                        $Targets = explode(";", $Banner['Hnt_GeoTarget']);
                        foreach($Targets as $key => $sTarget) {
                            $flag = true;
                            list($Target['country'], $Target['city']) = explode(":", $sTarget);

                            $Countries = explode("|", $Target['country']);
                            foreach($Countries as $key => $Country) {
                                $Not = preg_replace("/(\w+)/", "", $Country);
                                $Country = substr($Country, 2);
                                if($Not == "!=")
                                    $flag = (classUsers::$User['country'] != $Country && $flag);
                                else
                                    $flag = (classUsers::$User['country'] == $Country && $flag);
                            }

                            $Cityes = explode("|", $Target['city']);
                            foreach($Cityes as $key => $City) {
                                $Not = preg_replace("/(\w+)/", "", $City);
                                $City = substr($City, 2);
                                if($Not == "!=")
                                    $flag = (classUsers::$User['city'] != $City && $flag);
                                else
                                    $flag = (classUsers::$User['city'] == $City && $flag);
                            }
                            if($flag)
                                break;
                        }
                        if(!$flag)
                            unset($Banners[$Code]);
                    }
                }

                if($this->GetProperty("Shuffle"))
                    shuffle($Banners);

                /* должно быть $Banners[1] */
                foreach($Banners as $Code => $Banner)
                    $sBanners[] = $Banner;

                $PlaceCount = $this->GetProperty("Cnt");
                $PlaceCount = ($PlaceCount == 0) ? count($Banners) : $PlaceCount;
                $Cnt = 0;
                while($Cnt < $PlaceCount) {
                    if(is_array($sBanners[$Cnt])) {
                        $Lucky[$Cnt] = $sBanners[$Cnt];

                        $Lucky[$Cnt]['Template'] = $this->GetTmp($Lucky[$Cnt]['Tmp_Code']);
                        $this->Data = $Lucky[$Cnt]['Params'];

                        if($Lucky[$Cnt]['Template'] != "") {
                            if($Lucky[$Cnt]['Hnt_Stat'])
                                db_query("INSERT INTO bannerHits SET Hnt_Code = ".$Lucky[$Cnt]['Hnt_Code']);
                            $Lucky[$Cnt]['Template'] = str_replace($this->ClickId, $Lucky[$Cnt]['Hnt_Code'], $Lucky[$Cnt]['Template']);
                            $this->Data['_cnt'] = $this->_cnt++;
                            $this->Template2Php($Lucky[$Cnt]['Template'], $External = array(), $Eval = 1);
                        }

                    }

                    if($this->StopperId && !$this->_cnt) {
                        $this->PlaceId = $this->StopperId;
                        $this->StopperId = false;
                        $this->Action();
                    }

                    $Cnt++;
                }
            } else {
                $Stat = new classBannerStat();
                $Stat->Action();
            }
        }

        function GetParams($Hnt_Code=0) {
            $Params = array();
            $HPa = DB::selectArray("SELECT HPa_Code, HPa_Data, HPa_Value FROM bannerHintParams WHERE Hnt_Code = $Hnt_Code", "HPa_Code");
            foreach($HPa as $Key => $Value)
                $Params[$Value['HPa_Data']] = $Value['HPa_Value'];
            return $Params;
        }

        function GetTmp($Tmp_Code=0) {
            return DB::selectValue("SELECT Tmp_Content FROM bannerTemplates WHERE Tmp_Code = $Tmp_Code");
        }

        function SaveData($CacheFile,$Data=array()) {
            $ft=fopen($CacheFile,"w");
            fputs($ft, serialize($Data));
            fclose($ft);
        }

        function LoadData($CacheFile) {
            $Data = array();
            $Data = unserialize(file_get_contents($CacheFile));
            return $Data;
        }

        function classBanner($sec = "", $Parent = "") {
            parent::classProperties($sec, $Parent);

            $this->PlaceId = $this->GetProperty("PlaceId");
            $this->Stopper = $this->GetBlock("Stopper");
            $this->StopperId = $this->Stopper->PlaceId;

        }
    }

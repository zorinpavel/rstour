<?php

  define("BANNERS_STAT_PATH", ADMIN_URL."/banners/stat");

  class classBannerStat extends classBanner {

    function GetClassName() {
      return __CLASS__;
    }

    function Action() {
      list($this->Acc_Code, $this->Acc_Name) = mysql_fetch_row(db_query("SELECT Acc_Code, Acc_Name FROM bannerAccaunts WHERE us_id = ".$this->User['us_id']));
      if($this->Acc_Code) {
        $Banners = DB::selectArray("SELECT * FROM bannerHints WHERE Acc_Code = ".$this->Acc_Code." ORDER BY Hnt_Active DESC, Hnt_Code DESC", "Hnt_Code");
        echo "<h1>".$this->Acc_Name."</h1>";
        foreach($Banners as $Key => $Banner) {
          $Stat = DB::selectArray("SELECT * FROM bannerStat WHERE Hnt_Code = ".$Banner['Hnt_Code']." ORDER BY St_Time");

          echo "<div style=\"margin:30px 0px;\">";
          echo "<b>".$Banner['Hnt_Name']."</b><br>";
          $this->dPrintStat($Banner, $Stat);
          echo "</div>";

        }
      } else {
        echo "Ваш аккаунт не привязан ни к одному баннеру.";
      }
    }

    function dPrintStat($Banner, $Values) {
      ?>
      <script type="text/javascript" src="<?=BANNERS_STAT_PATH?>/swfobject.js"></script>
            <div id="flashcontent_<?php echo $Banner['Hnt_Code']; ?>">
                    <strong>You need to upgrade your Flash Player</strong>
            </div>

            <script type="text/javascript">
                    // <![CDATA[            
                    var so_<?php echo $Banner['Hnt_Code']; ?> = new SWFObject("<?=BANNERS_STAT_PATH?>/amline.swf", "amline", "700", "400", "8", "#FFFFFF");
                    so_<?php echo $Banner['Hnt_Code']; ?>.addVariable("path", "<?=BANNERS_STAT_PATH?>/");
                    so_<?php echo $Banner['Hnt_Code']; ?>.addVariable("settings_file", encodeURIComponent("<?=BANNERS_STAT_PATH?>/settings.xml"));
  <?

      $Hnt_Show = 0;
      $Hnt_Click = 0;
      $Hnt_Ctr = 0;
      if(is_array($Values)) {
        ?>                  so_<?php echo $Banner['Hnt_Code']; ?>.addVariable("chart_data", encodeURIComponent("<chart><?

        echo "<series>";
        foreach($Values as $Key => $Value) {
          echo "<value xid='$Key'>".date("d.m.y", strtotime($Value['St_Time']))."</value>";
        }
        echo "</series>";

        echo "<graphs>";

          echo "<graph gid='0'>";
          reset($Values);
          foreach($Values as $Key => $Value) {
            echo "<value xid='$Key'>".$Value['Hnt_Show']."</value>";
            $Hnt_Show = $Hnt_Show + $Value['Hnt_Show'];
          }
          echo "</graph>\" + \n";

          echo "\"<graph gid='1'>";
          reset($Values);
          foreach($Values as $Key => $Value) {
            echo "<value xid='$Key'>".$Value['Hnt_Click']."</value>";
            $Hnt_Click = $Hnt_Click + $Value['Hnt_Click'];
          }
          echo "</graph>\" + \n";

          echo "\"<graph gid='2'>";
          reset($Values);
          foreach($Values as $Key => $Value) {
            echo "<value xid='$Key'>";
            $Hnt_DCtr = ($Value['Hnt_Click'] == 0) ? 0 : round($Value['Hnt_Click']/$Value['Hnt_Show']*100, 2);
            echo $Hnt_DCtr;
            echo "</value>";
            $Hnt_Ctr = $Hnt_Ctr + $Hnt_DCtr;
          }
          echo "</graph>";

        echo "</graphs>";

        ?>                  </chart>"));<?
      } else {
        ?>                  so_<?php echo $Banner['Hnt_Code']; ?>.addVariable("chart_data", encodeURIComponent("<chart></chart>"));<?
      }

      ?>
                    so_<?php echo $Banner['Hnt_Code']; ?>.write("flashcontent_<?php echo $Banner['Hnt_Code']; ?>");
                  // ]]>
            </script>
        <?
          $Count = count($Values);
          if($Count > 0) {
        ?>
            Средние значения: <b><? echo round($Hnt_Show/$Count, 2); ?> &nbsp; &nbsp; <? echo round($Hnt_Click/$Count, 2); ?>  &nbsp; &nbsp; <? echo round($Hnt_Ctr/$Count, 2); ?></b>
        <?
            $Month = array_slice($Values, 0, 30);
            foreach($Month as $Key => $Value) {
              $Hnt_mShow = $Hnt_mShow + $Value['Hnt_Show'];
              $Hnt_mClick = $Hnt_mClick + $Value['Hnt_Click'];
              $Hnt_mCtr = $Hnt_mCtr + ($Value['Hnt_Click'] == 0) ? 0 : round($Value['Hnt_Click']/$Value['Hnt_Show']*100, 2);
            }
            $mCount = count($Month);

            echo "<br>".str_repeat("&nbsp;", 14);
            ?>За месяц (30 дн.): <b><? echo round($Hnt_mShow/$mCount, 2); ?> &nbsp; &nbsp; <? echo round($Hnt_mClick/$mCount, 2); ?>  &nbsp; &nbsp; <? echo round($Hnt_mCtr/$mCount, 2); ?></b><?
          }
        ?>
      <?
    }

    function classBannerStat($sec = "", $Parent = "") {
      parent::classBanner($sec, $Parent);
      $this->User = classUsers::$User;
    }

  }

<?php

    class classComments extends classSecurity {

        static $DoID = "cdo";
        static $CommentID = "comment";
        static $ParentID = "parent";
        static $ClassNameID = "ccn";
        static $ItemID = "id";

        public $ClassName, $ItemId, $ClassItem;

        var $Properties = array(
            "public" => array(
                "Names" => array(
                ),
                "Types" => array(
                ),
                "Labels" => array(
                )
            ),
            "private" => array(
                "Names" => array(
                    "Export",
                    "Limit" => "Limit",
                    "template_block_top",
                    "Separator",
                    "template_top",
                    "template_elem",
                    "template_bottom",
                    "Separator",
                    "template_block_bottom",
                    "Separator",
                    "template_form",
                    "template_empty",
                ),
                "Types" => array(
                    "InputYesNo",
                    "Limit" => "T5",
                    "InputTemplates",
                    "Separator",
                    "InputTemplates",
                    "InputTemplates",
                    "InputTemplates",
                    "Separator",
                    "InputTemplates",
                    "Separator",
                    "InputTemplates",
                    "InputTemplates",
                ),
                "Labels" => array(
                    "Экспорт?",
                    "Limit" => "Ограничение вывода",
                    "Верх блока",
                    "",
                    "Верх ветки",
                    "Комментарий",
                    "Низ ветки",
                    "",
                    "Низ блока",
                    "",
                    "Добавить / редактировать",
                    "Комментариев не найдено",
                )
            )
        );

        var $DefaultTemplates = array(
            "template_form" => array(0 => ""),
            "template_top" => array(0 => ""),
            "template_elem" => array(0 => ""),
            "template_bottom" => array(0 => ""),
            "template_block_top" => array(0 => ""),
            "template_block_bottom" => array(0 => ""),
            "template_empty" => array(0 => ""),
        );

        function GetClassName() {
            return __CLASS__;
        }

        function Action() {

            $this->ClassName = $this->Data['ClassName'] = $this->ClassName ?: $_REQUEST[self::$ClassNameID];
            $this->ItemId = $this->Data['ItemId'] = $this->ItemId ?: (int)$_REQUEST[self::$ItemID];
            $this->ClassItem = $this->ClassName.".".$this->ItemId;

            if(!$this->Export) {
                switch($this->DoId) {
                    case 'edit' :
                        $this->Edit();
                        break;
                    case 'update' :
                        $this->Update();
                        break;
                    case 'hide' :
                    case 'show' :
                        $this->HideShow();
                        break;
                    case 'delete' :
                        $this->Delete();
                        break;
                    case "events":
                        $this->ShowEvents();
                        break;
                    default:
                        break;
                }
            }

            if(!$this->isJsHttpRequest && !$this->Export) {
                $this->Ins2Php("template_block_top");

                $this->ShowTree();
                $this->dPrintForm();

                $this->Ins2Php("template_block_bottom");
            }

            if($this->Export)
                $this->ShowExport();

            if(debug()) {
                ?><pre><? print_r($this->Data); ?></pre><?
            }

        }


        public function ShowExport() {
            $Limit = $this->GetProperty("Limit") ?: 50;

            $Comments = DB::selectArray("SELECT com.*, UNIX_TIMESTAMP(UC_Time) AS UC_UnixTime, com.UC_Code AS CommentId, com.UC_Parent AS ParentId, ti.*
                                          FROM Comments AS com
                                            LEFT JOIN tripItems AS ti
                                              ON ti.T_Code = REPLACE(com.ClassItem, 'classTrip.', '')
                                          ORDER BY UC_Code ASC
                                          LIMIT $Limit", "UC_Code");

            $this->Data['Comments_Cnt'] = count($Comments);
            $classTrip = _autoload("classTrip");

            $this->Ins2Php("template_block_top");
            if($this->Data['Comments_Cnt']) {

                foreach($Comments as $Code => $Comment) {
                    $Comment['UC_Date'] = $this->mkToday($Comment['UC_UnixTime']);
                    $Comment['_owner'] = ($this->User['us_id'] && $this->User['us_id'] == $Comment['us_id']);

                    /* classTrip */
                    $Comment = array_merge($classTrip->GetItemVars($Comment, true, false), $Comment);

                    unset($Comment['events']);

                    $CommentsTree[$Comment['UC_Code']] = $Comment;
                    $CommentsTree[$Comment['UC_Code']]['Children'] = array();
                    $CommentsTree[$Comment['UC_Parent']]['Children'][$Comment['UC_Code']] = &$CommentsTree[$Comment['UC_Code']];

                    if($Comment['UC_Status'] <= 0 || $CommentsTree[$Comment['UC_Parent']]['_hidden'])
                        $CommentsTree[$Comment['UC_Code']]['_hidden'] = 1;
                    else
                        $CommentsTree[$Comment['UC_Code']]['_hidden'] = 0;

                }

                if(count($CommentsTree[0]['Children'])) {
                    $this->Ins2Php("template_top");
                    $this->OldData = $this->Data;
                    $this->dPrintTree($CommentsTree[0]['Children'], 1);
                    $this->Data = $this->OldData;
                    $this->Ins2Php("template_bottom");
                } else
                    $this->Ins2Php("template_empty");

            }
            $this->Ins2Php("template_block_bottom");

        }


        function HideShow() {
            if($this->CanEdit) {
                if($this->DoId == "hide") {
                    db_query("UPDATE Comments SET UC_Status = 0, events = CONCAT(events, '".$this->Events("HIDE")."') WHERE UC_Code = ".$this->CommentId);
                } elseif($this->DoId == "show") {
                    db_query("UPDATE Comments SET UC_Status = 1, events = CONCAT(events, '".$this->Events("SHOW")."') WHERE UC_Code = ".$this->CommentId);
                }

                $this->ReRating();

                if($this->isJsHttpRequest)
                    $this->AjaxData['Location'] = "#".self::$CommentID.$this->CommentId;
                else
                    header("Location: #".self::$CommentID.$this->CommentId);
            } else {
                array_push($this->Data['Errors'], array("Error" => "Вы не можете редактировать комментарии"));
                $this->Data['Error'] = 1;
            }
        }


        function Edit($Try = false) {

            $Bans = $this->GetBans($this->GetClassName());
            foreach($Bans as $Key => $Value) {
                array_push($this->Data["Errors"], array( "Error" => $Value));
            }

            if($this->CommentId) {
                $Data = DB::selectArray("SELECT * FROM Comments WHERE UC_Code = ".$this->CommentId);
                $this->Data = array_merge($this->Data, $Data);
                $this->Data['UC_Text'] = $this->UnCheck($Data['UC_Text']);
            }

            if($Try) {

                if($this->CommentId) {
                    if($_SESSION[$this->GetClassName()] && $_SESSION[$this->GetClassName()] > time() - 30)
                        array_push($this->Data['Errors'], array("Error" => "Следующую запись вы сможете добавить не раньше чем через 30 сек."));
                    if(!$this->CanEdit && $this->User['us_id'] != $this->Data['us_id'])
                        array_push($this->Data['Errors'], array("Error" => "Вы не можете редактировать этот комментарий"));
                }
                if(!$this->CanWrite)
                    array_push($this->Data['Errors'], array("Error" => "Вы не можете писать коментарии"));

                $this->Data['UC_Status'] = (int)$_REQUEST['UC_Status'];
                $this->Data['UC_Rate'] = (int)$_REQUEST['UC_Rate'];

                $this->Data['login'] = mysql_real_escape_string($_REQUEST['login']);
                if(!$this->Data['login'])
                    array_push($this->Data['Errors'], array("Error" => "Как вас зовут?"));

                $this->Data['email'] = mysql_real_escape_string($_REQUEST['email']);
                if(!$this->Data['email'])
                    array_push($this->Data['Errors'], array("Error" => "Укажите e-mail чтобы мы могли с вами связаться"));

                $this->Data['UC_Text'] = $this->Check($_REQUEST['UC_Text']);
                if(!$this->Data['UC_Text'])
                    array_push($this->Data['Errors'], array("Error" => "Вы забыли написать комментарий"));

                // Google captcha
                $link = "https://www.google.com/recaptcha/api/siteverify";
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $link);
                curl_setopt($ch, CURLOPT_HEADER, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_TIMEOUT, 1);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, "secret=6LfctR0UAAAAAFP2gwXssB3u0PyCohsdbMLfN2_U&response=".$_REQUEST['g-recaptcha-response']);
                $string = curl_exec($ch);
                $data['curl_errno'] = curl_errno($ch);
                if(!curl_errno($ch) && !strpos($string, "503"))
                    $data = json_decode($string, true);
                curl_close($ch);

                if(!$data['success'])
                    array_push($this->Data["Errors"], array("Error" => "Мы подозреваем что вы робот, попробуйте еще раз"));

                $isComment = DB::selectValue("SELECT UC_Code FROM Comments WHERE ClassItem = '".$this->ClassItem."' AND UC_Text = '".$this->Data['UC_Text']."'");
                if($isComment)
                    array_push($this->Data['Errors'], array("Error" => "Вы уже писали этот комментарий"));

            }

            $this->Data['Error'] = count($this->Data['Errors']);
            if($Try && !$this->Data['Error'])
                return true;

            $this->dPrintForm();
            $this->AjaxData = $this->Data;

        }


        function Update() {
            if($this->Edit(true)) {

                if($this->CommentId) {
                    $Query = "UPDATE Comments SET";
                    $Where = "WHERE UC_Code = ".$this->CommentId;
                    $Action = "EDIT";
                } else {
                    $Query = "INSERT INTO Comments SET";
                    $Action = "NEW";

                    $Cond[] = "ClassItem = '".$this->ClassItem."'";
                    $Cond[] = "UC_Parent = ".$this->ParentId;
                    $Cond[] = "us_id = ".($this->User['us_id'] ?: "NULL");
                    $Cond[] = "login = '".$this->Data['login']."'";
                    $Cond[] = "email = '".$this->Data['email']."'";
//                    if($this->User['avatar'])
//                        $Cond[] = "avatar = '".$this->User['avatar']."'";
                    $Cond[] = "UC_Time = NOW()";

                    $_SESSION[$this->GetClassName()] = time();

                }

                $Cond[] = "UC_Rate = ".$this->Data['UC_Rate'];
                $Cond[] = "UC_Text = '".mysql_real_escape_string($this->Data['UC_Text'])."'";
                $Cond[] = "events = CONCAT(events, '".$this->Events($Action)."')";
                $Condition = join(", ", $Cond);

                $query = "$Query $Condition $Where";
                DB::Query($query);
                $this->CommentId = $this->Data['CommentId'] = $this->CommentId ?: mysql_insert_id();

                $this->Data = array_merge($this->User, $this->Data);

                $this->Data['UC_Date'] = classProperties::mkToday(time());
                $this->Data['ParentId'] = $this->ParentId;
                $this->Data['_owner'] = ($this->User['us_id'] && $this->User['us_id'] == $this->Data['us_id']);

                $this->Ins2Php("template_elem");

                $this->AjaxData = $this->Data;

                $this->ReRating();
            } else {
                return;
            }
        }


        public function Delete() {
            if($this->CanDelete) {

                $Comments = DB::selectArray("SELECT UC_Code FROM Comments WHERE UC_Code = ".$this->CommentId." OR UC_Parent = ".$this->CommentId, "UC_Code");
                foreach($Comments as $Code => $Comment) {
                    DB::Query("DELETE FROM Comments WHERE UC_Code = $Code");
                }

                $this->ReRating();
            } else {
                array_push($this->Data['Errors'], array("Error" => "Вы не можете удалять комментарии"));
                $this->Data['Error'] = 1;
            }
        }


        public function ShowEvents() {
            $r = db_query("SELECT events FROM Comments WHERE UC_Code = ".$this->CommentId);
            list($Events) = mysql_fetch_row($r);
            echo "<pre>$Events</pre>";
        }


        function ShowTree() {

            $Hidden = "AND com.UC_Status = 1";
            if($this->CanEdit)
                $Hidden = "";

            $Comments = DB::selectArray("SELECT com.*, UNIX_TIMESTAMP(com.UC_Time) AS UC_UnixTime, com.UC_Code AS CommentId, com.UC_Parent AS ParentId
                                     FROM Comments AS com
                                    WHERE com.ClassItem = '".$this->ClassItem."' $Hidden ORDER BY com.UC_Code ASC", "UC_Code"); //" ORDER BY com.UC_Time");
            $this->Data['Comments_Cnt'] = count($Comments);

            foreach($Comments as $Code => $Comment) {
                $Comment['UC_Date'] = classProperties::mkToday($Comment['UC_UnixTime']);
                $Comment['_owner'] = ($this->User['us_id'] && $this->User['us_id'] == $Comment['us_id']);

                $Time = $this->User['prevlasttime'] - $Comment['UC_UnixTime'];

                $Comment['_new'] = 0;
                if($Time < 172800) // 2 дня
                    $Comment['_new'] = 1;
                if($Time < 86400)  // 1 день
                    $Comment['_new'] = 2;
                if($Time < 43200)  // 12 часов
                    $Comment['_new'] = 3;
                if($Time < 10800)  // 3 часа
                    $Comment['_new'] = 4;
                if($Time < 3600)   // 1 час
                    $Comment['_new'] = 5;
                if($Time < 1800)   // 30 минут
                    $Comment['_new'] = 6;
                if($Time < 600)    // 10 минут
                    $Comment['_new'] = 7;
                if($Time < 300)    // 5 минут
                    $Comment['_new'] = 8;

                /* опасно для нагрузки */
                //        $Comment = array_merge($this->Users->GetUserInfo($Comment['us_id']), $Comment);

                $CommentsTree[$Comment['UC_Code']] = $Comment;
                $CommentsTree[$Comment['UC_Code']]['Children'] = array();
                $CommentsTree[$Comment['UC_Parent']]['Children'][$Comment['UC_Code']] = &$CommentsTree[$Comment['UC_Code']];

                if($Comment['UC_Status'] <= 0 || $CommentsTree[$Comment['UC_Parent']]['_hidden'])
                    $CommentsTree[$Comment['UC_Code']]['_hidden'] = 1;
                else
                    $CommentsTree[$Comment['UC_Code']]['_hidden'] = 0;

            }

            if(count($CommentsTree[0]['Children'])) {
                $this->Ins2Php("template_top");
                $this->OldData = $this->Data;
                $this->dPrintTree($CommentsTree[0]['Children'], 1);
                $this->Data = $this->OldData;
                $this->Ins2Php("template_bottom");
            } else
                $this->Ins2Php("template_empty");

        }


        function dPrintTree($Tree, $Level) {
            foreach($Tree as $Key => $Comment) {
                $this->Data = array_merge($this->Data, $Comment);
                $this->Data['_level'] = ($Level > 10) ? 10 : $Level;
                $this->Ins2Php("template_elem");
            }
        }


        function dPrintForm() {
            $this->Data = array_merge($this->User, $this->Data);
            $this->Ins2Php("template_form");
        }


        function UnCheck($Str) {
            $Str = preg_replace("/<a href=\"\/redirect\.php\?url=(.*?)\"(?:.*?)>(.*?)<\/a>/im", "$1", $Str);
            $Str = str_replace("<br />", "", $Str);
            return $Str;
        }


        function Check($Str) {
            /* то что хранится в базе */
            $Str = htmlspecialchars($Str, ENT_NOQUOTES, "UTF-8");
            $possible_tags = array('&', '<code>', '</code>', '<quote>', '</quote>', '<b>', '</b>', '<i>', '</i>', '<u>', '</u>', '<h4>', '</h4>', '<strike>', '</strike>', '<br>', '<small>', '</small>', '<red>', '</red>', '<green>', '</green>', '<blue>', '</blue>', '<center>', '</center>');
            foreach($possible_tags as $key => $val) {
                $Str = str_replace(htmlspecialchars($val, ENT_NOQUOTES, "UTF-8"), $val, $Str);
            }

            $Str = preg_replace("/([http|s]+:\/\/)(\S+)/im", "<a href=\"/redirect.php?url=$1$2\" target=\"_blank\">$2</a>", $Str);

            $Str = Dirty::CheckWords($Str);

            $Str = nl2br($Str);
            return $Str;
        }


        public function ReRating() {
//            $Rate = DB::selectArray("SELECT SUM(UC_Rate) AS Sum, COUNT(UC_Code) AS Cnt FROM Comments WHERE ClassItem = '".$this->ClassItem."' AND UC_Status = 1");
//            $newRate = ceil($Rate['Sum'] / $Rate['Cnt']);
//
//            DB::Query("UPDATE contentData1 SET Data = $newRate WHERE Col_Code = 8 AND Ite_Code = ".$this->ItemId);
//            DB::Query("UPDATE contentItems SET Com_Cnt = ".$Rate['Cnt']." WHERE Ite_Code = ".$this->ItemId);
//
//            @unlink(CACHE_PATH."/".$this->ItemId.".itemvars");
//            @unlink(CACHE_PATH."/".$this->ItemId.".itemvars.part");
        }


        function classComments($sec = "", $Parent = "") {
            parent::classSecurity($sec, $Parent);

            $this->Data['DoID'] = self::$DoID;
            $this->Data['DoId'] = $this->DoId = $_REQUEST[self::$DoID];

            $this->Data['CommentID'] = self::$CommentID;
            $this->Data['CommentId'] = $this->CommentId = (int)$_REQUEST[self::$CommentID];

            $this->Data['ParentID'] = self::$ParentID;
            $this->Data['ParentId'] = $this->ParentId = (int)$_REQUEST[self::$ParentID];

            $this->Data['ClassNameID'] = self::$ClassNameID;
            $this->Data['ItemID'] = self::$ItemID;

            $this->Data['Errors'] = array();

            $this->Export = $this->GetProperty("Export");
            if(!$this->Export)
                unset($this->Properties["private"]["Names"]["Limit"]);

        }

        /* Ajax Tools */

        function GetQuote() {
            $CommentId = (int)$_REQUEST['CommentId'];
            if($CommentId) {
                $r = db_query("SELECT login, UC_Text FROM Comments WHERE UC_Code = $CommentId");
                list($this->AjaxData['login'], $this->AjaxData['Text']) = mysql_fetch_row($r);
            }
        }

    }

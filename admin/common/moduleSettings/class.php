<?php

/*
  
  п╖я┌п╬п╠я▀ п©п╬п╩я┐я┤п╦я┌я▄ я┤я┌п╬-я┌п╬ п╦п╥ п╫п╟я│я┌я─п╬п╣п╨ п╫п╟п╢п╬ п╡я▀п╥п╡п╟я┌я▄
  classSettings::GetUserSettings($Param, $us_id) пЁп╢п╣ $Param п╦п╪я▐ я│п╡п╬п╧я│я┌п╡п╟

*/

  class classSettings extends classSecurity {
    
    public static $DoID = "setdo";

    var $Properties = array(
      "public" => array(
        "Names" => array(
         ),
        "Types" => array(
        ),
        "Labels" => array(
        ), 
      ),
      "private" => array(
        "Names" => array(
          "AllowedSettings", 
          "template_main",
        ),
        "Types"   => array(
          "InputArrayPV", 
          "InputTemplates", 
        ),
        "Labels" => array(
          "п■п╬я│я┌я┐п©п╫я▀п╣ п╫п╟я│я┌я─п╬п╧п╨п╦", 
          "п⌠п╩п╟п╡п╫я▀п╧ я┬п╟п╠п╩п╬п╫", 
        ), 
      ), 
    );

    
    var $DefaultTemplates = array(
        "template_main" => array(0 => ""),
        "template_ok" => array(0 => ""), 
    );


    function GetClassName() {
          return __CLASS__;
    }

    
    function Action($Do = "") {

      $Do = $Do ? $Do : $_REQUEST[self::$DoID];

      if($_REQUEST['clear'] == 'all') {
        $this->clearAll();
      }

      if($this->User['us_id'] && is_array($this->AllowedSettings)) {
        $Arr = $this->GetUserSettings("");
        foreach($this->AllowedSettings as $Key => $dValue) {
          if($Do == "update") {
            $upValue = preg_replace("/[^A-z0-9]+/i", "\${1}", $_REQUEST[$Key]);
            $Arr[$this->SesId][$Key] = $upValue;
          }
          $this->Data['Settings'][$Key]['Name'] = $Key;
          $this->Data['Settings'][$Key]['Default'] = $dValue['Value'];
          $this->Data['Settings'][$Key]['Type'] = $dValue['Type'];
          $this->Data['Settings'][$Key]['Label'] = $dValue['Label'];
          $this->Data['Settings'][$Key]['Signature'] = $dValue['Signature'];
          $this->Data['Settings'][$Key]['Value'] = isset($Arr[$this->SesId][$Key]) ? $Arr[$this->SesId][$Key] : $dValue['Value'];
          $Data[$this->SesId][$Key] = $this->Data['Settings'][$Key]['Value'];
        }

        if($Do == "update") {
          $Arr = array_merge($Arr, $Data);
          $sData = serialize($Arr);
          db_query("REPLACE INTO UserSettings SET us_id = ".$this->User['us_id'].", Data = '$sData'");
        }
      }

      $this->Ins2Php("template_main");

    }

    function clearAll() {
      db_query("DELETE FROM UserSettings WHERE us_id = ".$this->User['us_id']);
    }


    public function GetUserSettings($Param = "", $us_id = 0) {
      $us_id = $us_id ? $us_id : $this->User['us_id'];
      list($Data) = mysql_fetch_row(db_query("SELECT Data FROM UserSettings WHERE us_id = $us_id"));
      $Arr = unserialize($Data);

      if($Param) {
        if(!isset($Arr[$this->SesId][$Param]))
          $Arr[$this->SesId][$Param] = $this->AllowedSettings[$Param]['Value'];
        return $Arr[$this->SesId][$Param];
      } else {
        foreach($this->AllowedSettings as $Key => $dValue) {
          if(!isset($Arr[$this->SesId][$Key]))
            $Arr[$this->SesId][$Key] = $dValue['Value'];
        }
        return $Arr[$this->SesId];
      }
    }

    
   public function classSettings($sec = "",$Parent = "") {
      parent::classSecurity($sec, $Parent);

      $this->AllowedSettings = $this->GetProperty("AllowedSettings");
      $this->SesId = $sec;

      $this->Data['Errors'] = array();
      $this->Data['DoID'] = self::$DoID;
    }

  } 

<?php

    class InputDesignSearch {
        public static function dPrint($Name, $ClassName, $ActionName = "searchByValue", $ValueKey = "", $Value = "", $PlaceHolder = "") {
            ?>
            <div class="js SearchValue">
                <div class="js Template dno" onclick="return { name: 'SearchValueResults' };">
                    {=Name}
                </div>
                <input type="hidden" name="<? echo $Name; ?>" value="<? echo $ValueKey; ?>" class="js SearchValue_InputHidden"">
                <input type="text" class="js SearchValue_Input" value="<? echo $Value; ?>" onclick="return { c: '<? echo $ClassName; ?>', a: '<? echo $ActionName; ?>', length: 1, empty: true };" autocomplete="off" placeholder="<? echo $PlaceHolder; ?>">
                <a class="js SearchValue_Action icon-searchvalue"></a>
                <div class="js SearchValue_Label dno"></div>
            </div>
            <?
        }
    }


    class InputDesignSelect {
        public static function dPrint($Name, $Data = array(), $SelectedKey, $Default = "[выбрать]", $Clear = false, $Width = 200, $cssClass = "", $CallBack = "", $ExtraData = "") {

            $SelectedValue = false;
            ?>
            <div class="js classHttp_Form_Select<?php echo " $cssClass"; ?>" id="select_<?php echo $Name; ?>" onclick="return { Name : '<?php echo $Name; ?>'<?php echo ($CallBack ? ", CallBack: '$CallBack'" : ""); echo ($ExtraData ? ", $ExtraData" : ""); ?>  };" style="width:<?php echo ($Width ? $Width."px" : "auto"); ?>;">
                <div class="box-select">

                    <input type="hidden" value="<?php echo $SelectedKey; ?>" class="js classHttp_Form_Select_Value" name="<?php echo $Name; ?>" id="<?php echo $Name; ?>" readonly>
                    <div class="js classHttp_Form_Select_Select select-all" style="width:<?php echo ($Width ? $Width."px" : "100%"); ?>;">
                        <ul class="select-values">
                            <?php
                                if($Default && $Default != "" && !is_array($Default)) {
                                    echo "<li><a href=\"#\" class=\"js classHttp_Form_Select_Option\" onclick=\"return { Key: '', Value: '$Default' };\">$Default</a></li>\n";
                                } elseif(is_array($Default)) {
                                    foreach($Default as $DefaultKey => $DefaultValue)
                                        echo "<li><a href=\"#\" class=\"js classHttp_Form_Select_Option\" onclick=\"return { Key: '$DefaultKey', Value: '$DefaultValue' };\">$DefaultValue</a></li>\n";
                                }

                                foreach($Data as $Key => $Value) {
                                    if(is_array($Value)) {

                                        if(count($Value['Children'])) {
                                            echo "<li><a href=\"#\" class=\"js classHttp_Form_Select_Option Parent\" onclick=\"return { Key: '$Key', Value: '".$Value['Value']."', Child: '$Key' };\">".$Value['Value']."</a></li>\n";
                                        } else {
                                            echo "<li><a href=\"#\" class=\"js classHttp_Form_Select_Option".($Value['ClassName'] ? " ".$Value['ClassName'] : "")."\" onclick=\"return { Key: '$Key', Value: '".$Value['Value']."' };\">".$Value['Value']."</a></li>\n";
                                        }
                                        if($Key == $SelectedKey)
                                            $SelectedValue = $Value['Value'];

                                        if(count($Value['Children'])) {
                                            echo "<ul class=\"js classHttp_Form_Select_Child select-values values-children\" onclick=\"return { Parent: '$Key' };\" style=\"width:".($Width ? ($Width-26)."px" : "100%").";\">";
                                            foreach($Value['Children'] as $sKey => $sValue) {
                                                echo "<li><a href=\"#\" class=\"js classHttp_Form_Select_Option Child Level".$sValue['Level']."\" onclick=\"return { Key: '$sKey', Value: '".$sValue['Value']."', Parent: '$Key' };\">- ".$sValue['Value']."</a></li>\n";
                                                if($sKey == $SelectedKey)
                                                    $SelectedValue = $sValue['Value'];
                                            }
                                            echo "</ul>";
                                        }
                                    } else {
                                        echo "<li><a href=\"#\" class=\"js classHttp_Form_Select_Option\" onclick=\"return { Key: '$Key', Value: '$Value' };\">$Value</a></li>\n";
                                        if($Key == $SelectedKey)
                                            $SelectedValue = $Value;
                                    }
                                }

                            ?>
                        </ul>
                    </div>
                    <div class="js classHttp_Form_Select_Current select-arrow">
                        <div><?php
                            if($SelectedValue)
                                echo $SelectedValue;
                            elseif(is_array($Default))
                                echo $Default[key($Default)];
                            elseif($Default)
                                echo $Default;
                            else
                                echo "&nbsp;";
                        ?></div>
                    </div>

                </div>
                <?php
                    if($Clear) {
                        echo "<a title=\"Сброс\" class=\"js classHttp_Form_Select_Cancel icon select-cancel\" href=\"#\"></a>";
                    }
                ?>
            </div>
        <?

        }

    }


    class InputDesignSelect2 {
        public static function dPrint($Names, $Data = array(), $SelectedKey, $Default = "", $Clear = false, $Width = 200, $cssClass = "", $CallBack = "", $ExtraData = "", $SelectedValue = "", $Location = false) {

            $jsNames = array();
            foreach($Names as $Name)
                $jsNames[] = "'$Name'";
            ?>
            <div class="js classHttp_Form_Select<?php echo " $cssClass"; ?>" id="select_2" onclick="return { Name : [<?php echo join(", ", array_values($jsNames)); ?>]<?php echo ($CallBack ? ", CallBack: '$CallBack'" : ""); echo ($ExtraData ? ", $ExtraData" : ""); echo ($Location ? ", Location: '$Location'" : ""); ?>  };" style="width:<?php echo ($Width ? $Width."px" : "auto"); ?>;">
                <div class="box-select">
                    <?
//                        foreach($Names as $Name)
//                            echo "<input type=\"hidden\" value=\"".$SelectedKey."\" class=\"js classHttp_Form_Select_Value\" name=\"".$Name."\" readonly />";
                    ?>
                    <div class="js classHttp_Form_Select_Select select-all" style="width:<?php echo ($Width ? $Width."px" : "100%"); ?>;">
                        <ul class="select-values">
                            <?php
                                if($Default && $Default != "" && !is_array($Default)) {
                                    echo "<li class=\"js classHttp_Form_Select_Option\" onclick=\"return { Key: '', Value: '$Default' };\">$Default</li>\n";
                                } elseif(is_array($Default)) {
                                    foreach($Default as $DefaultKey => $DefaultValue)
                                        echo "<li class=\"js classHttp_Form_Select_Option\" onclick=\"return { Key: '$DefaultKey', Value: '$DefaultValue' };\">$DefaultValue</li>\n";
                                }

                                foreach($Data as $Key => $Value) {
                                    if(is_array($Value)) {

                                        if(count($Value['Children'])) {
                                            echo "<li class=\"js classHttp_Form_Select_Option Parent\" onclick=\"return { Key: '$Key', Value: '".$Value['Value']."', Child: '$Key' };\">".$Value['Value']."</li>\n";
                                        } else {
                                            echo "<li class=\"js classHttp_Form_Select_Option".($Value['ClassName'] ? " ".$Value['ClassName'] : "")."\" onclick=\"return { Key: '$Key', Value: '".$Value['Value']."' };\">".$Value['Value']."</li>\n";
                                        }
                                        if($Key == $SelectedKey)
                                            $SelectedValue = $Value['Value'];

                                        if(count($Value['Children'])) {
                                            echo "<ul class=\"js classHttp_Form_Select_Child select-values values-children\" onclick=\"return { Parent: '$Key' };\" style=\"width:".($Width ? ($Width-26)."px" : "100%").";\">";
                                            foreach($Value['Children'] as $sKey => $sValue) {
                                                echo "<li class=\"js classHttp_Form_Select_Option Child Level".$sValue['Level']."\" onclick=\"return { Key: '$sKey', Value: '".$sValue['Value']."', Parent: '$Key' };\">- ".$sValue['Value']."</li>\n";
                                                if($sKey == $SelectedKey)
                                                    $SelectedValue = $sValue['Value'];
                                            }
                                            echo "</ul>";
                                        }
                                    } else {
                                        echo "<li class=\"js classHttp_Form_Select_Option\" onclick=\"return { Key: '$Key', Value: '$Value', KeyName: '".$Names[0]."' };\">$Value</li>\n";
                                        if($Key == $SelectedKey)
                                            $SelectedValue = $Value;
                                    }
                                }

                            ?>
                        </ul>
                    </div>
                    <div class="js classHttp_Form_Select_Current select-arrow">
                        <input type="text" name="sWord" placeholder="Начните вводить название города или тура" value="<?php
                            if($SelectedValue)
                                echo $SelectedValue;
                            elseif(is_array($Default))
                                echo $Default[key($Default)];
                            elseif($Default)
                                echo $Default;
                        ?>" autocomplete="off" class="js classHttp_Form_Select_Search" requires="classHttp_Form_Input" onclick="return { maxlength: 100, empty: true, c: 'classRegionSearch', a: 'SearchByValue' };">
                    </div>
                </div>
            </div>
            <?

        }

    }

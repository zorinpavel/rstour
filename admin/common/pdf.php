<?php

    
    class classPDF extends classProperties {

        static $ClassID = "c";
        static $ModuleID = "m";
        static $SecID = "s";
        static $DoID = "d";

        var $Properties = array(
            "public" => array(
                "Names" => array(
                    "template_header",
                    "template_footer",
                    "template_access_error",
                ),
                "Types" => array(
                    "InputTemplates",
                    "InputTemplates",
                    "InputTemplates",
                ),
                "Labels" => array(
                    "Шаблон Header",
                    "Шаблон Footer",
                    "Шаблон Access Error",
                )
            ),
            "private" => array(
                "Names" => array(
                ),
                "Types" => array(
                ),
                "Labels" => array(
                )
            )
        );

        var $DefaultTemplates = array(
            "template_header" => array(0 => ""),
            "template_footer" => array(0 => ""),
            "template_access_error" => array(0 => ""),
        );

        function GetClassName() {
            return __CLASS__;
        }

        function Action() {

            if($_REQUEST['mode'])
                $Protections = explode(",", $_REQUEST['mode']);
            else
                $Protections = array("print");

            $mpdf = new mPDF("utf-8", "A4", "", "", "15", "15", "38", "18");
            $mpdf->allow_charset_conversion = true;
            $mpdf->autoLangToFont = true;
            $mpdf->CSSselectMedia = "pdf";
            $mpdf->SetProtection($Protections); // it doesn't work

            ob_start();
            $this->Ins2Php("template_header");
            $PageHeader = ob_get_clean();
            $mpdf->SetHTMLHeader($PageHeader);

            ob_start();
            $this->Ins2Php("template_footer");
            $PageFooter = ob_get_clean();
            $mpdf->SetHTMLFooter($PageFooter);

            $classPage = _autoload("classPage");

            ob_start();
            echo "
        <!DOCTYPE html>
        <html lang=\"".PAGE_LANGUAGE."\">
        <head>
          <meta charset=\"UTF-8\" />
          <meta name=\"robots\" content=\"noindex, follow\" />
          <link rel=\"author\" href=\"".DOMAIN_NAME."\" />
          <link rel=\"shortcut icon\" href=\"/favicon.ico\" />
          <link rel=\"stylesheet\" href=\"/jas/".classPage::$Css.";pdf.css\" media=\"pdf\" />
          <title>PDF ".DOMAIN_NAME."</title>
        </head>
        <body>
        <div class=\"pdf-body\">";

            $ClassID = $_REQUEST[self::$ClassID];
            $DoID = $_REQUEST[self::$DoID];
            if($ClassID != "classPDF") {
                $ModuleID = $_REQUEST[self::$ModuleID];
                $SecID = (int)$_REQUEST[self::$SecID];

//                $EvalStr = "\$Object = _autoload(\"$ClassID\", \"$ModuleID\");";
//                eval($EvalStr);
                $Object = _autoload($ClassID, $ModuleID, $SecID);

                $Object->isPdf = true;
                if($Object->Can['read'])
                    $Object->Action($DoID);
                else
                    $this->Ins2Php("template_access_error");

            }

            echo "</div></body></html>";

            $PageContent = ob_get_clean();

            $mpdf->WriteHTML($PageContent);
            $mpdf->Output(preg_replace("/\.\w+/", "", DOMAIN_NAME).".pdf", "I");

        }

        function classPDF($sec = "", $Parent = "") {
            parent::classProperties($sec, $Parent);
            include_once(ADDON_PATH."/mpdf60/mpdf.php");
        }
    }

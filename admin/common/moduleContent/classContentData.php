<?php

    class classContentData extends classSecurity {

        public $DataType, $Items, $Groups, $Sec_Code, $Sections, $Str_Code, $Structures;
        public $TimeStart, $TimeEnd;
        public $ListScroll, $ContentFilter, $ContentCart;

        public $ItemID, $Ite_Code, $DateFormat;

        var $Properties = array(
            "public" => array(
                "Names" => array(
                    "SearchID" => "SearchID",
                    "TimeStartID",
                    "TimeEndID",
                    "ColsID",
                    "SectionsID",
                    "GroupSectionsID",
                    "DateFormat" => "DateFormat",
                    "IteID" => "IteID",
                ),
                "Types" => array(
                    "SearchID" => "T50",
                    "T50",
                    "T50",
                    "T50",
                    "T50",
                    "T50",
                    "IteID" => "T50",
                    "DateFormat" => "T50",
                ),
                "Labels" => array(
                    "SearchID" => "ID поискового параметра",
                    "ID начальной даты",
                    "ID конечной даты",
                    "Префикс поля поиска",
                    "ID рубрик для поиска",
                    "ID флага группировки рубрик",
                    "DateFormat"=>"Формат даты",
                    "IteID" => "ID",
                ),
            ),
            "private" => array(
                "Names" => array(
                    "DataType",
                    "GroupChildrenSections"=>"GroupChildrenSections",
                    "Section" => "Section",
                    "Sections" => "Sections",
                    "Structure" => "Structure",
                    "Structures" => "Structures",
                    "GroupSecFirst" => "GroupSecFirst",
                    "Separator",
                    "OrderBy" => "OrderBy",
                    "NoDate",
                    "ListScroll",
                    "ContentCart"=>"ContentCart",
                    "Separator",
                    "ContentFilter"=>"ContentFilter",
                    "AndOr" => "AndOr",
                ),
                "Types" => array(
                    "ContentDataTypeInputSelect",
                    "GroupChildrenSections"=>"InputYesNo",
                    "Section" => "InputSectionsTree",
                    "Sections" => "InputManySectionsContentData",
                    "Structure" => "InputContentStructureDBSelect",
                    "Structures" => "InputContentStructureDBMultiSelect",
                    "GroupSecFirst" => "InputYesNo",
                    "",
                    "OrderBy" => "InputContentOrderBy",
                    "InputYesNo",
                    "InputScroll",
                    "ContentCart"=>"InputSectionsContentCart",
                    "",
                    "ContentFilter" => "InputYesNo",
                    "AndOr" => "InputYesNo",
                ),
                "Labels" => array(
                    "Тип данных",
                    "GroupChildrenSections"=>"Групировать подразделы?",
                    "Section" => "Раздел",
                    "Sections" => "Разделы",
                    "Structure" => "Структура контента",
                    "Structures" => "Структуры контента",
                    "GroupSecFirst" => "Структуры внутри разделов",
                    "",
                    "OrderBy" => "Сортировать по",
                    "Учитывать даты",
                    "Блок 'показывать по'",
                    "ContentCart"=>"Корзина",
                    "Фильтр",
                    "ContentFilter"=>"Фильтр",
                    "AndOr" => "Использовать логику 'И' или 'ИЛИ'",
                )
            )
        );

        function GetClassName() {
            return __CLASS__;
        }

        function GetPath($SectionID = 0) {
            $SectionID = $SectionID ?: $this->GetSectionID();
            $Dir = "";
            $PageName = "";
            while ($SectionID) {
                $Obj = new classPage($SectionID);
                $Dir = $Obj->GetProperty("page_localdir")."/$Dir";
                $PageName = $PageName ?: $Obj->GetProperty("page_name");
                $SectionID = $Obj->GetProperty("page_parent");
            }
            return array("SectionLink" => "/$Dir", "SectionName" => $PageName);
        }

        function GetStrCols($Str_Code, $Part = true) {
            $ColCond = $Part ? "Col_Part = 1 AND ": "";
            $NamePart = $Part ? 1 : 0;
            static $StrCols = array();
            if(!(int)$Str_Code)
                return array();
            if(!isset($StrCols[$NamePart][$Str_Code])) {
                $StrCols[$NamePart][$Str_Code] = DB::selectArray("SELECT Col_Code, Col_Header, Col_Id, Col_Type, Lst_Code FROM contentColumns WHERE $ColCond Str_Code = $Str_Code", "Col_Code");
            }
            return $StrCols[$NamePart][$Str_Code];
        }

        function GetLstValues($Lst_Code) {
            static $LstValues = array();
            if(!(int)$Lst_Code) return array();
            if(!isset($LstValues[$Lst_Code])) {
                $LstValues[$Lst_Code] = DB::selectArray("SELECT LVa_Code, LVa_Header, LVa_Value FROM contentListValues WHERE Lst_Code = $Lst_Code", "LVa_Code");
            }
            return $LstValues[$Lst_Code];
        }

        /*
         * Список записей для вывода
         *
         * @param int $Limit
         * @param int $ReadItemVars
         */
        function GetContentList($Limit, $ReadItemVars = true) {
            if(is_object($this->Parent)) {
                if(!$this->Parent->GetProperty("ShowScroll"))
                    $this->NoListScroll = 1;
            }

            $JoinCond  = "";
            $QueryCond = "";
            $OrderBy   = $this->GetProperty("OrderBy");

            $SecCond = "1 = 0"; //Условие по разделам сайта
            $IteCond = "";
            $StateCond = "ci.Ite_State = 0"; //Показываем только с состоянием "нормальное"
            if($this->Ite_Code) {
                if(is_integer($this->Ite_Code)) {
                    $IteCond = "ci.Ite_Code = ".$this->Ite_Code;
                    $StateCond = "";
                } else $IteCond = "ci.Ite_Code != ".(int)substr($this->Ite_Code, 1);
            }

            $TimeCond = "";
            $TimeCond1 = "";
            $TimeCond2 = "";
            if($this->TimeStart) $TimeCond1 = 'Time > "'.$this->TimeStart.'"';
            if($this->TimeEnd) $TimeCond2 = 'Time < "'.$this->TimeEnd.'"';
            if($TimeCond1 && $TimeCond2)
                $TimeCond = $TimeCond1.' AND '.$TimeCond2;
            else
                $TimeCond = (strlen($TimeCond1) ? $TimeCond1 : $TimeCond2);

            $StrGroupOrder = "";  //Условие группировки по структурам (ORDER BY Str_Code=$Str_Code1, Str_Code=$Str_Code2...
            $SecGroupOrder = "";  //Условие группировки по разделам
            $GroupOrder = ""; //Окончательное условие группировки

            if(($this->CurDay || $this->CurMonth || $this->CurYear) && !$this->Ite_Code) {
                if($this->CurDay > 0) {
                    $Date = date("Y-m-d", mktime(0, 0, 0, $this->CurMonth, $this->CurDay, $this->CurYear));
                    $PeriodCond = "Ite_BDate = '$Date'";
                } else {
                    $Date = date("Y-m-d", mktime(0, 0, 0, $this->CurMonth, 1, $this->CurYear));
                    $DateEnd = date("Y-m-d", mktime(0, 0, 0, $this->CurMonth+1, 0, $this->CurYear));
                    $PeriodCond = "Ite_BDate >= '$Date' AND Ite_BDate <= '$DateEnd'";
                }
            } elseif(!$this->Ite_Code) {
                $Date = date("Y-m-d");
                $PeriodCond = "Ite_BDate <= '$Date' AND Ite_EDate >= '$Date'";
            }

            if($this->GetProperty("NoDate")) {
                $PeriodCond = ""; //" Ite_EDate >= '$Date'";
                $TimeCond = "";
            }

            if(isset($this->Sec_Code)) {
                //если один раздел сайта
                $SecCond = "(Sec_Code=".(int)$this->Sec_Code." OR isec.Section=".(int)$this->Sec_Code.')';
            } elseif(isset($this->Sections) && is_array($this->Sections['Sections']) && count($this->Sections['Sections'])) {
                //если много разделов
                $SecCond = "(Sec_Code IN (".join(',',$this->Sections['Sections']).")"." OR isec.Section IN(".join(',',$this->Sections['Sections'])."))";
                if($this->Sections['Group'] && is_array($this->Sections['Order'])) {
                    //если нужна групировка
//                    arsort($this->Sections['Order']);
                    foreach($this->Sections['Order'] as $Sec_Code => $Order)
                        $SecGroupOrder .= "ci.Sec_Code != $Sec_Code, ";
                } elseif ($this->Sections['Group']) {
                    $SecGroupOrder .= "Sec_Code,";
                }
            } elseif(!isset($this->Sec_Code) && !isset($this->Sections)) {
                //если разделы не указаны
                $SecCond = "";
            }

            if(isset($this->Str_Code)) {
                $StrCond="Str_Code=".$this->Str_Code;
                //      if ($SecCond==' 1=0') $SecCond=""; //??? для чего это?
            } elseif(isset($this->Structures) && is_array($this->Structures['Structures']) && count($this->Structures['Structures'])) {
                $StrCond = "Str_Code IN (".join(',',$this->Structures['Structures']).")";
                if($this->Structures['Group'] && is_array($this->Structures['Order'])) {
                    //если нужна групировка
                    asort($this->Structures['Order']);
                    while(list($Str_Code)=each($this->Structures['Order'])) {
                        $StrGroupOrder .= "ci.Str_Code != $Str_Code,";
                    }
                } elseif ($this->Structures['Group']) {
                    $StrGroupOrder .= "ci.Str_Code,";
                }
            } elseif(!isset($this->Str_Code) && !isset($this->Structures)) {
                $StrCond = "";
            }


            $WordsCond=''; //Условие поиска по поисковому запросу
            if($this->DataType == 6 || $this->DataType == 7) { // Общий поиск по сайту
                $Search = $_REQUEST[$this->GetProperty('SearchID')]; // Поисковый запрос
                $Words = preg_split('/\W+/', $Search, PREG_SPLIT_NO_EMPTY);
                $WordsCond = "";
                foreach($Words as $k => $v) {
                    $sw = $this->lemmatize($v);
                    if(strlen($sw))
                        $WordsCond .= "Data LIKE '%$sw%' OR ";
                }
                $WordsCond = substr($WordsCond, 0, -4);

                $Cols = DB::selectArray("SELECT Col_Code FROM contentColumns WHERE Col_Type IN (1,2,6) AND $StrCond", "Col_Code");
                if(count($Cols)) {
                    if(strlen($WordsCond)) {
                        $ColCond = "Col_Code IN (".join(',', array_keys($Cols)).")";
                        $query = "SELECT Ite_Code FROM contentData1 WHERE $ColCond AND $WordsCond ".
                            " UNION DISTINCT ".
                            "SELECT Ite_Code FROM contentData2 WHERE $ColCond AND $WordsCond ".
                            " UNION DISTINCT ".
                            "SELECT Ite_Code FROM contentData6 WHERE $ColCond AND $WordsCond ";
                        $Ite_Codes = DB::selectArray($query, "Ite_Code");
                        if(count($Ite_Codes))
                            $IteCond = "ci.Ite_Code IN (".join(",", array_keys($Ite_Codes)).")";
                    }
                }
                $IteCond .= strlen($IteCond) ? " OR " : "";

                if($WordsCond)
                    $IteCond .= "(".str_replace("Data LIKE", "Ite_Header LIKE", $WordsCond).")";
                else
                    $IteCond = "1=0";
            }

            $TagsCond = "";
            if($this->DataType == 1 && !$this->Ite_Code && class_exists("classTags")) {
                $Tag_Code = $_REQUEST[classTags::$TagID];
                if($Tag_Code) {
                    $TagItems = DB::selectArray("SELECT ci.Ite_Code FROM TagsIndex AS ti
                                                INNER JOIN contentItems AS ci
                                                  ON ci.Ite_Code = REPLACE(ti.Item, '".CONTENT_TAGS.".', '')
                                              WHERE
                                                ti.Tag_Code = $Tag_Code", "Ite_Code");
                    $TagsCond = "ci.Ite_Code IN (".(count($TagItems) > 0 ? join(', ', array_keys($TagItems)) : 0).")";
                }
            }

            $FilterCond = "";
            if($this->GetProperty("ContentFilter") && !$this->Ite_Code) {

                $this->ContentFilter = _autoload("classContentFilter");
                if(isset($this->Str_Code)) {
                    $Strs[] = $this->Str_Code;
                } elseif(isset($this->Structures) && is_array($this->Structures['Structures']) && count($this->Structures['Structures'])) {
                    foreach($this->Structures['Structures'] as $k => $v) {
                        $Strs[] = $v;
                    }
                }
                $CurrentFilters = $this->ContentFilter->GetRequestFilters($Strs);

                $FilteredItems = array();
                $AlreadyFiltered = false;
                foreach($CurrentFilters as $FilterID => $FilterValue) {
                    $TmpItems = array();
                    if($FilterID == "price") {
                        $TmpItems = $this->GetItemsByFilter($FilterID, $FilterValue);
                    } elseif(is_array($FilterValue)) {
                        foreach($FilterValue as $fvKey => $fvValue)
                            $TmpItems = $TmpItems + $this->GetItemsByFilter($FilterID, $fvValue);
                    } else
                        $TmpItems = $this->GetItemsByFilter($FilterID, $FilterValue);

                    if(count($FilteredItems) && $this->GetProperty("AndOr")) {
                        $FilteredItems = array_intersect_assoc($FilteredItems, $TmpItems);
                    } elseif(count($FilteredItems)) {
                        $FilteredItems = array_merge($FilteredItems, $TmpItems);
                    } elseif(!$AlreadyFiltered) {
                        $FilteredItems = $TmpItems;
                        $AlreadyFiltered = true;
                    }
                }
                if(count($CurrentFilters))
                    $FilterCond = "ci.Ite_Code IN (".join(", ", array_keys($FilteredItems)).")";
            }

            if($this->DataType == 9 && $this->Ite_Code) {
                $IteCond = "";
                $SecCond = "";
                $Ite_Codes = DB::selectArray("SELECT Ite_CodeR FROM contentRelated WHERE Ite_Code = ".$this->Ite_Code, "Ite_CodeR");
                if(is_array($Ite_Codes))
                    $IteCond = "ci.Ite_Code IN (".join(',', array_keys($Ite_Codes)).")";
                else
                    $IteCond = "ci.Ite_Code = 0";
            }

            if($this->DataType == 10) {
                if(classUsers::$User['us_id']) {
                    $dbIte_Codes = DB::selectArray("SELECT Ite_Code FROM contentViews WHERE us_id = ".classUsers::$User['us_id'], "Ite_Code");
                    $Ite_Codes = array_keys($dbIte_Codes);
                    $JoinCond = "LEFT JOIN contentViews AS cv ON cv.Ite_Code = ci.Ite_Code";
                    $QueryCond = ", cv.CV_Time";
                } else
                    $OrderBy = false;

                $contentViews = json_decode($_COOKIE['contentViews'], true);
                if(is_array($Ite_Codes) && is_array($contentViews))
                    $Ite_Codes = array_merge($Ite_Codes, array_keys($contentViews));
                elseif(is_array($contentViews))
                    $Ite_Codes = array_keys($contentViews);

                if(is_array($Ite_Codes))
                    $IteCond = "ci.Ite_Code IN (".join(',', $Ite_Codes).")";
            }

            if($this->DataType == 11) {
                $contentFavorites = json_decode($_COOKIE['contentFavorites'], true);
                if(is_array($Ite_Codes) && is_array($contentFavorites))
                    $Ite_Codes = array_merge($Ite_Codes, array_keys($contentFavorites));
                elseif(is_array($contentFavorites))
                    $Ite_Codes = array_keys($contentFavorites);

                if(is_array($Ite_Codes))
                    $IteCond = "ci.Ite_Code IN (".join(',', $Ite_Codes).")";
            }

            if(DEFAULT_LANGUAGE) {
                if(defined("PAGE_LANGUAGE") && PAGE_LANGUAGE != DEFAULT_LANGUAGE && PAGE_LANGUAGE != "") {
                    $LanguageCond = "Ite_Language = '".PAGE_LANGUAGE."'";
                } elseif (PAGE_LANGUAGE == DEFAULT_LANGUAGE || !PAGE_LANGUAGE) {
                    $LanguageCond = "(Ite_Language = '".DEFAULT_LANGUAGE."' OR Ite_Language IS NULL)";
                }
            }

            $OrderCond = preg_split("/[, ]+|[ ]/", $OrderBy);
            $OrderCond = (count($OrderCond) ? $OrderCond: array("Ite_Priority", "DESC"));

            $Limit = (int)$Limit;
            if($Limit) {
                $Limit = "LIMIT $Limit";
            } elseif (!$this->NoListScroll && $this->ListScroll->Data["Limit"]) {
                $Limit = "LIMIT ".$this->ListScroll->Data["First"].", ".$this->ListScroll->Data["Limit"];
            } else $Limit = "";

            $Conds = array();
            if ($IteCond) $Conds[1] = $IteCond;
            if ($SecCond) $Conds[2] = $SecCond;
            if ($StrCond) $Conds[3] = $StrCond;
            if ($StateCond) $Conds[4] = $StateCond;
            if ($PeriodCond) $Conds[5] = $PeriodCond;
            if ($TimeCond) $Conds[6] = $TimeCond;
            if ($FilterCond) $Conds[7] = $FilterCond;
            if ($LanguageCond) $Conds[8] = $LanguageCond;
            if ($TagsCond) $Conds[8] = $TagsCond;

            $Conds = join(' AND ',$Conds);

            $Items = DB::selectArray("SELECT ci.*, DATE_FORMAT(Ite_Time, '".$this->DateFormat."') as Ite_Time, UNIX_TIMESTAMP(Ite_Time) as Unixtime, au.login
                                        $QueryCond
                                        FROM contentItems AS ci
                                          LEFT JOIN contentItemsSections AS isec
                                            ON ci.Ite_Code = isec.Item
                                          LEFT JOIN AdmUsers AS au
                                            ON ci.us_id = au.us_id
                                        WHERE $Conds
                                          $JoinCond
                                          $Limit", "Ite_Code");

//            $GroupOrder = ($this->GetProperty("GroupSecFirst")) ? $SecGroupOrder.$StrGroupOrder : $StrGroupOrder.$SecGroupOrder;

            $Items = sortArray($Items, $OrderCond);

            if($this->Scroll)
                $this->Scroll->Data['Quantity'] = count($Items);

            if($this->Limit) {
                $Items = array_slice($Items, 0, $this->Limit, true);
            } elseif( $this->Scroll && ($this->Scroll->Part > $this->Scroll->Groups) ) {
                $Items = array_slice($Items, ( $this->Scroll->First - ( $this->Scroll->Limit * ($this->Scroll->Part - 1) ) ), $this->Scroll->Limit, true);
            } elseif($this->Scroll) {
                $Items = array_slice($Items, $this->Scroll->First, $this->Scroll->Limit, true);
            }

            $this->Items = array();
            foreach($Items as $Ite_Code => $Item) {
                if($ReadItemVars)
                    $Item = $this->GetItemVars($Item, (count($Items) != 1));
                if(is_object($this->ContentCart))
                    $Item['Ite_Quantity'] = $this->ContentCart->Cart[$Item['Ite_Code']];

                $this->Items[$Item['Ite_Code']] = $Item;
            }

            if($_REQUEST['order']) {
                $OrderArr = array();
                foreach($_REQUEST['order'] as $Key => $Direction) {
                    array_push($OrderArr, $Key);
                    array_push($OrderArr, strtoupper($Direction));
                }
                $this->Items = sortArray($this->Items, $OrderArr);
            }

            //Вот здесь можно на всякий случай построить привязки
            if($GroupOrder) {
                $Groups=array();
                if($this->Sections['Group'] && $this->Structures['Group']) {
                    if($this->GetProperty('GroupSecFirst')) {
                        $Type1='Sec_Code';$Type2='Str_Code';
                    } else {
                        $Type2='Sec_Code';$Type1='Str_Code';
                    }
                    $Groups=array($Type1=>array());
                    foreach($this->Items as $iKey=>$Item) {
                        if(!isset($Groups[$Type1][$Item[$Type1]])) {
                            $Groups[$Type1][$Item[$Type1]]=array($Type1=>$Item[$Type1], $Type2=>array());
                        }
                        if(!isset($Groups[$Type1][$Item[$Type1]][$Type2][$Item[$Type2]])) {
                            $Groups[$Type1][$Item[$Type1]][$Type2][$Item[$Type2]]=array($Type2=>$Item[$Type2], 'Items'=>array());
                        }
                        $Groups[$Type1][$Item[$Type1]][$Type2][$Item[$Type2]]['Items'][]=&$this->Items[$iKey];
                    }
                } elseif($this->Sections['Group'] || $this->Structures['Group']) {
                    if($this->Sections['Group']) {
                        $Type='Sec_Code';
                    } else {
                        $Type='Str_Code';
                    }
                    foreach($this->Items as $iKey=>$Item) {
                        if(!isset($Groups[$Type][$Item[$Type]])) {
                            $Groups[$Type][$Item[$Type]]=array($Type=>$Item[$Type], 'Items'=>array());
                        }
                        $Groups[$Type][$Item[$Type]]['Items'][]=&$this->Items[$iKey];
                    }
                }
                $this->Groups = $Groups;
            }
        }

        function GetItemsByFilter($FilterID, $FilterValue) {
            if(!is_array($FilterValue))
                $LVa_Code = DB::selectValue("SELECT LVa_Code FROM contentListValues WHERE LVa_Value = '$FilterValue'");

            if($LVa_Code) {
                $IteFilCodes  = DB::selectArray("SELECT Ite_Code FROM contentData3 LEFT JOIN contentColumns AS cc ON contentData3.Col_Code = cc.Col_Code WHERE cc.Col_Id = '$FilterID' AND contentData3.Data = $LVa_Code", "Ite_Code");
                $IteFilCodes2 = DB::selectArray("SELECT Ite_Code FROM contentData4 LEFT JOIN contentColumns AS cc ON contentData4.Col_Code = cc.Col_Code WHERE cc.Col_Id = '$FilterID' AND contentData4.Data = $LVa_Code", "Ite_Code");
                $IteFilCodes3 = DB::selectArray("SELECT Ite_Code FROM contentData31 LEFT JOIN contentColumns AS cc ON contentData31.Col_Code = cc.Col_Code WHERE cc.Col_Id = '$FilterID' AND contentData31.Data = $LVa_Code", "Ite_Code");
            } elseif(is_array($FilterValue)) {
                $IteFilCodes = DB::selectArray("SELECT Ite_Code FROM contentData1 LEFT JOIN contentColumns AS cc ON contentData1.Col_Code = cc.Col_Code WHERE cc.Col_Id = '$FilterID' AND (contentData1.Data >= ".$FilterValue['min']." AND contentData1.Data <= ".$FilterValue['max'].")", "Ite_Code");
            } else {
                $IteFilCodes2 = DB::selectArray("SELECT Ite_Code FROM contentData10 LEFT JOIN contentColumns AS cc ON contentData10.Col_Code = cc.Col_Code WHERE cc.Col_Id = '$FilterID' AND contentData10.Data = '$FilterValue'", "Ite_Code");
                $IteFilCodes3 = DB::selectArray("SELECT Ite_Code FROM contentData11 LEFT JOIN contentColumns AS cc ON contentData11.Col_Code = cc.Col_Code WHERE cc.Col_Id = '$FilterID' AND contentData11.Data = '$FilterValue'", "Ite_Code");
            }

            if(count($IteFilCodes2))
                $IteFilCodes = $IteFilCodes + $IteFilCodes2;
            if(count($IteFilCodes3))
                $IteFilCodes = $IteFilCodes + $IteFilCodes3;

            return $IteFilCodes;
        }


        /*
         * Возвращает массив с информацией о записи + кеширование
         *
         * @param array $Item
         * @param int $NamePart
         * @return array
         */
        public function GetItemVars($Item, $Part = true, $Related = false, $SaveData = true, $cached = true) {
            $CacheId = "contentItemVars.".$Item['Ite_Code'].($Part ? ".part" : "").$this->LngPrefix;

            if($cached)
                $CachedData = DBMem::Get($CacheId);

            if($CachedData) {
                $Item = array_merge($Item, $CachedData);
            } else {

//                $Item = DB::selectArray("SELECT Ite_Header, Sec_Code, Ite_BDate, Ite_EDate,
//                          DATE_FORMAT(Ite_BDate, '%d') AS Ite_BDay,
//                          DATE_FORMAT(Ite_BDate, '%m') AS Ite_BMonth,
//                          DATE_FORMAT(Ite_BDate, '%Y') AS Ite_BYear,
//                          DATE_FORMAT(Ite_EDate, '%d') AS Ite_EDay,
//                          DATE_FORMAT(Ite_EDate, '%m') AS Ite_EMonth,
//                          DATE_FORMAT(Ite_EDate, '%Y') AS Ite_EYear,
//                          Ite_Priority,
//                          Ite_State,
//                          Str_Code,
//                          #us_id,
//                          Ite_Time, Time_Change FROM contentItems WHERE Ite_Code = ".$Item['Ite_Code']);

                $ItemCols = $this->GetStrCols($Item['Str_Code'], $Part);

                if(CONTENT_TAGS) {
                    $classTags = _autoload("classTags");
                    $Item['Tags'] = $classTags->GetTags(CONTENT_TAGS, $Item['Ite_Code']);
                }

                foreach($ItemCols as $Col_Code => $Col) {
                    if($Col['Col_Type'] == 8 || $Col['Col_Type'] == 9) {
                        $Data = DB::selectArray("SELECT Cnt, Data FROM contentData".$Col['Col_Type']." WHERE Col_Code = ".$Col_Code." AND Ite_Code = ".$Item['Ite_Code']." ORDER BY Cnt");
                    } elseif($Col['Col_Type'] == 7) {
                        $Data = DB::selectArray("SELECT Cnt, Data, Data_Desc FROM contentData".$Col['Col_Type']." WHERE Col_Code = ".$Col_Code." AND Ite_Code = ".$Item['Ite_Code']." ORDER BY Cnt", "any");
                    } elseif($Col['Col_Type'] == 31) {
                        $Data = DB::selectArray("SELECT Data FROM contentData3 WHERE Col_Code = ".$Col_Code." AND Ite_Code = ".$Item['Ite_Code']);
                    } else {
                        $Data = DB::selectArray("SELECT Data FROM contentData".$Col['Col_Type']." WHERE Col_Code = ".$Col_Code." AND Ite_Code = ".$Item['Ite_Code']);
                    }
                    if($Col['Col_Type'] == 4) {
                        $LstValues = $this->GetLstValues($Col['Lst_Code']);
                        $ItemCols[$Col_Code]['Value'] = array();
                        foreach($Data as $Key => $Value)
                            $ItemCols[$Col_Code]['Value'][$Value['Data']] = $LstValues[(string)$Value['Data']];

                    } elseif($Col['Col_Type'] == 3 || $Col['Col_Type'] == 31) {
                        $LstValues = $this->GetLstValues($Col['Lst_Code']);
                        $ItemCols[$Col_Code]['Value'] = $LstValues[$Data['Data']]['LVa_Header'];
                    } elseif($Col['Col_Type'] == 7) {
                        $ItemCols[$Col_Code]['Value'] = array();
                        foreach($Data as $Key => $Value) {
                            $ItemCols[$Col_Code]['Value'][] = array(
                                "LstValue" => $Value['Data'],
                                "LstKey"   => $Value['Cnt'],
                                "LstDesc"  => $Value['Data_Desc'],
                            );
                            // ksort($ItemCols[$Col_Code]['Value']);
                        }
                    } elseif($Col['Col_Type'] == 8 || $Col['Col_Type'] == 9) {
                        $ItemCols[$Col_Code]['Value'] = array();
                        foreach($Data as $Key => $Value) {
                            $ItemCols[$Col_Code]['Value'][] = array(
                                "LstValue" => $Value['Data'],
                                "LstKey"   => $Value['Cnt']
                            );
                            // ksort($ItemCols[$Col_Code]['Value']);
                        }
                    } elseif($Col['Col_Type'] == 11) {
                        foreach($Data as $Key => $Value) {
                            if($Value && $Value['Data']) {
                                $StrVars = array("Ite_Code" => $Value['Data']);
                                $ItemCols[$Col_Code]['Value'][$Value['Data']] = $this->GetItemVars($StrVars, $Part, false, false);
                            }
                        }
                    } elseif($Col['Col_Type'] == 12) {
                        $ItemCols[$Col_Code]['Value'] = $Data['Data'];

                        if($ItemCols[$Col_Code]['Value'] != "0000-00-00") {
                            $addCol_Code = count($ItemCols) + 100;
                            list($Year, $Month, $Day) = explode("-", $ItemCols[$Col_Code]['Value']);
                            $ItemCols[$addCol_Code]['Col_Id'] = $Col['Col_Id']."_day";
                            $ItemCols[$addCol_Code]['Value'] = $Day;
                            $addCol_Code++;
                            $ItemCols[$addCol_Code]['Col_Id'] = $Col['Col_Id']."_month";
                            $ItemCols[$addCol_Code]['Value'] = $Month;
                            $addCol_Code++;
                            $ItemCols[$addCol_Code]['Col_Id'] = $Col['Col_Id']."_year";
                            $ItemCols[$addCol_Code]['Value'] = $Year;
                        } else {
                            unset($ItemCols[$Col_Code]['Value']);
                        }
                    } else {
                        $ItemCols[$Col_Code]['Value'] = $Data['Data'];
                    }
                }

                foreach($ItemCols as $Col) {
                    $Item[$Col['Col_Id']] = $Col['Value'];
                }
                $Item['ItemCols'] = $ItemCols;
                $Item = array_merge($Item, $Item);

                $Item['Ite_BMonthRus'] = self::$RusMonths[$Item['Ite_BMonth']];
                $Item = array_merge($Item, $this->GetPath($Item['Sec_Code']));
                $Item['Ite_Link'] = $Item['SectionLink']."?".$this->ItemID."=".$Item['Ite_Code'];

                if(SEF_URL) {
                    $Router = sefRouter::Get("classContent", $Item['Ite_Code']);
                    if($Router)
                        $Item['Ite_Link'] = (SEF_ITEM_SECTION ? "/" : $Item['SectionLink']).$Router['Url'].".html";
                }

                /* связанные объекты */
                if(!$Related) {
                    $RelatedItems = DB::selectArray("SELECT cr.*, ci.*, DATE_FORMAT(Ite_Time, '".$this->DateFormat."') as Ite_Time, UNIX_TIMESTAMP(Ite_Time) as Unixtime
                                                  FROM contentRelated AS cr
                                                INNER JOIN contentItems AS ci
                                                  ON ci.Ite_Code = cr.Ite_CodeR
                                                WHERE cr.Ite_Code = ".$Item['Ite_Code'], "Ite_CodeR");
                    foreach($RelatedItems as $Key => $Related) {
                        $Related['Ite_Code'] = $Related['Ite_CodeR'];
                        $Item['Related'][$Related['Ite_CodeR']] = $this->GetItemVars($Related, $Part, true, false);
                    }
                }

                if($SaveData)
                    DBMem::Set($Item, $CacheId);

            }
    
            /* не кешировать */
    
            if($_SESSION['_admin_login'])
                $Item['Ite_EditLink'] = ADMIN_URL."/content/ite_edit.php?Ite_Code=".$Item['Ite_Code'];

            return $Item;
        }


        function GetSectionID() {
            if(is_object($this->Parent)) {
                return $this->Parent->GetSectionID();
            } else {
                return $this->Parent;
            }
        }


        function Action() {
            // Empty
        }


        function classContentData($sec = "", $Parent = "") {
            // значение по умолчанию
            $this->Properties['public']['Values']['DateFormat'] = "%d.%m.%Y %H:%i";
            $this->Properties['public']['Values']['IteID'] = "id";
            $this->Properties['public']['Values']['SearchID'] = "query";

            parent::classSecurity($sec, $Parent);
            $this->DateFormat = $this->GetProperty('DateFormat');
            $this->DataType = $this->GetProperty("DataType");
            /*
                "1" => "Собственные данные",
                "2" => "Данные из родительского раздела",
                "21"  =>  "Данные из всех подразделов 1го уровня",
                "22"  =>  "Данные из всех подразделов 1го уровня заданного раздела",
                "3" => "Данные из заданного раздела",
                "4" => "Множественный выбор разделов",
                "5" => "Новое на сайте",
                "6" => "Общий поиск по сайту",
                "7" => "Поиск по заданной структуре",
                "8" => "Корзина",
                "9"  => "Связанные объекты",
            */
            if($this->DataType!=3 && $this->DataType!=22) {
                unset($this->Properties["private"]["Names"]["Section"]);
            }
            if($this->DataType!=4) {
                unset($this->Properties["private"]["Names"]["Sections"]);
            }

            if($this->DataType!=7) {
                unset($this->Properties["private"]["Names"]["Structure"]);
                $this->Structures=$this->GetProperty('Structures');
            } else {
                unset($this->Properties["private"]["Names"]["Structures"]);
                $this->Str_Code=$this->GetProperty('Structure');
            }

            if($this->DataType!=21 && $this->DataType!=22) {
                unset($this->Properties["private"]["Names"]["GroupChildrenSections"]);
            }

            /*
                if($this->DataType==8) {
                  unset($this->Properties["private"]["Names"]["Section"]);
                  unset($this->Properties["private"]["Names"]["Sections"]);
                  unset($this->Properties["private"]["Names"]["Structure"]);
                  unset($this->Properties["private"]["Names"]["Structures"]);
                  unset($this->Properties["private"]["Names"]["GroupChildrenSections"]);
                } else {
                  unset($this->Properties["private"]["Names"]["PriceCols"]);
                }
            */

            //Определим Sec_Code или Sections
            if($this->DataType == 1 || $this->DataType == 2 || $this->DataType == 9) {
                $Sec_Code = $this->GetSectionID();
                $this->Sec_Code = (int)$Sec_Code;
                if($this->DataType == 2 && $Sec_Code) {
                    $Sections = DB::selectArray("SELECT Sec_Parent FROM Sections WHERE Sec_Code = $Sec_Code", "Sec_Code");
                    foreach($Sections as $Sec_Code => $Value)
                        $this->Sec_Code = $Sec_Code;
                }
            } elseif($this->DataType==21) {
                $this->Sections = array("Sections" => array(), "Group" => $this->GetProperty("GroupChildrenSections"), "Order" => array());
                $Sec_Code = $this->GetSectionID();
                if((int)$Sec_Code || $Sec_Code === 0) {
                    $Sections = DB::selectArray("SELECT Sec_Code, Sec_Order FROM Sections WHERE Sec_Parent = $Sec_Code", "Sec_Code");
                    $Sections = SortArray($Sections, array("Sec_Code", "ASC", "Sec_Order", "DESC"));
                    foreach($Sections as $Sec_Code => $Section) {
                        $this->Sections['Sections'][] = $Sec_Code;
                        $this->Sections['Order'][$Sec_Code] = $Section['Sec_Order'];
                    }
                }
            } elseif($this->DataType==22) {
                $this->Sections=array('Sections'=>array(), "Group"=>$this->GetProperty("GroupChildrenSections"), "Order"=>array());
                $Sec_Code=$this->GetProperty("Section");
                if((int)$Sec_Code || $Sec_Code === 0) {
                    $Sections = DB::selectArray("SELECT Sec_Code, Sec_Order FROM Sections WHERE Sec_Parent = $Sec_Code", "Sec_Code");
                    foreach($Sections as $Sec_Code => $Section) {
                        $this->Sections['Sections'][] = $Sec_Code;
                        $this->Sections['Order'][$Sec_Code] = $Section['Sec_Order'];
                    }
                }
            } elseif($this->DataType==3) {
                $this->Sec_Code=$this->GetProperty('Section');
            } elseif($this->DataType==4) {
                $this->Sections = $this->GetProperty('Sections');
                if(is_array($this->Sections['Sections'])) {
                    foreach($this->Sections['Sections'] as $k=>$v) {
                        if(!$v) unset($this->Sections['Sections'][$k]);
                    }
                }
            } elseif($this->DataType==7) {
                if(isset($_REQUEST[$this->GetProperty('SectionsID')])) {
                    $Sections=$_REQUEST[$this->GetProperty('SectionsID')];
                    if(is_array($Sections) && count($Sections)) {
                        $this->Sections['Sections']=$Sections;
                        $this->Sections['Order'] = DB::selectArray("SELECT Sec_Code, Sec_Order FROM Sections WHERE Sec_Code IN (".join(",", $Sections).")", "Sec_Code");
                        $this->Sections['Group'] = $_REQUEST[$this->GetProperty('GroupSectionsID')];
                    } elseif((int)$Sections)
                        $this->Sec_Code = $Sections;
                }
            }

            if(!isset($this->Sections) || !isset($this->Structures) || !$this->Sections['Group'] || !$this->Structures['Group']) {
                unset($this->Properties['private']['Names']['GroupSecFirst']);
            }

            $this->ItemID = $this->GetProperty("IteID");

            if( !$this->GetProperty("ContentFilter") && !($this->DataType == 1 || $this->DataType == 3) ) {
                unset($this->Properties['private']['Names']['AndOr']);
            }

            // что-то тут не так
            //    if ((int)$_REQUEST[$IteID] && !$_REQUEST[$FilterID])
            $this->Ite_Code = $this->Ite_Code ?: ((int)$_REQUEST[$this->ItemID] && !$_REQUEST[$FilterID]) ? (int)$_REQUEST[$this->ItemID] : 0;

            $this->ListScroll = $this->GetBlock("ListScroll");

            if(!class_exists("classContentCart"))
                unset($this->Properties['private']['Names']['ContentCart']);
            else
                $this->ContentCart = $this->GetBlock("ContentCart");

            $this->TimeStart = $this->Time2Mysql($_REQUEST[$this->GetProperty('TimeStartID')]);
            $this->TimeEnd = $this->Time2Mysql($_REQUEST[$this->GetProperty('TimeEndID')]);

        }


        function Time2Mysql($a) {
            if(is_array($a)) {
                return $a['Y'].$a['m'].$a['d'].$a['H'].$a['i'].$a['s'];
            }
            if(!strlen($a)) return "";
            $time=strtotime($a);
            if($time!=-1) return (date("YmdHis", $time));
            return "";
        }

        function lemmatize($word) {
            $word=strtolower($word);
            $sWord=preg_replace('/(\w{2,})(ого|ово|ых|ам|ов|ек|их|ах|ях|ель|ми|му|ом|ть|ев|ок|еб|ка|ки|ке|ся|ыс|ют|ет|ым|ем|им|ам)$/',"\\1",$word);
            $sWord=str_replace('нн','н',$sWord);
            $sWord=preg_replace('/[аеёийоуыэюяьъ]+$/','',$sWord);
            $sWord=trim($sWord);
            if(strlen($sWord)<3) return '';
            return $sWord;
        }

        function contentFavoritesAdd() {
            $contentFavorites = json_decode($_COOKIE['contentFavorites'], true);
            $_COOKIE['contentFavorites'] = $contentFavorites ?: array();
            $_COOKIE['contentFavorites'][(int)$_REQUEST['id']] = time();
            setcookie("contentFavorites", json_encode($_COOKIE['contentFavorites']), time() + (86400 * 300), "/", ".".DOMAIN_NAME);
        }

    }

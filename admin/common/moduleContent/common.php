<?php


    class InputContentPriceCols extends Input {
        function dPrint() {
            echo "<table>";
            $Structures = DB::selectArray("SELECT Str_Code, Str_Name FROM contentStructure ORDER BY Str_Order", "Str_Code");
            foreach($Structures as $Str_Code => $Structure) {
                echo "<tr><td align=right>".$Structure['Str_Name']."</td><td>";
                $Obj = new InputDBSelect($this->Name.'['.$Str_Code.']', $this->Value[$Str_Code], "contentColumns", "Col_Code", "Col_Header", "Str_Code = $Str_Code", "Col_Order", "", $onChange);
                $Obj->Empty = 1;
                $Obj->dPrint();
                echo "</td></tr>";
            }
            echo "</table>";
        }

        function InputContentPriceCols($Name, $Value) {
            parent::Input($Name, $Value, $Class="classContentList");
        }
    }


    class ContentDataTypeInputSelect extends InputSelect {
        function ContentDataTypeInputSelect($Name, $Value) {
            parent::InputSelect(
                $Name,
                $Value,
                array(
                    "0"  => "[выбрать]",
                    "1"  => "Собственные данные",
                    "2"  => "Данные из родительского раздела",
                    "21" => "Данные из всех подразделов 1го уровня текущего раздела",
                    "22" => "Данные из всех подразделов 1го уровня заданного раздела",
                    "3"  => "Данные из заданного раздела",
                    "4"  => "Множественный выбор разделов",
                    "5"  => "Новое на сайте",
                    "6"  => "Общий поиск по сайту",
                    "7"  => "Поиск по заданной структуре",
                    "8"  => "Корзина",
                    "9"  => "Связанные объекты",
                    "10" => "Последние просмотренные",
                    "11" => "Избранное",
                )
            );
        }
    }


    class InputContentStructureDBSelect extends InputDBSelect {
        function InputContentStructureDBSelect($Name, $Value, $onChange="") {
            $this->InputDBSelect($Name, $Value, "contentStructure", "Str_Code", "Str_Name", "", "Str_Order DESC, Str_Code ASC", "", $onChange);
        }
    }


    class InputContentStructureDBMultiSelect extends InputDBMultiSelect {
        //Засунем группировку тут же
        var $SelectName;
        var $Structures;
        var $Group;
        var $Order;

        function dPrint() {
            $Structures = DB::selectArray("SELECT Str_Code, Str_Name FROM contentStructure ORDER BY Str_Order", "Str_Code");
            if(count($Structures)) {
                echo "<table border=0 cellspacing=1 cellpadding=1>";
                if($this->Value['Group'])
                    echo "<tr><td>&middot;</td><td>Сорт.</td><td>&nbsp;</td></tr>";
                if(!is_array($this->Value['Structures']))
                    $this->Value['Structures'] = array();
                foreach($Structures as $Str_Code => $Structure) {
                    $Checked = in_array($Str_Code, $this->Value['Structures']) ? " checked" : "";
                    echo "<tr><td><input type=\"checkbox\" name=\"".$this->Name."[Structures][]\" value=\"$Str_Code\" class=\"radio\"$Checked></td>";
                    if($this->Value['Group'])
                        echo "<td><input type=\"text\" name=\"".$this->Name."[Order][$Str_Code]\" value=\"".$this->Value["Order"]["$Str_Code"]."\" size=\"3\"></td>";
                    echo "<td>".$Structure['Str_Name']."</td></tr>";
                };
                echo "</table>";
            };
            if(count($this->Structures)>1) {
                $Group = new InputYesNo($this->Name."[Group]", $this->Group);
                echo("Группировать по структурам: ");$Group->dPrint();
            };
        }

        function InputContentStructureDBMultiSelect($Name, $Value, $onChange="") {
            parent::Input($Name, $Value);
            $this->Structures=$Value['Structures'];
            $this->Group=$Value['Group'];
            $this->Order=$Value['Order'];
        }
    }


    class InputFiltersDBMultiSelect extends InputDBMultiSelect {
        function dPrint() {
            $Checked = "";
            $Cols = DB::selectArray("SELECT Col_Code, Col_Header, Col_Id, Lst_Code FROM contentColumns WHERE Col_Type = 3 OR Col_Type = 4 ORDER BY Col_Order", "Col_Code");
            echo "<table border=0 cellspacing=1 cellpadding=1>";
            foreach($Cols as $Col_Code => $Col) {
                if(is_array($this->Value['Filters']))
                    $Checked = in_array($Col['Col_Id'], $this->Value['Filters']) ? " checked" : "";
                echo "<tr><td><input type=\"checkbox\" name=\"".$this->Name."[Filters][]\" value=\"".$Col['Col_Id']."\"$Checked></td>";
                echo "<td>".$Col['Col_Header']."</td></tr>";
            }
            echo "</table>";
        }
        function InputFiltersDBMultiSelect($Name, $Value, $onChange="") {
            parent::Input($Name, $Value);
            $this->Filters = $Value['Filters'];
        }
    }


    class InputManySectionsContentData extends Input {
        var $SectionsArr;
        var $Group;
        var $Order;

        function GetSectionName($Sec_Code) {
            if($Sec_Code)
                return $this->GetSectionName($this->SectionsArr[$Sec_Code]["Data"]["Parent"])." / ".$this->SectionsArr[$Sec_Code]["Data"]["Name"];
            else
                return "Главная";
        }

        function dPrint() {
            $this->SectionsArr = array();
            $Sections = DB::selectArray("SELECT Sec_Code, Sec_Parent, Sec_Name FROM Sections", "Sec_Code");
            foreach($Sections as $Sec_Code => $Section) {
                $this->SectionsArr[$Sec_Code] = array(
                    "Data" => array(
                        "Parent" => $Section['Sec_Parent'],
                        "Name" => $Section['Sec_Name']
                    ),
                    "Parent" => ""
                );
            }
            foreach($this->SectionsArr as $Key => $Section) {
                if($Section["Data"]["Parent"])
                    $this->SectionsArr[$Key]["Parent"] = &$this->SectionsArr[$Section["Data"]["Parent"]];
            }

            if(is_array($this->Value["Sections"])) {
                echo "<table border=0 cellspacing=1 cellpadding=1><tr><td>&middot;</td><td>Кол-во</td>";
                if($this->Value['Group']) echo "<td>Сорт.</td>";
                echo "<td>&nbsp;</td></tr>";
                foreach($this->Value["Sections"] as $Key => $Value) {
                    if ($Value) {
                        echo "<tr><td><input type=\"checkbox\" name=\"".$this->Name."[Sections][]\" value=\"$Value\" checked></td>";
                        echo "<td><input type=\"text\" name=\"".$this->Name."[Quantity][$Value]\" value=\"".$this->Value["Quantity"]["$Value"]."\" size=\"3\"></td>";
                        if($this->Value['Group']) echo "<td><input type=\"text\" name=\"".$this->Name."[Order][$Value]\" value=\"".$this->Value["Order"]["$Value"]."\" size=\"3\"></td>";
                        echo "<td><nobr>".$this->GetSectionName($Value)."</nobr>";
                    }
                }
                echo "</table>";
            }

            $Obj = new InputSectionsTree($this->Name."[Sections][]", "");
            $Obj->Empty=1;
            $Obj->Value="";
            $Obj->dPrint();

            if(count($this->SectionsArr)>1) {
                $Group=new InputYesNo($this->Name."[Group]",$this->Value['Group']);
                echo("<br>Группировать по разделам: ");$Group->dPrint();
            };
        }

        function InputManySectionsContentData($Name, $Value) {
            parent::Input($Name, $Value);
        }
    }


    class InputSectionsContentData extends InputModules {
        function InputSectionsContentData($Name, $Value) {
            parent::InputModules($Name, $Value, $Class="classContentData");
        }
    }


    class InputSectionsContentList extends InputModules {
        function InputSectionsContentList($Name, $Value) {
            parent::InputModules($Name, $Value, $Class="classContentList");
        }
    }


    class InputSectionsContentCart extends InputModules {
        function InputSectionsContentCart($Name, $Value) {
            parent::InputModules($Name, $Value, $Class = "classContentCart");
        }
    }


    class InputSectionsContentEdit extends InputModules {
        function InputSectionsContentFilter($Name, $Value) {
            parent::InputModules($Name, $Value, $Class = "classContentEdit");
        }
    }


    class InputContentOrderBy extends InputSelect {
        function InputContentOrderBy($Name, $Value) {
            $dbOptions = DB::selectArray("DESCRIBE contentItems");
            $Options = array();
            foreach($dbOptions as $Key => $Field) {
                $Options[$Field['Field']." ASC"] = $Field['Field']." по возрастанию";
                $Options[$Field['Field']." DESC"] = $Field['Field']." по убыванию";
            }
            $Options["Ite_Priority, Ite_BDate"] = "Ite_Priority, Ite_BDate по возрастанию";
            $Options["Ite_Priority DESC, Ite_BDate DESC"] = "Ite_Priority, Ite_BDate по убыванию";
            $Options["Ite_Priority DESC, Ite_EDate DESC"] = "Ite_Priority, Ite_EDate по убыванию";
            $Options["RANDOM"] = "Случайно";

            $this->Empty = true;
            parent::InputSelect($Name, $Value, $Options);
        }
    }


    class InputFilterData extends InputSelect {
        function InputFilterData($Name, $Value) {
            $dbOptions = DB::selectArray("SELECT Lst_Code, Lst_Name FROM contentLists ORDER BY Lst_Order DESC, Lst_Code ASC", "Lst_Code");
            foreach($dbOptions as $Key => $Element)
                $Options[$Element['Lst_Code']] = $Element['Lst_Name'];
            $this->InputSelect($Name, $Value, $Options);
        }
    }


    class InputFilterCols extends InputSelect {
        function InputFilterCols($Name, $Value) {
            $dbOptions = DB::selectArray("SELECT Col_Code, Col_Header FROM contentColumns ORDER BY Col_Order DESC, Col_Code", "Col_Code");
            foreach($dbOptions as $Key => $Element)
                $Options[$Element['Col_Code']] = $Element['Col_Header'];
            $this->InputSelect($Name, $Value, $Options);
        }
    }


    class InputContentListRelated extends InputModules {
        function InputContentListRelated($Name, $Value) {
            parent::InputModules($Name, $Value, $Class = "classContentList");
        }
    }


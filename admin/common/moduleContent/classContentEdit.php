<?

  class classContentEdit extends classProperties {
    var $Properties = array(
      "public" => array(
        "Names" => array(
          "Users", 
          "DoID",
        ), 
        "Types" => array(
          "InputUsers", 
          "T50",
        ), 
        "Labels" => array(
          "Блок пользователи", 
          "Идентификатор действия", 
        )
      ),
      "private" => array(
        "Names" => array(
          "ContentData", 
          "Sec_Code", 
          "Str_Code", 
          "PreMod",
          "Separator",
          "template_form_top",
          "template_form_1",
          "template_form_2",
          "template_form_6",
          "template_form_bottom",
          "template_form_error", 
          "template_form_ok", 
        ), 
        "Types" => array(
         "InputSectionsContentData", 
         "InputSectionsTree", 
         "InputContentStructureDBSelect", 
         "InputYesNo", 
         "",
         "InputTemplates", 
         "InputTemplates", 
         "InputTemplates", 
         "InputTemplates", 
         "InputTemplates", 
         "InputTemplates", 
         "InputTemplates", 
        ), 
        "Labels" => array(
          "Источник данных", 
          "Раздел", 
          "Структура",
          "Премодерация",
          "Шаблон Формы", 
          "Верх", 
          "Text", 
          "Textarea", 
          "HTML", 
          "Низ", 
          "Критическая ошибка", 
          "OK", 
        )
      )
    );

    var $DefaultTemplates = array(
      "template_form_top" => array("0" => ""), 
      "template_form_1" => array("0" => ""), 
      "template_form_2" => array("0" => ""), 
      "template_form_6" => array("0" => ""), 
      "template_form_bottom" => array("0" => ""), 
      "template_form_error" => array("0" => ""), 
      "template_form_ok" => array("0" => ""), 
    );

    function GetClassName() {
      return "classContentEdit";
    }

    function Action() {
      $this->Data['Errors'] = array();
      $this->Users->GetInfo();
      $this->User = $this->Users->Data;
      $this->Ite_Code = $this->ContentData->Ite_Code;
      $this->Sec_Code = $this->GetProperty("Sec_Code");
      $this->Str_Code = $this->GetProperty("Str_Code");
      $this->Columns = $this->GetColumns($this->ContentData->Str_Code);
      if($this->Ite_Code) {
        $this->ContentData->DataType = 1;
        $this->ContentData->Sec_Code = $this->Sec_Code;
        $this->ContentData->GetContentList(0);
        $this->Items = &$this->ContentData->Items;
        if(count($this->Items))
          $this->Data = array_merge($this->Data, array_pop($this->Items));
      }
      $Do = $_REQUEST[$this->DoID];
      switch($Do) {
        case "Post" :
          $this->DoPost();
        break;
        default:
          $this->DoEdit();
        break;
      }
    }

    function dPrintForm() {
      $this->Ins2Php("template_form_top");
      $OldData = $this->Data;
      foreach($this->Columns as $Col_Code => $ColumnData) {
        $ColumnData['Col_Name'] = 'Col_'.$Col_Code;
        switch ($ColumnData['Col_Type']) {
          case 1: // строка
          case 2: // большой текст
            $ColumnData['Col_Data'] = str_replace('"','&quot;',$ColumnData['Col_Data']);
            break;
          case 3: // список
            foreach ($ColumnData['ListValues'] as $LVa_Code => $ListValue) {
              if($ListValue['LVa_Code'] == $ColumnData['Col_Data']) {
                $ColumnData['ListValues'][$LVa_Code]['Selected'] = ' selected ';
              } else {
                $ColumnData['ListValues'][$LVa_Code]['Selected'] = '';
              }
            }
            break;
          case 4: // мультисписок
            foreach ($ColumnData['ListValues'] as $LVa_Code => $ListValue) {
              $ColumnData['Col_Data'] = (array)$ColumnData['Col_Data'];
              if (in_array($ListValue['LVa_Code'],$ColumnData['Col_Data'])) {
                $ColumnData['ListValues'][$LVa_Code]['Checked'] = ' checked ';
              } else {
                $ColumnData['ListValues'][$LVa_Code]['Checked'] = '';
              }
            }
            break;
          case 5: //TODO обработку файла
            break;
          case 6: // html поле
            $ColumnData['Col_Data'] = str_replace('"','&quot;',$ColumnData['Col_Data']);
            break;
        }
        $this->Data = $ColumnData;
        $this->Ins2Php("template_form_{$ColumnData['Col_Type']}");
      }
      $this->Data = $OldData;
      $this->Ins2Php("template_form_bottom");
    }


    function DoEdit($Try=0) {
      if($this->User['us_id'] != $this->Data['us_id'] && $this->Ite_Code) {
        array_push($this->Data['Errors'], array("Error" => "Вы не можете редактировать эту статью"));
        $this->Data['ErrorEdit'] = 1;
      }

      if($this->User['nouser']) {
        array_push($this->Data['Errors'], array("Error" => "Добавлять и редактировать статьи могут только зарегистрированные пользователи"));
        $this->Data['ErrorEdit'] = 1;
      }

      if($Try) {

        $this->Data['Ite_Header'] = strip_tags($_REQUEST['Header']);
        $this->Data['Ite_Header'] = substr($this->Data['Ite_Header'], 0, 255);

        if($this->Data['Ite_Header'] == "")
          array_push($this->Data['Errors'], array("Error" => "Не заполнен заголовок"));


        foreach($this->Columns as $Col_Code => $ColumnData) {
          $ColumnData['Col_Name'] = 'Col_'.$Col_Code;
          switch ($ColumnData['Col_Type']) {
            case 2:
              $this->Columns[$Col_Code]['Col_Data'] = substr(strip_tags($_REQUEST[$ColumnData['Col_Name']]), 0, 255);
            break;
            case 6:
              $this->Columns[$Col_Code]['Col_Data'] = $this->Check($_REQUEST[$ColumnData['Col_Name']]);
              $this->Columns[$Col_Code]['Col_Data'] = str_replace("<br />", "", $this->Columns[$Col_Code]['Col_Data']);
            break;
            default:
              $this->Columns[$Col_Code]['Col_Data'] = strip_tags($_REQUEST[$ColumnData['Col_Name']]);
            break;
          }
          if($ColumnData['Col_Must'] && $this->Columns[$Col_Code]['Col_Data'] == "") {
            array_push($this->Data['Errors'], array("Error" => "Не заполнено обязательное поле ".$ColumnData['Col_Header']."!"));
          }
        }
        
        if(count($this->Data['Errors']))
          $this->Data['PostError'] = 1;

      }

      if($Try && !count($this->Data['Errors']))
        return 1;

      if(!$this->Data['ErrorEdit']) {
        $this->dPrintForm();
      } else {
        $this->Ins2Php("template_form_error");
      }

      if($Try) return 0;
    }


    function DoPost() {
      if($this->DoEdit(1)) {
        $Cond = "";
        $Where = "";
        if($this->Ite_Code) {
          $Where = "WHERE Ite_Code = ".$this->Ite_Code;
          $Query = "UPDATE contentItems SET ";
        } else {
          $Query = "INSERT INTO contentItems SET ";
        }

        $Cond .= "Sec_Code = ".$this->Sec_Code.", ";
        $Cond .= "Ite_BDate = '".date("Y-m-d")."', ";
        $Cond .= "Ite_EDate = '".date("Y-m-d", mktime(0, 0, 0, date("m"), date("d"), date("Y")+1))."', ";
        $Cond .= "Ite_Header = '".mysql_escape_string($this->Data['Ite_Header'])."', ";
        $Cond .= $this->GetProperty("PreMod") ? "Ite_State = 1, " : "Ite_State = 0, ";
        $Cond .= "Str_Code = ".$this->Str_Code.", ";
        if(!$this->Ite_Code)
          $Cond .= "Ite_Time = NOW(), ";
        $Cond .= "Time_Change = NOW(), ";
        $Cond .= "us_id = ".$this->User['us_id'];

        $query = "$Query $Cond $Where";
        $r = db_query($query);
        $Ite_Code = $this->Ite_Code ? $this->Ite_Code : mysql_insert_id();

        if($Ite_Code) {
          @unlink(CACHE_PATH."/".$Ite_Code.".itemvars");
          @unlink(CACHE_PATH."/".$Ite_Code.".itemvars.part");
        }

        foreach($this->Columns as $Col_Code => $ColumnData) {
          $query = "REPLACE INTO contentData{$ColumnData['Col_Type']} SET Ite_Code = $Ite_Code, Col_Code = $Col_Code, Data = '".$ColumnData['Col_Data']."'";
          if($ColumnData['Col_Data'] != "")
            db_query($query);
        }

        if($r) {
          $this->Data['PostOk']  = 1;
          $this->Ins2Php("template_form_ok");
        } else {
          array_push($this->Data['Errors'], array("Error" => "Ошибка базы данных"));
        }
      } else {
        return;
      }
    }

    
    function GetColumns($OnlyAccess=true,$WithData=true) {
      $Columns = array();
      $query = "SELECT * FROM contentColumns WHERE Str_Code=".$this->Str_Code." ORDER BY Col_Order";
      $r = db_query($query);
      if (mysql_num_rows($r)) while ($Column=mysql_fetch_assoc($r)) {
        $Column['Col_Must'] = $Column['Col_Must'] ? 1 : 0;
        $Columns[$Column['Col_Code']] = $Column;
        if ($WithData) {
          if($this->Ite_Code)
            $Columns[$Column['Col_Code']]['Col_Data'] = $this->GetColumnData($Column['Col_Code'],$Column['Col_Type']);
          $Columns[$Column['Col_Code']] += $this->GetList($Column['Lst_Code']);
        }
      }
      return $Columns;
    }
    
    function GetColumnData($Col_Code,$Col_Type) {
      $query  = "SELECT Data FROM contentData{$Col_Type} WHERE Ite_Code={$this->Ite_Code} AND Col_Code={$Col_Code}";
      $r = db_query(sprintf($query,$Col_Type,$Col_Code));
      if ($Col_Type==4) { // мультисписок
        $ColumnsData = array();
        while (list($Data)=mysql_fetch_row($r)) {
          list($ColumnData[]) = $Data; 
        }
      } else {
        list($ColumnData) = (array)@mysql_fetch_row($r);
      }
      return $ColumnData;
    }
    
    function GetList($Lst_Code) {
      $List = array();
      if ($Lst_Code) {
        $query = "SELECT Lst_Name FROM contentLists WHERE Lst_Code={$Lst_Code}";
        $r = db_query($query);
        $List += (array)@mysql_fetch_assoc($r);
        $query = "SELECT * FROM contentListValues WHERE Lst_Code={$Lst_Code} ORDER BY LVa_Order";
        $r = db_query($query);
        while ($Val=@mysql_fetch_assoc($r)) {
          $List['ListValues'][$Val['LVa_Code']] = $Val;
        }
      }
      return $List;
    }

    function Check($Str) {
      $Str = htmlspecialchars($Str);
      $possible_tags = array('<code>', '</code>', '<quote>', '</quote>', '<b>', '</b>', '<i>', '</i>', '<u>', '</u>', '<strike>', '</strike>');
      foreach($possible_tags as $key => $val) {
        $Str = str_replace(htmlspecialchars($val), $val, $Str);
      }
      $Str = preg_replace("/&lt;a href=&quot;(http:\/\/[a-zA-Z0-9\/\?\#\&\._=-]*)&quot;&gt;(.*?)&lt;\/a&gt;/", "<a href=\"/redirect.php?\\1\" target=\"_blank\">\\2</a>", $Str);
      $Str = nl2br($Str);
      return $Str;
    }

    function classContentEdit($sec = "", $Parent = "") {
      parent::classProperties($sec, $Parent);
      $this->Users = $this->GetBlock("Users");
      $this->ContentData = $this->GetBlock("ContentData");
      $this->DoID = $this->GetProperty("DoID");
    }
  }

?>
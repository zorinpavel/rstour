<?

    
    class classContentCart extends classContentData {

        public $Items = array();
        public $Cart, $DoId, $PriceCols;

        public static $isDelivery = false;
        public static $isPayment = false;

        public static $DoID = "cd";
        public static $ItemsID = "ci";

        public static $StaticDiscounts = array(
            1 => array(
                "Name" => "Накопительная скидка",
                "StartSum" => 1000000,
                "Amount" => 10,
            ),
        );
    
        public static $Statuses = array(
            1  => "Поступил",
            2  => "Согласовано с клиентом",
            3  => "Ожидает оплаты",
            4  => "Оплачено",
            5  => "Передан в доставку",
            6  => "Выполнен",
            7  => "Отменен",
        );

        var $Properties=array(
            "public" => array(
                "Names" => array(
                    "MailAdmin",
                    "MailSubject",
                ),
                "Types" => array(
                    "T50",
                    "T50",
                ),
                "Labels" => array(
                    "E-mail администратора",
                    "Тема письма с уведомлением",
                )
            ),
            "private" => array(
                "Names" => array(
                    "PriceCols",
                    "Separator",
                    "SendMail",
                    "template_mail" => "template_mail",
                    "Separator",
                    "template_update_ok",
                    "Separator",
                    "template_top",
                    "template_first",
                    "template_item",
                    "template_sep",
                    "template_bottom",
                    "Separator",
                    "template_empty",
                    "template_orderform",
                    "template_delivery" => "template_delivery",
                    "template_payment" => "template_payment",
                    "template_ordersended",
                ),
                "Types" => array(
                    "InputContentPriceCols",
                    "",
                    "InputYesNo",
                    "template_mail" => "InputTemplates",
                    "",
                    "InputTemplates",
                    "",
                    "InputTemplates",
                    "InputTemplates",
                    "InputTemplates",
                    "InputTemplates",
                    "InputTemplates",
                    "",
                    "InputTemplates",
                    "InputTemplates",
                    "template_delivery" => "InputTemplates",
                    "template_payment" => "InputTemplates",
                    "InputTemplates",
                ),
                "Labels" => array(
                    "Колонки цен",
                    "Параметры отображения",
                    "Отсылать письмо пользователю?",
                    "template_mail" => "Шаблон письма",
                    "Шаблоны вывода",
                    "Товар в корзине",
                    "",
                    "Верх списка",
                    "Первый элемент",
                    "Второй элемент",
                    "Разделитель",
                    "Низ списка",
                    "",
                    "Корзина пуста",
                    "Шаблон 'Форма заказа'",
                    "template_delivery" => "Шаблон 'Выбор системы доставки'",
                    "template_payment" => "Шаблон 'Выбор платежной системы'",
                    "Шаблон 'Заказ отправлен'",
                )
            )
        );
        var $DefaultTemplates = array(
            "template_top" => array(0 => ""),
            "template_bottom" => array(0 => ""),
            "template_first" => array(0 => ""),
            "template_item" => array(0 => ""),
            "template_sep" => array(0 => ""),
            "template_empty"=> array(0 => ""),
            "template_postcard"=> array(0=> ""),
            "template_delivery"=> array(0=> ""),
            "template_payment"=> array(0 => ""),
            "template_orderform"=> array(0 => "
<form action=\"./\">
 <input type=\"hidden\" name=\"<ins type=\"data\" name=\"DoID\">\" value=\"OrderUpdate\">
  <whileins data=\"Params\">
    <ins type=\"data\" name=\"Par_Header\"><ifins exists=\"Par_Required\"> *</ifins>
    <input type=\"text\" name=\"<ins type=\"data\" name=\"Par_Name\">\" value=\"<ins type=\"data\" name=\"Par_Value\">\"<ifins exists=\"Par_Required\"> required</ifins>>
  </whileins>
 <input type=\"submit\">
</form>
          "),
            "template_message"=> array(0 => ""),
            "template_ordersended"=> array(0 => ""),
            "template_update_ok"=> array(0 => ""),
            "template_mail"=> array(0 => ""),
        );

        function GetClassName() {
            return __CLASS__;
        }

        function Action($Do = "") {
            $realDo = $Do ?: $this->DoId;

            $this->GetItems();

            switch($realDo) {
                case 'Update' :
                    $this->DoUpdate();
                    break;
                case 'RemoveItem' :
                    $this->DoRemoveItem();
                    break;
                case 'Order' :
                    $this->DoOrder();
                    break;
                case 'OrderUpdate':
                    $this->DoOrderUpdate();
                    break;
                case 'Delivery':
                    $this->DoDelivery();
                    break;
                case 'DeliveryUpdate':
                    $this->DoDeliveryUpdate();
                    break;
                case 'Payment' :
                    $this->DoPayment();
                    break;
                case 'PaymentUpdate':
                    $this->DoPaymentUpdate();
                    break;
                case 'Send' :
                    $this->DoSend();
                    break;
                case 'Clear' :
                    $this->ClearCartData();
                    break;
                case 'Show':
                default:
                    $this->DoShow();
                    break;
            }

            if(debug()) {
                ?><pre><? print_r($this->Cart); ?></pre><?
                ?><pre><? print_r($this->Data); ?></pre><?
            }

        }


        public function GetItems() {
            $this->Data['TotalSum'] = 0;
            $this->Data['TotalQuantity'] = 0;
            $this->Data['Items'] = array();

            if(count($this->Cart['Items']) <= 0)
                return;

            $IteCond = "Ite_Code IN (".join(array_keys($this->Cart['Items']),", ").")";
            $Date = date("Y-m-d");
            $PeriodCond = "Ite_BDate <= '$Date' AND Ite_EDate >= '$Date'";
            $StateCond = "Ite_State = 0";

            $OrderExpr = "Ite_Priority DESC, Ite_Code DESC";

            $Conds=array();
            if($IteCond) $Conds[]=$IteCond;
            if($StateCond) $Conds[]=$StateCond;
            if($PeriodCond) $Conds[]=$PeriodCond;
            $Conds=join(' AND ',$Conds);
    
            //запрос здесь нужен на случай деактивации товаров и всяких ошибок
            $Items = DB::selectArray("SELECT * FROM contentItems WHERE $Conds ORDER BY $OrderExpr", "Ite_Code");
            foreach($Items as $Ite_Code => $Item) {
                $Item = $this->GetItemVars($Item, true);
                $Item['Quantity'] = $this->Cart['Items'][$Item['Ite_Code']];
                $Item['CartPrice'] = (float) preg_replace('/\s+/','',$Item['ItemCols'][$this->PriceCols[$Item['Str_Code']]]['Value']);
                $Item['Sum'] = (int) $Item['Quantity'] * $Item['CartPrice'];
                $this->Data['TotalSum'] += $Item['Sum'];
                $this->Data['TotalQuantity'] += $Item['Quantity'];
                $this->Data['Items'][$Item['Ite_Code']] = $Item;
            }

            // считаем накопительную скидку
            if($this->User['us_id']) {
                $OrderSum = DB::Query("SELECT SUM(Ord_Sum) FROM cartData WHERE us_id = ".$this->User['us_id']." AND Ord_Status IN (1,3,4,5)");
                foreach(self::$StaticDiscounts as $Key => $Discount) {
                    if($OrderSum >= $Discount['StartSum']) {
                        $this->Data['Discount']['Name'] = $Discount['Name'];
                        $this->Data['Discount']['Amount'] = $Discount['Amount'];
                        $this->Data['Discount']['Sum'] = round($this->Data['TotalSum'] / 100 * $Discount['Amount']);
                        $this->Data['TotalSum'] -= $this->Data['Discount']['Sum'];
                        $this->Cart['Discount'] = $this->Data['Discount'];
                        break;
                    }
                }
            }

            $this->Data['Deliveries'] = DB::selectArray("SELECT * FROM cartDeliverySystems ORDER BY DS_Order DESC, DS_Code", "DS_Code");

            $this->Data['TotalSum'] += $this->Data['Deliveries'][$this->Cart['Delivery']['Delivery_Type']]['DS_Price'];
            $this->Cart['TotalSum'] = $this->Data['TotalSum'];

        }


        function DoUpdate() {
            if(is_array($_REQUEST[self::$ItemsID])) {
                foreach($_REQUEST[self::$ItemsID] as $Ite_Code => $Quantity) {
                    if($Quantity)
                        $this->Cart['Items'][$Ite_Code] = $Quantity;
                    else
                        unset($this->Cart['Items'][$Ite_Code]);
                }
            }

            $this->GetItems();
            $this->DoShow();
        }


        function DoShow() {
            $this->Data['ItemsCount'] = count($this->Data['Items']);
            if(count($this->Data['Items']) > 0) {
                $this->Ins2Php('template_top');

                $template_item = $this->GetTemplate("template_item");
                $template_item = ($template_item ? "template_item" : "template_first");

                foreach($this->Data['Items'] as $i => $Item) {
                    $this->Data = array_merge($this->Data, $Item);
                    if(!$i) {
                        $this->Ins2Php('template_first');
                    } else {
                        $this->Ins2Php($template_item);
                    }

                    if($i != count($this->Data['Items']))
                        $this->Ins2Php('template_sep');

                }
                $this->Ins2Php('template_bottom');
            } else {
                $this->Ins2Php('template_empty');
            }
        }


        function DoOrder() {

            if($this->User['us_id']) {
                $this->DoDelivery();
                return;
            }

            $Params = DB::selectArray("SELECT * FROM cartParams ORDER BY Par_Order DESC", "Par_Code");
            foreach($Params as $Par_Code => $Param) {
                $rParams[$Param['Par_Name']] = mysql_real_escape_string($_REQUEST[$Param['Par_Name']]);
                if(!isset($rParams[$Param['Par_Name']]))
                    $rParams[$Param['Par_Name']] = $this->Data['Params'][$Par_Code]['Par_Value'] ? $this->Data['Params'][$Par_Code]['Par_Value'] : $this->User[$Param['Par_Name']];

                $this->Data['Params'][$Par_Code] = array(
                    'Par_Code'     => $Par_Code,
                    'Par_Header'   => $Param['Par_Header'],
                    'Par_Name'     => $Param['Par_Name'],
                    'Par_Value'    => $rParams[$Param['Par_Name']],
                    'Par_Required' => ($Param['Par_Required'] ? "1" : ""),
                    'Par_Type'     => $Param['Par_Type'],
                );
            }

            $this->Ins2Php("template_orderform");

        }



        function DoOrderUpdate() {
            if($_REQUEST['UserAction'] != "enter") {
                $Params = DB::selectArray("SELECT * FROM cartParams ORDER BY Par_Order", "Par_Code");
                foreach($Params as $Par_Code => $Param) {
                    $rParams[$Param['Par_Name']] = mysql_real_escape_string($_REQUEST[$Param['Par_Name']]);
                    if(!isset($rParams[$Param['Par_Name']]))
                        $rParams[$Param['Par_Name']] = $this->Data['Params'][$Par_Code]['Par_Value'] ? $this->Data['Params'][$Par_Code]['Par_Value'] : $this->User[$Param['Par_Name']];

                    if($Param['Par_Required'] && !strlen($rParams[$Param['Par_Name']]) && !$this->User['us_id'])
                        array_push($this->Data['Errors'], array("Error" => "Не заполнены обязательные поля: ".$Param['Par_Header']));

                    if($Param['Par_Type'] != 3) {
                        $this->Data['Params'][$Par_Code] = array(
                            'Par_Code'     => $Par_Code,
                            'Par_Header'   => $Param['Par_Header'],
                            'Par_Name'     => $Param['Par_Name'],
                            'Par_Value'    => $rParams[$Param['Par_Name']],
                            'Par_Required' => ($Param['Par_Required'] ? "1" : ""),
                            'Par_Type'     => $Param['Par_Type'],
                        );
                        $this->Data[$Param['Par_Name']] = $rParams[$Param['Par_Name']];
                        $this->Cart['Params'][$Param['Par_Name']] = $rParams[$Param['Par_Name']];
                    } else {
                        unset($this->Data['Params'][$Par_Code]);
                    }
                }
            } else {
                $this->Users->CheckValidEnter($_REQUEST['login'], $_REQUEST['email'], $_REQUEST['phone'] ,$_REQUEST['passwd']);
                $this->User = classUsers::$User;
                $this->Data['Errors'] = $this->Users->Data['Errors'];
            }

            if(!count($this->Data['Errors']) && !$this->User['us_id']) {
                $this->Users->CheckValidReg(false);
                $this->User = $this->Users->Data;
                if(count($this->Users->Data['Errors']))
                    $this->Data['Errors'] = $this->Users->Data['Errors'];
                elseif($this->User['us_id'])
                    $this->Users->SetUser($this->User['us_id']);
            } elseif(!count($this->Data['Errors'])) {
                $this->Users->UpdateData();
            }
            $this->Data['Error'] = count($this->Data['Errors']);

            if($this->Data['Error']) {
                $this->DoOrder();
                return;
            }

            if(self::$isDelivery) {
                $this->DoDelivery();
                return;
            }
            if($this->isPayment){
                $this->DoPayment();
                return;
            }
    
            $this->InsertCartOrder();
            $this->DoSend();
    
        }


        function DoDelivery(){
            $this->Cart['Delivery']['Delivery_Type'] = (int)$_REQUEST['Delivery_Type'];

            if($this->Cart['Delivery']['Delivery_Type'])
                $this->Data['Deliveries'][$this->Data['Delivery_Type']]['_checked'] = 1;

            $this->Data = array_merge($this->Data, $this->Users->GetTotalInfo($this->User));

            $this->Ins2Php("template_delivery");
        }


        function DoDeliveryUpdate() {
            $this->Cart['Delivery']['Delivery_Type'] = (int)$_REQUEST['Delivery_Type'];
            if(!$this->Cart['Delivery']['Delivery_Type'])
                array_push($this->Data['Errors'], array("Error" => "Выберите способ доставки"));

            $this->Data['Delivery_Addr'] = mysql_real_escape_string(strip_tags($_REQUEST['Delivery_Addr']));
            $this->Data['Delivery_Place'] = mysql_real_escape_string(strip_tags($_REQUEST['Delivery_Place']));
            $this->Data['Delivery_Name'] = mysql_real_escape_string(strip_tags($_REQUEST['name']));
            $this->Data['Delivery_Mail'] = mysql_real_escape_string(strip_tags($_REQUEST['mail']));
            $this->Data['Delivery_Country'] = mysql_real_escape_string(strip_tags($_REQUEST['Delivery_Country']));
            $this->Data['Delivery_Region'] = mysql_real_escape_string(strip_tags($_REQUEST['Delivery_Region']));
            $this->Data['Delivery_Street'] = mysql_real_escape_string(strip_tags($_REQUEST['Delivery_Street']));
            $this->Data['Delivery_City'] = mysql_real_escape_string(strip_tags($_REQUEST['Delivery_City']));
            $this->Data['Delivery_House'] = mysql_real_escape_string(strip_tags($_REQUEST['Delivery_House']));
            $this->Data['Delivery_Flat'] = mysql_real_escape_string(strip_tags($_REQUEST['Delivery_Flat']));
            $this->Data['Delivery_Phone'] = mysql_real_escape_string(strip_tags($_REQUEST['phone']));
            $this->Data['Delivery_Date'] = mysql_real_escape_string(strip_tags($_REQUEST['Delivery_Date']));
            $this->Data['Delivery_Time'] = mysql_real_escape_string(strip_tags($_REQUEST['Delivery_Time']));
            $this->Data['Delivery_Comment'] = mysql_real_escape_string(strip_tags($_REQUEST['Delivery_Comment']));

            if(!$this->Data['Delivery_Name'])
                array_push($this->Data['Errors'], array("Error" => "Укажите ваше имя"));
            if(!$this->Data['Delivery_Mail'])
                array_push($this->Data['Errors'], array("Error" => "Укажите ваш e-mail"));
            if(!$this->Data['Delivery_City'])
                array_push($this->Data['Errors'], array("Error" => "Укажите город доставки"));
            if(!$this->Data['Delivery_Street'] || !$this->Data['Delivery_House'])
                array_push($this->Data['Errors'], array("Error" => "Укажите адрес доставки"));
            if(!$this->Data['Delivery_Phone'])
                array_push($this->Data['Errors'], array("Error" => "Укажите телефон для доставки"));

            $this->Cart['Delivery']['Delivery_Type'] = $this->Data['Delivery_Type'];
            $this->Cart['Delivery']['Delivery_Addr'] = $this->Data['Delivery_Addr'];
            $this->Cart['Delivery']['Delivery_Place'] = $this->Data['Delivery_Place'];
            $this->Cart['Delivery']['Delivery_Name'] = $this->Data['Delivery_Name'];
            $this->Cart['Delivery']['Delivery_Mail'] = $this->Data['Delivery_Mail'];
            $this->Cart['Delivery']['Delivery_Country'] = $this->Data['Delivery_Country'];
            $this->Cart['Delivery']['Delivery_Region'] = $this->Data['Delivery_Region'];
            $this->Cart['Delivery']['Delivery_City'] = $this->Data['Delivery_City'];
            $this->Cart['Delivery']['Delivery_Street'] = $this->Data['Delivery_Street'];
            $this->Cart['Delivery']['Delivery_House'] = $this->Data['Delivery_House'];
            $this->Cart['Delivery']['Delivery_Flat'] = $this->Data['Delivery_Flat'];
            $this->Cart['Delivery']['Delivery_Phone'] = $this->Data['Delivery_Phone'];
            $this->Cart['Delivery']['Delivery_Date'] = $this->Data['Delivery_Date'];
            $this->Cart['Delivery']['Delivery_Time'] = $this->Data['Delivery_Time'];
            $this->Cart['Delivery']['Delivery_Comment'] = $this->Data['Delivery_Comment'];
            if($this->Data['Delivery_Type'])
                $this->Cart['Delivery'] = array_merge($this->Cart['Delivery'], $this->Data['Deliveries'][$this->Data['Delivery_Type']]);

            $this->Data = array_merge($this->Data, $this->Cart['Delivery']);

            if(!count($this->Data['Errors']) && !$this->User['us_id']) {
                $this->Users->CheckValidReg(false);
                if(count($this->Users->Data['Errors']))
                    $this->Data['Errors'] = $this->Users->Data['Errors'];
                elseif($this->User['us_id'])
                    $this->Users->SetUser($this->User['us_id']);
            } elseif(!count($this->Data['Errors'])) {
                $this->Users->UpdateData();
            }
            $this->Data['Error'] = count($this->Data['Errors']);

            if($this->Data['Error']) {
                $this->DoDelivery();
                return;
            }

            if($this->isPayment) {
                $this->DoPayment();
                return;
            }

            $this->DoSend();
        }


        function DoPayment() {
            if(!$this->Data['DS_SelfPayment'])
                $Where = " WHERE PS_SelfPayment != 1";

            $this->Data['Payments'] = DB::selectArray("SELECT * FROM cartPaymentSystems $Where ORDER BY PS_Order DESC, PS_Code", "PS_Code");
            if(!$this->Cart['Ord_Code']) {
                $OrderLast = DB::Query("SHOW TABLE STATUS LIKE 'cartData'");
                $this->Cart['Ord_Code'] = $this->Data['Ord_Code'] = $OrderLast['Auto_increment'];
            } else
                $this->Data['Ord_Code'] = $this->Cart['Ord_Code'];

            $this->Data = array_merge($this->Data, $this->Cart['Payment']);
            if($this->Data['Payment_Type'])
                $this->Data['Payments'][$this->Data['Payment_Type']]['_checked'] = 1;

            $this->Ins2Php('template_payment');
        }


        function DoPaymentUpdate(){
            $this->Data['Payments'] = DB::selectArray("SELECT * FROM cartPaymentSystems ORDER BY PS_Order DESC, PS_Code", "PS_Code");

            $this->Data['Payment_Type'] = (int)$_REQUEST['Payment_Type'];
            if(!$this->Data['Payment_Type'])
                array_push($this->Data['Errors'], array("Error" => "Выберите способ оплаты"));

            $this->Cart['Payment']['Payment_Type'] = $this->Data['Payment_Type'];
            if($this->Data['Payment_Type'])
                $this->Cart['Payment'] = array_merge($this->Cart['Payment'], $this->Data['Payments'][$this->Data['Payment_Type']]);

            $this->Data = array_merge($this->Data, $this->Cart['Payment']);

            $this->Data['Error'] = count($this->Data['Errors']);

            if($this->Data['Error']) {
                $this->DoPayment();
                return;
            } else
                $this->InsertCartOrder();

            $this->DoSend();
        }


        function DoSend() {
            if(count($this->Cart['Items'])) {
                /* отправялем уведомление пользователю */
                $Mail = _autoload("classMail");
    
                $Mail->Data['Subject'] = "Заказ №".$this->Cart['Ord_Code']." на сайте ".DOMAIN_NAME;
                $Mail->Data['Message'] = "";
    
                $Mail->Data = array_merge($Mail->Data, $this->Cart);
                $Mail->Data = array_merge($Mail->Data, $this->Cart['User']);
    
                $OrderTemplate = $this->GetTemplate("template_mail");
                $OrderHtml = $Mail->Template2Php($OrderTemplate);
                $Mail->Data['Message'] = $OrderHtml;
    
                $Template = $Mail->GetTemplate("mailTemplate");
                $MessageHtml = $Mail->Template2Php($Template);
    
                $Emogrifier = new Emogrifier();
                $Emogrifier->setHtml($MessageHtml);
                $Emogrifier->setCss(classMail::getCss(DOMAIN_URL."/jas/mailcart.css"));
                $MessageHtml = $Emogrifier->emogrifyBodyContent();
    
                if($this->User['mail'])
                    $Mail->SendMail($Mail->Data['User']['mail'], $Mail->Data['Subject'], $MessageHtml);
                $Mail->SendMail($this->GetProperty("MailAdmin"), $this->GetProperty("MailSubject")." ".$this->Cart['Ord_Code'], $MessageHtml);
    
                if(count($Mail->Errors)) $this->Data['Errors'] = $Mail->Errors;
    
                $this->Data['Error'] = count($this->Data['Errors']);
            }
            
            $this->Ins2Php('template_ordersended');
            $this->ClearCartData();
        }
        
        
        public function ClearCartData() {
            unset($_SESSION['Cart']);
        }


        function InsertCartOrder() {
            $this->Data['Ord_Status'] = $this->Data['Ord_Status'] ?: 1; // Поступил
            $this->Data['Status_Name'] = self::$Statuses[$this->Data['Ord_Status']];
    
            $Time = "";
            $Order = "";
            if(!$this->Cart['Ord_Code'])
                $Time = "Ord_Time = NOW(),";
            else
                $Order = ", Ord_Code = ".$this->Cart['Ord_Code'];
    
            foreach($this->Data['Items'] as $Key => $Item) {
                unset($Item['ItemCols']);
                $this->Cart['ItemsData'][$Key] = $Item;
            }
            
            unset($this->Cart['Delivery']['DS_Data']);
            unset($this->Cart['Payment']['PS_Data']);

            $this->Cart['User'] = $this->User;
            unset($this->Cart['User']['Cart']);

            $Str = json_encode($this->Cart);

            $query = "REPLACE INTO cartData SET
                   Ord_Change = NOW(),
                   $Time
                   $Order
                   Ord_Status = ".$this->Data['Ord_Status'].",
                   Ord_Sum = ".$this->Cart['TotalSum'].",
                   us_id = ".$this->User['us_id'].",
                   Ord_Data = '".mysql_real_escape_string($Str)."'";
            
            $r = DB::Query($query);
            $this->Cart['Ord_Code'] = $this->Data['Ord_Code'] = mysql_insert_id();

            if(!$r)
                array_push($this->Data["Errors"], array("Error" => "Ошибка добавления заказа в базу данных! Свяжитесь с администрацией сайта: ".$this->GetProperty('MailAdmin')));
        }


        function classContentCart($sec = "", $Parent = "") {
            parent::classContentData($sec, $Parent);
            
            if(!self::$isDelivery)
                unset($this->Properties['private']['Names']['template_delivery']);
            if(!self::$isPayment)
                unset($this->Properties['private']['Names']['template_payment']);
            if(!$this->GetProperty("SendMail"))
                unset($this->Properties['private']['Names']['template_mail']);
    
    
            if(!isset($_SESSION['Cart']))
                $_SESSION['Cart'] = array();
            $this->Cart = &$_SESSION['Cart'];

            $this->Cart['Delivery'] = $this->Cart['Delivery'] ?: array();
            $this->Cart['Payment'] = $this->Cart['Payment'] ?: array();

            $this->Data = array_merge($this->Data, $this->Cart['Delivery']);
            $this->Data = array_merge($this->Data, $this->Cart['Payment']);

            $this->Data['DoID'] = self::$DoID;
            $this->Data['DoId'] = $this->DoId = $_REQUEST[self::$DoID];

            $this->PriceCols = $this->GetProperty("PriceCols");

            $this->Data['Errors'] = array();
            $this->Data['Messages'] = array();

        }


    }

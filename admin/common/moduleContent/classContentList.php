<?php

    class classContentList extends classProperties {

        var $ContentData;
        var $ListCount;
        var $ShowScroll;
        var $Items, $Groups;
        var $NoUseIteID;
        var $Structures;
        
        var $Properties = array(
            "public" => array(
                "Names" => array(
                ),
                "Types" => array(
                ),
                "Labels" => array(
                )
            ),
            "private" => array(
                "Names" => array(
                    "ContentData",
                    "ListCount",
                    "ShowScroll",
                    "OverrideParent",
                    "UseIteID",
                    "UseDate",
                    "Separator",
                    "template_top",
                    "template_bottom",
                    "Separator",
                    "template_list_top"=>"template_list_top",
                    "template_list_element1"=>"template_list_element1",
                    "template_list_element2"=>"template_list_element2",
                    "template_list_element3"=>"template_list_element3",
                    "template_list_element4"=>"template_list_element4",
                    "template_list_element5"=>"template_list_element5",
                    "template_list_sep"=>"template_list_sep",
                    "template_list_bottom"=>"template_list_bottom",
                    "GroupSep"=>"Separator",
                    "template_group_header"=>"template_group_header",
                    "template_group_footer"=>"template_group_footer",
                    "template_group_sep"=>"template_group_sep",
                    "SectionSep"=>"Separator",
                    "template_section_header"=>"template_section_header",
                    "template_section_footer"=>"template_section_footer",
                    "template_section_sep"=>"template_section_sep",
                ),
                "Types" => array(
                    "InputSectionsContentData",
                    "T50",
                    "InputYesNo",
                    "InputYesNo",
                    "InputYesNo",
                    "InputYesNo",
                    "",
                    "InputTemplates",
                    "InputTemplates",
                    "",
                    "template_list_top"=>"InputTemplates",
                    "template_list_element1"=>"InputTemplates",
                    "template_list_element2"=>"InputTemplates",
                    "template_list_element3"=>"InputTemplates",
                    "template_list_element4"=>"InputTemplates",
                    "template_list_element5"=>"InputTemplates",
                    "template_list_sep"=>"InputTemplates",
                    "template_list_bottom"=>"InputTemplates",
                    "GroupSep"=>"",
                    "template_group_header"=>"InputTemplates",
                    "template_group_footer"=>"InputTemplates",
                    "template_group_sep"=>"InputTemplates",
                    "SectionSep"=>"",
                    "template_section_header"=>"InputTemplates",
                    "template_section_footer"=>"InputTemplates",
                    "template_section_sep"=>"InputTemplates",
                ),
                "Labels" => array(
                    "Источник данных",
                    "Кол-во (0-все)",
                    "Выводить разбиение на страницы",
                    "Переопределять параметры родителя",
                    "Не учитывать IteID",
                    "Использовать дату",
                    "Шаблоны оформления блока",
                    "Шапка блока",
                    "Подвал блока",
                    "Шаблоны оформления списка",
                    "template_list_top"=>"Верх списка",
                    "template_list_element1"=>"Первый элемент",
                    "template_list_element2"=>"Второй элемент",
                    "template_list_element3"=>"Третий элемент",
                    "template_list_element4"=>"Четвертый элемент",
                    "template_list_element5"=>"Пятый элемент",
                    "template_list_sep"=>"Разделитель",
                    "template_list_bottom"=>"Низ списка",
                    "GroupSep"=>"Шаблоны оформления групп по типам контента",
                    "template_group_header"=>"Шапка группы",
                    "template_group_footer"=>"Подвал группы",
                    "template_group_sep"=>"Разделитель групп",
                    "SectionSep"=>"Шаблоны оформления групп по разделам сайта",
                    "template_section_header"=>"Шапка раздела",
                    "template_section_footer"=>"Подвал раздела",
                    "template_section_sep"=>"Разделитель разделов",
                )
            )
        );


        var $DefaultTemplates = array(
            "template_top" => array(0 => ""),
            "template_bottom" => array(0 => ""),
            "template_list_top" => array(0 => ""),
            "template_list_element1" => array(0 => ""),
            "template_list_element2" => array(0 => ""),
            "template_list_element3" => array(0 => ""),
            "template_list_element4" => array(0 => ""),
            "template_list_element5" => array(0 => ""),
            "template_list_sep" => array(0 => ""),
            "template_list_bottom" => array(0 => ""),
            "template_group_header"=> array(0 => ""),
            "template_group_footer"=> array(0 => ""),
            "template_group_sep"=> array(0 => ""),
            "template_section_header"=> array(0 => ""),
            "template_section_footer"=> array(0 => ""),
            "template_section_sep"=> array(0 => ""),
        );


        function GetClassName() {
            return "classContentList";
        }

        function Action() {
            if($this->NoUseIteID)
                $this->ContentData->Ite_Code = "";
            $this->ContentData->GetContentList($this->ListCount);
            $this->Items = &$this->ContentData->Items;
            $this->Groups = &$this->ContentData->Groups;
            
            $this->Data['ItemsCount'] = count($this->Items);
            $this->Data['Quantity'] = $this->ContentData->ListScroll->Data['Quantity'];
            
            $this->Ins2Php("template_top");
            //Выводим список элементов
            //Сначала надо посмотреть нет ли группировок
            if($this->ContentData->Sections['Group'] || $this->ContentData->Structures['Group']) {
                $this->ShowGroups($this->Groups);
            } elseif(count($this->Items)) {
                $this->ShowList($this->Items);
            } else {
                // если включен фильтр и ни чего не найдено
                if($this->ContentData->ContentFilter)
                    $this->ContentData->ContentFilter->Ins2Php("template_error");
                $this->Data['EmptyFound'] = 1;
            }
            
            $this->Ins2Php("template_bottom");
        }

        function ShowList(&$Items) {
            if(!count($Items)) return;
//            $template_list_element2 = $this->GetTemplate("template_list_element2");
//            $template_list_element2 = ($template_list_element2 ? "template_list_element2" : "template_list_element1");

            if($this->ContentData->DataType == 3)
                $this->Data = array_merge($this->Data, $this->GetSectionInfo($this->ContentData->Sec_Code));

            $this->Ins2Php('template_list_top');
            $i = 0;
            $iCount = count($Items);
            $Parity = 0;
            foreach($Items as $Key => $Item) {
                $this->Data = array_merge($this->Data,$Item);
                $this->Data['Id'] = $this->ContentData->GetProperty('IteID');
                $i++;
                $this->Data['_number'] = $i;
                $this->Data['_parity'] = $Parity;

                if($this->ItemTemplate[$i]) {
                    $this->Ins2Php($this->ItemTemplate[$i]);
                } else {
                    $this->Ins2Php($this->ItemTemplate[1]);
                }

                if($i != $iCount)
                    $this->Ins2Php('template_list_sep');
                $Parity = $Parity ? 0 : 1;
            }
            $this->Ins2Php('template_list_bottom');
        }

        function ShowGroups(&$Groups) {
            if(isset($Groups['Str_Code']) && is_array($Groups['Str_Code'])) {
                //Значит по структурам
                $gCount=count($Groups['Str_Code']);$i=0;
                foreach($Groups['Str_Code'] as $Key=>$Structure) {
                    $this->Data = array_merge($this->Data, $this->GetStructureInfo($Structure['Str_Code']));
                    $this->Ins2Php('template_group_header');

                    $this->ShowGroups($Groups['Str_Code'][$Key]);

                    $this->Data = array_merge($this->Data, $this->GetStructureInfo($Structure['Str_Code']));
                    $this->Ins2Php('template_group_footer');
                    if(++$i!=$gCount) $this->Ins2Php('template_group_sep');
                };
            } elseif(isset($Groups['Sec_Code']) && is_array($Groups['Sec_Code'])) {
                //Значит по разделам
                $gCount=count($Groups['Sec_Code']);$i=0;
                foreach($Groups['Sec_Code'] as $Key=>$Section) {
                    $this->Data=array_merge($this->Data,$this->GetSectionInfo($Section['Sec_Code']));
                    $this->Ins2Php('template_section_header');

                    $this->ShowGroups($Groups['Sec_Code'][$Key]);

                    $this->Data=array_merge($this->Data,$this->GetSectionInfo($Section['Sec_Code']));
                    $this->Ins2Php('template_section_footer');
                    if(++$i!=$gCount) $this->Ins2Php('template_section_sep');
                };
            } elseif(isset($Groups['Items']) && is_array($Groups['Items'])) {
                $this->ShowList($Groups['Items']);
            };
        }

        function GetSectionInfo($Sec_Code) {
            $Sec_Code=(int) $Sec_Code;
            static $Sections=array();
            if(isset($Sections[(string) $Sec_Code])) return $Sections[(string) $Sec_Code];
            $Sections[$Sec_Code] = array("Sec_Code"=>$Sec_Code);
            if((int)$Sec_Code) {

                $Page = new classPage($Sec_Code);
                $PageSignature = $Page->GetProperty("page_signature");
                $PageName = $Page->GetProperty("page_name");

                $Sections[$Sec_Code]['Sec_Name'] = $PageName;
                $Sections[$Sec_Code]['Sec_Signature'] = $PageSignature;
                $Sections[$Sec_Code]['Sec_Path'] = $this->ContentData->GetPath($Sec_Code);
            } else {
                $Sections[$Sec_Code]['Sec_Name']='Главная страница';
                $Sections[$Sec_Code]['Sec_Path']='/';
            };
            return $Sections[$Sec_Code];
        }

        function GetSectionID() {
            if (is_object($this->Parent)) return parent::GetSectionID();
            else return $this->Parent;
        }

        function GetStructureInfo($Str_Code) {
            $Str_Code=(int) $Str_Code;
            static $Structures=array();
            if(isset($Structures[(string) $Str_Code])) return $Structures[(string) $Str_Code];
            $Structures[$Str_Code]=array("Str_Code"=>$Str_Code);
            if((int) $Str_Code) {
                $r=mysql_query('select Str_Name from contentStructure where Str_Code='.$Str_Code);
                if(!mysql_num_rows) return(array());
                list($Str_Name)=mysql_fetch_row($r);
                $Structures[$Str_Code]['Str_Name']=$Str_Name;
            };
            return $Structures[$Str_Code];
        }

        function dPrintScroll() {
            if ($this->ContentData->SectionID && !$this->ContentData->NoListScroll) { // т.е. ContentData должен быть реальным блоком
                $this->ContentData->ListScroll->Action();
            }
        }

        function classContentList($sec = "", $Parent = "") {
            parent::classProperties($sec, $Parent);

            $this->ItemTemplate = array("1" => "template_list_element1");
            for ($i = 2; $i <= 5; $i++) {
                $Template = $this->GetTemplate("template_list_element$i");
                if($Template)
                    $this->ItemTemplate[$i] = "template_list_element$i";
                else
                    break;
            }

            //Для определения параметров посмотрим есть ли родитель и состояние флага переопределения
            if(!$this->GetProperty('OverrideParent') && is_object($Parent) && $Parent->GetClassName() == "classContent") {
                $this->ContentData = $Parent->GetBlock("ContentData");
                $this->ListCount = $Parent->GetProperty("ListCount");
                $this->ShowScroll = $Parent->GetProperty("ShowScroll");
            } else {
                $this->ContentData = $this->GetBlock("ContentData");
                $this->ListCount = $this->GetProperty("ListCount");
                $this->ShowScroll = $this->GetProperty("ShowScroll");
            }

            $this->NoUseIteID = $this->GetProperty("UseIteID");
            // если это связанные объекты
            if($this->ContentData->DataType == 9)
                $this->NoUseIteID = 0;

            if($this->GetProperty('OverrideParent')) {
                if($this->ContentData->Structures && !$this->ContentData->Structures['Group']) {
                    unset($this->Properties['private']['Names']['GroupSep']);
                    unset($this->Properties['private']['Names']['template_group_sep']);
                    unset($this->Properties['private']['Names']['template_group_header']);
                    unset($this->Properties['private']['Names']['template_group_footer']);
                }
                if(!$this->ContentData->Sections['Group']) {
                    unset($this->Properties['private']['Names']['SectionSep']);
                    unset($this->Properties['private']['Names']['template_section_sep']);
                    unset($this->Properties['private']['Names']['template_section_header']);
                    unset($this->Properties['private']['Names']['template_section_footer']);
                }
            };

            if($this->GetProperty("UseDate")) {
                $this->ContentData->CurDay = (int)$_REQUEST['cd'];
                $this->ContentData->CurMonth = (int)$_REQUEST['cm'];
                $this->ContentData->CurYear = (int)$_REQUEST['cy'];
            }

        }

    }

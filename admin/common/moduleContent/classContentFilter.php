<?php

    class classContentFilter extends classProperties {
        
        public $FilterColId, $FilterColCode, $FilterData, $FilterID;

        var $Properties=array(
            "public" => array(
                "Names" => array(
                ),
                "Types" => array(
                ),
                "Labels" => array(
                ),
            ),
            "private" => array(
                "Names" => array(
                    "PriceFilter",
                    "PriceFilterStep" => "PriceFilterStep",
                    "FilterColId" => "FilterColId",
                    "FilterData" => "FilterData",
                    "FilterID",
                    "UseSection",
                    "template_select",
                ),
                "Types" => array(
                    "InputYesNo",
                    "PriceFilterStep" => "T20",
                    "FilterColId" => "InputFilterCols",
                    "FilterData" => "InputFilterData",
                    "T20",
                    "InputYesNo",
                    "InputTemplates",
                ),
                "Labels" => array(
                    "Фильтровать по цене",
                    "PriceFilterStep" => "Шаг <small>(руб.)</small>",
                    "FilterColId" => "Колонка",
                    "FilterData" => "Список",
                    "ID Фильтра",
                    "Только из текущего раздела",
                    "Шаблон вывода:",
                )
            )
        );

        var $DefaultTemplates = array(
            "template_select" => array(0 => ""),
        );

        function GetClassName() {
            return __CLASS__;
        }

        function GetRequestFilters($Structures) {
            $Filters = array();
            $IDs = DB::selectArray("SELECT Col_Id FROM contentColumns WHERE (Col_Type = 1 OR Col_Type = 3 OR Col_Type = 31 OR Col_Type = 4 OR Col_Type = 10 OR Col_Type = 11) AND Str_Code IN (".join(',',$Structures).")");
            foreach($IDs as $ColID) {
                foreach($_REQUEST as $Key => $Value) {
                    if($ColID['Col_Id'] == $Key && $Value != "")
                        $Filters[$ColID['Col_Id']] = preg_replace("/\W/i", "", $_REQUEST[$ColID['Col_Id']]);
                }
            }
            
            return $Filters;
        }

        function GetFilterHeader() {
            /* берем из названия поля */
            return DB::selectValue("SELECT Col_Header FROM contentColumns WHERE Col_Code = '".$this->FilterColId."'");
            
            /* берем из названия списка */
//            return DB::selectValue("SELECT Lst_Name FROM contentLists WHERE Lst_Code = '".$this->FilterData."'");return $Name;
        }

        
        function Action() {
            $FilterID = $this->Data['FilterID'] = $this->FilterID;

            $this->Data['Filters'] = array();

            $Sec_Code = $this->GetSectionID();
            $Col_Code = $this->FilterColId;

            if($this->GetProperty("PriceFilter")) {

                $Items = DB::selectArray("SELECT ci.Ite_Code, cd1.Data as Price FROM contentItems as ci
                                             LEFT JOIN contentData1 as cd1
                                              ON cd1.Ite_Code = ci.Ite_Code AND cd1.Col_Code = $Col_Code
                                             WHERE ci.Sec_Code = $Sec_Code", "Ite_Code");

                $step = $this->GetProperty("PriceFilterStep");
                $max = $maxv = 0;
                $min = 9999999;
                foreach($Items as $Ite_Code => $Data) {
                    if($Data['Price'] < $min) {
                        $min = floor($Data['Price']/$step) * $step;
                    }
                    if($Data['Price'] > $max) {
                        $max = ceil($Data['Price']/$step) * $step;
                    }
                }
                $this->Data['MinPrice'] = $min;
                $this->Data['MaxPrice'] = $max;
                $this->Data['Step']     = $step;

                $cnt = ceil(($max - $min)/($step * 5));
                $rstep = floor( ($max - $min)/($step * $cnt) ) * $step;
                for($i = 1; $i <= $cnt; $i++) {
                    if($maxv >= $max || $i == $cnt)
                        $maxv = $max;
                    else
                        $maxv = $min + $rstep;
                    $Result[$i]['LVa_Header'] = "$min - $maxv";
                    $Result[$i]['LVa_Value'] = "$min - $maxv";
                    $min = $min + $rstep;
                }
            } else {
                if($this->GetProperty("UseSection")) {
                    $Result = DB::selectArray("SELECT clv.* FROM contentItems AS ci
                                    LEFT JOIN contentData3 AS cd3
                                     ON cd3.Ite_Code = ci.Ite_Code
                                    LEFT JOIN contentData4 AS cd4
                                     ON cd4.Ite_Code = ci.Ite_Code
                                    LEFT JOIN contentListValues AS clv
                                     ON (clv.LVa_Code = cd3.Data OR clv.LVa_Code = cd4.Data) AND clv.Lst_Code = ".$this->FilterData."
                                    WHERE
                                     ci.Sec_Code = $Sec_Code
                                  ", "LVa_Code");
                } else {
                    $Result = DB::selectArray("SELECT * FROM  contentListValues WHERE Lst_Code = ".$this->FilterData, "LVa_Code");
                }
            }

            // удаляем "нулевой" фильтр
            unset($Result['null']);

            foreach($Result as $Code => $Filter) {
                //      list($Items3) = mysql_fetch_row(db_query("SELECT count(cd3.Ite_Code) FROM contentData3 AS cd3 WHERE cd3.Col_Code = $Col_Code AND Data = ".$Filter['LVa_Value']));
                //      list($Items4) = mysql_fetch_row(db_query("SELECT count(cd4.Ite_Code) FROM contentData4 AS cd4 WHERE cd4.Col_Code = $Col_Code AND Data = ".$Filter['LVa_Value']));
                //      echo "$Items3 $Items4<br>";
                //      $Cnt = $Items3 + $Items4;
                $this->Data['Filters'][$Code] = array (
                    "Code" => $Code,
                    "Name" => $Filter['LVa_Header'],
                    "Value" => $Filter['LVa_Value'],
                    "Link" => GetLink($FilterID, $Filter['LVa_Value'], array("id")),
                    "order" => $Filter['LVa_Order']
                );
                
                $this->Data['Filters'][$Code]['current'] = ($Filter['LVa_Value'] == $_REQUEST[$FilterID]);
                
                if(is_array($_REQUEST[$FilterID])) {
                    foreach($_REQUEST[$FilterID] as $rCode => $rValue) {
                        if($Filter['LVa_Value'] == $rValue) {
                            $this->Data['Filters'][$Code]['current'] = 1;
                            break;
                        }
                    }
                }
            }

            $this->Data['Filters'] = sortArray($this->Data['Filters'], array("order", "DESC", "Code", "DESC"));

            $this->Data['Header'] = $this->GetFilterHeader();
            $this->Data['All_Link'] = GetLink($FilterID, "", array("id"));

            $this->Ins2Php("template_select");
            
            if(debug()) {
                ?><pre><? print_r($this->Data); ?></pre><?
            }
            
        }

        
        function classContentFilter($sec = "", $Parent = "") {
            parent::classProperties($sec, $Parent);

            if(!$this->GetProperty("PriceFilter")) {
                unset($this->Properties['private']['Names']['PriceFilterStep']);
            } else {
                $this->Properties['private']['Values']['UseSection'] = 1;
                $this->Properties['private']['Values']['FilterID'] = "price";
                unset($this->Properties['private']['Names']['FilterData']);
            }

            $this->FilterColId = $this->GetProperty("FilterColId");
            $this->FilterData = $this->GetProperty("FilterData");
            $this->FilterID = $this->GetProperty("FilterID");
    
        }

    }

<?php

    class classContent extends classProperties {
        var $ContentData;
        var $Items, $Groups;
        var $Structures;
        var $Properties=array(
            "public" => array(
                "Names" => array(
                ),
                "Types" => array(
                ),
                "Labels" => array(
                )
            ),
            "private" => array(
                "Names" => array(
                    "ContentData",
                    "template_item",
                    "template_empty",
                    "ShowOnlyOne",
                    "ShowFirst",
                    "SetError",
                    "Separator",
                    "ListCount",
                    "ShowScroll",
                    "ContentList",
                    "ContentListShort",
                    "ContentListRelated",
                ),
                "Types" => array(
                    "InputSectionsContentData",
                    "InputTemplates",
                    "InputTemplates",
                    "InputYesNo",
                    "InputYesNo",
                    "InputYesNo",
                    "",
                    "T50",
                    "InputYesNo",
                    "InputSectionsContentList",
                    "InputSectionsContentList",
                    "InputSectionsContentList",
                ),
                "Labels" => array(
                    "Источник данных",
                    "Элемент целиком",
                    "Ни чего не найдено",
                    "Если один элемент, то выводить его?",
                    "Если много выводить первую?",
                    "Если не найдено отдавать 404?",
                    "Оформление списка элементов",
                    "Кол-во (0-все)",
                    "Выводить разбиение на страницы",
                    "Список объектов",
                    "Список объектов краткий",
                    "Список связанных объектов",
                )
            )
        );


        var $DefaultTemplates = array(
            "template_item" => array(0 => "<h3><ins type=\"data\" name=\"Ite_Header\"></h3>\n
<ifins exists=\"anounce\"><p><ins type=\"data\" name=\"anounce\"></p></ifins>\n
<ifins exists=\"html\"><p><ins type=\"data\" name=\"html\"></p></ifins>"),
            "template_empty" => array(0 => ""),
        );


        function GetClassName() {
            return __CLASS__;
        }

        function Action() {
            if($this->ContentEdit->Action) {
                $this->ContentEdit->Action();
                return; // если редактируем, то classContent больше ничего не выводит!
            }
            if(@$this->ContentEdit)
                $this->ContentList->Data += (array)@$this->ContentEdit->Data_ContentList;

            $ShowFirst = $this->GetProperty("ShowFirst");

            if($this->GetProperty("ShowOnlyOne") || $this->ContentData->Ite_Code || $ShowFirst) {
                $this->ContentData->GetContentList($this->GetProperty("ListCount"));
                $this->Items = $this->ContentData->Items;
                $this->Groups = $this->ContentData->Groups;
                $ItemsCount = count($this->Items);
                
                if($ItemsCount == 1 || ($ShowFirst && $ItemsCount > 0) ) {

                    if($ShowFirst) {
                        reset($this->Items);
                        $this->Items[] = $this->ContentData->GetItemVars(current($this->Items), 0);
                    }

                    //Показываем единстенный элемент целиком
                    $this->Data = array_merge($this->Data, array_pop($this->Items));
                    
                    $this->setBuffered("page_title", $this->Data['Ite_Header']);

                    if(classUsers::$User['us_id'])
                        DB::Query("REPLACE INTO contentViews SET us_id = ".classUsers::$User['us_id'].", Ite_Code = ".$this->Data['Ite_Code']);
                    else {
                        $contentViews = json_decode($_COOKIE['contentViews'], true);
                        $_COOKIE['contentViews'] = $contentViews ?: array();
                        $_COOKIE['contentViews'][$this->Data['Ite_Code']] = time();
                        setcookie("contentViews", json_encode($_COOKIE['contentViews']), time() + (86400 * 300), "/", ".".DOMAIN_NAME);
                    }

                    $this->Ins2Php("template_item");

                    if(debug()) {
                        ?><pre><? print_r($this->Data); ?></pre><?
                    }
                    /* тут делаются дополнительные запросы,  но их быть не должно */

                    if(is_object($this->ContentListShort) && ($this->ContentData->Ite_Code || $ShowFirst) ) {
                        if($this->ContentData->Ite_Code)
                            $this->ContentListShort->ContentData->Ite_Code = "!".$this->ContentData->Ite_Code;
                        if($ShowFirst)
                            $this->ContentListShort->ContentData->Ite_Code = "!".$this->Data['Ite_Code'];
                        $this->ContentListShort->UseIteID = 1;

                        $this->ContentListShort->Action();
                    }

                    if(is_object($this->ContentListRelated) && ($this->ContentData->Ite_Code || $ShowFirst) ) {
                        $this->ContentListShort->UseIteID = 1;

                        $this->ContentListRelated->Action();
                    }

                }
            } else
                $ItemsCount = 0;

            if($ItemsCount != 1) {
                //Выводим список элементов
                if(is_object($this->ContentList)) {
                    if(!$this->GetProperty("ShowOnlyOne"))
                        $this->ContentList->UseIteID = 1;
                    $this->ContentList->Action();
                }
            }

            if($ItemsCount != 1 && $this->ContentData->Ite_Code) {
                if($this->GetProperty("SetError")) {
                    header("HTTP/1.1 404 Not Found");
                    header("Status: 404 Not Found");
//                    die;
                } else
                    $this->Ins2Php("template_empty");
            }

        }

        function GetSectionID() {
            if (is_object($this->Parent))
                return parent::GetSectionID();
            else
                return $this->Parent;
        }

        function classContent($sec = "", $Parent = "") {
            parent::classProperties($sec, $Parent);
            $this->ContentData = $this->GetBlock("ContentData");
            $this->ContentList = $this->GetBlock("ContentList");
            $this->ContentListShort = $this->GetBlock("ContentListShort");
            $this->ContentListRelated = $this->GetBlock("ContentListRelated");
            $this->ContentEdit = $this->GetBlock("ContentEdit");
        }

    }

<?php

    define("INS_VARIABLE", "variable");
    define("INS_FUNCTION", "function");
    define("INS_BLOCK", "block");
    define("INS_DATA", "data");
    define("INS_INCLUDE", "include");
    define("INS_BUFFERED", "buffered");

    define("TAG_INS", "ins");
    define("TAG_IFINS", "ifins");
    define("TAG_WHILEINS", "whileins");

    define("INS_PAGE_NORMAL", 0);
    define("INS_PAGE_SECTION", 1);
    define("INS_PAGE_REDIRECT", 2);
    define("INS_PAGE_NONE", 3);

    class classProperties {
        //    var $Properties = array("public" => array("Names" => array(), "Types" => array(), "Labels" => array()), "private" => array("Names" => array(), "Types" => array(), "Labels" => array()));
        var $Data = array();

        var $WhileContent = array();
        var $WhileCnt = 0;

        var $Blocks = array();
        var $Errors = array();

        public $SectionID;
        public $Parent;
        
        static $Buffered;
        
        public static $RusMonths = array(
            "01" => "Января",
            "02" => "Февраля",
            "03" => "Марта",
            "04" => "Апреля",
            "05" => "Мая",
            "06" => "Июня",
            "07" => "Июля",
            "08" => "Августа",
            "09" => "Сентября",
            "10" => "Октября",
            "11" => "Ноября",
            "12" => "Декабря",
        );

        public static $RusMonthsInt = array(
            1  => "Января",
            2  => "Февраля",
            3  => "Марта",
            4  => "Апреля",
            5  => "Мая",
            6  => "Июня",
            7  => "Июля",
            8  => "Августа",
            9  => "Сентября",
            10 => "Октября",
            11 => "Ноября",
            12 => "Декабря",
        );

        public static $RusMonthsIntNom = array(
            1  => "Январь",
            2  => "Февраль",
            3  => "Март",
            4  => "Апрель",
            5  => "Май",
            6  => "Июнь",
            7  => "Июль",
            8  => "Август",
            9  => "Сентябрь",
            10 => "Октябрь",
            11 => "Ноябрь",
            12 => "Декабрь",
        );

        public static $RusDayWeek = array(
            1  => "Пн",
            2  => "Вт",
            3  => "Ср",
            4  => "Чт",
            5  => "Пт",
            6  => "Сб",
            7  => "Вс",
        );

        function GetClassName() {
            return "dummy";
        }
    
        
        public function endBuffering() {
            if(ob_get_level() > 1) {
                $data = ob_get_contents();
                ob_end_clean();
                if(!empty(self::$Buffered)) {
                    foreach(self::$Buffered as $contentID => $contentData) {
                        $search[] = "{".$contentID."}";
                    }
                
                    $data = str_replace($search, self::$Buffered, $data);
                }
            
                echo $data;
            }
        }
    
    
        public static function showBuffered($contentID) {
            if(ob_get_level() > 1) {
                echo "{".$contentID."}";
            }
        }
    
    
        public static function setBuffered($contentID, $data) {
            self::$Buffered[$contentID] = $data;
        }
    
    
        function GetPropertiesPath() {
            $Path = SETTINGS_PATH."/".$this->GetClassName();
            return $Path;
        }

        function GetPropertiesFile($Id, $Section="") {
            $Section = ($Section ? "_$Section" : "");
            return $this->GetPropertiesPath().$Section."_$Id.php";
        }

        // public
    
        function GetNames($Id) {
            return $this->Properties["$Id"]["Names"];
        }

        function GetTypes($Id) {
            return $this->Properties["$Id"]["Types"];
        }

        function GetLabels($Id) {
            return $this->Properties["$Id"]["Labels"];
        }

        function GetValues($Id) {
            return $this->Properties["$Id"]["Values"];
        }

        function db_unquote($str) {
            $str = str_replace("&quot;", "\"", $str);
            //      $str = str_replace("\\", "\\\\", $str);
            return $str;
        }

        function GetPropertyValue($Property) {
            if(is_array($Property)) {
                foreach($Property as $Key => $Value) {
                    $Property[$Key] = $this->GetPropertyValue($Value);
                }
            } else {
                $Property = $this->db_unquote($Property);
            }
            return $Property;
        }

        function GetProperty($Property) {
            foreach($this->Properties as $ID => $Properties) {
                foreach($Properties["Names"] as $Key => $Value) {
                    if($Value == $Property)
                        return $this->GetPropertyValue(($Properties["Values"][$Value.$this->LngPrefix] ? $Properties["Values"][$Value.$this->LngPrefix] : $Properties["Values"][$Key]));
                }
            }
            return "";
        }

        function GetTemplate($Property) {
            $Value = $this->GetProperty($Property);
            $Value = is_array($Value) ? $Value['Value'] : 0;

            $Data = "";
            if(isset($this->DefaultTemplates[$Property][$Value])) {
                $Data = $this->DefaultTemplates[$Property][$Value];
            } else {
                if(file_exists(SETTINGS_PATH."/".$this->GetClassName()."_".$Property."_".$Value.$this->LngPrefix.".template")) {
                    $FileName = SETTINGS_PATH."/".$this->GetClassName()."_".$Property."_".$Value.$this->LngPrefix.".template";
                } else {
                    $FileName = SETTINGS_PATH."/".$this->GetClassName()."_".$Property."_".$Value.".template";
                }
                if (file_exists($FileName)) {
                    $Data = implode("", file($FileName));
                }
            }

            return $Data;
        }

        function GetBlock($Property) {
            $Property = $this->GetProperty($Property);
            if ($Property) {
                $Module = $Property["classes"];
                $Block  = $Property["blocks"];
                eval("\$Obj = _autoload($Module, '$Block', \$this);");
                return $Obj;
            }
        }

        function AddError($Error, $Tag) {
            array_push($this->Errors, "$Error: (".str_replace("<", "&lt;", $Tag).")");
        }

        function AddChildErrors($Obj) {
            reset($Obj->Errors);
            while (list($Key, $Value) = each($Obj->Errors)) {
                array_push($this->Errors, $Value);
            }
        }

        function ParseTag($Tag) {
            global $Blocks;
            global $Errors;

            $Attrs = array();

            $txtAttrs = substr($Tag, 5, -1);
            $Arr1 = explode(" ", $txtAttrs);
            while (list($Key, $Value) = each($Arr1)) {
                if ($Value) {
                    $Arr2 = explode("=", $Value);
                    if ($Arr2[0] && $Arr2[1]) {
                        $Delim1 = substr($Arr2[1], 0, 1);
                        $Delim2 = substr($Arr2[1], -1, 1);
                        if ($Delim1 == $Delim2 && ($Delim1 == "\"" || $Delim1 == "'")) {
                            $Arr2[1] = substr($Arr2[1], 1, -1);
                            $Attrs[strtolower($Arr2[0])] = $Arr2[1]; //strtolower($Arr2[1]);
                        }
                    }
                }
            }

            if ($Attrs["type"]==INS_VARIABLE || $Attrs["type"]==INS_FUNCTION || $Attrs["type"]==INS_BLOCK || $Attrs["type"]==INS_DATA || $Attrs["type"]==INS_INCLUDE || $Attrs["type"]==INS_BUFFERED) {
                if ($Attrs["name"]) {
                    if ($Attrs["type"]==INS_BLOCK && !$Attrs["class"] && !$Attrs["external"]) {
                        $this->AddError("Не определён класс", $Tag);
                    } else {
                        $Attrs["src"] = $Tag;
                        array_push($this->Blocks, $Attrs);
                    }
                } else {
                    $this->AddError("Не определено имя", $Tag);
                }
            } else {
                $this->AddError("Не определен тип", $Tag);
            }
        }

        function FindAllTags($Template) {
            $Offset = -1;
            do {
                $Offset = strpos($Template, "<".TAG_INS." ", $Offset+1);
                $NotFound = ($Offset === false);
                if (!$NotFound) {
                    $CloseOffset = strpos($Template, ">", $Offset);
                    $NextOffset = strpos($Template, "<", $Offset+1);
                    $Tag = substr($Template, $Offset, $CloseOffset - $Offset + 1);
                    $this->ParseTag($Tag);
                }
            } while (!$NotFound);
        }

        // #If {

        function FindTagsPos(&$Template, $BTag) {
            $Offset = -1;
            $Num = 1;

            $arrTags = array();
            do {
                $Offset = strpos($Template, $BTag, $Offset+1);
                $NotFound = ($Offset === false);
                if (!$NotFound) {
                    $CloseOffset = strpos($Template, ">", $Offset);
                    $Tag = substr($Template, $Offset, $CloseOffset - $Offset + 1);
                    $arrTags[$Num] = array(0 => $Offset, 1 => $CloseOffset, "tag" => $Tag);
                    $Num++;
                }
            } while (!$NotFound);
            return $arrTags;
        }

        function PlaceIfToTree($NewIf, &$Tree) {
            // Добавляет тэг <ifins> в дерево <ifins>
            foreach($Tree as $Key => $OldIf) {
                if ($NewIf["ee"] > $OldIf["bb"] && $OldIf["ee"] > $NewIf["bb"]) {
                    $this->PlaceIfToTree($NewIf, $Tree[$Key]["Ifs"]);
                    return;
                }
            }
            array_push($Tree, $NewIf);
        }

        function FindTagParams($Tag) {
            // Возвращает параметры тэга
            $Attrs = array();
            $txtAttrs = substr($Tag, 5, -1);
            $Arr1 = explode(" ", $txtAttrs);
            foreach($Arr1 as $Key => $Value) {
                if ($Value) {
                    $Arr2 = explode("=", $Value);
                    if ($Arr2[0] && $Arr2[1]) {
                        $Delim1 = substr($Arr2[1], 0, 1);
                        $Delim2 = substr($Arr2[1], -1, 1);
                        if ($Delim1 == $Delim2 && ($Delim1 == "\"" || $Delim1 == "'")) {
                            $Arr2[1] = substr($Arr2[1], 1, -1);
                            $Attrs[strtolower($Arr2[0])] = $Arr2[1];
                        }
                    }
                }
            }
            return $Attrs;
        }

        function AnalizeIf(&$If_b, &$If_e) {
            // Возвращает дерево <ifins> ... </ifins> ... </ifins> ... </ifins>
            $Ifs = array();
            $IfTree = array();

            $Size = sizeof($If_b);
            for ($i = 1; $i<=$Size; $i++) {
                for ($j = $Size; $j>=1; $j--) {
                    if (!$If_b[$j]["done"] && $If_b[$j][1] < $If_e[$i][0]) {
                        $If_b[$j]["done"] = 1;
                        break;
                    }
                }
                if ($j) {
                    array_push($Ifs,
                        array(
                            "bb" => $If_b[$j][0],
                            "be" => $If_b[$j][1],
                            "eb" => $If_e[$i][0],
                            "ee" => $If_e[$i][1],
                            "Tag" => $If_b[$j][tag],
                            "Ifs" => array(),
                        )
                    );
                }
                else
                    return 0;
            }

            for ($i=sizeof($Ifs)-1; $i>=0; $i--) {
                $this->PlaceIfToTree($Ifs[$i], $IfTree);
            }
            return $IfTree;
        }

        function ParseIf(&$Template) {
            $If_b = $this->FindTagsPos($Template, "<".TAG_IFINS." ");
            $If_e = $this->FindTagsPos($Template, "</".TAG_IFINS.">");

            $IfTree = $this->AnalizeIf($If_b, $If_e);

            foreach($IfTree as $Node) {
                $Params = $this->FindTagParams($Node["Tag"]);
                if ($Params["data"]) {
                    $Params["value"] = "\$this->Data[\"".$Params["data"]."\"]";
                }
                if($Params['equal']) {
                    $Data = "\$this->Data[\"".$Params["equal"]."\"]";
                    $Value = $Params["value"];
                    $Type = "equal";
                    $Not = 0;
                }
                elseif ($Params["exists"]) {
                    $Data = "\$this->Data[\"".$Params["exists"]."\"]";
                    $Type = "exists";
                    $Not = 0;
                }
                elseif ($Params["notexists"]) {
                    $Data = "\$this->Data[\"".$Params["notexists"]."\"]";
                    $Type = "exists";
                    $Not = 1;
                }
                elseif ($Params["existsor"]) {
                    $Datas = explode(",", $Params["existsor"]);
                    $Data = "\$this->Data[\"".$Datas[0]."\"]";
                    $Data1 = "\$this->Data[\"".$Datas[1]."\"]";
                    $Type = "existsor";
                    $Not = 0;
                }
                elseif ($Params["notexistsor"]) {
                    $Datas = explode(",", $Params["notexistsor"]);
                    $Data = "\$this->Data[\"".$Datas[0]."\"]";
                    $Data1 = "\$this->Data[\"".$Datas[1]."\"]";
                    $Type = "existsor";
                    $Not = 1;
                }
                elseif ($Params["existsand"]) {
                    $Datas = explode(",", $Params["existsand"]);
                    $Data = "\$this->Data[\"".$Datas[0]."\"]";
                    $Data1 = "\$this->Data[\"".$Datas[1]."\"]";
                    $Type = "existsand";
                    $Not = 0;
                }
                elseif ($Params["notexistsand"]) {
                    $Datas = explode(",", $Params["notexistsand"]);
                    $Data = "\$this->Data[\"".$Datas[0]."\"]";
                    $Data1 = "\$this->Data[\"".$Datas[1]."\"]";
                    $Type = "existsand";
                    $Not = 1;
                }
                elseif ($Params["can"]) {
                    if($Params['class'])
                        $Data = "classSecurity::Can(\"".$Params["can"]."\", \"".$Params["class"]."\")";
                    else
                        $Data = "\$this->Can[\"".$Params["can"]."\"]";
                    $Type = "can";
                    $Not = 0;
                }
                elseif ($Params["cannot"]) {
                    if($Params['class'])
                        $Data = "classSecurity::Can(\"".$Params["can"]."\", \"".$Params["class"]."\")";
                    else
                        $Data = "\$this->Can[\"".$Params["cannot"]."\"]";
                    $Type = "can";
                    $Not = 1;
                }
                else {
                    $Not   = $Params["not"];
                    $Type  = $Params["type"];
                    $Data  = "\$this->Data[\"".$Params["data"]."\"]";
                    $Value = $Params["value"];
                }

                if (!$Params["data"])
                    $Value = "'".mysql_real_escape_string($Value)."'";

                if ($Type == "exists")
                    $Res = "$Data";
//                    $Res = "$Data != \"\" && $Data != 0";
                elseif ($Type == "equal")
                    $Res = "$Data == $Value";
                elseif ($Type == "less")
                    $Res = "$Data < $Value";
                elseif ($Type == "grater")
                    $Res = "$Data > $Value";
                elseif ($Type == "existsor")
                    $Res = "$Data != \"\" || $Data1 != \"\"";
                //        elseif ($Type == "notexistsor")
                //          $Res = "$Data == \"\" || $Data1 == \"\"";
                elseif ($Type == "existsand")
                    $Res = "$Data != \"\" && $Data1 != \"\"";
                elseif ($Type == "can")
                    $Res = "$Data";
                if ($Not)
                    $Res = "!($Res)";

                $WTemplate = substr($Template, $Node["be"] + 1, $Node["eb"] - $Node["be"] - 1);

                $this->Template2Php($WTemplate, array(), 0);

                $Template = substr($Template, 0,
                        $Node["bb"]).
                    "<? if ($Res) { ?>".$WTemplate."<? } ?>".
                    substr($Template, $Node["ee"]+1);

            }
        }

        // } #If

        // #While {

        function ParseWhile(&$Template) {
            // Изменяет шаблон $Template в соответствии с <whileins>
            $If_b = $this->FindTagsPos($Template, "<".TAG_WHILEINS." ");
            $If_e = $this->FindTagsPos($Template, "</".TAG_WHILEINS.">");

            $IfTree = $this->AnalizeIf($If_b, $If_e);

            foreach($IfTree as $Node) {
                $Params = $this->FindTagParams($Node["Tag"]);
                $Name = $Params["data"];

                $WTemplate = substr($Template, $Node["be"] + 1, $Node["eb"] - $Node["be"] - 1);
                $this->Template2Php($WTemplate, array(), 0);

                $Template = substr($Template, 0, $Node["bb"]).
                    "<?
  \$OldData = \$this->Data;
  foreach(\$this->Data[\"$Name\"] as \$Key => \$arrData) {
    foreach(\$arrData as \$dataName => \$dataValue) {
      \$this->Data[\$dataName] = \$dataValue;
    }
?>$WTemplate<?
    unset(\$this->Data);
    \$this->Data = \$OldData;
  }
?>".substr($Template, $Node["ee"]+1);

            }
        }

        // } #While

        function Template2Php(&$Template, $External = array(), $Eval = 1) {
            $this->ParseWhile($Template);
            $this->ParseIf($Template);

            $this->FindAllTags($Template);

            foreach($this->Blocks as $Key => $Tag) {
                $BlockType  = $Tag["type"];
                $BlockName  = $Tag["name"];
                $BlockClass = $Tag["class"];
                $BlockCache = (int)$Tag["cache"];
                $BlockAction = $Tag["action"];
                $BlockSelf = $Tag["self"];

                if ($BlockType != INS_BLOCK)
                    unset($BlockCache);

                $EvalStr = "";
                if ($Tag["external"]) {
                    if ($BlockType == INS_VARIABLE) {
                        if ($Tag["nophptag"])
                            $EvalStr = "\$External[\"$BlockName\"]";
                        else
                            $EvalStr = "<? echo \$External[\"$BlockName\"]; ?>";
                    } elseif($BlockType == INS_INCLUDE) {
                        $EvalStr = "<?
 \$IncludePath = \$External[$BlockName] ? \$External[$BlockName].\".\" : \"\";
 \$IncludePath .= \"\\\"/\".\$External[$BlockName.\"_file\"].\"\\\"\";
  eval(\"\\\$Path = \$IncludePath;\");
  if(@file_exists(\$Path))
    include(\$Path);
?>";
                    } elseif($BlockType == INS_BLOCK) { //  && $External[$BlockName]["blocks"]
                        $EvalStr = "<?
 \$newClassName = \$External[$BlockName][\"classes\"];
 \$newBlockName = \$External[$BlockName][\"blocks\"];
 if (\$newBlockName) {
   eval(\"\\\$Obj = _autoload(\$newClassName, '\$newBlockName', \\\$this);\");
   \$Obj->Action();
 }
?>";

                    }
                } else {
                    if ($BlockType == INS_VARIABLE) {
                        if(DEFAULT_LANGUAGE) {
                            $EvalStr = "<? echo \$this->GetProperty(\"".$BlockName.$this->LngPrefix."\") ? \$this->GetProperty(\"".$BlockName.$this->LngPrefix."\") : \$this->GetProperty(\"".$BlockName."\"); ?>";
                        } else {
                            $EvalStr = "<? echo \$this->GetProperty(\"".$BlockName."\"); ?>";
                        }
                    }
                    elseif ($BlockType == INS_FUNCTION) {
                        $EvalStr = "<? \$this->$BlockName(); ?>";
                    }
                    elseif ($BlockType == INS_BLOCK) {
                        $EvalStr = "<? \$Obj = _autoload(\"$BlockClass\", \"$BlockName\", \$this); \$Obj->Action(\"$BlockAction\"); ?>";
                    }
                    elseif ($BlockType == INS_DATA) {
                        $EvalStr = "<? echo \$this->Data[\"$BlockName\"]; ?>";
                    }
                    elseif ($BlockType == INS_BUFFERED) {
                        $EvalStr = "<? \$this->showBuffered(\"$BlockName\"); ?>";
                    }
                    if ($BlockCache) {
                        $EvalStr = "<? \$this->GetCache(\"$BlockClass\", \"$BlockName\", \"$BlockAction\", $BlockCache); ?>";
                    }
                }

                $Template = str_replace($Tag["src"], $EvalStr, $Template);

                //        if( !$BlockSelf ) {
                //          $Template = preg_replace("/(?<=href\=\"|action\=\")(?=[^http:|\.|\?|#])(.*?)(?=\")/", DOMAIN_URL."$1", $Template);
                //        }


            }

            if ($Eval)
                eval("?>$Template<?");
        }


        function GetCache($BlockClass, $BlockName, $BlockAction, $BlockCache) {

            $EvalStr = "<? \$Obj = _autoload(\"$BlockClass\", \"$BlockName\", \$this); \$Obj->Action(\"$BlockAction\"); ?>";

            $CacheFile = "$BlockClass.$BlockName";
            $CacheFile = $BlockAction ? "$CacheFile.$BlockAction" : $CacheFile;
            $CacheFile .= $_SERVER['PHP_SELF'];
            //      $CacheFile .= ($_SERVER[QUERY_STRING] != "") ? "_".$_SERVER[QUERY_STRING] : "";
            $CacheFile = str_replace("/index.php", "", $CacheFile);
            $CacheFile = str_replace("&", "_", $CacheFile);
            $CacheFile = str_replace("=", "_", $CacheFile);
            $CacheFile = str_replace("/", "_", $CacheFile);
            $CacheFile = CACHE_PATH."/".$CacheFile;
            if(defined("DEFAULT_LANGUAGE")) {
                $CacheFile .= $this->LngPrefix;
            }

            if (file_exists($CacheFile)) {
                $Expired = ($BlockCache < time() - filectime($CacheFile) ? 1 : 0);
            } else {
                $Expired = 1;
            }
            if ($Expired) {
                ob_start();
                eval("?>".$EvalStr."<?");
                $EvalStr = ob_get_contents();
                ob_end_clean();

                $fp = fopen($CacheFile, "w");
                fputs($fp, $EvalStr);
                fclose($fp);
            }
            include($CacheFile);
        }


        function Ins2Php($Property) {
            $TemplateData = $this->GetProperty($Property);
            $External = is_array($TemplateData) ? $TemplateData['External'] : "";
            $Value = is_array($TemplateData) ? $TemplateData['Value'] : 0;

            $CompiledFile = COMPILED_PATH."/".$this->GetClassName().".".$Property.".".$Value.$this->LngPrefix.".php";

            if(defined("COMPILED_PATH")) {
                if (!file_exists($CompiledFile)) {
                    $Template = $this->GetTemplate($Property);
                    $this->Template2Php($Template, $External, 0);

                    $fp = fopen($CompiledFile, "w");
                    fputs($fp, $Template);
                    fclose($fp);
                }
                include($CompiledFile);
            } else {
                $Template = $this->GetTemplate($Property);
                $this->Template2Php($Template, $External);
            }
        }

        /*
            function GetSectionID() {
              if(is_object($this->Parent)) {
                return $this->Parent->GetSectionID();
              } else {
                return $this->Parent;
              }
            }
        */
        function GetSectionID() {
            $SectionID = 0;

            $Parent = $this->Parent;
            if ($Parent) {
                while ($Parent) {
                    $SectionID = $Parent->SectionID;
                    $Parent = $Parent->Parent;
                }
            }
            return $SectionID;
        }

        function Action() {
        }

        function LoadProperties($Id, $sec = "") {
            $PropertiesFileName = $this->GetPropertiesFile($Id, $sec);

            if (file_exists($PropertiesFileName)) {
                include($PropertiesFileName);
            } else {
                $Settings = array();
            }
            $Properties = &$this->Properties[$Id];

            foreach($Properties["Names"] as $Key => $Value) {
                if(!isset($Properties["Values"][$Key]) || isset($Settings[$Value])) {
                    $Properties["Values"][$Key] = $Settings[$Value];
                }
            }
        }


        public function GetPath($SectionID = 0) {
            $SectionID = $SectionID ? $SectionID : $this->GetSectionID();
            $Dir = "";
            while($SectionID) {
                $Obj = new classPage($SectionID);
                $Dir = $Obj->GetProperty("page_localdir")."/$Dir";
                $SectionID = $Obj->GetProperty("page_parent");
            }
            return "/$Dir";
        }

        
        public static function mkToday($Time) {
            if(preg_match("/\D+/", $Time))
                $Time = strtotime($Time);
            if( $Time >= mktime(0, 0, 0, date("m"),  date("d"),  date("Y")) && $Time <= mktime(0, 0, 0, date("m"), date("d")+1, date("Y")) ) {
                return "Сегодня ".date("H:i", $Time);
            } elseif( $Time >= mktime(0, 0, 0, date("m"),  date("d")-1,  date("Y")) && $Time <= mktime(0, 0, 0, date("m"), date("d"), date("Y")) ) {
                return "Вчера ".date("H:i", $Time);
            } elseif( $Time <= mktime(0, 0, 0, 1, 1, date("Y")) ) {
                return date("j", $Time)." ".self::$RusMonths[date("m", $Time)]." ".date("Y", $Time);
            } else {
                return date("j", $Time)." ".self::$RusMonths[date("m", $Time)]." ".date("H:i", $Time);
            }
        }
    
    
        function classProperties($sec = "", $Parent = "") {

            $this->LoadProperties("public");
            $this->LoadProperties("private");
            if($sec)
                $this->LoadProperties("private", $sec);

            $this->Parent = $Parent;
            $this->SectionID = $sec;

            if(DEFAULT_LANGUAGE != "")
                $this->LngPrefix = (PAGE_LANGUAGE != DEFAULT_LANGUAGE) ? "_".PAGE_LANGUAGE : "";
        }

    }

    
    
    class classInsParser extends classProperties {
        var $Properties = array(
            "private" => array(
                "Names" => array()
            ),
            "public" => array(
                "Names" => array()
            )
        );
    }


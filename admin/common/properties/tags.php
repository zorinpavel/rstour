<?php

    class ParseTags {

        public $Attrs = array();

        function ParseTags($Template, $TagName) {
            $Offset = -1;
            do {
                $Offset = strpos($Template, "<$TagName ", $Offset+1);
                $NotFound = ($Offset === false);
                if (!$NotFound) {
                    $CloseOffset = strpos($Template, ">", $Offset);
                    //          $NextOffset = strpos($Template, "<$TagName ", $Offset+1);
                    $Tag = substr($Template, $Offset, $CloseOffset - $Offset + 1);
                    array_push($this->Attrs, $this->ParseTag($Tag));
                }
            } while (!$NotFound);
        }


        function ParseTag($Tag) {
            //      $Attrs = array();

            $txtAttrs = substr($Tag, 5, -1);
            $Arr1 = explode(" ", $txtAttrs);
            while (list($Key, $Value) = each($Arr1)) {
                if ($Value) {
                    $Arr2 = explode("=", $Value);
                    if ($Arr2[0] && $Arr2[1]) {
                        $Delim1 = substr($Arr2[1], 0, 1);
                        $Delim2 = substr($Arr2[1], -1, 1);
                        if ($Delim1 == $Delim2 && ($Delim1 == "\"" || $Delim1 == "'")) {
                            $Arr2[1] = substr($Arr2[1], 1, -1);
                            $Attrs[strtolower($Arr2[0])] = $Arr2[1]; //strtolower($Arr2[1]);
                        }
                    }
                }
            }
            return $Attrs;
        }

    }

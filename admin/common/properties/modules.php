<?

  class InputModules extends InputSelect {
    var $PropName;
    var $BlockValue;

    function dPrint() {
      echo "
  <script language=\"JavaScript\">
    blockSelected[\"".$this->PropName."\"] = new Array();
    blockSelected[\"".$this->PropName."\"][\"".$this->Value."\"] = \"".$this->BlockValue."\";
  </script>";
      parent::dPrint();
      echo "
  <select name=\"".$this->PropName."[blocks]\"></select>
  <input type=\"button\" value=\"Свойства...\" class=\"blue\" onClick=\"window.location='sec_private.php?mod=".$this->Value."&sec=".$this->BlockValue."';\">
  <script language=\"JavaScript\">
    ChangeBlocks(\"".$this->Value."\", \"".$this->PropName."\");
  </script>";
    }

    function InputModules($Name, $Value, $Class="") {
      global $Modules;
      if ($Class) {
        $Options = array("$Class"=>"$Class");
      }
      else {
        $Options = array();
        reset($Modules);
        while (list(, $ModuleName) = each($Modules)) {
          $Options["$ModuleName"] = $ModuleName;
        }
      }

      $this->PropName = $Name;
      $this->BlockValue = $Value["blocks"];


      if (!$Value["classes"]) {
        reset($Options);
        list(, $Value["classes"]) = each($Options);
      }

      $this->InputSelect($Name."[classes]", $Value["classes"], $Options, "", "ChangeBlocks(this.options[selectedIndex].value, '$Name');");
    }
  }

?>
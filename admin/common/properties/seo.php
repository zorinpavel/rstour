<?php

  class Seo {

    public $MaxWords = 2;
    public $Descovers = array();
    
    public function SetLinks($Original = "") {
      $this->GetDescovers();
      foreach($this->Descovers as $Key => $Descover) {
        foreach($Descover['Words'] as $wKey => $origWord) {
          if($origWord) {
            $lWords = array();
            $Words = preg_split("/ /", $origWord, -1, PREG_SPLIT_NO_EMPTY);
            foreach($Words as $Wd) {
              array_push($lWords, $this->lemmatize($Wd));
            }
            $Word = "".join("?(?:\w+)?(?:\s+)", $lWords)."";

            preg_match_all("/(?<=\s|^)$Word?(?:\w+)(?:|\\|\.|,|;|\)|\"|\')/i", $Original, $Findings[$Key][$wKey], PREG_OFFSET_CAPTURE);

            unset($Findings[$Key][$wKey][1]);
            array_splice($Findings[$Key][$wKey][0], $this->MaxWords);

            foreach($Findings[$Key][$wKey][0] as $k => $Find) {
              $Str = substr($Original, 0, $Find[1]);
              $ReminderStr = substr($Original, $Find[1]);
              $Str .= preg_replace("/".$Find[0]."/", "<a href=\"".$this->Descovers[$Key]['Wo_Url']."\" title=\"".$this->Descovers[$Key]['Wo_Name']."\">\${0}</a>", $ReminderStr, 1);
              $ReminderStr = substr($Original, strlen($Str));
              $Original = $Str.$ReminderStr;
            }
          }
        }
      }
      return $Original;
    }

    public function GetDescovers() {
      $r = db_query("SELECT sw.*, swv.* FROM seoWords AS sw LEFT JOIN seoWordsVariants AS swv ON sw.Wo_Code = swv.Wo_Code");
      while($Word = mysql_fetch_assoc($r)) {
        $Words[$Word['Wo_Code']]['Words'][$Word['WoV_Code']] = $Word['WoV_Value'];
        $Words[$Word['Wo_Code']] = array_merge($Words[$Word['Wo_Code']], $Word);
      }
      $this->Descovers = $Words;
    }

    /*
     * ⌠╓═╚О╔Б ╝╙╝╜Г═╜╗О ╗╖ ╞╝╗А╙╝╒╝ё╝ ╖═╞Ю╝А═
     *
     * @param string $word
     * @return string
     */
    public function lemmatize($sWord) {
      $sWord = strtolower($sWord);
      $sWord = trim($sWord);
      $sWord = preg_replace("/(\w{2,})(╝ё╝|╝╒╝|К|╗╘)$/i", "\$1", $sWord);
      $sWord = preg_replace('/[═╔Я╗╘╝ЦКМНОЛЙК]+$/i','',$sWord);
      if(strlen($sWord)<2) return '';
      return $sWord;
    }

  }
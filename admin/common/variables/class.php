<?php

    class storeVariables extends InputArrayPV {

        static $Options = array(
            "1" => "T30",
            "2" => "InputCheckbox",
            "3" => "InputYesNo",
            "4" => "TA30",
        );

        function storeVariables($Name, $Value) {
            parent::Input($Name, $Value);
            foreach($Value as $Key => $Var) {
                if(!$Var['id'] || $Var['id'] == "")
                    unset($Value[$Key]);
            }
            $Value = $Value ?: array();
            array_push($Value, array(
                0 => array(
                    "id" => "",
                    "desc" => "",
                    "type" => "",
                    "value" => "",
                )
            ));
            $this->Value = $Value;
        }


        function dPrint() {
            echo "
                <table cellpadding=\"3\" cellspacing=\"0\" border=\"0\">
                 <tr>
                  <td><b>ID</b></td>
                  <td><b>Название</b></td>
                  <td><b>Тип</b></td>
                  <td><b>Значение по умолчанию</b></td>
                 </tr>";

            foreach($this->Value as $Key => $Value) {
                echo "<tr>";
                echo "<td>";
                $Obj = new InputText($this->Name."[$Key][id]", $Value['id'], 15);
                $Obj->dPrint();
                echo "</td>";

                echo "<td>";
                $Obj = new InputText($this->Name."[$Key][desc]", $Value['desc'], 30);
                $Obj->dPrint();
                echo "</td>";

                echo "<td>";
                $Obj = new InputSelect($this->Name."[$Key][type]", $Value['type'], self::$Options);
                $Obj->dPrint();
                echo "</td>";

                echo "<td>";

                switch($Value["type"]) {
                    case 0:
                    case 1:
                    default:
                        $Obj = new T30($this->Name."[$Key][value]", $Value['value']);
                        break;
                    case 2:
                        $Obj = new InputCheckbox($this->Name."[$Key][value]", $Value['value']);
                        break;
                    case 3:
                        $Obj = new InputYesNo($this->Name."[$Key][value]", $Value['value']);
                        break;
                    case 4:
                        $Obj = new TA30($this->Name."[$Key][value]", $Value['value']);
                        break;
                }
                $Obj->dPrint();
                echo "</td>";
                echo "</tr>";
            }

            echo "</table>";
        }
    }



    class storageVariables extends classProperties {
        var $Properties = array(
            "public" => array(
                "Names" => array(
                    "var" => "var",
                ),
                "Types" => array(
                    "var" => "storeVariables",
                ),
                "Labels" => array(
                    "var" => "Переменные",
                )
            ),
            "private" => array(
                "Names" => array(
                ),
                "Types" => array(
                ),
                "Labels" => array(
                )
            )
        );

        var $DefaultTemplates = array();

        function GetClassName() {
            return "variables";
        }

        function storageVariables($sec = "", $Parent = "") {
            parent::classProperties($sec, $Parent);
        }
    }

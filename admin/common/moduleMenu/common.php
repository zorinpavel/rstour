<?php

    class MenuTypeInputSelect extends InputSelect {
        function MenuTypeInputSelect($Name, $Value) {
            $Options = array(
                "1"=>"Подразделы текущего раздела",
                "2"=>"Подразделы заданного раздела",
                "4"=>"Хлебные крошки",
                "5"=>"Подразделы N уровня текущей ветви",
                "6"=>"Карта сайта",
                "7"=>"Несколькоуровневое меню",
                "8"=>"Несколькоуровневое меню N уровня ветви",
            );
            $this->InputSelect($Name, $Value, $Options);
        }
    }

    class InputSubSectionsTree extends InputSectionsTree {
        public $SelectName;
        public $SubSections = array();
        public $All;

        function dPrint() {
            parent::dPrint();

            $All = new InputYesNo($this->SelectName."[All]", $this->All);
            echo "<br>Все разделы: ";
            $All->dPrint();

            if (!$this->All) {
                echo "<br>Подразделы:";
                $query = "SELECT Sec_Code, Sec_Name, Sec_Order FROM Sections WHERE Sec_Parent='".$this->Value."' AND Sec_Visible > 0";
                $Sections = DB::selectArray($query, "Sec_Code");
                $Sections = sortArray($Sections, array("Sec_Order", "DESC", "Sec_Code", "ASC"));
                foreach($Sections as $Sec_Code => $Section) {
                    $Obj = new InputCheckbox($this->SelectName."[SubSections][$Sec_Code]", $this->SubSections[$Sec_Code]);
                    echo "<br>";
                    $Obj->dPrint();
                    echo " ".$Section['Sec_Name'];
                }
            }
        }

        function InputSubSectionsTree($Name, $Value) {
            parent::InputSectionsTree($Name."[Name]", $Value["Name"]);
            $this->SelectName = $Name;
            $this->SubSections = $Value["SubSections"];
            $this->All = $Value["All"];
        }
    }

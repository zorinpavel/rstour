<?php

    class classMenu extends classTreeData {

        var $Properties = array(
            "public" => array(
                "Names" => array(
                ),
                "Types" => array(
                ),
                "Labels" => array(
                )
            ),

            "private" => array(
                "Names" => array(
                    "MenuType",
                    "Parent" => "output_parent",
                    "Section" => "Section",
                    "Section2" => "Section",
                    "Deep" => "output_deep",
                    "Separator",
                    "template_top",
                    "template_element1",
                    "template_element2",
                    "template_element3",
                    "template_element4",
                    "template_element5",
                    "template_separator",
                    "template_indent",
                    "template_bottom"
                ),
                "Types" => array(
                    "MenuTypeInputSelect",
                    "Parent" => "InputYesNo",
                    "Section" => "InputSubSectionsTree",
                    "Section2" => "InputSectionsTree",
                    "Deep" => "T10",
                    "",
                    "InputTemplates",
                    "InputTemplates",
                    "InputTemplates",
                    "InputTemplates",
                    "InputTemplates",
                    "InputTemplates",
                    "InputTemplates",
                    "InputTemplates",
                    "InputTemplates"
                ),
                "Labels" => array(
                    "Тип меню",
                    "Parent" => "Выводить родителя?",
                    "Section" => "Раздел",
                    "Section2" => "Раздел",
                    "Deep" => "Уровень",
                    "",
                    "Верх",
                    "Заголовок 1",
                    "Заголовок 2",
                    "Заголовок 3",
                    "Заголовок 4",
                    "Заголовок 5",
                    "Разделитель",
                    "Отступ",
                    "Низ"
                )
            )
        );

        var $DefaultTemplates = array(
            "template_top" => array( 0 => "" ),
            "template_element1" => array( 0 => "" ),
            "template_element2" => array( 0 => "" ),
            "template_element3" => array( 0 => "" ),
            "template_element4" => array( 0 => "" ),
            "template_element5" => array( 0 => "" ),
            "template_separator" => array( 0 => "" ),
            "template_indent" => array( 0 => "" ),
            "template_bottom" => array( 0 => "" )
        );

        function GetClassName() {
            return __CLASS__;
        }


        function Action() {
            $Tree = $this->GetTree($this->GetProperty("output_deep"));

            if(sizeof($Tree)) {
                $this->Ins2Php("template_top");
                $Num = 0;
                $this->ActionTree($Tree, 1, $Num);
                $this->Ins2Php("template_bottom");
            }
        }


        function ActionTree($Thread, $Level, &$Num) {
//            static $AllPageVars;
//            if(!isset($AllPageVars))
//                $AllPageVars = array();

            $Template = ($this->ItemTemplate[$Level] ?
                $this->ItemTemplate[$Level] :
                $this->ItemTemplate[sizeof($this->ItemTemplate)]
            );

            $SectionID = (int)$this->GetSectionID();

            $DirName = dirname($_SERVER["REQUEST_URI"]);

            foreach($Thread as $Key => $Value) {
                $Num++;

                $this->Data["num"] = $Num;
                $this->Data["Sec_Code"] = $Key;
                preg_match("/([a-z0-9_\-]+)\/$/i", $Value["Link"], $matches);
                $this->Data["Sec_LocalDir"] = $matches[1];
                $this->Data["item"] = $Value["Item"];
                $this->Data["signature"] = $Value["Signature"];
                $this->Data["link"] = $Value["Link"];
                $this->Data["level"] = $Value["Level"];
                $this->Data["node"] = (sizeof($Value["Elements"]) ? "" : 1);
                $this->Data["count"] = count($Thread);

                $this->Data["parent"] = (substr($DirName."/", 0, strlen($Value["Link"])) == $Value["Link"] ? 1 : "");
                $this->Data["current"] = ( ($Key == $SectionID || substr($_SERVER["REQUEST_URI"], 0, strlen($Value["Link"])) == $Value['Link']) && !$this->Data['parent'] ? 1 : "");

//                $result = db_query("SELECT Sec_Code FROM Sections WHERE Sec_Parent = $Key AND Sec_Visible = 1");
                $this->Data['children'] = count($Value['Elements']) ? 1 : "";

                if(is_array($Value["PageVars"])) {
                    foreach($Value["PageVars"] as $PName => $PValue) {
                        $this->Data[$PName] = $PValue;
//                        $AllPageVars[$PName] = "";
                    }
                }

                if ($Num>1) {
                    $this->Ins2Php("template_separator");
                }
                for ($i = 2; $i<=$Level; $i++) {
                    $this->Ins2Php("template_indent");
                }

                $this->Ins2Php($Template);

//                foreach($AllPageVars as $PName)
//                    unset($this->Data[$PName]);

                $this->ActionTree($Value["Elements"], $Level+1, $Num);
            }
        }


        public function classMenu($sec = "", $Parent = "") {
            parent::classProperties($sec, $Parent);

            $this->ItemTemplate = array("1" => "template_element1");
            for ($i = 2; $i<=5; $i++) {
                $Template = $this->GetTemplate("template_element$i");
                if ($Template)
                    $this->ItemTemplate[$i] = "template_element$i";
                else
                    break;
            }

            $this->MenuType = $this->GetProperty("MenuType");

            if($this->MenuType != 2)
                unset($this->Properties["private"]["Names"]["Section"]);
            if($this->MenuType != 4 && $this->MenuType != 5 && $this->MenuType != 7 && $this->MenuType != 8)
                unset($this->Properties["private"]["Names"]["Deep"]);
            if($this->MenuType != 7)
                unset($this->Properties["private"]["Names"]["Section2"]);
            if($this->MenuType != 1 && $this->MenuType != 2 && $this->MenuType != 4 && $this->MenuType != 5 && $this->MenuType != 7 && $this->MenuType != 8)
                unset($this->Properties["private"]["Names"]["Parent"]);

            if(!self::$SectionsTree) {
                $Page = new classPage("index");
                $PageName = $Page->GetProperty("page_name".$Page->LngPrefix);

                self::$SectionsTree = array(
                    "0" => array(
                        "Sec_Code" => 0,
                        "Sec_Parent" => 0,
                        "Sec_Name" => $PageName ? $PageName : "Главная",
                        "Sec_LocalDir" => "/",
                        "Children" => array(
                        ),
                    )
                );
                $this->GetChildren(0);
            }
        }
    }

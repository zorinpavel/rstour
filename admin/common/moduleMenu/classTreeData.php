<?php

    class classTreeData extends classProperties {

        public static $SectionsTree;

        public function GetChildren($Parents) {

            $ParentsCond = "";
            $Sections = DB::selectArray("SELECT Sec_Code, Sec_Parent, Sec_Name, Sec_LocalDir, Sec_Order, Sec_Visible FROM Sections WHERE Sec_Parent IN ($Parents)", "Sec_Code");
            $Sections = sortArray($Sections, array("Sec_Order", "DESC", "Sec_Code", "ASC"));
            foreach($Sections as $Key => $Section) {
                $Sec_Code = $Section['Sec_Code'];
                self::$SectionsTree[$Sec_Code] = array(
                    "Sec_Code" => $Sec_Code,
                    "Sec_Parent" => $Section['Sec_Parent'],
                    "Sec_Name" => $Section['Sec_Name'],
                    "Sec_LocalDir" => self::$SectionsTree[$Section['Sec_Parent']]["Sec_LocalDir"].$Section['Sec_LocalDir']."/",
                    "Sec_Visible" => $Section['Sec_Visible'],
                    "Children" => array()
                );

                $Page = new classPage($Sec_Code);
                self::$SectionsTree[$Sec_Code]["Sec_Signature"] = $Page->GetProperty("page_signature");

                if(DEFAULT_LANGUAGE && PAGE_LANGUAGE != DEFAULT_LANGUAGE) {
                    $PageName = $Page->GetProperty("page_name".$Page->LngPrefix);
                    self::$SectionsTree[$Sec_Code]["Sec_Visible"] = $Page->GetProperty("page_visible".$Page->LngPrefix);
                    if($PageName)
                        self::$SectionsTree[$Sec_Code]["Sec_Name"] = $PageName;
                }
                self::$SectionsTree[$Section['Sec_Parent']]["Children"][$Sec_Code] = &self::$SectionsTree[$Sec_Code];
                $ParentsCond .= "$Sec_Code,";
            }
            if ($ParentsCond) {
                $ParentsCond = substr($ParentsCond, 0, -1);
                $this->GetChildren($ParentsCond);
            }

        }

        function GetPathArray() {

            $Path = array();
            $SectionID = (int)$this->GetSectionID();

            while ($SectionID) {
                $Path[$SectionID] = array(
                    "Dir" => self::$SectionsTree[$SectionID]["Sec_LocalDir"],
                    "Name" => self::$SectionsTree[$SectionID]["Sec_Name"],
                );
                $SectionID = self::$SectionsTree[$SectionID]["Sec_Parent"];
            }
            $Path[0] = array(
                "Dir" => "/",
                "Name" => self::$SectionsTree[0]["Sec_Name"],
            );
            $Path = array_reverse($Path, true);
            return $Path;
        }

        function GetNSubLevel($Level) {
            $Path = $this->GetPathArray();
            $Key = 0;
            if($Level) {
                for($i=1;$i<=$Level;$i++) {
                    list($Key, $Section) = each($Path);
                }
            }
            return $Key;
        }

        function GetSiteMap($SectionID, $Level, $MaxLevel) {

            $Tree = array();
            if(!$MaxLevel || $Level < $MaxLevel) {
                foreach(self::$SectionsTree[$SectionID]["Children"] as $Sec_Code => $Section) {
                    if(!$Section["Sec_Visible"]) //  && $Level>0
                        continue;

                    $Tree[$Sec_Code] = array(
                        "Item" => $Section["Sec_Name"],
                        "Link" => $Section["Sec_LocalDir"],
                        "Level" => $Level,
                        "Elements" => $this->GetSiteMap($Sec_Code, $Level+1, $MaxLevel)
                    );
                }
                if($Level == 0 && !$SectionID) {
                    $Tree = array(
                        "0" => array(
                            "Item" => self::$SectionsTree[0]["Sec_Name"],
                            "Link" => "/",
                            "Level" => $Level,
                            "Elements" => $Tree
                        )
                    );
                }
            }
            return $Tree;
        }

        
        public function GetTree($Level) {
            $Tree = array();

            if($this->MenuType == 5) {
                $SectionId = $this->GetNSubLevel($Level);
                if($SectionId) {
                    $this->Properties["private"]["Names"]["Section"] = "Section";
                    $this->Properties["private"]["Values"]["Section"] = array( "Name" => "$SectionId", "All"=>"1" );
                    $this->MenuType = 2;
                }
            }
            if($this->MenuType == 8) {
                $SectionId = $this->GetNSubLevel($Level);
                if($SectionId) {
                    $this->Properties["private"]["Names"]["Section"] = "Section";
                    $this->Properties["private"]["Values"]["Section"] = array( "Name" => "$SectionId", "All"=>"1" );
                }
            }

            $SectionID = $this->GetProperty("Section");

            if($this->MenuType == 1) {
                $SectionID = (int)$this->GetSectionID();
                if($this->GetProperty("output_parent")) {
                    $Tree[$SectionID] = array (
                        "Item" => self::$SectionsTree[$SectionID]["Sec_Name"],
                        "Link" => self::$SectionsTree[$SectionID]["Sec_LocalDir"],
                        "Level" => $Level-1,
                        "Elements" => array()
                    );
                }
                foreach(self::$SectionsTree[$SectionID]["Children"] as $Sec_Code => $Section) {
                    if (!$Section["Sec_Visible"])
                        continue;
                    $Tree[$Sec_Code] = array(
                        "Item" => $Section["Sec_Name"],
                        "Link" => $Section["Sec_LocalDir"],
                        "Level" => $Level,
                        "Elements" => array()
                    );
                }
            } elseif ($this->MenuType == 2) {
                if($SectionID["All"]) {
                    if($this->GetProperty("output_parent")) {
                        $Tree[$SectionID['Name']] = array (
                            "Item" => self::$SectionsTree[$SectionID["Name"]]["Sec_Name"],
                            "Link" => self::$SectionsTree[$SectionID["Name"]]["Sec_LocalDir"],
                            "Level" => $Level-1,
                            "Elements" => array()
                        );
                    }
                    foreach(self::$SectionsTree[$SectionID["Name"]]["Children"] as $Sec_Code => $Section) {
                        if(!$Section["Sec_Visible"])
                            continue;
                        $Tree[$Sec_Code] = array(
                            "Item" => $Section["Sec_Name"],
                            "Link" => $Section["Sec_LocalDir"],
                            "Level" => $Level,
                            "Elements" => array()
                        );
                    }
                } else {
                    if (is_array($SectionID["SubSections"]) && sizeof($SectionID["SubSections"])) {
                        foreach($SectionID["SubSections"] as $Sec_Code => $Section) {
                            if (!self::$SectionsTree[$Sec_Code]["Sec_Visible"])
                                continue;
                            $Tree[$Sec_Code] = array(
                                "Item" => self::$SectionsTree[$Sec_Code]["Sec_Name"],
                                "Link" => self::$SectionsTree[$Sec_Code]["Sec_LocalDir"],
                                "Elements" => array()
                            );
                        }
                    }
                }
            } elseif ($this->MenuType == 4) {
                $Elem = &$Tree;

                $Path = $this->GetPathArray();

                $CurrLevel = 0;
                foreach($Path as $Key => $Value) {
                    $CurrLevel++;
                    $Elem = array(
                        $Key => array(
                            "Item" => $Value["Name"],
                            "Link" => $Value["Dir"],
                            "Elements" => array()
                        )
                    );
                    $Elem = &$Elem[$Key]["Elements"];
                }

                if ($CurrLevel <= $Level)
                    $Tree = array();
                
            } elseif ($this->MenuType == 6) {
                $Tree = $this->GetSiteMap(0, 0, $Level);

            } elseif ($this->MenuType == 7) {
                $SectionID = $SectionID['Name'];
                $Tree = $this->GetSiteMap($SectionID, 0, $Level);

                if(!$this->GetProperty("output_parent"))
                    $Tree = $Tree[$SectionID]['Elements'];

            } elseif ($this->MenuType == 8) {
                $SectionID = $SectionID['Name'];
                $Tree = $this->GetSiteMap($SectionID, 0, $Level);

                if($this->GetProperty("output_parent")) {
                    $Tree = array(
                        $SectionID => array(
                            "Item" => self::$SectionsTree[$SectionID]["Sec_Name"],
                            "Signature" => self::$SectionsTree[$SectionID]["Sec_Signature"],
                            "Link" => self::$SectionsTree[$SectionID]["Sec_LocalDir"],
                            "Elements" => $Tree
                        )
                    );
                }

            }

            return $Tree;

        }

        public function classTreeData($sec = "", $Parent = "") {
            parent::classProperties($sec, $Parent);
        }

    }
<?php

    $Arr['Uploads'] = is_array($Arr['Uploads']) ? $Arr['Uploads'] : array();

    foreach($_REQUEST as $Name => $Value) {
        //    ${$Name} = urldecode($Value);
        ${$Name} = $Value;
    }

    foreach($Filters as $filterKey => $filterName) {
        if(!$FiltersCompare[$filterKey])
            unset($Filters[$filterKey]);
    }

    for ($i=1; $i <= $Quantity; $i++) {
        $Code = ${$Id."_$i"};
        $query = "";
        if(!$Code) {
            $query1 = "";
            $query2 = "";
            $Values = 0;

            foreach($Fields as $Key => $Value) {
                list($Type, $Null) = explode("/", $Arr['DBTypes'][$Key]);
                $InsertData = 1;

                ${$Value."_$i"} = mysql_real_escape_string(${$Value."_$i"});

                if(in_array($Value, $Arr['Uploads'])) {
                    if($_FILES[$Value."_$i"]['name']) {
                        $_REQUEST[$Arr['Id']] = $i;
                        UploadOne($Arr, $Value, $_FILES[$Value."_$i"]['name'], $_FILES[$Value."_$i"]['tmp_name'], $Arr['CopyDir']);
                        $UpdateData = 0;
                    }
                    $InsertData = 0;
                } elseif($Value == $Id || $Type == "IGNORE" || in_array($Value, $Filters)) {
                    $InsertData = 0;
                }

                if($InsertData) {
                    $query1 .= "$Value,";
                    switch($Type) {
                        case "INT":
                            $formValue = (int)${$Value."_$i"};
                            break;
                        case "FLOAT":
                            $formValue = (float)${$Value."_$i"};
                            break;
                        case "CHAR":
                        default:
                            $formValue = "'".trim(${$Value."_$i"})."'";
                            break;
                    }
                    
                    if($formValue != "''")
                        $Values++;
                    elseif($Null == "NULL")
                        $formValue = "NULL";
                    
                    $query2 .= "$formValue,";
                }
            }

            if(count($Filters)) {
                foreach($Filters as $Key => $Value) {
                    if($FiltersCompare[$Key] == "N") {
                        $query1 .= "$Value,";
                        $query2 .= (int)${$Value}.",";
                    }
                }
            }

            if($Values > 0 && $Values >= sizeof($Filters)) {
                $query = "INSERT INTO $TableName (".substr($query1, 0, -1).") VALUES (".substr($query2, 0, -1).")";
                DB::Query($query);
            }
        } else {
            $query = "UPDATE $TableName SET ";
            $Values = 0;
            foreach($Fields as $Key => $Value) {
                list($Type, $Null) = explode("/", $Arr['DBTypes'][$Key]);
                $UpdateData = 1;

                ${$Value."_$i"} = mysql_real_escape_string(${$Value."_$i"});

                if(in_array($Value, $Arr['Uploads'])) {
                    if($_FILES[$Value."_$i"]['name']) {
                        $_REQUEST[$Arr['Id']] = $Code;
                        UploadOne($Arr, $Value, $_FILES[$Value."_$i"]['name'], $_FILES[$Value."_$i"]['tmp_name'], $Arr['CopyDir']);
                    }
                    $UpdateData = 0;
                } elseif($Value == $Id || $Type == "IGNORE") {
                    $UpdateData = 0;
                }
                if($UpdateData) {
                    $formValue = "";
                    switch($Type) {
                        case "INT":
                            $formValue = (int)${$Value."_$i"};
                            break;
                        case "FLOAT":
                            $formValue = (float)${$Value."_$i"};
                            break;
                        case "YEAR":
                            list($ys, $ye) = explode("-", ${$Value."_$i"});
                            $ye = $ye ? $ye : date("Y")+1;
                            $j = 0;
                            if($ys && $ye) {
                                while($ys < $ye && $j < 20) {
                                    $formValue .= "$ys,";
                                    $j++; $ys++;
                                }
                                $formValue = "'$formValue$ye'";
                            } else {
                                $formValue = "NULL";
                            }
                            break;
                        case "CHAR":
                        default:
                            $formValue = "'".trim(${$Value."_$i"})."'";
                            break;
                    }
                    
                    if($formValue != "''")
                        $Values++;
                    elseif($Null == "NULL")
                        $formValue = "NULL";
                    
                    $query .= "$Value = $formValue,";
                }
            }

            if(count($Filters)) {
                foreach($Filters as $Key => $Value) {
                    if($FiltersCompare[$Key] == "N" && !in_array($Value, $Fields)) {
                        $query .= "$Value = ".(int)${$Value}.",";
                    }
                }
            }

            $query = ((count($Filters) && $Values < count($Filters) || $Values <= 0) ? "DELETE FROM $TableName" : substr($query, 0, -1))." WHERE $Id = $Code";
            DB::Query($query);
        }

//        echo $Id."_$i $Code - [$query] ($Values | ".sizeof($Filters).")<br>";

    }


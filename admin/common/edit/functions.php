<?php

    $QuaEleForOutputDefault = 10;
    $QuaPartGroupsForOutput = 10;

    $Part = ($_REQUEST['Part'] ? $_REQUEST['Part'] : 1);
    $QuaEleForOutput = ($_COOKIE['Limit'] ? $_COOKIE['Limit'] : $QuaEleForOutputDefault);

    $Limit = $QuaEleForOutput;

    $EmptyQua = 5;

    $Arr = $$Table;
    if(is_array($Arr)) {
        $TableName = $Arr[TableName];
        $Id = $Arr[Id];
        $Fields = Array();
        $Headers = Array();
        $Types = Array();

        foreach($Arr[Fields] as $Key => $Value) {
            $Fields[$Key] = $Value;
            $Headers[$Key] = $Arr[Headers][$Key];
            $Types[$Key] = $Arr[Types][$Key];
        }
        $Buttons = Array();
        $ButtonHeaders = Array();
        foreach($Arr[Buttons] as $Key => $Value) {
            $Buttons[$Key] = $Value;
            $ButtonHeaders[$Key] = $Arr[ButtonHeaders][$Key];
        }
        if (is_array($Arr[Filters])) {
            $Filters = Array();
            $FiltersNames = Array();
            $FiltersCompare = Array();
            $FiltersTypes = Array();
            foreach($Arr[Filters] as $Key => $Value) {
                $Filters[$Key] = $Value;
                $FiltersCompare[$Key] = $Arr[FiltersCompare][$Key];
                $FiltersNames[$Key] = $Arr[FiltersNames][$Key];
                $FiltersTypes[$Key] = $Arr[FiltersTypes][$Key];
                $FiltersJoin[$Key] = $Arr[FiltersJoin][$Key];
            }
        }
    }


    function GetPartFirstElem($Part, $PartSize) {
        return (($Part-1)*$PartSize) + 1;
    }

    function GetPartLastElem($Part, $PartSize) {
        return $Part*$PartSize;
    }

    function dPrintElementPartLinks($Link, $Part, $Quantity) {
        global $QuaEleForOutput, $QuaPartGroupsForOutput, $Limit;

        $QUERY_STRING = preg_replace("/(Part=)(\d)*/", "", $_SERVER['QUERY_STRING']);
        $QUERY_STRING = preg_replace("/^&/", "", $QUERY_STRING);
        $QUERY_STRING = preg_replace("/&$/", "", $QUERY_STRING);
        $QUERY_STRING = preg_replace("/[&]{2}/", "&", $QUERY_STRING);
        $Link .= ($QUERY_STRING ? "?$QUERY_STRING&" : "?");

        $Part = ($_REQUEST['Part'] ? $_REQUEST['Part'] : 1);
        $Part = (($Part-1)*$QuaEleForOutput<$Quantity ? $Part : 1);

        static $PL_Form = 0;
        $PL_Form++;

        dPrint("<table width=\"100%\">");
        dPrint("  <form method=\"get\" name=\"PartLinks_$PL_Form\" action=\"$Link\">");
        dPrint("  <tr>");
        dPrint("    <td align=\"left\" vAlign=\"top\">");
        if ($Part>$QuaPartGroupsForOutput) {
            $Previous = floor(($Part-1)/$QuaPartGroupsForOutput)*$QuaPartGroupsForOutput;
            dPrint("<a href=\"$Link"."Part=$Previous\" class=\"PartLink\">...</a>&nbsp; ");
        }
        $Num = $Previous;
        $i = 1;
        while ($Num*$QuaEleForOutput<$Quantity && $i<=$QuaPartGroupsForOutput) {
            $Num++;
            $First = GetPartFirstElem($Num, $QuaEleForOutput);
            $Last = GetPartLastElem($Num, $QuaEleForOutput);
            $Last = ($Last>$Quantity ? $Quantity : $Last);
            if ($Num == $Part) {
                dPrint("<span class=\"PartLink\">$First..$Last</span>");
            }
            else {
                dPrint("<a href=\"$Link"."Part=$Num\" class=\"PartLink\">[$First..$Last]</a>");
            }
            dPrint("&nbsp; ");
            $i++;
        }
        $Next = $Num + 1;
        if($Next * $QuaEleForOutput < $Quantity + $QuaEleForOutput) {
            dPrint("<a href=\"$Link"."Part=$Next\" class=\"PartLink\">...</a>");
        }
        if ($Limit==5 ||$Limit==10 ||$Limit==25 ||$Limit==50 ||$Limit==100) {
            ${S.$Limit} = " selected";
        }
        elseif ($Limit) {
            $AddiStr = "<option value=$Limit selected>$Limit</option>\n";
        }
        dPrint("    </td>");
        dPrint("<script language=\"JavaScript\">");
        dPrint("  function GetLimit_$PL_Form() {");
        dPrint("    value = (document.all ? document.forms['PartLinks_$PL_Form'].Limit.value : document.PartLinks_$PL_Form.Limit[document.PartLinks_$PL_Form.Limit.selectedIndex].value);");
        dPrint("    return value;");
        dPrint("  }");
        dPrint("</script>");
        dPrint("    <td align=\"right\" vAlign=\"top\">");
        dPrint("      Показывать&nbsp;по&nbsp;<select name=\"Limit\" onChange=\"CreateCookie('Limit', GetLimit_$PL_Form(), 31536000);  window.location='$Link"."Part=$Part'\">");
        dPrint("      $AddiStr");
        dPrint("      <option value=5 $S5>5</option>");
        dPrint("      <option value=10 $S10>10</option>");
        dPrint("      <option value=25 $S25>25</option>");
        dPrint("      <option value=50 $S50>50</option>");
        dPrint("      <option value=100 $S100>100</option>");
        dPrint("     </select>");
        dPrint("    </td>");
        dPrint("   </tr>");
        dPrint("  </form>");
        dPrint(" </table>");
    }


    function GetMaxEmptyNumber($i) {
        global $EmptyQua;

        if(round($i / $EmptyQua) != ($i / $EmptyQua)) {
            $MaxQua = ceil($i / $EmptyQua) * $EmptyQua;
        } else {
            $MaxQua = (($i / $EmptyQua) + 1) * $EmptyQua;
        }

        return $MaxQua;
    }


    function fAddCondition($AddCond, $CondKind, $Cond) {
        if(($Cond) && ($AddCond)) {
            $AddCond = " $CondKind ".$AddCond;
        }

        return $Cond.$AddCond;
    }


    function UploadOne($Arr, $Value, $FileName, $TmpFile, $CopyDir = "data") {
        $Ite_Code = (int)$_REQUEST[$Arr['Id']];
        $OldName = DB::selectValue("SELECT $Value FROM ".$Arr['TableName']." WHERE ".$Arr['Id']." = $Ite_Code");

        if($FileName) {
            //составим имя
            $filename = "/$CopyDir/$Ite_Code.".Cyr2Lat($FileName);

            if(!is_dir(CLIENT_PATH."/".$CopyDir))
                mkdir(CLIENT_PATH."/".$CopyDir, 0775);

            move_uploaded_file($TmpFile, CLIENT_PATH.$filename);
            chmod(CLIENT_PATH.$filename, 0775);
            db_query("UPDATE ".$Arr['TableName']." SET $Value = '$filename' WHERE ".$Arr['Id']." = $Ite_Code");

            if($OldName != "" && file_exists(CLIENT_PATH.$OldName)) {
                unlink(CLIENT_PATH.$OldName);
                $ACF = _autoload("classActualFile");
                $ACF->Expire($OldName);
            }
        }
    }

<?php
    
    $Arr = $$Table;
    $Values = array();

//    $dbFields = join(", ", $Arr['Fields']);
    $dbFields = "";
    foreach($Arr['Fields'] as $Key => $Field) {
        if($Field != "")
            $dbFields .= "$Field, ";
    }
    $dbFields = substr($dbFields, 0, -2);

    if ($_REQUEST['Update'] && !$Error) {
        include(EDIT_PATH."/update_one.php");
    }

    $Values = DB::selectArray("SELECT $dbFields, ".$Arr['Id']." FROM ".$Arr['TableName']." WHERE ".$Arr['Id']." = ".(int)$_REQUEST[$Arr['Id']]);

?>


    <div class="EditData" id="EditData">
    <fieldset>
    <legend class="Header"><?=$Arr['Header']?>: <b><?=$Values[$Arr['IdName']]?></b></legend>
    <form enctype="multipart/form-data" name="Main" id="MainForm" method="POST">

    <table width="100%" class="border">

        <?
            foreach($Arr['Fields'] as $Key => $Field) {
                $ClassName = $Arr['Types'][$Key];
                $Name = $Arr['Headers'][$Key];

                $InputValue = $Field == $Arr['Id'] ? (int)$_REQUEST[$Arr['Id']] : $Values[$Field];
                
                list($Type, $Null) = explode("/", $Arr['DBTypes'][$Key]);
                $InputValue = ($Type != "IGNORE") ? db_quote($InputValue) : $InputValue;

                eval("\$Input = new $ClassName(\$Field, \$InputValue);");
    
                echo "<tr>";
                if($Arr['Headers'][$Key])
                    echo "<td class=\"border\">".$Name."</td>";
                echo "<td class=\"border\" id=\"".$Field."_td\"".(!$Arr['Headers'][$Key] ? " colspan=2" : "").">";
                $Input->dPrint();
                echo "</td></tr>";
            }

            if(count($Arr['Buttons'])) {
                echo "<tr><td colspan=\"2\" class=\"border\">";
                foreach($Arr['Buttons'] as $Key => $ClassName) {
                    $Id = (int)$_REQUEST[$Arr['Id']];
                    eval("\$Input = new $ClassName($Id);");
                    $Input->dPrint();
                }
                echo "</td></tr>";
            }

            if($Arr['xUpload']) {
                ?>
                <tr>
                    <th colspan="2" class="border">Загрузка файла:</th>
                </tr>
                <tr>
                    <td class="border">Файл:</td>
                    <td class="border"><?php
                            $App = new InputApplet($Arr['xUpload'], $Values[$Arr['xUpload']], $Arr['CopyDir'], $Arr);
                            $App->dPrint();
                        ?></td>
                </tr>
                <?php
            }
        ?>

    </table>

<? if(!$Arr['ViewOnly'] && !$Arr['Params']) { ?>
    <input type="hidden" name="Update" value="1">
    <div id="SaveIt"><input type="submit" name="Update" value="Сохранить" class="green"></div>
<? } ?>

<? if(!$Arr['Params']) { ?>
    </form>
    </fieldset>
    </div>
<? } ?>
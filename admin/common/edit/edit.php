<?php


    class ModulesList {

        function GetModuleSections($ModuleName) {
            $FileName = SETTINGS_PATH."/$ModuleName"."_sections.php";

            if (file_exists($FileName)) {
                include($FileName);
                return $Sections;
            }
            else {
                return array();
            }
        }

        function dPrintSection($ModuleName, $SectionName) {
            echo "&nbsp;&nbsp;<img src=\"".ADMIN_URL."/images/module.gif\" align=\"absmiddle\" class=\"icon\"><a href=\"sec_private.php?mod=$ModuleName&sec=$SectionName\">$SectionName</a><br>\n";
        }

        function dPrintModuleSections($ModuleName) {
            $Sections = $this->GetModuleSections($ModuleName);
            while (list($Key, $Value) = each($Sections)) {
                $this->dPrintSection($ModuleName, $Value);
            }
        }

        function dPrintModule($ModuleName) {
            echo "<img src=\"".ADMIN_URL."/images/modules.gif\" align=\"absmiddle\" class=\"icon\"><a href=\"mod_public.php?mod=$ModuleName\" class=\"module-name\">$ModuleName</a><br>\n";
            $this->dPrintModuleSections($ModuleName);
        }

        function dPrint() {
            global $Modules;

            ?>
            <img src="<? echo ADMIN_URL; ?>/images/modules.gif"class="icon"><a href="mod_properties.php?mod=classPage" class="module-name">classPage</a><br>
            &nbsp;&nbsp;<img src="<? echo ADMIN_URL; ?>/images/module.gif"class="icon"><a href="sec_private.php?mod=classPage&sec=index">index</a><br>
            &nbsp;&nbsp;<img src="<? echo ADMIN_URL; ?>/images/module.gif"class="icon"><a href="sec_private.php?mod=classPage&sec=error">error</a><br>
            &nbsp;&nbsp;<img src="<? echo ADMIN_URL; ?>/images/module.gif"class="icon"><a href="sec_private.php?mod=classPopup&sec=popup">popup</a><br>
            &nbsp;&nbsp;<img src="<? echo ADMIN_URL; ?>/images/module.gif"class="icon"><a href="sec_private.php?mod=classPage&sec=printer">print</a><br>
            &nbsp;&nbsp;<img src="<? echo ADMIN_URL; ?>/images/module.gif"class="icon"><a href="sec_private.php?mod=classPDF">pdf</a><br>
            <?

            foreach($Modules as $Key => $Value) {
                $this->dPrintModule($Value);
            }
        }

    }



    class EditSections {
        public $ModuleName;
        public $Sections;

        function GetSectionsPath() {
            $FileName = SETTINGS_PATH."/".$this->ModuleName."_sections.php";
            return $FileName;
        }

        function SaveSections() {
            $Sections = "<?php\n\n  \$Sections = array(";

            if(sizeof($this->Sections)) {
                foreach($this->Sections as $Key => $Value) {
                    $Sections .= "\n    \"$Value\",";
                }
            }
            $Sections .= "\n  );\n";

            $fp = fopen($this->GetSectionsPath(), "w");
            fputs($fp, $Sections, strlen($Sections));
            fclose($fp);
        }

        function CheckSection($Name) {
            if(!in_array($Name, $this->Sections)) {
                array_push($this->Sections, $Name);
                $this->SaveSections();
            }
        }

        function DeleteSection($Name) {
            $Deleted = 0;
            foreach($this->Sections as $Key => $Value) {
                if ($Value == $Name) {
                    unset($this->Sections[$Key]);
                    $Deleted = 1;
                    break;
                }
            }
            if($Deleted) {
                $this->SaveSections();
            }
        }

        function EditSections($ModuleName) {
            $this->ModuleName = $ModuleName;

            $FileName = $this->GetSectionsPath();
            if (file_exists($FileName)) {
                include($FileName);
            }
            else {
                $Sections = array();
            }
            $this->Sections = $Sections;
        }
    }



    class EditProperties {
        var $Obj;
        var $Section;

        function dPrintFormTop() {

            echo "
<form action=\"".$_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']."\" method=\"post\" name=\"EditForm\">
<table class=\"PropertiesTable\">";
        }

        function dPrintFormBottom() {
            echo "
                 <tr>
                  <td>&nbsp;<input type=\"hidden\" name=\"Update\" value=\"1\"></td>
                  <td><input type=\"submit\" value=\"Сохранить\" class=\"green\"></td>
                 </tr>
                </table>
                </form>";
        }

        function dPrintProperty($Label, $Name, $Type, $Value) {
            eval("\$Input = new $Type('$Name', \$Value);");
            echo "
                 <tr>
                  <td align=\"right\" style=\"vertical-align:top\">$Label</td>
                  <td nowrap>";
                            $Input->dPrint();
                            echo "
                  </td>
                 </tr>";
        }

        function dPrintSeparator($Label="") {
            $Label = ($Label ? "<hr>$Label<br>" : "");
            echo "<tr><td colspan=\"2\" align=\"center\">$Label<hr></td></tr>";
        }

        function dPrint() {
            $classAbstractJS = new classAbstractJS();
            $classAbstractJS->dPrint();

            $this->dPrintFormTop();

            foreach($this->Obj["Names"] as $Key => $Name) {
                $Label = $this->Obj["Labels"]["$Key"];
                if($Name == "Separator") {
                    $this->dPrintSeparator($Label);
                } else {
                    $Type  = $this->Obj["Types"]["$Key"];
                    $Value = $this->Obj["Values"]["$Key"];
                    $this->dPrintProperty($Label, $Name, $Type, $Value);
                }
            }

            $this->dPrintFormBottom();
        }


        function GetArrayValue($arrValue) {
            if (is_array($arrValue)) {
                $Arr = "array( ";
                foreach($arrValue as $Key => $Value) {
                    $Arr .= "\"$Key\" => ".$this->GetArrayValue($Value).",";
                }
                $Arr = substr($Arr, 0, -1).")";
                return $Arr;
            }
            else {
                return "\"".db_quote($arrValue)."\"";
            }
        }

        function SaveSettings($Obj, $PropID, $sec) {
            if($PropID != "public" && !$sec)
                return;

            $PropertiesFileName = $Obj->GetPropertiesFile($PropID, $sec);
            $Names = $Obj->GetNames($PropID);

            $Settings = "";
            if(sizeof($Names)) {
                foreach($Names as $Key => $Value) {
                    if($Value != "Separator") {
                        if(isset($_POST[$Value])) {
                            $Settings .= "    \"$Value\" => ".$this->GetArrayValue($_POST[$Value]).",\n";
                        }
                    }
                }
            }
            $Settings = "<?php\n\n  \$Settings = array(\n".$Settings."  );\n";

            $fp = fopen($PropertiesFileName, "w");
            fputs($fp, $Settings, strlen($Settings));
            fclose($fp);
        }

        function EditProperties($Class, $PropID) {
            $this->Section = $_REQUEST['sec'] ?: $this->Section;

            eval("\$Obj = new $Class($this->Section);");

            if($_REQUEST["Update"]) {
                if($this->Section) {
                    $EditSections = new EditSections($Class);
                    $EditSections->CheckSection($this->Section);
                }
                $this->SaveSettings($Obj, $PropID, $this->Section);
            }

            eval("\$Obj = new $Class($this->Section);");
            $this->Obj = $Obj->Properties[$PropID];
        }

    }

  

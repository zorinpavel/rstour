<?php

    class EditDir extends EditProperties {

        function DelDir($Dir) {
            if(is_dir($Dir)) {
                rmdir($Dir);
            } else {
                echo "Error dir: ".$Dir;
                die();
            }
        }

        function DelFile($File) {
            unlink($File);
        }

        function CreateDir($Dir) {
            mkdir($Dir, 0755);
            chmod($Dir, 0755);
        }

        function MoveFile($From, $To="") {
            if ($To) {
                copy($From, $To);
                chmod($To, 0755);
            }
            $this->DelFile($From);
        }

        function MoveDir($From, $To = "") {
            if($To) {
                $this->CreateDir($To);
            }
            $Dir = dir($From);
            while ($Entry = $Dir->read()) {
                if ($Entry != "." && $Entry != "..") {
                    $FromEntry = "$From/$Entry";
                    $ToEntry = ($To ? "$To/$Entry" : "");
                    if (is_dir($FromEntry)) {
                        $this->MoveDir($FromEntry, $ToEntry);
                    }
                    elseif (is_file($FromEntry)) {
                        $this->MoveFile($FromEntry, $ToEntry);
                    }
                }
            }
            $Dir->close();
            $this->DelDir($From);
        }

        public function GetPath($Parent, $Dir) {
            while($Parent) {
                $Obj = new classPage($Parent);
                $Dir = $Obj->GetProperty("page_localdir")."/$Dir";
                $Parent = $Obj->GetProperty("page_parent");
            }
            return "$Dir";
        }

        function EditDir($PropID) {
            parent::EditProperties("classPage", $PropID);
        }
    }



    class EditPage extends EditDir {

        function dPrintFormTop() {
            parent::dPrintFormTop();
            echo "<input type=\"hidden\" name=\"page\" value=\"".$_REQUEST['page']."\">";
        }

        function CreateSection($Dir, $sec) {
            $this->CreateDir($Dir);
            $Page = "<?php\n\n  \$P = new classPage('$sec');\n  \$P->Action();\n";
            $Index = "$Dir/index.php";
            $fp = fopen($Index, "w");
            fputs($fp, $Page, strlen($Page));
            fclose($fp);
            chmod($Index, 0755);
        }

        function SaveSettings($Obj, $PropID, $sec) {

            $this->page_name = $_REQUEST["page_name"];
            $this->page_localdir = $_REQUEST["page_localdir"] ?: "";
            $this->page_parent = (int)$_REQUEST["page_parent"] ?: 0;
            $this->page_visible = (int)$_REQUEST["page_visible"];
            $this->page_access = (int)$_REQUEST["page_access"];

            $OldLocalDir = $Obj->GetProperty("page_localdir");
            $OldParent = $Obj->GetProperty("page_parent");
            $OldVisible = $Obj->GetProperty("page_visible");

            $DirPath = CLIENT_PATH."/".$this->GetPath($this->page_parent, $this->page_localdir);

            if($this->page_localdir == $OldLocalDir && $this->page_parent == $OldParent) {
                parent::SaveSettings($Obj, $PropID, $sec);

                DB::Query("UPDATE Sections SET Sec_Parent = ".$this->page_parent.", Sec_LocalDir = '".$this->page_localdir."', Sec_Name = '".$this->page_name."', Sec_Visible = ".$this->page_visible.", Sec_Access = ".$this->page_access." WHERE Sec_Code = ".(int)$sec);

            } elseif(file_exists($DirPath)) {
                $this->page_localdir = $OldLocalDir;
                $this->page_parent = $OldParent;
                $this->page_visible = $OldVisible;
            } else {
                parent::SaveSettings($Obj, $PropID, $sec);

                DB::Query("UPDATE Sections SET Sec_Parent = ".$this->page_parent.", Sec_LocalDir = '".$this->page_localdir."', Sec_Name = '".$this->page_name."', Sec_Visible = ".$this->page_visible.", Sec_Access = ".$this->page_access." WHERE Sec_Code = ".(int)$sec);

                if($this->page_localdir && $OldLocalDir) {
                    $this->page_localdir = $DirPath;
                    $OldLocalDir = $this->GetPath($OldParent, $OldLocalDir);

                    if ($this->page_localdir != $OldLocalDir || $this->page_parent != $OldParent) {
                        $this->MoveDir(CLIENT_PATH."/$OldLocalDir", $this->page_localdir);
                    }
                } elseif($this->page_localdir) {
                    $this->page_localdir = $DirPath;
                    $this->CreateSection($this->page_localdir, $sec);
                }
            }
        }

        function EditPage($PropID, $Sec_Code = 0) {
            $this->Section = $Sec_Code;
            parent::EditDir($PropID);
        }
    }

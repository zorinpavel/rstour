<?php

    class SectionsTree {
        var $Tree = array();
        var $MenuCookie = array();

        function dPrintTree($Code, $Name, $Level, $Sec_Parent=-1, $Visible, $Access) {
            $Code = (int)$Code;
            $Level = (int)$Level;
            $Sub = (int)count($this->Tree[$Code]);
            $PlusSection = ($Sub ? "<a href=\"#\" onClick=\"ShowSec({$Code},this);return false;\">".($Code > 0 && !in_array($Code, $this->MenuCookie) ? "<img src=\"".ADMIN_URL."/images/folder_p.gif\" class=\"icon\">" : "<img src=\"".ADMIN_URL."/images/folder_m.gif\" class=\"icon\">")."</a>"
                                 : (!$Access ? "<img src=\"".ADMIN_URL."/images/folder_access.png\" class=\"icon\">"
                                    : ($this->Tree['Type'][$Code] ? "<img src=\"".ADMIN_URL."/images/folder_link.png\" class=\"icon\">" : "<img src=\"".ADMIN_URL."/images/folder.gif\" class=\"icon\">")
                                   )
                           )."&nbsp;";
            $SpanSection_Begin = "<div id=\"Sec_{$Code}\" style=\"".($Code > 0 && !in_array($Code, $this->MenuCookie) ? "display:none;" : "")."\" >";
            $SpanSection_End = "</div>\n";
            $Indent = str_repeat("&nbsp;&nbsp;&nbsp;", $Level);

            if ($Visible == 0) $style = " style=\"color: #808080;\"";
            $secCode = $Code == 0 ? "index" : $Code;

            $Name = "<a href=\"page_properties.php?mod=classPage&sec=$secCode\"$style>$Name</a>";
            echo "<nobr>".$Indent.$PlusSection.$Name."</nobr><br>".$SpanSection_Begin;
            if(is_array($this->Tree[$Code])) {
                foreach($this->Tree[$Code] as $Key => $Name) {
                    $this->dPrintTree($Key, $Name, $Level+1, $Code, $this->Tree['Visible'][$Key], $this->Tree['Access'][$Key]);
                }
            }
            echo $SpanSection_End;
        }

        function dPrint() {
            $result = db_query("SELECT Sec_Code, Sec_Parent, Sec_Name, Sec_LocalDir, Sec_Visible, Sec_Access FROM Sections ORDER BY Sec_Order DESC, Sec_Code");
            $this->Tree = array();
            while (list($Sec_Code, $Sec_Parent, $Sec_Name, $Sec_LocalDir, $Sec_Visible, $Sec_Access) = mysql_fetch_row($result)) {
                $this->Tree[$Sec_Parent][$Sec_Code] = "<span class=\"section-name\">$Sec_Name</span> ($Sec_LocalDir)";

                $Page = new classPage($Sec_Code);
                $Sec_Type = $Page->GetProperty("page_type");
                if($Sec_Type)
                    $this->Tree['Type'][$Sec_Code] = $Sec_Type;

                $this->Tree['Visible'][$Sec_Code] = $Sec_Visible;
                $this->Tree['Access'][$Sec_Code] = $Sec_Access;
            }
            ?>
                <script type="text/javascript">var CookieName = "MenuSections";</script>
                <script type="text/javascript" src="<? echo ADMIN_URL; ?>/js/sectionstree.js"></script>
            <?
            $this->MenuCookie = @$_COOKIE['MenuSections'];
            $this->MenuCookie = explode('|',$this->MenuCookie);
            $this->dPrintTree(0, "Главная страница", 0, 0, 1, 1);
        }

        function SectionsTreeWithJs() {
        }

    }

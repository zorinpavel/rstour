<?php

    class classEditTemplates {

        var $Saved;
        var $New;

        var $Extended = array();

        function GetTemplateFile() {
            if($this->Lng) {
                return SETTINGS_PATH."/".$this->mod."_".$this->TemplatesName."_".$this->TemplateID."_".$this->Lng.".template";
            } else {
                return SETTINGS_PATH."/".$this->mod."_".$this->TemplatesName."_".$this->TemplateID.".template";
            }
        }

        function GetTemplate() {
            $FileName = $this->GetTemplateFile();
            if (file_exists($FileName)) {
                $Data = implode("", file($FileName));
                $Data = str_replace("&", "&amp;", $Data);
                $Data = str_replace("<textarea>", "&lt;textarea&gt;", $Data);
                $Data = str_replace("</textarea>", "&lt;/textarea&gt;", $Data);
            }
            else {
                $EvalStr = "\$Obj = new ".$this->mod."();";
                eval($EvalStr);
                $Data = $Obj->DefaultTemplates[$this->TemplatesName][$this->TemplateID];
            }
            return $Data;
        }

        function GetTemplatesFile() {
            return SETTINGS_PATH."/".$this->mod."_templates_".$this->TemplatesName.".php";
        }

        function GetCompiledTemplateFile($Lng = "") {
            if($Lng)
                $Lng = "_$Lng";
            return COMPILED_PATH."/".$this->mod.".".$this->TemplatesName.".".$this->TemplateID.$Lng.".php";
        }

        function GetTemplates() {
            $FileName = $this->GetTemplatesFile();
            if (file_exists($FileName)) {
                include($FileName);
            }
            else {
                $Data = array();
            }
            return $Data;
        }

        function dPrintTemplatesSelect() {
            $Templates = array();
            if($this->TemplateID) {
                $Templates = $this->GetTemplates();
            }
            if ($Templates[$this->TemplateID]) {
                echo "<select name=\"TemplateID\" onChange=\"window.location='templates.php?mod=".$this->mod."&TemplatesName=".$this->TemplatesName."&id='+document.Tmpls.TemplateID.options[document.Tmpls.TemplateID.selectedIndex].value".(DEFAULT_LANGUAGE ? "+'&lng='+document.Tmpls.lng.options[document.Tmpls.lng.selectedIndex].value" : "")."\">";

                while (list($Key, $Value) = each($Templates)) {
                    $Selected = ($Key == $this->TemplateID ? " selected" : "");
                    $TemplateName = ($Key == $this->TemplateID ? $Value : $TemplateName);
                    echo "<option value=\"$Key\"$Selected>$Value</option>";
                }
                echo "</select>";

                if(DEFAULT_LANGUAGE) {
                    echo "&nbsp;&nbsp;&nbsp;Язык: <select name=\"lng\" onChange=\"window.location='templates.php?mod=".$this->mod."&TemplatesName=".$this->TemplatesName."&id='+document.Tmpls.TemplateID.options[document.Tmpls.TemplateID.selectedIndex].value+'&lng='+document.Tmpls.lng.options[document.Tmpls.lng.selectedIndex].value\">";
                    foreach($this->TemplateLanguages as $Key => $Value) {
                        if($Key != DEFAULT_LANGUAGE)
                            echo "<option value=\"$Key\"".($Key == $this->Lng ? " selected" : "").">$Value</option>";
                    }
                    echo "</select>";
                }
                echo "<br>";
            } else {

                echo "<input type=\"hidden\" name=\"TemplateID\" value=\"0\">";
            }
            echo "<input type=\"text\" name=\"TemplateName\" value=\"$TemplateName\">";
        }

        function dPrint() {
            $parent_id = (int)$_REQUEST['parent_id'];
            if($parent_id) {
                $id = $this->TemplateID;
                $this->TemplateID = $parent_id;
                $TemplateBody = $this->GetTemplate();
                $this->TemplateID = $id;
            } else {
                $TemplateBody = $this->GetTemplate();
            }

            echo "
<table border=\"0\" cellpadding=\"0\" cellspacing=\"2\" width=100%>
<form action=\"".$_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']."\" method=\"POST\" name=\"Tmpls\">
 <tr>
  <td align=\"right\" valign=\"top\">Имя шаблона:</td>
  <td>
   <input type=\"hidden\" name=\"Update\" value=\"1\">
   <input type=\"hidden\" name=\"mod\" value=\"".$this->mod."\">
   <input type=\"hidden\" name=\"TemplatesName\" value=\"".$this->TemplatesName."\">";

            $this->dPrintTemplatesSelect();

            echo "
  </td>
 </tr>
 <tr>
  <td align=\"right\" valign=\"top\">Собственно&nbsp;шаблон:</td>
  <td valign=\"top\" width=100%><textarea name=\"TemplateBody\" id=\"TemplateBody\" cols=\"100\" rows=\"30\" wrap=\"off\" style=\"width:90%;height:90%;\">$TemplateBody</textarea></td>
 </tr>
 <tr>
  <td>&nbsp;</td>
  <td><input type=\"submit\" value=\"Сохраниить\" class=\"green\"></td>
</form>
</table>";

        }

        function SaveTemplate() {

            /*
                    $TemplateBody = str_replace("<?", "<pre>", $TemplateBody);
                    $TemplateBody = str_replace("?>", "</pre>", $TemplateBody);
            */

            $EvalStr = "\$Obj = new ".$this->mod."();";
            eval($EvalStr);

            $Default = 0;
            $Exists = 0;
            $lastID = 0;

            if(is_array($Obj->DefaultTemplates[$this->TemplatesName])) {
                foreach($Obj->DefaultTemplates[$this->TemplatesName] as $Key => $Template) {
                    $lastID = ($Key > $lastID ? $Key : $lastID);
                    if ($Key == $this->TemplateID)
                        $Default = 1;
                }
            }

            $Templates = $this->GetTemplates();
            foreach($Templates as $Key => $Template) {
                $lastID = ($Key > $lastID ? $Key : $lastID);
                if ($Key == $this->TemplateID)
                    $Exists = 1;
            }

            $this->Saved = 0;
            $this->New = 0;
            if($Default) {
                $this->TemplateID = 0;
            }

            if($_REQUEST['TemplateName'] || $this->TemplateID) {
                if(!$Exists) {
                    $this->New = ($Exists ? 0 : 1);
                    $this->TemplateID = $lastID + 1;
                }
                $Templates[$this->TemplateID] = $_REQUEST['TemplateName'];

                $ArrFile = "<?php\n\n    \$Data = array(\n";
                foreach($Templates as $Key => $Value) {
                    $ArrFile .= "        \"$Key\" => \"$Value\",\n";
                }
                $ArrFile = substr($ArrFile, 0, -1)."\n    );\n";

                $FileName = $this->GetTemplatesFile();
                $fp = fopen($FileName, "w");
                fputs($fp, $ArrFile, strlen($ArrFile));
                fclose($fp);
                chmod($FileName, 0755);

                $FileName = $this->GetTemplateFile();
                $fp = fopen($FileName, "w");
                fputs($fp, $_REQUEST['TemplateBody'], strlen($_REQUEST['TemplateBody']));
                fclose($fp);
                chmod($FileName, 0755);

                if(defined("COMPILED_PATH")) {
                    foreach($this->TemplateLanguages as $k => $v) {
                        $FileName = $this->GetCompiledTemplateFile($k);
                        if(file_exists($FileName))
                            @unlink($FileName);
                    }
                }

                $this->Saved = 1;
            }

        }

        function classEditTemplates() {
            $this->mod = $_REQUEST['mod'];
            $this->TemplatesName = $_REQUEST['TemplatesName'];
            $this->TemplateID = (int)$_REQUEST['id'];
            $this->Lng = $_REQUEST['lng'];

            global $TemplateSets;
            $this->TemplateLanguages = array_merge(array("" => ""), $TemplateSets);

            if($_REQUEST['Update']) {
                $this->SaveTemplate();
            }
        }
    }

<?php

    $dbquery = $Arr['Id']." = ".(int)$_REQUEST[$Arr['Id']].", ";

    $Arr['Uploads'] = is_array($Arr['Uploads']) ? $Arr['Uploads'] : array();

    foreach($Arr['Fields'] as $Key => $Field) {
        list($Type, $Null) = explode("/", $Arr['DBTypes'][$Key]);
        if(!in_array($Field, $Arr['Uploads']) && $Type != "IGNORE") {
            switch($Type) {
                case "INT":
                    $formValue = (int)$_REQUEST[$Field];
                    break;
                case "CHAR":
                case "DATE":
                default:
                    $formValue = "'".mysql_real_escape_string($_REQUEST[$Field])."'";
                    break;
            }
            if((!$formValue || $formValue == "''") && $Null)
                $formValue = "NULL";

            if($Type == "INT" || ($Null != "IGNORE" && ($formValue != 0 || $formValue != "")) )
                $dbquery .= $Field." = $formValue, ";

        }
    }
    $dbquery = substr($dbquery, 0, -2);

    foreach($Arr['Uploads'] as $Key => $Value) {
        UploadOne($Arr, $Value, $_FILES[$Value]['name'], $_FILES[$Value]['tmp_name'], $Arr['CopyDir']);
    }

    DB::Query("INSERT INTO ".$Arr['TableName']." SET $dbquery ON DUPLICATE KEY UPDATE $dbquery");



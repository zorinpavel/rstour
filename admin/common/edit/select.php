<div class="EditData">
    <fieldset>
        <legend class="Header"><? echo $Arr['Header']; ?></legend>

        <?

            if($_REQUEST['Update'])
                include(EDIT_PATH."/update.php");

            $FullFilter = 0;
            if(is_array($Filters) && sizeof($Filters)) {
                echo "<form id=\"Filter\" method=\"get\">";

                dPrintInputHidden("Part", $Part);


                foreach($Filters as $Key => $Value) {
                    ${$Value} = (int)${$Value};
                    $InputValue = $_REQUEST[$Value];
                    $ClassName = $FiltersTypes[$Key];

                    eval("\$Input = new $ClassName(\$Value, \$InputValue, \"this.form.submit();\");");

                    if($InputValue) {
                        $FullFilter++;
//                        $Compare = ($FiltersCompare[$Key] == "N") ? " = $InputValue" : ( ($FiltersCompare[$Key] == "IN" && is_array($Input->Values)) ? " IN (".join(", ", $Input->Values).")" : ( ($FiltersCompare[$Key] == "L") ? " LIKE '%$InputValue%'" : "" ) );
                        switch($FiltersCompare[$Key]) {
                            case "N":
                                $Compare = " = $InputValue";
                                break;
                            case "IN":
                                $Compare = " IN (".join(", ", $Input->Values).")";
                                break;
                            case "L":
                                $Compare = " LIKE '%$InputValue%'";
                                break;
                            case "DATEL":
                                $Compare = " <= '$InputValue'";
                                break;
                            case "DATEM":
                                $Compare = " >= '$InputValue'";
                                break;
                            case "DATEB":
//                                $Compare = " <= '".$Input->Values[0]."'";
//                                $Cond = fAddCondition("$Value$Compare", "AND", $Cond);
//                                $Compare = " >= '".$Input->Values[1]."'";
                                break;
                            default:
                                break;
                        }
                        if($FiltersJoin[$Key])
                            $Cond = fAddCondition($FiltersJoin[$Key]."$Compare", "AND", $Cond);
                        elseif($FiltersCompare[$Key])
                            $Cond = fAddCondition("$Value$Compare", "AND", $Cond);
                    }

                    /*
                          if ($InputValue) {
                            $FullFilter++;
                            $Compare = ($FiltersCompare[$Key]=="N" ? " = $InputValue" : " LIKE '%InputValue%'");
                            $Cond = fAddCondition("$Value$Compare", "AND", $Cond);
                          }
                    */

                    if($FiltersCompare[$Key])
                        echo "<i>".$FiltersNames[$Key].":</i> &nbsp; ";
                    $Input->dPrint();
                    echo " &nbsp; &nbsp; ";
                }

                echo "</form>";
            }

            if($Arr["Where"])
                $Cond = fAddCondition($Arr["Where"], "AND", $Cond);

            if($Cond)
                $Cond = " WHERE $Cond";

            $result = db_query("SELECT COUNT(*) FROM $TableName $Cond");
            list($Quantity) = mysql_fetch_row($result);
            $First = GetPartFirstElem($Part, $QuaEleForOutput) - 1;

            $query = "SELECT ".$Arr['Id'].",";
            foreach($Fields as $Key => $Value) {
                $query .= "$Value,";
            }
            $Qua = sizeof($Fields) + sizeof($Buttons);
            $Qua = ($Qua ? " colspan=\"$Qua\"" : "");
            $query = substr($query, 0, -1)." FROM $TableName $Cond ORDER BY ".$Arr['Order']." LIMIT $First, $Limit";
            $Result = DB::selectArray($query, $Arr['Id']);

            $i = 0;

            dPrint("<form enctype=\"multipart/form-data\" name=\"Main\" method=\"POST\">");
            dPrint("<table id=\"".$Arr['TableName']."\" class=\"border\">");
            dPrint("<tr>");
            foreach($Headers as $Key => $Value) {
                if ($Value)
                    dPrint("<th class=\"border\">$Value</th>");
            }
            foreach($ButtonHeaders as $Key => $Value)
                dPrint("<th class=\"border\">$Value</th>");
            dPrint("</tr>");

            foreach($Result as $Key => $Row) {
                $i++;

                dPrint("<tr id=\"".$Id."_".$Row[$Arr['Id']]."\" class=\"row_".$Row[$Arr['RowClassName']]."\">");
                dPrintInputHidden($Id."_$i", $Row[$Arr['Id']]);
                foreach($Fields as $fKey => $Value) {
                    if($Headers[$fKey])
                        dPrint("<td valign=\"top\" align=\"center\" class=\"border\" id=\"".$Value."_".$i."_td\">");

                    list($Type, $Null) = explode("/", $Arr['DBTypes'][$fKey]);
                    $Row[$Value] = ($Type != "IGNORE") ? db_quote($Row[$Value]) : $Row[$Value];

                    $EvalInputValue = $Row[$Value];
                    if($Arr['DBTypes'][$fKey] == "YEAR") {
                        $years = explode(",", $EvalInputValue);
                        if(count($years) > 1)
                            $EvalInputValue = $years[0]."-".$years[count($years)-1];
                    }
                    //      eval("\$Input = new ".$Types[$Key]."(\$EvalInputValue, \"".$Row[$fieldKey]."\");");
                    eval("\$Input = new ".$Types[$fKey]."(\"".$Value."_$i\", \$EvalInputValue);");
                    $Input->Row = $Row;
                    $Input->dPrint();

                    if($Headers[$fKey])
                        dPrint("  </td>");
                }

                foreach($Buttons as $bKey => $Value) {
                    dPrint("<td valign=\"top\" align=\"center\" class=\"border\" id=\"".$Value."_".$Row[$Arr['Id']]."\">");
                    eval("\$Button = new $Value(\"".$Row[$Arr['Id']]."\");");
                    $Button->Row = $Row;
                    $Button->dPrint();
                    dPrint("  </td>");
                }
                dPrint(" </tr>");
            }

            if(!$Arr["EditOnly"]) {
                $Row = array();
                if ($FullFilter == sizeof($Filters)) {
                    $MaxEmptyNumber = GetMaxEmptyNumber($i);
                    while ($i<$MaxEmptyNumber) {
                        $i++;
                        dPrint("<tr>");
                        dPrintInputHidden($Id."_$i", 0);
                        foreach($Fields as $Key => $Value) {
                            if ($Headers["$Key"])
                                dPrint("<td valign=\"top\" align=\"center\" class=\"border\">");
                            $EmptyValue = "";
                            foreach($Filters as $FKey => $FValue) {
                                if ($FValue == $Value) {
                                    $EmptyValue = $_REQUEST[$FValue];
                                    break;
                                }
                            }
                            eval("\$Input = new ".$Types[$Key]."(".$Value."_$i, \"$EmptyValue\");");
                            $Input->Row = $Row;
                            $Input->dPrint();
                            if ($Headers["$Key"])
                                dPrint("  </td>");
                        }
                        foreach($Buttons as $Key => $Value) {
                            dPrint("<td valign=\"top\" align=\"center\" class=\"border\">&nbsp;</td>");
                        }
                        dPrint("</tr>");
                    }
                }
            }

            dPrint("</table>");

            dPrintInputHidden("Quantity", $i);

            if(!$Arr["ViewOnly"]) {
                dPrint("<div id=\"SaveIt\">");
                dPrint("<input type=\"submit\" name=\"Update\" value=\"Сохранить\" class=\"green\">");
                dPrint("</div>");
            }

            dPrint("</form>");

            dPrintElementPartLinks($PHP_SELF, $Part, $Quantity);

        ?>

    </fieldset>

</div>
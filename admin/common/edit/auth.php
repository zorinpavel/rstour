<?php

    define(ADMIN_LOGIN, "5b7469905a5d0b934bea241ce552b9e2");
    define(ADMIN_PASS, "5d907853a9617cfd55fb62eae803595b");


    if(!isset($_SERVER['PHP_AUTH_USER'])) {
        auth();
    } else {
        getUser();
    }


    function auth() {
        header('Content-Type:	text/html; charset=utf-8');
        header('WWW-Authenticate: Basic realm="Input valid login"');
        header("HTTP/1.0 401 Unauthorized");
        echo "<h3>Доступ запрещен! Вы должны ввести правильные логин и пароль для доступа.</h3>";
        exit;
    }


    function getUser() {
        global $Sections, $SubSections, $CurrentSection;

        DB::getInstance();

        $CurrentSection = str_replace("/admin", "", dirname($_SERVER['PHP_SELF']));
        $CurrentSection = str_replace("/", "", $CurrentSection);

        $curSubSection = str_replace(dirname($_SERVER['PHP_SELF'])."/", "", $_SERVER['PHP_SELF']);
        $curSubSection = str_replace(".php", "", $curSubSection);

        $Sec_Code = $SubSections[$CurrentSection][$curSubSection]['Sec_Code'] ? $SubSections[$CurrentSection][$curSubSection]['Sec_Code'] : ($Sections[$CurrentSection]['Sec_Code'] ? $Sections[$CurrentSection]['Sec_Code'] : 1);

        $aulogin = mysql_real_escape_string($_SERVER['PHP_AUTH_USER']);
        $aupw = md5(mysql_real_escape_string($_SERVER['PHP_AUTH_PW']));

        if($Sec_Code == 1)
            $query = "SELECT au.us_id, au.login, aug.Gro_Code, 1 AS Sec_Code FROM AdmUsers AS au, AdmUserGroups AS aug WHERE (au.login = '$aulogin' AND au.passwd = '$aupw') AND aug.us_id = au.us_id";
        else
            $query = "SELECT au.us_id, au.login, aug.Gro_Code, ap.Sec_Code FROM AdmUsers AS au, AdmPermissions AS ap, AdmUserGroups AS aug WHERE (au.login = '$aulogin' AND au.passwd = '$aupw') AND aug.us_id = au.us_id AND aug.Gro_Code = ap.Gro_Code AND ap.Sec_Code = $Sec_Code";

        $Permissions = DB::selectArray($query);

        if(!$Permissions['us_id'] && !getEditAuth()) {
            //          if(is_array($SubSections[$CurrentSection])) {
            //              foreach($SubSections[$CurrentSection] as $Key => $SubSection) {
            //                  if($curSubSection != $Key) {
            //                      header("Location: ".$SubSections[$CurrentSection][$Key]['Path']);
            //                      $Location = true;
            //                      break;
            //                  }
            //              }
            //          }
//            if(!$Location)
                auth();
        }
    }


    function getEditAuth() {
        $auuser = md5(mysql_real_escape_string($_SERVER['PHP_AUTH_USER']));
        $aupw = md5(mysql_real_escape_string($_SERVER['PHP_AUTH_PW']));
        if( $auuser == ADMIN_LOGIN && $aupw == ADMIN_PASS ) {
            return true;
        } else {
            return false;
        }
    }


    class Permissions {

        public static $Permissions = false;

        public static function getPermissions($Sec_Code = 0) {
            $aulogin = mysql_real_escape_string($_SERVER['PHP_AUTH_USER']);
            $aupw = md5(mysql_real_escape_string($_SERVER['PHP_AUTH_PW']));

            $Cond = "(au.login = '$aulogin' AND au.passwd = '$aupw')";
            if(getEditAuth())
                $Cond = "true";

            if(!self::$Permissions)
                self::$Permissions = DBMem::selectArray("SELECT au.us_id, au.login, aug.Gro_Code, ap.Sec_Code FROM AdmUsers AS au INNER JOIN AdmUserGroups AS aug ON aug.us_id = au.us_id INNER JOIN AdmPermissions AS ap ON ap.Gro_Code = aug.Gro_Code WHERE $Cond", "Sec_Code", "permissions.".$aulogin, 500);

            if($Sec_Code) {
                if(getEditAuth())
                    self::$Permissions[$Sec_Code] = true;

                return self::$Permissions[$Sec_Code];
            }

            return self::$Permissions;
        }
    }

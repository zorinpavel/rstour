<?php

   include(EDIT_PATH."/auth.php");

   include(EDIT_PATH."/edit.php");
   include(EDIT_PATH."/editpages.php");
   include(EDIT_PATH."/edittemplates.php");
   include(EDIT_PATH."/abstractjs.php");

   include(EDIT_PATH."/sectionstree.php");

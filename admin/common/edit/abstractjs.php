<?php

  class classAbstractJS {
    var $Modules;

    function GetModuleSections($ModuleName) {
      $FileName = SETTINGS_PATH."/$ModuleName"."_sections.php";

      if (file_exists($FileName)) {
        include($FileName);
        return $Sections;
      }
      else {
        return array();
      }
    }

    function dPrint() {
      echo "
<script language=\"JavaScript\">
  function AddOption(theList, optValue, optLabel, Selected) {
    newOption = new Option(optLabel, optValue);
    optIndex = theList.options.length;
    theList.options[optIndex] = newOption;
    if (Selected) {
      theList.options[optIndex].selected = true;
    }
  }

  function DeleteOptions(theList) {
    while (theList.options.length) {
      theList.options[0] = null;
    }
  }

  blockSelected = new Array();

  function ChangeBlocks(className, Name) {
    BlocksName = Name + \"[blocks]\"
    theList = document.EditForm.elements[BlocksName];

    DeleteOptions(theList);
    optionSelected = (blockSelected[Name][className] != \"undefined\" ? blockSelected[Name][className] : \"\");";

      reset($this->Modules);
      while (list(, $ModuleName) = each($this->Modules)) {
        $Sections = $this->GetModuleSections($ModuleName);
        if (sizeof($Sections)) {

           echo "
    if (className == \"$ModuleName\") {
      AddOption(theList, \"\", \"(none)\", 0);";
           while (list($Key, $Section) = each($Sections)) {
             echo "
      AddOption(theList, \"$Section\", \"$Section\", \"$Section\" == optionSelected);";
           }
           echo "
    }";
        }
      }

      echo "
    blockSelected[Name][className] = -1;
  }
</script>

 ";

    }

    function classAbstractJS() {
      global $Modules;

      $this->Modules = $Modules;
    }
  }

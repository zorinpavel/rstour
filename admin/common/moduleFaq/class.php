<?php


    class classFaq extends classSecurity {

        public $ListScroll, $Lst_Code;

        var $Properties = array(
            "public" => array(
                "Names" => array(
                    "ListScroll",
                ),
                "Types" => array(
                    "InputScroll",
                ),
                "Labels" => array(
                    "Блок 'показывать по'",
                )
            ),
            "private" => array(
                "Names" => array(
                    "Limit",
                    "Form",
                    "isOutput",
                    "template_output",
                ),
                "Types" => array(
                    "T5",
                    "InputForm",
                    "InputYesNo",
                    "InputTemplates",
                ),
                "Labels" => array(
                    "Количество записей",
                    "Шаблоны вывода формы",
                    "Выводить записи",
                    "Шаблон вывода записи",
                )
            )
        );

        var $DefaultTemplates = array(
            "template_output" => array(0 => ""),
        );

        function GetClassName() {
            return __CLASS__;
        }

        public function Action() {
            if($this->GetProperty("isOutput") == 1) {
                $this->dPrintOut();
            }

            $this->Form->TabPrefix = "faq";
            $this->Form->Action();

            if(debug()) {
                ?><pre><? print_r($this->Data); ?></pre><?
            }

        }

        function dPrintOut() {
            $Lst_Code = $this->Lst_Code;
            $Limit = $this->Limit;
            if($Limit) {
                $Limit = "LIMIT $Limit";
            } elseif($this->ListScroll->Data["Limit"]) {
                $Limit = "LIMIT ".$this->ListScroll->Data["First"].", ".$this->ListScroll->Data["Limit"];
            } else $Limit = "";

            $this->ListScroll->Data['Quantity'] = DB::selectValue("SELECT count(*) FROM faqContent WHERE Lst_Code = $Lst_Code AND Con_State = 1 ORDER BY Con_Order,Con_Date");

            $this->Data["Records"] = DB::selectArray("SELECT * FROM faqContent WHERE Lst_Code= $Lst_Code AND Con_State = 1 ORDER BY Con_Order DESC,Con_Date DESC", "Con_Code");
            $this->Data['Fields'] = DB::selectArray("SELECT * FROM formFields WHERE Lst_Code = $Lst_Code ORDER BY Frm_Order", "Frm_Code");

            foreach($this->Data["Records"] as $Key => $Value) {
                $Data = $Value;

                if($Value['Ans_Data'] != '') {
                    $Data['isAnswer'] = 1;
                    $Data['AnswerDate'] = $this->mkToday($Value['Ans_Date']);
                    $Data['AnswerData'] = $Value['Ans_Data'];
                } else {
                    $Data['isAnswer'] = 0;
                }
                $Data['QuestionDate'] = $this->mkToday($Value['Con_Date']);

                $this->Data['Records'][$Key] = array_merge($Data, json_decode($Value["Con_Data"], true));

            }
            $this->Ins2Php("template_output");
        }

        function dPrintScroll() {
            $this->ListScroll->Action();
        }

        function classFaq($sec = "", $Parent = "") {
            parent::classSecurity($sec, $Parent);

            $this->Form = $this->GetBlock("Form");
            $this->Lst_Code = $this->Form->Lst_Code;

            $this->ListScroll = $this->GetBlock("ListScroll");
            $this->Limit = $this->GetProperty("Limit");

        }

    }

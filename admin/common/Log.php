<?php


    class Log {


        static $LogMode = false;
        static $LogFile = "default";

        static function AddLog($Data, $LogFile = false) {
            if(!self::$LogMode)
                return;

            $LogFile = $LogFile ?: self::$LogFile;
            $Data    = $Data ?: $_REQUEST;

            $logRow  = date("d.m.Y H:i:s")."\n";
            $logRow .= self::generateCallTrace();

            $logRow .= "\n";
            foreach($Data as $Key => $Value) {
                $logRow .= "$Key => $Value\n";
                if(is_array($Value)) {
                    foreach($Value as $vKey => $vValue)
                        $logRow .= "\t$vKey => $vValue\n";
                    if(is_array($vValue)) {
                        foreach($vValue as $vvKey => $vvValue)
                            $logRow .= "\t\t$vvKey => $vvValue\n";
                    }
                }
            }
            $logRow .= "\n\n";

            if($LogFile) {
                if(!is_dir(CLIENT_PATH."/logs/"))
                    mkdir(CLIENT_PATH."/logs/", 0775);

                $ft = fopen(CLIENT_PATH."/logs/".$LogFile.".log", "a");
                fputs($ft, $logRow);
                fclose($ft);
            } else {
                ?><pre><? echo $logRow; ?></pre><?
            }

            return $logRow;
        }


        static function generateCallTrace() {
            $e = new Exception();
            $trace = explode("\n", $e->getTraceAsString());
            // reverse array to make steps line up chronologically
            $trace = array_reverse($trace);
            array_shift($trace); // remove {main}
            array_pop($trace); // remove call to this method
            array_pop($trace); // remove call to Log method
            $length = count($trace);
            $result = array();

            for($i = 0; $i < $length; $i++) {
                $result[$i] = ($i + 1).substr($trace[$i], strpos($trace[$i], " "));
                $result[$i] = str_replace(CLIENT_PATH, "", $result[$i]);
            }

            return "\t" . implode("\n\t", $result);
        }


    }
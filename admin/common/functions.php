<?php


    function debug() {
        if( (in_array($_SERVER[HTTP_X_REAL_IP], array("127.0.0.1")) || isset($_REQUEST['debug']) || strpos($_SERVER['PHP_SELF'], "admin") !== false || strpos($_REQUEST['_path'], "admin") !== false ) && !$_REQUEST['undebug']){
            error_reporting(E_ALL & ~(E_NOTICE | E_STRICT));
            return true;
        } else {
            error_reporting(0);
            return false;
        }
    }


    function mysqldebug() {
        if(isset($_REQUEST['mysqldebug']))
            return true;

        return false;
    }


    function _autoload( $ClassName, $BlockName = "", $Parent = "", $isStatic = false ) {
        if(!class_exists($ClassName)) {
            $PathName = "MODULE_".strtoupper(str_replace("class", "", $ClassName))."_PATH";
            eval("\$PathVar = $PathName;");
            @include_once($PathVar."/export.php");
        }
        if( class_exists( $ClassName ) ) {
            if ( !$isStatic ) {
                eval("\$Obj = new $ClassName(\$BlockName, \$Parent);");
            } else {
                $Obj = call_user_func( array($ClassName, "getInstance") );
            }
            return $Obj;
        }
    }


    function db_quote($str) {
        $str = str_replace("\"", "&quot;", $str);
        $str = str_replace("'", "&#39;", $str);
        $str = str_replace("\\", "\\\\", $str);
        return $str;
    }


    function IsRobots() {
        $robots = "(google)|(yandex)|(scooter)|(stack)|(lycos)|(fast)|(rambler)|(metabot)|(nigma)|(bond)|(turtle)".
            "|(webalta)|(msn)|(altavista)|(aport)|(yahoo)|(bigmir)|(alltheweb)|(spider)|(bot)|(crawler)|(scanner)".
            "";
        if(preg_match("/{$robots}/msi",$_SERVER['HTTP_USER_AGENT'],$out)) return true;
        else
            return false;
    }


    function subWords($Data, $Stop, $Join = "") {
        $Str = "";
        $count = 0;
        $ArrData = explode(" ", strip_tags($Data));
        foreach($ArrData as $k => $v) {
            $count = $count + strlen($v);
            if($count <= $Stop) {
                $Str .= $v." ";
            } else {
                $Str = substr($Str, 0, -1).($Join ? " ".$Join : "");
                return $Str;
            }
        }
        return substr($Str, 0, -1);
    }
    
    /*
        Получить вариант слова со скланением
        wordForm(int, array('товар', 'товара', 'товаров'))
    */
    function wordForm($n, $forms) {
        return $n%10 == 1 && $n%100 != 11 ? $forms[0]:($n%10 >= 2 && $n%10 <= 4 && ($n%100 < 10 || $n%100 >= 20) ? $forms[1] : $forms[2]);
    }


    /*
        Обрезать окончание у слова
    */

    function cutWord($word) {
        $word = mb_strtolower($word);
        $sWord = preg_replace("/(\S{2,})(ого|ово|ых|ам|ов|ек|их|ах|ях|ель|ми|му|ом|ть|ев|ок|еб|ка|ки|ке|ся|ыс|ат|ют|ет|ым|ем|им|ам|ий|ая)$/", "$1", $word);
        $sWord = str_replace("нн", "н", $sWord);
        $sWord = preg_replace("/[аеёийоуыэюяьъск]+$/", "", $sWord);
        $sWord = trim($sWord);

        if(strlen($sWord) < 3)
            return false;

        return $sWord;
    }


    /*
        Выкидываем мусор из фразы
    */

    function dropWords($phrase) {
	    $reg = "/\s+(под|много|что|когда|где|или|которые|поэтому|все|будем|как)\s+/im";
	    $phrase = preg_replace($reg, "", $phrase);

	    return $phrase;
	}


	/*
        Сортируем массив
        sortArray(array(), array("name", "ASC"))
    */

    function sortArray() {
        $argv = func_get_args();
        $arguments = $argv[1];
        $array = $argv[0];
        $code = '';
        for ($c = 0; $c < count($arguments); $c += 2) {
            if (in_array($arguments[$c + 1], array("ASC", "DESC"))) {
//                $code .= 'return strcoll($a["'.$arguments[$c].'"], $b["'.$arguments[$c].'"]);';
                $code .= 'if ($a["'.$arguments[$c].'"] != $b["'.$arguments[$c].'"]) {';
                if ($arguments[$c + 1] == "ASC") {
                    $code .= 'return ($a["'.$arguments[$c].'"] < $b["'.$arguments[$c].'"] ? -1 : 1); }';
                } else {
                    $code .= 'return ($a["'.$arguments[$c].'"] < $b["'.$arguments[$c].'"] ? 1 : -1); }';
                }
            }
        }
        $code .= 'return 0;';
        $compare = create_function('$a, $b', $code);
        uasort($array, $compare);
        return $array;
    }


    /*
      Берем ссылку отрезая текущее значение и служебную приблуду
    */

    function GetLink($FilterID = "", $Value = false, $Replacement = array(), $Url = false) {
        $QueryString = $Url ?: $_SERVER['QUERY_STRING'];
//        $QueryString = $Url ?: $_SERVER['REQUEST_URI'];
        $QueryString = preg_replace("/(.*?)\?/", "", $QueryString);
        $QueryString = preg_replace("/\W?_path=([\w|\.|\-|%|\+|\/]*)/", "", $QueryString);

        $QueryString = preg_replace("/\Wc=([\w|\.|\-|%|\+|\/]*)/", "", $QueryString);
        $QueryString = preg_replace("/\Wm=([\w|\.|\-|%|\+|\/]*)/", "", $QueryString);
        $QueryString = preg_replace("/\Ws=([\w|\.|\-|%|\+|\/]*)/", "", $QueryString);

        if(count($Replacement)) {
            foreach($Replacement as $Key => $rValue) {
                $rValue = str_replace("[", "\[", $rValue);
                $rValue = str_replace("]", "\]", $rValue);
                $QueryString = preg_replace("/$rValue=([\w|\.|\-|%|\+]*)/", "", $QueryString);
            }
        }

        if($FilterID) {
            $rFilterID = str_replace("[", "\[", $FilterID);
            $rFilterID = str_replace("]", "\]", $rFilterID);
            $QueryString = preg_replace("/$rFilterID=([\w|\.|\-|%|\+]*)/", "", $QueryString);
        }

        $QueryString = preg_replace("/&{2,}/", "&", $QueryString);
        $QueryString = preg_replace("/^\?/", "", $QueryString);
        $QueryString = preg_replace("/^&/", "", $QueryString);
        $QueryString = preg_replace("/&$/", "", $QueryString);

        $PHP_SELF = str_replace("index.php", "", $_SERVER['PHP_SELF']);
        $PHP_SELF = str_replace("/setup/user.php", "", $PHP_SELF);
        $PHP_SELF = str_replace("/setup/parts.php", "", $PHP_SELF);
        $PHP_SELF = str_replace("/ajax.php", "", $PHP_SELF);

        $Lnk = $PHP_SELF.($_SERVER['REDIRECT_URL'] ?: "").($QueryString ? "?$QueryString" : "");

        if($Value)
            return $Lnk.($QueryString ? "&" : "?")."$FilterID=$Value";
        else
            return $Lnk;
    }


    /*
      Составляем имя файлов из кирилицы в латиницу
    */

    function Cyr2Lat($str, $lower = false) {

        if($lower)
            $str = mb_strtolower($str, "UTF-8");

        $str = str_replace(
            array(" "),
            array("-"),
            $str
        );

//        $str = str_replace(
//            array("+", "(", ")", "!", "'", "?", "/", ",", ".", ";", "*", "&"),
//            array("", "", "", "", "", "", "", "", "", "", "", ""),
//            $str
//        );

        $str = str_replace(
            array("ь", "ъ", "Ь", "Ъ"),
            array("", "", "", ""),
            $str
        );

        $str = str_replace(
            array("сх", "цх", "ех", "эх"),
            array("skh", "ckh", "ekh", "ehkh"),
            $str
        );

        $str = str_replace(
            array("а", "б", "в", "г", "д", "е", "з", "и", "к", "л", "м", "н", "о", "п", "р", "с", "т", "у", "ф", "ц", "ъ", "ы", "ь"),
            array("a", "b", "v", "g", "d", "e", "z", "i", "k", "l", "m", "n", "o", "p", "r", "s", "t", "u", "f", "c", "",  "y", ""),
            $str
        );

        $str = str_replace(
            array("А", "Б", "В", "Г", "Д", "Е", "З", "И", "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Ц", "Ъ", "Ы", "Ь"),
            array("A", "B", "V", "G", "D", "E", "Z", "I", "K", "L", "M", "N", "O", "P", "R", "S", "T", "U", "F", "C", "", "Y", ""),
            $str
        );

        $str = str_replace(
            array("э",  "х", "й", "ё",  "ж",  "ч",  "ш",  "щ",    "ю",  "я",  "Э",  "Х", "Й", "Ё",  "Ж",  "Ч",  "Ш",  "Щ",    "Ю",  "Я"),
            array("eh", "h", "j", "jo", "zh", "ch", "sh", "shch", "ju", "ja", "EH", "H", "J", "JO", "ZH", "CH", "SH", "SHCH", "JU", "JA"),
            $str
        );

        return preg_replace("/[^0-9-\w_]/", "", $str);
    }
    
    
    /*
      Переводим строку из латиницы в кириллицу
    */

    function Lat2Cyr($str) {
        $str = str_replace(
            array("skh", "ckh", "ekh", "ehkh"),
            array("сх", "цх", "ех", "эх"),
            $str
        );
    
        $str = str_replace(
            array("eh", "h", "j", "y", "jo", "zh", "ch", "sh", "shch", "ju", "ja", "EH", "H", "J", "Y", "JO", "ZH", "CH", "SH", "SHCH", "JU", "JA"),
            array("э",  "х", "й", "й", "ё",  "ж",  "ч",  "ш",  "щ",    "ю",  "я",  "Э",  "Х", "Й", "Й", "Ё",  "Ж",  "Ч",  "Ш",  "Щ",    "Ю",  "Я"),
            $str
        );

        $str = str_replace(
            array("a", "b", "v", "g", "d", "e", "z", "i", "k", "l", "m", "n", "o", "p", "r", "s", "t", "u", "f", "c", "",  "y", ""),
            array("а", "б", "в", "г", "д", "е", "з", "и", "к", "л", "м", "н", "о", "п", "р", "с", "т", "у", "ф", "к", "ъ", "ы", "ь"),
            $str
        );
    
        $str = str_replace(
            array("A", "B", "V", "G", "D", "E", "Z", "I", "K", "L", "M", "N", "O", "P", "R", "S", "T", "U", "F", "C", "", "Y", ""),
            array("А", "Б", "В", "Г", "Д", "Е", "З", "И", "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "К", "Ъ", "Ы", "Ь"),
            $str
        );
        
        return $str;
    }


    function Lat2Search($str) {
        $str = str_replace(
            array("Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P", "{", "}", "A", "S", "D", "F", "G", "H", "J", "K", "L", ":", "\"", "Z", "X", "C", "V", "B", "N", "M", "<", ">"),
            array("Й", "Ц", "У", "К", "Е", "Н", "Г", "Ш", "Щ", "З", "Х", "Ъ", "Ф", "Ы", "В", "А", "П", "Р", "О", "Л", "Д", "Ж", "Э", "Я", "Ч", "С", "М", "И", "Т", "Ь", "Б", "Ю"),
            $str
        );

        $str = str_replace(
            array("q", "w", "e", "r", "t", "y", "u", "i", "o", "p", "[", "]", "a", "s", "d", "f", "g", "h", "j", "k", "l", ";", "'", "z", "x", "c", "v", "b", "n", "m", ",", "."),
            array("й", "ц", "у", "к", "е", "н", "г", "ш", "щ", "з", "х", "ъ", "ф", "ы", "в", "а", "п", "р", "о", "л", "д", "ж", "э", "я", "ч", "с", "м", "и", "т", "ь", "б", "ю"),
            $str
        );

        return $str;
    }


    /*
      Из кириллицы в URL
    */

    function Cyr2Url($str) {
        $str = str_replace(
            array(" ", ",", "/"),
            array("_", "", "_"),
            $str
        );

        return $str;
    }


    /*
      Определяем язык браузера
    */

    function getPreferredLanguage() {
        global $TemplateSets;

        $langs = array();
        if(isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
            preg_match_all('/([a-z]{1,8}(-[a-z]{1,8})?)\s*(;\s*q\s*=\s*(1|0\.[0-9]+))?/i', $_SERVER['HTTP_ACCEPT_LANGUAGE'], $lang_parse);
            if(count($lang_parse[1])) {
                $langs = array_combine($lang_parse[1], $lang_parse[4]);
                $k = sizeof($langs);
                foreach($langs as $lang => $val) {
                    if($val === '') {
                        $langs[$lang] = 1;
                        $k--;
                    }
                }
                if($k)
                    arsort($langs, SORT_NUMERIC);
            }
        }
        foreach ($langs as $lang => $val) { break; }
        $lang = strtolower(mb_substr($lang, 0, 2));

        if(!in_array($lang, array_keys($TemplateSets)) || DEFAULT_LANGUAGE == "")
            $lang = DEFAULT_LANGUAGE ? DEFAULT_LANGUAGE : "ru";

        return $lang;
    }


    /*
      Опеределяем город
    */

    function getPreferredCity() {
        global $TemplateSets;
        $Data = classGeoIP::GetData();

        if($_REQUEST['LANG'] && in_array($_REQUEST['LANG'], array_keys($TemplateSets)))
            $Data['city'] = $_SESSION['LANG'] = $_REQUEST['LANG'];

        if($_SESSION['LANG'])
            return $_SESSION['LANG'];

        if(!in_array($Data['city'], array_keys($TemplateSets)) || DEFAULT_LANGUAGE == "")
            $lang = DEFAULT_LANGUAGE ?: "Moscow";
        else
            $lang = $Data['city'];

        return $lang;
    }


    /*
       Конвертируем даты
    */

    function dateDecode($encodedDate, $format = "Y-m-d") {
        return date($format, strtotime($encodedDate));
    }

    function dateEncode($decodedDate, $format = "d.m.Y") {
        return date($format, strtotime($decodedDate));
    }


    /*
        Генерим random string
    */

    function RandomString($length = 10, $String = false) {
        $characters = $String ?: "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $charactersLength = strlen($characters);
        $randomString = '';
        for($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }


    function insertAfter(&$input, $index, $newKey, $element) {
        if(!in_array($index, array_keys($input)))
            return $input;

        $tmpArray = array();
        foreach ($input as $key => $value) {
            $tmpArray[$key] = $value;
            if($key == $index)
                $tmpArray[$newKey] = $element;
        }

        $input = $tmpArray;
        return $input;
    }
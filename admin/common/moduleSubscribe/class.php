<?php


    class classSubscribe extends classSecurity {

        static $DoID = "sdo";
        public $DoId;

        var $Properties = array(
            "public" => array(
                "Names" => array(
                ),
                "Types" => array(
                ),
                "Labels" => array(
                )
            ),
            "private" => array(
                "Names" => array(
                    "Group",
                    "template_subscribe",
                    "template_un_subscribe",
                ),
                "Types" => array(
                    "InputGroup",
                    "InputTemplates",
                    "InputTemplates",
                ),
                "Labels" => array(
                    "Группа по умолчанию",
                    "Подписаться",
                    "Отписаться",
                )
            )
        );

        var $DefaultTemplates = array(
            "template_subscribe" => array(0 => ""),
            "template_un_subscribe" => array(0 => ""),
        );

        function GetClassName() {
            return __CLASS__;
        }

        function Action($Do = "") {
            $Do = $Do ?: $this->DoId;

            switch($Do) {
                case "subscribe":
                    $this->DoSubscribe();
                    $this->ShowForm();
                    break;
                case "show":
                default:
                    $this->ShowForm();
                    break;
            }

            if(debug()) {
                ?><pre><? print_r($this->Data); ?></pre><?
            }

        }


        public function ShowForm() {
            $this->Data['Error'] = count($this->Data['Errors']);
            $this->Data['Message'] = count($this->Data['Messages']);

            $this->Ins2Php("template_subscribe");
        }


        public function DoSubscribe() {
            $this->Data['email'] = mysql_real_escape_string($_REQUEST['email']);

            if(preg_match("/^(\w|\.|-)+?@(\w|-)+?\.\w{2,4}($|\.\w{2,4})$/i", trim($this->Data['email'])) && !$_REQUEST['spamfield']) {
                $name = DB::selectValue("SELECT u.name FROM Users AS u WHERE u.mail = '".$this->Data['email']."'");
                $us_id = DB::Query("REPLACE INTO subscribeUsers SET Us_Mail = '".$this->Data['email']."', Us_Name = ".($name ? "'$name'" : "NULL").", Us_Active = 1");
                DB::Query("REPLACE INTO subscribeUsersGroups SET us_id = $us_id, Gro_Code = ".$this->GetProperty("Group"));

                array_push($this->Data['Messages'], array("Message" => "Спасибо, вы успешно подписались"));

                unset($this->Data['email']);
            } else
                array_push($this->Data['Errors'], array("Error" => "Что-то не так с Вашим почтовым ящиком: ".$this->Data['email']));
        }


        public function classSubscribe($sec = "", $Parent = "") {
            parent::classSecurity($sec, $Parent);

            $this->Data['DoID'] = self::$DoID;
            $this->Data['DoId'] = $this->DoId = $_REQUEST[self::$DoID];

            $this->Data['Errors'] = array();
            $this->Data['Messages'] = array();
        }

    }

<?php
    
    
    class classPopup extends classPage {
        var $Obj;
        
        var $Properties = array(
            "public" => array(
                "Names" => array(
                ),
                "Types" => array(
                ),
                "Labels" => array(
                )
            ),
            "private" => array(
                "Names" => array(
                    "classID",
                    "moduleID",
                    "sectionID",
                    "page_popup",
                ),
                "Types" => array(
                    "T50",
                    "T50",
                    "T50",
                    "InputTemplates",
                ),
                "Labels" => array(
                    "Идентификатор класса",
                    "Идентификатор модуля",
                    "Идентификатор раздела",
                    "Шаблон выпадающего окна",
                )
            )
        );
        
        var $DefaultTemplates = array(
            "page_popup" => array( 0 => "" ),
        );
        
        function Headers() {
            if (method_exists($this->Obj, "SendHeaders")) {
                $this->Obj->sentHeaders();
            }
        }
        
        function HtmlParams() {
            if (method_exists($this->Obj, "InsertHtmlParams")) {
                $this->Obj->InsertHtmlParams();
            }
        }
        
        function Head() {
            if (method_exists($this->Obj, "InsertHead")) {
                $this->Obj->InsertHead();
            }
        }
        
        function BodyParams() {
            if (method_exists($this->Obj, "InsertBodyParams")) {
                $this->Obj->InsertBodyParams();
            }
        }
        
        function Body() {
            if (method_exists($this->Obj, "Action")) {
                $this->Obj->Action();
            }
        }
        
        function GetClassName() {
            return __CLASS__;
        }
        
        function Action() {
            $classID = $_REQUEST[$this->GetProperty("classID")];
            if ($classID != "classPopup") {
                $moduleID = $_REQUEST[$this->GetProperty("moduleID")];
                $sectionID = (int)$_REQUEST[$this->GetProperty("sectionID")];
                
                $EvalStr = "\$this->Obj = _autoload(\"$classID\", \"$moduleID\");";
                eval($EvalStr);
                
                $this->Ins2Php("page_popup");
            }
        }
        
        function classPopup($sec = "", $Parent = "") {
            parent::classPage($sec, $Parent);
        }
    }

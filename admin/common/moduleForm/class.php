<?php

    class classForm extends classProperties {

        var $Properties = array(
            "public" => array(
                "Names" => array(
                    "OField" => "OField",
                    "TimeStop" => "TimeStop",
                    "AttachTypes",
                    "MaxSizeAttach" => "MaxSizeAttach",
                    "AttachPath" => "AttachPath",
                ),
                "Types" => array(
                    "OField" => "T50",
                    "TimeStop" => "T50",
                    "T50",
                    "MaxSizeAttach" => "T50",
                    "AttachPath" => "T50",
                ),
                "Labels" => array(
                    "OField" => "Идентификатор поля",
                    "TimeStop" => "Задержка переж следующей отправкой <small>(мин.)</small>",
                    "Типы прикрепляемых файлов",
                    "MaxSizeAttach" => "Максимальный размер прикрепленного файла  <small>X*1024</small>",
                    "AttachPath" => "Путь к прикрепленным файлам",
                )
            ),
            "private" => array(
                "Names" => array(
                    "Lst_Code",
                    "UseMailList",
                    "adminmail" => "Frm_Mail",
                    "MailSubject",
                    "Capcha",
                    "template_top",
                    "checkmail" => "template_checkmail",
                    "template_text",
                    "template_textarea",
                    "template_checkbox",
                    "template_radio",
                    "template_select",
                    "template_upload",
                    "template_hidden",
                    "template_need",
                    "template_seporator",
                    "template_seporate",
                    "capcha" => "template_capcha",
                    "template_bottom",
                    "template_system",
                    "system_ok",
                    "Separator",
                    "isOutput",
                    "PostModerate",
                    "mailAll",
                    "template_output",
                    "mailTemplate",
                ),
                "Types" => array(
                    "formListDBSelect",
                    "InputYesNo",
                    "adminmail" => "T50",
                    "T50",
                    "InputYesNo",
                    "InputTemplates",
                    "checkmail" => "InputTemplates",
                    "InputTemplates",
                    "InputTemplates",
                    "InputTemplates",
                    "InputTemplates",
                    "InputTemplates",
                    "InputTemplates",
                    "InputTemplates",
                    "InputTemplates",
                    "InputTemplates",
                    "InputTemplates",
                    "capcha" => "InputTemplates",
                    "InputTemplates",
                    "InputTemplates",
                    "T50",
                    "",
                    "InputYesNo",
                    "InputYesNo",
                    "InputYesNo",
                    "InputTemplates",
                    "InputTemplates",
                ),
                "Labels" => array(
                    "Источник данных",
                    "Использовать список адресов?",
                    "adminmail" => "E-mail куда бедет отправлено письмо",
                    "Subject письма",
                    "Использовать капчу",
                    "Шаблон верха фомы",
                    "checkmail" => "Выбор адресата",
                    "Шаблон поля Text",
                    "Шаблон поля Textarea",
                    "Шаблон поля Checkbox",
                    "Шаблон поля Radio",
                    "Шаблон поля Select",
                    "Шаблон поля Upload",
                    "Шаблон поля Hidden",
                    "Обязательное поле",
                    "Объединяющее поле",
                    "Разделитель",
                    "capcha" => "Шаблон капчи",
                    "Шаблон низа формы",
                    "Шаблон ошибки",
                    "Сообщение при обработке формы",
                    "",
                    "Выводить записи",
                    "Премодерация",
                    "Отсылать целиком",
                    "Шаблон вывода записи",
                    "Шаблон письма",
                )
            )
        );

        var $DefaultTemplates = array(
            "template_top" => array(0 => "<form action=\"./\" method=\"POST\"><input type=\"hidden\" name=\"update\" value=\"1\">"),
            "template_text" => array(0 => "<ins type=\"data\" name=\"Label\"><ins type=\"function\" name=\"dPrintNeeded\">: <input type=\"text\" name=\"<ins type=\"data\" name=\"fieldName\">\" value=\"<ins type=\"data\" name=\"Value\">\"><br>"),
            "template_textarea" => array(0 => "<ins type=\"data\" name=\"Label\"><ins type=\"function\" name=\"dPrintNeeded\">: <textarea name=\"<ins type=\"data\" name=\"fieldName\">\"><ins type=\"data\" name=\"Value\"></textarea>\"><br>"),
            "template_checkbox" => array(0 => "<ins type=\"data\" name=\"Label\"><ins type=\"function\" name=\"dPrintNeeded\">: <input type=\"checkbox\" name=\"<ins type=\"data\" name=\"fieldName\">\" value=\"<ins type=\"data\" name=\"Value\">\"><br>"),
            "template_radio" => array(0 => "<ins type=\"data\" name=\"Label\"><ins type=\"function\" name=\"dPrintNeeded\">: <input type=\"radio\" name=\"<ins type=\"data\" name=\"fieldName\">\" value=\"<ins type=\"data\" name=\"Value\">\"><br>"),
            "template_select" => array(0 => "<ins type=\"data\" name=\"Label\">
<select name=\"<ins type=\"data\" name=\"fieldName\">\">
  <whileins data=\"List\">
    <option name=\"<ins type=\"data\" name=\"fieldName\">[<ins type=\"data\" name=\"LVa_Code\">]\" value=\"<ins type=\"data\" name=\"LVa_Value\">\" /><ins type=\"data\" name=\"LVa_Header\"></option>
  </whileins>
</select>"),
            "template_upload" => array(0 => "<input name=\"<ins type=\"data\" name=\"fieldName\">\" type=\"file\"><br>"),
            "template_hidden" => array(0 => "<input name=\"<ins type=\"data\" name=\"fieldName\">\" type=\"hidden\" value=\"<ins type=\"data\" name=\"LVa_Value\">\"><br>"),
            "template_bottom" => array(0 => "<input type=\"submit\"></form>"),
            "template_need" => array(0 => "<font color=\"red\">*</font>"),
            "template_seporator" => array(0 => "<b><ins type=\"data\" name=\"Label\"></b><br>"),
            "template_seporate" => array(0 => ""),
            "template_system" => array(0 => "<whileins data=\"Errors\"><ins type=\"data\" name=\"Error\"><br></whileins>"),
            "template_output" => array(0 => ""),
            "template_capcha" => array(0 => "Введите код: <img src=\"/wr_image.php\" align=\"absmiddle\"> <input type=\"text\" name=\"verify_code\" size=\"6\" maxlength=\"6\">"),
            "mailTemplate" => array(0 => "<b><ins type=\"variable\" name=\"Subject\"></b><br><br>\n<ins type=\"variable\" name=\"Message\">"),
            "template_checkmail" => array(0 => "<ins type=\"data\" name=\"Label\">
<select name=\"AdminMail\">
  <whileins data=\"AdminMails\">
    <option value=\"<ins type=\"data\" name=\"Fm_Code\">\"><ins type=\"data\" name=\"Fm_Name\"></option>
  </whileins>
</select>"),
        );

        function GetClassName() {
            return __CLASS__;
        }

        function Action() {

            $this->Lst_Code = $this->Lst_Code ? $this->Lst_Code : $this->GetProperty("Lst_Code");
            $this->TabPrefix = $this->TabPrefix ?: "form";
            $OField = $this->GetProperty("OField");

            $resList = DB::selectArray("SELECT * FROM formFields WHERE Lst_Code = ".$this->Lst_Code." AND Frm_Active = 1", "Frm_Code");
            $resList = SortArray($resList, array("Frm_Order", "DESC", "Frm_Code", "ASC"));

            if($this->GetProperty("isOutput") == 1) {
                $this->dPrintOut($this->Lst_Code);
            }

            if($this->Data['MailList']) {
                $this->Data['AdminMails'] = DB::selectArray("SELECT * FROM formMails WHERE Fm_Active = 1 AND Lst_Code = ".$this->Lst_Code, "Fm_Code");
                $this->Data['AdminMails'] = SortArray($this->Data['AdminMails'], array("Fm_Order", "DESC", "Fm_Code", "ASC"));
            }
//            $this->Data['AdminMails'][0] = array("Fm_Code" => 0, "Fm_Name" => "..", "Fm_Order" => 100000);

            if($_REQUEST["update"]) {
                $Con_State = ($this->GetProperty("PostModerate") ? 0 : 1);
                $Q1 = "Lst_Code, Con_Date, Con_State";
                $Q2 = "'".$this->Lst_Code."', NOW(), '$Con_State'";

                // Проверка на правильность заполения

                if($this->MailList && !(int)$_REQUEST['AdminMail']) {
                    array_push($this->Data["Errors"], array("Error" => "Вы не выбрали, кому хотите отправить сообщение!"));
                }

                foreach($resList as $Key => $Value) {
                    if($resList[$Key]["Frm_Need"] == 1 && !$_REQUEST[$resList[$Key]["Frm_Id"]] && $resList[$Key]["Frm_Type"] != 6) {
                        array_push($this->Data["Errors"], array("Error" => "Не заполнены обязательное поле ".$resList[$Key]["Frm_Field"].""));
                    }
                    if($resList[$Key]["Frm_Type"] == 1 && strlen($_REQUEST[$resList[$Key]["Frm_Id"]]) > 250) {
                        array_push($this->Data["Errors"], array("Error" => "Слишком много данных для текстогого поля ".$resList[$Key]["Frm_Field"].""));
                    }
                    if($resList[$Key]["Frm_Type"] == 2 && strlen($_REQUEST[$resList[$Key]["Frm_Id"]]) > 1050) {
                        array_push($this->Data["Errors"], array("Error" => "Слишком много данных для большого текстогого поля ".$resList[$Key]["Frm_Field"].""));
                    }
                    if($resList[$Key]["Frm_Type"] == 6 && $resList[$Key]["Frm_Need"] == 1 && !$_FILES[$resList[$Key]["Frm_Id"]]) {
                        array_push($this->Data["Errors"], array("Error" => "Не заполнены обязательное поле ".$resList[$Key]["Frm_Field"].""));
                    }
                    if($resList[$Key]["Frm_Type"] == 6 && $_FILES[$resList[$Key]["Frm_Id"]]['size'] > ($this->GetProperty("MaxSizeAttach")*1024)) {
                        array_push($this->Data["Errors"], array("Error" => "Допустимый размер прикрепленного файла ".$this->GetProperty("MaxSizeAttach")." Кбайт"));
                    }
                    if($resList[$Key]["Frm_Type"] == 6) {
                        $Parts = explode(".", $_FILES[$resList[$Key]['Frm_Id']]['name']);
                        $i = sizeof($Parts) - 1;
                        if($i > 0) {
                            $Ext = strtolower($Parts[$i]);
                            $Extensions = $this->GetProperty("AttachTypes");
                            if (!stristr($Extensions, $Ext)) {
                                $this->Data['Error'] = 1;
                                array_push($this->Data["Errors"], array("Error" => $Ext." - недопустимый тип прикрепленного файла"));
                            } else {
                                $this->UploadFile($_FILES[$resList[$Key]['Frm_Id']]['name'], $_FILES[$resList[$Key]["Frm_Id"]]['tmp_name']);
                                $Files[$resList[$Key]['Frm_Id']] = $_FILES[$resList[$Key]['Frm_Id']]['name'];
                            }
                        } else {
                            array_push($this->Data["Errors"], array("Error" => "Недопустимый тип прикрепленного файла"));
                        }
                    }
                }
    
                if($this->Capcha) {
//                if(!$_REQUEST['verify_code'] || strtoupper($_REQUEST['verify_code']) != strtoupper($_SESSION['verify_code'])) {
//                    array_push($this->Data["Errors"], array("Error" => self::$EnterErrors[15]));
//                }
        
                    // Google captcha
                    $link = "https://www.google.com/recaptcha/api/siteverify";
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $link);
                    curl_setopt($ch, CURLOPT_HEADER, false);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_TIMEOUT, 1);
                    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, "secret=6LfctR0UAAAAAFP2gwXssB3u0PyCohsdbMLfN2_U&response=".$_REQUEST['g-recaptcha-response']);
                    $string = curl_exec($ch);
                    $data['curl_errno'] = curl_errno($ch);
                    if(!curl_errno($ch) && !strpos($string, "503")) {
                        $data = json_decode($string, true);
                    }
                    curl_close($ch);
        
                    if(!$data['success'])
                        array_push($this->Data["Errors"], array("Error" => "Не правильно введен код с изображения"));
                }

//                if($this->Capcha && (!$_REQUEST['verify_code'] || strtoupper($_REQUEST['verify_code']) != $_SESSION['verify_code']) ) {
//                    array_push($this->Data["Errors"], array("Error" => "Не правильно введен код с изображения"));
//                }


                foreach($resList as $Key => $Value) {
                    if($_REQUEST[$resList[$Key]["Frm_Id"]])
                        $req[$resList[$Key]["Frm_Id"]] = $_REQUEST[$resList[$Key]["Frm_Id"]];
                }

                if($Files) {
                    foreach($Files as $fid => $fnm) {
                        $fnm = Cyr2Lat($fnm);
                        $req[$fid] = $fnm;
                    }
                }

                $Value = json_encode($req);

                $Q1 .= ", Con_Data";
                $Q2 .= ", '".mysql_real_escape_string($Value)."'";
                $query = "INSERT INTO ".$this->TabPrefix."Content ($Q1) VALUES ($Q2)";
                $Ite_Code = mysql_insert_id();

                $timeStop = $this->getProperty("TimeStop");
                if($_SESSION[$this->GetClassName()] > (time() - 60 * $timeStop) && $timeStop > 0) {
                    $min = ceil(($_SESSION[$this->GetClassName()] + (60 * $timeStop) - time()) / 60);
                    array_push($this->Data["Errors"], array("Error" => "В следующий раз вы сможете отправить данные через $min минут"));
                }

                if(count($this->Data['Errors']))
                    $this->Data['Error'] = 1;
                
                if(!$this->Data['Error']) {
                    DB::Query($query);

                    $_SESSION[$this->GetClassName()] = time();

                    if($this->Data['MailList']) {
                        $AdminEmail = $this->Data['AdminMails'][(int)$_REQUEST['AdminMail']]['Fm_Mail'];
                    } else {
                        $AdminEmail = $this->GetProperty("Frm_Mail");
                    }

                    if($this->GetProperty("mailAll")) {
                        $Text = "";
                        foreach($resList as $Key => $Value) {
                            $Val = $this->GetPostField($req[$resList[$Key]["Frm_Id"]]);
                            if ($resList[$Key]['Frm_Type'] == 6) {
                                $Attach = $resList[$Key]['Frm_Id'];
                                $Text .= "".$resList[$Key]['Frm_Field'].":<br><img src=\"".DOMAIN_URL.$this->GetProperty("AttachPath").$Val."\"><br>";
                            } elseif ($resList[$Key]['Frm_Type'] == 7) {
                                $Text .= "<b>".$resList[$Key]['Frm_Field']."</b><br>";
                            } else {
                                if(is_array($Val)) {
                                    $Text .= "".$resList[$Key]['Frm_Field'].":";
                                    foreach($Val as $k => $v) {
                                        $Text .=  "<i>".$v."</i>;";
                                    }
                                    $Text .= "<br>";
                                } else {
                                    $Text .= "".$resList[$Key]['Frm_Field'].": <i>".$Val."</i><br>"; //iconv("UTF-8", "KOI8-R", $Val)
                                }

                            }
                        }
                        $Text .= "\n\n<a href=\"".DOMAIN_URL."/admin/".$this->TabPrefix."/content.php\">".DOMAIN_URL."/admin/".$this->TabPrefix."/content.php</a>";
                    } else {
                        $Text = $this->GetProperty("MailSubject")."\n\n<a href=\"".DOMAIN_URL."/admin/".$this->TabPrefix."/content.php\">".DOMAIN_URL."/admin/".$this->TabPrefix."/content.php</a>";
                    }

                    $Subject = $this->GetProperty("MailSubject");

                    $Mail = _autoload("classMail");

                    $External = array(
                        "Subject" => $Subject,
                        "Message" => $Text,
                    );

                    $Template = $this->GetTemplate("mailTemplate");
                    $MessageHtml = $Mail->MailTemplate2Php($Template, $External);

                    $Mail->SendMail($AdminEmail, $Subject, $MessageHtml);

                    if(count($Mail->Errors)) {
                        foreach($Mail->Errors as $k => $v) {
                            array_push($this->Data["Errors"], array("Error" => $Mail->Errors[$k]));
                        }
                    } else {
                        array_push($this->Data["Messages"], array("Message" => $this->GetProperty("system_ok")));
                    }

                    if(count($this->Data['Errors']))
                        $this->Data['Error'] = 1;

                    if(count($this->Data['Messages']))
                        $this->Data['Message'] = 1;

                }
            }
    
            $this->dPrintForm($resList);
    
        }

        function UploadFile($Name, $TmpFile) {
            $CopyDir = $this->GetProperty("AttachPath");
            //составим имя
            $filename = "$CopyDir".Cyr2Lat($Name);

            move_uploaded_file($TmpFile, CLIENT_PATH.$filename);
        }

        function dPrintOut($Lst_Code) {
    
            $this->Data["Records"] = DB::selectArray("SELECT Con_Date, Con_Data FROM ".$this->TabPrefix."Content WHERE Lst_Code='$Lst_Code' AND Con_State='1' ORDER BY Con_Order,Con_Date", "Con_Code");
            $fieldsLabel = DB::selectArray("SELECT Frm_Field, Frm_Id, Frm_Type FROM formFields WHERE Lst_Code='$Lst_Code' ORDER BY Frm_Order");

            foreach($this->Data["Records"] as $Key => $Value) {
    
                $Data = json_decode($this->Data["Records"][$Key]["Con_Data"], true);

                $this->Data["Records"][$Key]["Fields"] = array();
                foreach($Data as $fk => $fv) {
                    if(is_array($Data[$fk])) {
                        $Fields["Value"] = "";
                        $c = 1;
                        foreach($Data[$fk] as $k => $v) {
                            $Fields["Value"] .= $v;
                            if($c != count($Data[$fk])) {
                                $Fields["Value"] .= ", ";
                            }
                            $c++;
                        }
                    } else {
                        $Fields["Value"] = $Data[$fk];
                    }
                    foreach($fieldsLabel as $flk => $flv) {
                        if($fieldsLabel[$flk]["Frm_Id"] == $fk) {
                            $Fields["Type"] = $fieldsLabel[$flk]["Frm_Type"];
                            $Fields["Label"] = $fieldsLabel[$flk]["Frm_Field"];
                        }
                    }
                    array_push($this->Data["Records"][$Key]["Fields"], $Fields);
                }
            }
            $this->Ins2Php("template_output");
        }

        function dPrintForm($resList) {
            if(count($this->Data["Errors"]) || count($this->Data['Messages']))
                $this->Ins2Php("template_system");

            $this->Ins2Php("template_top");
            if($this->MailList)
                $this->Ins2Php("template_checkmail");

            foreach($resList as $Key => $Value) {
                $this->Data = $Value;
                $this->Data[$this->GetProperty("OField")] = $this->Data["Frm_Id"];
                $this->Data["Label"] = $this->Data["Frm_Field"];
                if ($this->Data["Frm_Type"] == 1) {
                    $this->Data["Value"] = $this->GetPostField($_REQUEST[$this->Data["Frm_Id"]]);
                    $this->Ins2Php("template_text");
                }
                elseif($this->Data["Frm_Type"] == 2) {
                    $this->Data["Value"] = $this->GetPostField($_REQUEST[$this->Data["Frm_Id"]]);
                    $this->Ins2Php("template_textarea");
                }
                elseif($this->Data["Frm_Type"] == 3) {
                    fieldListDBSelect::fieldListSelect($this->Data["Frm_List"], "template_select", !$this->Data['Frm_Need']);
                }
                elseif($this->Data["Frm_Type"] == 4) {
                    fieldListDBSelect::fieldListSelect($this->Data["Frm_List"], "template_checkbox");
                }
                elseif($this->Data["Frm_Type"] == 5) {
                    fieldListDBSelect::fieldListSelect($this->Data["Frm_List"], "template_radio");
                }
                elseif($this->Data["Frm_Type"] == 6) {
                    fieldListDBSelect::fieldListSelect($this->Data["Frm_List"], "template_upload");
                }
                elseif($this->Data["Frm_Type"] == 7) {
                    $this->Data["Value"] = "";
                    $this->Ins2Php("template_seporator");
                }
                elseif($this->Data["Frm_Type"] == 8) {
                    $this->Data["Value"] = $this->GetPostField($_REQUEST[$this->Data["Frm_Id"]]);
                    $this->Ins2Php("template_hidden");
                }
                else {}
                $this->Ins2Php("template_seporate");
            }
            if($this->Capcha)
                $this->Ins2Php("template_capcha");

            $this->Ins2Php("template_bottom");
        }

        function classForm($sec = "", $Parent = "") {

            // значение по умолчанию
            $this->Properties['public']['Values']['MaxSizeAttach'] = 1;
            $this->Properties['public']['Values']['AttachPath'] = "/data/mail/";
            $this->Properties['public']['Values']['OField'] = "fieldName";
            $this->Properties['public']['Values']['TimeStop'] = "5";

            parent::classProperties($sec, $Parent);

            $this->Capcha = $this->GetProperty("Capcha");
            if(!$this->Capcha)
                unset($this->Properties['private']['Names']['capcha']);

            $this->MailList = $this->Data['MailList'] = $this->GetProperty("UseMailList");
            if($this->MailList)
                unset($this->Properties['private']['Names']['adminmail']);
            else
                unset($this->Properties['private']['Names']['checkmail']);

            $this->Data["Errors"] = array();
            $this->Data["Messages"] = array();

        }

        function dPrintNeeded() {
            if ($this->Data["Frm_Need"]) {
                $this->Ins2Php("template_need");
            }
        }

        function GetPostField($Var) {
            $Var = str_replace("<", "&lt", $Var);
            $Var = str_replace(">", "&gt", $Var);
            $Var = db_quote($Var);
            return $Var;
        }

    }


<?php

    class formListDBSelect extends InputDBSelect {
        function formListDBSelect($Name, $Value, $onChange="") {
            $this->InputDBSelect($Name, $Value, "formList", "Lst_Code", "Lst_Name", "", "Lst_Order, Lst_Code", "", $onChange);
        }
    }


    class fieldListDBSelect extends classProperties {
        public function fieldListSelect($Lst_Code, $template_name, $Empty = false) {
            $ListArr = DB::selectArray("SELECT LVa_Code, LVa_Header, LVa_Value FROM contentListValues WHERE Lst_Code = $Lst_Code", "LVa_Code");
            $ListArr = sortArray($ListArr, array("LVa_Order", "DESC", "LVa_Code", "ASC"));

            $this->Data["List"] = array();
            if($Empty)
                $this->Data['List'][0] = array("LVa_Header" => "..", "LVa_Value" => 0);

            foreach($ListArr as $LVa_Code => $List) {
                if(is_array(@$_REQUEST[$this->Data["Frm_Id"]])) {
                    if($_REQUEST[$this->Data["Frm_Id"]][$List["LVa_Code"]] == $List["LVa_Value"]) {
                        $List["checked"] = 1;
                    }
                    else {
                        $List["checked"] = 0;
                    }
                } else {
                    if(@$_REQUEST[$this->Data["Frm_Id"]] == $List["LVa_Value"]) {
                        $List["checked"] = 1;
                    }
                    else {
                        $List["checked"] = 0;
                    }
                }

                $this->Data["List"][$LVa_Code] = $List;
            }

            $this->Ins2Php($template_name);
        }
    }

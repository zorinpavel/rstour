<?php

  class classHTML extends classProperties {
    var $Properties = array(
      "public" => array(
        "Names" => array(
        ), 
        "Types" => array(
        ), 
        "Labels" => array(
        )
      ),
      "private" => array(
        "Names" => array(
          "html_template"
        ), 
        "Types" => array(
          "InputTemplates"
        ), 
        "Labels" => array(
          "Шаблон для вывода"
        )
      )
    );

    var $DefaultTemplates = array(
      "html_template" => array(0 => ""), 
    );

    function GetClassName() {
      return "classHTML";
    }

    function Action() {
      $this->Ins2Php("html_template");
    }

    function classHTML($sec = "", $Parent = "") {
      parent::classProperties($sec, $Parent);
    }
  }

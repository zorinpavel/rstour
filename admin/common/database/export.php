<?php

    if(!function_exists("mysql_connect"))
        include(DATABASE_PATH."/mysqli.php");
    else
        include(DATABASE_PATH."/mysql.php");

    if(class_exists("Memcache"))
        include(DATABASE_PATH."/memcache.php");
    elseif(class_exists("Memcached"))
        include(DATABASE_PATH."/memcached.php");
    elseif(class_exists("Redis"))
        include(DATABASE_PATH."/redis.php");
    else
        include(DATABASE_PATH."/files.php");

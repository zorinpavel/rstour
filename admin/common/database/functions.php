<?php

    function db_err($handle, $message) {
        DB::getLastError( $message );
    }

    function db_query( $query ) {
        return DB::Query( $query );
    }

    class DB {
        const ENC = 'utf8';

        private static $_connection = null;
        private static $_lastInsert = null;
        private static $_queryCnt;

        public static $_lastQuery;
        public static $_cntFields = null;
        public static $_cntRows = null;

        public static function getInstance() {
            if ( !self::$_connection ) {
                self::$_connection = mysql_connect( DB_HOST, DB_USER, DB_PASSWORD );
                if ( !self::$_connection ) {
                    die( "Ошибка соединения с базой данных. Мы уже знаем о проблеме и наши специалисты делают все возможное." );
                }
                mysql_query( "SET NAMES '".self::ENC."';" );
                self::selectDB();
                return self::$_connection;
            }
            return self::$_connection;
        }

        public static function selectDB($DB_Name = "") {
            $DB_Name = $DB_Name ? $DB_Name : DB_NAME;
            mysql_select_db( $DB_Name, self::$_connection );
        }

        public static function Query( $query, $key = NULL ) {
            $result = self::mysqlQuery( $query );
            if ( strpos($query, "NSERT INTO") ) {
                $result = @mysql_insert_id();
            } elseif( strpos($query, "PDATE") || strpos( $query, "ELETE" ) || strpos( $query, "EPLACE" ) ) {
                $result = @mysql_affected_rows();
            } elseif( strpos($query, "LTER" ) ) {
                $result = @mysql_error( self::getInstance() );
            }
            return $result;
        }

        public static function selectValue($query, $keyName = false) {
            list($Value) = mysql_fetch_row(self::Query($query));
            return $Value;
        }

        public static function selectArray($query, $keyName = null, $key = NULL, $time = 0) {
            $result = array();
            $tmp = self::mysqlQuery( $query );
            $rowcnt = mysql_num_rows( $tmp );

            if ( $rowcnt == 1 && is_null( $keyName ) ) {
                $result = mysql_fetch_assoc( $tmp );
            } elseif( $rowcnt > 1 || !is_null($keyName) ) {
                while( $row = mysql_fetch_assoc( $tmp ) ) {
                    if ( !$keyName || !array_key_exists( $keyName, $row ) ) {
                        $result[] = $row;
                    } else {
                        $key = is_null( $row[ $keyName ] ) ? 'null' : $row[ $keyName ];
                        $result[ $key ] = $row;
                    }
                }
            }
            mysql_free_result( $tmp );
            return $result;
        }

        public static function getLastError($message = "") {
            printf( "%s: %d: %s\n", self::$_lastQuery, mysql_errno( self::getInstance() ),mysql_error( self::getInstance() ) );
            die();
        }

        private static function getExplain($query) {
            echo "$query<br>";
            $Explain = DB::selectArray("EXPLAIN $query");
            if($Explain[0]) {
                foreach($Explain as $Key => $Value)
                    echo $Value['table'].": ".$Value['select_type']." - ".$Value['key']." - ".$Value['Extra']."<hr>";
            } else
                echo $Explain['table'].": ".$Explain['select_type']." - ".$Explain['key']." - ".$Explain['Extra']."<hr>";
        }

        private static function mysqlQuery( $query ) {

            $resource = mysql_query( $query, self::getInstance() );

            self::$_lastQuery = $query;
            self::$_queryCnt++;

            if( $_REQUEST['mysqldebug'] && strpos($query, "ELECT") && !strpos($query, "XPLAIN") )
                self::getExplain($query);

            if ( mysql_error( self::getInstance() ) ) {
                if(debug()) {
                    self::getLastError();
                    die;
                }
            } else
                return $resource;
        }

        public static function Close() {
            mysql_close( self::getInstance() );
        }

        public function __desctruct() {
            mysql_close( self::getInstance() );
        }
    }


<?php

    /* Compatibility for mysql version */
    if(!function_exists("mysql_real_escape_string")) {
        function mysql_real_escape_string($Value = "") {
            return DB::$_connection->real_escape_string($Value);
        }
    }


    class DB {
        const ENC = "utf8";

        public static $_connection = null;
        public static $_queryCnt;

        public static $_lastQuery;
        public static $_cntFields = null;
        public static $_cntRows = null;

        /* служебный метод */
        public static function connect($DB_HOST, $DB_USER, $DB_PASSWORD) {
            if(self::$_connection)
                self::Close();
            self::$_connection = new mysqli($DB_HOST, $DB_USER, $DB_PASSWORD);
            if(!self::$_connection || self::$_connection->connect_errno) {
                if(debug())
                    self::getLastError();
                die("Ошибка соединения с базой данных. Мы уже знаем о проблеме и наши специалисты делают все возможное.");
            }
            self::$_connection->query("SET NAMES '".self::ENC."';");
            return self::$_connection;
        }

        public static function getInstance() {
            if(!self::$_connection) {
                self::$_connection = new mysqli(DB_HOST, DB_USER, DB_PASSWORD);
                if(!self::$_connection || self::$_connection->connect_errno) {
                    if(debug())
                        self::getLastError();
                    die("Ошибка соединения с базой данных. Мы уже знаем о проблеме и наши специалисты делают все возможное.");
                }
                self::$_connection->query("SET NAMES '".self::ENC."';");
                self::selectDB();
                return self::$_connection;
            }
            return self::$_connection;
        }

        public static function selectDB($DB_Name = "") {
            $DB_Name = $DB_Name ?: DB_NAME;
            self::$_connection->select_db($DB_Name);
        }

        public static function Query( $query, $key = NULL ) {
            $result = self::mysqlQuery( $query );
            if(strpos($query, "NSERT INTO")) {
                $result = self::$_connection->insert_id;
            } elseif( strpos($query, "PDATE") || strpos( $query, "ELETE" ) || strpos( $query, "EPLACE" ) ) {
                $result = self::$_connection->num_rows;
            } elseif( strpos($query, "LTER" ) ) {
                $result = self::$_connection->connect_error;
            }
            return $result;
        }

        public static function selectValue($query, $keyName = false) {
            if(!self::$_connection)
                self::getInstance();

            $tmp = self::Query($query);
            list($Value) = $tmp->fetch_row();
            $tmp->free();
            return $Value;
        }

        public static function selectArray($query, $keyName = null, $key = NULL, $time = 0) {
            $result = array();
            $tmp = self::mysqlQuery($query);
            $rowcnt = $tmp->num_rows;

            if($tmp) {
                if($rowcnt == 1 && is_null($keyName)) {
                    $result = $tmp->fetch_array(MYSQLI_ASSOC);
                } elseif($rowcnt > 1 || !is_null($keyName)) {
                    while($row = $tmp->fetch_assoc()) {
                        if(!$keyName || !array_key_exists($keyName, $row)) {
                            $result[] = $row;
                        } else {
                            $key = is_null($row[$keyName]) ? 'null' : $row[$keyName];
                            $result[$key] = $row;
                        }
                    }
                }
                $tmp->free();
            }
            return $result;
        }

        public static function getLastError($message = "") {
            printf("%s: %d: %s\n", self::$_lastQuery, self::$_connection->errno, self::$_connection->error);
            die;
        }

        private static function getExplain($query) {
            echo "$query<br>";
            $Explain = DB::selectArray("EXPLAIN $query");
            if($Explain[0]) {
                foreach($Explain as $Key => $Value)
                    echo $Value['table'].": ".$Value['select_type']." - ".$Value['key']." - ".$Value['Extra']."<hr>";
            } else
                echo $Explain['table'].": ".$Explain['select_type']." - ".$Explain['key']." - ".$Explain['Extra']."<hr>";
        }

        private static function mysqlQuery($query) {

            if(self::$_connection)
                $resource = self::$_connection->query($query);

            self::$_lastQuery = $query;
            self::$_queryCnt++;

            if( mysqldebug() && strpos($query, "ELECT") && !strpos($query, "XPLAIN") )
                self::getExplain($query);

            if(self::$_connection->error) {
                if(debug())
                    self::getLastError();
            } else
                return $resource;
        }

        public static function Close() {
            if(self::$_connection)
                self::$_connection->close();
        }

        public function __desctruct() {
            if(self::$_connection)
                self::$_connection->close();
        }

    }


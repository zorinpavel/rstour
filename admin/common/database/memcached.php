<?php


    class DBMem extends DB {

        public static $_connection = null;

        public static function Query( $query, $key = NULL ) {
            $Obj = self::Get($key);

            if(!$Obj)
                return self::Set(DB::Query($query), $key);

            return $Obj;
        }


        public static function selectArray($query, $keyName = NULL, $key = NULL, $time = 10) {
            $Obj = self::Get($key);
            if(!$Obj) {
                $Obj = DB::selectArray($query, $keyName);
                self::Set($Obj, $key, $time);
            }

            return $Obj;
        }


        public static function Set($Obj, $key, $time = 10) {
            $key = DOMAIN_NAME.".".$key;
            self::getInstance();
            if(self::$_connection) {
                $result = self::$_connection->replace($key, $Obj);
                if(!$result)
                    self::$_connection->set($key, $Obj, 0, $time);
            }

            return $Obj;
        }


        public static function Get($key) {
            $key = DOMAIN_NAME.".".$key;
            self::getInstance();
            if(!self::$_connection)
                return false;

            return self::$_connection->get($key);
        }


        public static function Delete($key) {
            $key = DOMAIN_NAME.".".$key;
            self::getInstance();
            if(!self::$_connection)
                return false;

            return self::$_connection->delete($key);
        }


        public static function Close() {
            if(self::$_connection)
                self::$_connection->close();
        }


        public static function Stat() {
            if(self::$_connection)
                $AllKeys = self::$_connection->getAllKeys();
            else
                die("NO CONNECTION");
            if($AllKeys)
                print_r($AllKeys);
            else
                die("FALSE");
        }

        
        public static function ClearAll() {
            $keys = self::$_connection->getAllKeys();
            $regex = DOMAIN_NAME.".*";
            foreach($keys as $item) {
                if(preg_match('/'.$regex.'/', $item))
                    self::$_connection->delete($item);
            }
        }

        public static function getInstance() {
            if(!class_exists("Memcached"))
                return false;

            if(!self::$_connection) {
                self::$_connection = new Memcached();
                self::$_connection->addServer("127.0.0.1", 11211);
                if(!self::$_connection)
                    die("Ошибка соединения с memcached. Мы уже знаем о проблеме и наши специалисты делают все возможное.");
                
                return self::$_connection;
            }

            return self::$_connection;
        }


        public function __desctruct() {
            self::Close();
        }

    }


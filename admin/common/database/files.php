<?php


    class DBMem extends DB {

        public static $_connection = false;

        public static function Set($Obj, $key, $time = 0) {
            self::getInstance();
            if(self::$_connection) {
                $CacheFile = CACHE_PATH."/$key.json";
                $ft = fopen($CacheFile,"w");
                fputs($ft, json_encode($Obj));
                fclose($ft);
            }

            return $Obj;
        }


        public static function Get($key) {
            self::getInstance();
            if(!self::$_connection)
                return false;

            $CacheFile = CACHE_PATH."/$key.json";
            if(file_exists($CacheFile) && filesize($CacheFile) && !debug())
                return $CachedData = json_decode(file_get_contents($CacheFile), true);

            return false;
        }


        public static function Delete($key) {
            self::getInstance();
            if(!self::$_connection)
                return false;

            $CacheFile = CACHE_PATH."/$key.json";
            return @unlink($CacheFile);
        }


        public static function getInstance() {
            if(defined("CACHE_PATH") && is_dir(CACHE_PATH))
                self::$_connection = true;

            return self::$_connection;
        }

    }
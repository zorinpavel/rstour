<?php


    class DBMem extends DB {

        public static $_connection = null;

        public static function selectArray($query, $keyName = NULL, $key = NULL, $time = false) {
            $Obj = self::Get($key);
            if(!$Obj) {
                $Obj = DB::selectArray($query, $keyName);
                self::Set($Obj, $key, $time);
            }
            return $Obj;
        }


        public static function Set($Obj, $key, $time = false) {
            $key = DOMAIN_NAME.".".$key;
            self::getInstance();
//            if(self::$_connection && !debug()) {
            if(self::$_connection) {
                if($time)
                    self::$_connection->setex($key, $time, json_encode($Obj));
//                    self::$_connection->set($key, json_encode($Obj), $time);
                else
                    self::$_connection->set($key, json_encode($Obj));
            }
            return $Obj;
        }


        public static function Get($key) {
            $key = DOMAIN_NAME.".".$key;
            self::getInstance();
//            if(!self::$_connection || debug())
//            if(!self::$_connection || iotest())
            if(!self::$_connection)
                return false;
            $Obj = self::$_connection->get($key);
            if($Obj)
                return json_decode($Obj, true);
            else
                return false;
        }


        public static function Delete($key) {
            $key = DOMAIN_NAME.".".$key;
            self::getInstance();
            if(!self::$_connection)
                return false;
            return self::$_connection->del($key);
        }


        public static function Close() {
            if(self::$_connection)
                self::$_connection->close();
        }


        public static function Stat() {
            die("FALSE");
        }

        
        public static function Ttl($key) {
            $key = DOMAIN_NAME.".".$key;
            self::getInstance();
            if(!self::$_connection)
                return false;

            return self::$_connection->pttl($key);
        }


        public static function ClearAll() {
            self::getInstance();
            if(!self::$_connection)
                return false;

            $keys = self::$_connection->keys(DOMAIN_NAME.".*");
            foreach($keys as $item)
                self::$_connection->delete($item);

            return count($keys);
        }

        public static function getInstance() {
            if(!class_exists("Redis"))
                return false;

            if(!self::$_connection) {
                self::$_connection = new Redis();
                self::$_connection->connect("127.0.0.1", 6379);
                if(!self::$_connection)
                    die("Ошибка соединения с redis. Мы уже знаем о проблеме и наши специалисты делают все возможное.");
                
                return self::$_connection;
            }

            return self::$_connection;
        }


        public function __desctruct() {
            self::Close();
        }

    }


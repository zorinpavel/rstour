<?php
    

    class sefRouter {
        
        public static $Routed = false;
        
        public static function Register($ClassName, $ItemId, $Url, $Language = false) {
            if(!self::GetByUrl($Url, $Language)) {
                return DB::Query("REPLACE INTO sefRouters SET ClassName = '".mysql_real_escape_string($ClassName)."', ItemId = ".(int)$ItemId.", Url = '".mysql_real_escape_string($Url)."', Language = ".($Language ? "'$Language'" : "NULL"));
            } else
                return false;
        }
        
        
        public static function Expire($ClassName, $ItemId) {
            DB::Query("DELETE FROM sefRouters WHERE ClassName = '".mysql_real_escape_string($ClassName)."' AND ItemId = ".(int)$ItemId);
        }
        
        
        public static function GetByUrl($Url, $Language = false) {
            if($Language || (DEFAULT_LANGUAGE && PAGE_LANGUAGE))
                $LangCond = " AND Language = '".($Language ?: PAGE_LANGUAGE)."'";
            $Router = DB::selectArray("SELECT * FROM sefRouters WHERE Url = '".mysql_real_escape_string($Url)."'$LangCond");
            if($Router['Sef_Code'])
                return $Router;
            return false;
        }
        
        
        public static function Get($ClassName, $ItemId) {
            $Router = DB::selectArray("SELECT * FROM sefRouters WHERE ClassName = '".mysql_real_escape_string($ClassName)."' AND ItemId = ".(int)$ItemId);
            if($Router['Sef_Code'])
                return $Router;
            return false;
        }
    
    
        public static function GetRouteStatic($Str = "", $Language = false) {
            $Str = $Str ?: $_REQUEST['Route'];
            $Language = $Language ?: $_REQUEST['Language'];
            $Route = Cyr2Lat($Str, true);
            if(!self::GetByUrl($Route, $Language)) {
                return $Route;
            }
            return false;
        }
        
        
        /* classContent /admin/content/ */
        public function GetRoute($Str = "", $Language = false) {
            $Str = $Str ?: $_REQUEST['Route'];
            $Language = $Language ?: $_REQUEST['Language'];
            $Route = Cyr2Lat($Str, true);
            if(!$this->GetByUrl($Route, $Language)) {
                $this->AjaxData['Route'] = "/".$Route;
                return "/".$Route;
            }
            $this->AjaxData['Error'] = true;
            return false;
        }
        
        
    }
<?

    class classTags extends classProperties {

        public static $TagID = "tag";

        var $Properties = array(
            "public" => array(
                "Names" => array(
                    "TagID",
                ),
                "Types" => array(
                    "T30",
                ),
                "Labels" => array(
                    "Идентификатор тега",
                )
            ),
            "private" => array(
                "Names" => array(
                    "template_tags",
                ),
                "Types" => array(
                    "InputTemplates",
                ),
                "Labels" => array(
                    "Облако",
                )
            )
        );

        var $DefaultTemplates = array(
            "template_tags" => array("0" => ""),
        );

        function GetClassName() {
            return __CLASS__;
        }

        function Action() {
            /*
                  if($_REQUEST[$this->TagID]) {
            //        preg_match("/(\w| |-|_|\.)+/", $_REQUEST[$this->TagID], $pregTag);
            //        $TagName = $pregTag[0];
            //        if($TagName != "") {
                    $TagCode = (int)$_REQUEST[$this->TagID];
                    if($TagCode) {
                      $r = db_query("SELECT TI.Tag_Code, TI.Item FROM TagsIndex AS TI LEFT JOIN Tags AS T ON TI.Tag_Code = T.Tag_Code WHERE T.Tag_Code = $TagCode");
                      while($Item = mysql_fetch_assoc($r)) {
                        list($className, $Ite_Code) = explode(".", $Item['Item']);
                        $Items[$className][$Ite_Code] = $Ite_Code;
                      }
                      foreach($Items as $className => $IteCodes) {
                        $C = _autoload($className, "");
                        $C->Items = $IteCodes;
                        $C->Action();
                      }

                    } else {
                      $this->Cloud();
                    }
                  } else {
            */
            $this->Cloud();
            //      }
        }

        function UnsetTag($Tags, $minCount = 0) {
            /* максимальное кол-во тегов на странице */
            $Cnt  = 50;
            if(is_array($Tags)) {
                foreach($Tags as $Code => $Tag) {
                    if($Tag['Tag_Count'] <= $minCount && count($Tags) > $Cnt)
                        unset($Tags[$Code]);
                }
            }
            if(count($Tags) > $Cnt) {
                /* если много удаляем с большим шагом */
                $minCount = $minCount + 2;
                $Tags = $this->UnsetTag($Tags, $minCount);
            }
            return $Tags;
        }


        function Cloud() {
            $minCount = 10000;
            $maxCount = 0;
            $Tags = DB::selectArray("SELECT T.Tag_Code, T.Tag_Name, COUNT(TI.Tag_Code) AS Tag_Count FROM Tags AS T LEFT JOIN TagsIndex AS TI ON TI.Tag_Code = T.Tag_Code WHERE T.Tag_Code > 0 GROUP BY T.Tag_Code", "Tag_Code");
            foreach($Tags as $Tag_Code => $Tag) {
                $Tag['Link'] = "?".$this->TagID."=".$Tag['Tag_Code'];
                $Tags[$Tag['Tag_Code']] = $Tag;
                if($Tag['Tag_Count'] > $maxCount)
                    $maxCount = $Tag['Tag_Count'];
                if($Tag['Tag_Count'] > 1 && $Tag['Tag_Count'] < $minCount)
                    $minCount = $Tag['Tag_Count'];
            }

            /* удалим с маленьким индексом */

            $Tags = $this->UnsetTag($Tags, $minCount);

            /*
                    if(is_array($Tags)) {
                      foreach($Tags as $Code => $Tag) {
            //            echo $Tag['Tag_Count']." <= $minCount && ".count($Tags)." > 20<br>";
                        if($Tag['Tag_Count'] <= $minCount && count($Tags) > 20)
                          unset($Tags[$Code]);
                      }
                    }
            /* */

            $minCount = 10000;
            $maxCount = 0;
            foreach($Tags as $Code => $Tag) {
                $FinalTags[$Tag['Tag_Code']] = $Tag;
                if($Tag['Tag_Count'] > $maxCount)
                    $maxCount = $Tag['Tag_Count'];
                if($Tag['Tag_Count'] < $minCount)
                    $minCount = $Tag['Tag_Count'];
            }

            $this->Data['minCount'] = $minCount;
            $this->Data['maxCount'] = $maxCount;
            $this->Data['Tags'] = $FinalTags;
            $this->Ins2Php("template_tags");
        }


        function Add($Tag, $className, $Ite_Code) {
            $r = db_query("SELECT Tag_Code FROM Tags WHERE Tag_Name = '$Tag'");
            list($Tag_Code) = mysql_fetch_row($r);
            if(!$Tag_Code && $Tag != "") {
                db_query("INSERT INTO Tags SET Tag_Name = '$Tag'");
                $Tag_Code = mysql_insert_id();
            }
            $Item = "$className.$Ite_Code";
            if($Tag_Code)
                db_query("REPLACE INTO TagsIndex SET Tag_Code = $Tag_Code, Item = '$Item'");
        }

        function RemoveAll($className, $Ite_Code) {
            $Item = "$className.$Ite_Code";
            db_query("DELETE FROM TagsIndex WHERE Item = '$Item'");
        }

        function GetTags($className, $Ite_Code) {
            $Item = "$className.$Ite_Code";
            $Tags = DB::selectArray("SELECT T.Tag_Code, T.Tag_Name FROM TagsIndex as TI LEFT JOIN Tags as T ON TI.Tag_Code = T.Tag_Code WHERE TI.Item = '$Item'", "Tag_Code");
            return $Tags;
        }

        function classTags($sec = "", $Parent = "") {
            parent::classProperties($sec, $Parent);
            $this->TagID = self::$TagID; // $this->GetProperty("TagID");
        }

        function searchByValue() {
            $Value = mysql_escape_string(trim($_REQUEST['value']));
            echo $Value;
            $Tags = DB::selectArray("SELECT tags.Tag_Code, tags.Tag_Name, COUNT(ti.Tag_Code) AS Tag_Count FROM Tags AS tags LEFT JOIN TagsIndex AS ti ON ti.Tag_Code = tags.Tag_Code WHERE tags.Tag_Code > 0 AND LOWER(tags.Tag_Name) LIKE LOWER('$Value%') GROUP BY tags.Tag_Code ORDER BY Tag_Count DESC LIMIT 0, 10", "Tag_Code");
            $maxCount = 0;
            $minCount = 0;
            foreach($Tags as $Tag_Code => $Tag) {
                $Tag['Link'] = "?".$this->TagID."=".$Tag['Tag_Code'];
                $Tag['Name'] = $Tag['Tag_Name'];
                $Tag['Value'] = $Tag['Tag_Code'];
                $Tags[$Tag['Tag_Code']] = $Tag;
                if($Tag['Tag_Count'] > $maxCount)
                    $maxCount = $Tag['Tag_Count'];
                if($Tag['Tag_Count'] > 1 && $Tag['Tag_Count'] < $minCount)
                    $minCount = $Tag['Tag_Count'];
            }
            $this->AjaxData['minCount'] = $minCount;
            $this->AjaxData['maxCount'] = $maxCount;
            $this->AjaxData['Results'] = $Tags;
            if(!count($this->AjaxData['Results']))
                $this->AjaxData['Error'] = 1;
        }

    }

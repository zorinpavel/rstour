<?php


    class classTripCart extends classTrip {

        public $Items = array();
        public $Cart = array();

        static $DoID = "tcdo";
        static $CountKey = "tripDates";

        static $OrderStatuses = array(
            8  => "Аккаунт не проверен",
            1  => "Ожидание оплаты",
            2  => "На доставке",
            3  => "Оплачено",
            9  => "Частично оплачено",
            4  => "На бронировании",
            5  => "Подтверждено",
            6  => "Архив",
            7  => "Отменено",
        );

        var $Properties = array(
            "public" => array(
                "Names" => array(
                ),
                "Types" => array(
                ),
                "Labels" => array(
                )
            ),
            "private" => array(
                "Names" => array(
                    "template_update_ok",
                    "template_empty",
                    "Separator",
                    "template_top",
                    "template_item",
                    "template_item_sep",
                    "template_bottom",
                    "Separator",
                    "template_contacts",
                    "template_enterdata",
                    "template_delivery",
                    "template_payment",
                    "template_orderinfo",
                    "template_adminmail",
                ),
                "Types" => array(
                    "InputTemplates",
                    "InputTemplates",
                    "Separator",
                    "InputTemplates",
                    "InputTemplates",
                    "InputTemplates",
                    "InputTemplates",
                    "Separator",
                    "InputTemplates",
                    "InputTemplates",
                    "InputTemplates",
                    "InputTemplates",
                    "InputTemplates",
                    "InputTemplates",
                ),
                "Labels" => array(
                    "Товар в корзине",
                    "Корзина пуста",
                    "",
                    "Верх списка",
                    "Первый элемент",
                    "Разделитель",
                    "Низ списка",
                    "",
                    "Контактные данные",
                    "Данные туристов",
                    "Доставка",
                    "Оплата",
                    "Готово",
                    "Письмо администратору",
                )
            )
        );

        var $DefaultTemplates = array(
            "template_update_ok" => array(0 => ""),
            "template_empty" => array(0 => ""),
            "template_top" => array(0 => ""),
            "template_item" => array(0 => ""),
            "template_bottom" => array(0 => ""),
            "template_item_sep" => array(0 => ""),
            "template_contacts" => array(0 => ""),
            "template_enterdata" => array(0 => ""),
            "template_delivery" => array(0 => ""),
            "template_payment" => array(0 => ""),
            "template_orderinfo" => array(0 => ""),
            "template_adminmail" => array(0 => ""),
        );

        function GetClassName() {
            return __CLASS__;
        }


        function Action($Do = "") {
            $Do = $Do ? $Do : $_REQUEST[self::$DoID];

            $this->Data['Errors'] = array();
            $this->Data['Messages'] = array();

            if(!in_array($Do, array("Update")))
                $this->GetItems();
            
            if($_REQUEST['OrderId']) {
                $this->dPrintOrderInfo();
                return;
            }
            
            switch($Do) {
                case "Clear":
                    $this->ClearCartData();
                    break;
                case "Update":
                    $this->DoUpdate();
                    break;
                case "Contacts":
                    $this->DoContacts();
                    break;
                case "ContactsUpdate":
                    $this->DoContactsUpdate();
                    break;
                case "EnterData":
                    $this->DoEnterData();
                    break;
                case "EnterDataUpdate":
                    $this->DoEnterDataUpdate();
                    break;
                case "Delivery":
                    $this->DoDelivery();
                    break;
                case "DeliveryUpdate":
                    $this->DoDeliveryUpdate();
                    break;
                case "Payment":
                    $this->DoPayment();
                    break;
                case "PaymentUpdate":
                    $this->DoPaymentUpdate();
                    break;
                case "PaymentSuccess":
                    $this->DoPaymentSuccess();
                    break;
                case "PaymentFail":
                    $this->DoPaymentFail();
                    break;
                case "OrderInfo":
                    $this->dPrintOrderInfo();
                    break;
                default:
                    $this->DoShow();
                    break;
            }

            if(debug()) {
                ?><pre>CART:<? print_r($this->Cart); ?></pre><?
                ?><pre>DATA:<? print_r($this->Data); ?></pre><?
            }

        }


        public function DoShow() {
            if(count($this->Data['ItemsData']) > 0) {

                $Data = $this->Data;
                $this->Ins2Php('template_top');
                foreach($this->Data['ItemsData'] as $Key => $Item) {
                    $this->Data = array_merge($Data, $Item);
                    $this->Ins2Php('template_item');
                }
                $this->Data = $Data;
                $this->Ins2Php('template_bottom');

            } else {
                $this->ClearCartData();
                $this->Ins2Php('template_empty');
            }
        }


        public function DoUpdate() {
            if(is_array($_REQUEST[self::$ItemID])) {
                unset($this->Cart['Items']);
                unset($this->Data['ItemsData']);
                foreach($_REQUEST[self::$ItemID] as $Ite_Code => $Quantity) {
                    if($Quantity)
                        $this->Cart['Items'][$Ite_Code] = $Quantity;
                }
            }

            if(is_array($_REQUEST['extras'])) {
                foreach($_REQUEST['extras'] as $Key => $Quantity) {
                    if($Quantity)
                        $this->Cart['Extras'][$Key] = $Quantity;
                    else
                        unset($this->Cart['Extras'][$Key]);
                }
            }

            if(is_array($_REQUEST['dates'])) {
                unset($this->Cart['Dates']);
                unset($this->Data['Dates']);
                foreach($_REQUEST['dates'] as $dKey => $Date)
                    $this->Cart['Dates'][$dKey] = date("Y-m-d", strtotime($Date));
            }

            $this->GetItems();
            if(!$this->isJsHttpRequest)
                $this->DoShow();

        }


        public function GetItems() {
            if(count($this->Cart['Items']) <= 0)
                return;

            $this->Data['Deliveries'] = DB::selectArray("SELECT * FROM cartDeliverySystems ORDER BY DS_Order DESC, DS_Code", "DS_Code");
            $this->Data['Payments'] = DB::selectArray("SELECT * FROM cartPaymentSystems ORDER BY PS_Order DESC, PS_Code", "PS_Code");

            $IteCond = "T_Code IN (".join(array_keys($this->Cart['Items']), ", ").")";
            $Items = DB::selectArray("SELECT * FROM tripItems WHERE $IteCond", "T_Code");

            $this->Data['TotalSum'] = 0;
            $this->Data['TotalOriginalSum'] = 0;
            $this->Data['ExtrasTotalSum'] = 0;
            $this->Data['TotalCount'] = 0;

            foreach($Items as $Key => $Item) {
                $Item = $this->GetItemVars($Item, false);

                $Item['CartPrice'] = (float)preg_replace('/\s+/', '', $Item['T_Price']);
                $Item['Quantity'] = $this->Cart['Items'][$Item['T_Code']];
                //debug
                $Item['qSum'] = $Item['CartPrice'] * $Item['Quantity'];
                $this->Data['TotalCount'] += $this->Cart['Items'][$Item['T_Code']];

                if($this->Cart['Travelers']) {
                    $Item['Sum'] = 0;
                    $Item['KidsDiscount'] = 0;
                    foreach($this->Cart['Travelers'] as $i => $Traveler) {
                        if($Traveler['kid'] && $Item['T_KidsDiscount']) {
                            $Item['Sum'] += $Item['CartPrice'] - (float)preg_replace('/\s+/', '', $Item['T_KidsDiscount']);
                            $Item['KidsDiscount'] += (float)preg_replace('/\s+/', '', $Item['T_KidsDiscount']);
                        } else {
                            $Item['Sum'] += $Item['CartPrice'];
                        }
                    }
                }
                
                if($this->Cart['Extras']) {
                    foreach($this->Cart['Extras'] as $eKey => $eValue) {
                        if($Item['Extras'][$eKey]) {
                            $this->Data['Extras'][$eKey] = $Item['Extras'][$eKey];
                            $this->Data['Extras'][$eKey]['Quantity'] = (int)$eValue;
                            $this->Data['Extras'][$eKey]['Sum'] = (int)$eValue * $Item['Extras'][$eKey]['dE_Price'];
                            $this->Data['ExtrasTotalSum'] += $this->Data['Extras'][$eKey]['Sum'];
                        } else {
                            unset($this->Cart['Extras'][$eKey]);
                            unset($this->Data['Extras'][$eKey]);
                        }
                    }
                } else
                    $this->Cart['Extras'] = array();
    
                foreach($Item['Dates'] as $dKey => $Date) {
                    if(date("Y-m-d") > date("Y-m-d", strtotime($Date['T_Date'])))
                        unset($Item['Dates'][$dKey]);
                }
    
                if($this->Cart['Dates'][$Item['T_Code']]) {
                    foreach($Item['Dates'] as $diKey => $diDate) {
                        if($this->Cart['Dates'][$Item['T_Code']] == $diDate['T_Date']) {
                            $isDate = true;
                            if(date("Y-m-d") < date("Y-m-d", strtotime($this->Cart['Dates'][$Item['T_Code']])) && $diDate['T_BookPlaces'] > 0) {
                                $this->Cart['Dates'][$Item['T_Code']] = $diDate['T_Date'];
                                $this->Data['Dates'][$Item['T_Code']] = $diDate;
                                break;
                            } else {
                                array_push($this->Data['Errors'], array("Error" => "Недостаточно мест на выбранную дату. <a href=\"/cart/\">Выбрать другую дату</a>"));
                            }
                        }
                    }
                    if(!$isDate)
                        array_push($this->Data['Errors'], array("Error" => "В эту дату нет доступных туров"));
                } elseif(!count($Item['Dates'])) {
                    array_push($this->Data['Errors'], array("Error" => "Нет активных дат для бронирования"));
                } else {
                    foreach($Item['Dates'] as $diKey => $diDate) {
                        if($diDate['T_BookPlaces'] > 0) {
                            $this->Cart['Dates'][$Item['T_Code']] = $diDate['T_Date'];
                            $this->Data['Dates'][$Item['T_Code']] = $diDate;
                            break;
                        }
                    }
                }
    
                $this->Data['TotalSum'] += $Item['Sum'];
                $this->Data['TotalOriginalSum'] = $this->Data['TotalSum'];
                $this->Data['ItemsData'][$Item['T_Code']] = $Item;
                
            }
    
            if($this->Cart['Ord_DiscountRub']) {
                $this->Data['Discount']['Name'] = "Скидка на бронь";
                $this->Data['Discount']['Value'] = $this->Cart['Ord_DiscountRub'];
                $this->Data['Discount']['Sum'] = $this->Data['Discount']['Value'];
                $this->Data['TotalSum'] = $this->Data['TotalOriginalSum'] - $this->Data['Discount']['Sum'];
            } elseif($this->Cart['Ord_Discount']) {
                unset($this->Data['Discount']['Value']);
                $this->Data['Discount']['Name'] = "Скидка на бронь";
                $this->Data['Discount']['Amount'] = $this->Cart['Ord_Discount'];
                $this->Data['Discount']['Sum'] = round($this->Data['TotalOriginalSum'] / 100 * $this->Data['Discount']['Amount']);
                $this->Data['TotalSum'] = $this->Data['TotalOriginalSum'] - $this->Data['Discount']['Sum'];
            } elseif($this->User['Company'] && $this->User['Company']['C_Discount']) {
                unset($this->Data['Discount']['Value']);
                $this->Data['Discount']['Name'] = "Скидка компании";
                $this->Data['Discount']['Amount'] = round($this->User['Company']['C_Discount'] * (float)$Item['T_Discount']);
                $this->Data['Discount']['Sum'] = round($this->Data['TotalOriginalSum'] / 100 * $this->Data['Discount']['Amount']);
                $this->Data['TotalSum'] -= $this->Data['Discount']['Sum'];
            } elseif($this->User['Discount']) {
                unset($this->Data['Discount']['Value']);
                $this->Data['Discount']['Name'] = "Персональная скидка";
//                $this->Data['Discount']['Amount'] = round((int)$this->User['Discount'] * (float)$Item['T_Discount']);
                $this->Data['Discount']['Amount'] = (float)$this->User['Discount'];
                $this->Data['Discount']['Sum'] = round($this->Data['TotalOriginalSum'] / 100 * $this->Data['Discount']['Amount']);
                $this->Data['TotalSum'] -= $this->Data['Discount']['Sum'];
            }

            if($this->Cart['Pincode'] && $this->Cart['Pincode']['P_Discount'] > $this->User['Discount']) {
                $this->Data['Pincode'] = $this->Cart['Pincode'];
                $this->Data['Discount']['Name'] = "Скидка по пин-коду (".$this->Cart['Pincode']['P_Name'].")";
                $this->Data['Discount']['Amount'] = (float)$this->Cart['Pincode']['P_Discount'];
                $this->Data['Discount']['Sum'] = round($this->Data['TotalOriginalSum'] / 100 * $this->Data['Discount']['Amount']);
                $this->Data['TotalSum'] -= $this->Data['Discount']['Sum'];
            }

            $this->Cart['Discount'] = $this->Data['Discount'];

            $this->Cart['ExtrasTotalSum'] = $this->Data['ExtrasTotalSum'];
            $this->Data['TotalSum'] += $this->Data['ExtrasTotalSum'];
            $this->Cart['TotalSum'] = $this->Data['TotalSum'];
            $this->Cart['TotalOriginalSum'] = $this->Data['TotalOriginalSum'];

            $this->Data['ItemsCount'] = count($this->Data['ItemsData']);
            $this->Data['Error'] = count($this->Data['Errors']);

        }


        public function DoContacts() {
            if(count($this->Data['ItemsData']) <= 0) {
                $this->DoShow();
                return;
            }

            if($_REQUEST['Pincode']) {
                $Pincode = $this->CheckPin();
                if($Pincode['P_Code'] && $Pincode['P_Discount'] > $this->User['Discount'])
                    $this->Cart['Pincode'] = $Pincode;
            }

            if(is_array($_REQUEST['extras'])) {
                foreach($_REQUEST['extras'] as $Key => $Quantity) {
                    if($Quantity)
                        $this->Cart['Extras'][$Key] = $Quantity;
                    else
                        unset($this->Cart['Extras'][$Key]);
                }
            }

            if(is_array($_REQUEST['dates'])) {
                unset($this->Cart['Dates']);
                unset($this->Data['Dates']);
                foreach($_REQUEST['dates'] as $dKey => $Date)
                    $this->Cart['Dates'][$dKey] = date("Y-m-d", strtotime($Date));
            }

            if($this->User['us_id']) {
                $this->DoEnterData();
            } elseif($this->Data['Error']) {
                $this->DoShow();
            } else {
                $this->Ins2Php('template_contacts');
            }
        }


        public function DoContactsUpdate() {
            if($_REQUEST['UserAction'] == "enter") {
                $this->Users->CheckValidEnter($_REQUEST['mail'], $_REQUEST['mail'] ,$_REQUEST['pass']);
                $this->User = classUsers::$User;
                $this->Data['Errors'] = $this->Users->Data['Errors'];
            } else {
                if($_REQUEST['mail'] && $_REQUEST['pass'])
                    $this->Users->CheckValidReg(false);
                else
                    $this->Users->CheckValidFastReg(false);

                $this->User = classUsers::$User;
                $this->Data = array_merge($this->Data, $this->Users->Data);
            }
            $this->Data['Error'] = count($this->Data['Errors']);
            
            if($this->Data['Error']) {
                $this->DoContacts();
                return;
            }

            $this->DoEnterData();
        }


        public function DoEnterData() {
            if(count($this->Data['ItemsData']) <= 0) {
                $this->DoShow();
                return;
            }

            if(!$this->User['us_id']) {
                $this->DoContacts();
                return;
            }

            if(!$this->User['Company']) {
                $this->Cart['Travelers'][1]['name'] = $this->User['name'];
                $this->Cart['Travelers'][1]['lastname'] = $this->User['lastname'];
                $this->Cart['Travelers'][1]['middlename'] = $this->User['middlename'];
                $this->Cart['Travelers'][1]['birthdate'] = date("d.m.Y", mktime(0,0,0, $this->User['bth_month'], $this->User['bth_day'], $this->User['bth_year']));
                $this->Cart['Travelers'][1]['passport']['number'] = $this->User['PassportNumber'];
//                $this->Cart['Travelers'][1]['passport']['date'] = $this->User['PassportDate'];
                $this->Cart['Travelers'][1]['sex'] = $this->User['Sex'];
                foreach($this->Cart['Extras'] as $eKey => $Value) {
                    $this->Cart['Travelers'][1]['Extras'][$eKey] = 1;
                }

                if(!$this->User['status'] || !$this->User['mail']) {
                    $this->GetItems(); // пересчитать
                    $this->Cart['Ord_Status'] = 8; // аккаунт не проверен
                    $this->InsertCartOrder();
                    $OrderId = $this->Cart['OrderId'];
                    $this->ClearCartData();
                    header("Location: /cart/?OrderId=$OrderId");
                    die;
                }

            }

            $this->Data['Travelers'] = $this->Cart['Travelers'];

            $this->Ins2Php("template_enterdata");
        }


        public function DoEnterDataUpdate() {
            $Error = false;
            $Extras = array();
            for($i = 1; $i <= count($_REQUEST['name']); $i++) {
                $this->Data['Travelers'][$i]['name'] = mysql_real_escape_string(strip_tags($_REQUEST['name'][$i]));
                $this->Data['Travelers'][$i]['lastname'] = mysql_real_escape_string(strip_tags($_REQUEST['lastname'][$i]));
                $this->Data['Travelers'][$i]['middlename'] = mysql_real_escape_string(strip_tags($_REQUEST['middlename'][$i]));
                $this->Data['Travelers'][$i]['sex'] = (int)$_REQUEST['sex'][$i];
                $this->Data['Travelers'][$i]['kid'] = (int)$_REQUEST['kid'][$i];
                $this->Data['Travelers'][$i]['birthdate'] = dateDecode(mysql_real_escape_string($_REQUEST['birthdate'][$i]));
                $this->Data['Travelers'][$i]['passport']['number'] = mysql_real_escape_string(strip_tags($_REQUEST['passport']['number'][$i]));
//                $this->Data['Travelers'][$i]['passport']['date'] = $_REQUEST['passport']['date'][$i] ? dateDecode(mysql_real_escape_string($_REQUEST['passport']['date'][$i])) : false;

                if(!$this->Data['Travelers'][$i]['name'] || !$this->Data['Travelers'][$i]['lastname'] || !$this->Data['Travelers'][$i]['middlename'])
                    $Error = "Необходимо заполнить все поля";
//                if(!$this->Data['Travelers'][$i]['kid'] && $this->Data['Travelers'][$i]['birthdate'] < date("Y-m-d", mktime(0, 0, 0, date("m"), date("d"), date("Y")-14)) && (!$this->Data['Travelers'][$i]['passport']['number'] || !$this->Data['Travelers'][$i]['passport']['date']))
//                    $Error = "Необходимо заполнить данные паcпорта";

                // update extras
                if($_REQUEST['extras']) {
                    foreach($_REQUEST['extras'][$i] as $eKey => $Value) {
                        $this->Data['Travelers'][$i]['Extras'][$eKey] = $Value;
                        $Extras[$eKey] += $Value;
                    }
                }

            }

            $this->Cart['Extras'] = $Extras;
            $this->Cart['Travelers'] = $this->Data['Travelers'];
            foreach($this->Cart['Items'] as $T_Code => $Item)
                $this->Cart['Items'][$T_Code] = count($this->Cart['Travelers']);
            
            if($_REQUEST['TravelerPhone'])
                $this->Cart['TravelerPhone'] = mysql_real_escape_string(strip_tags($_REQUEST['TravelerPhone']));

            if($_REQUEST['TravelerComment'])
                $this->Cart['TravelerComment'] = mysql_real_escape_string(strip_tags($_REQUEST['TravelerComment']));

            if($Error)
                array_push($this->Data['Errors'], array("Error" => $Error));

            $this->Data['Error'] = count($this->Data['Errors']);

            if($this->Data['Error']) {
                $this->DoEnterData();
                return;
            }

            $this->GetItems(); // пересчитать

            if($this->User['Company']) {
                $this->Cart['Ord_Status'] = 4; // "на бронировнии"
                $this->InsertCartOrder();
                $OrderId = $this->Cart['OrderId'];
                $this->ClearCartData();
                header("Location: /cart/?OrderId=$OrderId");
                die;
            } else {
                if($this->User['lastname'] == $this->Data['Travelers'][1]['lastname'] && $this->User['name'] == $this->Data['Travelers'][1]['name']) {
                    list($by, $bm, $bd) = explode("-", $this->Data['Travelers'][1]['birthdate']);
                    DB::Query("UPDATE Users SET name = '" . $this->Data['Travelers'][1]['name'] . "', lastname = '" . $this->Data['Travelers'][1]['lastname'] . "', middlename = '" . $this->Data['Travelers'][1]['middlename'] . "', bth_day = $bd, bth_month = $bm, bth_year = $by WHERE us_id = " . $this->User['us_id']);

                    $Data = classUsers::GetUserFields(array("us_id" => $this->User['us_id']));
                    if (count($Data['Fields'])) {
                        foreach ($Data['Fields'] as $Code => $Field) {
                            if($Field['Uf_Name'] == "PassportNumber")
                                $Data[$Field['Uf_Name']] = $this->Data['Travelers'][1]['passport']['number'];
//                            if($Field['Uf_Name'] == "PassportDate")
//                                $Data[$Field['Uf_Name']] = $this->Data['Travelers'][1]['passport']['date'];
                            if ($Field['Uf_Name'] == "Sex")
                                $Data[$Field['Uf_Name']] = $this->Data['Travelers'][1]['sex'];
                        }
                        $dbData = json_encode($Data);
                        DB::Query("REPLACE INTO UserData SET Data = '$dbData', us_id = " . $this->User['us_id']);
                    }
                }

                $this->InsertCartOrder();

                if(!$this->User['status']) {
                    $OrderId = $this->Cart['OrderId'];
                    $this->ClearCartData();
                    header("Location: /cart/?OrderId=$OrderId");
                    die;
                } else
                    $this->DoDelivery();
            }
        }


        public function DoDelivery() {
            if(count($this->Data['ItemsData']) <= 0) {
                $this->DoShow();
                return;
            }

            if($this->Data['Delivery_Type'])
                $this->Data['Deliveries'][$this->Data['Delivery_Type']]['_checked'] = 1;
            else {
                foreach($this->Data['Deliveries'] as $Key => $Delivery) {
                    $this->Data['Deliveries'][$Key]['_checked'] = 1;
                    break;
                }
            }

            $this->Ins2Php('template_delivery');
        }


        public function DoDeliveryUpdate() {
            $this->Data['Delivery_Type'] = (int)$_REQUEST['Delivery_Type'] ?: $this->Cart['Delivery']['Delivery_Type'];

            $this->Data['Delivery_Addr'] = mysql_real_escape_string(strip_tags($_REQUEST['Delivery_Addr']));
            $this->Data['Delivery_Name'] = mysql_real_escape_string(strip_tags($_REQUEST['Delivery_Name']));
            $this->Data['Delivery_Comment'] = mysql_real_escape_string(strip_tags($_REQUEST['Delivery_Comment']));

            if($this->Data['Deliveries'][$this->Data['Delivery_Type']]['DS_SelfPayment']) { // оплата при получении
                if(!$this->Data['Delivery_Addr'])
                    array_push($this->Data['Errors'], array("Error" => "Укажите адрес доставки"));
                if(!$this->Data['Delivery_Name'])
                    array_push($this->Data['Errors'], array("Error" => "Укажите имя получателя"));
            }

            $this->Cart['Delivery']['Delivery_Type'] = $this->Data['Delivery_Type'];
            $this->Cart['Delivery']['Delivery_Addr'] = $this->Data['Delivery_Addr'];
            $this->Cart['Delivery']['Delivery_Name'] = $this->Data['Delivery_Name'];
            $this->Cart['Delivery']['Delivery_Comment'] = $this->Data['Delivery_Comment'];

            if($this->Data['Delivery_Type']) {
                $this->Cart['Delivery'] = array_merge($this->Cart['Delivery'], $this->Data['Deliveries'][$this->Data['Delivery_Type']]);
            } else
                array_push($this->Data['Errors'], array("Error" => "Выберите способ доставки"));

            $this->Data['TotalSum'] += $this->Data['Deliveries'][$this->Data['Delivery_Type']]['DS_Price'];
            $this->Cart['TotalSum'] = $this->Data['TotalSum'];

            unset($this->Cart['Delivery']['DS_Data']);
            $this->Data['Error'] = count($this->Data['Errors']);

            if($this->Data['Error']) {
                $this->DoDelivery();
                return;
            }

            foreach($this->Data['ItemsData'] as $T_Code => $Item) {
                foreach($Item['Dates'] as $TD_Code => $Date) {
                    if($Date['T_Date'] == $this->Cart['Dates'][$T_Code]) {
                        if($Date['T_Limit'] && $Date['T_OpenPlaces'] > 0 && $Date['T_OpenPlaces'] >= $this->Cart['Items'][$T_Code]) {
                            $this->DoPayment();
                            return;
                        } else {
                            $this->Cart['Ord_Status'] = 4; // "на бронировнии"
                            $this->InsertCartOrder();
                            $OrderId = $this->Cart['OrderId'];
                            $this->ClearCartData();
                            header("Location: /cart/?OrderId=$OrderId");
                            die;
                        }
                    }
                }
            }

        }


        public function DoPayment() {
            if(count($this->Data['ItemsData']) <= 0) {
                $this->DoShow();
                return;
            }

            if(!$this->Cart['Delivery']['DS_SelfPayment']) {
                foreach($this->Data['Payments'] as $Key => $Payment) {
                    if($Payment['PS_SelfPayment'])
                        unset($this->Data['Payments'][$Key]);
                }
            }

            $this->Ins2Php('template_payment');
        }


        public function DoPaymentUpdate() {
            if(!$this->Cart['Payment'])
                $this->Cart['Payment'] = array();

            $this->Data['Payment_Type'] = (int)$_REQUEST['Payment_Type'] ?: $this->Cart['Payment']['Payment_Type'];
            if($this->Data['Payment_Type'])
                $this->Data['Payment'] = $this->Cart['Payment'] = array_merge($this->Cart['Payment'], $this->Data['Payments'][$this->Data['Payment_Type']]);
            else
                array_push($this->Data['Errors'], array("Error" => "Выберите способ оплаты"));

            unset($this->Cart['Payment']['PS_Data']);
            $this->Data['Error'] = count($this->Data['Errors']);

            if($this->Data['Error']) {
                $this->DoPayment();
                return;
            } else {
                $this->InsertCartOrder();
                $this->ClearCartData();
                header("Location: /merchant/".$this->Data['Payment']['PS_Id'].".php?OrderId=".$this->Data['OrderId']);
                die;
            }
        }


        public function DoPaymentSuccess() {
            $OrderId = (int)$_REQUEST['OrderId'];
            array_push($this->Data['Messages'], array("Message" => "Ваша бронь успешно оплачена"));
            $this->dPrintOrderInfo($OrderId);
        }


        public function DoPaymentFail() {
            $OrderId = (int)$_REQUEST['OrderId'];
            array_push($this->Data['Errors'], array("Error" => "Ошибка платежной системы"));
            $this->dPrintOrderInfo($OrderId);
        }


        public function InsertCartOrder() {
            foreach($this->Data['ItemsData'] as $T_Code => $Item) {
                unset($this->Data['ItemsData'][$T_Code]['T_Desc']);
                unset($this->Data['ItemsData'][$T_Code]['events']);
                unset($this->Data['ItemsData'][$T_Code]['Types']);
                unset($this->Data['ItemsData'][$T_Code]['Regions']);
                unset($this->Data['ItemsData'][$T_Code]['Images']);
                unset($this->Data['ItemsData'][$T_Code]['T_Places']);
                unset($this->Data['ItemsData'][$T_Code]['Days']);
                
                foreach($Item['Included'] as $dI_Code => $Included)
                    unset($this->Data['ItemsData'][$T_Code]['Included'][$dI_Code]['dI_Desc']);
                
                foreach($Item['Hotels'] as $H_Code => $Hotel) {
                    unset($this->Data['ItemsData'][$T_Code]['Hotels'][$H_Code]['dH_Desc']);
                    unset($this->Data['ItemsData'][$T_Code]['Hotels'][$H_Code]['Images']);
                }
            }
            foreach($this->Data['Dates'] as $TD_Code => $Date) {
                $T_Date = $Date['T_Date'];
                break;
            }

            unset($this->Cart['Ord_Data']);
            $this->Cart['ItemsData'] = $this->Data['ItemsData'];
            $this->Cart['ExtrasData'] = $this->Data['Extras'];
            $this->Cart['DatesData'] = $this->Data['Dates'];
            $this->Cart['User'] = $this->User;

//            $this->Data['Ord_Status'] = $this->Cart['Ord_Status'] ?: 1; // по умолчанию "ожидание оплаты"
            $this->Data['Ord_Status'] = $this->Cart['Ord_Status'] ?: 4; // по умолчанию "на бронировании"

            if($this->Cart['OrderId'])
                $where = ", Ord_Code = ".$this->Cart['OrderId'];

            $Str = json_encode($this->Cart);
            $Last = DB::selectArray("SHOW TABLE STATUS LIKE 'cartData'");

//            $this->Data['Ord_Number'] = $this->Cart['Ord_Number'] = $Item['T_Barcode'].($this->User['Company'] ? "-".$this->User['Company']['C_Code'] : "")."-".($this->Cart['OrderId'] ?: $Last['Auto_increment']);
            $this->Data['Ord_Number'] = $this->Cart['Ord_Number'] = $Item['T_Barcode']."-".date("dmy", strtotime($T_Date))."-".($this->Cart['OrderId'] ?: $Last['Auto_increment']);

            $query = "REPLACE INTO cartData SET
                   Ord_Time = NOW(),
                   Ord_Number = '".$this->Data['Ord_Number']."',
                   Ord_Status = ".$this->Data['Ord_Status'].",
                   Ord_Sum = ".$this->Cart['TotalSum'].",
                   T_Code = ".$Item['T_Code'].",
                   T_Date = '".$this->Cart['Dates'][$Item['T_Code']]."',
                   us_id = ".$this->User['us_id'].",
                   C_Code = ".($this->User['Company'] ? $this->User['Company']['C_Code'] : "NULL").",
                   Ord_Data = '".mysql_real_escape_string($Str)."'";

            $query .= "$where";
            DB::Query($query);
            $mysqlOrderId = mysql_insert_id();

            if(!$this->Cart['OrderId']) {
                foreach($this->Cart['Items'] as $T_Code => $Quantity) {
                    $Date = $this->Cart['Dates'][$T_Code];
                    break;
                }
    
                classTripCart::Count($T_Code);

                $this->sendAdminMail($mysqlOrderId, array(), true);
            }
            
            $this->Cart['OrderId'] = $this->Data['OrderId'] = $mysqlOrderId;
            
        }


        public function dPrintOrderInfo($OrderId = false) {
            $OrderId = $this->Cart['OrderId'] ?: $OrderId ?: (int)$_REQUEST['OrderId'];

            $Order = DB::selectArray("SELECT * FROM cartData WHERE Ord_Code = $OrderId");
            $this->Data = array_merge($this->Data, classTripOrders::GetOrderVars($Order));

            if(count($this->Data['ItemsData']) <= 0) {
                header("Location: /user/");
                exit;
            }

            $this->Data['Error'] = count($this->Data['Errors']);
            $this->Data['Message'] = count($this->Data['Messages']);

            $this->Ins2Php("template_orderinfo");

        }


        public function sendAdminMail($OrderId = 0, $Data = array(), $SendToAdmin = false) {
            $Order = DB::selectArray("SELECT * FROM cartData WHERE Ord_Code = $OrderId");
            $this->Data = array_merge($this->Data, classTripOrders::GetOrderVars($Order));
            $this->Data = array_merge($this->Data, $Data);

            if(count($this->Data['ItemsData']) <= 0)
                return;

            $Mail = _autoload("classMail");
    
            $Mail->Data['Subject'] = "Бронь ".$this->Data['Ord_Number'];
            $Mail->Data['Message'] = "";

            $Mail->Data = array_merge($Mail->Data, $this->Data);

            if($SendToAdmin) {
                $Template = $this->GetTemplate("template_adminmail");
                $MessageHtml = $Mail->Template2Php($Template);
                $Mail->SendMail($Mail->adminMail.", irina.lavrinova@rostislavl.com", $Mail->Data['Subject'], $MessageHtml);
            }
            
            /* отправялем уведомление пользователю */
            $Mail->Data['Subject'] = $Data['Subject'] ?: "Бронь ".$this->Data['Ord_Number']." на сайте ".DOMAIN_NAME;
            $Template = $Mail->GetTemplate("mailTemplate");
            
            $OrderTemplate = $this->GetTemplate("template_orderinfo");
            $OrderHtml = $Mail->Template2Php($OrderTemplate);
            
            $Emogrifier = new Emogrifier();
            $Emogrifier->setHtml($OrderHtml);
            $Emogrifier->setCss(classMail::getCss(DOMAIN_URL."/jas/mailcart.css"));
            $Mail->Data['Message'] = $Emogrifier->emogrifyBodyContent();
            
            $MessageHtml = $Mail->Template2Php($Template);
            $Mail->SendMail($Mail->Data['User']['mail'], $Mail->Data['Subject'], $MessageHtml);

            if(count($Mail->Errors) > 0) {
                foreach($Mail->Errors as $Error)
                    array_push($this->Data['Errors'], array("Error", $Error));
            }

            $this->Data['Error'] = count($this->Data['Errors']);
            $this->Data['Message'] = count($this->Data['Messages']);
        }


        public function ClearCartData() {
            unset($_SESSION['Cart']['Extras']);
            unset($_SESSION['Cart']['ExtrasData']);
            unset($_SESSION['Cart']['Dates']);
            unset($_SESSION['Cart']['DatesData']);
            unset($_SESSION['Cart']['Discount']);
            unset($_SESSION['Cart']['Pincode']);
            unset($_SESSION['Cart']['TotalSum']);
            unset($_SESSION['Cart']['ExtrasTotalSum']);
            unset($_SESSION['Cart']['TotalOriginalSum']);
            unset($_SESSION['Cart']['Items']);
            unset($_SESSION['Cart']['ItemsData']);
            unset($_SESSION['Cart']['Travelers']);
            unset($_SESSION['Cart']['Delivery']);
            unset($_SESSION['Cart']['Payment']);
            unset($_SESSION['Cart']['Ord_Number']);
            unset($_SESSION['Cart']['OrderId']);
            unset($_SESSION['Cart']['Ord_Status']);
            unset($_SESSION['Cart']['User']);
            unset($_SESSION['Cart']['TravelerPhone']);
            unset($_SESSION['Cart']['TravelerComment']);
        }


        public static function Count($T_Code) {
            $AllCounts = DBMem::Get(self::$CountKey) ?: array();
            $AllCounts[$T_Code] = $T_Code;
            DBMem::Set($AllCounts, self::$CountKey);
        }


        public function CheckPin($Pin = false) {
            $Pin = $Pin ?: preg_replace("/\W+/", "" ,$_REQUEST['Pincode']);
            $Data = array();

            if($Pin) {
                $Pincode = DB::selectArray("SELECT * FROM pinCodes WHERE P_Barcode = '".mysql_real_escape_string($Pin)."'");
                if($Pincode['P_Code'])
                    $Data = $Pincode;
                else
                    $Data['Error'] = true;
            }

            $this->AjaxData = $Data;
            return $Data;
        }


        public function classTripCart($sec = "", $Parent = "") {
            parent::classTrip($sec, $Parent);

            if(!isset($_SESSION['Cart']))
                $_SESSION['Cart'] = array();
            $this->Cart = &$_SESSION['Cart'];
        }

    }

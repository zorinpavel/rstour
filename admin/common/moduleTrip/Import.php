<?php
    /**
     * Date: 09.03.2018
     * Time: 9:33
     * Project: rstour.ru
     */
    
    class importTrip extends classTrip {
        
        public function GetTripByExternal($ExternalCode = 0, $Part = true, $Save = false) {
            $Trip = DB::selectArray("SELECT * FROM tripItems WHERE T_External = $ExternalCode");
            if($Trip['T_Code'])
                return $this->GetItemVars($Trip, $Part, $Save);
            else
                return false;
        }
        
        
        public function UpdateItem($Item) {
            $Query = "
                 T_External     = '".$Item['T_External']."',
                 T_Barcode      = '".$Item['T_Barcode']."',
                 T_Name         = '".$Item['T_Name']."',
                 T_Desc         = '".$Item['T_Desc']."',
                 T_Comment      = '".$Item['T_Comment']."',
                 T_Days         = ".(int)$Item['T_Days'].",
                 T_Price        = ".$Item['T_Price'].",
                 T_KidsDiscount = ".$Item['T_KidsDiscount'].",
                 T_Image        = ".($Item['T_Image'] ? "'".$Item['T_Image']."'" : "NULL").",
                 T_Visible      = ".$Item['T_Visible']."
            ";
            
            $QueryUpdate = "
                 T_External     = '".$Item['T_External']."',
                 T_Barcode      = '".$Item['T_Barcode']."',
                 T_Days         = ".(int)$Item['T_Days'].",
                 T_Price        = ".$Item['T_Price'].",
                 T_KidsDiscount = ".$Item['T_KidsDiscount'].",
                 T_Comment      = '".$Item['T_Comment']."'
            ";
            
            DB::Query("INSERT INTO tripItems SET $Query
                         ON DUPLICATE KEY UPDATE T_Code = LAST_INSERT_ID(T_Code), $QueryUpdate");
            $T_Code = mysql_insert_id();
            
            return $T_Code;
        }
        
        
        public function UpdateDates($T_Code, $Dates = array()) {
            foreach($Dates as $Key => $Value) {
                $Date = dateDecode($Value['Date']);
                $dbValues[] = "($T_Code, '$Date', ".$Value['Places'].", ".($Value['Limit'] ?: 0 ).")";
            }

            if(count($dbValues))
                DB::Query("INSERT INTO tripDates (T_Code, T_Date, T_Places, T_Limit)
                        VALUES
                          ".join(", ", $dbValues)."
                        ON DUPLICATE KEY UPDATE
                            T_Code = VALUES(T_Code), T_Places = VALUES(T_Places), T_Date = VALUES(T_Date), T_Limit = VALUES(T_Limit)");
            
        }
        
        
        public function UpdateOptions($T_Code, $Options = array()) {
            if(count($Options['Types'])) {
                $Values = array();
                foreach($Options['Types'] as $dT_Code)
                    $Values[] = "($T_Code, $dT_Code)";
                if(count($Values))
                    DB::Query("INSERT INTO tripTypes (T_Code, dT_Code)
                            VALUES
                              ".join(", ", $Values)."
                            ON DUPLICATE KEY UPDATE
                                T_Code = VALUES(T_Code), dT_Code = VALUES(dT_Code)");
            }

            if(count($Options['Regions'])) {
                $Values = array();
                foreach($Options['Regions'] as $dR_Code)
                    $Values[] = "($T_Code, $dR_Code)";
                if(count($Values))
                    DB::Query("INSERT INTO tripRegions (T_Code, dR_Code)
                            VALUES
                              ".join(", ", $Values)."
                            ON DUPLICATE KEY UPDATE
                                T_Code = VALUES(T_Code), dR_Code = VALUES(dR_Code)");
            }
            
            if(count($Options['Cities'])) {
                $Values = array();
                foreach($Options['Cities'] as $dC_Code)
                    $Values[] = "($T_Code, $dC_Code)";
                if(count($Values))
                    DB::Query("INSERT INTO tripCities (T_Code, dC_Code)
                            VALUES
                              ".join(", ", $Values)."
                            ON DUPLICATE KEY UPDATE
                                T_Code = VALUES(T_Code), dC_Code = VALUES(dC_Code)");
            }
            
            if(count($Options['Includes'])) {
                $Values = array();
                foreach($Options['Includes'] as $dI_Code)
                    $Values[] = "($T_Code, $dI_Code)";
                if(count($Values))
                    DB::Query("INSERT INTO tripIncluded (T_Code, dI_Code)
                            VALUES
                              ".join(", ", $Values)."
                            ON DUPLICATE KEY UPDATE
                                T_Code = VALUES(T_Code), dI_Code = VALUES(dI_Code)");
            }
            
        }
        
    }
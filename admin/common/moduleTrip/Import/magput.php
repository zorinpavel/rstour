<?php


    include_once(COMMON_PATH."/cUrl.php");
    include_once(MODULE_TRIP_PATH."/Import.php");
    
    $classImport = new importTrip();
    
    $BarcodePrefix = "MP.";
    $XMLArray = cUrl::Request("GET", "http://www.magturyview.ru/xml/1day.php", array(), array(), "XML");

    $Externals = array();
    $Limit = 20;

    foreach($XMLArray['response_params'] as $Key => $Trip) {
        $ExternalKey = false;
        foreach($Trip as $FieldKey => $FieldValue) {
            list($FieldName, $FieldKey) = explode("_", $FieldKey);

            if($FieldName == "id") {
                $Externals[$BarcodePrefix.$FieldValue] = array(
                    "Id" => $BarcodePrefix.$FieldValue
                );
                $ExternalKey = $BarcodePrefix.$FieldValue;
            }

            if($FieldName == "name") {
                $Externals[$ExternalKey]['Name'] = $FieldValue;
            }

            if($FieldName == "description") {
                $Externals[$ExternalKey]['Desc'] = strip_tags($FieldValue, "<br><p>");
            }

            if($FieldName == "url") {
                $Externals[$ExternalKey]['Image'] = $FieldValue;
            }

            if($FieldName == "data") {
                $Externals[$ExternalKey]['Dates'][$FieldKey] = array();
                foreach($FieldValue as $DateKey => $DateValue) {
                    if($DateKey == "data")
                        $Externals[$ExternalKey]['Dates'][$FieldKey]['Date'] = dateDecode($DateValue);
                    
                    if($DateKey == "mest") {
                        $Externals[$ExternalKey]['Dates'][$FieldKey]['Places'] = (int)$DateValue;
                        $Externals[$ExternalKey]['Dates'][$FieldKey]['Limit'] = 0;
                    }
                    
                    if($DateKey == "price_adult")
                        $Externals[$ExternalKey]['Price'] = $Externals[$ExternalKey]['Price'] > (int)$DateValue ? $Externals[$ExternalKey]['Price'] : (int)$DateValue;
                    
                    if($DateKey == "price_child")
                        $Externals[$ExternalKey]['KidsPrice'] = (int)$Externals[$ExternalKey]['Price'] - (int)$DateValue;
                }
            }

        }

        if($Count++ >= $Limit)
            break;

    }
    
    foreach($Externals as $exKey => $ex)
        $prepExternals[$exKey] = "'$exKey'";

    $dbExternals = DB::selectArray("SELECT * FROM tripItems WHERE T_External IN (".join(", ", $prepExternals).")", "T_External");
    
    $Updates = 0;
    $Inserts = 0;

    foreach($Externals as $Key => $ExternalTrip) {
        if($ExternalTrip['Image']) {
            $fileName = strtolower($ExternalTrip['Id']).".".preg_replace("/.*\/(.+\.[a-z]{3})$/", "$1", strtolower($ExternalTrip['Image']));
            if(!is_file(CLIENT_PATH."/data/$fileName")) {
                $file = @file_get_contents($ExternalTrip['Image']);
                if($file) {
                    $ft = fopen(CLIENT_PATH."/data/$fileName", "w");
                    fputs($ft, $file);
                    fclose($ft);
                } else
                    $ExternalTrip['Image'] = false;
            }
        
            $ExternalTrip['Image'] = ($ExternalTrip['Image'] ? "/data/$fileName" : false);
        }
    
        $newTrip = array(
            "T_External"     => $ExternalTrip['Id'],
            "T_Barcode"      => $ExternalTrip['Id'],
            "T_Name"         => $ExternalTrip['Name'],
            "T_Desc"         => $ExternalTrip['Desc'],
            "T_Days"         => 1,
            "T_Price"        => $ExternalTrip['Price'],
            "T_KidsDiscount" => 0,
            "T_Image"        => $ExternalTrip['Image'],
            "T_Visible"      => 0,
            "T_Comment"      => "IMPORT ".date("d.m.Y h:i"),
        );
        $T_Code = $classImport->UpdateItem($newTrip);

        if(in_array($Key, array_keys($dbExternals))) {
            $classImport->UpdateDates($dbExternals[$Key]['T_Code'], $ExternalTrip['Dates']);
    
            $ExternalTrip['Options'] = array(
                "Types"     => array(1),
                "Includes"  => array(1, 10),
            );
            $classImport->UpdateOptions($dbExternals[$Key]['T_Code'], $ExternalTrip['Options']);
    
            @unlink(CACHE_PATH."/".$dbExternals[$Key]['T_Code'].".tourvars");
            @unlink(CACHE_PATH."/".$dbExternals[$Key]['T_Code'].".tourvars.part");

            $Updates++;
        } else {
            $classImport->UpdateDates($T_Code, $ExternalTrip['Dates']);
    
            $ExternalTrip['Options'] = array(
                    "Types"     => array(1),
                    "Includes"  => array(1, 10),
                );
            $classImport->UpdateOptions($T_Code, $ExternalTrip['Options']);
    
            @unlink(CACHE_PATH."/$T_Code.tourvars");
            @unlink(CACHE_PATH."/$T_Code.tourvars.part");

            $Inserts++;
        }
    }
    
    echo "Обновлено: $Updates<br>Добавлено: $Inserts";


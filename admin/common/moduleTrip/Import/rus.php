<?php


    include_once(COMMON_PATH."/cUrl.php");
    include_once(MODULE_TRIP_PATH."/Import.php");
    
    $classImport = new importTrip();
    
    $BarcodePrefix = "R.";
    $JSONArray = cUrl::Request("GET", "https://www.rtoperator.ru/export.html?entity=all&data_type=json");

    $Externals = array();
    $Limit = 300;

    foreach($JSONArray['response_params']['tour'] as $Key => $Trip) {
        $ExternalKey = false;
        foreach($Trip as $FieldName => $FieldValue) {
            if($FieldName == "tour_id") {
                $Externals[$BarcodePrefix.$FieldValue] = array(
                    "Id" => $BarcodePrefix.$FieldValue
                );
                $ExternalKey = $BarcodePrefix.$FieldValue;
            }

            if($FieldName == "title") {
                $Externals[$ExternalKey]['Name'] = strip_tags($FieldValue);
            }

            if($FieldName == "htmlCode") {
                $Externals[$ExternalKey]['Desc'] = strip_tags($FieldValue, "<br><p>");
            }

            if($FieldName == "images") {
                $Externals[$ExternalKey]['Image'] = $FieldValue[0];
            }

            if($FieldName == "dates") {
                $Externals[$ExternalKey]['Dates'] = array();
                foreach($FieldValue as $DateKey => $DateValue) {
                    $Externals[$ExternalKey]['Dates'][$DateKey]['Date'] = dateDecode($DateValue['date_start']);
                    $Externals[$ExternalKey]['Dates'][$DateKey]['Places'] = (int)$DateValue['places_available'] <= 0 ? 0 : (int)$DateValue['places'];
                    $Externals[$ExternalKey]['Dates'][$DateKey]['Limit'] = (int)$DateValue['places_available'];
                }
                $Interval = date_diff(date_create($DateValue['date_start']), date_create($DateValue['date_finish']));
                $Externals[$ExternalKey]['Days'] = (int)$Interval->format('%a') + 1;
            }

            if($FieldName == "services") {
                foreach($FieldValue[0] as $ServiceKey => $ServiceValue) {
                    if($ServiceKey == "price_for_insider")
                        $Externals[$ExternalKey]['Price'] = (int)$ServiceValue;
                    
                    if($ServiceKey == "discount")
                        $Externals[$ExternalKey]['Discounts'] = $ServiceValue;
                }
            }

            if($FieldName == "cities") {
                $Regions[$ExternalKey] = array();
                $ExternalCities = explode(" - ", $FieldValue);
                foreach($ExternalCities as $Key => $exCityName)
                    $ExternalCities[$Key] = "'$exCityName'";
                $Cities[$ExternalKey] = DB::selectArray("SELECT * FROM dataCities WHERE dC_Name IN (".join(", ", $ExternalCities).")", "dC_Code");
                foreach($Cities[$ExternalKey] as $dC_Code => $City)
                    $Regions[$ExternalKey][$City['dR_Code']] = $City['dR_Code'];
            }

        }

        if($Count++ >= $Limit)
            break;

    }



    foreach($Externals as $exKey => $ex)
        $prepExternals[$exKey] = "'$exKey'";

    $dbExternals = DB::selectArray("SELECT * FROM tripItems WHERE T_External IN (".join(", ", $prepExternals).")", "T_External");
    
    $Updates = 0;
    $Inserts = 0;

    foreach($Externals as $Key => $ExternalTrip) {
        if(in_array($Key, array_keys($dbExternals))) {
            $updateTrip = array(
                "T_External"     => $ExternalTrip['Id'],
                "T_Barcode"      => $ExternalTrip['Id'],
                "T_Days"         => $ExternalTrip['Days'],
                "T_Price"        => $ExternalTrip['Price'],
                "T_KidsDiscount" => 0,
                "T_Comment"      => "UPDATE ".date("d.m.Y h:i"),
                "T_Visible"      => 0,
            );
            $T_Code = $classImport->UpdateItem($updateTrip);

            $classImport->UpdateDates($dbExternals[$Key]['T_Code'], $ExternalTrip['Dates']);

            @unlink(CACHE_PATH."/".$dbExternals[$Key]['T_Code'].".tourvars");
            @unlink(CACHE_PATH."/".$dbExternals[$Key]['T_Code'].".tourvars.part");

            $Updates++;
        } else {
            if($ExternalTrip['Image']) {
                $fileName = strtolower($ExternalTrip['Id']).".".preg_replace("/.*\/(.+\.[a-z]{3})$/", "$1", strtolower($ExternalTrip['Image']));
                if(!is_file(CLIENT_PATH."/data/$fileName")) {
                    $file = @file_get_contents($ExternalTrip['Image']);
                    if($file) {
                        $ft = fopen(CLIENT_PATH."/data/$fileName", "w");
                        fputs($ft, $file);
                        fclose($ft);
                    } else
                        $ExternalTrip['Image'] = false;
                }

                $ExternalTrip['Image'] = ($ExternalTrip['Image'] ? "/data/$fileName" : false);
            }

            $newTrip = array(
                "T_External"     => $ExternalTrip['Id'],
                "T_Barcode"      => $ExternalTrip['Id'],
                "T_Name"         => $ExternalTrip['Name'],
                "T_Desc"         => $ExternalTrip['Desc'],
                "T_Days"         => $ExternalTrip['Days'],
                "T_Price"        => $ExternalTrip['Price'],
                "T_KidsDiscount" => 0,
                "T_Image"        => $ExternalTrip['Image'],
                "T_Visible"      => 0,
                "T_Comment"      => "IMPORT ".date("d.m.Y h:i"),
            );
            $T_Code = $classImport->UpdateItem($newTrip);

            $ExternalTrip['Options'] = array(
                "Types"     => array(($Externals[$Key]['Days'] > 1 ? 2 : 1), 3, 4),
                "Cities"    => array_keys($Cities[$Key]),
                "Regions"   => array_keys($Regions[$Key]),
                "Includes"  => array(1, 10),
            );
            $classImport->UpdateOptions($T_Code, $ExternalTrip['Options']);

            $classImport->UpdateDates($T_Code, $ExternalTrip['Dates']);

            @unlink(CACHE_PATH."/$T_Code.tourvars");
            @unlink(CACHE_PATH."/$T_Code.tourvars.part");

            $Inserts++;
        }
    }
    
    echo "Обновлено: $Updates<br>Добавлено: $Inserts";
    
    if(debug()) {
        ?><pre><? print_r(array_keys($Externals)); ?></pre><?
    }


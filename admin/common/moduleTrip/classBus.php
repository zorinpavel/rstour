<?php


    class classBus {

        public static $ColsNames = array("0", "A", "B", "C", "D", "E", "F");

        public static $BusSettings = array(
            0 => array(
                "Name" => "Стандартный автобус",
                "rows" => 13,
                "cols" => 4,
                "reserved" => array(
                    "1C", "1D"
                ),
                "excluded" => array(
                    "6C", "6D", "7C", "7D"
                ),
            )
        );


        public static function GetBus($Bus_Code = 0) {
            $Bus_Code = $Bus_Code ?: 0;
            return self::$BusSettings[$Bus_Code];
        }

    }

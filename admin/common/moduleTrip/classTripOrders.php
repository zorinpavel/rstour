<?php


    class classTripOrders extends classTripCart {

        static $DoID = "todo";
        static $OrderID = "oid";
        static $DocumentID = "did";

        public $OrderId, $DocumentId;

        var $Properties = array(
            "public" => array(
                "Names" => array(
                    "Section",
                ),
                "Types" => array(
                    "InputSectionsTree",
                ),
                "Labels" => array(
                    "Основной раздел",
                )
            ),
            "private" => array(
                "Names" => array(
                    "template_top",
                    "template_items",
                    "template_bottom",
                    "Separator",
                    "template_one",
                    "template_empty",
                    "template_forbidden",
                    "Separator",
                    "template_edit",
                    "Separator",
                    "template_voucher",
                    "template_booking",
                    "template_invoice",
                    "template_invoice_bank",
                ),
                "Types" => array(
                    "InputTemplates",
                    "InputTemplates",
                    "InputTemplates",
                    "Separator",
                    "InputTemplates",
                    "InputTemplates",
                    "InputTemplates",
                    "Separator",
                    "InputTemplates",
                    "Separator",
                    "InputTemplates",
                    "InputTemplates",
                    "InputTemplates",
                    "InputTemplates",
                ),
                "Labels" => array(
                    "Верх",
                    "Список заказов",
                    "Низ",
                    "",
                    "Один заказ",
                    "Нет заказов",
                    "Доступ закрыт",
                    "",
                    "Редактировать",
                    "Документы",
                    "Ваучер",
                    "Подтверждение бронирования",
                    "Счет на оплату",
                    "Квитанция",
                )
            )
        );

        var $DefaultTemplates = array(
            "template_top" => array(0 => ""),
            "template_items" => array(0 => ""),
            "template_bottom" => array(0 => ""),
            "template_one" => array(0 => ""),
            "template_empty" => array(0 => ""),
            "template_forbidden" => array(0 => ""),
            "template_edit" => array(0 => ""),
            "template_voucher" => array(0 => ""),
            "template_booking" => array(0 => ""),
            "template_invoice" => array(0 => ""),
            "template_invoice_bank" => array(0 => ""),
        );

        function GetClassName() {
            return __CLASS__;
        }

        function Action($Do = "") {
            $Do = $Do ?: $_REQUEST[self::$DoID];

            $this->Data['Errors'] = array();
            $this->Data['Messages'] = array();

            if($this->CanRead) {

                switch($Do) {
                    case "print":
                        if($this->OrderId)
                            $this->DocumentPrint();
                        break;
                    case "Edit":
                        $this->Edit();
                        break;
                    case "Update":
                        $this->Update();
                        break;
                    case "show":
                    default:
                        if($this->OrderId)
                            $this->ShowOrder();
                        else
                            $this->ShowOrders(0);
                        break;
                }
            } else {
                array_push($this->Data["Errors"], array("Error" => "Вы не можете просматривать заказы"));
                $this->Data['Error'] = 1;
                $this->Ins2Php("template_forbidden");
            }

            if(debug()) {
                ?><pre><? print_r($this->Data); ?></pre><?
            }
        }


        public function Edit($Try = false) {
            if(!$this->OrderId)
                $this->CanEdit = $this->Can['edit'] = false;
            else {
                $Orders = $this->GetOrders();
                $cartData = json_decode($Orders[$this->OrderId]['Ord_Data'], true);
                if(!$Orders[$this->OrderId] || in_array($Orders[$this->OrderId]['Ord_Status'], array(2, 3, 6, 7, 9)))
                    $this->CanEdit = $this->Can['edit'] = false;
                else
                    $this->Data = array_merge($this->Data, $this->GetOrderVars($Orders[$this->OrderId]));
            }

            if(!$this->CanEdit) {
                array_push($this->Data["Errors"], array("Error" => "Вы не можете редактировать эту бронь"));
                $this->Data['Error'] = 1;
                $this->Ins2Php("template_forbidden");
                return false;
            }

            if($Try) {
                $Error = false;
                $UpdateFlag = false;
                $EventsArray = array();
                foreach($this->Data['Travelers'] as $Key => $Traveler) {
                    if(
                        $Traveler['name'] != $_REQUEST['Travelers'][$Key]['name'] ||
                        $Traveler['lastname'] != $_REQUEST['Travelers'][$Key]['lastname'] ||
                        $Traveler['middlename'] != $_REQUEST['Travelers'][$Key]['middlename'] ||
                        $Traveler['sex'] != $_REQUEST['Travelers'][$Key]['sex'] ||
                        $Traveler['birthdate'] != $_REQUEST['Travelers'][$Key]['birthdate'] ||
                        $Traveler['passport']['number'] != $_REQUEST['Travelers'][$Key]['passport']['number']
//                        $Traveler['passport']['date'] != $_REQUEST['Travelers'][$Key]['passport']['date']
                    ) {
                        $newTraveler = $_REQUEST['Travelers'][$Key];
                        $newTraveler['kid'] = $Traveler['kid'];
                        $newTraveler['Extras'] = $Traveler['Extras'];

                        $EventsArray['Travelers '.$Key] = "";
                        foreach($_REQUEST['Travelers'][$Key] as $rKey => $rValue) {
                            if($Traveler[$rKey] != $_REQUEST['Travelers'][$Key][$rKey] && !is_array($rValue))
                                $EventsArray['Travelers '.$Key] .= "\n            $rKey: ".$Traveler[$rKey]." => ".$_REQUEST['Travelers'][$Key][$rKey];
                            elseif(is_array($rValue) && $rKey != "Extras") {
                                $EventsArray['Travelers '.$Key] .= "\n            $rKey:";
                                foreach($rValue as $rValueKey => $rValueValue)
                                    $EventsArray['Travelers '.$Key] .= "\n                  $rValueKey: ".$Traveler[$rKey][$rValueKey]." => ".$_REQUEST['Travelers'][$Key][$rKey][$rValueKey];
                            }
                        }

                        if(!$_REQUEST['Travelers'][$Key]['name'] || !$_REQUEST['Travelers'][$Key]['lastname'] || !$_REQUEST['Travelers'][$Key]['middlename'])
                            $Error = "Необходимо заполнить все поля";
//                        if(!$_REQUEST['Travelers'][$Key]['kid'] && $_REQUEST['Travelers'][$Key]['birthdate'] < date("Y-m-d", mktime(0, 0, 0, date("m"), date("d"), date("Y")-14)) && (!$_REQUEST['Travelers'][$Key]['passport']['number'] || !$_REQUEST['Travelers'][$Key]['passport']['date']))
//                            $Error = "Необходимо заполнить данные папорта";

                        $this->Data['Travelers'][$Key] = $cartData['Travelers'][$Key] = $newTraveler;
                        $UpdateFlag = true;
                    }
                }

                if($Error)
                    array_push($this->Data['Errors'], array("Error" => $Error));


                $Delivery_Addr = mysql_real_escape_string(strip_tags($_REQUEST['Delivery_Addr']));
                $Delivery_Name = mysql_real_escape_string(strip_tags($_REQUEST['Delivery_Name']));
                $Delivery_Comment = mysql_real_escape_string(strip_tags($_REQUEST['Delivery_Comment']));

                if($this->Data['Delivery'] && $this->Data['Delivery']['DS_SelfPayment']) {
                    if(!$Delivery_Addr)
                        array_push($this->Data['Errors'], array("Error" => "Укажите адрес доставки"));
                    if(!$Delivery_Name)
                        array_push($this->Data['Errors'], array("Error" => "Укажите имя получателя"));

                    if(
                        $this->Data['Delivery']['Delivery_Addr'] != $Delivery_Addr ||
                        $this->Data['Delivery']['Delivery_Name'] != $Delivery_Name ||
                        $this->Data['Delivery']['Delivery_Comment'] != $Delivery_Comment
                    ) {
                        $EventsArray['Delivery']  = "\n            ".$this->Data['Delivery']['Delivery_Name']." => ".$Delivery_Name;
                        $EventsArray['Delivery'] .= "\n            ".$this->Data['Delivery']['Delivery_Addr']." => ".$Delivery_Addr;
                        $EventsArray['Delivery'] .= "\n            ".$this->Data['Delivery']['Delivery_Comment']." => ".$Delivery_Comment;
                        $cartData['Delivery']['Delivery_Addr'] = $Delivery_Addr;
                        $cartData['Delivery']['Delivery_Name'] = $Delivery_Name;
                        $cartData['Delivery']['Delivery_Comment'] = $Delivery_Comment;
                        $this->Data['Delivery'] = $cartData['Delivery'] = array_merge($this->Data['Delivery'], $cartData['Delivery']);
                        $this->Data = array_merge($this->Data, $this->Data['Delivery']);
                        $UpdateFlag = true;
                    }
                }
    
                $cartData['TravelerPhone'] = $this->Data['TravelerPhone'] = mysql_real_escape_string(strip_tags($_REQUEST['TravelerPhone']));
                $cartData['TravelerComment'] = $this->Data['TravelerComment'] = mysql_real_escape_string(strip_tags($_REQUEST['TravelerComment']));
    
                $classCart = _autoload("classTripCart");
                $classCart->Cart = $cartData;
                $classCart->Cart->User = $cartData['User'];
                $classCart->GetItems();
                
                if($cartData['TotalSum'] != $classCart->Cart['TotalSum']) {
                    $newTotalSum = $classCart->Cart['TotalSum'];
                    $EventsArray['TotalSum']  = "\n            ".$cartData['TotalSum']." => ".$classCart->Cart['TotalSum'];
                    $UpdateFlag = true;
                }
                if($cartData['TotalOriginalSum'] != $classCart->Cart['TotalOriginalSum']) {
                    $newTotalSum = $classCart->Cart['TotalOriginalSum'];
                    $EventsArray['TotalOriginalSum']  = "\n            ".$cartData['TotalOriginalSum']." => ".$classCart->Cart['TotalOriginalSum'];
                    $UpdateFlag = true;
                }
                if($cartData['ExtrasTotalSum'] != $classCart->Cart['ExtrasTotalSum']) {
                    $newTotalSum = $classCart->Cart['ExtrasTotalSum'];
                    $EventsArray['ExtrasTotalSum']  = "\n            ".$cartData['ExtrasTotalSum']." => ".$classCart->Cart['ExtrasTotalSum'];
                    $UpdateFlag = true;
                }
                
                $cartData = $classCart->Cart;
                $classCart->ClearCartData();

                if($UpdateFlag && !count($this->Data['Errors'])) {
                    $DBCartData = json_encode($cartData);
                    DB::Query("UPDATE cartData SET ".($newTotalSum ? "Ord_Sum = '$newTotalSum', " : "")." Ord_Data = '".mysql_real_escape_string($DBCartData)."', events = CONCAT(events, '".classUserEvents::Events("USER EDIT", $EventsArray)."') WHERE Ord_Code = ".$this->OrderId);
                }
            }
    
            $this->Data['Error'] = count($this->Data['Errors']);
            
            if($Try && !count($this->Data['Errors']))
                return true;

            $this->Ins2Php("template_edit");

            return false;
        }


        public function Update() {
            if($this->Edit(true)) {
                array_push($this->Data["Messages"], array("Message" => "Ваши изменения сохранены и отправлены на обработку"));
                $this->Data['Message'] = 1;
                $this->Ins2Php("template_edit");
            }
        }


        public function DocumentPrint() {
            $Orders = $this->GetOrders();
            $Order = $Orders[$this->OrderId];

            if($Order['Ord_Code'] && $this->DocumentId && ($Order['us_id'] == $this->User['us_id'] || $Order['C_Code'] == $this->User['Company']['C_Code'] || debug())) {
                $this->Data = array_merge($this->Data, $this->GetOrderVars($Order));
                $this->Data = array_merge($this->Data, $this->Data['ItemsData'][$Order['T_Code']]);

                switch($this->DocumentId) {
                    case 1:
                    default:
                        $this->Ins2Php("template_voucher");
                        break;
                    case 2:
                        $this->Ins2Php("template_booking");
                        break;
                    case 3:
                        $this->Data['Commission'] = 30;
                        $this->Ins2Php("template_invoice");
                        break;
                    case 4:
                        $this->Ins2Php("template_invoice_bank");
                        break;
                }
            } else {
                array_push($this->Data["Errors"], array("Error" => "Заказов не найдено или не указан документ"));
                $this->Data['Error'] = 1;
                $this->CanRead = $this->Can['read'] = false;
                $this->Ins2Php("template_forbidden");
            }
        }


        public function ShowOrder() {
            $Orders = $this->GetOrders();
            $Order = $Orders[$this->OrderId];

            if($Order['Ord_Code']) {
                $this->Data = array_merge($this->Data, $this->GetOrderVars($Order));

                if(in_array($this->Data['Ord_Status'], array(2, 3, 6, 7, 9)))
                    $this->CanEdit = $this->Can['edit'] = false;

                $this->Ins2Php("template_one");
            } else {
                array_push($this->Data["Errors"], array("Error" => "Заказов не найдено"));
                $this->Data['Error'] = 1;
                $this->Ins2Php("template_empty");
            }

        }


        public function ShowOrders($Limit = 20) {
            $Orders = $this->GetOrders();
            $this->Data['OrdersCount'] = count($Orders);

            $this->Ins2Php("template_top");

            if($this->Data['OrdersCount']) {

                $OrderCond = array();
                if($_REQUEST['order']) {
                    foreach($_REQUEST['order'] as $Field => $Direction) {
                        $OrderCond[] = $Field;
                        $OrderCond[] = $Direction;
                    }
                }
                $OrderCond = array_merge( $OrderCond, array("Ord_Status", "ASC", "Ord_Change", "DESC", "Ord_Code", "DESC") );
                $Orders = SortArray($Orders, $OrderCond);

                if($this->Scroll)
                    $this->Scroll->Data['OrdersCount'] = $this->Data['OrdersCount'];

                if($Limit) {
                    $Orders = array_slice($Orders, 0, $Limit, true);
                } elseif( $this->Scroll && ($this->Scroll->Data['Part'] > $this->Scroll->GetProperty("Groups")) ) {
                    $Orders = array_slice($Orders, ( $this->Scroll->Data['First'] - ( $this->Scroll->Data['Limit'] * ($this->Scroll->Data['Part'] - 1) ) ), $this->Scroll->Data['Limit'], true);
                } else {
                    $Orders = array_slice($Orders, $this->Scroll->Data['First'], $this->Scroll->Data['Limit'], true);
                }
    
                // дополнительная сортировка
                foreach($Orders as $Ord_Code => $Order) {
                    switch($Order['Ord_Status']) {
                        case 8:
                            $_order = 10;
                            break;
                        case 2:
                            $_order = 9;
                            break;
                        case 1:
                            $_order = 8;
                            break;
                        case 4:
                            $_order = 7;
                            break;
                        case 5:
                            $_order = 6;
                            break;
                        case 3:
                        case 9:
                            $_order = 5;
                            break;
                        default:
                            $_order = 1;
                            break;
                    }
                    $Orders[$Ord_Code]['_order'] = $_order;
                }
                $Orders = SortArray($Orders, array("_order", "DESC", "Ord_Status", "ASC", "Ord_Change", "DESC", "Ord_Code", "DESC"));
                
                $Data = $this->Data;
                $CanEdit = $this->CanEdit;
                foreach($Orders as $Ord_Code => $Order) {
                    $Order = $this->GetOrderVars($Order);

                    $this->Data = array_merge($Data, $Order);

                    if(in_array($this->Data['Ord_Status'], array(2, 3, 6, 7, 9)))
                        $this->CanEdit = $this->Can['edit'] = false;
                    else
                        $this->CanEdit = $this->Can['edit'] = $CanEdit;

                    $this->Ins2Php("template_items");
                }
                $this->Data = $Data;

            } else {
                $this->Data['Empty'] = 1;
                $this->Ins2Php("template_empty");
            }

            $this->Ins2Php("template_bottom");

        }


        public function GetOrders($Data = array()) {
            $Data = $Data ?: $_REQUEST;

            if(!debug()) {
                if($this->User['Company']) {
                    $Cond['company'] = "C_Code = ".$this->User['Company']['C_Code'];
                } else {
                    $Cond['us_id'] = "us_id = ".$this->User['us_id'];
                    $Cond['company'] = "C_Code IS NULL";
                }
            }
        
            if($this->OrderId)
                $Cond['orderid'] = "Ord_Code = ".$this->OrderId;

            if($Data['Ord_Status'] && !$Data['T_Archive'])
                $Cond['status'] = "Ord_Status IN (".join(", ", $Data['Ord_Status']).")";
            
            if($Data['T_Date'])
                $Cond['date'] = "T_Date >= '".dateDecode($Data['T_Date'])."'";
            if($Data['T_Date_end'])
                $Cond['date'] = "(T_Date >= '".dateDecode($Data['T_Date'])."' AND T_Date <= '".dateDecode($Data['T_Date_end'])."')";

            if($Data['T_Name']) {
                $TripCodes = DB::selectArray("SELECT T_Code FROM tripItems WHERE LOWER(T_Name) LIKE LOWER('%".$Data['T_Name']."%')", "T_Code");
                if(count($TripCodes))
                    $Cond['name'] = "T_Code IN (".join(", ", array_keys($TripCodes)).")";
                else
                    $Cond['name'] = "false";
            }

            $Cond['all'] = "true";
            $Conds = join(" AND ", $Cond);

            $Orders = DB::selectArray("SELECT * FROM cartData WHERE $Conds", "Ord_Code");
           
            $this->Owner = true;

            return $Orders;
        }


        public static function GetOrderVars($Order) {
            $Order['Status_Name'] = self::$OrderStatuses[$Order['Ord_Status']];
            $Order['Ord_Date'] = self::mkToday($Order['Ord_Change']);

            $Order = array_merge(json_decode($Order['Ord_Data'], true), $Order);

            foreach($Order['Items'] as $T_Code => $Quantity)
                break;
            
            $TravelersCount = count($Order['Travelers']);
            if($TravelersCount < $Quantity) {
                for($i = 1; $i <= $Quantity; $i++) {
                    if(!$Order['Travelers'][$i])
                        $Order['Travelers'][$i] = array();
                }
            }

            $Order['Ord_Link'] = self::$SectionPath."?".self::$OrderID."=".$Order['Ord_Code'];
            $Order['Trip_Date'] = $Order['DatesData'][$T_Code]['T_Date'];
            $Order['Trip_DateStr'] = date("j", strtotime($Order['Trip_Date']))." ".self::$RusMonthsInt[date("n", strtotime($Order['Trip_Date']))]." ".self::$RusDayWeek[date("N", strtotime($Order['Trip_Date']))];
            $Order['TravelersCount'] = count($Order['Travelers']);

            return $Order;
        }


        public function classTripOrders($sec = "", $Parent = "") {
            parent::classTripCart($sec, $Parent);

            $this->OrderId = $this->Data['OrderId']   = (int)$_REQUEST[self::$OrderID];
            $this->DocumentId = $this->Data['DocumentId']   = (int)$_REQUEST[self::$DocumentID];

            self::$Sec_Code = $this->Data['Sec_Code'] = $this->GetProperty("Section") ?: $this->GetSectionID();
            self::$SectionPath = $this->Data['SectionPath'] = $this->GetPath(self::$Sec_Code);

        }

    }

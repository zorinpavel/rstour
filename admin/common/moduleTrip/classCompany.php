<?php


    class classCompany extends classSecurity {

        static $DoID = "codo";
        static $ItemID = "cocode";
        static $DocumentID = "did";
        static $SectionPath;

        public $DocumentId, $Limit, $Scroll, $C_Code;

        var $Properties = array(
            "public" => array(
                "Names" => array(
                    "Scroll",
                ),
                "Types" => array(
                    "InputScroll",
                ),
                "Labels" => array(
                    "Показывать по",
                )
            ),
            "private" => array(
                "Names" => array(
//                    "Limit",
                    "template_documents_list",
                    "template_edit",
                    "template_forbidden",
                    "Separator",
                    "template_agreement",
                    "template_annex1",
                ),
                "Types" => array(
//                    "T5",
                    "InputTemplates",
                    "InputTemplates",
                    "InputTemplates",
                    "Separator",
                    "InputTemplates",
                    "InputTemplates",
                ),
                "Labels" => array(
//                    "Ограничение вывода",
                    "Список документов",
                    "Редатировать",
                    "Доступ запрещен",
                    "Документы",
                    "Договор",
                    "Приложение 1",
                )
            )
        );

        var $DefaultTemplates = array(
            "template_documents_list" => array(0 => ""),
            "template_agreement" => array(0 => ""),
            "template_annex1" => array(0 => ""),
            "template_edit" => array(0 => ""),
            "template_forbidden" => array(0 => ""),
        );

        function GetClassName() {
            return __CLASS__;
        }

        function Action($Do = "") {
            $Do = $Do ?: $_REQUEST[self::$DoID];

            switch($Do) {
                case "print":
                    if($this->C_Code)
                        $this->DocumentPrint();
                    break;
                case "edit":
                        $this->Edit();
                    break;
                case "update":
                        $this->Update();
                    break;
                default:
                    $this->Ins2Php("template_documents_list");
                    break;
            }

            if(debug()) {
                ?><pre><? print_r($this->Data); ?></pre><?
            }

        }


        public function Edit() {
            if($this->User['Company'] && $this->User['Company']['C_Code'] == $this->C_Code) {
                $this->Data = array_merge($this->Data, $this->GetCompany($this->User['Company']['C_Code']));
                $this->Ins2Php("template_edit");
            } else {
                array_push($this->Data["Errors"], array("Error" => "Компания не найдена или у вас нет доступа"));
                $this->Data['Error'] = true;
                $this->CanRead = $this->Can['read'] = false;
                $this->Ins2Php("template_forbidden");
            }
        }


        public function DocumentPrint() {
            if($this->User['Company'] && $this->User['Company']['C_Code'] == $this->C_Code) {
                $this->Data = array_merge($this->Data, $this->GetCompany($this->User['Company']['C_Code']));
                switch($this->DocumentId) {
//                    case 2:
//                        $this->Ins2Php("template_annex1");
//                        break;
                    case 1:
                    default:
                        $this->Ins2Php("template_agreement");
                        $this->Ins2Php("template_annex1");
                        break;
                }
            } else {
                array_push($this->Data["Errors"], array("Error" => "Компания не найдена или у вас нет доступа"));
                $this->Data['Error'] = true;
                $this->CanRead = $this->Can['read'] = false;
                $this->Ins2Php("template_forbidden");
            }
        }


        public static function GetCompany($C_Code = 0) {
            $Company = DB::selectArray("SELECT * FROM Companies WHERE C_Code = $C_Code AND C_Active = 1");
            if($Company['C_Code'])
                return $Company;
            else
                return false;
        }


        public static function GetUserCompany($us_id = 0) {
            $Company = DB::selectArray("SELECT c.* FROM UserCompanies AS uc INNER JOIN Companies AS c ON c.C_Code = uc.C_Code AND c.C_Active = 1 WHERE uc.us_id = $us_id");
            if($Company['C_Code'])
                return $Company;
            else
                return false;
        }


        public function CheckValidReg($Try = false) {
            if($Try) {
                $C_Name = $this->Data['C_Name'] = db_quote(strip_tags($_REQUEST['C_Name']));
                $C_FullName = $this->Data['C_FullName'] = db_quote(strip_tags($_REQUEST['C_FullName']));
                $C_Phone = $this->Data['C_Phone'] = db_quote(strip_tags($_REQUEST['C_Phone']));
                $C_Address = $this->Data['C_Address'] = db_quote(strip_tags($_REQUEST['C_Address']));
                $C_PostAddress = $this->Data['C_PostAddress'] = db_quote(strip_tags($_REQUEST['C_PostAddress']));
                $C_UrAddress = $this->Data['C_UrAddress'] = db_quote(strip_tags($_REQUEST['C_UrAddress']));
                $C_BossName = $this->Data['C_BossName'] = db_quote(strip_tags($_REQUEST['C_BossName']));
                $C_NDS = $this->Data['C_NDS'] = (int)$_REQUEST['C_NDS'];
                $C_INN = $this->Data['C_INN'] = db_quote(strip_tags($_REQUEST['C_INN']));
                $C_KPP = $this->Data['C_KPP'] = db_quote(strip_tags($_REQUEST['C_KPP']));
                $C_BankName = $this->Data['C_BankName'] = db_quote(strip_tags($_REQUEST['C_BankName']));
                $C_Account = $this->Data['C_Account'] = db_quote(strip_tags($_REQUEST['C_Account']));
                $C_BankBik = $this->Data['C_BankBik'] = db_quote(strip_tags($_REQUEST['C_BankBik']));
                $C_BankAccount = $this->Data['C_BankAccount'] = db_quote(strip_tags($_REQUEST['C_BankAccount']));

                if(!$C_Name || !$C_FullName || !$C_Phone || !$C_Address || !$C_PostAddress || !$C_UrAddress || !$C_BossName || !$C_INN || !$C_KPP || !$C_BankName || !$C_Account || !$C_BankBik || !$C_BankAccount)
                    array_push($this->Data['Errors'], array("Error" => "Необходимо заполнить все поля"));

                $this->Data['Error'] = count($this->Data['Errors']);
                if(!$this->Data['Error'])
                    return true;
            }
            return false;
        }


        public function Update($us_id = 0) {
            $us_id = $us_id ?: $this->User['us_id'];
            if($this->CheckValidReg(true)) {
                if($us_id) {
                    DB::Query(($this->C_Code ? "UPDATE Companies" : "INSERT INTO Companies")."
                                    SET C_Active = 1,
                                        C_Date = NOW(),
                                        C_Name = '".$this->Data['C_Name']."',
                                        C_FullName = '".$this->Data['C_FullName']."',
                                        C_Phone = '".$this->Data['C_Phone']."',
                                        C_Address = '".$this->Data['C_Address']."',
                                        C_PostAddress = '".$this->Data['C_PostAddress']."',
                                        C_UrAddress = '".$this->Data['C_UrAddress']."',
                                        C_BossName = '".$this->Data['C_BossName']."',
                                        C_NDS = ".$this->Data['C_NDS'].",
                                        C_INN = '".$this->Data['C_INN']."',
                                        C_KPP = '".$this->Data['C_KPP']."',
                                        C_BankName = '".$this->Data['C_BankName']."',
                                        C_Account = '".$this->Data['C_Account']."',
                                        C_BankBik = '".$this->Data['C_BankBik']."',
                                        C_BankAccount = '".$this->Data['C_BankAccount']."'".
                                    ($this->C_Code ? "WHERE C_Code = ".$this->C_Code : "")
                              );
                    $this->C_Code = $this->C_Code ?: mysql_insert_id();

                    DB::Query("REPLACE INTO UserCompanies SET us_id = $us_id, C_Code = ".$this->C_Code);
                    array_push($this->Data['Messages'], array("Message" => "Данные вашей компании сохранены"));
                } else
                    array_push($this->Data['Errors'], array("Error" => "Для регистрации или редактирования компании нужен активный пользователь"));
            }

            $this->Data['Error'] = count($this->Data['Errors']);
            $this->Data['Message'] = count($this->Data['Messages']);

            $this->Ins2Php("template_edit");
        }


        public function classCompany($sec = "", $Parent = "") {
            parent::classSecurity($sec, $Parent);

            $this->Scroll = $this->GetBlock("Scroll");
            $this->Limit = $this->GetProperty("Limit");

            $this->C_Code = $this->Data['C_Code'] = (int)$_REQUEST[self::$ItemID] ?: $this->User['Company']['C_Code'];
            $this->DocumentId = $this->Data['DocumentId']   = (int)$_REQUEST[self::$DocumentID];

            self::$SectionPath = $this->Data['SectionPath'] = $this->GetPath($this->GetSectionID());

            $this->Data['Errors'] = array();
            $this->Data['Messages'] = array();

        }

    }
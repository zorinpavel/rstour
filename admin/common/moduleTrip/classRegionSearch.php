<?php
    /**
     * Date: 04.10.2018
     * Time: 22:59
     * Project: rstour.ru
     */


    class classRegionSearch {

        public $AjaxData;

        public function SearchByValue($Str = false) {
            $this->AjaxData['Errors'] = array();
            $this->AjaxData['Results'] = array();

            $Str = mysql_real_escape_string(mb_strtolower($Str ?: trim($_REQUEST['value'])));
            $Words = $this->Clear($Str);

            $Cond = array();
            foreach($Words as $Word)
                $Cond[] = "LOWER(dr.dR_Name) LIKE LOWER('%$Word%')";

            $Regions = DB::selectArray("SELECT dR_Name AS Name, dR_Code AS Value, 1 AS Level, dR_Code FROM dataRegions AS dr
                                                            WHERE ".join(" OR ", $Cond)." LIMIT 10", "dR_Code");

            foreach($Regions as $dR_Code => $Region) {
                $Region['Key'] = "dR_Code";
                $this->AjaxData['Results']["r_".$dR_Code] = $Region;
            }

            $Cond = array();
            foreach($Words as $Word)
                $Cond[] = "LOWER(dc.dC_Name) LIKE LOWER('%$Word%')";

            $Cities  = DB::selectArray("SELECT dC_Name AS Name, dC_Code AS Value, 2 AS Level, dC_Code, dr.dR_Code, dr.dR_Name FROM dataCities AS dc
                                          INNER JOIN dataRegions AS dr
                                            ON dr.dR_Code = dc.dR_Code 
                                          WHERE ".join(" OR ", $Cond)." LIMIT 10", "dC_Code");

            if(count($Cities)) {
                foreach($Cities AS $dC_Code => $City) {
                    $City['Key'] = "dC_Code";
                    if(!in_array("r_".$City['dR_Code'], array_keys($this->AjaxData['Results']))) {
                        $this->AjaxData['Results']["r_".$City['dR_Code']] = array(
                            "Name" => $City['dR_Name'],
                            "Value" => $City['dR_Code'],
                            "dR_Code" => $City['dR_Code'],
                            "Level" => 1,
                            "Key" => "dR_Code",
                        );
                    }

                    $this->AjaxData['Results'] = insertAfter($this->AjaxData['Results'], "r_".$City['dR_Code'], "c_".$dC_Code, $City);
                }
            }

//            $Cond = array();
//            foreach($Words as $Word)
//                $Cond[] = "LOWER(t.T_Name) LIKE LOWER('%$Word%')";
//
//            $Items  = DB::selectArray("SELECT t.T_Name AS Name, t.T_Code AS Value, 3 AS Level, t.T_Code, dc.dC_Code, dc.dC_Name FROM tripItems AS t
//                                          INNER JOIN tripCities AS tc
//                                            ON tc.T_Code = t.T_Code
//                                          INNER JOIN dataCities AS dc
//                                            ON dc.dC_Code = tc.dC_Code
//                                          WHERE ".join(" OR ", $Cond)." LIMIT 10", "T_Code");

            $classTrip = _autoload("classTrip");
            $classTrip->DataType = 1;
            $Items = $classTrip->GetItems(array("sWord" => $Str));

            if(count($Items)) {
                foreach($Items AS $T_Code => $Item) {
                    $Item = $classTrip->GetItemVars($Item, true, false);
                    $Item['Key']   = "tcode";
                    $Item['Name']  = $Item['T_Name'];
                    $Item['Value'] = $Item['T_Code'];
                    $Item['Level'] = 3;
                    foreach($Item['Cities'] as $dC_Code => $City)
                        break;
                    $Item['dC_Code'] = $City['dC_Code'];
                    $Item['dC_Name'] = $City['dC_Name'];
                    unset($Item['events']);

                    if(!in_array("c_".$Item['dC_Code'], array_keys($this->AjaxData['Results']))) {
                        $this->AjaxData['Results']["c_".$Item['dC_Code']] = array(
                            "Name" => $Item['dC_Name'],
                            "Value" => $Item['dC_Code'],
                            "dC_Code" => $Item['dC_Code'],
                            "Level" => 2,
                            "Key" => "dC_Code",
                        );
                    }

                    $this->AjaxData['Results'] = insertAfter($this->AjaxData['Results'], "c_".$Item['dC_Code'], "t_".$T_Code, $Item);
                }
            }

            if(debug()) {
                $this->AjaxData['Results'][0] = array(
                    "Name"  => join(" OR ", $Cond),
                    "Value" => 0,
                );
                $this->AjaxData['Results'][1] = $Cities;
                $this->AjaxData['Results'][2] = $Items;
            }

            if(!count($this->AjaxData['Results']))
                array_push($this->AjaxData['Errors'], array("Error" => "Мест не найдено"));

            $this->AjaxData['Error'] = count($this->AjaxData['Errors']);

            if(debug()) {
                ?><pre><? print_r($this->AjaxData); ?></pre><?
            }
        }


        public function Clear($query) {
            $keywords = array();

            $query = dropWords($query);
            $words = preg_split("/\s+/", $query, 0, PREG_SPLIT_NO_EMPTY);

            foreach($words as $word) {
                $word = cutWord($word);
                if($word)
                    array_push($keywords, $word);
            }

            return $keywords;
        }

    }
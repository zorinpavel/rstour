<?php

    class classCity extends classSecurity {

        static $ItemID = "ccode";
        static $Sec_Code, $SectionPath;

        public $ItemId, $Limit;

        var $Properties = array(
            "public" => array(
                "Names" => array(
                    "Scroll",
                    "Section",
                ),
                "Types" => array(
                    "InputScroll",
                    "InputSectionsTree",
                ),
                "Labels" => array(
                    "Показывать по",
                    "Основной раздел",
                )
            ),
            "private" => array(
                "Names" => array(
                    "Limit",
                    "template_items_top",
                    "template_items",
                    "template_items_bottom",
                    "Separator",
                    "template_one",
                    "template_empty",
                ),
                "Types" => array(
                    "T5",
                    "InputTemplates",
                    "InputTemplates",
                    "InputTemplates",
                    "Separator",
                    "InputTemplates",
                    "InputTemplates",
                ),
                "Labels" => array(
                    "Ограничение вывода",
                    "Верх списка",
                    "Список городов",
                    "Низ списка",
                    "",
                    "Один город",
                    "Ни чего не найдено",
                )
            )
        );

        var $DefaultTemplates = array(
            "template_items_top" => array(0 => ""),
            "template_items" => array(0 => ""),
            "template_items_bottom" => array(0 => ""),
            "template_one" => array(0 => ""),
            "template_empty" => array(0 => ""),
        );

        function GetClassName() {
            return __CLASS__;
        }

        function Action($Do = "") {

            $this->Data['Errors'] = array();

            switch($Do) {
                default:
                    if($this->ItemId)
                        $this->ShowItem();
                    else
                        $this->ShowItems($this->Limit);
                    break;
            }

            if(debug()) {
                ?><pre><? print_r($this->Data); ?></pre><?
            }

        }


        public function ShowItem() {
            $Items = $this->GetItems();
            $Item = $Items[$this->ItemId];

            if($Item['dC_Code']) {
                $this->Data = array_merge($this->Data, $this->GetItemVars($Item, false));
                $this->Ins2Php("template_one");

                $this->setBuffered("page_title", $this->Data['dC_metaTitle']);
                $this->setBuffered("page_meta_desc", $this->Data['dC_metaDesc']);
            } else {
                array_push($this->Data["Errors"], array("Error" => "Город не найден"));
                $this->Data['Error'] = 1;
                $this->Ins2Php("template_empty");
            }

        }


        public function ShowItems($Limit = 0) {
            $Items = $this->GetItems();
            $this->Data['ItemsCount'] = count($Items);

            $this->Ins2Php("template_items_top");

            if($this->Data['ItemsCount']) {

                $OrderCond = array();
                if($_REQUEST['order']) {
                    foreach($_REQUEST['order'] as $Field => $Direction) {
                        $OrderCond[] = $Field;
                        $OrderCond[] = $Direction;
                    }
                }
                $OrderCond = array_merge( $OrderCond, array("dC_Name", "ASC", "dC_Code", "DESC") );
                $Items = SortArray($Items, $OrderCond);

                if($this->Scroll)
                    $this->Scroll->Data['Quantity'] = $this->Data['ItemsCount'];

                if($Limit) {
                    $Items = array_slice($Items, 0, $Limit, true);
                } elseif( $Limit && $this->Scroll && ($this->Scroll->Data['Part'] > $this->Scroll->GetProperty("Groups")) ) {
                    $Items = array_slice($Items, ( $this->Scroll->Data['First'] - ( $this->Scroll->Data['Limit'] * ($this->Scroll->Data['Part'] - 1) ) ), $this->Scroll->Data['Limit'], true);
                } elseif($Limit) {
                    $Items = array_slice($Items, $this->Scroll->Data['First'], $this->Scroll->Data['Limit'], true);
                }

                $Data = $this->Data;
                foreach($Items as $dC_Code => $Item) {
                    $Item = $this->GetItemVars($Item);
                    $this->Data = array_merge($Data, $Item);
                    $this->Ins2Php("template_items");
                }
                $this->Data = $Data;

            } else {
                $this->Data['Empty'] = 1;
                $this->Ins2Php("template_empty");
            }

            $this->Ins2Php("template_items_bottom");

        }


        public function GetItems($Data = array()) {
            $Data = $Data ?: $_REQUEST;

            $Cond['all'] = "true";
            $Conds = join(" AND ", $Cond);

            if($this->ItemId) {
                $Cities = DB::selectArray("SELECT * FROM dataCities WHERE dC_Code = ".$this->ItemId, "dC_Code");
            } else {
                $Cities = DBMem::Get($this->GetClassName());
                if($Cities)
                    return $Cities;

                $Cities = DB::selectArray("SELECT dc.* FROM dataCities AS dc WHERE $Conds", "dC_Code");

//                $classTrip = _autoload("classTrip");
//                $classTrip->DataType = 1;
                foreach($Cities as $dC_Code => $City) {
//                    $Items = $classTrip->GetItems(["dC_Code" => $dC_Code]);

                    $countItems = DB::selectValue("SELECT COUNT(ti.T_Code) FROM tripCities AS tc LEFT JOIN tripItems AS ti ON ti.T_Code = tc.T_Code AND ti.T_Visible = 1 WHERE tc.dC_Code = $dC_Code");

                    if(!$countItems)
                        unset($Cities[$dC_Code]);
                }

                DBMem::Set($Cities, $this->GetClassName(), 10000);
            }

            return $Cities;
        }


        public function GetItemVars($Item, $Part = true) {
            $CacheId = $Item['dC_Code'].".cityvars".($Part ? ".part" : "");
            $CacheFile = CACHE_PATH."/".$CacheId.$this->LngPrefix;

            if(file_exists($CacheFile) && filesize($CacheFile) && !debug()) {
                $Item = array_merge($Item, unserialize(file_get_contents($CacheFile)));
            } else {

                $Item['dC_Link'] = self::$SectionPath."?".self::$ItemID."=".$Item['dC_Code'];

                $Router = sefRouter::Get("Cities", $Item['dC_Code']);
                $Item['Tours_Link'] = $Router ? $Router['Url'].".html" : "/tours/?dC_Code=".$Item['dC_Code'];

                $Item['Regions'] = DB::selectArray("SELECT dr.dR_Code, dr.dR_Name FROM dataCities AS dc INNER JOIN dataRegions AS dr ON dr.dR_Code = dc.dR_Code WHERE dc.dC_Code = ".$Item['dC_Code'], "dR_Code");
                $Item['Regions'] = sortArray($Item['Regions'], array("dR_Code", "ASC"));
                $Item['Images'] = DB::selectArray("SELECT dCI_Image FROM dataCitiesImages WHERE dC_Code = ".$Item['dC_Code'], "");

                if(!$Part) {
                }

                $this->SaveData($CacheFile, $Item);
            }

            return $Item;
        }


        public function SaveData($CacheFile, $Data = array()) {
            $ft = fopen($CacheFile, "w");
            fputs($ft, serialize($Data));
            fclose($ft);
        }


        public function classCity($sec = "", $Parent = "") {
            parent::classSecurity($sec, $Parent);

            $this->Scroll = $this->GetBlock("Scroll");
            $this->Limit = $this->GetProperty("Limit");

            $this->ItemId = $this->Data['ItemId'] = (int)$_REQUEST[self::$ItemID];

            self::$Sec_Code = $this->Data['Sec_Code'] = $this->GetProperty("Section") ?: $this->GetSectionID();
            self::$SectionPath = $this->Data['SectionPath'] = $this->GetPath(self::$Sec_Code);

        }

    }

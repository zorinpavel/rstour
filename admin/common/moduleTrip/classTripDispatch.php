<?php
    
    /**
     * Date: 23.02.17
     * Time: 11:02
     * Project: rstour.ru
     */
    class classTripDispatch extends classTripOrders {
    
        var $Properties = array(
            "public" => array(
                "Names" => array(
                ),
                "Types" => array(
                ),
                "Labels" => array(
                )
            ),
            "private" => array(
                "Names" => array(
                    "template_top",
                    "template_items",
                    "template_bottom",
                    "Separator",
                    "template_empty",
                ),
                "Types" => array(
                    "InputTemplates",
                    "InputTemplates",
                    "InputTemplates",
                    "",
                    "InputTemplates",
                ),
                "Labels" => array(
                    "Верх",
                    "Список заказов",
                    "Низ",
                    "",
                    "Ни чего не найдено",
                )
            )
        );
    
        var $DefaultTemplates = array(
            "template_top" => array(0 => ""),
            "template_items" => array(0 => ""),
            "template_bottom" => array(0 => ""),
            "template_empty" => array(0 => ""),
        );

        function GetClassName() {
            return __CLASS__;
        }
    
        function Action($Do = "") {
            $Do = $Do ? $Do : $_REQUEST[self::$DoID];
        
            $this->Data['Errors'] = array();
            $this->Data['Messages'] = array();
        
            if($this->CanRead) {
            
                switch($Do) {
                    case "show":
                    default:
                        $this->ShowTable();
                        break;
                }
            } else {
                array_push($this->Data["Errors"], array("Error" => "Вы не можете просматривать заказы"));
                $this->Data['Error'] = 1;
                $this->Ins2Php("template_forbidden");
            }
        
            if(debug()) {
                ?><pre><? print_r($this->Data); ?></pre><?
            }
        }
    
    
        public function ShowTable($Limit = 100) {
            $Orders = $this->GetOrders(array("Ord_Status" => array(3,5)));
            $this->Data['OrdersCount'] = count($Orders);
            
            $this->Ins2Php("template_top");
        
            if($this->Data['OrdersCount']) {
            
                $OrderCond = array();
                if($_REQUEST['order']) {
                    foreach($_REQUEST['order'] as $Field => $Direction) {
                        $OrderCond[] = $Field;
                        $OrderCond[] = $Direction;
                    }
                }
                $OrderCond = array_merge( $OrderCond, array("T_Date", "DESC", "Ord_Status", "ASC", "Ord_Change", "DESC", "Ord_Code", "DESC") );
                $Orders = SortArray($Orders, $OrderCond);
            
                if($Limit)
                    $Orders = array_slice($Orders, 0, $Limit, true);
    
                $Dates = array();
                foreach($Orders as $Key => $Order) {
                    $Dates[$Order['T_Date']]['Date'] = strtotime($Order['T_Date']);
                    $Dates[$Order['T_Date']]['DateStr'] = date("j", strtotime($Order['T_Date']))." ".self::$RusMonthsInt[date("n", strtotime($Order['T_Date']))]." ".self::$RusDayWeek[date("N", strtotime($Order['T_Date']))];
                    $Order = $this->GetOrderVars($Order);

                    if(!$Dates[$Order['T_Date']]['Trips'][$Order['T_Code']]) {
                        // если гид, место или время поменялись, то надо это отображать
                        foreach($Order['DatesData'] as $T_Code => $Item) {
                            $TripData = DB::selectArray("SELECT td.*, DATE_FORMAT(td.T_CollectionTime, '%H:%i') AS T_CollectionTime, DATE_FORMAT(td.T_DepartureTime, '%H:%i') AS T_DepartureTime, dg.* FROM tripDates AS td LEFT JOIN dataGuides AS dg ON dg.dG_Code = td.T_Guide WHERE td.TD_Code = ".$Item['TD_Code'], "TD_Code");
                        }
                        $Dates[$Order['T_Date']]['Trips'][$Order['T_Code']] = $TripData[$Item['TD_Code']];
                        $Dates[$Order['T_Date']]['Trips'][$Order['T_Code']]['T_Name'] = $Order['ItemsData'][$Order['T_Code']]['T_Name'];
                    }
                    
                    foreach($Order['Travelers'] as $tKey => $Traveler)
                        $Dates[$Order['T_Date']]['Trips'][$Order['T_Code']]['Travelers'][] = $Traveler;
                }
                
                $Dates = sortArray($Dates, array("Date", "DESC"));
    
                $Data = $this->Data;
                foreach($Dates as $Date_Code => $Date) {
                    $this->Data = array_merge($Data, $Date);
                    
                    $this->Ins2Php("template_items");
                }
                $this->Data = $Data;
            
            } else {
                $this->Data['Empty'] = 1;
                $this->Ins2Php("template_empty");
            }
        
            $this->Ins2Php("template_bottom");
        
        }
    
        
        public function classTripDispatch($sec = "", $Parent = "") {
            parent::classTripOrders($sec, $Parent);
            
        }

    }
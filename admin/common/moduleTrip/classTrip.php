<?php

    class classTrip extends classSecurity {

        static $DoID = "tdo";
        static $ItemID = "tcode";
        static $Sec_Code, $SectionPath;

        public $ItemId, $Limit, $Scroll, $DataType;

        var $Properties = array(
            "public" => array(
                "Names" => array(
                    "Scroll",
                    "Section",
                ),
                "Types" => array(
                    "InputScroll",
                    "InputSectionsTree",
                ),
                "Labels" => array(
                    "Показывать по",
                    "Основной раздел",
                )
            ),
            "private" => array(
                "Names" => array(
                    "DataType",
                    "Limit",
                    "template_items_top",
                    "template_items",
                    "template_items_bottom",
                    "Separator",
                    "template_one",
                    "template_empty",
                ),
                "Types" => array(
                    "DataTypeSelect",
                    "T5",
                    "InputTemplates",
                    "InputTemplates",
                    "InputTemplates",
                    "Separator",
                    "InputTemplates",
                    "InputTemplates",
                ),
                "Labels" => array(
                    "Тип данных",
                    "Ограничение вывода",
                    "Верх списка",
                    "Список туров",
                    "Низ списка",
                    "",
                    "Один тур",
                    "Ни чего не найдено",
                )
            )
        );

        var $DefaultTemplates = array(
            "template_items_top" => array(0 => ""),
            "template_items" => array(0 => ""),
            "template_items_bottom" => array(0 => ""),
            "template_one" => array(0 => ""),
            "template_empty" => array(0 => ""),
        );

        function GetClassName() {
            return __CLASS__;
        }

        function Action($Do = "") {
            $Do = $Do ?: $_REQUEST[self::$DoID];

            $this->Data['Errors'] = array();

            switch($Do) {
                default:
                    if($this->ItemId)
                        $this->ShowItem();
                    else
                        $this->ShowItems($this->Limit);
                    break;
            }

            if(debug()) {
                ?><pre><? print_r($this->Data); ?></pre><?
            }

        }


        public function ShowItem() {
            $Items = $this->GetItems();
            $Item = $Items[$this->ItemId];

            if($Item['T_Code']) {
                $Item = $this->GetItemVars($Item, false);
                foreach($Item['Dates'] as $Key => $Date) {
                    if(date("Y-m-d") >= date("Y-m-d", strtotime($Date['T_Date'])))
                        unset($Item['Dates'][$Key]);
                }

                $this->Data = array_merge($this->Data, $Item);
                $this->Ins2Php("template_one");

                if($Item['T_Name'])
                    $this->setBuffered("page_name", $Item['T_Name']);
//                if($Item['T_Title'])
//                    $this->setBuffered("page_title", $Item['T_Title']);
                    $this->setBuffered("page_title", "Тур \"".$Item['T_Name']."\" - ".$Item['T_Days']." ".wordForm($Item['T_Days'], array("день", "дня", "дней"))." - ".$Item['T_Price']." руб. | Туроператор \"Ростиславль\"");
//                if($Item['T_MetaDesc'])
//                    $this->setBuffered("page_meta_desc", $Item['T_MetaDesc']);
                    $this->setBuffered("page_meta_desc", "Оставьте заявку на тур - ".$Item['T_Name']." и получите скидку в 5% при первом бронировании через сайт. Мы свяжемся с вами и расскажем о туре подробнее.");

            } else {
                array_push($this->Data["Errors"], array("Error" => "Тур не найден"));
                $this->Data['Error'] = 1;
                $this->Ins2Php("template_empty");
            }

        }


        public function ShowItems($Limit = 0) {
            $Items = $this->GetItems();
            $this->Data['ItemsCount'] = count($Items);
            $this->Data['Limit'] = $Limit;

            $this->Ins2Php("template_items_top");

            if($this->Data['ItemsCount']) {

                $dateIndex = $_REQUEST['sDate'] ?: date("Y-m-d");
                
                foreach($Items as $T_Code => $Item) {
                    $Item = $this->GetItemVars($Item);
                    
                    $dateSort = true;
                    if(!count($Item['Dates']))
                        $Item['_dateindex'] = -100;
                    foreach($Item['Dates'] as $Key => $Date) {
                        if(date("Y-m-d") > date("Y-m-d", strtotime($Date['T_Date'])))
                            unset($Item['Dates'][$Key]);
                        elseif($dateSort) {
                            $interval = date_diff(date_create($Date['T_Date']), date_create($dateIndex));
                            $Item['_dateindex'] = $Item['_dateindex'] - $interval->format("%a%");
                            $dateSort = false;
                        }
                    }
                    $Items[$T_Code] = $Item;
                }

                $OrderCond = array();
                if($_REQUEST['order']) {
                    foreach($_REQUEST['order'] as $Field => $Direction) {
                        $OrderCond[] = $Field;
                        $OrderCond[] = $Direction;
                    }
                }
                
                if($_REQUEST['sDate'] || $_REQUEST['fDate'] || $_REQUEST['oDate'])
                    $OrderCond = array_merge( $OrderCond, array("_relevant", "DESC", "_dateindex", "DESC", "T_Order", "DESC", "T_Price", "ASC", "T_Code", "DESC") );
                else
                    $OrderCond = array_merge( $OrderCond, array("_relevant", "DESC", "T_Order", "DESC", "_dateindex", "DESC", "T_Price", "ASC", "T_Code", "DESC") );
                $Items = sortArray($Items, $OrderCond);

                if($this->Scroll)
                    $this->Scroll->Data['Quantity'] = $this->Data['ItemsCount'];

                if($Limit) {
                    $Items = array_slice($Items, 0, $Limit, true);
                } elseif( $this->Scroll && ($this->Scroll->Data['Part'] > $this->Scroll->GetProperty("Groups")) ) {
                    $Items = array_slice($Items, ( $this->Scroll->Data['First'] - ( $this->Scroll->Data['Limit'] * ($this->Scroll->Data['Part'] - 1) ) ), $this->Scroll->Data['Limit'], true);
                } elseif($this->Scroll) {
                    $Items = array_slice($Items, $this->Scroll->Data['First'], $this->Scroll->Data['Limit'], true);
                }

                $Data = $this->Data;
                foreach($Items as $T_Code => $Item) {
                    $this->Data = array_merge($Data, $Item);
                    $this->Ins2Php("template_items");
                }
                $this->Data = $Data;

            } else {
                $this->Data['Empty'] = 1;
                $this->Ins2Php("template_empty");
            }

            $this->Ins2Php("template_items_bottom");

        }


        public function GetItems($Data = array(), $All = false) {
            $Data = $Data ?: $_REQUEST;

            $DescCond = "";
            $TitleCond = "";
            if($Data['sWord']) {
                $Data['sWord'] = dropWords($Data['sWord']);
                $Words = preg_split("/\s+/", $Data['sWord'], 0, PREG_SPLIT_NO_EMPTY);
                foreach($Words as $k => $w) {
                    $OriginalWords[] = $w;
                    $sw = cutWord($w);
                    if(!strlen($sw))
                        unset($Words[$k]);
                    else {
                        $Words[$k] = $sw;
                        $DescCond .= "LOWER(T_Desc) LIKE LOWER('%$sw%') AND ";
                        $TitleCond .= "LOWER(T_Name) LIKE LOWER('%$sw%') AND ";
                    }
                }

                $TitleCond = substr($TitleCond, 0, -4);
                $DescCond = substr($DescCond, 0, -4);
                $Cond['word'] = "(($TitleCond) OR ($DescCond))";
    
                $coeff_title = round(40/count($Words));
                $coeff_desc = round(20/count($Words));
                $Relevant = "IF (LOWER(T_Name) LIKE LOWER('%".join(" ", $OriginalWords)."%'), 200, 0) + IF (LOWER(T_Desc) LIKE LOWER('%".join(" ", $OriginalWords)."%'), 50, 0)";
                foreach($Words as $Word) {
                    $Relevant .= " + IF (LOWER(T_Name) LIKE LOWER('%".$Word."%'), ".$coeff_title.", 0)";
                    $Relevant .= " + IF (LOWER(T_Desc) LIKE LOWER('%".$Word."%'), ".$coeff_desc.", 0)";
                }
                $Relevant = ", ($Relevant) AS _relevant";

            }

            if($Data['T_Price'])
                $DateCond['tPrice'] = "T_Price <= '".$Data['T_Price']."'";

            if($Data['sBarcode'])
                $Cond['tBarcode'] = "T_Barcode LIKE '".$Data['sBarcode']."%'";

            if($Data['sExternal']) {
                if($Data['sExternal'] == "NULL")
                    $Cond['tExternal'] = "T_External IS NULL";
                else
                    $Cond['tExternal'] = "T_External LIKE '".$Data['sExternal']."%'";
            }

            if(!$All)
                $Cond['visible'] = "T_Visible = 1";

            $Cond['all'] = "true";

            $Conds = join(" AND ", $Cond);

            if($this->ItemId) {
                $Items = DB::selectArray("SELECT * FROM tripItems WHERE T_Code = ".$this->ItemId." AND ".$Conds, "T_Code");
            } else {
                $Items = DB::selectArray("SELECT * $Relevant FROM tripItems WHERE $Conds", "T_Code");

                if($this->DataType == 1)
                    $DateCond['sDate'] = "td.T_Date >= NOW() + INTERVAL 0 DAY";
                else
                    $DateCond['sDate'] = "td.T_Date >= NOW() - INTERVAL 3 MONTH";

                if($Data['sDate'])
                    $DateCond['sDate'] = "td.T_Date >= '".dateDecode($Data['sDate'])."'";
                if($Data['fDate'])
                    $DateCond['fDate'] = "td.T_Date <= '".dateDecode($Data['fDate'])."'";
                if($Data['oDate'])
                    $DateCond['oDate'] = "td.T_Date = '".dateDecode($Data['oDate'])."'";
    
                if($Data['Favorites']) {
                    $indexFavorites = DB::selectArray("SELECT ti.*, tf.T_Code FROM tripFavorites AS tf LEFT JOIN tripItems AS ti ON ti.T_Code = tf.T_Code WHERE tf.us_id = ".classUsers::$User['us_id'], "T_Code");
                    if(is_array($indexFavorites) && is_array($Items))
                        $Items = array_intersect_assoc($Items, $indexFavorites);
                    elseif(is_array($indexFavorites))
                        $Items = $indexFavorites;
                }

                if(!$Data['AllDates']) {
                    $indexDates = DB::selectArray("SELECT ti.*, td.TD_Code FROM tripDates AS td LEFT JOIN tripItems AS ti ON ti.T_Code = td.T_Code WHERE ".join(" AND ", $DateCond), "T_Code");
                    foreach($Items as $T_Code => $Item) {
                        if($Item['T_OnRequest'])
                            $indexDates[$T_Code] = $Item;
                    }
                    if(is_array($indexDates) && is_array($Items))
                        $Items = array_intersect_assoc($Items, $indexDates);
                    elseif(is_array($indexDates))
                        $Items = $indexDates;
                }

                if((int)$Data['dR_Code']) {
                    $indexRegions = DB::selectArray("SELECT ti.*, tr.dR_Code, 10 AS _relevant FROM tripRegions AS tr LEFT JOIN tripItems AS ti ON ti.T_Code = tr.T_Code WHERE tr.dR_Code = ".(int)$Data['dR_Code'], "T_Code");
                    if(is_array($indexRegions) && is_array($Items))
                        $Items = array_intersect_assoc($Items, $indexRegions);
                    elseif(is_array($indexRegions))
                        $Items = $indexRegions;

                    $RegionData = DB::selectArray("SELECT dR_seoTitle, dR_seoDesc, dR_seoHeader, dR_seoText FROM dataRegions WHERE dR_Code = ".(int)$Data['dR_Code']);
                    if($RegionData['dR_seoTitle'])
                        $this->setBuffered("page_title", $RegionData['dR_seoTitle']);
                    if($RegionData['dR_seoDesc'])
                        $this->setBuffered("page_meta_desc", $RegionData['dR_seoDesc']);
                    if($RegionData['dR_seoHeader'])
                        $this->setBuffered("page_signature", $RegionData['dR_seoHeader']);

                    $this->Data['seoText'] = $RegionData['dC_seoText'];
                }

                if((int)$Data['dC_Code']) {
                    $indexCities = DB::selectArray("SELECT ti.*, tc.dC_Code, 10 AS _relevant FROM tripCities AS tc LEFT JOIN tripItems AS ti ON ti.T_Code = tc.T_Code WHERE tc.dC_Code = ".(int)$Data['dC_Code'], "T_Code");
                    if(is_array($indexCities) && is_array($Items))
                        $Items = array_intersect_assoc($Items, $indexCities);
                    elseif(is_array($indexCities))
                        $Items = $indexCities;

                    $CityData = DB::selectArray("SELECT dC_seoTitle, dC_seoDesc, dC_seoHeader, dC_seoText FROM dataCities WHERE dC_Code = ".(int)$Data['dC_Code']);
                    if($CityData['dC_seoTitle'])
                        $this->setBuffered("page_title", $CityData['dC_seoTitle']);
                    if($CityData['dC_seoDesc'])
                        $this->setBuffered("page_meta_desc", $CityData['dC_seoDesc']);
                    if($CityData['dC_seoHeader'])
                        $this->setBuffered("page_signature", $CityData['dC_seoHeader']);

                    $this->Data['seoText'] = $CityData['dC_seoText'];
                }

                if((int)$Data['dT_Code']) {
                    $indexTypes = DB::selectArray("SELECT ti.*, tt.dT_Code, 10 AS _relevant FROM tripTypes AS tt LEFT JOIN tripItems AS ti ON ti.T_Code = tt.T_Code WHERE tt.dT_Code = ".(int)$Data['dT_Code'], "T_Code");
                    if(is_array($indexTypes) && is_array($Items))
                        $Items = array_intersect_assoc($Items, $indexTypes);
                    elseif(is_array($indexTypes))
                        $Items = $indexTypes;

                    $TypeData = DB::selectArray("SELECT dT_seoHeader, dT_seoTitle, dT_seoDesc, dT_seoText, dT_topText FROM dataTypes WHERE dT_Code = ".(int)$Data['dT_Code']);
                    if($TypeData['dT_seoTitle'])
                        $this->setBuffered("page_title", $TypeData['dT_seoTitle']);
                    if($TypeData['dT_seoDesc'])
                        $this->setBuffered("page_meta_desc", $TypeData['dT_seoDesc']);
                    if($TypeData['dT_seoHeader'])
                        $this->setBuffered("page_signature", $TypeData['dT_seoHeader']);

                    $this->Data['seoText'] = $TypeData['dT_seoText'];
                    $this->Data['topText'] = $TypeData['dT_topText'];
                }

//                if($Data['sWord']) {
//                    $DaysCond = "";
//                    $Relevant = "";
//                    foreach($Words as $Word) {
//                        $DaysCond .= "LOWER(td.D_Desc) LIKE LOWER('%$Word%') AND ";
//                        $Relevant .= " + IF (LOWER(td.D_Desc) LIKE LOWER('%".$Word."%'), ".$coeff_desc.", 0)";
//                    }
//                    $DaysCond = substr($DaysCond, 0, -4);
//                    $Relevant = ", (".substr($Relevant, 3).") AS _relevant";
//
//                    $indexDays = DB::selectArray("SELECT ti.*, td.TD_Code $Relevant FROM tripDays AS td LEFT JOIN tripItems AS ti ON ti.T_Code = td.T_Code WHERE $DaysCond", "T_Code");
//                    if(is_array($indexDays) && is_array($Items)) {
//                        $tmpItems = array_merge($Items, $indexDays);
//                        foreach($tmpItems as $Key => $Item)
//                            $Items[$Item['T_Code']] = $Item;
//                        if(debug()) {
//                            foreach($Items as $Key => $Value) {
//                                echo $Value['T_Code']." ";
//                                echo $Value['T_Name']." ";
//                                echo $Value['_relevant']."<br>";
//                            }
//                        }
//                        if(debug()) {
//                            foreach($tmpItems as $Key => $Value) {
//                                echo $Value['T_Code']." ";
//                                echo $Value['T_Name']." ";
//                                echo $Value['_relevant']."<br>";
//                            }
//                        }
//                    } elseif(is_array($indexDays))
//                        $Items = $indexDays;
//                }
                
            }

            if($this->DataType == 2) {
                $dateItems = array();

                foreach($Items as $T_Code => $Item) {
                    $Item = $this->GetItemVars($Item, true);

                    foreach($Item['Dates'] as $Key => $Date) {
                        $dateItems[$Date['T_Date']][$T_Code] = $Item;
                    }
                }

                ksort($dateItems);
                return $dateItems;
            }

            return $Items;
        }


        public function GetItemVars($Item, $Part = true, $Save = true) {
            $CacheId = $Item['T_Code'].".tourvars".($Part ? ".part" : "");
            $CacheFile = CACHE_PATH."/".$CacheId.$this->LngPrefix;

            if(file_exists($CacheFile) && filesize($CacheFile) && !debug()) {
                $Item = array_merge($Item, json_decode(file_get_contents($CacheFile), true));
            } else {

//                $Item['T_Price'] = number_format($Item['T_Price'], 0, "", " ");
                $Item['Ite_Link'] = self::$SectionPath."?".self::$ItemID."=".$Item['T_Code'];

                $Router = sefRouter::Get("classTrip", $Item['T_Code']);
                if($Router)
                    $Item['Ite_Link'] = $Router['Url'].".html";

                $Item['Types'] = DB::selectArray("SELECT dt.dT_Code, dt.dT_Name FROM tripTypes AS tt INNER JOIN dataTypes AS dt ON dt.dT_Code = tt.dT_Code WHERE tt.T_Code = ".$Item['T_Code'], "dT_Code");
                $Item['Regions'] = DB::selectArray("SELECT tr.TR_Code, dr.dR_Code, dr.dR_Name FROM tripRegions AS tr INNER JOIN dataRegions AS dr ON dr.dR_Code = tr.dR_Code WHERE tr.T_Code = ".$Item['T_Code'], "dR_Code");
                $Item['Regions'] = sortArray($Item['Regions'], array("TR_Code", "ASC"));
                $Item['Cities'] = DB::selectArray("SELECT tc.TC_Code, dc.dC_Code, dc.dR_Code, dc.dC_Name FROM tripCities AS tc INNER JOIN dataCities AS dc ON dc.dC_Code = tc.dC_Code WHERE tc.T_Code = ".$Item['T_Code'], "dC_Code");
                $Item['Cities'] = sortArray($Item['Cities'], array("TC_Code", "ASC"));
                $Item['Images'] = DB::selectArray("SELECT T_Image FROM tripImages WHERE T_Code = ".$Item['T_Code'], "TI_Code");
                $Item['T_Places'] = DB::selectArray("SELECT T_Place FROM tripPlaces WHERE T_Code = ".$Item['T_Code'], "TP_Code");

                if(!$Part) {
                    $Item['Hotels'] = DB::selectArray("SELECT dh.dH_Code, dh.dH_Name, dh.dH_Desc FROM tripHotels AS th INNER JOIN dataHotels AS dh ON dh.dH_Code = th.dH_Code WHERE th.T_Code = ".$Item['T_Code'], "dH_Code");
                    foreach($Item['Hotels'] as $dH_Code => $Hotel)
                        $Item['Hotels'][$dH_Code]['Images'] = DB::selectArray("SELECT dHI_Code, dHI_Image FROM dataHotelsImages WHERE dH_Code = $dH_Code", "dHI_Code");

                    $Item['Days'] = DB::selectArray("SELECT D_Code, D_Desc FROM tripDays WHERE T_Code = ".$Item['T_Code']." ORDER BY D_Code ASC", "D_Code");
                    $Item['Included'] = DB::selectArray("SELECT di.dI_Code, di.dI_Name, di.dI_Desc FROM tripIncluded AS ti INNER JOIN dataIncluded AS di ON di.dI_Code = ti.dI_Code WHERE ti.T_Code = ".$Item['T_Code'], "dI_Code");
                    $Item['Extras'] = DB::selectArray("SELECT di.dI_Code, di.dI_Name, di.dI_Desc, di.dI_Price, IF (ti.TI_Price IS NOT NULL, ti.TI_Price, di.dI_Price) AS dE_Price FROM tripExtras AS ti INNER JOIN dataIncluded AS di ON di.dI_Code = ti.dI_Code WHERE ti.T_Code = ".$Item['T_Code'], "dI_Code");
                }

                if($Save)
                    $this->SaveData($CacheFile, $Item);
            }

            /* не кешировать даты и лимиты */
            $Item['Dates'] = DB::selectArray("SELECT * FROM tripDates WHERE T_Code = ".$Item['T_Code']." ORDER BY T_Date", "TD_Code");
            foreach($Item['Dates'] as $Key => $Date) {
                $Item['Dates'][$Key]['T_DateStr'] = date("j", strtotime($Date['T_Date']))." ".self::$RusMonthsInt[date("n", strtotime($Date['T_Date']))];
                $Item['Dates'][$Key]['T_OpenPlaces'] = $Date['T_Limit'] ? ($Date['T_Limit'] - $Date['T_Paid']) : ($Date['T_Places'] - $Date['T_Paid']);
                $Item['Dates'][$Key]['T_BookPlaces'] = $Date['T_Limit'] ? ($Date['T_Limit'] - $Date['T_Booked']) : ($Date['T_Places'] - $Date['T_Booked']);
            }

            if(classUsers::$User['us_id'])
                $Item['Favorite'] = DB::selectValue("SELECT COUNT(T_Code) FROM tripFavorites WHERE T_Code = ".$Item['T_Code']." AND us_id = ".classUsers::$User['us_id']);

            return $Item;
        }


        public function SaveData($CacheFile, $Data = array()) {
            $ft = fopen($CacheFile, "w");
            fputs($ft, json_encode($Data));
            fclose($ft);
        }


        public function favoriteAdd() {
            if($this->ItemId && $this->User['us_id']) {
                $Favorite = DB::selectValue("SELECT COUNT(T_Code) FROM tripFavorites WHERE T_Code = ".$this->ItemId." AND us_id = ".$this->User['us_id']);

                echo $Favorite;

                if($Favorite > 0) {
                    DB::Query("DELETE FROM tripFavorites WHERE T_Code = ".$this->ItemId." AND us_id = ".$this->User['us_id']);
                } else {
                    DB::Query("INSERT INTO tripFavorites SET T_Code = ".$this->ItemId.", us_id = ".$this->User['us_id']);
                }
            }
        }


        public function classTrip($sec = "", $Parent = "") {
            parent::classSecurity($sec, $Parent);
            
            $this->Scroll = IsRobots() ? false : $this->GetBlock("Scroll");
            $this->DataType = $this->GetProperty("DataType");
            $this->Limit = $this->GetProperty("Limit");

            $this->ItemId = $this->Data['ItemId'] = (int)$_REQUEST[self::$ItemID];

            self::$Sec_Code = $this->Data['Sec_Code'] = $this->GetProperty("Section") ?: $this->GetSectionID();
            self::$SectionPath = $this->Data['SectionPath'] = $this->GetPath(self::$Sec_Code);

        }

    }

<?php

    class classUserEvents extends classProperties {

        public static function Events($Action = "SYSTEM", $Array = array()) {
            $events = "$Action:\n";
            if($_SERVER['PHP_AUTH_USER']) {
                $events .= "      UserName: ".$_SERVER['PHP_AUTH_USER']."\n";
            } elseif(classUsers::$User) {
                $events .= "      UserID: ".classUsers::$User['us_id']."\n";
                $events .= "      UserName: ".classUsers::$User['login']."\n";
                $events .= "      Session: ".classUsers::$User['session']."\n";
            }
            $events .= "      IP: ".self::GetIP()."\n";
            $events .= "      Time: ".date("d.m.Y H:i:s")."\n";
            if(count($Array)) {
                foreach($Array as $Key => $Value)
                    if(is_array($Value)) {
                        $events .= "      $Key:\n";
                        foreach($Value as $vKey => $vValue)
                            $events .= "            $vKey: $vValue\n";
                    } else
                        $events .= "      $Key: $Value\n";
            }
            $events .= "\n";
            return $events;
        }


        public static function GetEvents($Event) {
            $Events = array();
            $Eve = explode("\n", $Event);
            foreach($Eve as $Key => $Line) {
                list($EveKey, $EveValue) = explode(": ", $Line);
                if($EveKey)
                    $Events[$EveKey] = $EveValue;
            }
            return $Events;
        }


        public static function GetIP() {
            if (getenv("HTTP_CLIENT_IP") && strcasecmp(getenv("HTTP_CLIENT_IP"),"unknown"))
                $ip = getenv("HTTP_CLIENT_IP");

            elseif (getenv("HTTP_X_FORWARDED_FOR") && strcasecmp(getenv("HTTP_X_FORWARDED_FOR"), "unknown"))
                $ip = getenv("HTTP_X_FORWARDED_FOR");

            elseif (getenv("REMOTE_ADDR") && strcasecmp(getenv("REMOTE_ADDR"), "unknown"))
                $ip = getenv("REMOTE_ADDR");

            elseif (!empty($_SERVER['REMOTE_ADDR']) && strcasecmp($_SERVER['REMOTE_ADDR'], "unknown"))
                $ip = $_SERVER['REMOTE_ADDR'];

            else
                $ip = "unknown";

            return($ip);
        }

        function classUserEvents($sec="", $Parent="") {
            parent::classProperties($sec, $Parent);
        }

    }

  
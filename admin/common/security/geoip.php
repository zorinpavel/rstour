<?php

    /* HTML5 location */

    class classGeoIP extends classUserEvents {

        public static function GetData($IP = "") {

            $Data = array();
            $IP = $IP ? $IP : self::GetIP();

            if($IP != "127.0.0.1") {

                $Data = self::GetIpGeoBase($IP);
                if($Data['country'] == "RU" || $Data['country'] == "XX")
                    $Data = self::GetHostIp($IP);

            }
            $Data['ip'] = $IP;

            return $Data;
        }


        public static function GetHostIp($IP = "") {

            $link = "api.hostip.info/get_html.php?ip=$IP&position=true";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $link);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
            $string = curl_exec($ch);

            $data['curl'] = curl_errno($ch);

            if(!curl_errno($ch) && !strpos($string, "503")) {

                $d = explode("\n", $string);

                $data['country'] = trim(preg_replace("/Country:(.*)\((.*)\)/", "$2", $d[0]));
                $data['city'] = trim(preg_replace("/City:(.*)/", "$1", $d[1]));
                $data['lat'] = trim(preg_replace("/Latitude:(.*)/", "$1", $d[3]));
                $data['lng'] = trim(preg_replace("/Longitude:(.*)/", "$1", $d[4]));
            }
            curl_close($ch);

            return $data;

        }


        public static function GetIpGeoBase($IP = "") {

            $link = "ipgeobase.ru:7020/geo?ip=$IP";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $link);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
            $string = curl_exec($ch);

            if(!curl_errno($ch)) {

                $string = iconv("windows-1251", "utf-8", $string);

                $pa["country"] = "#<country>(.*)</country>#is";
                $pa["city"] = "#<city>(.*)</city>#is";
                $pa["region"] = "#<region>(.*)</region>#is";
                $pa["district"] = "#<district>(.*)</district>#is";
                $pa["lat"] = "#<lat>(.*)</lat>#is";
                $pa["lng"] = "#<lng>(.*)</lng>#is";

                $data = array();
                foreach($pa as $key => $pattern) {
                    preg_match($pattern, $string, $out);
                    if(isset($out[1]) && $out[1])
                        $data[$key] = trim($out[1]);
                }

//                $data['city'] = Cyr2Lat($data['city']);
            }
            curl_close($ch);

            return $data;

        }

    }


<?php


    class classSecurity extends classUserEvents {

        public static $Permissions;

        public $Can;
        public $CanRead, $CanWrite, $CanEdit, $CanDelete;
        public $Owner = false;


        public static function GetCan($Obj = "", $UserData = "") {
            $User = $UserData ?: classUsers::$User;

            if(!self::$Permissions || $UserData) { // $UserData не текущий пользователь
                if($User['us_id'] && $User['Group']) {
                    $GroCond = "Gro_Code = ".$User['Group'];
                } else {
                    $GroCond = "Gro_Code = 0";
                }

                $Can = DB::selectArray("SELECT * FROM UserSecurity WHERE $GroCond", "Sec_Obj");

                foreach($Can as $Object => $Permissions) {
                    foreach($Permissions as $Key => $Value) {
                        unset($Can[$Object]['Gro_Code']);
                        unset($Can[$Object]['Sec_Obj']);
                        if($Value)
                            $Can[$Object][str_replace("can_", "", $Key)] = $Value;
                        unset($Can[$Object][$Key]);
                    }
                }
                if(!$UserData) {
                    self::$Permissions = $Can;
                } else {
                    if($Obj)
                        return $Can[$Obj];
                    else
                        return $Can;
                }
            }

            if($Obj)
                return self::$Permissions[$Obj];
            else
                return self::$Permissions;
        }


        public static function Can($Do, $Obj = "", $UserData = "") {
            $User = $UserData ?: classUsers::$User;

            if(!self::$Permissions)
                self::GetCan($Obj, $UserData);

            if($UserData) {
                if($User['us_id'] && $User['Group']) {
                    $GroCond = " AND Gro_Code = ".$User['Group'];
                } else {
                    $GroCond = " AND Gro_Code = 0";
                }

                $Can = false;
                if($Obj && $Do)
                    list($Can) = mysql_fetch_row(db_query("SELECT can_$Do FROM UserSecurity WHERE Sec_Obj = '$Obj' $GroCond AND can_$Do = 1"));

                return $Can;
            } else {
                return self::$Permissions[$Obj][$Do];
            }
        }


        public function GetBans($Obj = "", $Value = "") {
            $Bans = array();
            $query = "SELECT ub.*, u.login FROM UserBans AS ub
                  LEFT JOIN Users AS u ON ub.us_id = u.us_id WHERE (
                   ub.Ban_Value = '".$this->User['us_id']."' OR
                   ub.Ban_Value = '".$this->User['login']."' OR
                   ub.Ban_Value = '".$this->User['session']."' OR
                   ub.Ban_Value = '".$this->GetIP()."'
               ";
            if($Value)
                $query .= "OR ub.Ban_Value LIKE '%$Value%'";

            if($Obj) {
                $query .= ") AND ub.Ban_Obj = '$Obj'";
            } else {
                $query .= ")";
            }
            $query .= " GROUP BY Ban_Reason";

            $r = db_query($query);
            while($Ban = mysql_fetch_assoc($r)) {
                array_push($Bans, "Пользователь ".$Ban['login']." наложил на Вас ограничение: \"".$Ban['Ban_Reason']."\" Срок действия до: ".date("d.m.Y H:i", $Ban['Ban_Time']));
            }
            return $Bans;
        }


        public function classSecurity($sec = "", $Parent = "") {
            parent::classUserEvents($sec, $Parent);

            $this->Users = _autoload("classUsers", "Users", $sec);
            if($this->Users) {
                if(!classUsers::$User)
                    $this->Users->GetInfo();

                $this->User = classUsers::$User;
                $this->PageUser = classUsers::$PageUser;
                $this->Can = classSecurity::GetCan($this->GetClassName());
            } else
                $this->Can = false;

            if($this->Can) {
                $this->CanRead = $this->Can['read'];
                $this->CanWrite = $this->Can['write'];
                $this->CanEdit = $this->Can['edit'] = (classSecurity::Can("edit", $this->GetClassName()) || $this->Owner);
                $this->CanDelete = $this->Can['delete'] = (classSecurity::Can("delete", $this->GetClassName()) || $this->Owner);
            }

        }

    }

<?php


    include_once(ADMIN_PATH."/setup.php");


    AdminHeaders();
    MainNavi("index");

?>

    <div class="EditData">
        <fieldset>
            <legend class="Header">Сбросить кеш</legend>
            <div style="text-align:center">
                <form method="POST">
                    <input type="hidden" name="Update" value="1">
                    <?
                        if($_REQUEST['Update']) {
                            if ($Dir = opendir(CACHE_PATH)) {
                                while (false !== ($file = readdir($Dir))) {
                                    if($file != "." && $file != "..")
                                        unlink(CACHE_PATH."/".$file);
                                }
                                closedir($Dir);
                                echo "<div class=\"message\">Все временные файлы CACHE удалены ...</div>";
                            }
                            if ($Dir = opendir(COMPILED_PATH)) {
                                while (false !== ($file = readdir($Dir))) {
                                    if($file != "." && $file != "..")
                                        unlink(COMPILED_PATH."/".$file);
                                }
                                closedir($Dir);
                                echo "<div class=\"message\">Все временные файлы COMPILED удалены ...</div>";
                            }
                            if(class_exists("Memcache")) {
                                DBMem::getInstance();
                                DBMem::$_connection->flush();
                                echo "<div class=\"message\">Весь MEMCACHE был очищен ...</div>";
                            }
                            if(class_exists("Memcached")) {
                                DBMem::getInstance();
                                DBMem::ClearAll();
                                echo "<div class=\"message\">Весь MEMCACHED был очищен ...</div>";
                            }

                        } else {

                            ?>

                            <div class="attention">Вы действительно хотите удалить все временный файлы?</div>
                            <br>
                            <input type="submit" value="Да" class="red" style="width:8em;">

                        <?
                        }
                    ?>
                </form>
            </div>
        </fieldset>
    </div>

<?

    
    AdminFooter();

<?php


    include_once(ADMIN_PATH."/setup.php");


    AdminHeaders();
    MainNavi("index");

    $tImages = DB::selectArray("SELECT T_Code, T_Image FROM tripItems WHERE T_Visible = 0", "T_Code");
    $allImages = DB::selectArray("SELECT im.* FROM tripItems AS t LEFT JOIN tripImages AS im ON im.T_Code = t.T_Code AND t.T_Visible = 0", "T_Code");

    foreach($tImages as $T_Code => $Image) {
        echo "<div id=\"ti_".$T_Code."\">".$Image['T_Image']." - ";

        $_REQUEST['file'] = $Image['T_Image'];
        require(ADMIN_PATH."/travel/file_del.php");

        echo "</div>";
    }

    foreach($allImages as $T_Code => $Image) {
        echo "<div id=\"ti_".$T_Code."\">".$Image['T_Image']." - ";

        $_REQUEST['file'] = $Image['T_Image'];
        require(ADMIN_PATH."/travel/file_del.php");

        echo "</div>";
    }

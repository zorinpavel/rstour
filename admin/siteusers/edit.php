<?php

    $usrGroup = 2;
    include_once(ADMIN_PATH."/setup.php");
    include_once("setup.php");


    AdminHeaders();
    MainNavi("siteusers");

    $us_id = DB::selectValue("SELECT us_id FROM Users WHERE us_id = ".(int)$_REQUEST['us_id']);
    if(!$us_id) {
        ?>
            <script type="text/javascript">
              window.location = 'users.php?error=nouser';
            </script>
        <?
    }

    $userFields = DB::selectArray("SELECT * FROM UserFields ORDER BY Uf_Code", "Uf_Code");

    if($_REQUEST['Update']) {
        if(count($userFields)) {
            foreach($_REQUEST['fields'] as $Name => $Value) {
                if($Value)
                    $Data[$Name] = $Value;
            }
            $dbData = json_encode($Data);
            db_query("REPLACE INTO UserData SET Data = '$dbData', us_id = $us_id");
        }
        /* только одна группа для пользователя */
        db_query("REPLACE INTO UserGroups SET Gro_Code = ".(int)$_REQUEST['Gro_Code'].", us_id = $us_id");
        db_query("REPLACE INTO UserCompanies SET C_Code = ".(int)$_REQUEST['C_Code'].", us_id = $us_id");
        db_query("UPDATE Users SET login = '".$_REQUEST['login']."', mail = '".$_REQUEST['mail']."', name = '".$_REQUEST['name']."', phone = '".$_REQUEST['phone']."' WHERE us_id = $us_id");
    }

    $Table = "UserEdit";
    include(EDIT_ONE_PATH);

?>
    <table width="100%" class="border"><tr><th colspan="2" class="border">GEO Location:</th></tr>
    <tr>
        <td class="border" colspan="2">
            <?
                $TimeLog = DB::selectArray("SELECT * FROM UserTimeLog WHERE us_id = $us_id");
                echo date("d.m.Y H:i", $TimeLog['lasttime'])."<br>";
                echo $TimeLog['ip']."<br>";
                echo $TimeLog['country']." / ".$TimeLog['city']."<br>";
            ?>
        </td>
    </tr>
    </table>
<?

    $Gro_Code = DB::selectValue("SELECT Gro_Code FROM UserGroups WHERE us_id = $us_id");
    $C_Code = DB::selectValue("SELECT C_Code FROM UserCompanies WHERE us_id = $us_id");

    echo $C_Code;

?>
    <table width="100%" class="border"><tr><th colspan="2" class="border">Параметры:</th></tr>
    <tr>
        <td class="border">Группа:</td>
        <td class="border"><? $Gro = new GroupDBSelect("Gro_Code", $Gro_Code); $Gro->dPrint(); ?></td>
    </tr>
    <tr>
        <td class="border">Компания:</td>
        <td class="border"><? $CompanySelect = new CompanyDBSelect("C_Code", $C_Code); $CompanySelect->dPrint(); ?></td>
    </tr>

<?
    $dbData = DB::selectArray("SELECT * FROM UserData WHERE us_id = $us_id");
    $Data = json_decode($dbData['Data'], true);

    if($C_Code)
        $Company = classCompany::GetCompany($C_Code);

    foreach($userFields as $Key => $Field) {
        echo "<tr>";
        echo "<td class=\"border\">".$Field['Uf_Desc']."</td>";
        echo "<td class=\"border\">";
            if($Field['Uf_Name'] == "Discount" && $Company['C_Code']) {
                $F = new T30RO("fields[".$Field['Uf_Name']."]", $Company['C_Discount']);
                $F->dPrint();
            } else {
                $F = new T30("fields[".$Field['Uf_Name']."]", $Data[$Field['Uf_Name']]);
                $F->dPrint();
            }
        echo "</td>";
        echo "</tr>";
    }
?>
    </table>
    <table width="100%" class="border"><tr><th colspan="2" class="border">BAN-ы:</th></tr>
        <tr>
            <td class="border">
                <?
                    $Bans = DB::selectArray("SELECT * FROM UserBans WHERE Ban_Value = $us_id", "Ban_Code");
                    foreach($Bans as $Key => $Ban) {
                        echo $Ban['Ban_Obj']." ".$Ban['us_id']." ".$Ban['Ban_Reason']." ".date("d.m.Y H:i", $Ban['Ban_Time'])."<br>";
                    }
                ?>
            </td>
        </tr>
    </table>

    <input type="hidden" name="Update" value="1">
    <div id="SaveIt"><input type="submit" name="Update" value="Сохранить" class="green"></div>
    </form>

    </fieldset>
    </div>

<?

    AdminFooter();
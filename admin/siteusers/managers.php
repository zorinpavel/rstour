<?php

    include_once(ADMIN_PATH."/setup.php");
    include_once("setup.php");


    AdminHeaders();
    MainNavi("siteusers");

    $C_Code = (int)$_REQUEST['C_Code'];
    $Users = DB::selectArray("SELECT u.*, g.Gro_Code, g.Gro_Name FROM UserCompanies AS uc INNER JOIN Users AS u ON u.us_id = uc.us_id LEFT JOIN UserGroups AS ug ON ug.us_id = uc.us_id LEFT JOIN Groups AS g ON ug.Gro_Code = g.Gro_Code WHERE uc.C_Code = $C_Code", "us_id");
    $CompanyName = DB::selectValue("SELECT C_Name FROM Companies WHERE C_Code = $C_Code");
?>
    <div class="EditData">
        <fieldset>
            <legend class="Header">Менеджеры <b><? echo $CompanyName; ?></b></legend>
            <table class="border">
                <?
                    foreach($Users as $us_id => $User) {
                        echo "<tr>";
                        echo "<td class=\"border\">";
                        $UserLogin = new T10RO("login", $User['login']);
                        $UserLogin->dPrint();
                        echo "</td>";
                        echo "<td class=\"border\">";
                        $UserName = new T30RO("name", $User['lastname']." ".$User['name']." ".$User['middlename']);
                        $UserName->dPrint();
                        echo "</td>";
                        echo "<td class=\"border\">";
                        $UserGroup = new T20RO("Gro_Code", $User['Gro_Name']);
                        $UserGroup->dPrint();
                        echo "</td>";
                        echo "<td class=\"border\">";
                        $UserEdit = new GoUsrEdit($User['us_id']);
                        $UserEdit->dPrint();
                        echo "</td>";
                        echo "</tr>";
                    }
                ?>
            </table>
        </fieldset>
    </div>
<?

    AdminFooter();
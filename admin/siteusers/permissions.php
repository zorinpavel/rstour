<?php

    include_once(ADMIN_PATH."/setup.php");
    include_once("setup.php");


    AdminHeaders();
    MainNavi("siteusers");

    $Sec_Obj = $_REQUEST['Sec_Obj'];

    if($Sec_Code) {
        $r = db_query("SELECT Sec_Obj FROM UserSecurity WHERE Sec_Obj = '$Sec_Obj'");
        list($Sec_Obj) = mysql_fetch_row($r);
    }

    if($Sec_Obj) {

        if($_REQUEST['Update'] == 2 && $_REQUEST['Permissions']) {
            db_query("DELETE FROM UserSecurity WHERE Sec_Obj = '$Sec_Obj'");
            foreach($_REQUEST['Permissions'] as $Gro_Code => $Value) {
                $query = "INSERT INTO UserSecurity SET Sec_Obj = '$Sec_Obj', Gro_Code = $Gro_Code";
                if($Value['can_delete'])
                    $query .= ", can_delete = ".$Value['can_delete'];
                if($Value['can_edit'])
                    $query .= ", can_edit = ".$Value['can_edit'];
                if($Value['can_write'])
                    $query .= ", can_write = ".$Value['can_write'];
                if($Value['can_read'])
                    $query .= ", can_read = ".$Value['can_read'];
                //        echo "$query<br>";
                db_query($query);
            }
        }

        $Permissions = array();

        $p = db_query("SELECT
                     G.Gro_Code, G.Gro_Name, P.Sec_Obj, P.Gro_Code as PGro_Code, P.can_delete, P.can_edit, P.can_write, P.can_read
                   FROM
                     Groups AS G
                     LEFT JOIN UserSecurity AS P ON P.Gro_Code = G.Gro_Code AND P.Sec_Obj = '$Sec_Obj'
                   ORDER BY G.Gro_Order");
        while($Perms = mysql_fetch_assoc($p)) {
            $Permissions[$Perms['Gro_Code']] = $Perms;
        }

        $p0 = db_query("SELECT P.Sec_Obj, P.Gro_Code as Gro_Code, P.Gro_Code as PGro_Code, P.can_delete, P.can_edit, P.can_write, P.can_read FROM UserSecurity AS P WHERE P.Sec_Obj = '$Sec_Obj' AND P.Gro_Code = 0");
        while($Perms = mysql_fetch_assoc($p0)) {
            $Permissions[$Perms['Gro_Code']] = $Perms;
        }


        ?>

        <div style="width:60%;margin-left:10%;">
            <fieldset><legend class="Header">Определить права: <b><?=$Sec_Obj?></b></legend>
                <form method="POST">
                    <input type="hidden" name="Update" value="2">
                    <input type="hidden" name="Sec_Obj" value="<?=$Sec_Obj?>">

                    <table class="border">
                        <tr>
                            <th class="border">Группа</th>
                            <th class="border">удалять delete</th>
                            <th class="border">редактировать edit</th>
                            <th class="border">писать write</th>
                            <th class="border">читать read</th>
                        </tr>
                        <?


                            foreach($Permissions as $Code => $Permision) {
                                if($Code != 0) {
                                    echo "<tr><td class=\"border\">".$Permision['Gro_Name'].":</td>";
                                    echo "<td class=\"border\" align=\"center\">";

                                    $Input = new InputCheckbox("Permissions[".$Permision['Gro_Code']."][can_delete]", $Permision['can_delete']);
                                    $Input->dPrint();

                                    echo "</td>";
                                    echo "<td class=\"border\" align=\"center\">";

                                    $Input = new InputCheckbox("Permissions[".$Permision['Gro_Code']."][can_edit]", $Permision['can_edit']);
                                    $Input->dPrint();

                                    echo "</td>";
                                    echo "<td class=\"border\" align=\"center\">";

                                    $Input = new InputCheckbox("Permissions[".$Permision['Gro_Code']."][can_write]", $Permision['can_write']);
                                    $Input->dPrint();

                                    echo "</td>";
                                    echo "<td class=\"border\" align=\"center\">";

                                    $Input = new InputCheckbox("Permissions[".$Permision['Gro_Code']."][can_read]", $Permision['can_read']);
                                    $Input->dPrint();

                                    echo "</td>";
                                    echo "</tr>";
                                }
                            }

                            echo "<tr><td class=\"border\">Все остальные:</td>";
                            echo "<td class=\"border\" align=\"center\">";

                            $Input = new InputCheckbox("Permissions[0][can_delete]", $Permissions[0]['can_delete']);
                            $Input->dPrint();

                            echo "</td>";
                            echo "<td class=\"border\" align=\"center\">";

                            $Input = new InputCheckbox("Permissions[0][can_edit]", $Permissions[0]['can_edit']);
                            $Input->dPrint();

                            echo "</td>";
                            echo "<td class=\"border\" align=\"center\">";

                            $Input = new InputCheckbox("Permissions[0][can_write]", $Permissions[0]['can_write']);
                            $Input->dPrint();

                            echo "</td>";
                            echo "<td class=\"border\" align=\"center\">";

                            $Input = new InputCheckbox("Permissions[0][can_read]", $Permissions[0]['can_read']);
                            $Input->dPrint();

                            echo "</td>";
                            echo "</tr>";

                        ?>
                    </table>

                    <div id="SaveIt"><input type="submit" class="green" value="Сохранить"/> <input type="button" class="red" value="Удалить все" onclick="if(confirm('Вы действительно хотите права доступа у объекта <?=$Sec_Obj?>?')) { openUrl('permissions_del.php?Sec_Obj=<?=$Sec_Obj?>', 'permissions_del', this, true, 0, 100, 'Удалить права доступа'); }"/></div>


                </form>
            </fieldset>
        </div>

        <?

    } else {

        ?>

        <div style="width:60%;margin-left:10%;">
            <fieldset><legend class="Header">Определить права: <b><?=$Sec_Obj?></b></legend>
                <form method="GET">
                    <input type="hidden" name="Update" value="1">
                    <input type="text" name="Sec_Obj" value="<?=$Sec_Obj?>">
                    <div id="SaveIt"><input type="submit" class="green" value="Сохранить"></div>
                </form>
            </fieldset>
        </div>

        <?

    }

?>

    <div style="width:60%;margin-left:10%;">
        <fieldset><legend class="Header">Текущие объекты:</b></legend>
            <?
                $r = db_query("SELECT * FROM UserSecurity GROUP BY Sec_Obj");
                while($Obj = mysql_fetch_assoc($r)) {
                    echo "<a href=\"?Update=1&Sec_Obj=".$Obj['Sec_Obj']."\">".$Obj['Sec_Obj']."</a><br>";
                }
            ?>
        </fieldset>
    </div>

<?

    AdminFooter();
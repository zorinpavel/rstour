<?php

    class GoUsrEdit extends InputButton {
        function GoUsrEdit($Code) {
            if(!Permissions::getPermissions(56))
                $this->Disabled = true;
            $this->InputButton("Go$Code", "Редактировать", "", "window.location = './edit.php?us_id=$Code';", "green");
        }
    }

    class GoUsrPass extends InputButton {
        function GoUsrPass($Code) {
            $this->InputButton("Go$Code", "Смнеить пароль", "", "window.location = './pass.php?us_id=$Code';", "blue");
        }
    }

    class GoUsrEditGroup extends InputButton {
        function GoUsrEditGroup($Code) {
            $this->Code = $Code;
            $this->InputButton("Go$Code", "Сменить группу", "", "window.location = './group_edit.php?us_id=$Code';", "blue");
        }
        function dPrint() {
            $r = db_query("SELECT Groups.Gro_Name FROM UserGroups, Groups WHERE UserGroups.Gro_Code = Groups.Gro_Code AND UserGroups.us_id = ".$this->Code);
            while($Groups = mysql_fetch_assoc($r)) {
                $i++;
                echo "<b>".$Groups['Gro_Name']."</b>";
                if($i < mysql_num_rows($r)) echo ", ";
            }
            //        echo " &nbsp; ";
            //        parent::dPrint();
        }
    }

    class GroupDBSelect extends InputDBSelect {
        function GroupDBSelect($Name, $Value) {
            $this->InputDBSelect($Name, $Value, "Groups", "Gro_Code", "Gro_Name", "", "Gro_Order", "");
            $this->Empty = 1;
        }
    }

    class InputHiddenUser extends InputHidden {
        function dPrint() {
            $result = db_query("SELECT login FROM Users WHERE us_id='".$this->Value."'");
            list($login) = mysql_fetch_row($result);
            echo " &nbsp; <b>$login</b><br>";
            parent::dPrint();
        }

        function InputHiddenUser($Name, $Value) {
            global $us_id;
            $this->InputHidden($Name, $us_id);
        }
    }

    class GoPermissions extends InputButton {
        function GoPermissions($Code) {
            $this->InputButton("Go$Code", "Права доступа", "", "window.location = 'permissions.php?Code=$Code';");
        }
    }

    class InputUserFieldType extends InputSelect {
        function InputUserFieldType($Name, $Value) {
            parent::InputSelect($Name, $Value,
                array(
                    "1"  => "Текстовое поле",
                    "2"  => "Checkbox",
                )
            );
            $this->Empty = 1;
        }
    }


    class InputDaySelect extends InputSelect {
        function InputDaySelect($Name, $Value, $onChange="") {
            for($d = 1; $d <= 31; $d++)
                $Options[$d] = $d;
            $this->InputSelect($Name, $Value, $Options, "", $onChange);
        }
    }


    class InputMonthSelect extends InputSelect {
        function InputMonthSelect($Name, $Value, $onChange="") {
            for($m = 1; $m <= 12; $m++)
                $Options[$m] = classProperties::$RusMonthsInt[$m];
            $this->InputSelect($Name, $Value, $Options, "", $onChange);
        }
    }


    class InputYearSelect extends InputSelect {
        function InputYearSelect($Name, $Value, $onChange="") {
            for($y = 1950; $y <= 2000; $y++)
                $Options[$y] = $y;
            $this->InputSelect($Name, $Value, $Options, "", $onChange);
        }
    }


    class InputCheckboxRO extends InputCheckbox {
        function InputCheckboxRO($Name, $Value) {
            if(!Permissions::getPermissions(56))
                $this->Onclick = "return false;";
            $this->InputCheckbox($Name, $Value);
        }
    }


    $Users = array(
        "TableName"      => "Users",
        "Id"             => "us_id",
        "Order"          => "status DESC, us_id DESC",
        "Header"         => "Пользователи",
        "Headers"        => array("#", "X", "Дата регистрации", "Логин", "email", "Фамилия", "Имя", "Отчество"),
        "Fields"         => array("us_id", "status", "regtime", "login", "mail", "lastname", "name", "middlename"),
        "Types"          => array("InputLabel", "InputCheckboxRO", "InputTime", "T10RO", "T20RO", "T10", "T10", "T10"),
        "DBTypes"        => array("IGNORE", "INT", "IGNORE", "CHAR", "CHAR", "CHAR", "CHAR", "CHAR"),
        "Uploads"        => array(),
        "ButtonHeaders"  => array("Группы", "&nbsp;"),
        "Buttons"        => array("GoUsrEditGroup", "GoUsrEdit"),
        "Filters"        => array(),
        "FiltersNames"   => array(),
        "FiltersTypes"   => array(),
        "FiltersCompare" => array(),
        "EditOnly"       => true,
    );

    $Groups = array(
        "TableName"      => "Groups",
        "Id"             => "Gro_Code",
        "Order"          => "Gro_Order",
        "Fields"         => array("Gro_Name", "Gro_Order"),
        "Types"          => array("T50", "T5"),
        "Uploads"        => array(),
        "Header"         => "Группы пользователей",
        "Headers"        => array("Название", "Порядок<br>вывода"),
        "Buttons"        => array(),
        "ButtonHeaders"  => array(),
        "Filters"        => array(),
        "FiltersNames"   => array(),
        "FiltersTypes"   => array(),
        "FiltersCompare" => array(),
        "EditOnly"       => 0,
    );

    $UserSecurity = array(
        "TableName"      => "UserSecurity",
        "Id"             => "Sec_Code",
        "Order"          => "Sec_Code, Sec_Obj",
        "Fields"         => array("Sec_Obj"),
        "Types"          => array("T50"),
        "Uploads"        => array(),
        "Header"         => "Список объектов",
        "Headers"        => array("", "Объект"),
        "Buttons"        => array("GoPermissions"),
        "ButtonHeaders"  => array("&nbsp;"),
        "Filters"        => array(),
        "FiltersNames"   => array(),
        "FiltersTypes"   => array(),
        "FiltersCompare" => array("N"),
        "EditOnly"       => 0,
    );

    $UserEdit = array(
        "TableName"     => "Users",
        "Id"            => "us_id",
        "IdName"        => "login",
        "Headers"       => array("Активный", "Дата регистрации", "Login", "Mail", "Фамилия", "Имя", "Отчество", "День рождения", "Месяц", "Год", "Телефон", "О себе"),
        "Fields"        => array("status", "regtime", "login", "mail", "lastname", "name", "middlename", "bth_day", "bth_month", "bth_year", "phone", "descr"),
        "Types"         => array("InputCheckboxRO", "InputTime", "T30", "T30", "T30", "T30", "T30", "InputDaySelect", "InputMonthSelect", "InputYearSelect", "T30", "TA10"),
        "Header"        => "Редактировать пользователя",
        "Buttons"       => array("GoUsrPass"),
        "ButtonHeaders" => array("&nbsp;"),
        "ViewOnly"      => 0,
        "Params"        => 1,
    );


    $Fields = array(
        "TableName"      => "UserFields",
        "Header"         => "Список полей",
        "Id"             => "Uf_Code",
        "Order"          => "Uf_Code",
        "Headers"        => array("Имя", "Тип", "Описание", "Primary"),
        "Fields"         => array("Uf_Name", "Uf_Type", "Uf_Desc", "Uf_Primary"),
        "Types"          => array("T20", "InputUserFieldType", "T20", "InputCheckbox"),
        "DBTypes"        => array("CHAR", "INT", "CHAR", "INT/NULL"),
        "Uploads"        => array(),
        "Buttons"        => array(),
        "ButtonHeaders"  => array(),
        "Filters"        => array(),
        "FiltersNames"   => array(),
        "FiltersTypes"   => array(),
        "FiltersCompare" => array("N"),
        "EditOnly"       => 0
    );


    class CompanyDBSelect extends InputDBSelect {
        function CompanyDBSelect($Name, $Value) {
            $this->InputDBSelect($Name, $Value, "Companies", "C_Code", "C_Name", "C_Active = 1", "C_Name", "");
            $this->Empty = 1;
        }
    }


    class GoManagers extends InputButton {
        function GoManagers($Code) {
            $this->InputButton("Go$Code", "Менеджеры", "", "window.location = 'managers.php?C_Code=$Code';");
        }
    }


    class GoCompanyEdit extends InputButton {
        function GoCompanyEdit($Code) {
            $this->InputButton("Go$Code", "Редактировать", "", "window.location = 'cedit.php?C_Code=$Code';", "green");
        }
    }


    $Companies = array(
        "Header"         => "Турагентства",
        "TableName"      => "Companies",
        "Id"             => "C_Code",
        "Order"          => "C_Code",
        "Headers"        => array("Название", "Скидка %", "Активно"),
        "Fields"         => array("C_Name", "C_Discount", "C_Active"),
        "Types"          => array("T30", "T5RO", "InputCheckbox"),
        "Uploads"        => array(),
        "ButtonHeaders"  => array("", ""),
        "Buttons"        => array("GoManagers", "GoCompanyEdit"),
        "Filters"        => array(),
        "FiltersNames"   => array(),
        "FiltersTypes"   => array(),
        "FiltersCompare" => array("N"),
        "EditOnly" => 0
    );


    $CompanyEdit = array(
        "Header"        => "Редактировать турагентство",
        "TableName"     => "Companies",
        "Id"            => "C_Code",
        "IdName"        => "C_Code",
        "Headers"       => array("Data", "Активно", "Скидка %", "Название", "Полное название", "Телефон", "Адрес", "Почтовый адрес", "Юридический адрес", "ФИО Директора", "НДС", "ИНН", "КПП", "Номер счета", "Банк", "БИК", "Кор. счет"),
        "Fields"        => array("C_Date", "C_Active", "C_Discount", "C_Name", "C_FullName", "C_Phone", "C_Address", "C_PostAddress", "C_UrAddress", "C_BossName", "C_NDS", "C_INN", "C_KPP", "C_Account", "C_BankName", "C_BankBik", "C_BankAccount"),
        "Types"         => array("InputDateRO", "InputCheckbox", "T5", "T30", "T30", "T20", "T50", "T50", "T50", "T30", "InputCheckbox", "T20", "T20", "T20", "T50", "T20", "T20"),
        "DBTypes"       => array("IGNORE"),
        "ButtonHeaders" => array("Менеджеры"),
        "Buttons"       => array("GoManagers"),
        "ViewOnly"      => 0,
        "Params"        => 0,
    );




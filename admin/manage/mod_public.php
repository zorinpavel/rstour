<?php
    
    include_once(ADMIN_PATH."/setup.php");
    
    
    AdminHeaders();
    MainNavi("manage");
    
    $sec = $_REQUEST['sec'];
    $mod = $_REQUEST['mod'];
    
?>

    <table width="100%" id="Frames">
        <tr>
            <td id="rtd">
                
                <?
                    
                    $ModulesList = new ModulesList();
                    $ModulesList->dPrint();
                
                ?>
            </td>
            <td id="mtd">
                <?
                    
                    $NaviSelected = 1;
                    include("mod_navi.php");
                
                ?><FIELDSET><LEGEND>Общие свойства класса</LEGEND><?
                        
                        $EditProperties = new EditProperties($mod, "public");
                        $EditProperties->dPrint();
                    
                    ?></FIELDSET><?
                
                ?>
            </td>
        </tr>
    </table>

<?
    
    AdminFooter();

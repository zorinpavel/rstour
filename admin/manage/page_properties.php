<?php

    include_once(ADMIN_PATH."/setup.php");


    AdminHeaders();
    MainNavi("manage");

    $sec = $_REQUEST['sec'];
    $mod = $_REQUEST['mod'];

?>

    <table width="100%" id="Frames">
        <tr>
            <td id="rtd">

                <?

                    $SectionsTree = new SectionsTree();
                    $SectionsTree->dPrint();

                ?>
            </td>
            <td id="mtd">
                <?

                    $NaviSelected = 1;
                    include("page_navi.php");

                    if(isset($sec)) {
                        $EditPage = new EditPage("private", $sec);

                        ?><FIELDSET><LEGEND>Свойства раздела <b>"<? echo $EditPage->Obj['Values']['page_name']; ?>"</b></LEGEND><?

                        $EditPage->dPrint();

                        ?></FIELDSET><?
                    }
                ?>
            </td>
        </tr>
    </table>

<?

    AdminFooter();

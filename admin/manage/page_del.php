<?php

    include_once(ADMIN_PATH."/setup.php");

    require_once(COMMON_PATH."/JsHttpRequestLib/JsHttpRequest.php");
    $JsHttpRequest = new JsHttpRequest("utf-8");

    $sec = (int)$_REQUEST['sec'];

    $Obj = new classPage($sec);

    $LocalDir = $Obj->GetProperty("page_localdir");
    $Parent = $Obj->GetProperty("page_parent");
    $DirPath = CLIENT_PATH."/".editDir::GetPath($Parent, $LocalDir);

    $result = db_query("SELECT COUNT(*) FROM Sections WHERE Sec_Parent = $sec");
    list($Cnt) = mysql_fetch_row($result);
    if(!$Cnt && is_dir($DirPath)) {
        $EditPage = new EditPage("private", $sec);
        $EditPage->MoveDir($DirPath);
        db_query("DELETE FROM Sections WHERE Sec_Code = $sec");
        db_query("DELETE FROM Permissions WHERE Sec_Code = $sec");

        $Obj = new classPage($sec);
        $SettingsPath = $Obj->GetPropertiesFile("private", $sec);
        @unlink($SettingsPath);

        $PageVarsPath = SETTINGS_PATH."/$sec.pagevars";
        @unlink($PageVarsPath);

        $Deleted = true;
    }

    if($Deleted) {
        echo "
            Раздел удален!
            <script language=\"JavaScript\">
                window.location = 'page_properties.php?mod=classPage';
            </script>
        ";
    } else {
        echo "<font color=\"red\">Нельзя удалить раздел - в нем есть подразделы - удалите сначала их !!!</font>";
    }

  
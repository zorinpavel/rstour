<?php

    $NaviStyles = array();
    for ($i=1;$i<=2;$i++) {
        $NaviStyles[$i] = ($i == $NaviSelected ? "green" : "gray");
    }

?>

<FIELDSET>
    <LEGEND>Операции</LEGEND>

    <table cellspacing="1" cellpadding="0" border="0">
        <tr>
            <?
                if($sec) {
                    ?>
                    <td><input type="button" class="<? echo $NaviStyles[1]; ?>" value="Свойства" onClick="window.location='page_properties.php?mod=classPage&sec=<? echo $sec; ?>';"></td>
                    <?
                }

                $Sections = mysql_fetch_assoc(db_query("SHOW TABLE STATUS LIKE 'Sections'"));
                $LastId = $Sections['Auto_increment'];

            ?>
            <td>
                <form method="POST" onsubmit="return sendForm(this);" action="page_create.php">
                    <input type="hidden" name="target" value="page_operations">
                    <input type="hidden" name="page_parent" value="<? echo $sec; ?>">
                    <input type="hidden" name="page_name" value="Новый раздел <? echo $LastId; ?>">
                    <input type="hidden" name="page_localdir" value="NewSection_<? echo $LastId; ?>">
                    <input type="submit" class="blue" value="Создать подраздел...">
                </form>
            </td>
            <td><input type="button" class="<? echo $NaviStyles[2]; ?>" value="Подразделы" onClick="window.location='sub_pages.php?sec=<? echo $sec; ?>';"></td>
            <?
                if($sec && $sec != "index") {
                    ?>
                    <td><input type="button" class="red" value="Удалить раздел" onClick="if (confirm('А вы точно уверены, что хотите это удалить ?')) { openUrl('page_del.php?sec=<? echo $sec; ?>', 'page_operations'); }"></td>
                    <td>&nbsp;&nbsp;&nbsp;</td><td><input type="button" class="blue" value="Содержимое" onClick="window.top.location='/admin/content/?Sec_Code=<? echo $sec; ?>'"></td>
                    <?
                } elseif($sec == "index") {
                    ?>
                    <td><input type="button" class="<? echo $NaviStyles[2]; ?>" value="Часные свойства по умолчанию" onClick="window.location='mod_private.php?mod=<? echo $mod; ?>';"></td>
                    <?
                }
            ?>
        </tr>
    </table>

    <div id="page_operations"></div>

</FIELDSET>
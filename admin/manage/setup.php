<?php

    class GoListValues extends InputButton {
        function GoListValues($Code) {
            $this->InputButton("Go$Code", "Значения", "", "window.location = 'listvalues.php?Lst_Code=$Code';");
        }
    }

    class GoStruColumns extends InputButton {
        function GoStruColumns($Code) {
            $this->InputButton("Go$Code", "Колонки", "", "window.location = 'columns.php?Str_Code=$Code';");
        }
    }

    class contentColTypesInputSelect extends InputSelect {
        function dPrint() {
            global $Row, $Str_Code;

            if($Row['Col_Code']) {
                $Col_Type = DB::selectValue("SELECT Col_Type FROM contentColumns WHERE Str_Code = $Str_Code AND Col_Code = ".$Row['Col_Code']);
                if(!$Col_Type)
                    $this->Empty = 1;
                parent::dPrint();
            } else
                echo "&nbsp;";
        }

        function contentColTypesInputSelect($Name, $Value) {
            $Options = array(
                "1"  => "Текстовое поле",
                "2"  => "Большое текстовое поле",
                "3"  => "Список значений",
                "4"  => "Мультиcписок значений",
                "31" => "Переключатель",
                "10" => "Checkbox",
                "5"  => "Загрузка файла",
                "6"  => "HTML",
                "7"  => "Мультифайл",
                "8"  => "Множественные текстовые поля",
                "9"  => "Множественные большие текстовые поля",
                "11" => "Структура",
                "12" => "Дата",
            );
            $this->InputSelect($Name, $Value, $Options);
        }
    }

    class contentListsDBSelect2 extends contentListsDBSelect {
        function dPrint() {
            global $Row;
            if($Row['Col_Type'] == 3 || $Row['Col_Type'] == 4 || $Row['Col_Type'] == 31) {
                parent::dPrint();
            } elseif($Row['Col_Type'] == 11) {
                $Structure = new InputContentStructureDBSelect($this->Name, $this->Value);
                $Structure->dPrint();
            } else {
                echo "&nbsp;";
            }
        }

        function contentListsDBSelect2($Name, $Value) {
            parent::contentListsDBSelect($Name, $Value);
        }
    }

    $contentStructure = array(
        "TableName" => "contentStructure",
        "Id"            => "Str_Code",
        "Order"         => "Str_Order DESC, Str_Code",
        "Fields"        => array("Str_Name", "Str_Order"),
        "Types"         => array("T50", "T5"),
        "Uploads"       => array(),
        "Header"        => "Структуры",
        "Headers"       => array("Название", "Порядок<br>вывода"),
        "Buttons"       => array("GoStruColumns"),
        "ButtonHeaders" => array("&nbsp;"),
        "Filters"       => array(),
        "FiltersNames"  => array(),
        "FiltersTypes"   => array(),
        "FiltersCompare" => array(),
        "EditOnly" => 0
    );

    $contentColumns = array(
        "Header"        => "Колонки",
        "TableName"     => "contentColumns",
        "Id"            => "Col_Code",
        "Order"         => "Col_Order DESC, Col_Code",
        "Fields"        => array("Col_Header", "Col_Order", "Col_Type", "Lst_Code", "Col_Id", "Col_Part", "Col_Must"),
        "Types"         => array("T50", "T5", "contentColTypesInputSelect", "contentListsDBSelect2", "T10", "InputCheckbox", "InputCheckbox"),
        "Headers"       => array("Название колонки", "Порядок<br>вывода", "Тип", "Список", "<b>ID</b><br>колонки", "В анонсе", "*"),
        "Uploads"       => array(),
        "Buttons"       => array(),
        "ButtonHeaders" => array(),
        "Filters"       => array("Str_Code"),
        "FiltersNames"  => array("Структура"),
        "FiltersTypes"   => array("InputContentStructureDBSelect"),
        "FiltersCompare" => array("N"),
        "EditOnly" => 0
    );

    $contentLists = array(
        "TableName" => "contentLists",
        "Id"            => "Lst_Code",
        "Order"         => "Lst_Order DESC, Lst_Code",
        "Fields"        => array("Lst_Name", "Lst_Order"),
        "Types"         => array("T50", "T5"),
        "Uploads"       => array(),
        "Header"        => "Списки",
        "Headers"       => array("Название", "Порядок<br>вывода"),
        "Buttons"       => array("GoListValues"),
        "ButtonHeaders" => array("&nbsp;"),
        "Filters"       => array(),
        "FiltersNames"  => array(),
        "FiltersTypes"   => array(),
        "FiltersCompare" => array(),
        "EditOnly" => 0
    );

    $contentListValues = array(
        "TableName" => "contentListValues",
        "Id"            => "LVa_Code",
        "Order"         => "LVa_Order DESC, LVa_Code",
        "Headers"       => array("Имя", "Значение", "Порядок<br>вывода"),
        "Fields"        => array("LVa_Header", "LVa_Value", "LVa_Order"),
        "Types"         => array("T50", "T10", "T5"),
        "Uploads" => array(),
        "Header"        => "Значения списков",
        "Buttons"       => array(),
        "ButtonHeaders" => array("&nbsp;"),
        "Filters"       => array("Lst_Code"),
        "FiltersNames"  => array("Название справочника"),
        "FiltersTypes"   => array("contentListsDBSelect"),
        "FiltersCompare" => array("N"),
        "EditOnly" => 0
    );


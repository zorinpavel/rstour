<?php

    include_once(ADMIN_PATH."/setup.php");


    AdminHeaders();
    MainNavi("manage");


?>

    <table width="100%" id="Frames">
        <tr>
            <td id="rtd">

                <?

                    $SectionsTree = new SectionsTree();
                    $SectionsTree->dPrint();

                ?>
            </td>
            <td id="mtd">
                <?

                    $NaviSelected = 2;
                    include("page_navi.php");

                    $mod = "classPage";
                    $sec = (int)$_REQUEST['sec'];

                    if($sec > -1) {
                        ?><FIELDSET><LEGEND>Подразделы</LEGEND><?
                        if($_REQUEST['Update']) {
                            foreach($_REQUEST['Sec_Order'] as $Key => $Order) {
                                db_query("UPDATE Sections SET Sec_Order = ".(int)$Order." WHERE Sec_Code = $Key");
                            }
                        }

                        $Sections = DB::selectArray("SELECT Sec_Code, Sec_Name, Sec_Order FROM Sections WHERE Sec_Parent = $sec ORDER BY Sec_Order DESC, Sec_Code", "Sec_Code");
                        if(count($Sections)) {
                            echo "
<table border=\"0\" cellpadding=\"0\" cellspacing=\"2\">
<form action=\"".$_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']."\" method=\"post\" name=\"EditForm\">
 <tr>
  <td align=\"right\" nowrap><b>Название раздела</b></td>
  <td><b>Порядок вывода</b></td>
 </tr>";
                            foreach($Sections as $Sec_Code => $Section) {
                                echo "
            <tr>
             <td align=\"right\" nowrap>".$Section['Sec_Name']."</td>
             <td><input type=\"text\" name=\"Sec_Order[$Sec_Code]\" value=\"".$Section['Sec_Order']."\" size=\"5\"></td>
            </tr>
          ";
                            }
                            echo "
 <tr>
  <td>&nbsp;<input type=\"hidden\" name=\"Update\" value=\"1\"></td>
  <td><input type=\"submit\" value=\"Сохранить\" class=\"green\"></td>
 </tr>
</form>
</table>";
                        }

                        ?></FIELDSET><?
                    }
                ?>
            </td>
        </tr>
    </table>

<?

    AdminFooter();

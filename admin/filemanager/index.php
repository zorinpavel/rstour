<?php

  include_once(ADMIN_PATH."/setup.php");
  include_once("ajax.php");

  AdminHeaders();
  MainNavi("filemanager");

  $MAX_UPLOAD_SIZE = min(asBytes(ini_get('post_max_size')), asBytes(ini_get('upload_max_filesize')));

?>

<style>
    A {text-decoration: none;}
    th {
        font-weight: normal; color: #1F75CC; background-color: #F0F9FF; padding:.5em 1em .5em .2em;
        text-align: left;cursor:pointer;user-select: none;
    }
    th .indicator {
        margin-left: 6px;
    }
    thead {
        border-top: 1px solid #82CFFA; border-bottom: 1px solid #96C4EA;border-left: 1px solid #E7F2FB;
        border-right: 1px solid #E7F2FB;
    }
    #top {}
    #mkdir {
        display:inline-block;
        float:right;
    }
    label {
        display:block; font-size:11px; color:#555555;
    }
    #file_drop_target {
        border: 1px dashed #CCCCCC;
        color: #CCCCCC;
        margin: 0 auto;
        padding: 12px 0;
        text-align: center;
        width: 500px;
    }
    #file_drop_target.drag_over {border: 4px dashed #96C4EA; color: #96C4EA;}
    #file_drop_target > INPUT {
        display: none;
    }
    #upload_progress {padding: 4px 0;}
    #upload_progress .error {color:#a00;}
    #upload_progress > div { padding:3px 0;}
    .no_write #mkdir, .no_write #file_drop_target {display: none}
    .progress_track {display:inline-block;width:200px;height:10px;border:1px solid #333;margin: 0 4px 0 10px;}
    .progress {background-color: #82CFFA;height:10px; }
    #breadcrumb {
        color: #AAAAAA;
        display: inline-block;
        float: left;
        padding-top: 20px;
    }
    #folder_actions {width: 50%;float:right;}
    .sort_hide{ display:none;}
    table {border-collapse: collapse;width:100%;}
    thead {max-width: 1024px}
    td {
        padding: 3px;
        border-bottom: 1px solid #DDEEFF;
        white-space: nowrap;
        vertical-align: middle;
    }
    td.first {white-space: normal;}
    td.empty { color: #777777; font-style: italic; text-align: center; padding:3em 0;}
    .is_dir .size { color:transparent; font-size: 0;}
    .is_dir .size:before {content: "--"; color: #333333;}
    .is_dir .download{visibility: hidden}
    a.delete {
        background: url('/admin/images/delete.png') no-repeat scroll center center;
        display: inline-block;
        width: 16px;
        height: 16px;
        margin: 2px;
    }
    .name {
        background: url('/admin/images/file.png') no-repeat scroll left center;
        padding: 6px 0 6px 30px;
    }
    .is_dir .name {
        background: url('/admin/images/folder.gif') no-repeat scroll left center;
        padding: 6px 0 6px 30px;
    }
    .download {
        background: url('/admin/images/download.png') no-repeat scroll center center;
        display: inline-block;
        width: 16px;
        height: 16px;
        margin: 2px;
    }
</style>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script type="text/javascript">
    (function($){
        $.fn.tablesorter = function() {
            var $table = this;
            this.find('th').click(function() {
                var idx = $(this).index();
                var direction = $(this).hasClass('sort_asc');
                $table.tablesortby(idx,direction);
            });
            return this;
        };
        $.fn.tablesortby = function(idx,direction) {
            var $rows = this.find('tbody tr');
            function elementToVal(a) {
                var $a_elem = $(a).find('td:nth-child('+(idx+1)+')');
                var a_val = $a_elem.attr('data-sort') || $a_elem.text();
                return (a_val == parseInt(a_val) ? parseInt(a_val) : a_val);
            }
            $rows.sort(function(a,b){
                var a_val = elementToVal(a), b_val = elementToVal(b);
                return (a_val > b_val ? 1 : (a_val == b_val ? 0 : -1)) * (direction ? 1 : -1);
            });
            this.find('th').removeClass('sort_asc sort_desc');
            $(this).find('thead th:nth-child('+(idx+1)+')').addClass(direction ? 'sort_desc' : 'sort_asc');
            for(var i =0;i<$rows.length;i++)
                this.append($rows[i]);
            this.settablesortmarkers();
            return this;
        };
        $.fn.retablesort = function() {
            var $e = this.find('thead th.sort_asc, thead th.sort_desc');
            if($e.length)
                this.tablesortby($e.index(), $e.hasClass('sort_desc') );

            return this;
        };
        $.fn.settablesortmarkers = function() {
            this.find('thead th span.indicator').remove();
            this.find('thead th.sort_asc').append('<span class="indicator">&darr;<span>');
            this.find('thead th.sort_desc').append('<span class="indicator">&uarr;<span>');
            return this;
        }
    })(jQuery);

    $(function(){
        var XSRF = (document.cookie.match('(^|; )_sfm_xsrf=([^;]*)')||0)[2];
        var MAX_UPLOAD_SIZE = <?php echo $MAX_UPLOAD_SIZE ?>;
        var $tbody = $('#list');
        $(window).bind('hashchange',list).trigger('hashchange');
        $('#table').tablesorter();

        $('.delete').live('click',function(data) {
            $.post("",{'do':'delete',file:$(this).attr('data-file'),xsrf:XSRF},function(response){
                list();
            },'json');
            return false;
        });

        $('#mkdir').submit(function(e) {
            var hashval = window.location.hash.substr(1),
                $dir = $(this).find('[name=name]');
            e.preventDefault();
            $dir.val().length && $.post('ajax.php',{'do':'mkdir',name:$dir.val(),xsrf:XSRF,file:hashval},function(data){
                list();
            },'json');
            $dir.val('');
            return false;
        });

        // file upload stuff
        $('#file_drop_target').bind('dragover',function(){
            $(this).addClass('drag_over');
            return false;
        }).bind('dragend',function(){
            $(this).removeClass('drag_over');
            return false;
        }).bind('drop',function(e){
            e.preventDefault();
            var files = e.originalEvent.dataTransfer.files;
            $.each(files,function(k,file) {
                uploadFile(file);
            });
            $(this).removeClass('drag_over');
        }).bind('dblclick',function(e){
            $('input[type=file]').click();
        });
        $('input[type=file]').change(function(e) {
            e.preventDefault();
            $.each(this.files, function(k, file) {
                uploadFile(file);
            });
        });


        function uploadFile(file) {
            var folder = window.location.hash.substr(1);
            if(file.size > MAX_UPLOAD_SIZE) {
                var $error_row = renderFileSizeErrorRow(file,folder);
                $('#upload_progress').append($error_row);
                window.setTimeout(function(){$error_row.fadeOut();},5000);
                return false;
            }

            var $row = renderFileUploadRow(file,folder);
            $('#upload_progress').append($row);
            var fd = new FormData();
            fd.append('file_data',file);
            fd.append('file',folder);
            fd.append('xsrf',XSRF);
            fd.append('do','upload');
            var xhr = new XMLHttpRequest();
            xhr.open('POST', 'ajax.php');
            xhr.onload = function() {
                $row.remove();
                list();
            };
            xhr.upload.onprogress = function(e){
                if(e.lengthComputable) {
                    $row.find('.progress').css('width',(e.loaded/e.total*100 | 0)+'%' );
                }
            };
            xhr.send(fd);
        }

        function renderFileUploadRow(file,folder) {
            return $row = $('<div/>')
                .append( $('<span class="fileuploadname" />').text( (folder ? folder+'/':'')+file.name))
                .append( $('<div class="progress_track"><div class="progress"></div></div>')  )
                .append( $('<span class="size" />').text(formatFileSize(file.size)) )
        }

        function renderFileSizeErrorRow(file, folder) {
            return $row = $('<div class="error" />')
                .append( $('<span class="fileuploadname" />').text( 'Error: ' + (folder ? folder+'/':'')+file.name))
                .append( $('<span/>').html(' file size - <b>' + formatFileSize(file.size) + '</b>'
                    +' exceeds max upload size of <b>' + formatFileSize(MAX_UPLOAD_SIZE) + '</b>')  );
        }

        function list() {
            var hashval = window.location.hash.substr(1);
            $.get('ajax.php',{'do':'list','file':hashval}, function(data) {
                $tbody.empty();
                $('#breadcrumb').empty().html(renderBreadcrumbs(hashval));
                if(data.success) {
                    $.each(data.results,function(k,v){
                        $tbody.append(renderFileRow(v));
                    });
                    !data.results.length && $tbody.append('<tr><td class="empty" colspan=5>Папка пуста</td>')
                    data.is_writable ? $('body').removeClass('no_write') : $('body').addClass('no_write');
                } else {
                    console.warn(data.error.msg);
                }
                $('#table').retablesort();
            },'json');
        }

        function renderFileRow(data) {
            var $link = $('<a class="name" />')
                .attr('href', data.is_dir ? '#' + data.path : '/data/' + data.path)
                .text(data.name);
            $link = data.is_dir ? $link : $link.attr('target', '_blank');
            var $dl_link = $('<a/>').attr('href','?do=download&file='+encodeURIComponent(data.path))
                .addClass('download').text('');
            var $delete_link = $('<a href="#" />').attr('data-file',data.path).addClass('delete').text('');
            var perms = [];
            if(data.is_readable) perms.push('read');
            if(data.is_writable) perms.push('write');
//            if(data.is_executable) perms.push('exec');
            var $html = $('<tr />')
                .addClass(data.is_dir ? 'is_dir' : '')
                .append( $('<td class="first" />').append($link) )
                .append( $('<td/>').attr('data-sort',data.is_dir ? -1 : data.size)
                    .html($('<span class="size" />').text(formatFileSize(data.size))) )
                .append( $('<td/>').attr('data-sort',data.mtime).text(formatTimestamp(data.mtime)) )
                .append( $('<td/>').text(perms.join('+')) )
                .append( $('<td/>').append($dl_link).append( data.is_deleteable ? $delete_link : '') )
            return $html;
        }

        function renderBreadcrumbs(path) {
            var base = "",
                $html = $('<div/>').append( $('<a href=#>/ data</a></div>') );
            $.each(path.split('/'),function(k,v){
                if(v) {
                    $html.append( $('<span/>').text(' / ') )
                        .append( $('<a/>').attr('href','#'+base+v).text(v) );
                    base += v + '/';
                }
            });
            return $html;
        }

        function formatTimestamp(unix_timestamp) {
            var m = ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июнь', 'Июль', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'];
            var d = new Date(unix_timestamp * 1000);
            return [d.getDate(),' ',m[d.getMonth()],' ',d.getFullYear()," ",
                (d.getHours() % 12 || 12),":",(d.getMinutes() < 10 ? '0' : '')+d.getMinutes(),
                " ",d.getHours() >= 12 ? 'PM' : 'AM'].join('');
        }

        function formatFileSize(bytes) {
            var s = ['bytes', 'KB','MB','GB','TB','PB','EB'];
            for(var pos = 0;bytes >= 1000; pos++,bytes /= 1024);
            var d = Math.round(bytes*10);
            return pos ? [parseInt(d/10),".",d%10," ",s[pos]].join('') : bytes + ' bytes';
        }
    })

</script>

<div class="EditData">
    <div id="top">
        <form action="?" method="post" id="mkdir" />
            <label for=dirname>Создать папку</label><input id=dirname type=text name=name value="" />
            <input type="submit" value="Создать" class="green">
        </form>
        <div id="file_drop_target">
            Перетащите файлы сюда или выберите двойным кликом
            <input type="file" multiple />
        </div>
        <div id="breadcrumb">&nbsp;</div>
    </div>

    <div id="upload_progress"></div>

    <table id="table">
        <thead>
            <tr>
                <th>Имя</th>
                <th>Размер</th>
                <th>Дата</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody id="list"></tbody>
    </table>
</div>

<?

    AdminFooter();
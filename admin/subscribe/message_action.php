<?php
  
  error_reporting(E_ALL ^ E_NOTICE);
  $usrGroup = 11;
  include_once(ADMIN_PATH."/setup.php");
  include_once("setup.php");

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<META http-equiv=Content-Type content="text/html; charset=utf-8">
<title>ADMIN MAIN</title>
</head>
<body style="color:#FFFFFF;background-color:#000000">

<?php

  $Mes_Code = (int)$_REQUEST['Mes_Code'];
  $First = (int)$_REQUEST['First'] ? (int)$_REQUEST['First'] : 0;

  if($Mes_Code && is_array($_REQUEST['Groups']) && $_REQUEST['Update']) {

    $Mail = _autoload("classMail");
    $Mail->Debug = true;

    $Message = DB::selectArray("SELECT * FROM subscribeMessage WHERE Mes_Code = $Mes_Code");
    $Mail->Data = $Mail->GetParams($Message['Mes_Code']);
    $Template = $Mail->GetTmp($Message['Tmp_Code']);

    $Groups = join(", ", $_REQUEST['Groups']);
    $Users = DB::selectArray("SELECT su.* FROM subscribeUsersGroups AS sug LEFT JOIN subscribeUsers AS su ON su.us_id = sug.us_id WHERE sug.Gro_Code IN ($Groups) AND su.Us_Active = 1 GROUP BY su.us_id LIMIT $First, 1", "us_id");
    
    foreach($Users as $us_id => $User) {
      $External['UserName'] = $User['Us_Name'];
      $External['UserMail'] = $User['Us_Mail'];

      $Mail->SendMail($User['Us_Mail'], $Message['Mes_Subject'], $Mail->MailTemplate2Php($Template, $External) );
//      echo $Mail->MailTemplate2Php($Template, $External);
    }

    $requestGroups = "";
    if(count($Users) > 0) {
      $First++;
      foreach($_REQUEST['Groups'] as $Key => $Group) {
        $requestGroups .= "&Groups[$Key]=$Group";
      }
      echo "<script>setTimeout('window.location.href = \'?First=$First&Mes_Code=$Mes_Code$requestGroups&Update=1\'', 10000);</script>";
    } else {
      @unlink(CACHE_PATH."/mail.template");
      echo "ok";
    }
  } elseif($Mes_Code && is_array($_REQUEST['Groups']) && $_REQUEST['Cron']) {
    echo "Crontab must be here...";
  }

?>

<div style="text-decoration: blink !important;">_</div>

</body>
</html>


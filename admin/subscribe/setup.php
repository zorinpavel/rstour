<?php


    class ParamTypeInputSelect extends InputSelect {
        function ParamTypeInputSelect($Name, $Value) {
            $Options = array(
                "1"=>"Текстовое поле",
                "2"=>"Большое текстовое поле",
                "3"=>"Загрузка файла",
                "4"=>"HTML",
            );
            $this->InputSelect($Name, $Value, $Options);
        }
    }

    class GoEditTmpl extends InputButton {
        function GoEditTmpl($Code) {
            $this->InputButton("Qua$Code", "Редактировать", "", "window.location.href = 'edit_template.php?Tmp_Code=$Code';");
        }
    }

    class GoDelUsr extends InputButton {
        function GoDelUsr($Code) {
            $this->InputButton("Qua$Code", "Удалить", "", "if(confirm('Вы действительно хотите удалить пользователя из рассылки?')) { openUrl('del_user.php?us_id=$Code', 'delUsr', this, true, 0, 100, 'Удалить пользователя'); }", "red");
        }
    }

    class GoEditUsr extends InputButton {
        function GoEditUsr($Code) {
            $this->InputButton("Qua$Code", "Редактировать", "", "window.location.href = 'edit_user.php?us_id=$Code';");
        }
    }

    class GoEditMessage extends InputButton {
        function GoEditMessage($Code) {
            $this->InputButton("Qua$Code", "Параметры", "", "window.location.href = 'message_edit.php?Mes_Code=$Code';");
        }
    }

    class GoSendMessage extends InputButton {
        function GoSendMessage($Code) {
            $this->InputButton("Qua$Code", "Разослать", "", "window.location.href = 'message_send.php?Mes_Code=$Code';", "blue");
        }
    }

    class subscribeTemplatesDBSelect extends InputDBSelect {
        function subscribeTemplatesDBSelect($Name, $Value, $onChange="") {
            $this->InputDBSelect($Name, $Value, "subscribeTemplates", "Tmp_Code", "Tmp_Name", "", "Tmp_Order", "", $onChange);
            $this->Empty = 1;
        }
    }

    class InputGroupsDBMultiSelect extends InputDBMultiSelect {
        function dPrint() {
            $Checked = "";
            $list = db_query("SELECT Gro_Name, Gro_Code FROM Groups");
            echo "<table border=0 cellspacing=1 cellpadding=1>";
            while(list($Gro_Name, $Gro_Code) = mysql_fetch_row($list)) {
                if(is_array($this->Options))
                    $Checked = array_key_exists($Gro_Code, $this->Options) ? "checked" : "";
                echo "<tr><td><input type=\"checkbox\" name=\"".$this->Name."[]\" value=\"$Gro_Code\" class=\"radio\" $Checked></td>";
                echo "<td>$Gro_Name</td></tr>";
            }
            echo "</table>";
        }
        function InputGroupsDBMultiSelect($Name, $Value, $onChange="") {
            parent::InputDBMultiSelect($Name, $SelectValue, "subscribeUsersGroups", "Gro_Code", "us_id", "us_id = $Value", "", "", "", 1);
        }
    }

    class InputMessageGroupsDBMultiSelect extends InputDBMultiSelect {
        function InputMessageGroupsDBMultiSelect($Name, $Value, $onChange="") {
            parent::InputDBMultiSelect($Name, $Value, "Groups", "Gro_Code", "Gro_Name", "", "", "", "", 1);
        }
    }

    class InputGroupsDBSelect extends InputDBSelect {
        function InputGroupsDBSelect($Name, $Value, $onChange="") {
            parent::InputDBSelect("Groups", $Value, "Groups", "Gro_Code", "Gro_Name", "", "Gro_Order DESC, Gro_Code", "", "", "");
            $this->Empty = 1;
        }
    }

    class InputUsersGroupsDBSelect extends InputDBSelect {
        function InputUsersGroupsDBSelect($Name, $Value, $onChange="") {
            $Users = DB::selectArray("SELECT us_id FROM subscribeUsersGroups WHERE Gro_Code = $Value");
            foreach($Users as $Key => $User) {
                $this->Values[$Key] = $User['us_id'];
            }
            parent::InputDBSelect("Gro_Code", $Value, "Groups", "Gro_Code", "Gro_Name", "", "Gro_Order DESC, Gro_Code", "", $onChange);
            $this->Empty = 1;
        }
    }

    class GoUsersGroups extends InputButton {
        function GoUsersGroups($Code) {
            $this->InputButton("Qua$Code", "Адреса", "", "window.location.href = 'users_groups.php?Gro_Code=$Code';");
        }
    }

    $Templates = array(
        "TableName" => "subscribeTemplates",
        "Id"            => "Tmp_Code",
        "Order"         => "Tmp_Order DESC, Tmp_Code DESC",
        "Fields"        => array("Tmp_Name", "Tmp_Order"),
        "Types"         => array("T30", "T5"),
        "Uploads"       => array(),
        "Header"        => "Шаблоны для рассылки",
        "Headers"       => array("Название шаблона", "Порядок<br>вывода"),
        "Buttons"       => array("GoEditTmpl"),
        "ButtonHeaders" => array("&nbsp;"),
        "Filters"       => array(),
        "FiltersNames"  => array(),
        "FiltersTypes"   => array(),
        "FiltersCompare" => array(),
        "EditOnly" => 0
    );

    $Messages = array(
        "TableName" => "subscribeMessage",
        "Id"            => "Mes_Code",
        "Order"         => "Mes_Order DESC, Mes_Code DESC",
        "Fields"        => array("Mes_Name", "Mes_Order"),
        "Types"         => array("T30", "T5"),
        "Uploads"       => array(),
        "Header"        => "Список рассылок",
        "Headers"       => array("Название", "Порядок<br>вывода"),
        "Buttons"       => array("GoEditMessage", "GoSendMessage"),
        "ButtonHeaders" => array("&nbsp;", "&nbsp;"),
        "Filters"       => array(),
        "FiltersNames"  => array(),
        "FiltersTypes"   => array(),
        "FiltersCompare" => array(),
        "EditOnly" => 0
    );

    $MessageEdit = array(
        "TableName" => "subscribeMessage",
        "Id"            => "Mes_Code",
        "IdName"        => "Mes_Name",
        "Fields"        => array("Mes_Name", "Tmp_Code", "Mes_Subject"),
        "Types"         => array("T30", "subscribeTemplatesDBSelect", "T50"),
        "Headers"       => array("Название", "Шаблон", "Заголовок письма"),
        "Header"        => "Редактировать сообщение",
        "Buttons"       => array(),
        "ViewOnly" => 0,
        "Params" => 1,
    );

    $Users = array(
        "TableName" => "subscribeUsers",
        "Id"            => "us_id",
        "Order"         => "Us_Name, us_id",
        "Fields"        => array("Us_Mail", "Us_Name", "Us_Active"),
        "Types"         => array("T30", "T30", "InputCheckbox"),
        "Uploads"       => array(),
        "Header"        => "Пользователи",
        "Headers"       => array("E-Mail", "Имя", "&nbsp;"),
        "Buttons"       => array("GoEditUsr", "GoDelUsr"),
        "ButtonHeaders" => array("&nbsp;", "&nbsp;"),
        "Filters"       => array(),
        "FiltersNames"  => array(),
        "FiltersTypes"   => array(),
        "FiltersCompare" => array(),
        "EditOnly" => 0
    );

    $UserEdit = array(
        "TableName" => "subscribeUsers",
        "Id"            => "us_id",
        "IdName"        => "us_id",
        "Fields"        => array("Us_Name", "Us_Mail", "Us_Active"),
        "Types"         => array("T30", "T30", "InputCheckbox"),
        "Headers"       => array("Имя", "E-Mail", "Активный"),
        "Header"        => "Редактировать пользователя",
        "Buttons"       => array(),
        "ViewOnly" => 0,
        "Params" => 1,
    );

    $Groups = array(
        "TableName" => "subscribeGroups",
        "Id"            => "Gro_Code",
        "Order"         => "Gro_Order DESC, Gro_Code",
        "Fields"        => array("Gro_Name", "Gro_Order"),
        "Types"         => array("T50", "T5"),
        "Uploads" => array(),
        "Header"        => "Группы пользователей",
        "Headers"       => array("Название", "Порядок<br>вывода"),
        "Buttons"       => array("GoUsersGroups"),
        "ButtonHeaders" => array("&nbsp;"),
        "Filters"       => array(),
        "FiltersNames"  => array(),
        "FiltersTypes"   => array(),
        "FiltersCompare" => array(),
        "EditOnly" => 0
    );

    $UsersGroups = array(
        "TableName" => "subscribeUsers",
        "Id"            => "us_id",
        "Order"         => "Us_Name, us_id",
        "Fields"        => array("Us_Name", "Us_Active"),
        "Types"         => array("T30", "InputCheckbox"),
        "Uploads"       => array(),
        "Header"        => "Пользователи в группе",
        "Headers"       => array("Имя", "&nbsp;"),
        "Buttons"       => array("GoEditUsr"),
        "ButtonHeaders" => array("&nbsp;"),
        "Filters"       => array("Gro_Code"),
        "FiltersNames"  => array("Группа"),
        "FiltersTypes"   => array("InputUsersGroupsDBSelect"),
        "FiltersCompare" => array("IN"),
        "FiltersJoin"   => array("us_id"),
        "EditOnly" => 1,
        "ViewOnly" => 1,
    );
  
  

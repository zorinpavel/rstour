<?php

    define(ENGINE_PATH, ADMIN_PATH."/content/edit");

    $pageIndex = ENGINE_PATH."/index.php";
    $pageDig_Edit = ENGINE_PATH."/dig_edit.php";
    $pageDig_Del = ENGINE_PATH."/dig_del.php";

    $Dig_AuthorDefault = "";
    $Dig_AuthorURLDefault = "";
    $Dig_CopyrightDefault = "";

    $ContentUsers = false;
    $ContentRelated = false;

    $Digest = "contentItems";

    include(ENGINE_PATH."/lang/ru.php");
    include(EDITOR_PATH."/dhtml.php");

    
    class contentUsersDBSelect extends InputDBSelect {
        function contentUsersDBSelect($Name, $Value) {
            $this->InputDBSelect($Name, $Value, "AdmUsers", "us_id", "login", "", "us_id", "", "");
            $this->Empty = 1;
        }
    }

    
    class contentListValuesDBSelect extends InputDBSelect {
        function contentListValuesDBSelect($Name, $Value, $Lst_Code) {
            $this->InputDBSelect($Name, $Value, "contentListValues", "LVa_Code", "LVa_Header", "Lst_Code='$Lst_Code'", "LVa_Order, LVa_Code", "", "");
            $this->Empty = 1;
        }
    }

    
    class contentListValuesDBMultiSelect extends InputDBMultiSelect {
        function contentListValuesDBMultiSelect($Name, $Value, $Lst_Code, $CheckBoxes) {
            $this->InputDBMultiSelect($Name, $Value, "contentListValues", "LVa_Code", "LVa_Header", "Lst_Code='$Lst_Code'", "LVa_Order, LVa_Code", "", "", $CheckBoxes);
        }
    }

    
    class contentListValuesDBMultiSelectRadio extends InputDBSelect {
        function contentListValuesDBMultiSelectRadio($Name, $Value, $Lst_Code, $CheckBoxes) {
            $this->InputDBSelect($Name, $Value, "contentListValues", "LVa_Code", "LVa_Header", "Lst_Code='$Lst_Code'", "LVa_Order, LVa_Code", "", "", $CheckBoxes);
        }

        function dPrint() {
            foreach($this->Options as $Key => $Value) {
                $Checked = ($Key == $this->Value ? " checked" : "");
                dPrint("<input type=\"radio\" name=\"".$this->Name."\" value=\"$Key\" class=\"radio\" id=\"id$Key\"$Checked><label for=\"id$Key\">$Value</label>");
            }
        }
    }
    
    
    class contentFilesDBMultiSelect extends InputMultiSelect {
        function dPrint($Cnt = 1) {
            if($this->Value) {
                foreach($this->Value as $k => $Value) {
                    dPrint("<div id=\"".$this->Name."_v_$k\">");
                    dPrint("<input type=\"file\" name=\"".$this->Name."[$k]\">");
                    if($Value)
                        dPrint("<a href=\"".$Value['Data']."\" target=\"_blank\">".$Value['Data']."</a> &nbsp; <small>[<a href=\"#\" onClick=\"if(confirm('Вы действительно хотите удалить файл?')) { openUrl('file_del.php?file=".$Value['Data']."', '".$this->Name."_v_$k'); return false; }\" class=\"SectionLineEdit\">удалить</a>]</small>");
                    dPrint("<br><input type=\"text\" value=\"".$Value['Data_Desc']."\" name=\"".$this->Name."[$k]\" size=\"50\" style=\"margin:3px 0 10px;\">");
                    dPrint("</div>");
                }
            } else {
                dPrint("<div id=\"".$this->Name."_".$Cnt."\">");
                dPrint("<input type=\"file\" name=\"".$this->Name."[$Cnt]\">");
                dPrint("<br><input type=\"text\" value=\"\" name=\"".$this->Name."[$Cnt]\" size=\"50\" style=\"margin:3px 0 10px;\">");
                dPrint("</div>");
            }
        }
    }

    
    class contentFieldDBMultiSelect extends InputMultiSelect {
        //    function contentFieldDBMultiSelect($Name, $Value, $Lst_Code) {
        //      $this->InputDBMultiSelect($Name, $Value, "contentListValues", "LVa_Code", "LVa_Header", "Lst_Code='$Lst_Code'", "LVa_Order", "", "");
        //    }
        function dPrint($Cnt=0) {
            if($this->Value) {
                if(!is_array($this->Value)) {
                    $this->Val[0] = $this->Value;
                } else {
                    $this->Val = $this->Value;
                }
                foreach($this->Val as $k => $Value) {
                    dPrint("<div id=\"".$this->Name."_v_$k\">");
                    $Input = new T50($this->Name."[$k]", $Value);
                    $Input->dPrint();
                    dPrint("</div>");
                }
            } else {
                if(!$Cnt)
                    dPrint("<div id=\"".$this->Name."_".$Cnt."\">");
                $Input = new T50($this->Name."[]", "");
                $Input->dPrint();
                if(!$Cnt)
                    dPrint("</div>");
            }
        }
    }

    
    class contentTFieldDBMultiSelect extends InputMultiSelect {
        //    function contentTFieldDBMultiSelect($Name, $Value, $Lst_Code) {
        //      $this->InputDBMultiSelect($Name, $Value, "contentListValues", "LVa_Code", "LVa_Header", "Lst_Code='$Lst_Code'", "LVa_Order", "", "");
        //    }
        function dPrint($Cnt=0) {
            if($this->Value) {
                if(!is_array($this->Value)) {
                    $this->Val[0] = $this->Value;
                } else {
                    $this->Val = $this->Value;
                }
                foreach($this->Val as $k => $Value) {
                    dPrint("<div id=\"".$this->Name."_v_$k\">");
                    dPrint("<textarea class=\"title\" style=\"width:100%\" rows=\"3\" name=\"".$this->Name."[$k]\">$Value</textarea>");
                    dPrint("</div>");
                }
            } else {
                if(!$Cnt)
                    dPrint("<div id=\"".$this->Name."_".$Cnt."\">");
                dPrint("<textarea class=\"title\" style=\"width:100%\" rows=\"3\" name=\"".$this->Name."[]\"></textarea>");
                if(!$Cnt)
                    dPrint("</div>");
            }
        }
    }

    
    class InputContentLanguageSelect extends InputSelect {

        function InputContentLanguageSelect($Name, $Value) {
            global $TemplateSets;

            $this->Value = $Value ? $Value : "";
            if($_COOKIE['Lang'] && $_COOKIE['Lang'] != "all")
                $this->Value = $_COOKIE['Lang'];

            if(!$this->Value)
                $this->Value = DEFAULT_LANGUAGE;

            $this->Name = $Name;
            $this->Options = $TemplateSets;
        }

        function dPrint() {
            parent::dPrint();
        }

    }

    
    class InputAdditionalSections extends Input {
        var $SectionsArr;

        function GetSectionName($Sec_Code) {
            if ($Sec_Code)
                return $this->GetSectionName($this->SectionsArr[$Sec_Code]["Data"]["Parent"])." / ".$this->SectionsArr[$Sec_Code]["Data"]["Name"];
            else
                return "Главная";
        }

        function dPrint() {
            $this->SectionsArr = array();
            $Sections = DB::selectArray("SELECT Sec_Code, Sec_Parent, Sec_Name FROM Sections", "Sec_Code");
            foreach($Sections as $Sec_Code => $Section) {
                $this->SectionsArr[$Sec_Code] = array(
                    "Data" => array(
                        "Parent" => $Section['Sec_Parent'],
                        "Name" => $Section['Sec_Name']
                    ),
                    "Parent" => ""
                );
            }
            foreach($this->SectionsArr as $Key => $Section) {
                if($Section["Data"]["Parent"])
                    $this->SectionsArr[$Key]["Parent"] = &$this->SectionsArr[$Section["Data"]["Parent"]];
            }

            if(is_array($this->Value)) {
                echo "<table border=0 cellspacing=1 cellpadding=1>";
                foreach($this->Value as $Key => $Value) {
                    echo "<tr><td><input type=\"checkbox\" name=\"".$this->Name."[]\" value=\"$Value\" checked></td>".
                         "<td><nobr>".$this->GetSectionName($Value)."</nobr></td></tr>";
                }
                echo "</table>";
            }

            $Obj = new InputSectionsTree($this->Name."[]", "", $this->onClick, $this->Style);
            $Obj->Empty = 1;
            $Obj->Value = "-1";
            $Obj->dPrint();
        }

        function InputAdditionalSections($Name, $Ite_Code, $onClick="", $Style="") {
            $AddSections = DB::selectArray("SELECT Section FROM contentItemsSections WHERE Item = $Ite_Code");
            parent::Input($Name, $AddSections, $Style);
        }
    }

    
    class contentItemsDBSelect extends InputDBSelect {
        function contentItemsDBSelect($Name, $Value, $Str_Code) {
            $this->InputDBSelect($Name, $Value, "contentItems", "Ite_Code", "Ite_Header", "Str_Code = '$Str_Code' AND Ite_State = 0", "Ite_Priority DESC, Ite_Code", "", "");
            $this->Empty = 1;
        }
    }


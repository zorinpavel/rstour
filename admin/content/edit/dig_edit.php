<?php

    include(ENGINE_PATH."/functions.php");

    $Ite_Code = (int)$_REQUEST['Ite_Code'];
    $Sec_Code = (int)$_REQUEST['Sec_Code'];
    $Str_Code = (int)$_REQUEST['Str_Code'];
    

    if(!$_REQUEST['Update']) {
        dPrintDigestHeader($Ite_Code);
    } else {

        foreach($_POST as $Name => $Value) {
            if(!is_array($Value))
                ${$Name} = mysql_real_escape_string($Value);
        }

        $Ite_Header = trim($Ite_Header);
        $us_id = (int)$_REQUEST['us_id'] ?: "NULL";

        if($Ite_Code) {
            foreach($TemplateSets as $Key => $Lang) {
                DBMem::Delete("contentItemVars.".$Ite_Code."_$Key");
                DBMem::Delete("contentItemVars.".$Ite_Code.".part_$Key");
            }
            DBMem::Delete("contentItemVars.".$Ite_Code);
            DBMem::Delete("contentItemVars.".$Ite_Code.".part");
        }

        if(!($Dig_BDay+$Dig_BMonth+$Dig_BYear+$Dig_EDay+$Dig_EMonth+$Dig_EYear)) {
            $Dig_BDay = date("d", time());
            $Dig_BMonth = date("m", time());
            $Dig_BYear = date("Y", time());
            $Dig_EDay = date("d", time());
            $Dig_EMonth = date("m", time());
            $Dig_EYear = date("Y", mktime(0, 0, 0, date("m"), date("d"), date("Y")+5));
        }

        if($Ite_Code) {
            $query = "UPDATE contentItems SET".
                " Ite_Header     = '$Ite_Header'";
            if(DEFAULT_LANGUAGE && $Ite_Language)
                $query .= ",Ite_Language = '$Ite_Language'";
            $query .= ",Ite_BDate     = '".fYearMonthDay2Str($Dig_BDay, $Dig_BMonth, $Dig_BYear)."'".
                ",Ite_EDate      = '".fYearMonthDay2Str($Dig_EDay, $Dig_EMonth, $Dig_EYear)."'".
                ",Ite_State      = '$Ite_State'".
                ",Ite_Priority   = ".(int)$Ite_Priority.
                ",Sec_Code       = '$Sec_Code'".
                ",Str_Code       = '$Str_Code'".
                ",us_id          = $us_id".
                ",Time_Change    = NOW()".
                " WHERE Ite_Code = $Ite_Code";
            DB::Query($query);
        } else {
            $query = "INSERT INTO contentItems SET".
                " Ite_Header    = '$Ite_Header'";
            if(DEFAULT_LANGUAGE && $Ite_Language)
                $query .= ",Ite_Language = '$Ite_Language'";
            $query .= ",Ite_BDate    = '".fYearMonthDay2Str($Dig_BDay, $Dig_BMonth, $Dig_BYear)."'".
                ",Ite_EDate     = '".fYearMonthDay2Str($Dig_EDay, $Dig_EMonth, $Dig_EYear)."'".
                ",Ite_State     = '$Ite_State'".
                ",Ite_Priority  = ".(int)$Ite_Priority.
                ",Sec_Code      = '$Sec_Code'".
                ",Ite_Time      = NOW()".
                ",Str_Code      = '$Str_Code'";
                ",us_id         = $us_id";
            DB::Query($query);
            $Ite_Code = mysql_insert_id();
        };
    
        if(SEF_URL && $Ite_Sef)
            sefRouter::Register("classContent", $Ite_Code, $Ite_Sef);

        $Relate = $_POST["Relate"];
        $RelCheck = $_POST['RelCheck'] ?: array();

        if($Ite_Code) {
            if(count($Relate)) {
                foreach($Relate as $key => $value) {
                    if(strlen($value)) {
                        $text = htmlspecialchars($value);
                        if((int)$text) {
                            $isExists = DB::selectValue("SELECT Ite_Code FROM contentItems WHERE Ite_Code = ".(int)$text);
                            if($isExists) {
                                if($Ite_Code != (int)$text) {
                                    DB::Query("INSERT INTO contentRelated SET Ite_Code = $Ite_Code, Ite_CodeR = ".(int)$text." ON DUPLICATE KEY UPDATE Ite_Code = $Ite_Code, Ite_CodeR = ".(int)$text);
                                    if(RELATED_REVERSE)
                                        DB::Query("INSERT INTO contentRelated SET Ite_Code = ".(int)$text.", Ite_CodeR = $Ite_Code ON DUPLICATE KEY UPDATE Ite_Code = ".(int)$text.", Ite_CodeR = $Ite_Code");
                                    $RelCheck[] = (int)$text;
                                }
                            }
                        } else {
                            $result = DB::selectArray("SELECT Ite_Code FROM contentItems WHERE LOWER(Ite_Header) LIKE LOWER('%$text%')", "Ite_Code");
                            foreach($result as $Ite_CodeR => $Item) {
                                if($Ite_Code != $Ite_CodeR) {
                                    DB::Query("INSERT INTO contentRelated SET Ite_Code = $Ite_Code, Ite_CodeR = $Ite_CodeR ON DUPLICATE KEY UPDATE Ite_Code = $Ite_Code, Ite_CodeR = $Ite_CodeR");
                                    if(RELATED_REVERSE)
                                        DB::Query("INSERT INTO contentRelated SET Ite_Code = $Ite_CodeR, Ite_CodeR = $Ite_Code ON DUPLICATE KEY UPDATE Ite_Code = $Ite_CodeR, Ite_CodeR = $Ite_Code");
                                    $RelCheck[] = $Ite_CodeR;
                                }
                            }
                        }
                    }
                }
            }
            $Items = DB::selectArray("SELECT cr.Ite_Code, cr.Ite_CodeR, ci.Ite_Header FROM contentRelated AS cr
                                      INNER JOIN contentItems AS ci
                                        ON cr.Ite_CodeR = ci.Ite_Code
                                      WHERE cr.Ite_Code = $Ite_Code", "Ite_CodeR");

            if(count($Items)) {
                foreach($Items as $Ite_CodeR => $Item) {
                    if(!in_array($Ite_CodeR, array_values($RelCheck))) {
                        DB::Query("DELETE FROM contentRelated WHERE Ite_Code = $Ite_Code AND Ite_CodeR = $Ite_CodeR");
                        if(RELATED_REVERSE)
                            DB::Query("DELETE FROM contentRelated WHERE Ite_CodeR = $Ite_Code AND Ite_Code = $Ite_CodeR");
                    }
                }
            }
        }

        if($Ite_Code) {
            $AddSections = $_POST['AddSections'];
            DB::Query("DELETE FROM contentItemsSections WHERE Item = $Ite_Code");
            if(isset($AddSections) && is_array($AddSections)) {
                foreach($AddSections as $S) {
                    if(!empty($S) && $S != $Sec_Code)
                        DB::Query("INSERT INTO contentItemsSections SET Item = $Ite_Code, Section = $S");
                }
            }
        }

        if(!$_POST['ChangeStructure']) {

            if(CONTENT_TAGS) {
                $classTags = _autoload("classTags");
                $classTags->RemoveAll(CONTENT_TAGS, $Ite_Code);
                $Tags = explode(",", $_POST['Ite_Tags']);
                foreach($Tags as $Key => $Tag)
                    $classTags->Add($Tag, CONTENT_TAGS, $Ite_Code);
            }

            if($Ite_Code) {
                $r = DB::Query("SELECT Col_Code, Col_Type, Col_Must, Lst_Code FROM contentColumns WHERE Str_Code = ".$Str_Code);
                while(list($Col_Code, $Col_Type, $Col_Must, $Lst_Code) = mysql_fetch_row($r)) {
                    if($Col_Type == 1) {
                        $Col_Value = str_replace("\"", "&quot;", @$_REQUEST['Ite_Col'.$Col_Code]);
                    } elseif ($Col_Type == 2) {
                        $Col_Value = nl2br($_REQUEST['Ite_Col'.$Col_Code]);
                    } elseif ($Col_Type == 6) {
                        $Col_Value = removeStyles($_REQUEST['Ite_Col'.$Col_Code]);
                    } elseif ($Col_Type != 31 && $Col_Type != 4 && $Col_Type != 7 && $Col_Type != 8 && $Col_Type != 9) {
                        $Col_Value = @$_REQUEST['Ite_Col'.$Col_Code];
                    } else {
                        $Col_Value = @$_REQUEST['Ite_Col'.$Col_Code];
                    }
                    
                    if($Col_Type == 31) {
                        DB::Query("REPLACE INTO contentData3 VALUES ($Ite_Code, $Col_Code, '".$Col_Value."')");
                    } elseif($Col_Type == 4) {
                        DB::Query("DELETE FROM contentData4 WHERE Ite_Code = $Ite_Code AND Col_Code = $Col_Code");
                        if(is_array($Col_Value)) {
                            foreach($Col_Value as $data) {
                                DB::Query("INSERT INTO contentData4 VALUES ($Ite_Code, $Col_Code, $data)");
                            }
                        }
                    } elseif($Col_Type == 5) {
                        if($_FILES['Ite_Col'.$Col_Code]['size'])
                            UploadFile('contentData5',$Ite_Code, $Col_Code, $_FILES['Ite_Col'.$Col_Code]['name'], $_FILES['Ite_Col'.$Col_Code]['tmp_name']);
                    } elseif($Col_Type == 7) {
                        if($_FILES['Ite_Col'.$Col_Code]) {
                            $Col_Value = array();
                            $Col_Value = $_FILES['Ite_Col'.$Col_Code];
                            foreach($Col_Value['name'] as $k => $v) {
                                if($Col_Value['size'][$k] > 0)
                                    UploadMultiFile("contentData7", $Ite_Code, $Col_Code, $Col_Value['name'][$k], $Col_Value['tmp_name'][$k], $k);
                            }
                        }
                        $Col_Value = @$_REQUEST['Ite_Col'.$Col_Code];
                        if(is_array($Col_Value)) {
                            foreach($Col_Value as $key => $data) {
                                $data = ($data != "") ? "'$data'" : "NULL";
                                DB::Query("UPDATE contentData7 SET Data_Desc = $data WHERE Ite_Code = $Ite_Code AND Col_Code = $Col_Code AND Cnt = $key");
                            }
                        }
                    } elseif($Col_Type == 8 || $Col_Type == 9) {
                        DB::Query("DELETE FROM contentData$Col_Type WHERE Ite_Code = $Ite_Code AND Col_Code = $Col_Code");
                        if(is_array($Col_Value)) {
                            foreach($Col_Value as $key => $data) {
                                if($data != "") {
                                    DB::Query("INSERT INTO contentData$Col_Type VALUES ($Ite_Code, $Col_Code, '$data', $key)");
                                }
                            }
                        }
                    } else {
                        DB::Query("REPLACE INTO contentData$Col_Type VALUES ($Ite_Code, $Col_Code, '".mysql_real_escape_string($Col_Value)."')");
                    }
                }
            }
        }

        echo "
          <script language\"JavaScript\">
            if(!window.opener) {
              win = window.open('ite_edit.php?Ite_Code=$Ite_Code');
              window.location = 'index.php?Sec_Code=$Sec_Code';
              win.focus();
            } else {
              window.opener.location='index.php?Sec_Code=$Sec_Code';
              window.location='ite_edit.php?Ite_Code=$Ite_Code';
            }
          </script>
        ";

    }

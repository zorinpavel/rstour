<?php

    require(ENGINE_PATH."/design.php");

    function view_quote($str) {
        $str = preg_replace("/\"/i", "&quot;", $str);
        $str = preg_replace("/\<br \/\>/i", "", $str);
        return $str;
    }

    
    function UploadFile($Table, $Ite_Code, $Col_Code, $Name, $TmpFile, $CopyDir="data") {
        $query = "SELECT Data FROM $Table WHERE Ite_Code=$Ite_Code AND Col_Code=$Col_Code";
        $result = db_query($query);
        $OldFile = mysql_num_rows($result);
        if($OldFile) {
            list($OldName) = mysql_fetch_row($result);
        }

        if($Name != "none" && $Name != "") {
            //составим имя
            preg_match("/\.(\w+)$/", $Name, $Extension);
            $filename = "/$CopyDir/$Ite_Code.$Col_Code.".Cyr2Lat(str_replace($Extension[1], "", $Name)).".".$Extension[1];
            
            //предварительно удалим то что было
            move_uploaded_file($TmpFile, CLIENT_PATH.$filename);
            db_query("REPLACE INTO $Table VALUES ($Ite_Code, $Col_Code, '$filename')");
        }

        if($OldFile && file_exists(CLIENT_PATH.$OldName)) {
            @unlink(CLIENT_PATH.$OldName);
            if(class_exists("classActualFile")) {
                $ACF = _autoload("classActualFile");
                $ACF->Expire($OldName);
            }
        }
    }

    
    function UploadMultiFile($Table, $Ite_Code, $Col_Code, $Name, $TmpFile, $Key = 0, $CopyDir = "data") {
        if (($Name!="none") && ($Name!="")) {
            //составим имя
            preg_match("/\.(\w+)$/", $Name, $Extension);
            $filename = "/$CopyDir/$Ite_Code.$Col_Code.".Cyr2Lat(str_replace($Extension[1], "", $Name)).".".$Extension[1];

            move_uploaded_file($TmpFile, CLIENT_PATH.$filename);
            db_query("INSERT INTO $Table SET Ite_Code = $Ite_Code, Col_Code = $Col_Code, Data = '$filename', Cnt = $Key");
        }
    }

    
    function removeStyles($Data) {
        if(defined("REMOVE_STYLES") && REMOVE_STYLES) {
            $Data = preg_replace("/(<[td|table]+.*)( style=\".*?\")/i", "$1", $Data);
            $Data = preg_replace("/(<[td|table]+.*)( width=\"\d+(px)?\")/i", "$1", $Data);
            $Data = preg_replace("/(<[td|table]+.*)( border=\"\d+(px)?\")/i", "$1", $Data);
        }
        return $Data;
    }
  

<?php
    
    include(ENGINE_PATH."/functions.php");

    if($_REQUEST['Refresh']) {
        foreach($_REQUEST['Ite_Code'] as $i) {
            $Ite_Code = $_REQUEST['Ite_Code'][$i];
            if(!$Ite_Code)
                continue;
            
            ${Ite_Priority.$i} = (int)$_REQUEST['Ite_Priority'][$i];
            
            ${Dig_BDay.$i}     = (int)$_REQUEST['Dig_BDay'][$i];
            ${Dig_BMonth.$i}   = (int)$_REQUEST['Dig_BMonth'][$i];
            ${Dig_BYear.$i}    = (int)$_REQUEST['Dig_BYear'][$i];
            
            ${Dig_EDay.$i}     = (int)$_REQUEST['Dig_EDay'][$i];
            ${Dig_EMonth.$i}   = (int)$_REQUEST['Dig_EMonth'][$i];
            ${Dig_EYear.$i}    = (int)$_REQUEST['Dig_EYear'][$i];
            
            $query = "UPDATE contentItems SET ".
                " Ite_State      = ".$_REQUEST['Ite_State'][$i].
                ",Ite_BDate      = '".fYearMonthDay2Str(${Dig_BDay.$i}, ${Dig_BMonth.$i}, ${Dig_BYear.$i})."'".
                ",Ite_EDate      = '".fYearMonthDay2Str(${Dig_EDay.$i}, ${Dig_EMonth.$i}, ${Dig_EYear.$i})."'".
                ",Ite_Priority   = ".${Ite_Priority.$i}.
                " WHERE Ite_Code = $Ite_Code";
            $result = DB::Query($query);
            
            @unlink(CACHE_PATH."/".$Ite_Code.".itemvars");
            @unlink(CACHE_PATH."/".$Ite_Code.".itemvars.part");
            foreach($TemplateSets as $Key => $Value) {
                @unlink(CACHE_PATH."/".$Ite_Code.".itemvars_".$Key);
                @unlink(CACHE_PATH."/".$Ite_Code.".itemvars.part_".$Key);
            }
        }
    }
    
    if(!$FilterKind)
        $FilterKind = "Valid";
    
    if($FilterKind == "Valid")
        $CheckedValid = " CHECKED";
    elseif($FilterKind == "Period")
        $CheckedPeriod = " CHECKED";
    
    $BDay = date("d", time());
    $BMonth = date("m", time());
    $BYear = date("Y", time());
    
    if(!$Dig_BDay) {
        $Dig_BDay = $BDay; }
    if(!$Dig_BMonth) {
        $Dig_BMonth = $BMonth; }
    if(!$Dig_BYear) {
        $Dig_BYear = $BYear; }
    if(!$Dig_EDay) {
        $Dig_EDay = $BDay; }
    if(!$Dig_EMonth) {
        $Dig_EMonth = $BMonth; }
    if(!$Dig_EYear) {
        $Dig_EYear = $BYear; }
    
    $contentOrder = "Ite_State = 1, Ite_Priority DESC, Ite_BDate DESC, Ite_Code DESC";
    if($FilterKind == "Valid") {
        $Cond = "WHERE Ite_EDate >= curdate()";
    } elseif($FilterKind == "Period") {
        $Cond = "WHERE Ite_BDate <= '".fYearMonthDay2Str($Dig_EDay, $Dig_EMonth, $Dig_EYear)."' AND Ite_EDate >= '".fYearMonthDay2Str($Dig_BDay, $Dig_BMonth, $Dig_BYear)."'";
    }
    
    $Sec_Code = (int)$_REQUEST['Sec_Code'];
    
    $Cond .= " AND Sec_Code = $Sec_Code";
    $Cond = "WHERE (Sec_Code = $Sec_Code OR isec.Section = $Sec_Code)";
    
    include(ENGINE_PATH."/templates/i_top.php");
    
    dPrintDigests($Cond, $contentOrder);


<?php
    
    define("FROM", "с");
    define("UNTIL", "по");
    define("CLOSE_WINDOW", "Закрыть окно");
    define("REMARK", "Примечание");
    
    define("LIST_ADD_ISSUE", "Добавить");
    define("LIST_REFRESH_LIST", "Обновить список");
    define("LIST_SHOW_LIST", "Показать список");
    
    define("LIST_SHOW_VALID", "Показывать активные на сегодня");
    define("LIST_SHOW_PERIOD", "Показывать за период");
    
    define("LIST_ISSUE_HEADER", "Заголовок");
    define("LIST_ISSUE_PERIOD", "Период показа (дд/мм/гггг)");
    define("LIST_ISSUE_PRIORITY", "Приоритет вывода");
    define("LIST_ISSUE_STATE", "Текущее состояние");
    
    define("LIST_REMARK_TODAY", "показывается сегодня");
    define("LIST_REMARK_FUTURE", "показывается в будущем");
    define("LIST_REMARK_HIDDEN", "не показывается");
    
    define("ISSUE_STATE_NORMAL", "нормальное");
    define("ISSUE_STATE_ARCHIVE", "архивное");
    
    define("ISSUE_REFRESH_1", "Сохранить");
    define("ISSUE_REFRESH_2", "Изменить");
    define("ISSUE_VIEW", "Просмотреть");
    define("ISSUE_DELETE", "Удалить");
    
    define("ISSUE_HEADER", "Заголовок");
    define("ISSUE_STRUCTURE", "Структура контента");
    define("ISSUE_PRIORITY", "Приоритет вывода");
    define("ISSUE_PERIOD_BEGIN", "Дата начала показа");
    define("ISSUE_PERIOD_END", "Дата окончания показа");
    
    define("ISSUE_REMARK", "обязательное поле");

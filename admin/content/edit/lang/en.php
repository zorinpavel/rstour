<?php
    
    define("FROM", "from");
    define("UNTIL", "until");
    define("CLOSE_WINDOW", "Close window");
    define("REMARK", "Remark");
    
    define("LIST_ADD_ISSUE", "Add issue");
    define("LIST_REFRESH_LIST", "Update list");
    define("LIST_SHOW_LIST", "Show list");
    
    define("LIST_SHOW_VALID", "Show&nbsp;active&nbsp;for&nbsp;today");
    define("LIST_SHOW_PERIOD", "Show&nbsp;for&nbsp;the&nbsp;period");
    
    define("LIST_ISSUE_HEADER", "Issue header");
    define("LIST_ISSUE_PERIOD", "Period for display");
    define("LIST_ISSUE_PRIORITY", "Display priority");
    define("LIST_ISSUE_STATE", "Current state");
    
    define("LIST_REMARK_TODAY", "visible today");
    define("LIST_REMARK_FUTURE", "will be visible in the future");
    define("LIST_REMARK_HIDDEN", "hidden");
    
    define("ISSUE_STATE_NORMAL", "normal");
    define("ISSUE_STATE_ARCHIVE", "archive");
    
    define("ISSUE_REFRESH_1", "Save");
    define("ISSUE_REFRESH_2", "Update");
    define("ISSUE_VIEW", "View");
    define("ISSUE_DELETE", "Delete");
    
    define("ISSUE_HEADER", "Issue&nbsp;header");
    define("ISSUE_STRUCTURE", "Content structure");
    define("ISSUE_PRIORITY", "Display priority");
    define("ISSUE_PERIOD_BEGIN", "Beginning date");
    define("ISSUE_PERIOD_END", "Ending date");
    
    define("ISSUE_REMARK", "necessarily field");

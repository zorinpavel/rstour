<table width="100%" cellspacing="0" cellpadding="3" align="center" class="border" bgcolor="#E5FFE5">
    <tr>
        <td style="width: 30%;" align="right">ЧПУ:</td>
        <td>
            <input type="text" name="Ite_Sef" id="Ite_Sef" value="<? echo $Ite_Sef; ?>" onkeyup="makeSef(this.value, 100);return false;" placeholder="" autocomplete="off" style="width:60%;margin-bottom:3px;">.html
            <br>
            <button class="blue" onclick="makeSef(false);return false;">Из заголовка</button>
        </td>
    </tr>
</table>

<script type="text/javascript">
    function makeSef(Value, keyTimeout = 0) {
        Value = Value || document.getElementById("Ite_Header").value;
        var Language = document.getElementById("Ite_Language") ? document.getElementById("Ite_Language").value : false;
        
        clearTimeout(keyTimeout);
        keyTimeout = setTimeout(function() {
            Ajax.Get("/ajax.php", {
                c: 'sefRouter',
                a: 'GetRoute',
                Route: Value,
                Language: Language
            }, fillValue, false, false, false);
        }, keyTimeout);
    }
    
    function fillValue() {
        var Ite_Sef = document.getElementById("Ite_Sef");
        if(this.responseJS.Error) {
            Ite_Sef.style.backgroundColor = "rgba(255, 0, 0, 0.5)";
        } else {
            Ite_Sef.style.backgroundColor = false;
            Ite_Sef.value = this.responseJS.Route;
        }
    }
</script>
<table width="100%" cellspacing="0" cellpadding="3" align="center" class="border" bgcolor="#E5FFE5">
    <tr>
        <td align="right" width="15%"><? echo ISSUE_STRUCTURE;?>:&nbsp;</td>
        <td width="45%">
            <input type="hidden" name="ChangeStructure" value="0"><?
                $ChangeTypeAlert = $Ite_Code ? "if(confirm('Осторожно, при смене типа объекта будут утеряны данные старого типа. Вы действительно отите сменить тип?')) { this.form.elements['ChangeStructure'].value=1;this.form.submit(); } else { this.selectedIndex = window.SI; }" : "this.form.submit();";
                $InputContentStructure = new InputContentStructureDBSelect("Str_Code", $Str_Code, $ChangeTypeAlert, "");
                $InputContentStructure->dPrint();
            ?>
            <script>window.SI = window.document.forms['DigEdit'].elements['Str_Code'].selectedIndex;</script>
        </td>
        <? if(DEFAULT_LANGUAGE) { ?>
            <td align="right" width="20%">Язык:&nbsp;</td>
            <td width="20%"><?
                $InputLanguage = new InputContentLanguageSelect("Ite_Language", $Ite_Language, "", "");
                $InputLanguage->dPrint();
            ?></td>
        <? } else { ?>
            <td width="40%" colspan="2">&nbsp;</td>
        <? } ?>
    </tr>
    <tr>
        <td align="right" width="15%">Основной раздел:&nbsp;</td>
        <td width="45%"><?
                $InputSectionsTree = new InputSectionsTree("Sec_Code", $Sec_Code, "", "width:100%");
                $InputSectionsTree->dPrint();
            ?></td>
        <td align="right" width="20%"><? echo LIST_ISSUE_STATE;?>:&nbsp;</td>
        <td width="20%">
            <select name="Ite_State">
                <option value="0"><? echo ISSUE_STATE_NORMAL; ?></option>
                <option value="1"<? echo ($Ite_State ? " selected" : ""); ?>><? echo ISSUE_STATE_ARCHIVE; ?></option>
            </select>
        </td>
    </tr>
    <tr>
        <td align="right" width="15%" valign=top>Дополнительные разделы:&nbsp;</td>
        <td width="45%"><?
                $InputAdditionalSections = new InputAdditionalSections("AddSections", $Ite_Code, "", "width:100%");
                $InputAdditionalSections->dPrint();
            ?></td>
        <td align="right" width="20%"><? echo LIST_ISSUE_PRIORITY;?>:&nbsp;</td>
        <td width="20%">
            <input maxlength="4" name="Ite_Priority" size="1" value="<? echo $Ite_Priority; ?>">
        </td>
    </tr>
    <tr>
        <td align="right" width="15%"><? echo LIST_ISSUE_PERIOD; ?>:&nbsp;</td>
        <td width="45%">
            
            <? echo FROM; ?>&nbsp;
            <input maxlength="2" name="Dig_BDay" size="1" value="<? echo $Dig_BDay; ?>">
            <input maxlength="2" name="Dig_BMonth" size="1" value="<? echo $Dig_BMonth; ?>">
            <input maxlength="4" name="Dig_BYear" size="3" value="<? echo $Dig_BYear; ?>">
            &nbsp;&nbsp;<? echo UNTIL; ?>&nbsp;
            <input maxlength="2" name="Dig_EDay" size="1" value="<? echo $Dig_EDay; ?>">
            <input maxlength="2" name="Dig_EMonth" size="1" value="<? echo $Dig_EMonth; ?>">
            <input maxlength="4" name="Dig_EYear" size="3" value="<? echo $Dig_EYear; ?>">
        </td>
        <td align="right" width="20%" valign=top>&nbsp;</td>
        <td width="20%">&nbsp;</td>
    </tr>
    <? if((int)$Dig_Date !== 0) { ?>
        <tr>
            <td align="right" width="15%">Дата создания / изменения:&nbsp;</td>
            <td width="45%">
                <? echo $Dig_Date; ?>
                <?
                    if((int)$Dig_Date_Change !== 0 && $Dig_Date_Change != $Dig_Date)
                        echo " / ".$Dig_Date_Change;
                ?>
            </td>
            <td align="right" width="20%" valign=top>&nbsp;</td>
            <td width="20%">&nbsp;</td>
        </tr>
    <? } ?>
</table>


<? if(CONTENT_USERS) { ?>
    <br>
    <?php include(ENGINE_PATH."/templates/e_user.php"); ?>
<? } ?>

<? if(CONTENT_RELATED) { ?>
    <br>
    <?php include(ENGINE_PATH."/templates/e_relate.php"); ?>
<? } ?>

<? if(CONTENT_TAGS) { ?>
    <br>
    <?php include(ENGINE_PATH."/templates/e_tags.php"); ?>
<? } ?>

<? if(SEF_URL) { ?>
    <br>
    <?php include(ENGINE_PATH."/templates/e_sef.php"); ?>
<? } ?>

<br>

<table width="100%" border="0" cellspacing="0" cellpadding="3" align="center" class="border">
    <tr>
        <td><input type="submit" value="<? echo ISSUE_REFRESH_1; ?>" class="green"></td>
        <td width="100%">&nbsp;</td>
    </tr>
</table>

<br>

<table width="100%" border="0" cellspacing="0" cellpadding="3" align="center">
    <tr>
        <td width="10%">
            <b><? echo ISSUE_HEADER; ?></b><span style="color:red">*</span>
        </td>
        <td width="90%">
            <input name="Ite_Header" id="Ite_Header" class="Title" style="width:95%;font-size:14pt" value="<? echo view_quote($Ite_Header); ?>">
        </td>
    </tr>

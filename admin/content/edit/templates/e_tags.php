<style type="text/css">
    .contentTags {}
    .contentTags UL {
        margin: 0;
        padding: 0;
    }
    .contentTags UL LI > SPAN {
        background-color: rgba(192, 192, 192, 0.5);
        border: 1px solid #c0c0c0;
        border-radius: 2px;
        color: #3f7d96;
        display: inline-block;
        font-size: 80%;
        height: 0.8em;
        margin-left: 0.4em;
        padding: 2px;
        text-align: center;
        vertical-align: middle;
        width: 0.8em;
        cursor: pointer;
    }
    .contentTags UL LI > SPAN:hover {
        color: red;
    }
    .contentTags > UL > LI {
        background-color: rgba(255, 255, 255, 0.8);
        border: 1px solid #c0c0c0;
        border-radius: 3px;
        display: inline-block;
        line-height: 1;
        padding: 4px 4px 4px 8px;
        vertical-align: middle;
        margin: 0 2px;
    }
    .contentTags > UL > LI:last-child {
        display: inline-block;
        padding: 0;
        background: none;
        border: 0 none;
        position: relative;
    }
    .contentTags > UL > LI:last-child > SPAN {
        display: none;
    }
    .tagsList {
        background-color: #ffffff;
        border: 1px solid #c0c0c0;
        position: absolute;
        top: 1.7em;
        width: 200px;
    }
    .tagsList > A {
        cursor: pointer;
        display: block;
        line-height: 1.5;
        padding: 3px 6px;
    }
    input[name="Ite_Tags"] {
        display: none;
    }
</style>

<table width="70%" cellspacing="0" cellpadding="3" align="center" class="border" bgcolor="#E5FFE5">
    <tr>
        <td style="width: 30%;" align="right">Теги:</td>
        <td>

            <div class="contentTags">
                <ul><li><input type="text" name="tempTag" placeholder="добавить..." onkeyup="SearchValues(this);" id="tempTag" style="width:194px;" autocomplete="off"></li></ul>
                <input type="text" name="Ite_Tags" value="" id="tags">
            </div>

        </td>
    </tr>
</table>

<script type="text/javascript">
    var keyTimeout = null;

    var tagsList = document.getElementById("tagsList");
    var tempTag = document.getElementById("tempTag");
    var liTempTag = tempTag.parentNode;
    var Ul = liTempTag.parentNode;

    function SearchValues(inputObj) {
        clearTimeout(keyTimeout);
        keyTimeout = setTimeout(function() {
            var Data = {
                c: 'classTags',
                a: 'searchByValue',
                value: inputObj.value
            };
            if(inputObj.value.indexOf(",") > 0) {
                var Str = inputObj.value.substr(-inputObj.value.length, inputObj.value.length-1);
                addTag({ Name: Str });
                tempTag.value = "";
                addTagToString(Str);
            } else
                Ajax.Send("GET", "/ajax.php", Data, false, false, "none", RequestBack);
        }, 500);
    }

    function RequestBack() {
        if(!tagsList) {
            tagsList = document.createElement("div");
            tagsList.id = "tagsList";
            tagsList.className = "tagsList";
            liTempTag.appendChild(tagsList);
        } else {
            while(tagsList.firstChild) {
                tagsList.removeChild(tagsList.firstChild);
            }
        }
        if(!this.responseJS.Error) {
            for(var i in this.responseJS.Results) {
                addTagChoice(this.responseJS.Results[i]);
            }
        }
    }

    function addTagChoice(Tag) {
        var A = document.createElement("a");
        var textNode = document.createTextNode(Tag.Name + " (" + Tag.Tag_Count + ")");
        A.appendChild(textNode);
        A.onclick = function() {
            addTag(Tag);
            liTempTag.removeChild(tagsList);
            tagsList = null;
            tempTag.value = "";

        };
        tagsList.appendChild(A);
    }

    function addTag(Tag) {
        var Li = document.createElement("li");
        Li.appendChild(document.createTextNode(Tag.Name));
        var Span = document.createElement("span");
        Span.appendChild(document.createTextNode("X"));
        Span.onclick = function() { removeTag(Span, Tag.Name); };
        Li.appendChild(Span);
        liTempTag.parentNode.insertBefore(Li, liTempTag);
        addTagToString(Tag.Name);
    }

    function addTagToString(tagName) {
        var tagsInput = document.getElementById("tags");
        tagsInput.value = tagsInput.value != "" ? tagsInput.value + "," + tagName : tagName;
    }

    function removeTag(Span, tagName) {
        var Li = Span.parentNode;
        Ul.removeChild(Li);
        var tagsInput = document.getElementById("tags");
        var tagsArray = tagsInput.value.split(",");
        var tagsStr = "";
        for(var i in tagsArray) {
            if(tagName == tagsArray[i])
                tagsArray.splice(i, 1);
        }
        tagsInput.value = tagsArray.join(",");
    }
</script>

<?
    $classTags = _autoload("classTags");
    $className = CONTENT_TAGS;
    $Ite_Tags = $classTags->GetTags($className, $Ite_Code);
    foreach($Ite_Tags as $Tag_Code => $Tag) {
        echo "<script>addTag({ Name: '".$Tag['Tag_Name']."' });</script>";
    }

?>

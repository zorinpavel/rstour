<?php
    
    $Ite_Code = (int)$_REQUEST['Ite_Code'];
    
    $Item = DB::selectArray("SELECT * FROM contentItems WHERE Ite_Code = $Ite_Code");
    $Columns = DB::selectArray("SELECT * FROM contentColumns WHERE Str_Code = ".$Item['Str_Code'], "Col_Code");

    foreach($Columns AS $Col_Code => $Col) {
        if($Col['Col_Type'] == 5) {
            $r = DB::Query("SELECT Data from contentData5 where Ite_Code = $Ite_Code");
            while(list($file) = mysql_fetch_row($r)) {
                if($file and file_exists(CLIENT_PATH.$file)) {
                    @unlink(CLIENT_PATH.$file);
                    if(class_exists("classActualFile")) {
                        $ACF = _autoload("classActualFile");
                        $ACF->Expire($file);
                    }
                }
            }
        }
        if($Col['Col_Type'] == 7) {
            $r = DB::Query("SELECT Data from contentData7 where Ite_Code = $Ite_Code");
            while(list($file) = mysql_fetch_row($r)) {
                if($file and file_exists(CLIENT_PATH.$file)) {
                    @unlink(CLIENT_PATH.$file);
                    if(class_exists("classActualFile")) {
                        $ACF = _autoload("classActualFile");
                        $ACF->Expire($file);
                    }
                }
            }
        }
        DB::Query("DELETE FROM contentData".$Col['Col_Type']." WHERE Ite_Code = $Ite_Code");
    }
    
    DB::Query("DELETE FROM contentItems WHERE Ite_Code = $Ite_Code");
    DB::Query("DELETE FROM contentRelated WHERE Ite_Code = $Ite_Code");
    DB::Query("DELETE FROM contentItemsSections WHERE Item = $Ite_Code");
    
    if(CONTENT_TAGS) {
        $classTags = _autoload("classTags");
        $classTags->RemoveAll(CONTENT_TAGS, $Ite_Code);
    }
    
    if(SEF_URL)
        sefRouter::Expire("classContent", $Ite_Code);

    @unlink(CACHE_PATH."/".$Ite_Code.".itemvars");
    @unlink(CACHE_PATH."/".$Ite_Code.".itemvars.part");
    
    echo "
        <script language\"JavaScript\">
          if(!window.opener) {
            window.location = 'index.php?Sec_Code=$Sec_Code';
          } else {
            window.opener.location = 'index.php?Sec_Code=$Sec_Code';
          }
          window.close(1);
        </script>
    ";

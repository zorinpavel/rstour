<?php
    
    $TodayColor  = "#E5FFE5";
    $FutureColor = "#ffffcc";
    $ArchColor   = "#EFEFEF";
    $BlankColor  = "#e0e0e0";
    $AlertColor  = "#fee6e6";
    
    define("NEWS_EDIT_WARNING_DAYS", 3);
    
    $dbStructures = DB::selectArray("SELECT Str_Code, Str_Name FROM contentStructure", "Str_Code");
    foreach($dbStructures as $Str_Code => $Structure)
        $Structures[$Str_Code] = $Structure['Str_Name'];

    function GetMaxEmptyNumber($i) {
        $EmptyQua = 5;
        if (round($i/$EmptyQua) != ($i/$EmptyQua)) {
            $MaxQua = ceil($i/$EmptyQua)*$EmptyQua;
        }
        else {
            $MaxQua = (($i/$EmptyQua) + 1)*$EmptyQua;
        }
        return $MaxQua;
    }
    
    function fInPeriod($BDay,$BMonth,$BYear,$EDay,$EMonth,$EYear) {
        // Проверяет находится ли сегодняшняя дата в промежутке от B до E
        $mk0 = time();
        $mk1 = mktime(0,0,0,$BMonth,$BDay,$BYear);
        $mk2 = mktime(23,59,59,$EMonth,$EDay,$EYear);
        if (($mk1 <= $mk0) && ($mk2 >= $mk0)) {
            return 1;
        }
        return 0;
    }
    
    function LZero($Str, $Len) {
        return substr("00000".$Str, -$Len, $Len);
    }
    
    function fDayMonthYear2Str($Day, $Month, $Year) {
        // Возвращает дату в формате DD.MM.YYYY
        return LZero($Day, 2).".".LZero($Month, 2).".".LZero($Year, 4);
    }
    
    function fYearMonthDay2Str($Day, $Month, $Year) {
        // Возвращает дату в формате YYYY-MM-DD
        return LZero($Year, 4)."-".LZero($Month, 2)."-".LZero($Day, 2);
    }
    
    function dPrintRow($Ite_Code, $Ite_Header, $Dig_BDay, $Dig_BMonth, $Dig_BYear, $Dig_EDay, $Dig_EMonth, $Dig_EYear, $Ite_Priority, $Ite_State, $Str_Code, $Ite_Language) {
        global $TodayColor, $FutureColor, $ArchColor, $BlankColor, $AlertColor, $Structures;
        
        $Dig_BDay = substr("0".$Dig_BDay, -2, 2);
        $Dig_BMonth = substr("0".$Dig_BMonth, -2, 2);
        $Dig_EDay = substr("0".$Dig_EDay, -2, 2);
        $Dig_EMonth = substr("0".$Dig_EMonth, -2, 2);
        
        if (!$Ite_State) {
            if (fInPeriod($Dig_BDay, $Dig_BMonth, $Dig_BYear, $Dig_EDay, $Dig_EMonth, $Dig_EYear)) {
                $TRColor = $TodayColor;
            } elseif (mktime(0, 0, 0, $Dig_BMonth, $Dig_BDay, $Dig_BYear) > time()) {
                $TRColor = $FutureColor;
            } else {
                $TRColor = $ArchColor;
            }
        } else {
            $TRColor = $ArchColor;
        }
        
        if(!$Ite_State)
            $Selected0 = "SELECTED";
        else
            $Selected1 = "SELECTED";
        
        $D = date("d");
        $M = date("m");
        $Y = date("Y");
        $Today = fYearMonthDay2Str($D, $M, $Y);
        
        $WarningTime = time() + (NEWS_EDIT_WARNING_DAYS * 24 * 60 * 60);
        $D = date("d", $WarningTime);
        $M = date("m", $WarningTime);
        $Y = date("Y", $WarningTime);
        $WarningDate = fYearMonthDay2Str($D, $M, $Y);
        
        $IssueBDate = fYearMonthDay2Str($Dig_BDay, $Dig_BMonth, $Dig_BYear);
        $IssueEDate = fYearMonthDay2Str($Dig_EDay, $Dig_EMonth, $Dig_EYear);
        
        if ($Today >= $IssueBDate && $WarningDate >= $IssueEDate && $IssueEDate >= $Today && !$Ite_State) {
            $FontB = "<font color=\"red\"><small>Элемент скоро будет скрыт !!!</small><br>";
            $FontE = "</font>";
            $TRColor = $AlertColor;
        }
        include(ENGINE_PATH."/templates/i_line.php");
    }
    
    function dPrintSectionRow($Section, &$Issues, &$OldIssues, &$OldDates) {
        global $TodayColor, $FutureColor, $ArchColor, $BlankColor, $AlertColor;
        
        $TRColor = ($Section['Sec_Visible'] ? $TodayColor : $ArchColor);
        include(ENGINE_PATH."/templates/i_section_line.php");
    }
    
    function GetSectionIssuesInfo($SecArray) {
        $Issues = array();
        $OldIssues = array();
        $OldDates = array();
        $IssuesTmp = array();
        $OldIssuesTmp = array();
        $OldDatesTmp = array();
        
        $SecLinks = array();
        
        $SecCond = "";
        foreach($SecArray as $Code => $Section) {
            $SecLinks[$Section['Sec_Code']] = $Section['Sec_Code'];
            $SecCond .= $Section['Sec_Code'].", ";
        }
        $SecCond = substr($SecCond, 0, -2);

        // TODO сделать рекурсивно по всем вложенным разделам
        if($SecCond) {
            $Sections = DB::selectArray("SELECT Sec_Code, Sec_Parent FROM Sections WHERE Sec_Parent IN ($SecCond)", "Sec_Code");
            $SecCond2 = "";
            foreach($Sections as $Sec_Code => $Section) {
                $SecLinks[$Sec_Code] = $SecLinks[$Section['Sec_Parent']];
                $SecCond2 .= "$Sec_Code, ";
            }
            if($SecCond2)
                $SecCond = $SecCond.", ".substr($SecCond2, 0, -2);

            if($SecCond) {
                $IssuesTmp = DB::selectArray("SELECT Sec_Code, COUNT(*) AS Cnt FROM contentItems WHERE Sec_Code IN ($SecCond) GROUP BY Sec_Code", "Sec_Code");

                $D = date("d");
                $M = date("m");
                $Y = date("Y");
                $Today = fYearMonthDay2Str($D, $M, $Y);

                $WarningTime = time() + (NEWS_EDIT_WARNING_DAYS * 24 * 60 * 60);
                $D = date("d", $WarningTime);
                $M = date("m", $WarningTime);
                $Y = date("Y", $WarningTime);
                $WarningDate = fYearMonthDay2Str($D, $M, $Y);

                $OldIssuesTmp = DB::selectArray("SELECT Sec_Code, COUNT(*) AS Cnt, MIN(Ite_EDate) FROM contentItems WHERE Sec_Code IN ($SecCond) AND Ite_BDate<'$Today' AND Ite_EDate<'$WarningDate' AND Ite_State=0 GROUP BY Sec_Code", "Sec_Code");
                $OldDatesTmp = $OldIssuesTmp;

                foreach($IssuesTmp as $Sec_Code => $Cnt)
                    $Issues[$SecLinks[$Sec_Code]] += $Cnt['Cnt'];
                foreach($OldIssuesTmp as $Sec_Code => $Cnt) {
                    $OldIssues[$SecLinks[$Sec_Code]] += $Cnt['Cnt'];

                    $D1 = &$OldDates[$SecLinks[$Sec_Code]]['Ite_EDate'];
                    if($D1)
                        $D1 = ($D1 > $OldDatesTmp[$Sec_Code]['Ite_EDate'] ? $OldDatesTmp[$Sec_Code]['Ite_EDate'] : $D1); else
                        $D1 = $OldDatesTmp[$Sec_Code]['Ite_EDate'];
                }
            }
        }

        return array($Issues, $OldIssues, $OldDates);
    }
    
    function dPrintDigests($Cond, $Order) {
        // Выводит элементы показанные за период
        global $Digest, $DigText, $Sec_Code, $Part, $Structures;
        
        $LangCond = "";
        if($_COOKIE['Lang'])
            $LangCond = " AND contentItems.Ite_Language = '".$_COOKIE['Lang']."'";
        if($_COOKIE['Lang'] == "all")
            $LangCond = "";
        if(DEFAULT_LANGUAGE && $_COOKIE['Lang'] == DEFAULT_LANGUAGE)
            $LangCond = " AND (contentItems.Ite_Language IS NULL OR contentItems.Ite_Language = '' OR contentItems.Ite_Language = '".DEFAULT_LANGUAGE."')";
        
        $Sections = DB::selectArray("SELECT Sec_Code, Sec_Name, Sec_Visible, Sec_Order FROM Sections WHERE Sec_Parent = $Sec_Code ORDER BY Sec_Order DESC, Sec_Code", "Sec_Code");
        list($Issues, $OldIssues, $OldDates) = GetSectionIssuesInfo($Sections);

        $Cnt = DB::selectValue("SELECT COUNT(ci.Ite_Code) FROM contentItems AS ci LEFT JOIN contentItemsSections AS isec ON ci.Ite_Code = isec.Item $Cond");

        $PartSize = 50;
        $Part = (int)$_REQUEST['Part'];
        $Part = ($Part < 1 ? 1 : $Part);
        $Part = ($Part * $PartSize > $Cnt ? (int)($Cnt/$PartSize)+1 : $Part);
        $First = (($Part-1)*$PartSize);
        
        $query = "
            SELECT DISTINCT Ite_Code,
               Ite_Header,
               Ite_Language,
               DATE_FORMAT(Ite_BDate, '%d') AS Dig_BDay,
               DATE_FORMAT(Ite_BDate, '%m') AS Dig_BMonth,
               DATE_FORMAT(Ite_BDate, '%Y') AS Dig_BYear,
               DATE_FORMAT(Ite_EDate, '%d') AS Dig_EDay,
               DATE_FORMAT(Ite_EDate, '%m') AS Dig_EMonth,
               DATE_FORMAT(Ite_EDate, '%Y') AS Dig_EYear,
               Ite_Priority,
               Ite_State,
               Str_Code,
               Ite_Time, Time_Change
            FROM contentItems LEFT JOIN contentItemsSections AS isec ON contentItems.Ite_Code=isec.Item
              $Cond $LangCond
            ORDER BY $Order LIMIT $First, $PartSize";
        
        //  echo "$query<br>";
        
        $Items = DB::selectArray($query, "Ite_Code");

        foreach($Sections as $Sec_Code => $Section)
            dPrintSectionRow($Section, $Issues, $OldIssues, $OldDates);

        foreach($Items as $Ite_Code => $Item)
            dPrintRow($Ite_Code, $Item['Ite_Header'], $Item['Dig_BDay'], $Item['Dig_BMonth'], $Item['Dig_BYear'], $Item['Dig_EDay'], $Item['Dig_EMonth'], $Item['Dig_EYear'], $Item['Ite_Priority'], $Item['Ite_State'], $Item['Str_Code'], $Item['Ite_Language']);

        echo "<input type=\"hidden\" name=\"Quantity\" value=\"$Cnt\">";
        include(ENGINE_PATH."/templates/i_parts.php");
    }
    
    function dPrintDigestHeader($Ite_Code) {
        // Выводит заголовок новости для редактирования
        global $Digest, $DigText;
        global $Sec_Code;
        
        if ($Ite_Code == 0) {
            $Ite_Code = 0;
            $Ite_Header = "";
            $Dig_BDay = date("d", time());
            $Dig_BMonth = date("m", time());
            $Dig_BYear = date("Y", time());
            $Dig_EDay = date("d", time());
            $Dig_EMonth = date("m", time());
            $Dig_EYear = date("Y", mktime(0, 0, 0, date("m"), date("d"), date("Y")+5));
            $Ite_Priority = 0;
            $Ite_State = 0;
            $Str_Code = 0;
        } else {
            $query = "
                SELECT Ite_Header,
                   DATE_FORMAT(Ite_BDate, '%d') AS Dig_BDay,
                   DATE_FORMAT(Ite_BDate, '%m') AS Dig_BMonth,
                   DATE_FORMAT(Ite_BDate, '%Y') AS Dig_BYear,
                   DATE_FORMAT(Ite_EDate, '%d') AS Dig_EDay,
                   DATE_FORMAT(Ite_EDate, '%m') AS Dig_EMonth,
                   DATE_FORMAT(Ite_EDate, '%Y') AS Dig_EYear,
                   Ite_Priority,
                   Ite_State,
                   Str_Code,
                   Ite_Language,
                   Sec_Code,
                   us_id,
                   DATE_FORMAT(Ite_Time,'%d.%m.%Y %H:%i') AS Dig_Date,
                   DATE_FORMAT(Time_Change,'%d.%m.%Y %H:%i') AS Dig_Date_Change
                FROM contentItems
                WHERE Ite_Code = $Ite_Code";
            $Item = DB::selectArray($query);

            extract($Item);
        }
        
        if(SEF_URL && $Ite_Code) {
            $Router = sefRouter::Get("classContent", $Ite_Code);
            if($Router)
                $Ite_Sef = $Router['Url'];
        }
    
        $Str_Code = $Str_Code ?: DB::selectValue("SELECT Str_Code FROM contentStructure ORDER BY Str_Order DESC, Str_Code ASC LIMIT 1");

        include(ENGINE_PATH."/templates/e_top.php");
        include(ENGINE_PATH."/templates/e_header.php");
        
        if($Str_Code) {
            //Получаем параметры структуры и выводим по каждому поле ввода
            $Cols = DB::selectArray("SELECT Col_Code, Col_Header, Col_Type, Col_Must, Lst_Code FROM contentColumns WHERE Str_Code = $Str_Code ORDER BY Col_Order DESC, Col_Code", "Col_Code");

            foreach($Cols as $Col_Code => $Col) {
                extract($Col, EXTR_OVERWRITE);
                if($Col['Col_Type'] == 7 || $Col['Col_Type'] == 8) {
                    $Col_Data = DB::selectArray("SELECT Data, Data_Desc, Cnt FROM contentData".$Col['Col_Type']." WHERE Ite_Code = $Ite_Code AND Col_Code = $Col_Code ORDER BY Cnt", "Cnt");
                } elseif($Col['Col_Type'] == 31) {
                    $Col_Data = DB::selectArray("SELECT Data FROM contentData3 WHERE Ite_Code = $Ite_Code AND Col_Code = $Col_Code");
                } else {
                    $Col_Data = DB::selectArray("SELECT Data FROM contentData".$Col['Col_Type']." WHERE Ite_Code = $Ite_Code AND Col_Code = $Col_Code");
                }
                if(in_array($Col['Col_Type'], array(1, 2, 3, 5, 6, 10, 11, 12))) {
                    $Col_Data = $Col_Data['Data'];
                }
                
                include(ENGINE_PATH."/templates/e_column".$Col['Col_Type'].".php");
            }
        }
        
        include(ENGINE_PATH."/templates/e_bottom.php");
    }


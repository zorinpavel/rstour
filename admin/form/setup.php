<?
    
    class InputFormsSelect extends InputDBSelect {
        function InputFormsSelect($Name, $Value, $onChange="") {
            $this->InputDBSelect($Name, $Value, "formList", "Lst_Code", "Lst_Name", "", "Lst_Order, Lst_Code", "", $onChange);
        }
    }
    
    class ColTypesInputSelect extends InputSelect {
        function ColTypesInputSelect($Name, $Value) {
            $this->Empty=1;
            $Options = array(
                "1" => "Текстовое поле",
                "2" => "Большое текстовое поле",
                "3" => "Список значений",
                "4" => "МультиСписок значений",
                "5" => "Переключатель",
                "6" => "Загрузка файла",
                "7" => "Объединяющие поле",
                "8" => "Hidden",
            );
            $this->InputSelect($Name, $Value, $Options);
        }
    }
    
    class contentListsDBSelect2 extends contentListsDBSelect {
        function dPrint() {
            global $Row;
            
            if ($Row[4] == 3 || $Row[4]==4 || $Row[4]==5) {
                parent::dPrint();
            }
            else {
                echo "&nbsp;";
            }
        }
        
        function contentListsDBSelect2($Name, $Value) {
            parent::contentListsDBSelect($Name, $Value);
        }
    }
    
    class GoColumns extends InputButton {
        function GoColumns($Code) {
            $this->InputButton("Go$Code", "Колонки", "", "window.location = 'columns.php?Lst_Code=$Code';");
        }
    }
    
    class GoDelContent extends InputButton {
        function GoDelContent($Code) {
            $this->InputButton("Go$Code", "Удалить", "", "if(confirm('Вы действительно хотите удалить?')) { openUrl('del_content.php?Con_Code=$Code', 'delContent', this, true, 200, 200, 'Удалить содержимое'); }", "red");
        }
    }
    
    class GoMails extends InputButton {
        function GoMails($Code) {
            $this->InputButton("Go$Code", "Адреса", "", "window.location = 'mails.php?Lst_Code=$Code';");
        }
    }
    
    $formList = array(
        "TableName" => "formList",
        "Id"            => "Lst_Code",
        "Order"         => "Lst_Order",
        "Fields"        => array("Lst_Name", "Lst_Order"),
        "Types"         => array("T50", "T5"),
        "Uploads" => array(),
        "Header"        => "Список форм",
        "Headers"       => array("Название", "Порядок<br>вывода"),
        "Buttons"       => array("GoColumns"),
        "ButtonHeaders" => array("&nbsp;"),
        "Filters"       => array(),
        "FiltersNames"  => array(),
        "FiltersTypes"   => array(),
        "FiltersCompare" => array(),
        "EditOnly" => 0
    );
    
    $formFields = array(
        "Header"        => "Список полей",
        "TableName"     => "formFields",
        "Id"            => "Frm_Code",
        "Order"         => "Frm_Order DESC, Frm_Code",
        "Headers"       => array("Название", "Имя", "Тип поля", "Список", "X", "*", "Порядок<br>вывода"),
        "Fields"        => array("Frm_Field", "Frm_Id", "Frm_Type", "Frm_List", "Frm_Active", "Frm_Need", "Frm_Order"),
        "Types"         => array("T30", "T30", "ColTypesInputSelect", "contentListsDBSelect2", "InputCheckbox", "InputCheckbox", "T5"),
        "Uploads"       => array(),
        "Buttons"       => array(),
        "ButtonHeaders" => array(),
        "FiltersNames"  => array("Форма"),
        "Filters"       => array("Lst_Code"),
        "FiltersTypes"   => array("InputFormsSelect"),
        "FiltersCompare" => array("N"),
        "EditOnly"      => 0
    );
    
    $formContent = array(
        "TableName" => "formContent",
        "Header"        => "Данные формы",
        "Id"            => "Con_Code",
        "Order"         => "Con_Code DESC",
        "Fields"        => array("Con_Data", "Con_Date", "Con_Order", "Con_State"),
        "Types"         => array("formContentElem", "InputTime", "T5", "InputCheckbox"),
        "DBTypes"         => array("IGNORE", "IGNORE", "INT", "INT"),
        "Uploads" => array(),
        "Headers"       => array("Данные", "Дата", "Порядок<br>вывода", "[X]"),
        "Buttons"       => array("GoDelContent"),
        "ButtonHeaders" => array("&nbsp;"),
        "Filters"       => array("Lst_Code"),
        "FiltersNames"  => array("Форма"),
        "FiltersTypes"   => array("InputFormsSelect"),
        "FiltersCompare" => array("N"),
        "EditOnly" => 1
    );
    
    class formContentElem extends InputHidden {
        
        function formContentElem($Name, $Value) {
            $this->Value = $Value;
            $this->Name = $Name;
        }

        function dPrint() {
            $Lst_Code = $_REQUEST['Lst_Code'] ? $_REQUEST['Lst_Code'] : 1;
            $Value = json_decode($this->Value, true);
            
            $fieldsLabel = DB::selectArray("SELECT Frm_Field, Frm_Id, Frm_Type FROM formFields WHERE Lst_Code='$Lst_Code' ORDER BY Frm_Order");
            
            echo "<table cellpadding=0 cellspacing=1 border=0 bgcolor=\"#FEFEFE\" width=100%>";
            $i = 0;
            $Fields = array();
            foreach($fieldsLabel as $flk => $flv) {
                $Fields[$i]["Type"] = $fieldsLabel[$flk]["Frm_Type"];
                $Fields[$i]["Label"] = $fieldsLabel[$flk]["Frm_Field"];
                $Fields[$i]["Id"] = $fieldsLabel[$flk]["Frm_Id"];
                
                if(is_array($Value[$Fields[$i]["Id"]])) {
                    $Fields[$i]["Value"] = "";
                    $c = 1;
                    while(list($k, $v) = each($Value[$Fields[$i]["Id"]])) {
                        $Fields[$i]["Value"] .= $v;
                        if($c != count($Value[$flk])) {
                            $Fields[$i]["Value"] .= ", ";
                        }
                        $c++;
                    }
                } else {
                    $Fields[$i]["Value"] = $Value[$Fields[$i]["Id"]];
                    unset($Value[$Fields[$i]["Id"]]);
                }
                $i++;
            }
            foreach($Fields as $k => $v) {
                if($Fields[$k]["Type"] == 7) {
                    echo "<tr><td valign=top class=border colspan=2>&nbsp;&nbsp;&nbsp;<i>".$Fields[$k]["Label"]."</i></td></tr>";
                } elseif($Fields[$k]["Type"] == 6) {
                    echo "<tr><td valign=top class=border width=35%>".$Fields[$k]["Label"]."</td><td valign=top class=border style=\"background:#FEFEFE\"><a href=\"/data/mail/".$Fields[$k]["Value"]."\" target=\"_blank\">".$Fields[$k]["Value"]."</a></td></tr>";
                } else {
                    echo "<tr><td valign=top class=border width=35%>".$Fields[$k]["Label"]."</td><td valign=top class=border style=\"background:#FEFEFE\">".$Fields[$k]["Value"]."</td></tr>";
                }
            }
            if(is_array($Value)) {
                echo "<tr><td colspan=2>Дополнтельные поля:</td></tr>";
                foreach($Value as $k => $v) {
                    echo "<tr><td>$k:</td><td>$v</td></tr>";
                }
            }
            echo "</table>";
        }
    }
    
    $formMails = array(
        "TableName"     => "formMails",
        "Id"            => "Fm_Code",
        "Order"         => "Fm_Order DESC, Fm_Code",
        "Fields"        => array("Fm_Name", "Fm_Mail", "Fm_Order", "Fm_Active"),
        "Types"         => array("T30", "T30", "T5", "InputCheckbox"),
        "Header"        => "Список адресов",
        "Headers"       => array("Имя", "Адрес", "Порядок<br>вывода", "[X]"),
        "Buttons"       => array(),
        "ButtonHeaders" => array(),
        "Uploads"       => array(),
        "Filters"       => array("Lst_Code"),
        "FiltersNames"  => array("Форма"),
        "FiltersTypes"   => array("InputFormsSelect"),
        "FiltersCompare" => array("N"),
        "EditOnly"      => 0
    );


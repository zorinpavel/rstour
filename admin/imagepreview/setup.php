<?php

  class GoClearFolder extends InputButton {
    function GoClearFolder($Code) {
      $this->InputButton("Go$Code", "Очистить", "", "if(confirm('Вы действительно хотите очистить содержимое папки?')) { openUrl('clear_folder.php?Fol_Code=$Code', 'clearFolder', this, true, 0, 200, 'Удалить содержимое'); }");
    }
  }

  class GoDelFolder extends InputButton {
    function GoDelFolder($Code) {
      $this->InputButton("Go$Code", "Удалить", "", "if(confirm('Вы действительно хотите удалить настройки и все содержимое?')) { openUrl('del_folder.php?Fol_Code=$Code', 'delFolder', this, true, 0, 200, 'Удалить содержимое'); }", "red");
    }
  }

  $PreviewTypes = array(
    "TableName" => "PreviewTypes",
    "Id"            => "Pt_Code",
    "Order"         => "Pt_Code",
    "Fields"        => array("Pt_Path", "Pt_Width", "Pt_Height", "Pt_Quality", "Pt_Fill", "Pt_Comment", "Pt_Watermark", "Pt_Crop"),
    "Types"         => array("T20RO", "T5", "T5", "T5", "T5", "T20", "T20", "InputCheckbox"),
    "Uploads" => array(), 
    "Header"        => "Типы превью", 
    "Headers"       => array("Директория", "Ширина", "Высота", "Качество jpg", "Фон", "Комментарий", "Водный знак", "Crop"),
    "Buttons"       => array("GoClearFolder", "GoDelFolder"),
    "ButtonHeaders" => array("&nbsp;", "&nbsp;"),
    "Filters"       => array(),
    "FiltersNames"  => array(),
    "FiltersTypes"   => array(), 
    "FiltersCompare" => array(), 
    "EditOnly" => 0
  );

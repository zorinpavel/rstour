<?php

  $usrGroup = 0;
  include_once(ADMIN_PATH."/setup.php");
  include_once("setup.php");

  require_once(COMMON_PATH."/JsHttpRequestLib/JsHttpRequest.php");
  $JsHttpRequest = new JsHttpRequest("utf-8");

  $Fol_Code = (int)$_REQUEST['Fol_Code'];

  $f = db_query("SELECT Pt_Path FROM PreviewTypes WHERE Pt_Code = $Fol_Code");
  list($Cnt) = mysql_fetch_row($f);

  if($Cnt) {
    $DirPath = IPV_PREVIEWS_PATH."/$Cnt";
    ClearFolder($DirPath);
  } else {
    $Error = "Не могу найти такую запись";
  }

  if(!$Error) {
    ?>
    Очищено!
    <script>
      window.location = window.location;
    </script>
    <?
  } else {
    echo "<div class=\"error\">$Error</div>";
  }

  function ClearFolder($DirPath) {
    if ($handle = @opendir($DirPath)) {
      while (false !== ($file = readdir($handle))) {
        if($file != "." && $file != ".." && $file != "_settings.php") {
          if(is_dir("$DirPath/$file")) {
            $Error = ClearFolder("$DirPath/$file");
            if(!rmdir("$DirPath/$file"))
              $Error = "Не могу удалить директорию $DirPath/$file";
          } else {
            if(!unlink("$DirPath/$file"))
              $Error = "Не могу удалить файл $DirPath/$file";
          }
        }
      }
      closedir($handle);
    } else {
      $Error = "Не могу открыть директорию $DirPath";
    }
    return $Error;
  }

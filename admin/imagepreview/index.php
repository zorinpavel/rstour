<?php

  $usrGroup = 0;
  include_once(ADMIN_PATH."/setup.php");
  include_once("setup.php");


  AdminHeaders();
  MainNavi("imagepreview");

  $Table = "PreviewTypes";
  include(EDIT_DATA_PATH);

  if($Update) {
    foreach($_REQUEST as $Name => $Value) {
      ${$Name} = $Value;
    }

    for ($i=1; $i<=$Quantity; $i++) {
      $Code = ${$Id."_$i"};
      reset($Fields);
      $DirPath = IPV_PREVIEWS_PATH."/".${"Pt_Path_$i"};
      
      if(!is_dir($DirPath)) {
        mkdir($DirPath, 0777);
        chmod($DirPath, 0777);
      }
        
      $Index = "$DirPath/_settings.php";
      if(file_exists($Index))
        unlink($Index);

      $Width = (int)${"Pt_Width_$i"};
      $Height = (int)${"Pt_Height_$i"};
      $Quality = (int)${"Pt_Quality_$i"};
      $Fill = ${"Pt_Fill_$i"};
      $Comment = ${"Pt_Comment_$i"};
      $Watermark = ${"Pt_Watermark_$i"};
      $Crop = ${"Pt_Crop_$i"};

      $Settings = "<?

  \$_settings = array(\n";
     $Settings .= $Width ? "    \"width\" => $Width,\n" : "";
     $Settings .= $Height ? "    \"height\" => $Height,\n" : "";
     $Settings .= "    \"quality\" => ";
     $Settings .= $Quality ? "$Quality,\n" : "80,\n";
     $Settings .= $Fill ? "    \"fill\" => '$Fill',\n" : "";
     $Settings .= "    \"comment\" => '$Comment',\n";
     $Settings .= $Watermark ? "    \"watermark\" => '$Watermark',\n" : "";
     $Settings .= ($Crop == 1) ? "    \"crop\" => $Crop,\n" : "";
     $Settings .= "  );

?>";

      $fp = fopen($Index, "w");
      fputs($fp, $Settings, strlen($Settings));
      fclose($fp);
      chmod($Index, 0777);

    }
  }


  AdminFooter();

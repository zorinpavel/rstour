<?php

  $usrGroup = 0;
  include_once(ADMIN_PATH."/setup.php");
  include_once("setup.php");

  require_once(COMMON_PATH."/JsHttpRequestLib/JsHttpRequest.php");
  $JsHttpRequest = new JsHttpRequest("utf-8");

  $Fol_Code = (int)$_REQUEST['Fol_Code'];

  $f = db_query("SELECT Pt_Path FROM PreviewTypes WHERE Pt_Code = $Fol_Code");
  list($Cnt) = mysql_fetch_row($f);

  if($Cnt) {
    $df = db_query("DELETE FROM PreviewTypes WHERE Pt_Code = $Fol_Code");
    if(!$df) {
      $Error = "Ошибка базы данных";
    } else {
      $DirPath = IPV_PREVIEWS_PATH."/$Cnt";
      ClearFolder($DirPath);
    }
  } else {
    $Error = "Не могу найти такую запись";
  }

  if(!$Error) {
    ?>
    Удалено!
    <script>
      window.location = window.location;
    </script>
    <?
  } else {
    echo "<div class=\"error\">$Error</div>";
  }

  function ClearFolder($DirPath) {
    if ($handle = @opendir($DirPath)) {
      while (false !== ($file = readdir($handle))) {
        if($file != "." && $file != "..") {
          if(is_dir("$DirPath/$file")) {
            $Error = ClearFolder("$DirPath/$file");
            @rmdir("$DirPath/$file");
          }
          @unlink("$DirPath/$file");
        }
      }
      closedir($handle);
    } else {
      $Error = "Не могу открыть директорию $DirPath";
    }
    return $Error;
  }

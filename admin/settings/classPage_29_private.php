<?php

  $Settings = array(
    "page_css_file" => "",
    "page_meta_desc" => "",
    "page_meta_keywords" => "",
    "page_title" => "Диспетчерский план",
    "page_name" => "Диспетчерский план",
    "page_signature" => "",
    "page_parent" => "17",
    "page_localdir" => "dispatch",
    "page_type" => "0",
    "page_template" => array( "Value" => "6","External" => array( "contentMain" => array( "classes" => "classUsers","blocks" => "companyUsers"),"contentSecond" => array( "classes" => "classTripDispatch","blocks" => "tripDispatch"))),
    "page_visible" => "1",
    "page_access" => "0",
  );

<?php

  $Settings = array(
    "template_top" => array( "Value" => "0"),
    "template_items" => array( "Value" => "0"),
    "template_bottom" => array( "Value" => "0"),
    "template_one" => array( "Value" => "0"),
    "template_empty" => array( "Value" => "0"),
    "template_forbidden" => array( "Value" => "1"),
    "template_edit" => array( "Value" => "0"),
    "template_voucher" => array( "Value" => "1"),
    "template_booking" => array( "Value" => "1"),
    "template_invoice" => array( "Value" => "1"),
    "template_invoice_bank" => array( "Value" => "1"),
  );

<?php

  $Settings = array(
    "page_css_file" => "",
    "page_meta_desc" => "Не знаете где купить тур по России? Описание и способы, а так же места покупки экскурсионных туров по России",
    "page_meta_keywords" => "туры, туры по России, экскурсионные программы по России, где купить туры по России",
    "page_title" => "Места продажи экскурсионных туров по России",
    "page_name" => "Где купить",
    "page_signature" => "",
    "page_parent" => "0",
    "page_localdir" => "wheretobuy",
    "page_type" => "0",
    "page_template" => array( "Value" => "2","External" => array( "contentMain" => array( "classes" => "classContent","blocks" => "ownContent"),"contentSecond" => array( "classes" => "classHTML","blocks" => ""))),
    "page_visible" => "1",
    "page_access" => "1",
  );

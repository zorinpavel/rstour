<?php

  $Settings = array(
    "page_css_file" => "",
    "page_meta_desc" => "Самая свежая и актуальная информация о туристической компании Ростиславль",
    "page_meta_keywords" => "туры по России, туры, автобусные туры по России, экскурсионные программы по России",
    "page_title" => "О компании",
    "page_name" => "О компании",
    "page_signature" => "",
    "page_parent" => "0",
    "page_localdir" => "about",
    "page_type" => "0",
    "page_template" => array( "Value" => "2","External" => array( "contentMain" => array( "classes" => "classContent","blocks" => "ownContent"),"contentSecond" => array( "classes" => "classHTML","blocks" => ""))),
    "page_visible" => "1",
    "page_access" => "1",
  );

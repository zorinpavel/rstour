<?php

  $Settings = array(
    "page_css_file" => "",
    "page_meta_desc" => "Пожалуйста, ознакомьтесь с настоящей памяткой туристам до начала экскурсионного тура по России",
    "page_meta_keywords" => "туры, автобусные туры, экскурсионные программы по России",
    "page_title" => "Туристам",
    "page_name" => "Туристам",
    "page_signature" => "",
    "page_parent" => "0",
    "page_localdir" => "for_tourists",
    "page_type" => "0",
    "page_template" => array( "Value" => "2","External" => array( "contentMain" => array( "classes" => "classContent","blocks" => "ownContent"),"contentSecond" => array( "classes" => "classHTML","blocks" => ""))),
    "page_visible" => "1",
    "page_access" => "1",
  );

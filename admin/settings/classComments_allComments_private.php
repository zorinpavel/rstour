<?php

  $Settings = array(
    "Export" => "1",
    "Limit" => "50",
    "template_block_top" => array( "Value" => "2"),
    "template_top" => array( "Value" => "1"),
    "template_elem" => array( "Value" => "2"),
    "template_bottom" => array( "Value" => "1"),
    "template_block_bottom" => array( "Value" => "1"),
    "template_form" => array( "Value" => "0"),
    "template_empty" => array( "Value" => "0"),
  );

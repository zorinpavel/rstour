<?php

  $Settings = array(
    "ContentData" => array( "classes" => "classContentData","blocks" => "ownData"),
    "template_item" => array( "Value" => "1"),
    "template_empty" => array( "Value" => "0"),
    "ShowOnlyOne" => "1",
    "ShowFirst" => "1",
    "ListCount" => "",
    "ShowScroll" => "0",
    "ContentList" => array( "classes" => "classContentList"),
    "ContentListShort" => array( "classes" => "classContentList"),
    "ContentListRelated" => array( "classes" => "classContentList"),
  );

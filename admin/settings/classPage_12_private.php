<?php

  $Settings = array(
    "page_css_file" => "",
    "page_meta_desc" => "Перечень городов России поможет с легкостью сориентироваться  при составлении программы или подбора тура",
    "page_meta_keywords" => "туры, автобусные туры, экскурсионные туры по России из Москвы",
    "page_title" => "Города России",
    "page_name" => "Куда поехать",
    "page_signature" => "",
    "page_parent" => "0",
    "page_localdir" => "where_to_go",
    "page_type" => "0",
    "page_template" => array( "Value" => "2","External" => array( "contentMain" => array( "classes" => "classCity","blocks" => "citiesList"))),
    "page_visible" => "1",
    "page_access" => "1",
  );

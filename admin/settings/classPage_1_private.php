<?php

  $Settings = array(
    "page_css_file" => "",
    "page_meta_desc" => "",
    "page_meta_keywords" => "",
    "page_title" => "Личный кабинет",
    "page_name" => "Личный кабинет",
    "page_signature" => "",
    "page_parent" => "0",
    "page_localdir" => "user",
    "page_type" => "0",
    "page_template" => array( "Value" => "6","External" => array( "contentMain" => array( "classes" => "classUsers","blocks" => "Users"),"contentSecond" => array( "classes" => "classHTML","blocks" => ""))),
    "page_visible" => "0",
    "page_access" => "1",
  );

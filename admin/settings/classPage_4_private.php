<?php

  $Settings = array(
    "page_css_file" => "",
    "page_meta_desc" => "Наш календарь туров по России помогает быстро найти ближайший тур по России и присоединиться к группе",
    "page_meta_keywords" => "туры, автобусные туры, экскурсионные программы по России, туры по России",
    "page_title" => "Календарь туров",
    "page_name" => "Календарь туров",
    "page_signature" => "",
    "page_parent" => "0",
    "page_localdir" => "calendar",
    "page_type" => "0",
    "page_template" => array( "Value" => "2","External" => array( "contentMain" => array( "classes" => "classHTML","blocks" => "tripCalendar"),"contentSecond" => array( "classes" => "classHTML","blocks" => ""))),
    "page_visible" => "1",
    "page_access" => "1",
  );

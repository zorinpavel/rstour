<?php

  $Settings = array(
    "page_css_file" => "",
    "page_meta_desc" => "Если вам понравилось с нами работать мы всегда рада получить ваши отзывы",
    "page_meta_keywords" => "туры по России, отзывы о турах по России, экскурсионные программы по России",
    "page_title" => "Отзывы",
    "page_name" => "Отзывы",
    "page_signature" => "",
    "page_parent" => "0",
    "page_localdir" => "faq",
    "page_type" => "0",
    "page_template" => array( "Value" => "2","External" => array( "contentMain" => array( "classes" => "classHTML","blocks" => "VKComments"),"contentSecond" => array( "classes" => "classHTML","blocks" => ""))),
    "page_visible" => "1",
    "page_access" => "1",
  );

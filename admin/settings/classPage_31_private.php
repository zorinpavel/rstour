<?php

  $Settings = array(
    "page_css_file" => "",
    "page_meta_desc" => "",
    "page_meta_keywords" => "",
    "page_title" => "Правила обработки информации",
    "page_name" => "Правила обработки информации",
    "page_signature" => "",
    "page_parent" => "0",
    "page_localdir" => "rules",
    "page_type" => "0",
    "page_template" => array( "Value" => "2","External" => array( "contentMain" => array( "classes" => "classHTML","blocks" => "Rules"),"contentSecond" => array( "classes" => "classHTML","blocks" => "Confirmation"))),
    "page_visible" => "0",
    "page_access" => "1",
  );

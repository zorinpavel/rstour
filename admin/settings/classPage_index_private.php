<?php

  $Settings = array(
    "page_css_file" => "",
    "page_meta_desc" => "Туроператор Ростиславль организует и проводит экскурсионные туры по самым интересным историческим местам России.",
    "page_meta_keywords" => "туры, автобусные туры, экскурсии, достопримечательности, экскурсии по россии",
    "page_title" => "Туроператор по России. Экскурсионные туры по городам России из Москвы. Цена. Туристическая компания Ростиславль.",
    "page_name" => "Главная",
    "page_signature" => "",
    "page_type" => "0",
    "page_template" => array( "Value" => "5","External" => array( "contentMain" => array( "classes" => "classHTML","blocks" => "indexBanners"))),
    "page_visible" => "1",
    "page_access" => "1",
  );

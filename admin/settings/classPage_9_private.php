<?php

  $Settings = array(
    "page_css_file" => "",
    "page_meta_desc" => "Если у вас вопрос по поводу покупки или доставки экскурсионного тура по России, вы можете задать их прям у нас на сайте",
    "page_meta_keywords" => "туры по России, экскурсионные туры по России, вопросы и ответы по турам",
    "page_title" => "Задать вопрос",
    "page_name" => "Задать вопрос",
    "page_signature" => "",
    "page_parent" => "0",
    "page_localdir" => "questions",
    "page_type" => "0",
    "page_template" => array( "Value" => "2","External" => array( "contentMain" => array( "classes" => "classForm","blocks" => "questionForm"),"contentSecond" => array( "classes" => "classHTML","blocks" => ""))),
    "page_visible" => "0",
    "page_access" => "1",
  );

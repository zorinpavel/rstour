<?php

  $Settings = array(
    "ContentData" => array( "classes" => "classContentData","blocks" => "ownData"),
    "ListCount" => "0",
    "ShowScroll" => "0",
    "OverrideParent" => "1",
    "UseIteID" => "0",
    "UseDate" => "0",
    "template_top" => array( "Value" => "0"),
    "template_bottom" => array( "Value" => "0"),
    "template_list_top" => array( "Value" => "1"),
    "template_list_element1" => array( "Value" => "1"),
    "template_list_element2" => array( "Value" => "0"),
    "template_list_element3" => array( "Value" => "0"),
    "template_list_element4" => array( "Value" => "0"),
    "template_list_element5" => array( "Value" => "0"),
    "template_list_sep" => array( "Value" => "0"),
    "template_list_bottom" => array( "Value" => "1"),
  );

<?php

  $Settings = array(
    "page_css_file" => "",
    "page_meta_desc" => "Программы и условия страхования туристов на экускурсионных программах по России",
    "page_meta_keywords" => "туры, туры по России, страховка туристов, страхование, экскурсионные программы",
    "page_title" => "Условия страхования",
    "page_name" => "Страхование",
    "page_signature" => "",
    "page_parent" => "0",
    "page_localdir" => "insurance",
    "page_type" => "0",
    "page_template" => array( "Value" => "2","External" => array( "contentMain" => array( "classes" => "classContent","blocks" => "ownContent"),"contentSecond" => array( "classes" => "classHTML","blocks" => ""))),
    "page_visible" => "0",
    "page_access" => "0",
  );

<?php

  $Settings = array(
    "ContentData" => array( "classes" => "classContentData","blocks" => "ownData"),
    "template_item" => array( "Value" => "2"),
    "template_empty" => array( "Value" => "0"),
    "ShowOnlyOne" => "0",
    "ShowFirst" => "0",
    "SetError" => "0",
    "ListCount" => "0",
    "ShowScroll" => "0",
    "ContentList" => array( "classes" => "classContentList","blocks" => "blogList"),
    "ContentListShort" => array( "classes" => "classContentList","blocks" => ""),
    "ContentListRelated" => array( "classes" => "classContentList","blocks" => ""),
  );

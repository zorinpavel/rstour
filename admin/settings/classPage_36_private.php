<?php

  $Settings = array(
    "page_css_file" => "",
    "page_meta_desc" => "Вакансии туристической компании Ростиславль",
    "page_meta_keywords" => "туры по России, туры, автобусные туры по России, экскурсионные программы по России",
    "page_title" => "Вакансии Ростиславль",
    "page_name" => "Вакансии",
    "page_signature" => "",
    "page_parent" => "0",
    "page_localdir" => "vacancies",
    "page_type" => "0",
    "page_template" => array( "Value" => "2","External" => array( "contentMain" => array( "classes" => "classContent","blocks" => "ownContent"),"contentSecond" => array( "classes" => "classHTML","blocks" => ""))),
    "page_visible" => "0",
    "page_access" => "1",
  );

<?php

  $Settings = array(
    "template_update_ok" => array( "Value" => "0"),
    "template_empty" => array( "Value" => "1"),
    "template_top" => array( "Value" => "1"),
    "template_item" => array( "Value" => "1"),
    "template_item_sep" => array( "Value" => "0"),
    "template_bottom" => array( "Value" => "1"),
    "template_contacts" => array( "Value" => "1"),
    "template_enterdata" => array( "Value" => "1"),
    "template_delivery" => array( "Value" => "1"),
    "template_payment" => array( "Value" => "1"),
    "template_orderinfo" => array( "Value" => "2"),
    "template_adminmail" => array( "Value" => "1"),
  );

<?php

  $Settings = array(
    "page_css_file" => "",
    "page_meta_desc" => "",
    "page_meta_keywords" => "",
    "page_title" => "Личный кабинет - Редактировать реквизиты",
    "page_name" => "Редактировать реквизиты",
    "page_signature" => "",
    "page_parent" => "17",
    "page_localdir" => "edit",
    "page_type" => "0",
    "page_template" => array( "Value" => "2","External" => array( "contentMain" => array( "classes" => "classUsers","blocks" => "companyUsers"),"contentSecond" => array( "classes" => "classCompany","blocks" => "Company"))),
    "page_visible" => "1",
    "page_access" => "0",
  );

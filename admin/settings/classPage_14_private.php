<?php

  $Settings = array(
    "page_css_file" => ";pages,user.css",
    "page_meta_desc" => "",
    "page_meta_keywords" => "",
    "page_title" => "Личный кабинет - Начало сотрудничества",
    "page_name" => "Начало сотрудничества",
    "page_signature" => "",
    "page_parent" => "17",
    "page_localdir" => "instructions",
    "page_type" => "0",
    "page_template" => array( "Value" => "2","External" => array( "contentMain" => array( "classes" => "classUsers","blocks" => "companyUsers"),"contentSecond" => array( "classes" => "classCompany","blocks" => "Company"))),
    "page_visible" => "1",
    "page_access" => "0",
  );

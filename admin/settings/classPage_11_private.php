<?php

  $Settings = array(
    "page_css_file" => "",
    "page_meta_desc" => "Контакты туристической компании Ростиславль",
    "page_meta_keywords" => "туры по России, туры, автобусные туры по России, экскурсионные программы по России",
    "page_title" => "Контакты",
    "page_name" => "Контакты",
    "page_signature" => "",
    "page_parent" => "0",
    "page_localdir" => "contacts",
    "page_type" => "0",
    "page_template" => array( "Value" => "2","External" => array( "contentMain" => array( "classes" => "classHTML","blocks" => "contactsContent"),"contentSecond" => array( "classes" => "classHTML","blocks" => ""))),
    "page_visible" => "1",
    "page_access" => "1",
  );

<?php

  $Settings = array(
    "page_css_file" => "",
    "page_meta_desc" => "",
    "page_meta_keywords" => "",
    "page_title" => "",
    "page_name" => "Подписка",
    "page_signature" => "",
    "page_parent" => "0",
    "page_localdir" => "subscribe",
    "page_type" => "0",
    "page_template" => array( "Value" => "2","External" => array( "contentMain" => array( "classes" => "classSubscribe","blocks" => "Subscribe"),"contentSecond" => array( "classes" => "classHTML","blocks" => ""))),
    "page_visible" => "0",
    "page_access" => "1",
  );

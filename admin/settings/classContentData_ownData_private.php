<?php

  $Settings = array(
    "DataType" => "1",
    "Structures" => array( "Structures" => array( "0" => "1","1" => "2")),
    "OrderBy" => "Ite_Priority DESC, Ite_BDate DESC",
    "NoDate" => "0",
    "ListScroll" => array( "classes" => "classScroll","blocks" => ""),
    "ContentCart" => array( "classes" => "classContentCart"),
    "ContentFilter" => "0",
    "AndOr" => "0",
  );

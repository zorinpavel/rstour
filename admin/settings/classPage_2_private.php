<?php

  $Settings = array(
    "page_css_file" => "",
    "page_meta_desc" => "Автобусные туры по России из Москвы от компании &quot;Ростиславль&quot;. Наши менеджеры помогут вам при поиске и составлении экскурсионной программы",
    "page_meta_keywords" => "туры, автобусные туры, экскурсии по России, экскурсионные туры из Москвы",
    "page_title" => "Каталог экскурсионных и автобусных туров по России из Москвы",
    "page_name" => "Все туры",
    "page_signature" => "Экскурсионные туры по России",
    "page_parent" => "0",
    "page_localdir" => "tours",
    "page_type" => "0",
    "page_template" => array( "Value" => "3"),
    "page_visible" => "1",
    "page_access" => "1",
  );

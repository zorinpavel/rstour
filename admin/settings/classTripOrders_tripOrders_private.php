<?php

  $Settings = array(
    "template_top" => array( "Value" => "1"),
    "template_items" => array( "Value" => "1"),
    "template_bottom" => array( "Value" => "1"),
    "template_one" => array( "Value" => "1"),
    "template_empty" => array( "Value" => "1"),
    "template_forbidden" => array( "Value" => "1"),
    "template_edit" => array( "Value" => "1"),
    "template_voucher" => array( "Value" => "1"),
    "template_booking" => array( "Value" => "1"),
    "template_invoice" => array( "Value" => "0"),
    "template_invoice_bank" => array( "Value" => "0"),
  );

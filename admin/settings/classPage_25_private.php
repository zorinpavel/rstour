<?php

  $Settings = array(
    "page_css_file" => "",
    "page_meta_desc" => "Общая информация по работе с туристическими агентствами",
    "page_meta_keywords" => "туры по России, туры, автобусные туры, агентский договор",
    "page_title" => "Информация для туристических агентств",
    "page_name" => "Агентствам",
    "page_signature" => "",
    "page_parent" => "0",
    "page_localdir" => "for_agensies",
    "page_type" => "0",
    "page_template" => array( "Value" => "2","External" => array( "contentMain" => array( "classes" => "classContent","blocks" => "ownContent"),"contentSecond" => array( "classes" => "classHTML","blocks" => ""))),
    "page_visible" => "1",
    "page_access" => "1",
  );

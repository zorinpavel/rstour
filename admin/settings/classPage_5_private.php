<?php

  $Settings = array(
    "page_css_file" => "",
    "page_meta_desc" => "Наши условия по оплате и доставке экскурсионных туров по России ",
    "page_meta_keywords" => "туры по России, автобусные туры, оплата туров по России",
    "page_title" => "Оплата и доставка",
    "page_name" => "Оплата и доставка",
    "page_signature" => "",
    "page_parent" => "0",
    "page_localdir" => "payment",
    "page_type" => "0",
    "page_template" => array( "Value" => "2","External" => array( "contentMain" => array( "classes" => "classContent","blocks" => "ownContent"),"contentSecond" => array( "classes" => "classHTML","blocks" => ""))),
    "page_visible" => "1",
    "page_access" => "1",
  );

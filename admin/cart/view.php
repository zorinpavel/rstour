<?php

    include_once(ADMIN_PATH."/setup.php");
    include_once("setup.php");


    AdminHeaders();
    MainNavi("cart");

    $classTrip = _autoload("classTrip");

    $Ord_Code = (int)$_REQUEST['Ord_Code'];
    $Order = DB::selectArray("SELECT * FROM cartData WHERE Ord_Code = $Ord_Code");
    $cartData = json_decode($Order['Ord_Data'], true);

    $Table = "OrderView";
    include(EDIT_ONE_PATH);

    foreach($cartData['ItemsData'] as $T_CodeKey => $Item)
        break;
    $Item = $classTrip->GetItemVars($Item, false);
    foreach($Item['Dates'] as $Key => $Date) {
        if(date("Y-m-d") > date("Y-m-d", strtotime($Date['T_Date'])) && $Date['T_Date'] != $Order['T_Date'])
            unset($Item['Dates'][$Key]);
    }

?>
    <table width="100%" class="border">
        <tr><th colspan="2" class="border">Дополнительные параметры:</th></tr>
        <tr>
            <td class="border">Пользователь</td>
            <td class="border">
                <?
                    echo "<a href=\"".ADMIN_URL."/siteusers/edit.php?us_id=".$cartData['User']['us_id']."\" target=\"_blank\"><b>".$cartData['User']['lastname']." ".$cartData['User']['name']." ".$cartData['User']['middlename']."</b></a>";
                    echo "<br>".$cartData['User']['phone']."<br>".$cartData['User']['mail'];
                    if($cartData['User']['Company']) {
                        echo "<br><b>".$cartData['User']['Company']['C_Name']."</b><br>".$cartData['User']['Company']['C_Phone'];
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td class="border">Тур</td>
            <td class="border">
                <?
                    echo "<h3>".$Item['T_Name']."</h3>";
                    echo "<select name=\"date\">";
                    foreach($Item['Dates'] as $rKey => $Date) {
                        echo "<option value=\"".$Date['T_Date']."\"";
                        if($Date['T_Date'] == $Order['T_Date'])
                          echo " selected";
                        echo ">".$Date['T_DateStr']."</option>";
                    }
                    echo "</select>";
                    echo "<ul><lh>В тур включено:</lh>";
                    foreach($Item['Included'] as $iKey => $iValue) {
                        echo "<li>".$iValue['dI_Name']."</li>";
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td class="border">Туристы</td>
            <td class="border">
                <table width="100%" class="border">
                    <?
                        $color = true;
                        foreach($cartData['Travelers'] as $tKey => $Traveler) {
                            $color = !$color;
                            echo "<tr style=\"".($color ? "background-color:#F9F9F9;" : "")."\"><td class=\"border\">ФИО</td><td class=\"border\" style=\"white-space: nowrap;\">";
                            $tLastName = new InputText("travelers[$tKey][lastname]", $Traveler['lastname'], 20, "font-size:110%;margin-bottom:0.2em;");
                            $tLastName->dPrint();
                            $tName = new InputText("travelers[$tKey][name]", $Traveler['name'], 20, "font-size:110%;margin-bottom:0.2em;");
                            $tName->dPrint();
                            $tMiddleName = new InputText("travelers[$tKey][middlename]", $Traveler['middlename'], 20, "font-size:110%;margin-bottom:0.2em;");
                            $tMiddleName->dPrint();
                            echo "</td></tr><tr style=\"".($color ? "background-color:#F9F9F9;" : "")."\"><td class=\"border\">Пол</td><td class=\"border\">";
                            $tSex = new InputRadio("travelers[$tKey][sex]", 1, $Traveler['sex'], "мужской");
                            $tSex->dPrint();
                            $tSex = new InputRadio("travelers[$tKey][sex]", 2, $Traveler['sex'], "женский");
                            $tSex->dPrint();
                            echo "</td></tr><tr style=\"".($color ? "background-color:#F9F9F9;" : "")."\"><td class=\"border\">Дата рождения</td><td class=\"border\">";
                            $tBirthdate = new InputText("travelers[$tKey][birthdate]", $Traveler['birthdate'], 10);
                            $tBirthdate->dPrint();
                            echo "</td></tr><tr style=\"".($color ? "background-color:#F9F9F9;" : "")."\"><td class=\"border\">Ребенок до 14 лет</td><td class=\"border\">";
                            $tKid = new InputCheckbox("travelers[$tKey][kid]", $Traveler['kid'], "", "да");
                            $tKid->dPrint();
                            echo "</td></tr><tr style=\"".($color ? "background-color:#F9F9F9;" : "")."\"><td class=\"border\">Паспорт</td><td class=\"border\">";
                            $tPassport = new T10("travelers[$tKey][passport][number]", $Traveler['passport']['number']);
                            $tPassport->dPrint();
//                            $tPassport = new T10("travelers[$tKey][passport][date]", $Traveler['passport']['date']);
//                            $tPassport->dPrint();
                            echo "</td></tr><tr style=\"".($color ? "background-color:#F9F9F9;" : "")."\"><td class=\"border\">Дополнительно приобретено:</td><td class=\"border\">";
                            echo "<ul>";
                            foreach($Item['Extras'] as $eKey => $eValue) {
                                echo "<li><label><input type=\"checkbox\" name=\"travelers[$tKey][Extras][$eKey]\"".($Traveler['Extras'][$eKey] ? " checked" : "").">".$eValue['dI_Name']." ".$eValue['dE_Price']." руб.</label></li>";
                            }
                            echo "</ul>";
                            echo "</td></tr>";
                        }
                    ?>
                </table>
            </td>
        </tr>

        <? if($cartData['Delivery']) { ?>
        <tr>
            <td class="border">Доставка</td>
            <td class="border">
                <?
                    $dAddr = new InputText("Delivery_Name", $cartData['Delivery']['Delivery_Name'], 20, "margin-bottom:0.2em");
                    $dAddr->dPrint();
                    echo "<br>";
                    $dAddr = new InputText("Delivery_Addr", $cartData['Delivery']['Delivery_Addr'], 50, "margin-bottom:0.2em");
                    $dAddr->dPrint();
                    echo "<br>";
                    $dAddr = new TA30("Delivery_Comment", $cartData['Delivery']['Delivery_Comment']);
                    $dAddr->dPrint();
                ?>
            </td>
        </tr>
        <? } ?>

        <? if($cartData['Payment']) { ?>
        <tr>
            <td class="border">Оплата</td>
            <td class="border">
                <?
                    echo $cartData['Payment']['PS_Name'];
                ?>
            </td>
        </tr>
        <? } ?>

        <? if($cartData['Discount']['Amount']) { ?>
        <tr>
            <td class="border">Скидка</td>
            <td class="border">
                <?
                    echo $cartData['Discount']['Name']."<br>".$cartData['Discount']['Amount']."% = ".number_format($cartData['Discount']['Sum'], 2, ".", " ")." руб.";
                ?>
            </td>
        </tr>
        <? } ?>

        <tr>
            <td class="border"><b>Итого</b></td>
            <td class="border"><b><? echo number_format($cartData['TotalSum'], 2, ".", " "); ?></b> руб.</td>
        </tr>
    </table>

    </form>

    </fieldset>
</div>

<? if(Permissions::getPermissions(94)) { ?>
    <div style="width:60%;margin:2em 10%;">
        <fieldset><legend class="Header">Events:</legend>
            <pre><? echo $Order['events']; ?></pre>
        </fieldset>
    </div>
<? } ?>

<?


    AdminFooter();
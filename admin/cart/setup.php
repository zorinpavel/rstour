<?php


    class GoOrderEdit extends InputButton {
        function GoOrderEdit($Code) {
            if(Permissions::getPermissions(95))
                $this->InputButton("Go$Code", "Подробности", "", "window.location = '".ADMIN_URL."/cart/view.php?Ord_Code=$Code'", "blue");
            if(Permissions::getPermissions(92))
                $this->InputButton("Go$Code", "Редактировать", "", "window.location = '".ADMIN_URL."/cart/edit.php?Ord_Code=$Code'", "green");
        }
    }

    class GoDelOrder extends InputButton {
        function GoDelOrder($Code) {
            if(!Permissions::getPermissions(93))
                $this->Disabled = true;
            $this->InputButton("Go$Code", "Удалить", "", "if(confirm('Вы действительно хотите удалить этот заказ?')) { openUrl('".ADMIN_URL."/cart/delete.php?Ord_Code=$Code', 'delOrder', this, true, 0, 200, 'Удалить заказ'); }", "red");
        }
    }

    class ParamsInputTypes extends InputSelect {
        function ParamsInputTypes($Name, $Value) {
            $this->Empty=1;
            $Options = array(
                "1" => "Текстовое поле",
                "2" => "Большое текстовое поле",
                "3" => "Пароль",
            );
            $this->InputSelect($Name, $Value, $Options);
        }
    }

    class InputStatusSelect extends InputSelect {
        function InputStatusSelect($Name, $Value, $onChange="") {
            $Options = classTripCart::$OrderStatuses;
            $this->InputSelect($Name, $Value, $Options, "", $onChange);
        }
    }

    class InputStatusSelectAlert extends InputSelect {
        function InputStatusSelectAlert($Name, $Value, $onChange="") {
            $Options = classTripCart::$OrderStatuses;
            $this->InputSelect($Name, $Value, $Options, "font-size:120%;", "if(!confirm('Вы действительно хотите сменить статус брони?')) { this.selectedIndex = SelectedIndex; }");
        }
        function dPrint() {
            parent::dPrint();
            ?><script type="text/javascript">var SelectedIndex = document.getElementById('<? echo $this->Name; ?>').selectedIndex;</script><?
        }
    }

    class InputDBSelectDelivery extends InputDBSelect {
        function InputDBSelectDelivery($Name, $Value, $onChange="") {
            $this->InputDBSelect($Name, $Value, "cartDeliverySystems", "DS_Code", "DS_Name", "", "", "", $onChange);
        }
    }

    $cartParams = array(
        "TableName" => "cartParams",
        "Id"            => "Par_Code",
        "Order"         => "Par_Order DESC, Par_Code",
        "Fields"        => array("Par_Code", "Par_Header", "Par_Name", "Par_Type", "Par_Required", "Par_Order"),
        "Types"         => array("InputHidden", "T30", "T30", "ParamsInputTypes", "InputCheckbox", "T5"),
        "Uploads"       => array(),
        "Header"        => "Параметры корзины",
        "Headers"       => array("", "Название", "Name", "Тип", "*", "Порядок<br>вывода"),
        "Buttons"       => array(),
        "ButtonHeaders" => array(),
        "Filters"       => array(),
        "FiltersNames"  => array(),
        "FiltersTypes"   => array(),
        "FiltersCompare" => array(),
        "EditOnly" => 0
    );

    $cartDataTable = array(
        "TableName" => "cartData",
        "Header"        => "Список заказов",
        "Id"            => "Ord_Code",
        "Order"         => "Ord_Code DESC",
        "Headers"       => array("#", "Номер брони", "Дата", "Заказ"),
        "Fields"        => array("Ord_Code", "Ord_Number", "Ord_Time", "Ord_Data"),
        "Types"         => array("InputLabel", "InputLabel", "InputTime", "cartContentElem"),
        "DBTypes"         => array("IGNORE", "IGNORE", "IGNORE", "IGNORE"),
        "Uploads"       => array(),
        "ButtonHeaders" => array("", ""),
        "Buttons"       => array("GoOrderEdit", "GoDelOrder"),
        "Filters"       => array("Ord_Status"),
        "FiltersNames"  => array("Статус заказа"),
        "FiltersTypes"   => array("InputStatusSelect"),
        "FiltersCompare" => array("N"),
        "EditOnly" => 1,
        "ViewOnly" => 1,
    );

    $cartDeliverySystems = array(
        "TableName" => "cartDeliverySystems",
        "Id"            => "DS_Code",
        "Order"         => "DS_Order DESC, DS_Code",
        "Fields"        => array("DS_Code", "DS_Name", "DS_Data", "DS_Price", "DS_SelfPayment", "DS_Order"),
        "Types"         => array("InputHidden", "T30", "TA50", "T10", "InputCheckbox", "T5"),
        "Uploads"       => array(),
        "Header"        => "Список способов доставки",
        "Headers"       => array("", "Имя", "Описание", "Цена", "Оплата<br>при получении", "Порядок<br>вывода"),
        "Buttons"       => array(),
        "ButtonHeaders" => array(),
        "Filters"       => array(),
        "FiltersNames"  => array(),
        "FiltersTypes"   => array(),
        "FiltersCompare" => array(),
        "EditOnly"      => 0,
    );

    $cartPaymentSystems = array(
        "Header"        => "Список платежных систем",
        "TableName"     => "cartPaymentSystems",
        "Id"            => "PS_Code",
        "Order"         => "PS_Order DESC, PS_Code",
        "Headers"       => array("#", "Id системы", "Имя", "Описание", "Только<br>при получении", "Порядок<br>вывода"),
        "Fields"        => array("PS_Code", "PS_Id", "PS_Name", "PS_Data", "PS_SelfPayment", "PS_Order"),
        "Types"         => array("InputLabel", "T10RO", "T30", "TA50", "InputCheckbox", "T5"),
        "DBTypes"         => array("IGNORE", "CHAR", "CHAR", "CHAR", "INT", "INT"),
        "Uploads"       => array(),
        "Buttons"       => array(),
        "ButtonHeaders" => array(),
        "Filters"       => array(),
        "FiltersNames"  => array(),
        "FiltersTypes"   => array(),
        "FiltersCompare" => array(),
        "EditOnly"      => 0,
    );


    class cartContentElem {

        function cartContentElem($Name, $Value) {
            $this->Value = $Value;
            $this->Name = $Name;
        }

        function dPrint() {
            $Value = json_decode($this->Value, true);

            echo "<b>".$Value['User']['lastname']." ".$Value['User']['name']." ".$Value['User']['middlename']."</b><br>".$Value['User']['phone']." ".$Value['User']['mail'];

            if($Value['User']['Company']) {
                echo "<br>".$Value['User']['Company']['C_Name'];
            }

            echo "<table class=\"border\" width=100%>";

            if(is_array($Value['ItemsData'])) {
                foreach($Value['ItemsData'] as $Key => $Data) {
                    echo "<tr><td valign=top class=border width=35% align=left>".$Data['T_Barcode']." ".$Data['T_Name']."</td><td valign=top class=border style=\"background:#EEEEEE\" align=left>";
                    echo $Data['Quantity']." x ".$Data['CartPrice']." = ".$Data['Sum']." руб.<br>";
                    if($Data['KidsDiscount'])
                        echo "<small>(Скидка для детей ".$Data['KidsDiscount']." руб.)</small>";
                    echo "</td></tr>";
                }
            }

            if(is_array($Value['ExtrasData'])) {
                foreach($Value['ExtrasData'] as $Key => $Data) {
                    echo "<tr><td valign=top class=border width=35% align=left>".$Data['dI_Name']."</td><td valign=top class=border style=\"background:#EEEEEE\" align=left>";
                    echo $Data['Quantity']." x ".$Data['dE_Price']." = ".$Data['Sum']." руб.<br>";
                    echo "</td></tr>";
                }
            }

            echo "<tr><td valign=top class=border width=35% align=left>Туристы</td><td valign=top class=border style=\"background:#EEEEEE\" align=left>";
            foreach($Value['Travelers'] as $tKey => $Traveler) {
                echo $tKey.". ".$Traveler['lastname']." ".$Traveler['name']." ".$Traveler['middlename']."<br>";
            }
            echo "</td></tr>";


            if(!$Value['User']['Company']) {
                echo "<tr><td valign=top class=border width=35% align=left>Доставка</td><td valign=top class=border style=\"background:#EEEEEE\" align=left>";
                if($Value['Delivery']['DS_Price'])
                    echo "1 x ".$Value['Delivery']['DS_Price']." = ".$Value['Delivery']['DS_Price']." руб.<br>";
                echo $Value['Delivery']['DS_Name']."<br>";
                echo $Value['Delivery']['Delivery_Name']." ".$Value['Delivery']['Delivery_Phone']."<br>";
                echo $Value['Delivery']['Delivery_Addr']."<br>";
                echo "</td></tr>";

                echo "<tr><td valign=top class=border width=35% align=left>Оплата</td><td valign=top class=border style=\"background:#EEEEEE\" align=left>";
                echo $Value['Payment']['PS_Name'];
                echo "</td></tr>";
            }

            if($Value['Discount']['Amount']) {
                echo "<tr><td valign=top class=border width=35% align=left>Скидка</td><td valign=top class=border style=\"background:#EEEEEE\" align=left>";
                echo $Value['Discount']['Name']."<br>".$Value['Discount']['Amount']."% = ".$Value['Discount']['Sum']." руб.</td></tr>";
            }

            echo "<tr><td valign=top class=border width=35% align=left><b>Итого:</b></td><td valign=top class=border style=\"background:#EEEEEE\" align=left><b>".$Value["TotalSum"]." руб.</b></td></tr>";
            echo "</table>";

            if($_REQUEST['debug']) {
                ?><div style="text-align:left;"><pre><? print_r($Value); ?></pre></div><?
            }

        }
    }


    $OrderEdit = array(
        "Header"        => "Редактировать бронь",
        "TableName"     => "cartData",
        "Id"            => "Ord_Code",
        "IdName"        => "Ord_Number",
        "Headers"       => array("Номер брони", "Дата", "Дата изменения", "Комментарий"),
        "Fields"        => array("Ord_Number", "Ord_Time", "Ord_Change", "Ord_Comment"),
        "Types"         => array("T20RO", "InputTime", "InputTime", "TA30"),
        "DBTypes"       => array("IGNORE", "IGNORE", "IGNORE", "CHAR/NULL"),
        "ButtonHeaders" => array(),
        "Buttons"       => array(),
        "ViewOnly"      => 0,
        "Params"        => 1,
    );


    $OrderView = array(
        "Header"        => "Подробности брони",
        "TableName"     => "cartData",
        "Id"            => "Ord_Code",
        "IdName"        => "Ord_Number",
        "Headers"       => array("Номер брони", "Дата", "Дата изменения", "Статус"),
        "Fields"        => array("Ord_Number", "Ord_Time", "Ord_Change", "Ord_Status"),
        "Types"         => array("T20RO", "InputTime", "InputTime", "InputStatusSelect"),
        "DBTypes"       => array("IGNORE", "IGNORE", "IGNORE", "IGNORE"),
        "ButtonHeaders" => array(),
        "Buttons"       => array(),
        "ViewOnly"      => 1,
        "Params"        => 1,
    );

<?php

    include(EDIT_PATH."/auth.php");

    require_once(COMMON_PATH."/JsHttpRequestLib/JsHttpRequest.php");
    $JsHttpRequest = new JsHttpRequest("utf-8");


    $classTrip       = _autoload("classTrip");
    $classTripOrders = _autoload("classTripOrders");
    $classTripCart   = _autoload("classTripCart");

    $Current = $_REQUEST['place'];
    $Ord_Code = (int)$_REQUEST['Ord_Code'];

    $Order = DB::selectArray("SELECT * FROM cartData WHERE Ord_Code = $Ord_Code");
    if(!$Order['Ord_Code']) {
        ?>Не могу найти такую бронь...<?
        die;
    }

    $cartData = $classTripOrders->GetOrderVars($Order);
    unset($cartData['Ord_Data']);

    foreach($cartData['DatesData'] as $TD_Code => $Date)
        break;

    $Booked = array();

    $Orders = DB::selectArray("SELECT * FROM cartData WHERE T_Code = ".$Order['T_Code']." AND T_Date = '".$Order['T_Date']."' AND Ord_Code != $Ord_Code AND Ord_Status IN (1, 2, 3, 5, 9)", "Ord_Code");
    foreach($Orders as $oKey => $oValue) {
        $oValueData = $classTripOrders->GetOrderVars($oValue);
        foreach($oValueData['Travelers'] as $otKye => $otValue) {
            if($otValue['place'])
                array_push($Booked, $otValue['place']);
        }
    }

    
    $theBus = classBus::GetBus($Date['T_Bus']);

?>
    <script type="text/javascript">
        window.travelersIds = [<? echo join(", ", array_keys($Current)); ?>];
        window.currentTravelerId = 1;

        window.setPlace = function(Object, Place) {
            Object.className = Object.className + " current";
            var curPlace = document.getElementById("legend[" + window.currentTravelerId + "]").innerHTML;
            if(curPlace)
                document.getElementById(curPlace).className = "bus-place";

            document.getElementById("travelers[" + window.currentTravelerId + "][place]").value = Place;
            document.getElementById("legend[" + window.currentTravelerId + "]").innerHTML = Place;
            if(window.currentTravelerId < window.travelersIds.length)
                window.currentTravelerId++;
            else
                window.currentTravelerId = 1;

            return false;
        }

        window.clearPlace = function(Id) {
            var Place = document.getElementById("legend[" + Id + "]").innerHTML;
            document.getElementById("legend[" + Id + "]").innerHTML = "";
            document.getElementById("travelers[" + Id + "][place]").value = "";
            document.getElementById(Place).className = "bus-place";

            return false;
        }
    </script>
<?

    echo "<h3 class=\"bus-name\">".$theBus['Name']."</h3>";

    echo "<div class=\"the-bus-container\">";

    echo "<div class=\"bus-legend\">";
    foreach($cartData['Travelers'] as $tKey => $Traveler) {
        echo "<div class=\"legend-row\"><span>$tKey.</span><span>".$Traveler['lastname']." ".$Traveler['name']." ".$Traveler['middlename']."</span><b id=\"legend[$tKey]\">".$Current[$tKey]."</b> <a href=\"#!\" role=\"button\" class=\"red\" onclick=\"return window.clearPlace($tKey);\"><img src=\"/admin/images/delete.png\"></a></div>";
    }
    echo "<br><br><button class=\"blue\" onclick=\"return Destroy(document.getElementById('win_place'), document.body);\"> &nbsp; Ок &nbsp; </button>";
    echo "</div>";

    echo "<div class=\"the-bus\">";
        echo "<div class=\"bus-row\">
                    <div class=\"driver-place\">&nbsp;</div>
                    <div class=\"bus-empty\">&nbsp;</div>
                    <div class=\"bus-empty\">&nbsp;</div>
                    <div class=\"bus-empty\">&nbsp;</div>
              </div>";
            for($rowKey = 1; $rowKey <= $theBus['rows']; $rowKey++) {
                echo "<div class=\"bus-row\">";
                if($rowKey == $theBus['rows'])
                    $theBus['cols']++;
                for($colKey = 1; $colKey <= $theBus['cols']; $colKey++) {
                    if(!in_array($rowKey.classBus::$ColsNames[$colKey], $theBus['excluded'])) {
                        echo "<div id=\"".$rowKey.classBus::$ColsNames[$colKey]."\" class=\"bus-place";
                        if(in_array($rowKey.classBus::$ColsNames[$colKey], $theBus['reserved']))
                            echo " reserved\"";
                        elseif(is_array($Booked) && in_array($rowKey.classBus::$ColsNames[$colKey], $Booked))
                            echo " booked\"";
                        else {
                            if(is_array($Current) && in_array($rowKey.classBus::$ColsNames[$colKey], $Current))
                                echo " current";
                            echo "\" onclick=\"return window.setPlace(this, '".$rowKey.classBus::$ColsNames[$colKey]."');\"";
                        }
                        echo ">".$rowKey.classBus::$ColsNames[$colKey]."</div>";
                    } else
                        echo "<div class=\"bus-empty\">&nbsp;</div>";

                    if($colKey == $theBus['cols'] / 2 && $rowKey < $theBus['rows'])
                        echo "<div class=\"bus-empty\">&nbsp;</div>";
                }
                echo "</div>";
            }
        echo "</div>";

    echo "</div>";
    

<?php

    include_once(ADMIN_PATH."/setup.php");
    include_once("setup.php");


    AdminHeaders();
    MainNavi("cart");

    echo "<link rel=\"stylesheet\" href=\"".ADMIN_URL."/css/bus.css\" type=\"text/css\">";

    $classTrip       = _autoload("classTrip");
    $classTripOrders = _autoload("classTripOrders");
    $classTripCart   = _autoload("classTripCart");

    $Ord_Code = (int)$_REQUEST['Ord_Code'];

    $Order = DB::selectArray("SELECT *, UNIX_TIMESTAMP(Open_Time) AS Open_Time FROM cartData WHERE Ord_Code = $Ord_Code");
    $cartData = $classTripOrders->GetOrderVars($Order);
    unset($cartData['Ord_Data']);
    
    if(!$Order['Ord_Code']) {
        ?><script>window.location = '<? echo ADMIN_URL."/cart/"; ?>';</script><?
        die;
    }


    // Open_Time обновляется каждые n минут
    $timeToSave = 1;
    if($Order['Open_Time'] > time() - 60 * $timeToSave && !$_REQUEST['openTime']) {
        $Error = true;
        ?>
        <div class="attention">
            Документ открыт только для просмотра!<br>
            <br><a href="#!" onclick="window.location.reload(); return false;">Открыть заново?</a> <i>Ваши данные НЕ будут сохранены</i>
        </div>
        <?
    } else {
        ?>
        <script type="text/javascript">
            var Data = {
                'Ord_Code': <? echo $Ord_Code; ?>
            };

            Ajax.Get("opentime.php", Data, function() {}, "opentime", "loader", false);

            var keyTimeout = null, x = 0;
            window.clearInterval(keyTimeout);

            keyTimeout = window.setInterval(function() {
                if (++x == 10) {
                    window.clearInterval(keyTimeout);
                    var Info = document.createElement("div");
                    Info.className = "information";
                    Info.innerHTML = "Закройте документ!";
                    var EditData = document.getElementById("EditData");
                    EditData.parentNode.insertBefore(Info, EditData);
                }

                Ajax.Get("opentime.php", Data, function() {}, "opentime", "loader", false);
            }, <? echo ($timeToSave * 60000); ?>);
        </script>
        <?
    }


    // openTime передается вместе с сохранением
    if($_REQUEST['Update'] && strtotime($Order['Ord_Change']) > $_REQUEST['openTime']) {
        $Error = true;
        ?>
        <div class="attention">
            Бронь была сохранена на другом устройстве!<br>
            <?
                $events = explode("EDIT:\n", $Order['events']);
                echo "<pre>".end($events)."</pre>";
            ?>
            <br><a href="#!" onclick="window.location.reload(); return false;">Открыть заново?</a> <i>Ваши данные НЕ будут сохранены</i>
        </div>
        <?
        die;
    }


    $Table = "OrderEdit";
    include(EDIT_ONE_PATH);

    echo "<input type=\"hidden\" name=\"openTime\" value=\"".time()."\">";

    foreach($cartData['ItemsData'] as $T_CodeKey => $Item)
        break;
    $Item = $classTrip->GetItemVars($Item, false, false);
    foreach($Item['Dates'] as $Key => $Date) {
        if(date("Y-m-d") > date("Y-m-d", strtotime($Date['T_Date'])) && $Date['T_Date'] != $Order['T_Date'])
            unset($Item['Dates'][$Key]);
    }

    // обновить данные пользователя
    if($Order['us_id'] && $cartData['User']['us_id']) {
        $User = classUsers::GetUserInfo($Order['us_id']);
        $cartData['User'] = $User;
    }
    

    if($_REQUEST['Update'] && Permissions::getPermissions(92)) {
        foreach($cartData['Items'] as $T_Code => $Quantity)
            break;
        foreach($cartData['DatesData'] as $TD_Code => $Date)
            break;
    
        unset($cartData['ExtrasData']);
        $UpdateFlag = false;
        $LogExtras = false;
        $EventsArray = array();

        foreach($_REQUEST['travelers'] as $Key => $Traveler) {
            if($Traveler['delete']) {
                unset($cartData['Travelers'][$Key]);
                $UpdateFlag = true;
            } elseif(!$cartData['Travelers'][$Key]) {
                $cartData['Travelers'][$Key] = array();
                $_REQUEST['travelers'][$Key]['Extras'] = array();
                $UpdateFlag = true;
            }
        }

        $cartData['Items'][$T_Code] = $cartData['ItemsData'][$T_Code]['Quantity'] = $Quantity = count($cartData['Travelers']);
        $cartData['TotalOriginalSum'] = $Item['CartPrice'] * $Quantity;
        $cartData['ItemsData'][$T_Code]['Sum'] = $cartData['TotalOriginalSum'];

        foreach($cartData['Travelers'] as $Key => $Traveler) {
            if(!$Traveler['Extras'])
                $Traveler['Extras'] = array();

            $cartData['Travelers'][$Key]['Extras'] = $_REQUEST['travelers'][$Key]['Extras'];

            if($_REQUEST['travelers'][$Key]['Extras']) {
                foreach($_REQUEST['travelers'][$Key]['Extras'] as $eKey => $eValue) {
                    if(!in_array($eKey, array_keys($Traveler['Extras']))) { // в $_REQUEST новые значения
                        $cartData['Travelers'][$Key]['Extras'][$eKey] = 1;
//                        $cartData['TotalSum'] += $Item['Extras'][$eKey]['dE_Price'];
                        $UpdateFlag = true;
                        $LogExtras = "\n                 false => ".$Item['Extras'][$eKey]['dI_Name'];
                    }
                }
            }
            foreach($Traveler['Extras'] as $oeKey => $oeValue) {
                if(!$_REQUEST['travelers'][$Key]['Extras'] || !in_array($oeKey, array_keys($_REQUEST['travelers'][$Key]['Extras']))) { // в старых значениях есть то чего нет в $_REQUEST
                    unset($cartData['Travelers'][$Key]['Extras'][$oeKey]);
//                    $cartData['TotalSum'] -= $Item['Extras'][$oeKey]['dE_Price'];
                    $UpdateFlag = true;
                        $LogExtras = "\n                 ".$Item['Extras'][$oeKey]['dI_Name']." => false";
                }
            }

            if($LogExtras)
                $EventsArray['Travelers '.$Key] .= "\n            Extras:$LogExtras";

            if(
                $Traveler['name'] != $_REQUEST['travelers'][$Key]['name'] ||
                $Traveler['lastname'] != $_REQUEST['travelers'][$Key]['lastname'] ||
                $Traveler['middlename'] != $_REQUEST['travelers'][$Key]['middlename'] ||
                $Traveler['sex'] != $_REQUEST['travelers'][$Key]['sex'] ||
                $Traveler['birthdate'] != $_REQUEST['travelers'][$Key]['birthdate'] ||
                $Traveler['kid'] != $_REQUEST['travelers'][$Key]['kid'] ||
                $Traveler['passport']['number'] != $_REQUEST['travelers'][$Key]['passport']['number'] ||
//                $Traveler['passport']['date'] != $_REQUEST['travelers'][$Key]['passport']['date'] ||
                $Traveler['place'] != $_REQUEST['travelers'][$Key]['place']
                ) {
                    $newTraveler = $_REQUEST['travelers'][$Key];

                    $EventsArray['Travelers '.$Key] = "";
                    foreach($_REQUEST['travelers'][$Key] as $rKey => $rValue) {
                        if($Traveler[$rKey] != $_REQUEST['travelers'][$Key][$rKey] && !is_array($rValue))
                            $EventsArray['Travelers '.$Key] .= "\n            $rKey: ".$Traveler[$rKey]." => ".$_REQUEST['travelers'][$Key][$rKey];
                        elseif(is_array($rValue) && $rKey != "Extras") {
                            $EventsArray['Travelers '.$Key] .= "\n            $rKey:";
                            foreach($rValue as $rValueKey => $rValueValue)
                                $EventsArray['Travelers '.$Key] .= "\n                  $rValueKey: ".$Traveler[$rKey][$rValueKey]." => ".$_REQUEST['travelers'][$Key][$rKey][$rValueKey];
                        }

                        if($rKey == "place") {
                            // TODO проверить места
                        }
                    }

                    // TODO проверить наличие мест

                    $cartData['Travelers'][$Key] = $newTraveler;
                    $UpdateFlag = true;
                }
    
            if($cartData['Travelers'][$Key]['kid'] && $Item['T_KidsDiscount']) {
                $cartData['TotalOriginalSum'] = $cartData['TotalOriginalSum'] - (float)preg_replace('/\s+/', '', $Item['T_KidsDiscount']);
            }

        }

        if($_REQUEST['Pincode']) {
            $cartData['Pincode'] = $classTripCart->CheckPin();
        }
    
        if($Order['Ord_Discount'] != $_REQUEST['Ord_Discount']) {
            $EventsArray['Ord_Discount'] = "\n            ".$Order['Ord_Discount']." => ".$_REQUEST['Ord_Discount'];
            $Order['Ord_Discount'] = (int)$_REQUEST['Ord_Discount'];
            $UpdateFlag = true;
        }
    
        if($Order['Ord_DiscountRub'] != $_REQUEST['Ord_DiscountRub']) {
            $EventsArray['Ord_DiscountRub'] = "\n            ".$Order['Ord_DiscountRub']." => ".$_REQUEST['Ord_DiscountRub'];
            $Order['Ord_DiscountRub'] = (int)$_REQUEST['Ord_DiscountRub'];
            $UpdateFlag = true;
        }
    
        if(!$Order['Ord_DiscountRub'] && !$Order['Ord_Discount'] && !($cartData['User']['Company'] && $cartData['User']['Company']['C_Discount']) && $cartData['User']['Discount'])
            unset($cartData['Discount']);

        unset($cartData['Discount']);
        if($Order['Ord_DiscountRub']) {
            $cartData['Discount']['Name'] = "Скидка на бронь";
            $cartData['Discount']['Value'] = $Order['Ord_DiscountRub'];
            $cartData['Discount']['Sum'] = $cartData['Discount']['Value'];
            $cartData['TotalSum'] = $cartData['TotalOriginalSum'] - $cartData['Discount']['Sum'];
        } elseif($Order['Ord_Discount']) {
            unset($cartData['Discount']['Value']);
            $cartData['Discount']['Name'] = "Скидка на бронь";
            $cartData['Discount']['Amount'] = $Order['Ord_Discount'];
            $cartData['Discount']['Sum'] = round($cartData['TotalOriginalSum'] / 100 * $cartData['Discount']['Amount']);
            $cartData['TotalSum'] = $cartData['TotalOriginalSum'] - $cartData['Discount']['Sum'];
        } elseif($cartData['User']['Company'] && $cartData['User']['Company']['C_Discount']) {
            unset($cartData['Discount']['Value']);
            $cartData['Discount']['Name'] = "Скидка компании";
            $cartData['Discount']['Amount'] = round((int)$cartData['User']['Company']['C_Discount'] * (float)$Item['T_Discount']);
            $cartData['Discount']['Sum'] = round($cartData['TotalOriginalSum'] / 100 * $cartData['Discount']['Amount']);
            $cartData['TotalSum'] = $cartData['TotalOriginalSum'] - $cartData['Discount']['Sum'];
        } elseif($cartData['User']['Discount']) {
            unset($cartData['Discount']['Value']);
            $cartData['Discount']['Name'] = "Персональная скидка";
//            $cartData['Discount']['Amount'] = round($cartData['User']['Discount'] * $Item['T_Discount']);
            $cartData['Discount']['Amount'] = (float)$cartData['User']['Discount'];
            $cartData['Discount']['Sum'] = round($cartData['TotalOriginalSum'] / 100 * $cartData['Discount']['Amount']);
            $cartData['TotalSum'] = $cartData['TotalOriginalSum'] - $cartData['Discount']['Sum'];
        } elseif($cartData['Pincode'] && $cartData['Pincode']['P_Discount'] > $cartData['User']['Discount']) {
            unset($cartData['Discount']['Value']);
            $cartData['Discount']['Name'] = "Скидка по пин-коду (".$cartData['Pincode']['P_Name'].")";
            $cartData['Discount']['Amount'] = (float)$cartData['Pincode']['P_Discount'];
            $cartData['Discount']['Sum'] = round($cartData['TotalOriginalSum'] / 100 * $cartData['Discount']['Amount']);
            $cartData['TotalSum'] = $cartData['TotalOriginalSum'] - $cartData['Discount']['Sum'];
        } else
            $cartData['TotalSum'] = $cartData['TotalOriginalSum'];
        
        $cartData['ExtrasTotalSum'] = 0;
        foreach($cartData['Travelers'] as $Key => $Traveler) {
            if($Traveler['Extras']) {
                foreach($Traveler['Extras'] as $oeKey => $oeValue) {
                    if(!$cartData['ExtrasData'][$oeKey])
                        $cartData['ExtrasData'][$oeKey] = $Item['Extras'][$oeKey];
    
//                    echo "<hr>";
//                    echo $cartData['ExtrasData'][$oeKey]['Quantity']."<br>";
//                    echo $cartData['ExtrasData'][$oeKey]['Sum']." += ".$Item['Extras'][$oeKey]['dE_Price']."<br>";
//                    echo $cartData['ExtrasTotalSum']." += ".$Item['Extras'][$oeKey]['dE_Price']."<br>";
                    
                    $cartData['ExtrasData'][$oeKey]['Quantity']++;
                    $cartData['ExtrasData'][$oeKey]['Sum'] += $Item['Extras'][$oeKey]['dE_Price'];
                    $cartData['ExtrasTotalSum'] += $Item['Extras'][$oeKey]['dE_Price'];
                    $cartData['Extras'][$oeKey] = $cartData['ExtrasData'][$oeKey]['Quantity'];

//                    echo "<hr>";
//                    echo $cartData['ExtrasData'][$oeKey]['Quantity']."<br>";
//                    echo $cartData['ExtrasData'][$oeKey]['Sum']."<br>";
//                    echo $cartData['ExtrasTotalSum']."<br>";

                }
            }
        }
        $cartData['TotalSum'] += $cartData['ExtrasTotalSum'];

        if((int)$Order['Ord_Sum'] != $cartData['TotalSum']) {
            $newSum = $cartData['TotalSum'];
            $EventsArray['TotalSum']  = "\n            ".$Order['Ord_Sum']." => ".$newSum;
            $UpdateFlag = true;
        }

        if(
            $cartData['Delivery']['Delivery_Name'] != $_REQUEST['Delivery_Name'] ||
            $cartData['Delivery']['Delivery_Addr'] != $_REQUEST['Delivery_Addr'] ||
            $cartData['Delivery']['Delivery_Comment'] != $_REQUEST['Delivery_Comment']
            ) {
                $newDelivery['Delivery_Name'] = $_REQUEST['Delivery_Name'];
                $newDelivery['Delivery_Addr'] = $_REQUEST['Delivery_Addr'];
                $newDelivery['Delivery_Comment'] = $_REQUEST['Delivery_Comment'];
                $EventsArray['Delivery']  = "\n            ".$cartData['Delivery']['Delivery_Name']." => ".$newDelivery['Delivery_Name'];
                $EventsArray['Delivery'] .= "\n            ".$cartData['Delivery']['Delivery_Addr']." => ".$newDelivery['Delivery_Addr'];
                $EventsArray['Delivery'] .= "\n            ".$cartData['Delivery']['Delivery_Comment']." => ".$newDelivery['Delivery_Comment'];
                $cartData['Delivery'] = $newDelivery;
                $UpdateFlag = true;
            }

        if($_REQUEST['Ord_Status'] && $Order['Ord_Status'] != $_REQUEST['Ord_Status']) {
            $cartData['Ord_Status'] = (int)$_REQUEST['Ord_Status'];
            $EventsArray['Status'] = "\n            ".classTripCart::$OrderStatuses[$Order['Ord_Status']]." => ".classTripCart::$OrderStatuses[$_REQUEST['Ord_Status']];
            $UpdateFlag = true;

            $classTripCart = _autoload("classTripCart", "tripCart");
            $classTripCart->sendAdminMail($Ord_Code, array("Subject" => "Изменение статуса брони ".$Order['Ord_Number']." на сайте ".DOMAIN_NAME, "Ord_Status" => $cartData['Ord_Status']), false);
        }

        if($Order['T_Date'] != $_REQUEST['date']) {
            $newDate = dateDecode($_REQUEST['date']);
            $EventsArray['TripDate'] = "\n            ".$Order['T_Date']." => ".$newDate;
            $cartData['Dates'][$T_Code] = $newDate;
            $UpdateFlag = true;
        }
        
        if($UpdateFlag) {
            $DBCartData = json_encode($cartData);
            DB::Query("UPDATE cartData SET ".
                ($newDate ? "T_Date = '$newDate', " : "").
                ($newSum ? "Ord_Sum = '$newSum', " : "").
                "Ord_Discount = ".((int)$_REQUEST['Ord_Discount'] ?: 'NULL').
                ", Ord_DiscountRub = ".((int)$_REQUEST['Ord_DiscountRub'] ?: 'NULL').
                ", Ord_Status = ".(int)$_REQUEST['Ord_Status'].
                ", Ord_Data = '".mysql_real_escape_string($DBCartData)."', events = CONCAT(events, '".classUserEvents::Events("EDIT", $EventsArray)."') WHERE Ord_Code = $Ord_Code");
        }

        classTripCart::Count($Order['T_Code']);

        $Order = DB::selectArray("SELECT * FROM cartData WHERE Ord_Code = $Ord_Code");
        $cartData = $classTripOrders->GetOrderVars($Order);
        unset($cartData['Ord_Data']);

    }

?>
    <table width="100%" class="border">
        <tr><th colspan="2" class="border">Дополнительные параметры:</th></tr>
        <tr>
            <td class="border">Статус</td>
            <td class="border">
                <?
                    $OrderStatus = new InputStatusSelectAlert("Ord_Status", $Order['Ord_Status']);
                    if(!$cartData['User']['status'])
                        $OrderStatus->Disabled = true;
                    $OrderStatus->dPrint();
                ?>
            </td>
        </tr>
        <tr>
            <td class="border">Пользователь</td>
            <td class="border">
                <?
                    echo "<a href=\"".ADMIN_URL."/siteusers/edit.php?us_id=".$cartData['User']['us_id']."\" target=\"_blank\"><b>".$cartData['User']['lastname']." ".$cartData['User']['name']." ".$cartData['User']['middlename']."</b></a>";
                    echo "<br>".$cartData['User']['phone']."<br>".$cartData['User']['mail'];
                    if($cartData['User']['Company']) {
                        echo "<br><b>".$cartData['User']['Company']['C_Name']."</b><br>".$cartData['User']['Company']['C_Phone'];
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td class="border">Тур</td>
            <td class="border">
                <?
                    echo "<h3><a href=\"".ADMIN_URL."/travel/edit.php?T_Code=".$Item['T_Code']."\" target=\"_blank\">".$Item['T_Name']."</a></h3>";
                    echo "<select name=\"date\">";
                    foreach($Item['Dates'] as $rKey => $Date) {
                        echo "<option value=\"".$Date['T_Date']."\"";
                        if($Date['T_Date'] == $Order['T_Date'])
                          echo " selected";
                        echo ">".$Date['T_DateStr']."</option>";
                    }
                    echo "</select>";
                    echo "<ul><lh>В тур включено:</lh>";
                    foreach($Item['Included'] as $iKey => $iValue) {
                        echo "<li>".$iValue['dI_Name']."</li>";
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td class="border">Туристы</td>
            <td class="border">
                <?
                    foreach($cartData['Travelers'] as $tKey => $Traveler) {
                        echo "<h3>Турист $tKey</h3>";
                        echo "<table width=\"100%\" class=\"border\">";
                        echo "<tr><td class=\"border\">ФИО</td><td class=\"border\" style=\"white-space: nowrap;\">";
                        $tLastName = new InputText("travelers[$tKey][lastname]", $Traveler['lastname'], 20, "font-size:110%;margin-bottom:0.2em;");
                        $tLastName->dPrint();
                        $tName = new InputText("travelers[$tKey][name]", $Traveler['name'], 20, "font-size:110%;margin-bottom:0.2em;");
                        $tName->dPrint();
                        $tMiddleName = new InputText("travelers[$tKey][middlename]", $Traveler['middlename'], 20, "font-size:110%;margin-bottom:0.2em;");
                        $tMiddleName->dPrint();
                        if($tKey > 1)
                            echo "<input type=\"checkbox\" name=\"travelers[$tKey][delete]\" id=\"travelers[$tKey][delete]\" value=\"true\" style=\"visibility: hidden\" onchange=\"this.form.submit();\"><label role=\"button\" for=\"travelers[$tKey][delete]\" class=\"red\"><img src=\"".ADMIN_URL."/images/delete.png\"></label>";
                        echo "</td></tr>";
                        echo "<tr><td class=\"border\">Пол</td><td class=\"border\">";
                        $tSex = new InputRadio("travelers[$tKey][sex]", 1, $Traveler['sex'], "мужской");
                        $tSex->dPrint();
                        $tSex = new InputRadio("travelers[$tKey][sex]", 2, $Traveler['sex'], "женский");
                        $tSex->dPrint();
                        echo "</td></tr>";
                        echo "<tr><td class=\"border\">Дата рождения</td><td class=\"border\">";
                        $tBirthdate = new T10("travelers[$tKey][birthdate]", $Traveler['birthdate'], 10);
                        $tBirthdate->dPrint();
                        echo "</td></tr><tr style=\"".($color ? "background-color:#F9F9F9;" : "")."\"><td class=\"border\">Ребенок до 14 лет</td><td class=\"border\">";
                        $tKid = new InputCheckbox("travelers[$tKey][kid]", $Traveler['kid'], "", "да");
                        $tKid->dPrint();
                        echo "</td></tr>";
                        echo "<tr><td class=\"border\">Паспорт</td><td class=\"border\">";
                        $tPassport = new T10("travelers[$tKey][passport][number]", $Traveler['passport']['number']);
                        $tPassport->dPrint();
//                        $tPassport = new T10("travelers[$tKey][passport][date]", $Traveler['passport']['date']);
//                        $tPassport->dPrint();
                        echo "</td></tr>";
                        echo "<tr><td class=\"border\">Дополнительно приобретено:</td><td class=\"border\">";
                        echo "<ul>";
                        foreach($Item['Extras'] as $eKey => $eValue) {
                            echo "<li><label><input type=\"checkbox\" name=\"travelers[$tKey][Extras][$eKey]\"".($Traveler['Extras'][$eKey] ? " checked" : "")."> ".$eValue['dI_Name']." ".$eValue['dE_Price']." руб.</label></li>";
                        }
                        echo "</ul>";
                        echo "</td></tr>";
                        echo "<tr><td class=\"border\">Место в автобусе:</td><td class=\"border\">";
//                        $tPlace = new InputHidden("travelers[$tKey][place]", $Traveler['place']);
//                        $tPlace->dPrint();
                        $tPlace = new T5RO("travelers[$tKey][place]", $Traveler['place']);
                        $tPlace->dPrint();
                        $checked = "";
                        foreach($cartData['Travelers'] as $tpKey => $tpTraveler)
                            $checked .= "&place[$tpKey]=".$tpTraveler['place'];
                        echo "<a href=\"#!\" role=\"button\" onclick=\"return openUrl('bus.php?Ord_Code=$Ord_Code".$checked."', 'place', this, true, 600, 650, 'Выбрать место в автобусе');\">Выбрать место</a>";
                        echo "</td></tr>";
                        echo "</table><br>";
                    }
                ?>
                <input type="checkbox" name="travelers[<? echo end(array_keys($cartData['Travelers'])) + 1; ?>][]" id="travelers[add]" value="" style="visibility: hidden" onchange="this.form.submit();"><label class="blue" role="button" for="travelers[add]">Добавить туриста</label>
            </td>
        </tr>

        <? if($cartData['Delivery']) { ?>
        <tr>
            <td class="border">Доставка</td>
            <td class="border">
                <?
                    $dAddr = new InputText("Delivery_Name", $cartData['Delivery']['Delivery_Name'], 20, "margin-bottom:0.2em");
                    $dAddr->dPrint();
                    echo "<br>";
                    $dAddr = new InputText("Delivery_Addr", $cartData['Delivery']['Delivery_Addr'], 50, "margin-bottom:0.2em");
                    $dAddr->dPrint();
                    echo "<br>";
                    $dAddr = new TA30("Delivery_Comment", $cartData['Delivery']['Delivery_Comment']);
                    $dAddr->dPrint();
                ?>
            </td>
        </tr>
        <? } ?>

        <? if($cartData['Payment']) { ?>
        <tr>
            <td class="border">Оплата</td>
            <td class="border">
                <?
                    echo $cartData['Payment']['PS_Name']."<br>";
                    echo "paymentType: ".$cartData['Payment']['paymentType']."<br>";
                    echo "invoiceId: ".$cartData['Payment']['invoiceId']."<br>";
                    echo "paymentPayerCode: ".$cartData['Payment']['paymentPayerCode'];
                ?>
            </td>
        </tr>
        <? } ?>

        <? if($cartData['User']['Company']) { ?>
        <tr>
            <td class="border">Телефон для связи с группой/туристом</td>
            <td class="border">
                <?
                    $tTPhone = new T15("TravelerPhone", $cartData['TravelerPhone']);
                    $tTPhone->dPrint();
                ?>
            </td>
        </tr>
        <tr>
            <td class="border">Комментарий к брони</td>
            <td class="border">
                <?
                    $tTComment = new TA50("TravelerComment", $cartData['TravelerComment']);
                    $tTComment->dPrint();
                ?>
            </td>
        </tr>
        <? } ?>

        <tr>
            <td class="border">Скидка на бронь <small>(%)</small></td>
            <td class="border">
                <?
                    $tDiscount = new T5("Ord_Discount", $cartData['Ord_Discount']);
                    $tDiscount->dPrint();
                ?>
            </td>
        </tr>

        <tr>
            <td class="border">Скидка на бронь <small>(руб)</small></td>
            <td class="border">
                <?
                    $tDiscount = new T5("Ord_DiscountRub", $cartData['Ord_DiscountRub']);
                    $tDiscount->dPrint();
                ?>
            </td>
        </tr>

        <tr>
            <td class="border">Пин-код</td>
            <td class="border">
                <?
                    $pDiscount = new T15("Pincode", $cartData['Pincode']['P_Barcode']);
                    $pDiscount->dPrint();
                ?>
            </td>
        </tr>

        <? if($cartData['Discount']['Amount']) { ?>
        <tr>
            <td class="border">Скидка</td>
            <td class="border">
                <?
                    echo $cartData['Discount']['Name']."<br>".($cartData['Discount']['Value'] ? $cartData['Discount']['Value']." руб." : $cartData['Discount']['Amount']."%")." = ".number_format($cartData['Discount']['Sum'], 2, ".", " ")." руб.";
                ?>
            </td>
        </tr>
        <? } ?>

        <tr>
            <td class="border"><b>Итого</b></td>
            <td class="border"><b><? echo number_format($cartData['TotalSum'], 2, ".", " "); ?></b> руб.</td>
        </tr>
    </table>

<? if(Permissions::getPermissions(92) && !$Error) { ?>
        <div id="SaveIt">
            <input type="hidden" name="Update" value="Update">
            <input type="submit" value="Сохранить" class="green">
            <?
                $InputButton = new GoDelOrder($Order['Ord_Code']);
                $InputButton->dPrint();
            ?>
        </div>
<? } ?>

    </form>

    </fieldset>
</div>

<? if($_REQUEST['debug']) { ?>
    <pre><? print_r($Order); ?></pre>
    <pre><? print_r($cartData); ?></pre>
<? } ?>

<? if(Permissions::getPermissions(94)) { ?>
    <div style="width:60%;margin:2em 10%;">
        <fieldset><legend class="Header">Events:</legend>
            <pre><? echo $Order['events']; ?></pre>
        </fieldset>
    </div>
<? } ?>

<?


    AdminFooter();

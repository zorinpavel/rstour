<?php

    include_once(ADMIN_PATH."/setup.php");
    include_once("setup.php");


    AdminHeaders();
    MainNavi("cart");

?>
    <div style="width:40%;margin-left:10%;">
        <fieldset><legend class="Header">Введите один или несколько параметров для поиска:</legend>
            <form enctype="multipart/form-data" name="Main" method="GET">
                <table>
                    <tr>
                        <td>Номер брони</td>
                        <td>
                            <?
                                $InputText = new T20("Ord_Number", $_REQUEST['Ord_Number']);
                                $InputText->dPrint();
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Дата тура</td>
                        <td>
                            <?
                                $InputDate = new InputDatePicker("T_Date", $_REQUEST['T_Date']);
                                $InputDate->dPrint();
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Агентство</td>
                        <td>
                            <?
                                $InputText = new T20("C_Name", $_REQUEST['C_Name']);
                                $InputText->dPrint();
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>ФИО пользователя</td>
                        <td>
                            <?
                                $InputText = new T20("User", $_REQUEST['User']);
                                $InputText->dPrint();
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Код пользователя (us_id)</td>
                        <td>
                            <?
                                $InputText = new T20("us_id", $_REQUEST['us_id']);
                                $InputText->dPrint();
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Название тура или код</td>
                        <td>
                            <?
                                $InputText = new T20("T_Name", $_REQUEST['T_Name']);
                                $InputText->dPrint();
                            ?>
                        </td>
                    </tr>
                </table>
                <div id="SaveIt">
                    <input type="submit" value="Искать" class="green">
                    <button type="submit" name="All" value="1" class="blue">Показать все заказы</button>
                </div>
            </form>
        </fieldset>
    </div>
<?
    $BlankColor  = "#FFFFFF";
    $TodayColor  = "#E5FFE5";
    $FutureColor = "#FFFFCC";
    $ArchColor   = "#EEEEEE";
    $AlertColor  = "#FEE6E6";

    if($_REQUEST['Ord_Number'])
        $Cond[] = "(Ord_Number LIKE '%".$_REQUEST['Ord_Number']."%' OR Ord_Code = ".(int)$_REQUEST['Ord_Number'].")";
    if($_REQUEST['T_Date'])
        $Cond[] = "T_Date = '".dateDecode($_REQUEST['T_Date'])."'";
    if($_REQUEST['C_Name']) {
        $Companies = DB::selectArray("SELECT * FROM Companies WHERE C_Name LIKE '%".$_REQUEST['C_Name']."%'", "C_Code");
        $Cond[] = "C_Code IN (".join(", ", array_keys($Companies)).")";
    }
    if($_REQUEST['User']) {
        $Users = DB::selectArray("SELECT * FROM Users WHERE name LIKE '%".$_REQUEST['User']."%' OR lastname LIKE '%".$_REQUEST['User']."%'", "us_id");
        $Cond[] = "us_id IN (".join(", ", array_keys($Users)).")";
    }
    if($_REQUEST['us_id']) {
        $Cond[] = "us_id = ".(int)$_REQUEST['us_id'];
    }
    if($_REQUEST['T_Name']) {
        $Tours = DB::selectArray("SELECT * FROM tripItems WHERE T_Name LIKE '%".$_REQUEST['T_Name']."%' OR T_Barcode LIKE '%".$_REQUEST['T_Name']."%'", "T_Code");
        $Cond[] = "T_Code IN (".join(", ", array_keys($Tours)).")";
    }
    if($_REQUEST['All']) {
        $Cond[] = "true";
    }

    if(count($Cond)) {
        ?>
            <div class="EditData">
                <fieldset><legend class="Header">Резйльтаты поиска:</legend>
                    <table class="border" width="100%">
        <?
        $Conditions = "WHERE ".join(" AND ", $Cond);
        $Orders = DB::selectArray("SELECT * FROM cartData $Conditions", "Ord_Code");

        $OrderCond = array("Ord_Status", "ASC", "Ord_Change", "DESC", "Ord_Code", "DESC");
        $Orders = SortArray($Orders, $OrderCond);

        foreach($Orders as $Ord_Code => $Order) {
            switch($Order['Ord_Status']) {
                case 1:
                case 9:
                    $BackgroundColor = $FutureColor;
                    break;
                case 4:
                    $BackgroundColor = $AlertColor;
                    break;
                case 6:
                case 7:
                    $BackgroundColor = $ArchColor;
                    break;
                case 3:
                    $BackgroundColor = $TodayColor;
                    break;
                case 2:
                case 5:
                default:
                    $BackgroundColor = $BlankColor;
                    break;
            }
            echo "<tr style=\"background-color: $BackgroundColor;\"><td class=\"border\">";
            $InputLabel = new T20RO("Ord_Number", $Order['Ord_Number']);
            $InputLabel->dPrint();
            echo "<br>";
            $InputLabel = new InputLabel("Ord_Status", classTripCart::$OrderStatuses[$Order['Ord_Status']]);
            $InputLabel->dPrint();
            echo "</td><td class=\"border\">";
            $cartContentElem = new cartContentElem("Ord_Data", $Order['Ord_Data']);
            $cartContentElem->dPrint();
            echo "</td><td class=\"border\">";
            $InputButton = new GoOrderEdit($Order['Ord_Code']);
            $InputButton->dPrint();
            $InputButton = new GoDelOrder($Order['Ord_Code']);
            $InputButton->dPrint();
            echo "</td></tr>";
        }
        ?>
                    </table>
                </fieldset>
            </div>
        <?
    }

    AdminFooter();
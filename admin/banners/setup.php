<?php
    
    
    class GoEditTmpl extends InputButton {
        function GoEditTmpl($Code) {
            $this->InputButton("Qua$Code", "Редактировать", "", "window.location.href = 'edit_template.php?Tmp_Code=$Code';", "green");
        }
    }
    
    class DelBanner extends InputButton {
        function DelBanner($Code) {
            $this->InputButton("Qua$Code", "Удалить", "", "if(confirm('Вы действительно хотите удалить баннер и всю статистику по нему?')) { openUrl('del_banner.php?Hnt_Code=$Code', 'delBan', this, true, 0, 100, 'Удалить баннер'); }", "red");
        }
    }
    
    class ParamTypeInputSelect extends InputSelect {
        function ParamTypeInputSelect($Name, $Value) {
            $Options = array(
                "1"=>"Текстовое поле",
                "2"=>"Большое текстовое поле",
                "3"=>"Загрузка файла",
            );
            $this->InputSelect($Name, $Value, $Options);
        }
    }
    
    class GoStat extends InputButton {
        function GoStat($Code) {
            $this->InputButton("Qua$Code", "Статистика", "", "window.location.href = 'stat.php?Hnt_Code=$Code';");
        }
    }
    
    class GoAccEdit extends InputButton {
        function GoAccEdit($Code) {
            $this->InputButton("Qua$Code", "Редактировать", "", "window.location.href = 'accaunt_edit.php?Acc_Code=$Code';", "green");
        }
    }
    
    class GoAccBanners extends InputButton {
        function GoAccBanners($Code) {
            $this->InputButton("Qua$Code", "Баннеры", "", "window.location.href = 'banners.php?Acc_Code=$Code';", "blue");
        }
    }
    
    class GoPlaBanners extends InputButton {
        function GoPlaBanners($Code) {
            $this->InputButton("Qua$Code", "Баннеры", "", "window.location.href = 'banners.php?Pla_Code=$Code';", "blue");
        }
    }
    
    class UsersDBSelect extends InputDBSelect {
        function UsersDBSelect($Name, $Value, $onChange="") {
            $this->InputDBSelect($Name, $Value, "Users", "us_id", "login", "status = 1", "us_id", "", $onChange);
            $this->Empty = 1;
        }
    }
    
    class bannerTemplatesDBSelect extends InputDBSelect {
        function bannerTemplatesDBSelect($Name, $Value, $onChange="") {
            $this->InputDBSelect($Name, $Value, "bannerTemplates", "Tmp_Code", "Tmp_Name", "", "Tmp_Order DESC", "", $onChange);
            $this->Empty = 1;
        }
    }
    
    class bannerAccauntsDBSelect extends InputDBSelect {
        function bannerAccauntsDBSelect($Name, $Value, $onChange="") {
            $this->InputDBSelect($Name, $Value, "bannerAccaunts", "Acc_Code", "Acc_Name", "", "Acc_Code DESC", "", $onChange);
        }
    }
    
    class BannerStatDBSelect extends InputDBSelect {
        function BannerStatDBSelect($Name, $Value, $onChange="") {
            $this->InputDBSelect($Name, $Value, "bannerHints", "Hnt_Code", "Hnt_Name", "", "Hnt_Code DESC", "", $onChange);
        }
    }
    
    class GoEditHint extends InputButton {
        function GoEditHint($Code) {
            $this->InputButton("Qua$Code", "Параметры", "", "window.location.href = 'hint_edit.php?Hnt_Code=$Code';", "green");
        }
    }
    
    $Accaunts = array(
        "TableName" => "bannerAccaunts",
        "Id"            => "Acc_Code",
        "Order"         => "Acc_Code",
        "Fields"        => array("Acc_Name", "Acc_Active"),
        "Types"         => array("T30", "InputCheckbox"),
        "Uploads"       => array(),
        "Header"        => "Список аккаунтов",
        "Headers"       => array("Название", "Активный"),
        "Buttons"       => array("GoAccEdit", "GoAccBanners"),
        "ButtonHeaders" => array("&nbsp;", "&nbsp;"),
        "Filters"       => array(),
        "FiltersNames"  => array(),
        "FiltersTypes"   => array(),
        "FiltersCompare" => array(),
        "EditOnly" => 0
    );
    
    $AccauntEdit = array(
        "TableName" => "bannerAccaunts",
        "Id"            => "Acc_Code",
        "IdName"        => "Acc_Name",
        "Fields"        => array("Acc_Name", "us_id", "Acc_Active"),
        "Types"         => array("T30", "UsersDBSelect", "InputCheckbox"),
        "Headers"       => array("Название", "Пользоватль", "Активный"),
        "Header"        => "Редактировать аккаунт",
        "Buttons"       => array("GoAccBanners"),
        "ViewOnly" => 0,
        "Params" => 0,
    );
    
    $HintEdit = array(
        "TableName" => "bannerHints",
        "Id"            => "Hnt_Code",
        "IdName"        => "Hnt_Name",
        "Fields"        => array("Hnt_Name", "Pla_Code", "Acc_Code", "Tmp_Code", "Hnt_Url", "Hnt_Stat", "Hnt_Active"),
        "Types"         => array("T30", "bannerPlacesDBSelect", "bannerAccauntsDBSelect", "bannerTemplatesDBSelect", "T30", "InputCheckbox", "InputCheckbox"),
        "Headers"       => array("Название", "Место", "Аккаунт", "Шаблон", "URL", "Вести статистику", "Активный"),
        "Header"        => "Редактировать беннер",
        "Buttons"       => array(),
        "ViewOnly" => 0,
        "Params" => 1,
    );
    
    $Places = array(
        "TableName" => "bannerPlaces",
        "Id"            => "Pla_Code",
        "Order"         => "Pla_Order DESC, Pla_Code",
        "Fields"        => array("Pla_Code", "Pla_Name", "Pla_Order"),
        "Types"         => array("InputHidden", "T30", "T5"),
        "Uploads"       => array(),
        "Header"        => "Рекламные места",
        "Headers"       => array("", "Название места", "Порядок<br>вывода"),
        "Buttons"       => array("GoPlaBanners"),
        "ButtonHeaders" => array("&nbsp;"),
        "Filters"       => array(),
        "FiltersNames"  => array(),
        "FiltersTypes"   => array(),
        "FiltersCompare" => array(),
        "EditOnly" => 0
    );
    
    $Templates = array(
        "TableName" => "bannerTemplates",
        "Id"            => "Tmp_Code",
        "Order"         => "Tmp_Order DESC, Tmp_Code DESC",
        "Fields"        => array("Tmp_Name", "Tmp_Order"),
        "Types"         => array("T30", "T5"),
        "Uploads"       => array(),
        "Header"        => "Шаблоны для баннеров",
        "Headers"       => array("Название шаблона", "Порядок<br>вывода"),
        "Buttons"       => array("GoEditTmpl"),
        "ButtonHeaders" => array("&nbsp;"),
        "Filters"       => array(),
        "FiltersNames"  => array(),
        "FiltersTypes"   => array(),
        "FiltersCompare" => array(),
        "EditOnly" => 0
    );
    
    $BannersAcc = array(
        "TableName" => "bannerHints",
        "Id"            => "Hnt_Code",
        "Order"         => "Hnt_Order DESC, Hnt_Active DESC, Hnt_Code DESC",
        "Fields"        => array("Pla_Code", "Acc_Code", "Hnt_Name", "Tmp_Code", "Hnt_Active"),
        "Types"         => array("InputHidden", "InputHidden", "T30", "bannerTemplatesDBSelect", "InputCheckbox"),
        "Uploads"       => array(),
        "Header"        => "Рекламные баннеры",
        "Headers"       => array("", "", "Название баннера", "Шаблон баннера", "[X]"),
        "Buttons"       => array("GoEditHint", "DelBanner", "GoStat"),
        "ButtonHeaders" => array("&nbsp;", "&nbsp;", "&nbsp;"),
        "Filters"       => array("Acc_Code"),
        "FiltersNames"  => array("Аккаунт"),
        "FiltersTypes"   => array("bannerAccauntsDBSelect"),
        "FiltersCompare" => array("N"),
        "EditOnly" => 0
    );
    
    $BannersPla = array(
        "TableName" => "bannerHints",
        "Id"            => "Hnt_Code",
        "Order"         => "Hnt_Order DESC, Hnt_Active DESC, Hnt_Code DESC",
        "Fields"        => array("Pla_Code", "Acc_Code", "Hnt_Name", "Tmp_Code", "Hnt_Active", "Hnt_Order"),
        "Types"         => array("InputHidden", "InputHidden", "T30", "bannerTemplatesDBSelect", "InputCheckbox", "T5"),
        "Uploads"       => array(),
        "Header"        => "Рекламные баннеры",
        "Headers"       => array("", "", "Название баннера", "Шаблон баннера", "[X]", "Порядок<br>вывода"),
        "Buttons"       => array("GoEditHint", "DelBanner", "GoStat"),
        "ButtonHeaders" => array("&nbsp;", "&nbsp;", "&nbsp;"),
        "Filters"       => array("Pla_Code"),
        "FiltersNames"  => array("Рекламное место"),
        "FiltersTypes"   => array("bannerPlacesDBSelect"),
        "FiltersCompare" => array("N"),
        "EditOnly" => 0
    );
    
    
    define("BANNERS_STAT_PATH", ADMIN_URL."/banners/stat");
    
    $Stat = array(
        "TableName" => "bannerStat",
        "Header"        => "Рекламные баннеры",
        "Id"            => "Hnt_Code",
        "Order"         => "St_Time",
        "Fields"        => array("Hnt_Code", "Hnt_Show", "Hnt_Click", "St_Time"),
        "Filters"       => array("Hnt_Code"),
        "FiltersNames"  => array("Баннер"),
        "FiltersTypes"   => array("BannerStatDBSelect"),
        "ViewOnly" => 1,
        "Function" => "dPrintStat",
    );
    
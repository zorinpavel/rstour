<?
    $usrGroup = 10;
    include_once(ADMIN_PATH."/setup.php");
    include_once("setup.php");


    AdminHeaders();
    MainNavi("banners");

    $Table = "Stat";
    include(EDIT_PATH."/select_function.php");

    function dPrintStat($Values) {
        ?>
        <script type="text/javascript" src="<?=BANNERS_STAT_PATH?>/swfobject.js"></script>
        <div id="flashcontent">
            <strong>You need to upgrade your Flash Player</strong>
        </div>

        <script type="text/javascript">
            // <![CDATA[
            var so = new SWFObject("<?=BANNERS_STAT_PATH?>/amline.swf", "amline", "700", "400", "8", "#FFFFFF");
            so.addVariable("path", "<?=BANNERS_STAT_PATH?>/");
            so.addVariable("settings_file", encodeURIComponent("<?=BANNERS_STAT_PATH?>/settings.xml"));
            <?

                $Hnt_Show = 0;
                $Hnt_Click = 0;
                $Hnt_Ctr = 0;
                if(is_array($Values)) {
                  ?>                  so.addVariable("chart_data", encodeURIComponent("<chart><?

      echo "<series>";
      foreach($Values as $Key => $Value) {
        echo "<value xid='$Key'>".date("d.m.y", strtotime($Value['St_Time']))."</value>";
      }
      echo "</series>";

      echo "<graphs>";

        echo "<graph gid='0'>";
        reset($Values);
        foreach($Values as $Key => $Value) {
          echo "<value xid='$Key'>".$Value['Hnt_Show']."</value>";
          $Hnt_Show = $Hnt_Show + $Value['Hnt_Show'];
        }
        echo "</graph>\" + \n";

        echo "\"<graph gid='1'>";
        reset($Values);
        foreach($Values as $Key => $Value) {
          echo "<value xid='$Key'>".$Value['Hnt_Click']."</value>";
          $Hnt_Click = $Hnt_Click + $Value['Hnt_Click'];
        }
        echo "</graph>\" + \n";

        echo "\"<graph gid='2'>";
        reset($Values);
        foreach($Values as $Key => $Value) {
          echo "<value xid='$Key'>";
          $Hnt_DCtr = ($Value['Hnt_Click'] == 0) ? 0 : round($Value['Hnt_Click']/$Value['Hnt_Show']*100, 2);
          echo $Hnt_DCtr;
          echo "</value>";
          $Hnt_Ctr = $Hnt_Ctr + $Hnt_DCtr;
        }
        echo "</graph>";

      echo "</graphs>";

      ?>                  </chart>"));<?
    } else {
      ?>                  so.addVariable("chart_data", encodeURIComponent("<chart></chart>"));<?
    }

    ?>
            so.write("flashcontent");
            // ]]>
        </script>
        <?
        $Count = count($Values);
        if($Count > 0) {
            ?>
            Средние значения: <b><? echo round($Hnt_Show/$Count, 2); ?> &nbsp; &nbsp; <? echo round($Hnt_Click/$Count, 2); ?>  &nbsp; &nbsp; <? echo round($Hnt_Ctr/$Count, 2); ?></b>
            <?
        }
        ?>
        <?
    }

    
    AdminFooter();
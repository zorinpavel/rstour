<?php
    
    class CImage {
        function __construct() {
            session_start();
            
            $verify_code = $_SESSION['verify_code'] = bin2hex(openssl_random_pseudo_bytes(2));
            
            header("Content-type: image/gif");
            header ("Expires: Mon, 1 Jun 1999 01:00:00 GMT");
            
            if(!function_exists("imagecreate")) {
                readfile("/img/sp.gif");
                exit;
            }
            
            $im = imagecreate(80, 40);
            
            $bg    = imagecolorallocate($im, 250, 250, 250);
            $black = imagecolorallocate($im, 0, 0, 0);
            $white   = imagecolorallocate($im, 255, 255, 255);
            $red = imagecolorallocate($im, 255, 0, 0);
            
            $len = strlen($verify_code);
            mt_srand($this->MakeSeed());
            
            $x = 10;
            for($i=0; $i < $len; ++$i) {
                imagettftext($im, 20, 0, $x, rand(25, 35), $black, CLIENT_PATH."/fonts/RobotoRegular.ttf", $verify_code[$i]);
                $x = $x + 15;
            }
            
            imagegif($im);
            exit;
        }
        
        function MakeSeed() {
            list($usec, $sec) = explode(' ', microtime());
            return((float)$sec + ((float)$usec * 100000));
        }
        
    }
    
    $image = new CImage();

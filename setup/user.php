<?php

    preg_match("/^(user\/)?([a-zA-Z0-9-_\.]+)(?<!\/)/", $_REQUEST['_path'], $Localdir);

    if($_REQUEST['_path']) {

        DB::getInstance();
        if((int)$Localdir[2])
            list($us_id, $Userdir) = mysql_fetch_row(db_query("SELECT us_id, us_id FROM Users WHERE us_id = ".(int)$Localdir[2]));
        else
            list($us_id, $Userdir) = mysql_fetch_row(db_query("SELECT us_id, localdir FROM Users WHERE localdir = '".strtolower(mysql_real_escape_string($Localdir[2]))."'"));

        if($Userdir) {

            _autoload("classUsers");
            classUsers::$PageUser = classUsers::GetUserInfo($us_id);

            $Filename =  $_SERVER['DOCUMENT_ROOT']."/user".preg_replace("/(user)?(\/)?($Userdir)/i", "", $_REQUEST['_path'])."index.php";

            if(file_exists($Filename)) {
                include($Filename);
            } else {
                /* 404 */
                include($_SERVER['DOCUMENT_ROOT']."/user/index.php");
            }

        } else {
            IncludeUrl();
        }
    } else {
        IncludeUrl();
    }


    function IncludeUrl() {
        if(file_exists($_SERVER['DOCUMENT_ROOT']."/".$_REQUEST['_path']."index.php")) {
            include($_SERVER['DOCUMENT_ROOT']."/".$_REQUEST['_path']."index.php");
        } else {
            header("HTTP/1.1 404 Not Found");
            header("Status: 404 Not Found");
            include($_SERVER['DOCUMENT_ROOT']."/error.php");
        }
    }
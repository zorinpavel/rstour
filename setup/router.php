<?php

    preg_match("/(.*\/)?([a-zA-Z0-9-_\.]+)\.html/", $_REQUEST['_path'], $Localdir);

    if($Localdir[2]) {
        DB::getInstance();
        if($Router = sefRouter::GetByUrl("/".$Localdir[1].$Localdir[2])) {
            switch($Router['ClassName']) {
                case 'classContent':
                    $classContentData = _autoload("classContentData");
                    $_REQUEST[$classContentData->ItemID] = $Router['ItemId'];
                    if(SEF_ITEM_SECTION) {
                        $classContentData->Ite_Code = $Router['ItemId'];
                        $classContentData->GetContentList(0, false);
                        $Sec_Code = $classContentData->Items[$Router['ItemId']]['Sec_Code'];
                        $Dir = classProperties::GetPath($Sec_Code);
                    }
                    break;
                case 'classTrip':
                    $classTrip = _autoload("classTrip");
                    $_REQUEST[classTrip::$ItemID] = $Router['ItemId'];
                    $Dir = classTrip::$SectionPath;
                    break;
                case 'Regions':
                    $_REQUEST['dR_Code'] = $Router['ItemId'];
                    break;
                case 'Cities':
                    $_REQUEST['dR_Code'] = DB::selectValue("SELECT dR_Code FROM dataCities WHERE dC_Code = ".$Router['ItemId']);
                    $_REQUEST['dC_Code'] = $Router['ItemId'];
                    break;
                case 'Types':
                    $_REQUEST['dT_Code'] = $Router['ItemId'];
                    break;
                default:
                    $_REQUEST['ItemId'] = $Router['ItemId'];
                    break;
            }
        } else {
            Error();
        }
    }

    if(SEF_ITEM_SECTION && $Dir) {
        if(file_exists(CLIENT_PATH.$Dir."index.php"))
            include(CLIENT_PATH.$Dir."index.php");
        else
            Error();
    } else {
        if(file_exists((CLIENT_PATH."/".preg_replace("/".$Localdir[2].".html/i", "", $_REQUEST['_path'])."index.php")))
            include(CLIENT_PATH."/".preg_replace("/".$Localdir[2].".html/i", "", $_REQUEST['_path'])."index.php");
        else
            Error();
    }
    
    function Error() {
        header("HTTP/1.1 404 Not Found");
        header("Status: 404 Not Found");
        include(CLIENT_PATH."/error.php");
        die;
    }